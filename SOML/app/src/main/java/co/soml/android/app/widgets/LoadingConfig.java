package co.soml.android.app.widgets;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since May.20.2015
 */
public class LoadingConfig {
    private static LoadingConfig mLoadingConfig;
    public int mDrawable;
    public int mResourceStringId;
    public String mLoading;
    public boolean mIsLoading;
    public Integer mTextColor;

    public LoadingConfig() {
        mResourceStringId = mDrawable = -1;
        mIsLoading = true;
    }

    public LoadingConfig setLoadingText(int id) {
        this.mResourceStringId = id;
        return this;
    }

    public LoadingConfig setLoadingText(String text) {
        this.mResourceStringId = -1;
        this.mLoading = text;
        return this;
    }

    public LoadingConfig setTextColor(Integer color) {
        this.mTextColor = color;
        return this;
    }

    public LoadingConfig setLoading(boolean isLoading) {
        this.mIsLoading = isLoading;
        return this;
    }

    public LoadingConfig clearLoadingText() {
        setLoadingText(null);
        return this;
    }

    public static LoadingConfig getDefault() {
        if (mLoadingConfig == null) {
            mLoadingConfig = new LoadingConfig();
        }
        return mLoadingConfig;
    }

    public LoadingConfig setDefault() {
        mLoadingConfig = this;
        return this;
    }

    public void setDefault(LoadingConfig loadingConfig) {
        mLoadingConfig = loadingConfig;
    }
}
