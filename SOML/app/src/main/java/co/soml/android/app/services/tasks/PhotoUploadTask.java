/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/17/15 3:40 PM.
 **/

package co.soml.android.app.services.tasks;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.IntentCompat;
import android.text.TextUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import co.soml.android.app.R;
import co.soml.android.app.StoryApp;
import co.soml.android.app.activities.StoryMainActivity;
import co.soml.android.app.controllers.ControllerMessage;
import co.soml.android.app.models.*;
import co.soml.android.app.models.Error;
import co.soml.android.app.services.PhotoResult;
import co.soml.android.app.services.UploadNotifier;
import co.soml.android.app.services.apis.AppRestApi;
import co.soml.android.app.services.rest.ItemTypeAdapterFactory;
import co.soml.android.app.utils.LogUtils;
import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;
import retrofit.mime.TypedFile;

public class PhotoUploadTask extends AsyncTask<Photo, Integer, PhotoResult> {

    public static final String TAG = LogUtils.makeLogTag(PhotoUploadTask.class.getSimpleName());
    protected Photo mPhoto;
    protected Story mStory;
    protected Context mContext;
    protected UploadNotifier mUploadNotifier;
    private String mErrorMessage = "";
    private boolean mIsMediaError = false;
    private boolean mErrorUnavailable = false;
    private String mAction = null;
    private Handler mServiceHandler = null;
    private Callback mCallback;
    private RequestInterceptor mRequestInterceptor;
    private Gson mGson;
    private GsonConverter mGsonConverter = new GsonConverter(mGson);
    private PhotoUploadProgressListener mProgressListener;

    private long mTotalSize;
    private int mRequestOwner;
    private int mRequestMessage;

    public PhotoUploadTask(String action, Handler serviceHandler) {
        this.mAction = action;
        this.mServiceHandler = serviceHandler;
        this.mGson = new Gson();
        this.mGsonConverter = new GsonConverter(mGson);
        mGson = new GsonBuilder().registerTypeAdapterFactory(new ItemTypeAdapterFactory()).create();
    }

    public void setUploadNotifier(UploadNotifier uploadNotifier) {
        mUploadNotifier = uploadNotifier;
    }

    public UploadNotifier getUploadNotifier() {
        return mUploadNotifier;
    }

    public Photo getPhoto() {
        return mPhoto;
    }

    public void setPhoto(Photo photo) {
        mPhoto = photo;
    }

    public Story getStory() {
        return mStory;
    }

    public void setStory(Story story) {
        mStory = story;
    }

    public int getRequestOwner() {
        return mRequestOwner;
    }

    public void setRequestOwner(int requestOwner) {
        mRequestOwner = requestOwner;
    }

    public int getRequestMessage() {
        return mRequestMessage;
    }

    public void setRequestMessage(int requestMessage) {
        mRequestMessage = requestMessage;
    }

    @Override
    protected PhotoResult doInBackground(Photo... params) {
        final String fn = "UploadingPhoto(): ";
        mPhoto = params[0];
        mPhoto.setUploaded(false);

        final PhotoResult result = new PhotoResult();
        final AppRestApi api = RestApiUtils.getAppRestApi();
        mUploadNotifier = new UploadNotifier(mContext);

        String photoCaption = TextUtils.isEmpty(mPhoto.getCaption()) ? null : mPhoto.getCaption();
        String photoUploadTitle = String.format("Posting %s", photoCaption);
        String photoUploadMessage = String.format("Uploading %s content", mContext.getResources().getText(R.string.photo_id));
        mUploadNotifier.updateNotificationMessage(photoUploadTitle, photoUploadMessage);

        final File photoFile = new File(mPhoto.getOriginalUrl());
        TypedFile typedImage = new TypedFile("application/octet-stream", photoFile);
        mTotalSize = photoFile.length();

        mProgressListener = new PhotoUploadProgressListener() {
            @Override
            public void transferred(long num) {
                int progress = (int) ((num / (float) mTotalSize) * 100);
                LogUtils.LOGD(TAG, String.format("Percentage of uploading: %d", progress));
                sendProgressMessage((int) mPhoto.getId(), mStory.getId(), mPhoto.getCaption(), progress, num, mTotalSize);
            }
        };

        CountingTypedFile photoCountingFile = new CountingTypedFile("image/jpeg",photoFile, mProgressListener);
        mCallback = new Callback<PhotoResult>() {
            @Override
            public void success(PhotoResult photoResult, Response response) {
                result.setPhoto(photoResult.getPhoto());
                result.setResultCode(response.getStatus());
                result.setResultMessage(response.getReason());
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_UPLOAD_PHOTO_COMPLETED %s", fn, result));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_UPLOAD_PHOTO_COMPLETED, mRequestOwner, mRequestMessage, result);
            }
            @Override
            public void failure(RetrofitError retrofitError) {
                final Error error = new Error();
                LogUtils.LOGD(TAG, String.format("Response: %s", retrofitError.getResponse()));
                final int errorCode = retrofitError.getResponse().getStatus();
                final String errorMessage = retrofitError.getMessage();
                final String errorException = retrofitError.getResponse().getReason();

                error.setErrorCode(errorCode);
                error.setErrorMessage(errorMessage);
                error.setErrorException(errorException);
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_ERROR -- %d,%s", fn, errorCode, errorMessage));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_UPLOAD_PHOTO_FAILED, mRequestOwner, mRequestMessage, error);
            }
        };
        api.uploadPhotos(mStory.getId(), photoCountingFile, mPhoto.getCaption(), mCallback);
        return result;
    }

    @Override
    protected void onCancelled(PhotoResult photoResult) {
        super.onCancelled(photoResult);
        mUploadNotifier.updateNotificationWithError(mErrorMessage, mIsMediaError, false);
    }

    public void sendMessage(int code, int requestOwner, int requestMsg, Object obj) {
        String fn = "sendMessage(): ";
        final Message msg = new Message();
        LogUtils.LOGD(TAG, fn + "code = " + ControllerMessage.toString(code) + "(" + code +")");
        LogUtils.LOGD(TAG, fn + "requestOwner = " + requestOwner);
        LogUtils.LOGD(TAG, fn + "requestMessage = " + requestMsg);

        if (obj instanceof Error) {
            final Error error = (Error) obj;
            int errorCode = error.getErrorCode();
            LogUtils.LOGD(TAG, fn + "errorCode = " + ControllerMessage.toString(error.getErrorCode()) + "(" + errorCode + ")");
        }

        msg.what = code;
        msg.arg1 = requestOwner;
        msg.arg2 = requestMsg;
        msg.obj = obj;

        if (isCancelled()) {
            LogUtils.LOGD(TAG, "Task Cancelled. Do not send message");
        } else {
            mServiceHandler.sendMessage(msg);
        }
    }

    public interface PhotoUploadProgressListener {
        void transferred(long num);
    }

    public class CountingTypedFile extends TypedFile {
        private static final int BUFFER_SIZE = 4096;
        private final PhotoUploadProgressListener mListener;

        public CountingTypedFile(String mimeType, File file, PhotoUploadProgressListener listener) {
            super(mimeType, file);
            mListener = listener;
        }

        public void writeTo(OutputStream out) throws IOException {
            byte[] buffer = new byte[BUFFER_SIZE];
            final FileInputStream inStream = new FileInputStream(super.file());
            long total = 0;
            try {
                int read;
                while ((read = inStream.read()) != -1) {
                    total += read;
                    this.mListener.transferred(total);
                    out.write(buffer, 0, read);
                }
            } finally {
                inStream.close();
            }
        }
    }

    protected void sendProgressMessage(int id, long storyId, String photoCaption, int progress, long uploadedSize, long totalSize) {
        // Send Message to UI Component
        final Message msg = new Message();
        msg.what = ControllerMessage.UPLOADING_CONTENT_PROGRESS;
        msg.arg1 = id;
        msg.arg2 = progress;

        final Bundle bundle = new Bundle();
        bundle.putLong("StoryId", storyId);
        bundle.putString("PhotoCaption", photoCaption);
        bundle.putLong("UploadedSize", uploadedSize);
        bundle.putLong("TotalSize", totalSize);
        msg.obj = bundle;

        PhotoUploadTask.this.mServiceHandler.sendMessage(msg);

        try {
            Thread.sleep(200);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
}
