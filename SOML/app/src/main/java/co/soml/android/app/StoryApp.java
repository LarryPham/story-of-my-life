package co.soml.android.app;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.http.HttpResponseCache;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.facebook.appevents.AppEventsLogger;
import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.CacheRequest;
import java.net.CacheResponse;
import java.net.URI;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import co.soml.android.app.activities.StoryMainActivity;
import co.soml.android.app.activities.WelcomeActivity;
import co.soml.android.app.controllers.ActivityHandler;
import co.soml.android.app.controllers.ControllerMessage;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.models.Error;

import co.soml.android.app.models.StorySettings;
import co.soml.android.app.models.UpdatingStoryList;
import co.soml.android.app.models.UploadJobList;
import co.soml.android.app.models.User;
import co.soml.android.app.services.StoriesResult;
import co.soml.android.app.services.StoryService;
import co.soml.android.app.services.StoryUploadService;
import co.soml.android.app.services.UserResult;
import co.soml.android.app.services.tasks.Action;
import co.soml.android.app.utils.AccountUtils;
import co.soml.android.app.utils.ActivityLauncher;
import co.soml.android.app.utils.BitmapLruCache;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.utils.ProfilingUtils;
import io.fabric.sdk.android.Fabric;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its
 * documentation are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable
 * form without the prior written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with
 * respect to the contents, and assumes no responsibility for any errors that might appear in the software and
 * documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.01.2015
 */
public class StoryApp extends MultiDexApplication {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "lWV1V4ilkYiXDODvhu61jybsc";
    private static final String TWITTER_SECRET = "yK52fc43NT9ZULaeNrFYC2x8DBNQYu1RWRJn1H6fceM5RhJ4LJ";

    public static final String TAG = AppConstants.PREFIX + StoryApp.class.getSimpleName();
    private StoryDataModel mDataModel = new StoryDataModel();
    private UpdatingStoryList mUpdateStoryList = new UpdatingStoryList();

    private int mTabIndex = 0;
    private Locale mLocale = null;
    private boolean isChangedLocale = false;
    private boolean isChangedLocaleForStoryList = false;
    private boolean isUpdateStoryList = false;
    private int mFlag = 0;

    private SharedPreferences mPreferences;
    private ArrayList<ActivityHandler> mHandlerList = new ArrayList<ActivityHandler>();
    private AppMainHandler mAppMainHandler = new AppMainHandler();
    private StoryService mStoryService;
    private StoryUploadService mUploadService;
    private UploadJobList mUploadJobList;

    private HashMap<Integer, PendingIntent> mAlarmPendingIntent = new HashMap<>();
    private HashMap<Integer, Boolean> mExitedStoryMap = new HashMap<>();
    private OnStoryUploadedListener mUploadedListener = null;
    private boolean mStoriesShouldRefresh;

    private StorySettings mSettings;
    private TwitterAuthConfig mTwitterAuthConfig;
    private static Picasso mPicasso;
	private static Context mContext;
    private static StoryApp mInstance;
    private static BitmapLruCache mBitmapCache;

    public boolean mShouldBeRefreshScreen = false;
    public User mCurrentUser;

    public StoryApp() {
        mInstance = this;
    }

    public static StoryApp getInstance() {
        if (mInstance != null && mInstance instanceof StoryApp) {
            return (StoryApp) mInstance;
        } else {
            mInstance = new StoryApp();
            mInstance.onCreate();
            return (StoryApp) mInstance;
        }
    }

    public User getCurrentUser() {
        return this.mCurrentUser;
    }

    public void setCurrentUser(User user) {
        mCurrentUser = user;
    }

    public void removeCurrentUser() {
        if (mCurrentUser!= null && mCurrentUser.getId() != 0) {
            mCurrentUser = new User();
        }
    }

    public boolean isShouldBeRefreshScreen() {
        return mShouldBeRefreshScreen;
    }

    public void setShouldBeRefreshScreen(boolean shouldBeRefreshScreen) {
        mShouldBeRefreshScreen = shouldBeRefreshScreen;
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            LogUtils.LOGD(TAG, String.format("OnReceive: Action[%s]", action));
            if (action.equals(Intent.ACTION_CONFIGURATION_CHANGED)) {
                Configuration config = getResources().getConfiguration();
                if (config != null) {
                    LogUtils.LOGD(TAG, String.format("Changed Language - [%s][%s]", config.locale.getLanguage(),
                            config.locale.getCountry()));
                }
            }
        }
    };

    public static BitmapLruCache getBitmapCache() {
        if (mBitmapCache == null) {
            int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
            int cacheSize = maxMemory / 16;
            mBitmapCache = new BitmapLruCache(cacheSize);
        }
        return mBitmapCache;
    }

    public UpdatingStoryList getUpdateStoryList() {
        return mUpdateStoryList;
    }

    public void setUpdateStoryList(UpdatingStoryList storyList) {
        this.mUpdateStoryList = storyList;
    }

    public int getTabIndex() {
        return this.mTabIndex;
    }

    public void setTabIndex(int tabIndex) {
        this.mTabIndex = tabIndex;
    }

    public StoryDataModel getAppDataModel() {
        return mDataModel;
    }

    public Locale getCurrentLocale() {
        return this.mLocale;
    }

    public void setCurrentLocale(Locale locale) {
        this.mLocale = locale;
    }

    public boolean isChangedLocale() {
        return this.isChangedLocale;
    }

    public void setIsChangedLocale(boolean isChangedLocale) {
        this.isChangedLocale = isChangedLocale;
    }

    public boolean isUpdateStoryList() {
        return this.isUpdateStoryList;
    }

    public void setIsUpdateStoryList(boolean updatedStoryList) {
        this.isUpdateStoryList = updatedStoryList;
    }

    public StorySettings getAppSettings() {
        return mSettings;
    }

    public static Context getContext() {
        return mContext;
    }

    public StoryUploadService getUploadService() {
        return mUploadService;
    }

    public void setUploadService(StoryUploadService uploadService) {
        mUploadService = uploadService;
    }

    public UploadJobList getUploadJobList() {
        return mUploadJobList;
    }

    public void setUploadJobList(UploadJobList uploadJobList) {
        this.mUploadJobList = uploadJobList;
    }

    public void setOnStoryUploadedListener(OnStoryUploadedListener listener) {
        mUploadedListener = listener;
    }

    public OnStoryUploadedListener getUploadedListener() {
        return mUploadedListener;
    }

    public void storyUploaded(int id) {
        if (mUploadedListener != null) {
            try {
                mUploadedListener.onStoryUploaded(id);
            } catch (Exception ex) {
                mStoriesShouldRefresh = true;
            }
        } else {
            mStoriesShouldRefresh = true;
        }
    }

    public void storyUploadFailed(int id) {
        if (mUploadedListener != null) {
            try {
                mUploadedListener.onStoryUploadedFailed(id);
            } catch (Exception ex) {
                mStoriesShouldRefresh = true;
            }
        } else {
            mStoriesShouldRefresh = true;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
        mSettings = new StorySettings(this);
        MultiDex.install(this);
	    ProfilingUtils.start("StoryApp.onCreate()");
        mTwitterAuthConfig = new TwitterAuthConfig(AppConstants.TWITTER_KEY, AppConstants.TWITTER_SECRET);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(mTwitterAuthConfig), new Twitter(authConfig));
	    initCache();

	    ApplicationLifecycleMonitor pnBackendMonitor = new ApplicationLifecycleMonitor();
	    registerComponentCallbacks(pnBackendMonitor);
	    registerActivityLifecycleCallbacks(pnBackendMonitor);
    }


	private void initCache() {
		final File cachedDir = this.getCacheDir();
		java.net.ResponseCache.setDefault(new java.net.ResponseCache() {

			private String escape(String url) {
				return url.replace("/", "-").replace(".", "-");
			}

			@Override
			public CacheResponse get(URI uri, String requestMethod, Map<String, List<String>> requestHeaders) throws IOException {
				if (uri == null) {
					LogUtils.LOGE(TAG, "get(): Error: uri is null.");
					return null;
				}
				final File file = new File(cachedDir, escape(uri.getPath()));
				if (file.exists()) {
					return new java.net.CacheResponse() {

						@Override
						public InputStream getBody() throws IOException {
							return new java.io.FileInputStream(file);
						}

						@Override
						public Map<String, List<String>> getHeaders() throws IOException {
							return null;
						}
					};
				} else {
					return null;
				}
			}

			@Override
			public CacheRequest put(URI uri, URLConnection connection) throws IOException {
				final File file = new File(cachedDir, escape(connection.getURL().getPath()));
				return new java.net.CacheRequest() {
					@Override
					public void abort() {
						file.delete();
					}

					@Override
					public OutputStream getBody() throws IOException {
						return new FileOutputStream(file);
					}
				};
			}
		});
	}

	public static void enableHttpResponseCache(Context context) {
		try {
			long httpCacheSize = 5 * 1024 * 1024;
			File httpCacheDir = new File(context.getCacheDir(), "https");
			HttpResponseCache.install(httpCacheDir, httpCacheSize);
		} catch (IOException ex) {
			LogUtils.LOGW(TAG, "Failed to enable https response cache");
		}
	}

	public static void flushHttpCache() {
		HttpResponseCache cache = HttpResponseCache.getInstalled();
		if (cache != null) {
			cache.flush();
		}
	}

    public void addHandlerIfNotExist(ActivityHandler handler) {
        boolean blnExisted = false;
        try {
            int size = mHandlerList.size();
            for (int i = 0; i < size; i++) {
                if (mHandlerList.get(i).hashCode() == handler.hashCode()) {
                    blnExisted = true;
                    break;
                }
            }

            if (!blnExisted) {
                LogUtils.LOGE(TAG, "addHandlerIfNotExisted");
                addHandler(handler);
            }
        } catch (Exception ex) {
            LogUtils.LOGE(TAG, String.format("addHandlerIfNotExit()" + "throws exception-[%s]", ex.getMessage()));
            ex.printStackTrace();
        }
    }

    public AppMainHandler getAppMainHandler() {
        return this.mAppMainHandler;
    }

    public void setStoryService(StoryService service) {
        this.mStoryService = service;
    }

    public StoryService getStoryService() {
        return this.mStoryService;
    }

    public void addHandler(ActivityHandler handler) {
        LogUtils.LOGD(TAG, String.format("addHandler: %s", handler.toString()));
        mHandlerList.add(handler);
    }

    public void removeHandler(ActivityHandler handler) {
        LogUtils.LOGD(TAG, String.format("removing the handler-[%s]", handler.toString()));
        mHandlerList.remove(handler);
        LogUtils.LOGD(TAG, String.format("count of remained handlers: %d", mHandlerList.size()));
    }

    public void removeAllHandler() {
        LogUtils.LOGD(TAG, String.format("remove all of handlers[%d]", mHandlerList.size()));
        mHandlerList.clear();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        AppEventsLogger.deactivateApp(this);
    }

    @SuppressLint("HandlerLeak")
    public class AppMainHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            String fn = "handleMessage(): ";
            LogUtils.LOGD(TAG, fn + "[AppMainHandler]" + msg.toString());
            final int handlersSize = mHandlerList.size();
            LogUtils.LOGD(TAG, fn + String.format("[AppMainHandler] handleList's Size: %d", handlersSize));
            switch (msg.what) {
                case ControllerMessage.REQUEST_STORY_IMAGE_FROM_SERVER_COMPLETED: {
                    Log.i(TAG, "REQUEST_STORY_IMAGE_FROM_SERVER_COMPLETED");
                    if (msg.arg1 == AppConstants.HASH_CODE_FOR_IMAGE) {
                        Log.i(TAG, "REQUEST_STORY_IMAGE_FROM_SERVER_COMPLETED");
                    }
                    break;
                }
                case ControllerMessage.AUTO_UPDATE_FEED: {
                    break;
                }

                case ControllerMessage.AUTO_UPDATE_COMPLETED: {
                    StoriesResult result = (StoriesResult) msg.obj;
                    // Commit result to Db
                    break;
                }

                case ControllerMessage.AUTO_UPDATE_FAILED: {
                    final Error error = (Error) msg.obj;
                    int errorCode = error.getErrorCode();
                    LogUtils.LOGE(TAG, fn + " ErrorCode: " + ControllerMessage.toString(error.getErrorCode()) + "(" + errorCode + ")");
                    break;
                }
                // Handling the Email Login's case
                case ControllerMessage.REQUEST_TO_SERVER_SIGN_IN_WITH_EMAIL_ACCOUNT_COMPLETED: {
                    LogUtils.LOGI(TAG, "REQUEST_TO_SERVER_SIGN_IN_EMAIL_ACCOUNT_COMPLETED");
                    UserResult result = (UserResult) msg.obj;
                    if (result == null) {
                        return;
                    }
                    // If app don't have any active acocunt already, let's add this account to preference storage.
                    if (!AccountUtils.hasActiveAccount(mContext)) {
                        // Checking the result's token and saving the Authentication's Token to the Preferences object.
                        if (result.getUser().getAuthToken() != null) {
                            final String authToken = result.getUser().getAuthToken();
                            final String emailAddress = result.getUser().getEmail();
                            final String fullName = result.getUser().getFullName();
                            final int userId = result.getUser().getId();

                            setCurrentUser(new User(result.getUser()));
                            AccountUtils.setAuthToken(mContext, result.getUser().getFullName(), authToken);
                            // Saving account name, image cover_url and profile_url
                            AccountUtils.setActiveAccount(mContext, result.getUser().getFullName(), AppConstants.EMAIL_ACCOUNT_TYPE);
                            AccountUtils.setEmailAccountAddress(mContext, fullName, emailAddress);
                            AccountUtils.setEmailAccountImageURL(mContext, result.getUser().getFullName(),
                                    result.getUser().getAvatar() != null ? result.getUser().getAvatar().mUrl : null);
                            AccountUtils.setEmailCoverURL(mContext, result.getUser().getFullName(), result.getUser().getAvatar() != null ? (result
                                    .getUser().getAvatar().mThumb != null ? result.getUser().getAvatar().mThumb.mUrl : null) : null);
                            AccountUtils.setUserId(mContext, userId);
                        }
                    }
                    break;
                }
                // Handling the GuestSignin's case.
                case ControllerMessage.REQUEST_TO_SERVER_SIGN_IN_GUEST_COMPLETED: {
                    LogUtils.LOGD(TAG, "REQUEST_TO_SERVER_SIGN_IN_GUEST_COMPLETED");
                    UserResult result = (UserResult) msg.obj;
                    if (result == null) {
                        return;
                    }
                    // Checking the current acitve account which has already or not?
                    if (!AccountUtils.hasActiveAccount(mContext)) {
                        //if (result.getUser().getAuthToken() != null) {
                        setCurrentUser(new User(result.getUser()));
                        final String authToken = result.getUser().getAuthToken();
                        // If user is anonymous, we should add fullname for this account full-name as it to Preference storage.
                        final String anonymousName = "Anonymous";
                        AccountUtils.setAuthToken(mContext, anonymousName, authToken);
                        // Sets the current user as active account
                        AccountUtils.setActiveAccount(mContext, anonymousName, AppConstants.GUEST_ACCOUNT_TYPE);
                        //}
                    }
                    break;
                }
                // Handling the Facebook SocialNetwork's SignIn case
                case ControllerMessage.REQUEST_TO_SERVER_SIGN_IN_WITH_FACEBOOK_COMPLETED: {
                    LogUtils.LOGD(TAG, "REQUEST_TO_SERVER_SIGN_IN_FACEBOOK_COMPLETED");
                    UserResult result = (UserResult) msg.obj;

                    if (result == null) {
                        return;
                    }
                    if (!AccountUtils.hasActiveAccount(mContext)) {
                        if (result.getUser().getAuthToken() != null) {
                            final String authToken = result.getUser().getAuthToken();
                            final String fbUserName = result.getUser().getFullName();
                            final String fbProfileImageURL = result.getUser().getAvatar().mUrl;

                            setCurrentUser(new User(result.getUser()));
                            // For Social network, we can not added profile_url, or cover_url into the {@link Application} object.
                            // Just updating them after feedback the signals to controllers fo update it later.
                            AccountUtils.setFacebookAuthToken(mContext, fbUserName, authToken);
                            AccountUtils.setActiveAccount(mContext, fbUserName, AppConstants.FACEBOOK_ACCOUNT_TYPE);
                            // Sets this facebook account as active account
                            AccountUtils.setEmailAccountAddress(mContext, fbUserName, result.getUser().getEmail());
                            AccountUtils.setFacebookAccountProfileImageURL(mContext, fbUserName, fbProfileImageURL);
                        }
                    }
                    break;
                }
                // Handling the Twitter's SignIn case
                case ControllerMessage.REQUEST_TO_SERVER_SIGN_IN_WITH_TWITTER_COMPLETED: {
                    LogUtils.LOGD(TAG, "REQUEST_TO_SERVER_SIGN_IN_TWITTER_COMPLETED");
                    UserResult result = (UserResult) msg.obj;
                    if (result == null) {
                        return;
                    }

                    if (!AccountUtils.hasActiveAccount(mContext)) {
                        if (result.getUser().getAuthToken() != null) {
                            final String authToken = result.getUser().getAuthToken();
                            final String twitterUserName = result.getUser().getFullName();
                            final String profileURL = result.getUser().getAvatar().mUrl;

                            AccountUtils.setTwitterAuthToken(mContext, twitterUserName, authToken);
                            AccountUtils.setActiveAccount(mContext, twitterUserName, AppConstants.TWITTER_ACCOUNT_TYPE);
                            AccountUtils.setEmailAccountAddress(mContext, twitterUserName, result.getUser().getEmail());
                            AccountUtils.setTwitterAccountProfileImageURL(mContext, twitterUserName, profileURL);

                            setCurrentUser(new User(result.getUser()));
                        }
                    }
                    break;
                }
                // Handling the User's Registration
                case ControllerMessage.REQUEST_TO_SERVER_REGISTER_USER_COMPLETED: {
                    LogUtils.LOGD(TAG, "REQUEST_TO_SERVER_REGISTER_USER_COMPLETED");
                    UserResult result = (UserResult) msg.obj;
                    if (result == null) {
                        return;
                    }

                    if (!AccountUtils.hasActiveAccount(mContext)) {
                        if (result.getUser().getAuthToken() != null) { // Checking user's authtoken is not null
                            final String authToken = result.getUser().getAuthToken();
                            final String userName = result.getUser().getFullName();
                            final String userEmail = result.getUser().getEmail();
                            final String profileImageURL = result.getUser().getAvatar().mUrl;

                            setCurrentUser(new User(result.getUser()));
                            AccountUtils.setAuthToken(mContext, userName, authToken);
                            // For first registered account, we don't have the profile_image url or user cover_url for savting them into preference
                            // object that why we should update them after calling <code>Updater</code> for uploading the profile_url or image_url
                            AccountUtils.setActiveAccount(mContext, userName, AppConstants.EMAIL_ACCOUNT_TYPE);
                            AccountUtils.setEmailAccountAddress(mContext, userName, userEmail);
                            AccountUtils.setEmailAccountImageURL(mContext, userName, profileImageURL);
                        }
                    }
                    break;
                }
            }

            for (int i = 0; i < handlersSize; i++) {
                LogUtils.LOGD(TAG, fn + "Message's Id: " + msg.what + " handler of " + mHandlerList.get(i).getClass().getSimpleName());
                ActivityHandler activityHandler = mHandlerList.get(i);
                LogUtils.LOGD(TAG, fn + " handlerList's size: " + mHandlerList.size());
                activityHandler.callBack(msg);
            }
        }
    }

	public void showProgressDialog(boolean cancelable) {
		final Intent intent = new Intent();
		intent.setAction(Action.SHOW_PROGRESS);
		sendBroadcast(intent);
	}

	public void dismissWaitDialog() {
		final Intent intent = new Intent();
		intent.setAction(Action.DISMISS);
		sendBroadcast(intent);
	}

	public static void signOutAsyncWithProgressBar(Context context, SignOutAsync.SignOutCallback callback) {
		new SignOutAsync(context, callback).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}

    public String getCacheDirPath() {
        return getCacheDir().getAbsolutePath();
    }

	public static class SignOutAsync extends AsyncTask<Void, Void, Void> {

		ProgressDialog mProgressDialog;
		WeakReference<Context> mWeakContext;
		SignOutCallback mCallback;

		public interface SignOutCallback{
			void onSignOut();
		}

		public SignOutAsync(Context context, SignOutCallback callback) {
			mWeakContext = new WeakReference<Context>(context);
			mCallback = callback;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Context context = mWeakContext.get();
			if (context != null) {
				mProgressDialog = ProgressDialog.show(context, null, context.getText(R.string.signning_out));
			}
		}

		@Override
		protected Void doInBackground(Void... params) {
			Context context = mWeakContext.get();
			if (context != null) {
				signOut(context);
                StoryApp.getInstance().removeCurrentUser();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void aVoid) {
			super.onPostExecute(aVoid);
			if (mProgressDialog != null) {
				mProgressDialog.dismiss();
			}

			if (mCallback != null) {
				mCallback.onSignOut();
			}
		}
	}

	public static void signOut(final Context context) {
		AccountUtils.removeAllPrefs(context);
	}

    public interface OnStoryUploadedListener {
        void onStoryUploaded(int id);
        void onStoryUploadedFailed(int id);
    }

    private class ApplicationLifecycleMonitor implements Application.ActivityLifecycleCallbacks, ComponentCallbacks2 {

        private final int DEFAULT_TIMEOUT = 2 * 60;
        private Date mLastPingDate;
        private Date mApplicationOpenedDate;
        boolean isInBackground = true;

        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

        }

        @Override
        public void onActivityStarted(Activity activity) {

        }

        @Override
        public void onActivityResumed(Activity activity) {

        }

        @Override
        public void onActivityPaused(Activity activity) {

        }

        @Override
        public void onActivityStopped(Activity activity) {

        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

        }

        @Override
        public void onActivityDestroyed(Activity activity) {

        }

        @Override
        public void onTrimMemory(int level) {
            if (level == ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN) {
                isInBackground = true;
            }
        }

        @Override
        public void onConfigurationChanged(Configuration newConfig) {

        }

        @Override
        public void onLowMemory() {

        }
    }

}


