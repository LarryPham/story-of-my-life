package co.soml.android.app.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import co.soml.android.app.AppConstants;
import co.soml.android.app.R;
import co.soml.android.app.StoryApp;
import co.soml.android.app.activities.BaseActivity;
import co.soml.android.app.activities.InvalidateParam;
import co.soml.android.app.activities.SessionsActivity;
import co.soml.android.app.activities.StoryMainActivity;
import co.soml.android.app.controllers.ControllerMessage;
import co.soml.android.app.controllers.SessionController;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.models.User;
import co.soml.android.app.utils.CommonUtil;
import co.soml.android.app.utils.EditTextUtils;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.widgets.ProgressWheel;
import co.soml.android.app.widgets.StoryTextView;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.23.2015
 */
public class NewUserFragment extends BaseFragment implements EmailAuthPolicy, TextWatcher {
    public static final String TAG = LogUtils.makeLogTag(NewUserFragment.class);
    public static final String FRAGMENT_PARAMS = "SessionsParams";
    public static final String FRAGMENT_PAGE_PARAMS = "SessionPage";

    public SessionController mController;
    private BaseActivity mActivity;
    private StoryDataModel mDataModel;
    protected StoryApp mStoryApp;

    @Bind(R.id.signup_email_edittext)
    EditText mEmailEditText;
    @Bind(R.id.signup_password_edittext)
    EditText mPasswordEditText;
    @Bind(R.id.signup_password_confirmation_edittext)
    EditText mPasswordConfirmationEditText;
    @Bind(R.id.signup_user_descriptions)
    EditText mUserDescriptionEditText;
    @Bind(R.id.signup_firstname_edittext)
    EditText mUserFirstName;
    @Bind(R.id.signup_lastname_edittext)
    EditText mUserLastName;
    @Bind(R.id.signup_email_button)
    StoryTextView mSignupButton;
    @Bind(R.id.signup_progress_text)
    StoryTextView mProgressTextView;
    @Bind(R.id.register_progress_bar)
    RelativeLayout mProgressBarLayout;
    @Bind(R.id.signup_progress_wheel)
    ProgressWheel mProgressWheel;

    public static NewUserFragment newInstance() {
        LogUtils.LOGD(TAG, "NewInstance(): ");
        NewUserFragment NewUserFragment = new NewUserFragment();
        final Bundle args = new Bundle();
        args.putString(FRAGMENT_PARAMS, AppConstants.SIGNUP_FRAGMENT_KEY);
        NewUserFragment.setArguments(args);
        return NewUserFragment;
    }

    public NewUserFragment() {
    }

    private View.OnClickListener mSignupListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            signUp();
        }
    };

    private final TextView.OnEditorActionListener mEditorActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            return onDoneEvent(actionId, event);
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (SessionsActivity) getActivity();
        mStoryApp = (StoryApp) mActivity.getApplicationContext();
        mDataModel = mStoryApp.getAppDataModel();
        mController = new SessionController(mActivity, this, mDataModel);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.layout_signup_account, container, false);
        ButterKnife.bind(this, rootView);

        mSignupButton.setOnClickListener(mSignupListener);

        mUserFirstName.addTextChangedListener(this);
        mUserLastName.addTextChangedListener(this);
        mEmailEditText.addTextChangedListener(this);
        mPasswordEditText.addTextChangedListener(this);

        mPasswordConfirmationEditText.addTextChangedListener(this);
        mUserDescriptionEditText.addTextChangedListener(this);
        mUserDescriptionEditText.setOnEditorActionListener(mEditorActionListener);

        return rootView;
    }

    public boolean isEmailValid(String emailAddress) {
        if (TextUtils.isEmpty(emailAddress)) {
            return false;
        }
        return Patterns.EMAIL_ADDRESS.matcher(emailAddress).matches();
    }

    private boolean isFieldsFilled() {
        return EditTextUtils.getText(mUserFirstName).trim().length() > 0
                && EditTextUtils.getText(mUserLastName).trim().length() > 0
                && EditTextUtils.getText(mEmailEditText).trim().length() > 0
                && EditTextUtils.getText(mPasswordEditText).trim().length() > 0
                && EditTextUtils.getText(mPasswordConfirmationEditText).trim().length() > 0;
    }

    public void signUp() {
        if (!isUserDataValid()) {
            return;
        }

        if (!CommonUtil.isNetworkOnline(mActivity)) {
            return;
        }

        String userEmailAddress = EditTextUtils.getText(mEmailEditText).trim();
        String userPassword = EditTextUtils.getText(mPasswordEditText).trim();
        String userPasswordConfirmation = EditTextUtils.getText(mPasswordConfirmationEditText).trim();
        String userDescription = EditTextUtils.getText(mUserDescriptionEditText).trim();
        String userFullName = EditTextUtils.getText(mUserFirstName).trim().concat(" ")
                .concat(EditTextUtils.getText(mUserLastName).trim());

        SessionController.AuthenticateReqParam requestParam = new SessionController
                .AuthenticateReqParam(userFullName, userEmailAddress,
                userPassword, userPasswordConfirmation, userDescription);
        mController.sendMessage(ControllerMessage.REQUEST_TO_SERVER_ACCOUNT_REGISTER, requestParam);

        startProgress(getString(R.string.connecting_to_server));
    }

    @Override
    public void startProgress(String message) {
        mSignupButton.setVisibility(View.GONE);
        mProgressBarLayout.setVisibility(View.VISIBLE);
        mProgressTextView.setVisibility(View.VISIBLE);

        mEmailEditText.setEnabled(false);
        mPasswordEditText.setEnabled(false);
        mUserFirstName.setEnabled(false);
        mUserLastName.setEnabled(false);
        mPasswordConfirmationEditText.setEnabled(false);
        mUserDescriptionEditText.setEnabled(false);
        mProgressWheel.spin();
        mProgressBarLayout.setEnabled(true);
        mProgressTextView.setText(message);
    }

    @Override
    public void updateProgress(String message) {
        mProgressTextView.setText(message);
    }

    @Override
    public void endProgress() {
        mSignupButton.setVisibility(View.VISIBLE);
        mProgressBarLayout.setVisibility(View.GONE);
        mProgressTextView.setVisibility(View.GONE);

        mUserFirstName.setEnabled(true);
        mUserLastName.setEnabled(true);
        mEmailEditText.setEnabled(true);
        mPasswordEditText.setEnabled(true);
        mPasswordConfirmationEditText.setEnabled(true);
        mUserDescriptionEditText.setEnabled(true);
        mProgressBarLayout.setEnabled(true);
        mProgressTextView.setEnabled(true);
    }

    @Override
    public void onDoneAction() {
        signUp();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public boolean isUserDataValid() {
        final String email = EditTextUtils.getText(mEmailEditText).trim();
        final String password = EditTextUtils.getText(mPasswordEditText).trim();
        final String passwordConfirmation = EditTextUtils.getText(mPasswordConfirmationEditText).trim();
        boolean resultValue = true;
        if (!isEmailValid(email)) {
            mEmailEditText.setError(getString(R.string.email_required_field));
            mEmailEditText.requestFocus();
            resultValue = false;
        }
        if (TextUtils.isEmpty(email)) {
            mEmailEditText.setError(getString(R.string.email_required_field));
            mEmailEditText.requestFocus();
            resultValue = false;
        }

        if (TextUtils.isEmpty(password)) {
            mPasswordEditText.setError(getString(R.string.password_required_field));
            mPasswordEditText.requestFocus();
            resultValue = false;
        }

        if (TextUtils.isEmpty(passwordConfirmation)) {
            mPasswordConfirmationEditText.setError(getString(R.string.password_confirmation_required_field));
            mPasswordConfirmationEditText.requestFocus();
            resultValue = false;
        }

        if (!password.equalsIgnoreCase(passwordConfirmation)) {
            mPasswordConfirmationEditText.setError(getString(R.string.password_confirmation_same_password));
            mPasswordConfirmationEditText.requestFocus();
            resultValue = false;
        }
        return resultValue;
    }

    public boolean onDoneEvent(int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE || event != null && (event.getAction() == KeyEvent.ACTION_DOWN
                && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
            if (!isUserDataValid()) {
                return true;
            }

            InputMethodManager manager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            View view = getActivity().getCurrentFocus();
            if (view != null) {
                manager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
            // call child action
            onDoneAction();
            return true;
        }
        return false;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (isFieldsFilled()) {
            mSignupButton.setEnabled(true);
        } else {
            mSignupButton.setEnabled(false);
        }

        mPasswordEditText.setError(null);
        mPasswordConfirmationEditText.setError(null);
        mEmailEditText.setError(null);
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void invalidate() {

    }

    @Override
    public void invalidate(Object... params) {
        final InvalidateParam param = (InvalidateParam) params[0];
        if (param.getMessage() == ControllerMessage.REQUEST_TO_SERVER_REGISTER_USER_COMPLETED) {
            User result = (User) param.getObj();
            if (result != null) {
                LogUtils.LOGD(TAG, String.format("Saving the received user[%s]", result.getEmail()));
                StoryApp.getInstance().setCurrentUser(result);
                // Navigating to the Collections Of Stories activity.
                final Intent collectionsIntent = new Intent(mActivity, StoryMainActivity.class);
                startActivity(collectionsIntent);
                mActivity.finish();
            }
        }
    }
}
