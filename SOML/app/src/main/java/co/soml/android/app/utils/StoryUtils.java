package co.soml.android.app.utils;


import java.util.Comparator;
import java.util.Map;

/**
 *  Copyright (C) 2015 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation
 *  are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied,
 *  reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior
 *  written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with respect to the contents, and
 *  assumes no responsibility for any errors that might appear in the software and documents. This publication and the
 *  contents hereof are subject to change without notice.
 *  @author Larry Pham(Email: larrypham.vn@gmail.com)
 *  @since 6/25/15 3:18 PM
 **/

public class StoryUtils {

    public static Comparator<Object> UserNameComparator = new Comparator<Object>() {
        @Override
        public int compare(Object lhs, Object rhs) {
            Map<String, Object> userMap1 = (Map<String,Object>) lhs;
            Map<String, Object> userMap2 = (Map<String,Object>) rhs;

            String userName1 = getUserNameFromAccountMap(userMap1);
            String userName2 = getUserNameFromAccountMap(userMap2);

            return userName1.compareToIgnoreCase(userName2);
        }
    };

    public static Comparator<Object> UserIdComparator = new Comparator<Object>() {
        @Override
        public int compare(Object lhs, Object rhs) {
            Map<String, Object> userMap1= (Map<String, Object>) lhs;
            Map<String, Object> userMap2 = (Map<String, Object>) rhs;

            long userId1 = getUserIdFromAccountMap(userMap1);
            long userId2 = getUserIdFromAccountMap(userMap2);


            return (userId1 >= userId2) ? 1 : 0;
        }
    };

    public static String getUserNameFromAccountMap(Map<String, Object> account) {
        return StringUtils.notNullStr(MapUtils.getMapStr(account, "FullName"));
    }

    public static String getEmailFromAccountMap(Map<String, Object> account) {
        return StringUtils.notNullStr(MapUtils.getMapStr(account, "Email"));
    }

    public static long getUserIdFromAccountMap(Map<String, Object> account) {
        return MapUtils.getMapLong(account, "UserId");
    }

    public static long getIdFromAccountMap(Map<String, Object> account) {
        return MapUtils.getMapInt(account, "Id");
    }
}
