package co.soml.android.app.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import co.soml.android.app.R;

public class MediaUtils {
	public static final String TAG = LogUtils.makeLogTag(MediaUtils.class.getSimpleName());

	public class RequestCode {
		public static final int ACTIVITY_REQUEST_CODE_PICTURE_LIBRARY = 1000;
		public static final int ACTIVITY_REQUEST_CODE_TAKE_PHOTO = 1100;
	}
	public interface LaunchCameraCallback {
		public void onMediaCapturePathReady(String mediaCapturePath);
	}

	private static void showSDCardRequiredDialog(final Context context) {
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
		dialogBuilder.setTitle(context.getResources().getText(R.string.sdcard_title));
		dialogBuilder.setMessage(context.getResources().getText(R.string.sdcard_mesage));
		dialogBuilder.setPositiveButton(context.getString(R.string.okay_dialog_positive_button), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		dialogBuilder.setCancelable(true);
		dialogBuilder.create().show();
	}

	public static void launchPictureLibrary(final AppCompatActivity activity) {
		Intent intent = new Intent(Intent.ACTION_PICK);
		intent.setType("image/*");
		intent.setAction(Intent.ACTION_GET_CONTENT);
		activity.startActivityForResult(Intent.createChooser(intent, activity.getString(R.string.pick_photo)),
				RequestCode.ACTIVITY_REQUEST_CODE_PICTURE_LIBRARY);
	}

	public static void launchCamera(AppCompatActivity activity, LaunchCameraCallback callback) {
		String state = android.os.Environment.getExternalStorageState();
		if (!state.equalsIgnoreCase(Environment.MEDIA_MOUNTED)) {
			showSDCardRequiredDialog(activity);
		} else {
			Intent intent = prepareLaunchCameraIntent(callback);
			activity.startActivityForResult(intent, RequestCode.ACTIVITY_REQUEST_CODE_TAKE_PHOTO);
		}
	}

	private static Intent prepareLaunchCameraIntent(LaunchCameraCallback callback) {
		File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);

		String mediaCapturePath = path + File.separator + "Camera" + File.separator + "story-" + System.currentTimeMillis() + ".jpg";
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(mediaCapturePath)));

		if (callback != null) {
			callback.onMediaCapturePathReady(mediaCapturePath);
		}

		File directory = new File(mediaCapturePath).getParentFile();
		if (!directory.exists() && !directory.mkdirs()) {
			try {
				throw new IOException("Path to file could not be created");
			} catch (IOException ex) {
				LogUtils.LOGE(TAG, ex.getMessage());
			}
		}
		return intent;
	}

	public static boolean isValidImage(String url) {
		if (url == null) {
			return false;
		}
		return url.endsWith(".png") || url.endsWith(".jpg") || url.endsWith(".jpeg") || url.endsWith(".gif");
	}

	public static boolean isMusic(String url) {
		if (url == null) {
			return false;
		}
		return url.endsWith(".mp3");
	}

	public static String getDate(long ms) {
		Date date = new Date(ms);
		SimpleDateFormat sdf = new SimpleDateFormat("MMM d, yyyy '@' HH:mm", Locale.ENGLISH);
		sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
		return sdf.format(date);
	}

	public static boolean isLocalFile(String state) {
		if (state == null)
			return false;
		return (state.equalsIgnoreCase("queued") || (state.equalsIgnoreCase("uploading")) || state.equalsIgnoreCase("retry")
		 || state.equalsIgnoreCase("failed"));
	}

	public static Uri getLastRecordedAudioUri(final AppCompatActivity activity) {
		String[] proj = { MediaStore.Audio.Media._ID };
		Uri contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;

		String sortOrder = MediaStore.Audio.AudioColumns.DATE_ADDED + "ESC";
		CursorLoader loader = new CursorLoader(activity, contentUri, proj, null, null, sortOrder);
		Cursor cursor = loader.loadInBackground();
		cursor.moveToFirst();

		return Uri.parse(contentUri.toString() + "/" + cursor.getLong(0));
	}

	public static int getMinimumImageWidth(Context context, Uri curStream, String imageWidthStorySettingString) {
		int imageWidthStorySetting = Integer.MAX_VALUE;
		if (!imageWidthStorySettingString.endsWith("Original Size")) {
			try {
				imageWidthStorySetting = Integer.valueOf(imageWidthStorySettingString);
			} catch (NumberFormatException ex) {
				LogUtils.LOGD(TAG, ex.getMessage());
			}
		}

		int[] dimensions = ImageUtils.getImageSize(curStream, context);
		int imageWidthPictureSetting = dimensions[0] == 0 ? Integer.MAX_VALUE : dimensions[0];

		if (Math.min(imageWidthStorySetting, imageWidthPictureSetting) == Integer.MAX_VALUE) {
			return 1024;
		} else {
			return Math.min(imageWidthPictureSetting, imageWidthStorySetting);
		}
	}

	public static boolean isMediaStore(Uri mediaUri) {
		if (mediaUri != null && mediaUri.toString().startsWith("content://media/")) {
			return true;
		} else {
			return false;
		}
	}

	public static Uri downloadExternalMedia(Context context, Uri imageUri) {
		if (context == null || imageUri == null) {
			return null;
		}

		File cachedDir = null;
		String mimeType = context.getContentResolver().getType(imageUri);
		boolean isAudio = (mimeType != null && mimeType.contains("audio"));
		if (android.os.Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
			String mediaFolder = isAudio ? "audio" : "images";
			cachedDir = new File(android.os.Environment.getExternalStorageDirectory() + "/Story/" + mediaFolder);
		} else {
			if (context.getApplicationContext() != null) {
				cachedDir = context.getApplicationContext().getCacheDir();
			}
		}

		if (cachedDir != null && !cachedDir.exists()) {
			cachedDir.mkdirs();
		}

		try {
			InputStream inStream;
			if (imageUri.toString().startsWith("content://")) {
				inStream = context.getContentResolver().openInputStream(imageUri);
				if (inStream == null) {
					LogUtils.LOGE(TAG, "OpenInputStream returned null");
					return null;
				}
			} else {
				inStream = new URL(imageUri.toString()).openStream();
			}

			String fileName = "story-" + System.currentTimeMillis();
			if (isAudio) {
				fileName += "." + MimeTypeMap.getSingleton().getExtensionFromMimeType(mimeType);
			}

			File file = new File(cachedDir, fileName);
			OutputStream outStream = new FileOutputStream(file);

			byte data[] = new byte[1024];
			int count;
			while ((count = inStream.read(data)) != -1) {
				outStream.write(data, 0, count);
			}

			outStream.flush();
			outStream.close();
			inStream.close();

			return Uri.fromFile(file);
		} catch (IOException e) {
			e.printStackTrace();
			LogUtils.LOGE(TAG, e.getMessage());
		}
		return null;
	}

	public static String getMimeTypeOfInputStream(InputStream inStream) {
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(inStream, null, options);
		return options.outMimeType;
	}

	public static String getMediaFileMimeType(File mediaFile) {
		String originalFileName = mediaFile.getName().toLowerCase();
		String mimeType = UrlUtils.getUrlMimeType(originalFileName);

		if (TextUtils.isEmpty(mimeType)) {
			try {
				String filePathForGuessingMime;
				if (mediaFile.getPath().contains("://")) {
					filePathForGuessingMime = Uri.encode(mediaFile.getPath(), ":/");
				} else {
					filePathForGuessingMime = "file://" + Uri.encode(mediaFile.getPath(), "/");
				}

				URL urlForGuessingMime = new URL(filePathForGuessingMime);
				URLConnection connection = urlForGuessingMime.openConnection();
				String guessedContentType = connection.getContentType();
				if (!TextUtils.isEmpty(guessedContentType) && !guessedContentType.equals("content/unknown")) {
					mimeType = guessedContentType;
				}
			} catch (MalformedURLException ex) {
				LogUtils.LOGE(TAG, "MalformedURLException while trying to guess the content type for the file here."
						+ mediaFile.getPath() + " with URLConnection", ex);
			} catch (IOException e) {
				e.printStackTrace();
				LogUtils.LOGE(TAG, "Error while trying to guess the content type for the file here." + mediaFile.getPath() + " with " +
						"URLConnection", e);
			}

			if (TextUtils.isEmpty(mimeType)) {
				try {
					DataInputStream inputStream = new DataInputStream(new FileInputStream(mediaFile));
					String mimeTypeFromStream = getMimeTypeOfInputStream(inputStream);
					if (!TextUtils.isEmpty(mimeTypeFromStream)) {
						mimeType = mimeTypeFromStream;
					}
					inputStream.close();
				} catch (FileNotFoundException ex) {
					LogUtils.LOGE(TAG, "FileNotFoundException while trying to guess the content type for the file " + mediaFile.getPath());
				} catch (IOException e) {
					LogUtils.LOGE(TAG, "IOException while trying to guess the content type for the file " + mediaFile.getPath(), e);
				}
			}

			if (TextUtils.isEmpty(mimeType)) {
				mimeType = "";
			} else {
				if (mimeType.equalsIgnoreCase("audio/mp3")) {
					mimeType = "audio/mp3";
				}
			}
		}
		return mimeType;
	}

	public static String getMediaFileName(File mediaFile, String mimeType) {
		String originalFileName = mediaFile.getName().toLowerCase();
		String extension = MimeTypeMap.getFileExtensionFromUrl(originalFileName);

		if (!TextUtils.isEmpty(extension)) {
			return originalFileName;
		}

		if (!TextUtils.isEmpty(mimeType)) {
			String fileExtension = getExtensionFroMimeType(mimeType);
			if (!TextUtils.isEmpty(fileExtension)) {
				originalFileName += "." + fileExtension;
			}
		} else {
			LogUtils.LOGE(TAG, "No MimeType and No Extension For " + mediaFile.getPath());
		}
		return originalFileName;
	}

	public static String getExtensionFroMimeType(String mimeType) {
		if (TextUtils.isEmpty(mimeType)) {
			return "";
		}

		MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
		String fileExtensionForMimeType = mimeTypeMap.getExtensionFromMimeType(mimeType);
		if (TextUtils.isEmpty(fileExtensionForMimeType)) {
			String[] split = mimeType.split("/");
			fileExtensionForMimeType = split.length > 1 ? split[1] : split[0];
		}
		return fileExtensionForMimeType.toLowerCase();
	}
}
