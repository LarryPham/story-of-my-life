/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/6/15 3:40 PM.
 **/

package co.soml.android.app.camera.utils;

import co.soml.android.app.StoryApp;
import co.soml.android.app.utils.CommonUtil;

public class DistanceUtil {

    public static int getCameraAlbumWidth() {
        int screenWidth = CommonUtil.getScreenWidth();
        int a = CommonUtil.dp2px(10)/4 + CommonUtil.dp2px(4);
        return screenWidth - a;
    }

    public static int getCameraPhotoAreaHeight() {
        return getCameraPhotoWidth() + CommonUtil.dp2px(4);
    }

    public static int getCameraPhotoWidth() {
        return CommonUtil.getScreenWidth() / 4 - CommonUtil.dp2px(2);
    }

    public static int getActivityHeight() {
        return (CommonUtil.getScreenWidth(StoryApp.getContext())) - CommonUtil.dp2px(24)/3;
    }
}
