package co.soml.android.app.utils;

import com.twitter.sdk.android.core.Session;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.20.2015
 */
public class TwitterSessionsUtils {
    public static final String TAG = LogUtils.makeLogTag(TwitterSessionsUtils.class);

    public static Session saveInitializeSessionState(Session twitterSession, Session digitsSession) {
        if (twitterSession != null) {
            saveSessionActive("Sessions: user with active session", twitterSession);
            return twitterSession;
        } else if (digitsSession != null) {
            saveSessionActive("Sessions: user with active digits session", digitsSession);
            return digitsSession;
        } else {
            saveSessionInActive("Sessions: anonymous user");
            return null;
        }
    }

    public static void saveSessionActive(String message, Session session) {
        saveSessionActive(message, String.valueOf(session.getId()));
    }

    public static void saveSessionInActive(String message) {
        saveSessionState(message, null, false);
    }

    public static void saveSessionActive(String message, String userIdentifier) {
        saveSessionState(message, userIdentifier, true);
    }

    public static void saveSessionState(String message, String userIdentifier, boolean active) {
        ;
    }
}
