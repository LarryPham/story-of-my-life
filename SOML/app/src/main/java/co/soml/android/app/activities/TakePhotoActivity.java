/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division.
 *    This software and its documentation are confidential and proprietary information of Sugar Ventures Inc.
 *    No part of the software and documents may be copied, reproduced, transmitted, translated, or reduced to any
 *    electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes
 *    no responsibility for any errors that might appear in the software and documents.
 *    This publication and the contents hereof are subject to change without notice.
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 7/21/15 1:09 AM.
 **/

package co.soml.android.app.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ViewSwitcher;

import java.io.File;

import co.soml.android.app.R;
import co.soml.android.app.utils.AnimUtils;
import co.soml.android.app.utils.CommonUtil;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.widgets.RevealBackgroundView;
import co.soml.android.app.widgets.StoryRecyclerView;

public class TakePhotoActivity extends BaseActivity implements RevealBackgroundView.OnStateChangeListener, View.OnClickListener{
    public static final String TAG = LogUtils.makeLogTag(TakePhotoActivity.class.getSimpleName());

    private static final String ARG_REVEAL_START_LOCATION = "reveal_start_location";
    private static final Interpolator ACCELERATE_INTERPOLATOR = new AccelerateInterpolator();
    private static final Interpolator DECELERATE_INTERPOLATOR =  new DecelerateInterpolator();
    private static int STATE_TAKE_PHOTO = 0;
    private static int STATE_SETUP_PHOTO = 1;

    private int mCurrentState;
    private File mPhotoPath;
    private RevealBackgroundView mRevealBackgroundView;
    private View mTakePhotoRoot;
    private View mShutter;

    private ViewSwitcher mUpperPanel;
    private ViewSwitcher mLowerPanel;

    private StoryRecyclerView mRecyclerView;
    private ImageButton mTakePhotoButton;
    private ImageView mTakePhoto;
    private ImageButton mActionClose;
    private boolean mPendingIntro;

    public static void startCameraFromLocation(int[] startingLocation, Activity startingActivity) {
        final Intent intent = new Intent(startingActivity, TakePhotoActivity.class);
        intent.putExtra(ARG_REVEAL_START_LOCATION, startingLocation);
        startingActivity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mRevealBackgroundView = (RevealBackgroundView) findViewById(R.id.view_reveal_background);
        mTakePhotoRoot = findViewById(R.id.view_photo_root);
        mTakePhoto = (ImageView) findViewById(R.id.view_take_photo);
        mTakePhotoButton = (ImageButton) findViewById(R.id.view_take_photo);

        mUpperPanel = (ViewSwitcher) findViewById(R.id.view_upper_panel);
        mLowerPanel = (ViewSwitcher) findViewById(R.id.view_lower_panel);
        mRecyclerView = (StoryRecyclerView) findViewById(R.id.recyclerview_filters);

        updateStatusBarColor();
        updateState(STATE_TAKE_PHOTO);
        setupRevealBackground(savedInstanceState);
        setupPhotoFilters();

        mUpperPanel.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                mUpperPanel.getViewTreeObserver().removeOnPreDrawListener(this);
                mPendingIntro = true;
                mUpperPanel.setTranslationX(-mUpperPanel.getHeight());
                mLowerPanel.setTranslationY(mLowerPanel.getHeight());
                return true;
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void updateStatusBarColor() {
        if (CommonUtil.hasLollipop()) {
            getWindow().setStatusBarColor(0xff111111);
        }
    }

    private void setupRevealBackground(Bundle savedInstanceState) {
        mRevealBackgroundView.setFillPaintColor(0xFF16181a);
        mRevealBackgroundView.setOnStateChangedListener(this);
        if (savedInstanceState == null) {
            final int[] startingLocation = getIntent().getIntArrayExtra(ARG_REVEAL_START_LOCATION);
            mRevealBackgroundView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    mRevealBackgroundView.getViewTreeObserver().removeOnPreDrawListener(this);
                    mRevealBackgroundView.startFromLocation(startingLocation);
                    return true;
                }
            });
        } else {
          mRevealBackgroundView.setToFinishedTime();
        }
    }

    private void setupPhotoFilters() {

    }


    @Override
    protected void invalidate() {

    }

    @Override
    protected void invalidate(Object... params) {

    }

    public void showTakenPicture(Bitmap bitmap) {
        mUpperPanel.showNext();
        mLowerPanel.showNext();
        mTakePhoto.setImageBitmap(bitmap);
        updateState(STATE_SETUP_PHOTO);
    }
    @Override
    public void onBackPressed() {
        if (mCurrentState == STATE_SETUP_PHOTO) {
            mTakePhotoButton.setEnabled(true);
            mUpperPanel.showNext();
            mLowerPanel.showNext();
            updateState(STATE_TAKE_PHOTO);
        } else {
            super.onBackPressed();
        }
    }

    private void updateState(int state) {
        mCurrentState = state;
        if (mCurrentState == STATE_TAKE_PHOTO) {
            mUpperPanel.setInAnimation(this, R.anim.slide_in_from_right);
            mLowerPanel.setInAnimation(this, R.anim.slide_in_from_right);

            mUpperPanel.setOutAnimation(this, R.anim.slide_in_from_left);
            mLowerPanel.setOutAnimation(this, R.anim.slide_in_from_left);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mTakePhotoButton.setVisibility(View.GONE);
                }
            }, 400);
        } else if (mCurrentState == STATE_SETUP_PHOTO) {
            mUpperPanel.setInAnimation(this, R.anim.slide_in_from_left);
            mLowerPanel.setInAnimation(this, R.anim.slide_in_from_left);

            mUpperPanel.setOutAnimation(this, R.anim.slide_in_from_right);
            mLowerPanel.setOutAnimation(this, R.anim.slide_in_from_right);
            mTakePhotoButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onStateChanged(int state) {
        if (RevealBackgroundView.STATE_FINISHED == state) {
            mTakePhotoRoot.setVisibility(View.VISIBLE);
            if (mPendingIntro) {
                startIntroAnimation();
            } else {
                mTakePhotoRoot.setVisibility(View.INVISIBLE);
            }
        }
    }

    private void startIntroAnimation() {
        mUpperPanel.animate().translationY(0).setDuration(400).setInterpolator(DECELERATE_INTERPOLATOR);
        mLowerPanel.animate().translationY(0).setDuration(400).setInterpolator(DECELERATE_INTERPOLATOR).start();
    }

    @Override
    public void onClick(View view) {
        final int viewId = view.getId();
        switch (viewId) {
            case R.id.button_take_photo: {
                mTakePhotoButton.setEnabled(false);
                animateShutter();
                break;
            }
            case R.id.action_accept: {
                //
                break;
            }
            case R.id.action_close: {
                this.finish();
                break;
            }
            case R.id.action_camera_rear:{
                break;
            }
        }
    }

    private void animateShutter() {
        mShutter.setVisibility(View.VISIBLE);
        mShutter.setAlpha(0.1f);

        ObjectAnimator alphaAnim = ObjectAnimator.ofFloat(mShutter, "alpha", 0f, 0.8f);
        alphaAnim.setDuration(100);
        alphaAnim.setStartDelay(100);
        alphaAnim.setInterpolator(ACCELERATE_INTERPOLATOR);

        ObjectAnimator alphaOutAnim = ObjectAnimator.ofFloat(mShutter, "alpha", 0.8f, 0f);
        alphaOutAnim.setDuration(200);
        alphaOutAnim.setInterpolator(DECELERATE_INTERPOLATOR);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playSequentially(alphaAnim, alphaOutAnim);
        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mShutter.setVisibility(View.GONE);
            }
        });
        animatorSet.start();
    }

}
