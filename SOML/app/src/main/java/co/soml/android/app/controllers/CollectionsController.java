package co.soml.android.app.controllers;

import android.content.Intent;
import android.os.Message;

import java.util.List;

import co.soml.android.app.StoryApp;
import co.soml.android.app.activities.BaseActivity;
import co.soml.android.app.activities.InvalidateParam;
import co.soml.android.app.activities.StoryMainActivity;
import co.soml.android.app.activities.adapters.StoryPostsAdapter;
import co.soml.android.app.fragments.BaseFragment;
import co.soml.android.app.models.Story;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.models.StoryFeedList;
import co.soml.android.app.services.StoriesResult;
import co.soml.android.app.services.VoteResult;
import co.soml.android.app.services.tasks.Action;
import co.soml.android.app.utils.LogUtils;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since May.21.2015
 */
public class CollectionsController extends BaseController {
    protected BaseActivity mActivity;
    protected StoryDataModel mDataModel;
    protected StoryApp mStoryApp;

    public CollectionsController(BaseActivity activity, StoryDataModel dataModel) {
        super(activity, dataModel);
        this.mActivity = activity;
        this.mStoryApp = (StoryApp) activity.getApplicationContext();
        this.mDataModel = dataModel;
    }

    public CollectionsController(BaseActivity activity, StoryDataModel dataModel, boolean isFirstRunningMode) {
        super(activity, dataModel, isFirstRunningMode);
        this.mActivity = (StoryMainActivity) activity;
        this.mStoryApp = (StoryApp) activity.getApplicationContext();
        this.mDataModel = dataModel;
        if (isFirstRunningMode) {
            mStoryApp.removeAllHandler();
        }
    }

    public CollectionsController(BaseActivity activity, BaseFragment fragment, StoryDataModel dataModel) {
        super(activity, fragment, dataModel);
        this.mActivity =  activity;
        this.mStoryApp = (StoryApp) activity.getApplicationContext();
        this.mDataModel = dataModel;
        this.mFragment = fragment;
    }

    @Override
    protected void handleMessage(Message msg) {
        switch (msg.what) {
            // Sending message to the service component for parsing the list of stories
            case ControllerMessage.REQUEST_TO_SERVER_STORIES_LATEST: {
                final Intent intent = obtainIntent(ControllerMessage.REQUEST_TO_SERVER_STORIES_LATEST);
                final RequestStoriesParam param = (RequestStoriesParam) msg.obj;
                // Sending params which has been added into header of request
                intent.putExtra(Action.X_API_VERSION, param.mApiVersion);
                intent.putExtra(Action.X_AUTH_TOKEN, param.mAuthToken);
                intent.putExtra(Action.STORY_SCOPE, param.mScopeName);
                // Paginating params
                intent.putExtra(Action.LIMIT, param.mLimit);
                intent.putExtra(Action.START_INDEX, param.mStartIndex);
                // Setting action for requesting the list of stories by specified scope
                intent.setAction(Action.REQUEST_TO_SERVER_STORY_LATEST);
                mActivity.startService(intent);
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_STORIES_FEATURED: {
                final Intent intent = obtainIntent(ControllerMessage.REQUEST_TO_SERVER_STORIES_FEATURED);
                final RequestStoriesParam param = (RequestStoriesParam) msg.obj;
                // Sending params which has been added into header of request
                intent.putExtra(Action.X_API_VERSION, param.mApiVersion);
                intent.putExtra(Action.X_AUTH_TOKEN, param.mAuthToken);
                intent.putExtra(Action.STORY_SCOPE, param.mScopeName);
                // Paginating params
                intent.putExtra(Action.LIMIT, param.mLimit);
                intent.putExtra(Action.START_INDEX, param.mStartIndex);
                // Setting action for requesting the list of stories by specified scope
                intent.setAction(Action.REQUEST_TO_SERVER_STORY_FEATURED);
                mActivity.startService(intent);
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_STORIES_FOLLOWING: {
                final Intent intent = obtainIntent(ControllerMessage.REQUEST_TO_SERVER_STORIES_FOLLOWING);
                final RequestStoriesParam param = (RequestStoriesParam) msg.obj;
                // Sending params which has been added into header of request
                intent.putExtra(Action.X_API_VERSION, param.mApiVersion);
                intent.putExtra(Action.X_AUTH_TOKEN, param.mAuthToken);
                intent.putExtra(Action.STORY_SCOPE, param.mScopeName);
                // Paginating params
                intent.putExtra(Action.LIMIT, param.mLimit);
                intent.putExtra(Action.START_INDEX, param.mStartIndex);
                // Setting action for requesting the list of stories by specified scope
                intent.setAction(Action.REQUEST_TO_SERVER_STORY_FOLLOWING);
                mActivity.startService(intent);
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_STORY_LIKE: {
                final Intent intent = obtainIntent(ControllerMessage.REQUEST_TO_SERVER_STORY_LIKE);
                final StoryPostsAdapter.RequestParam param = (StoryPostsAdapter.RequestParam) msg.obj;
                intent.putExtra(Action.STORY_ID, param.mId);
                intent.setAction(Action.REQUEST_TO_SERVER_STORY_LIKE);
                mActivity.startService(intent);
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_STORY_UNLIKE: {
                final Intent intent = obtainIntent(ControllerMessage.REQUEST_TO_SERVER_STORY_UNLIKE);
                final StoryPostsAdapter.RequestParam param= (StoryPostsAdapter.RequestParam) msg.obj;
                intent.putExtra(Action.STORY_ID, param.mId);
                intent.setAction(Action.REQUEST_TO_SERVER_STORY_UNLIKE);
                mActivity.startService(intent);
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_STORY_LIKE_COMPLETED: {
                if (checkOwner(msg)) {
                    final VoteResult result = (VoteResult) msg.obj;
                    final String vote = result.getVote();
                    final InvalidateParam invalidateParam = new InvalidateParam(msg.what, result);
                    mFragment.invalidate(invalidateParam);
                }
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_STORY_UNLIKE_COMPLETED: {
                if (checkOwner(msg)) {
                    final VoteResult result = (VoteResult) msg.obj;
                    final String vote = result.getVote();
                    final InvalidateParam invalidateParam = new InvalidateParam(msg.what, result);
                    mFragment.invalidate(invalidateParam);
                }
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_COMMENT_STORY: {
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_COMMENT_UNLIKE: {

            }
            // Handling the succeeded signals for request the list of stories completed from server
            case ControllerMessage.REQUEST_TO_SERVER_STORIES_FEATURED_COMPLETED: {
                if (checkOwner(msg)) {
                    final StoriesResult result = (StoriesResult) msg.obj;
                    final List<Story> storyList = result.getStoryList();
                    final StoryFeedList targetedStories = mDataModel.getFeaturedStoryList();
                    if (targetedStories != null) {
                        targetedStories.clear();
                        // Updating the new items for current list of story into the data model
                        if (storyList != null && storyList.size() > 0) {
                            for (int index = 0; index < storyList.size(); index++) {
                                targetedStories.add(new Story(storyList.get(index)));
                                LogUtils.LOGI(TAG, String.format("[GUN]Story: [%08d:%s]", storyList.get(index).getId(),
                                        storyList.get(index).getName()));
                            }
                        }
                        // invalidating the view(activity) again for adapter which will be notified data-set changed...
                        final InvalidateParam param = new InvalidateParam(msg.what, targetedStories);
                        mFragment.invalidate(param);
                    }
                }
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_STORIES_FOLLOWING_COMPLETED: {
                if (checkOwner(msg)) {
                    final StoriesResult result = (StoriesResult) msg.obj;
                    final List<Story> jsonStoryList = result.getStoryList();
                    final StoryFeedList targetedStories = mDataModel.getFollowingStoryList();
                    if (targetedStories != null) {
                        targetedStories.clear();
                        // Updating the new items for current list of story into the data model
                        if (jsonStoryList != null && jsonStoryList.size() > 0) {
                            for (int index = 0; index < jsonStoryList.size(); index++) {
                                targetedStories.add(new Story(jsonStoryList.get(index))); // Adding each item of stories into this list
                                // Must to observing all of item into this list for reviewing the id and title
                                LogUtils.LOGI(TAG, String.format("[GUN]Story: [%08d:%s]", jsonStoryList.get(index).getId(),
                                        jsonStoryList.get(index).getName()));
                            }
                        }
                        // Invalidating the view component (activity) again for adapter which will be data-set changed..
                        final InvalidateParam param = new InvalidateParam(msg.what, msg.obj);
                        mFragment.invalidate(param);
                    }
                }
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_STORIES_LATEST_COMPLETED: {
                if (checkOwner(msg)) {
                    final StoriesResult result = (StoriesResult) msg.obj;
                    final List<Story> jsonStoryList = result.getStoryList();
                    final StoryFeedList targetedStories = mDataModel.getLatestStoryList();
                    if (targetedStories != null) {
                        targetedStories.clear();
                        // Updating the new items for current list of story into the data model
                        if (jsonStoryList != null && jsonStoryList.size() > 0) {
                            for (int index = 0; index < jsonStoryList.size(); index++) {
                                targetedStories.add(new Story(jsonStoryList.get(index))); // Adding each item of stories into this list
                                // Must to observing all of item into this list for reviewing the id and title
                                LogUtils.LOGI(TAG, String.format("[GUN]Story: [%08d:%s]", jsonStoryList.get(index).getId(),
                                        jsonStoryList.get(index).getName()));
                            }
                        }
                        // Invalidating the view component (activity) again for adapter which will be data-set changed..
                        final InvalidateParam param = new InvalidateParam(msg.what, msg.obj);
                        mFragment.invalidate(param);
                    }
                }
                break;
            }
        }
    }

    public static class RequestStoriesParam {
        public int mApiVersion;
        public String mAuthToken;

        public long mUserId;
        public String mScopeName;
        public int mStartIndex;
        public int mLimit;
        public String mState;

        public RequestStoriesParam(long userId, String scopeName, int startIndex, int limit, int apiVersion, String authToken) {
            this.mUserId = userId;
            this.mScopeName = scopeName;
            this.mStartIndex = startIndex;
            this.mLimit = limit;
            this.mApiVersion = apiVersion;
            this.mAuthToken = authToken;
        }

        public RequestStoriesParam(String scopeName, int startIndex, int limit, int apiVersion, String authToken) {
            this.mAuthToken = authToken;
            this.mApiVersion = apiVersion;
            this.mScopeName = scopeName;
            this.mStartIndex = startIndex;
            this.mLimit = limit;
        }
    }

    public static class SubscribeStoryParam {
        public int mStoryId;
        public String mReason;
        public String mMessage;

        public SubscribeStoryParam(int storyId) {
            this.mStoryId = storyId;
        }

        public SubscribeStoryParam(int storyId, String reason, String message) {
            this.mStoryId = storyId;
            this.mReason = reason;
            this.mMessage = message;
        }
    }
}
