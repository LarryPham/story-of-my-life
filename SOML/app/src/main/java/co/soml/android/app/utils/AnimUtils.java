package co.soml.android.app.utils;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.OvershootInterpolator;

import co.soml.android.app.R;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its
 * documentation are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable
 * form without the prior written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with
 * respect to the contents, and assumes no responsibility for any errors that might appear in the software and
 * documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.04.2015
 */
public class AnimUtils {
    private AnimUtils() {
        throw new AssertionError();
    }

    public static void fadeIn(View targetView) {
        startAnimation(targetView, R.anim.fade_in, null);
        if (targetView.getVisibility() != View.VISIBLE) {
            targetView.setVisibility(View.VISIBLE);
        }
    }

    public static void fadeOut(View targetView) {
        startAnimation(targetView, R.anim.fade_out, null);
        if (targetView.getVisibility() != View.GONE) {
            targetView.setVisibility(View.GONE);
        }
    }

    public static void flyIn(final View targetView) {
        Context context = targetView.getContext();
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.story_flyin);
        if (animation == null) {
            return;
        }

        animation.setInterpolator(new OvershootInterpolator(0.9f));
        long duration = context.getResources().getInteger(android.R.integer.config_mediumAnimTime);
        animation.setDuration((long) (duration * 1.5f));

        targetView.startAnimation(animation);
        targetView.setVisibility(View.VISIBLE);
    }

    public static void flyOut(final View targetView) {
        Animation.AnimationListener listener = new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                targetView.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        };
        startAnimation(targetView, R.anim.story_flyout, listener);
    }

    public static void startAnimation(View targetView, int animResId) {
        startAnimation(targetView, animResId, null);
    }

    public static void startAnimation(View targetView, int animResId, int duration) {
        startAnimation(targetView, animResId, duration, null);
    }

    public static void startAnimation(View targetView, int animResId, Animation.AnimationListener listener) {
        if (targetView == null) {
            return;
        }

        final Animation animation = AnimationUtils.loadAnimation(targetView.getContext(), animResId);
        if (animation == null) {
            return;
        }
        if (listener != null) {
            animation.setAnimationListener(listener);
        }

        targetView.startAnimation(animation);
    }

    public static void startAnimation(View targetView, int animResId, int duration,
                                      Animation.AnimationListener listener) {
        if (targetView == null) {
            return;
        }

        final Animation animation = AnimationUtils.loadAnimation(targetView.getContext(), animResId);

        if (animation == null) {
            return;
        }

        if (listener != null) {
            animation.setAnimationListener(listener);
        }

        animation.setDuration(duration);
        targetView.startAnimation(animation);
    }
}
