/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 * @author: Larry Pham (email: larrypham.vn@gmail.com)
 * Date: 6/10/15 2:05 PM
 **/

package co.soml.android.app.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

import co.soml.android.app.utils.LogUtils;

public class StoryLocation implements Serializable, Parcelable {
    public static final String TAG = LogUtils.makeLogTag(StoryLocation.class.getSimpleName());

    public final static double INVALID_LATITUDE = 9999;
    public final static double INVALID_LONGITUDE = 9999;

    public final static double MIN_LATITUDE = -90;
    public final static double MIN_LONGITUDE = -180;

    public static final double MAX_LATITUDE = 90;
    public static final double MAX_LONGITUDE = 180;

    private double mLatitude = INVALID_LATITUDE;
    private double mLongitude = INVALID_LONGITUDE;

    public double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(double latitude) {
        if (!isValidLatitude(latitude)) {
            throw new IllegalArgumentException("Invalid latitude; must be between the range " + MIN_LATITUDE + " and "
                    + MAX_LATITUDE);
        }
        mLatitude = latitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(double longitude) {
        if (!isValidLongitude(longitude)) {
            throw new IllegalArgumentException("Invalid longitude; must be between the range " + MIN_LONGITUDE + " and "
                    + MAX_LONGITUDE);
        }
        mLongitude = longitude;
    }

    public boolean isValid() {
        return isValidLatitude(mLatitude) && isValidLongitude(mLongitude);
    }

    public boolean isValidLatitude(double latitude) {
        return latitude >= MIN_LATITUDE && latitude <= MAX_LATITUDE;
    }

    public boolean isValidLongitude(double longitude) {
        return longitude >= MIN_LONGITUDE && longitude <= MAX_LONGITUDE;
    }

    public StoryLocation() {

    }

    public StoryLocation(double latitude, double longitude) {
        setLatitude(latitude);
        setLongitude(longitude);
    }

    public StoryLocation(Parcel source) {
        readFromParcel(source);
    }

    public int hashCode() {
        final int prime = 31;
        int hashCode = 1;
        hashCode = prime * hashCode + (Double.valueOf(mLatitude).hashCode());
        hashCode = prime * hashCode + (Double.valueOf(mLongitude).hashCode());
        return hashCode;
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }

        if (other instanceof StoryLocation) {
            StoryLocation otherLocation = (StoryLocation) other;
            return this.mLatitude == otherLocation.mLatitude && this.mLongitude == otherLocation.mLongitude;
        }
        return false;
    }

    public static boolean equals(Object firstLocation, Object secondLocation) {
        if (firstLocation == secondLocation) {
            return true;
        }
        if (firstLocation == null || secondLocation == null) {
            return false;
        }
        return firstLocation.equals(secondLocation);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(mLatitude);
        dest.writeDouble(mLongitude);
    }

    public void readFromParcel(Parcel source) {
        mLatitude = source.readDouble();
        mLongitude = source.readDouble();
    }

    public static Creator<StoryLocation> CREATOR = new Creator<StoryLocation>() {
        @Override
        public StoryLocation createFromParcel(Parcel source) {
            return new StoryLocation(source);
        }

        @Override
        public StoryLocation[] newArray(int size) {
            return new StoryLocation[size];
        }
    };
}
