/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 7/25/15 10:44 PM.
 **/

package co.soml.android.app.services.tasks;

/**
 * An instance of the <code>AsyncTask</code> class for loading the user by searching specified keywords. When user input the keyword for
 * searching available users into server-side backend. It will load the list of users has been contained keywords like that.
 */
public class SearchUsersTask {
}
