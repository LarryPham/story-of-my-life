/**
 * Copyright (C)  2015 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc.
 * Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 * @author: Larry Pham (email: larrypham.vn@gmail.com)
 * @date: 7/15/15 8:47 AM
 **/

package co.soml.android.app.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Stack;

import co.soml.android.app.R;
import co.soml.android.app.StoryApp;
import co.soml.android.app.activities.AppInterfaces;
import co.soml.android.app.activities.InvalidateParam;
import co.soml.android.app.activities.StoryMainActivity;
import co.soml.android.app.activities.UserProfileActivity;
import co.soml.android.app.controllers.ControllerMessage;
import co.soml.android.app.controllers.UserProfileController;
import co.soml.android.app.fragments.adapters.PeopleAdapter;
import co.soml.android.app.models.Story;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.models.StoryUserList;
import co.soml.android.app.models.User;
import co.soml.android.app.services.StatusResult;
import co.soml.android.app.services.UserListResult;
import co.soml.android.app.services.tasks.Action;
import co.soml.android.app.utils.AccountUtils;
import co.soml.android.app.utils.CommonUtil;
import co.soml.android.app.utils.Lists;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.widgets.ProgressWheel;
import co.soml.android.app.widgets.StoryRecyclerView;
import co.soml.android.app.widgets.StoryTextView;

public class UsersListFragment extends BaseFragment {

    public static final String TAG = LogUtils.makeLogTag(UsersListFragment.class.getSimpleName());
    public static final String USER_LIST = "Users";
    public static final String USER_LIST_TYPE = "ListType";
    public static final int X_API_VERSION = 1;

    public StoryDataModel mDataModel;
    public int mUserId;
    public StoryUserList mUsers = new StoryUserList();
    public String mListType;

    public UserProfileActivity mUserProfileActivity;
    public StoryApp mApp;
    public UserProfileController mController;

    public View mRootView;
    private View mEmptyView;
    private View mLoadingView;

    private ProgressWheel mProgressMore;
    private StoryTextView mEmptyTitleView;
    private StoryTextView mEmptyDescriptionView;
    private StoryRecyclerView mUserRecyclerView;

    private float mDipScale;
    private int mDensity;
    private PeopleAdapter mAdapter;
    public Bundle mArguments;

    public AppInterfaces.UnfollowUserListener mUnFollowUserListener = new AppInterfaces.UnfollowUserListener() {
        @Override
        public void onUnfollowUser(View view) {
            final String fn = "[UNFOLLOW_USER]";
            final int userId = (Integer) view.getTag();
            final int currentUserId = mApp.getCurrentUser().getId();
            if (mController != null && userId != currentUserId) {
                final int apiVersion = 1;
                final String authToken = AccountUtils.getAuthToken(getActivity());
                LogUtils.LOGD(TAG, fn + String.format(" unfollowing the user[%d]", userId));
                final UserProfileController.RequestParams params = new UserProfileController.RequestParams(userId, apiVersion, authToken);
                mController.sendMessage(ControllerMessage.REQUEST_TO_SERVER_USER_UNFOLLOW, params);
            }
        }
    };
    public AppInterfaces.FollowUserListener mFollowUserListener = new AppInterfaces.FollowUserListener() {
        @Override
        public void onFollowUser(View view) {
            final String fn = "[FOLLOW_USER]";
            final int userId = (Integer) view.getTag();
            final int currentUserId = mApp.getCurrentUser().getId();
            if (mController != null && userId != currentUserId) {
                final int apiVersion = 1;
                final String authToken = AccountUtils.getAuthToken(getActivity());
                LogUtils.LOGD(TAG, fn + String.format(" following the user[%d]", userId));
                final UserProfileController.RequestParams params = new UserProfileController.RequestParams(userId, apiVersion, authToken);
                mController.sendMessage(ControllerMessage.REQUEST_TO_SERVER_USER_FOLLOW, params);
            }
        }
    };
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mActivity = (UserProfileActivity) getActivity();
        this.mApp = (StoryApp) mActivity.getApplicationContext();
        this.mDataModel = (StoryDataModel) mApp.getAppDataModel();
        mDipScale = getResources().getDisplayMetrics().density;
        mDensity = CommonUtil.getDensity(mActivity);

        mArguments = getArguments();
        if (mArguments.get(USER_LIST) != null) {
            mUsers = (StoryUserList) mArguments.get(USER_LIST);
        }
        mUserId = mArguments.getInt("USER_ID");
        mListType = (String) mArguments.get(USER_LIST_TYPE);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LogUtils.LOGD(TAG, "onCreateView()");
        mRootView = inflater.inflate(R.layout.layout_profile_subview, container, false);
        return mRootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mEmptyView = mRootView.findViewById(R.id.empty_view);

        mUserRecyclerView = (StoryRecyclerView) mRootView.findViewById(R.id.user_profile_recyclerview);
        mLoadingView = mRootView.findViewById(R.id.loading_more_view);
        mProgressMore = (ProgressWheel) mRootView.findViewById(R.id.loading_progress_view);

        mEmptyTitleView = (StoryTextView) mEmptyView.findViewById(R.id.empty_view_title);
        mEmptyDescriptionView = (StoryTextView) mEmptyView.findViewById(R.id.empty_view_description);
        bindUsersAdapter(mUserRecyclerView);

    }

    @Override
    public void onResume() {
        LogUtils.LOGI(TAG, "onResume()");
        super.onResume();

        if (mController == null) {
            mController = new UserProfileController(mActivity, this, mDataModel);
        }

        final String authToken = AccountUtils.getAuthToken(mActivity);
        final UserProfileController.RequestParams requestParams = new UserProfileController.RequestParams(mUserId, X_API_VERSION, authToken);

        if (mUsers != null) {
            if (mUsers.size() > 0) {
                mEmptyView.setVisibility(View.GONE);
            } else {
                if (mListType.equalsIgnoreCase("FOLLOWING_USERS")) {
                    LogUtils.LOGD(TAG, String.format("Request to server for loading the following users of uesr[%d]", mUserId));
                    mController.sendMessage(ControllerMessage.REQUEST_TO_SERVER_USER_FOLLOWING, requestParams);
                } else if (mListType.equalsIgnoreCase("FOLLOWERS")) {
                    LogUtils.LOGD(TAG, String.format("Request to server for loading the followers of user[%d]", mUserId));
                    mController.sendMessage(ControllerMessage.REQUEST_TO_SERVER_USER_FOLLOWERS, requestParams);
                }
            }
        }
        footerVisibility();
    }

    protected void bindUsersAdapter(StoryRecyclerView recyclerView) {
        if (mListType != null) {
            if (mListType.equalsIgnoreCase("FOLLOWING_USERS")) {
                mAdapter = new PeopleAdapter(getActivity(), StoryDataModel.UsersType.FOLLOWING, mUsers);
            } else if (mListType.equalsIgnoreCase("FOLLOWERS")) {
                mAdapter = new PeopleAdapter(getActivity(), StoryDataModel.UsersType.FOLLOWERS, mUsers);
            }
        }
        mAdapter.setFollowUserListener(mFollowUserListener);
        mAdapter.setUnfollowUserListener(mUnFollowUserListener);
        recyclerView.setAdapter(mAdapter);

    }

    protected void footerVisibility() {

    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            LogUtils.LOGD(TAG, String.format("OnReceive: Action[%s]",action));
            if (action.equalsIgnoreCase(Action.SHOW_PROGRESS)) {
                //showProgressDialog();
            } else if (action.equalsIgnoreCase(Action.DISMISS)) {
                // dismisProgressDialog();
            }
        }
    };

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LogUtils.LOGI(TAG, "onDestroyView()");

        StoryRecyclerView recyclerView = (StoryRecyclerView) mRootView.findViewById(R.id.user_profile_recyclerview);
        if (recyclerView != null) {
            PeopleAdapter adapter = (PeopleAdapter) recyclerView.getAdapter();
        }
        // Recycling the root-view and detaching the RootView from parent view.
        if (mRootView != null && (ViewGroup) mRootView.getParent() != null) {
            ((ViewGroup) mRootView.getParent()).removeView(mRootView);
            mRootView = null;
        }
    }

    @Override
    public void invalidate() {

    }

    public static class HistoryStack extends Stack<String> {
        private final String mKeyName;

        public HistoryStack(String keyName) {
            this.mKeyName = keyName;
        }

        public void restoreName(Bundle bundle) {
            clear();
            if (bundle.containsKey(mKeyName)) {
                ArrayList<String> history = bundle.getStringArrayList(mKeyName);
                if (history != null) {
                    this.addAll(history);
                }
            }
        }

        public void saveInstance(Bundle bundle) {
            if (!isEmpty()) {
                ArrayList<String> history = Lists.newArrayList();
                history.addAll(this);
                bundle.putStringArrayList(mKeyName, history);
            }
        }
    }

    @Override
    public void invalidate(Object... params) {
        if (params[0] instanceof InvalidateParam) {
            final InvalidateParam param = (InvalidateParam) params[0];
            LogUtils.LOGD(TAG, "Invalidating the views from UserProfile ListView");
            switch (param.getMessage()) {
                case ControllerMessage.REQUEST_TO_SERVER_USER_FOLLOWERS_COMPLETED: {
                    final StoryUserList followers = (StoryUserList) param.getObj();
                    if (followers != null && followers.size() > 0) {
                        mEmptyView.setVisibility(View.GONE);
                        mAdapter.setUsers(followers);
                        mAdapter.notifyDataSetChanged();
                        mUserRecyclerView.getAdapter().notifyDataSetChanged();
                    } else {
                        mEmptyView.setVisibility(View.VISIBLE);
                    }
                    break;
                }
                case ControllerMessage.REQUEST_TO_SERVER_USER_FOLLOWING_COMPLETED: {
                    final StoryUserList followingUsers = (StoryUserList) param.getObj();
                    if (followingUsers != null && followingUsers.size() > 0) {
                        mEmptyView.setVisibility(View.GONE);
                        mAdapter.setUsers(followingUsers);
                        mAdapter.notifyDataSetChanged();
                        mUserRecyclerView.getAdapter().notifyDataSetChanged();
                    } else {
                        mEmptyView.setVisibility(View.VISIBLE);
                    }
                    break;
                }
                case ControllerMessage.REQUEST_TO_SERVER_USER_FOLLOW_COMPLETED: {
                    LogUtils.LOGD(TAG, "Invalidating the view when request to server for getting the UserInfo from server");
                    final StatusResult result = (StatusResult) param.getObj();
                    final int userId = result.getUserId();
                    final User followUser = mAdapter.getUsers().findUserById(userId);

                    int position = -1;
                    for (int index = 0; index < mAdapter.getUsers().size(); index ++) {
                        if (mAdapter.getUsers().get(index).getId() == userId) {
                            position = index;
                        }
                    }
                    if (position > -1) {
                        followUser.setFollowedByCurrentUser(true);
                        mUserRecyclerView.getAdapter().notifyItemChanged(position, followUser);
                    }
                    break;
                }
                case ControllerMessage.REQUEST_TO_SERVER_USER_UNFOLLOW_COMPLETED: {
                    LogUtils.LOGD(TAG, "Invalidating the view when request to server for getting the UserInfo from server");
                    final StatusResult result = (StatusResult) param.getObj();
                    final int userId = result.getUserId();
                    final User unfollowUser = mAdapter.getUsers().findUserById(userId);

                    int position = -1;
                    for (int index = 0; index < mAdapter.getUsers().size(); index ++) {
                        if (mAdapter.getUsers().get(index).getId() == userId) {
                            position = index;
                        }
                    }
                    if (position > -1) {
                        unfollowUser.setFollowedByCurrentUser(false);
                        mUserRecyclerView.getAdapter().notifyItemChanged(position, unfollowUser);
                    }
                    break;
                }
            }
        }
    }
}
