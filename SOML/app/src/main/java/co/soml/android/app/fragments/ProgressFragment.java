/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 7/23/15 3:33 PM.
 **/

package co.soml.android.app.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;

import co.soml.android.app.R;
import co.soml.android.app.controllers.BaseController;
import co.soml.android.app.controllers.ControllerMessage;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.widgets.StoryTextView;

/**
 * Implementation of the fragment to display content. Based on {@link android.support.v4.app.Fragment}. If you are waiting for the
 * initial data, you'll can displaying during this time an indeterminate progress indicator.
 */
public class ProgressFragment extends Fragment {
    public static final String TAG = LogUtils.makeLogTag(ProgressFragment.class.getSimpleName());

    private View mProgressContainer;
    private View mContentContainer;
    private View mContentView;
    private View mEmptyView;

    private boolean mContentShown;
    private boolean mIsContentEmpty;

    private int mActionCount = 0;
    private BaseController mOwnerController = null;

    public ProgressFragment() {

    }

    /**
     * Provide default implementation to return a simple view. Subclasses can override to replace with their own layout. If doing so, the
     * returned view hierarchy <em>must</em> have a progress container whose id is {@link co.soml.android.app.R.id#progress_container,
     *  R.id.progress_container}, content container whose id is {@link co.soml.android.app.R.id#content_container R.id.content_container}
     *  and can optionally have a sibling view id {@link android.R.id#empty android.R.id.empty} that is to be shown when the content is
     *  empty.
     *
     * <p>If you are overriding this method with your own custom content, consider including the standard layout
     * {@link co.soml.android.app.R.layout#layout_progress_fragment} in your layout file, so that you continue to retain all of the
     * standard behavior of ProgressFragment. In particular, this is currently the only way to have the built-in indeterminant progress
     * state be shown.
     * </p>
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_progress_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ensureContent();
    }

    @Override
    public void onDestroyView() {
        mContentShown = false;
        mIsContentEmpty = false;
        mProgressContainer = mContentContainer = mContentView = mEmptyView = null;
        super.onDestroyView();
    }

    public int getActionCount() {
        return mActionCount;
    }

    public void cancel() {
        LogUtils.LOGD(TAG, String.format("[GUN] Cancel ActionCount[%d] reset[%s]", mActionCount, mOwnerController));
        if (getActionCount() > 0) {
            if (mOwnerController != null) {
                mOwnerController.sendMessage(ControllerMessage.CANCEL_WAITING_DIALOG, null);
            }
        }
        mActionCount = 0;
    }

    public synchronized void incrementActionCount() {
        this.mActionCount++;
    }

    public synchronized void decrementActionCount() {
        this.mActionCount--;
    }

    /**
     * Return content view or null if the content view has not been initialized
     * @return mContentView or null
     * @see #setContentView(android.view.View)
     * @see #setContentView(int)
     */
    public View getContentView() {
        return this.mContentView;
    }

    /**
     * Sets the content view from a layout resource.
     *
     * @param layoutResId Resource ID to be inflated.
     * @see #setContentView(android.view.View)
     * @see #getContentView()
     */
    public void setContentView(int layoutResId) {
        final LayoutInflater inflater = LayoutInflater.from(getActivity());
        final View contentView = inflater.inflate(layoutResId, null);
        setContentView(contentView);
    }

    /**
     * Sets the content view to an explicit view. If the content view was installed earlier, the content will be replaced with a new view.
     *
     * @param view The desired content to display. Value can't be null.
     * @see #setContentView(int)
     * @see #getContentView()
     */
    public void setContentView(View view) {
        ensureContent();
        if (view == null) {
            throw new IllegalArgumentException("Content view can't be null");
        }
        if (mContentContainer instanceof ViewGroup) {
            ViewGroup contentContainer = (ViewGroup) mContentContainer;
            if (mContentView == null) {
                contentContainer.addView(view);
            } else {
                int index = contentContainer.indexOfChild(mContentView);
                contentContainer.removeView(mContentView);
                contentContainer.addView(view, index);
            }

            mContentView = view;
        } else {
            throw new IllegalStateException("Can't be used with a custom content view");
        }
    }

    public void setContentShown(boolean shown) {
        setContentShown(shown, true);
    }

    /**
     * The default content for a ProgressFragment has a TextView that can be shown when the content is empty {@link #setContentEmpty}
     * @param resId
     */
    public void setEmptyText(int resId) {
        setEmptyText(getString(resId));
    }

    public void setEmptyText(CharSequence text) {
        ensureContent();
        if (mEmptyView != null && mEmptyView instanceof StoryTextView) {
            ((StoryTextView) mEmptyView).setText(text);
        } else {
            throw new IllegalStateException("Can't be used with a custom content view");
        }
    }

    public void setContentShownNoAnimation(boolean shown) {
        setContentShown(shown, false);
    }

    public void setContentShown(boolean shown, boolean animate) {
        ensureContent();
        if (mContentShown == shown) {
            return;
        }

        mContentShown = shown;
        if (shown) {
            if (animate) {
                mProgressContainer.startAnimation(AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_out));
                mContentContainer.startAnimation(AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_in));
            } else {
                mProgressContainer.clearAnimation();
                mContentContainer.clearAnimation();
            }

            mProgressContainer.setVisibility(View.GONE);
            mContentContainer.setVisibility(View.VISIBLE);
        } else {
            if (animate) {
                mProgressContainer.startAnimation(AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_in));
                mContentContainer.startAnimation(AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_out));
            } else {
                mProgressContainer.clearAnimation();
                mContentContainer.clearAnimation();
            }
            mProgressContainer.setVisibility(View.VISIBLE);
            mContentContainer.setVisibility(View.GONE);
        }
    }

    /**
     * Returns true if content is empty. The default content is not empty.
     *
     * @return true if content is null or empty
     * @see #setContentEmpty(boolean)
     */
    public boolean isContentEmpty() {
        return mIsContentEmpty;
    }

    /**
     * If the content is empty, then set true otherwise false. The default content is not empty. You can't call this method if the
     * content view has not been initialized before {@link #setContentView(android.view.View)} and content view not null.
     * @param isContentEmpty true if content is empty else false
     * @see #isContentEmpty()
     */
    public void setContentEmpty(boolean isContentEmpty) {
        ensureContent();
        if (mContentView == null) {
            throw new IllegalStateException("Content View must be initialized before");
        }

        if (isContentEmpty) {
            mEmptyView.setVisibility(View.VISIBLE);
            mContentView.setVisibility(View.GONE);
        } else {
            mEmptyView.setVisibility(View.GONE);
            mContentView.setVisibility(View.VISIBLE);
        }

        mIsContentEmpty = isContentEmpty;
    }

    /**
     * Initialization of views.
     */
    private void ensureContent() {
        if (mContentContainer != null && mProgressContainer != null) {
            return;
        }

        View root = getView();
        if (root == null) {
            throw new IllegalStateException("Content view not yet created");
        }
        mProgressContainer = root.findViewById(R.id.progress_container);
        if (mProgressContainer == null) {
            throw new RuntimeException("Your content must have a ViewGroup whose id attribute is 'R.id.progress_container'");
        }
        mContentContainer = root.findViewById(R.id.content_container);
        if (mContentContainer == null) {
            throw new RuntimeException("Your content must have a ViewGroup whose id attribute is 'R.id.content_container'");
        }
        mEmptyView = root.findViewById(android.R.id.empty);
        if (mEmptyView != null) {
            mEmptyView.setVisibility(View.GONE);
        }
        mContentShown = true;
        if (mContentView == null) {
            setContentShown(false, false);
        }
    }
}
