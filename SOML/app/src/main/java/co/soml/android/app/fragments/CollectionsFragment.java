/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 * @author: Larry Pham (email: larrypham.vn@gmail.com)
 * Date: 6/2/15 6:11 PM
 **/

package co.soml.android.app.fragments;

import com.techiedb.libs.droppy.DroppyClickCallbackInterface;
import com.techiedb.libs.droppy.DroppyMenuItem;
import com.techiedb.libs.droppy.DroppyMenuPopup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.soml.android.app.AppConstants;
import co.soml.android.app.R;
import co.soml.android.app.StoryApp;
import co.soml.android.app.activities.AppInterfaces;
import co.soml.android.app.activities.BaseActivity;
import co.soml.android.app.activities.CommentsActivity;
import co.soml.android.app.activities.InvalidateParam;
import co.soml.android.app.activities.PublishStoryActivity;
import co.soml.android.app.activities.StoryMainActivity;
import co.soml.android.app.activities.UserProfileActivity;
import co.soml.android.app.activities.adapters.StoryPostsAdapter;
import co.soml.android.app.camera.activities.CameraActivity;
import co.soml.android.app.controllers.CollectionsController;
import co.soml.android.app.controllers.ControllerMessage;
import co.soml.android.app.models.Comment;
import co.soml.android.app.models.Story;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.models.StoryFeedList;
import co.soml.android.app.models.StoryTypes.StoryPostListType;
import co.soml.android.app.models.User;
import co.soml.android.app.services.VoteResult;
import co.soml.android.app.services.tasks.Action;
import co.soml.android.app.utils.AccountUtils;
import co.soml.android.app.utils.CommonUtil;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.utils.NetworkUtils;
import co.soml.android.app.utils.SwipeToRefreshHelper;
import co.soml.android.app.widgets.StoryRecyclerView;
import co.soml.android.app.widgets.StoryTextView;

import static android.support.v7.widget.RecyclerView.State;
import static co.soml.android.app.models.StoryTypes.StoryPostListType.FEATURED_TYPE;

public class CollectionsFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener
        , AppInterfaces.OnPostPopupListener , AppInterfaces.DataLoadedListener, AppInterfaces.DataRequestedListener
        , AppInterfaces.ShowProfileListener, AppInterfaces.ContributePhotosListener, AppInterfaces.CommentActionListener{
    public static final String TAG = LogUtils.makeLogTag(CollectionsFragment.class.getSimpleName());
    public static final String USER_ID = "UserId";
    public static final String SCOPE_NAME = "ScopeName";
    public static final String START_INDEX = "StartIndex";
    public static final String LIMIT = "Limit";
    public static final String STORY_POST_LIST_TYPE = "StoryFeedType";
    public static final String COLLECTIONS_VIEW_STATE = "CollectionsViewState";
    public static final String X_API_VERSION = "X_API_VERSION";
    public static final String X_API_TOKEN = "X_API_TOKEN";
    public static final int REQUEST_TO_TAKE_PHOTO = 10001;

    private LayoutInflater mLayoutInflater;
    private Story mStory;
    private StoryApp mStoryApp;
    private BaseActivity mActivity;

    private StoryPostsAdapter mAdapter;
    private CollectionsController mController;
    private Context mContext;

    private Bundle mParams;
    private View mRootView;
    private StoryDataModel mStoryDataModel;
    private StoryFeedList mStories = new StoryFeedList();
    private boolean mNeedRequery = false;

    private int mApiVersion;
    private String mAuthToken;

    private int mUserId;
    private int mLimit = 10;
    private int mStartIndex = 1;
    private String mScopeName;

    private boolean mWasPaused;
    private StoryPostListType mStoryListType;
    private SwipeToRefreshHelper mSwipeToRefreshHelper;

    @Bind(R.id.recycler_view)
    StoryRecyclerView mRecyclerView;
    @Bind(R.id.swipe_refresh_layout)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @Bind(R.id.action_add)
    FloatingActionButton mActionButton;
    /*@Bind(R.id.add_footer_textview)
    StoryTextView mNotificationTextView;*/
    @Bind(R.id.empty_view)
    View mEmptyView;

    protected DroppyMenuPopup mFeedDroppyMenu;

    private LinearLayoutManager mLinearLayoutManager;
    private ArrayList<View> mFooterViews = null;

    public static int mUpdateState = 0;
    public static int mReUpdateState = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mStoryApp = (StoryApp) (this.getActivity()).getApplicationContext();
        mContext = this.getActivity().getApplicationContext();
        mStoryDataModel = mStoryApp.getAppDataModel();
        mActivity =  (BaseActivity) this.getActivity();
        mController = new CollectionsController(mActivity, this, mStoryDataModel);
        mParams = getArguments();

        if (mParams != null) {
            mStoryListType = (StoryPostListType) mParams.get(STORY_POST_LIST_TYPE);
            if (mStoryListType == null) {
                mStoryListType = StoryPostListType.FEATURED_TYPE;
            }
            mApiVersion = mParams.getInt(X_API_VERSION, 1);
            mAuthToken = mParams.getString(X_API_TOKEN);
            mScopeName = mParams.getString(SCOPE_NAME);

            mUserId = mParams.getInt(USER_ID, -1);
            getStoriesDataModel();
            if (!isExistedData()) {
                onInitializedData();
            }

        }
        mAdapter = new StoryPostsAdapter(mContext, FEATURED_TYPE, mStories);
        mAdapter.setOnPopupListener(this);
        mAdapter.setFeedController(mController);
        mAdapter.setOnDataLoadedListener(this);
        mAdapter.setShowProfileListener(this);
        mAdapter.setContributePhotosListener(this);
    }

    public void onInitializedData() {
        final CollectionsController.RequestStoriesParam param;
        if (mStoryListType != null) {
            if (mStoryListType == StoryPostListType.FEATURED_TYPE) {
                param = new CollectionsController.RequestStoriesParam(mScopeName, mStartIndex, mLimit, mApiVersion, mAuthToken);
                mController.sendMessage(ControllerMessage.REQUEST_TO_SERVER_STORIES_FEATURED, param);
            } else if (mStoryListType == StoryPostListType.LATEST_TYPE) {
                param = new CollectionsController.RequestStoriesParam(mScopeName, mStartIndex, mLimit, mApiVersion, mAuthToken);
                mController.sendMessage(ControllerMessage.REQUEST_TO_SERVER_STORIES_LATEST, param);
            } else if (mStoryListType == StoryPostListType.FOLLOWING_TYPE) {
                param = new CollectionsController.RequestStoriesParam(mUserId, mScopeName, mStartIndex, mLimit, mApiVersion, mAuthToken);
                mController.sendMessage(ControllerMessage.REQUEST_TO_SERVER_STORIES_FOLLOWING, param);
            }
        }
    }

    public boolean isExistedData() {
        getStoriesDataModel();
        if (mStories != null && mStories.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public void checkExistedData() {
        if (mStories != null && mStories.size() > 0) {
            LogUtils.LOGD(TAG, "StoryFeedList's not empty: ");
            mRecyclerView.setVisibility(View.VISIBLE);
            mEmptyView.setVisibility(View.GONE);
        } else {
            LogUtils.LOGD(TAG, "StoryFeedList's empty");
            mRecyclerView.setVisibility(View.GONE);
            mEmptyView.setVisibility(View.VISIBLE);
        }
    }

    public void getStoriesDataModel() {
        switch (mStoryListType) {
            case FEATURED_TYPE: {
                mStories = mStoryDataModel.getFeaturedStoryList();
                LogUtils.LOGD(TAG, String.format("FeaturedFeedList's Size Into CollectionsFragment: %d", mStories.size()));
                break;
            }
            case FOLLOWING_TYPE: {
                mStories = mStoryDataModel.getFollowingStoryList();
                LogUtils.LOGD(TAG, String.format("FollowingFeedList's Size Into CollectionsFragment: %d", mStories.size()));
                break;
            }
            case TRENDING_TYPE: {
                mStories = mStoryDataModel.getTrendingStoryList();
                LogUtils.LOGD(TAG, String.format("TrendingFeedList's Size Into CollectionsFragment: %d", mStories.size()));
                break;
            }
            case LATEST_TYPE: {
                mStories = mStoryDataModel.getLatestStoryList();
                LogUtils.LOGD(TAG, String.format("LatestFeedList's Size Into CollectionsFragment: %d", mStories.size()));
                break;
            }
            default:
                break;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.story_fragment_list, null);
        ButterKnife.bind(this, mRootView);
        return mRootView;
    }

    private Runnable mUpdateRunnable = new Runnable() {
        @Override
        public void run() {
            if (isExistedData()) {
                onInitializedData();
            }
            getStoriesDataModel();
            mAdapter.notifyDataSetChanged();
        }
    };

    public void checkEmptyView() {
        if (!isAdded()) {
            return;
        }
        boolean isEmpty = hasCollectionsAdapter() && getStoriesAdapter().isEmpty();
        final LinearLayout emptyView = (LinearLayout) getView().findViewById(R.id.empty_view);
        final StoryTextView title_empty_view = (StoryTextView) getView().findViewById(R.id.title_empty);
        final StoryTextView description_empty_view = (StoryTextView) getView().findViewById(R.id.description_empty);
        if (isEmpty) {
            switch (getStoryListType()) {
                case LATEST_TYPE: {
                    title_empty_view.setText(R.string.latest_empty_stories_list);
                    break;
                }
                case FOLLOWING_TYPE: {
                    title_empty_view.setText(R.string.following_empty_stories_list);
                    break;
                }
                case FEATURED_TYPE: {
                    title_empty_view.setText( R.string.featured_empty_stories_list);
                    break;
                }
            }
        }
        emptyView.setVisibility(isEmpty ? View.VISIBLE : View.GONE);
    }

    public void refresh() {
        if (hasCollectionsAdapter()) {
            getStoriesAdapter().refresh();
        }
    }

    private boolean hasCollectionsAdapter() {
        return (mAdapter != null);
    }

    private StoryPostsAdapter getStoriesAdapter() {
        if (mAdapter == null) {
            mAdapter = new StoryPostsAdapter(mContext, mStoryListType, mStories);
            mAdapter.setOnDataLoadedListener(new AppInterfaces.DataLoadedListener() {
                @Override
                public void onDataLoaded(boolean isEmpty) {
                    checkEmptyView();
                }
            });
        }
        return mAdapter;
    }

    public StoryPostListType getStoryListType() {
        return mStoryListType;
    }

    private OnScrollListener mOnScrollListener = new OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            mSwipeRefreshLayout.setEnabled(mLinearLayoutManager.findFirstCompletelyVisibleItemPosition() == 0);
        }

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }
    };

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (!isExistedData()) {
            onInitializedData();
        }
        getStoriesDataModel();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mSwipeRefreshLayout.setColorSchemeColors(R.color.color_primary_dark, R.color.color_primary, R.color.color_blur_primary);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.postDelayed(mUpdateRunnable, 3000);
        mSwipeToRefreshHelper = new SwipeToRefreshHelper(getActivity(), mSwipeRefreshLayout, new SwipeToRefreshHelper.RefreshListener() {
            @Override
            public void onRefreshStarted() {
                if (!isAdded()) {
                    return;
                }
                if (!NetworkUtils.isNetworkAvailable(getActivity())) {
                    showSwipeToRefreshProgress(false);
                    return;
                }
                showSwipeToRefreshProgress(true);
            }
        });

        mRecyclerView.setHasFixedSize(true);
        ArrayList<View> headerViews = new ArrayList<>();
        headerViews.add(getActionBarView());

        mLinearLayoutManager = new LinearLayoutManager(mActivity) {
            @Override
            protected int getExtraLayoutSpace(State state) {
                return 300;
            }
        };

        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addItemDecoration(new StoryRecyclerView.StoryItemDecoration(CommonUtil.dpToPx(-5), CommonUtil.dpToPx(10)));

        mFooterViews = new ArrayList<>();
        mActionButton.setTag(R.id.scroll_threshold_key, 3);
        mFooterViews.add(mActionButton);
        mRecyclerView.addOnScrollListener(mOnScrollListener);
    }

    @Override
    public void onResume() {
        super.onResume();
       /* if (!isExistedData()) {
            onInitializedData();
        }
        getStoriesDataModel();*/
    }

   /* @Nullable @OnClick(R.id.add_footer_textview)
    protected void dismissFooterView() {
        if (mNotificationTextView.getVisibility() == VISIBLE) {
            mNotificationTextView.setVisibility(View.GONE);
        }
    }*/

    @Nullable @OnClick(R.id.action_add)
    protected void startTakePhotoActivity() {
        if (mActionButton.getVisibility() == View.VISIBLE) {
            final Intent addNewStoryIntent = new Intent(getActivity(), PublishStoryActivity.class);
            getActivity().startActivityForResult(addNewStoryIntent, REQUEST_TO_TAKE_PHOTO);
            getActivity().finish();
        }
    }

    protected void showSwipeToRefreshProgress(boolean showProgress) {
        if (mSwipeToRefreshHelper != null && mSwipeToRefreshHelper.isRefreshing() != showProgress) {
            mSwipeToRefreshHelper.setRefreshing(showProgress);
        }
    }

    protected void updateStoriesFromFeeds(ControllerMessage msg, final Action updateAction) {
        if (!NetworkUtils.isNetworkAvailable(getActivity())) {
            LogUtils.LOGI(TAG, "Story List > Network Unavailable, Cancelled StoryFeed Update");
            return;
        }
        mController.sendMessage(ControllerMessage.AUTO_UPDATE_LIST_FEED, updateAction);
    }

    protected void reloadStoriesFeed(StoryFeedList stories) {
        if (stories != null && stories.size() > 0) {
            if (hasStoriesFeedAdapter()) {
            }
        }
    }

    protected boolean hasStoriesFeedAdapter() {
        return (mAdapter != null);
    }

    @Override
    public void invalidate() {
        //mRecyclerView.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void onPause() {
        LogUtils.LOGD(TAG, "onPause()");
        getStoriesDataModel();
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    protected View getActionBarView() {
        Window window = getActivity().getWindow();
        View view = window.getDecorView();
        int resId = getResources().getIdentifier("action_bar_container", "id", "android");
        return view.findViewById(resId);
    }

    @Override
    public void onDestroy() {
        LogUtils.LOGD(TAG, String.format("onDestroy(): FeedList's Size: %d", mStories.size()));
        super.onDestroy();
    }

    @Override
    public void invalidate(Object... params) {
        final InvalidateParam param = (InvalidateParam) params[0];
        if (param.getMessage() == ControllerMessage.REQUEST_TO_SERVER_STORIES_LATEST_COMPLETED) {
            mSwipeToRefreshHelper.setRefreshing(false);
            mAdapter.notifyDataSetChanged();
        } else if (param.getMessage() == ControllerMessage.REQUEST_TO_SERVER_STORIES_FEATURED_COMPLETED) {
            mAdapter.notifyDataSetChanged();
        } else if (param.getMessage() == ControllerMessage.REQUEST_TO_SERVER_STORIES_FOLLOWING_COMPLETED) {
            mAdapter.notifyDataSetChanged();
        } else if (param.getMessage() == ControllerMessage.REQUEST_TO_SERVER_STORY_LIKE_COMPLETED) {
            final StoryFeedList stories = mAdapter.getStories();
            final VoteResult result = (VoteResult) param.getObj();

            mAdapter.getStories().findStoryById(result.getId())
                    .setLikeByCurrentUser(true);
            mAdapter.getStories().findStoryById(result.getId())
                    .increaseLikeCount();
            mAdapter.notifyDataSetChanged();
            mRecyclerView.getAdapter().notifyItemChanged(result.getId());
        }  else if (param.getMessage() == ControllerMessage.REQUEST_TO_SERVER_STORY_UNLIKE_COMPLETED) {
            final StoryFeedList stories = mAdapter.getStories();
            final VoteResult result = (VoteResult) param.getObj();

            mAdapter.getStories().findStoryById(result.getId())
                    .setLikeByCurrentUser(false);
            mAdapter.getStories().findStoryById(result.getId())
                    .decreaseLikeCount();
            mAdapter.notifyDataSetChanged();
            mRecyclerView.getAdapter().notifyItemChanged(result.getId());
        }
    }

    @Override
    public void onRefresh() {
        LogUtils.LOGD(TAG, String.format("Refreshing RecycleView"));
        getStoriesDataModel();
    }

    public void updateAction(StoryFeedList stories) {
        int connectMode = CommonUtil.checkConnectivityMode(getActivity());
        if (stories != null && stories.size() > 0) {
            if (connectMode == AppConstants.CONN_WIFI || connectMode == AppConstants.CONN_3G) {
                if (connectMode == AppConstants.CONN_3G && (!CommonUtil.checkEnableMobileNetwork(getActivity()))) {
                    Toast.makeText(getActivity(), getString(R.string.msg_error_connection_failed), Toast.LENGTH_SHORT).show();
                    return;
                }
            }

        }
    }

    @Override
    public void onShowPopup(View view, Story story) {
        final DroppyMenuPopup droppyMenuPopup = buildDroppyMenu(view, story);
        droppyMenuPopup.show();
    }

    @Override
    public void onDataLoaded(boolean isEmpty) {

    }

    @Override
    public void onRequestData() {

    }

    @Override
    public void onShowProfile(View view) {
        final Intent showProfileIntent = new Intent(getActivity(), UserProfileActivity.class);
        final Bundle params = new Bundle();
        final int position = (Integer) view.getTag();
        if (mAdapter != null && mAdapter.getItemCount() > 0) {
            if (mAdapter.getStories().get(position) != null) {
                final int userId = mAdapter.getStories().get(position).getUserId();
                params.putInt("USER_ID", userId);
            }
        }
        showProfileIntent.putExtras(params);
        ((StoryMainActivity) getActivity()).startActivity(showProfileIntent);
    }

    @Override
    public void onContributePhotos(View view) {
        final int position = (Integer) view.getTag();
        final Intent takePhotoIntent = new Intent(getActivity(), CameraActivity.class);
        getActivity().startActivityForResult(takePhotoIntent, 1);
    }

    @Override
    public void onActionResult(boolean succeeded, Comment comment) {
        final Intent showCommentActivity = new Intent(getActivity(), CommentsActivity.class);

    }

    public static class CollectionsInvalidateParam {
        public int mMessage;
        public Object mObject;
        public int mStoryId;
    }

    public void showUpdateAllLayout() {
        LogUtils.LOGD(TAG, "Updating all of stories into current list");
        mStoryApp = (StoryApp) getActivity().getApplication();
    }

    public static final int REPORT_MENU_ITEM_POS = 0;
    public static final int SHARE_FACEBOOK_MENU_ITEM_POS = 1;
    public static final int SHARE_TWITTER_MENU_ITEM_POS= 2;

    protected DroppyMenuPopup buildDroppyMenu(final View anchorButton, final Story story) {
        DroppyMenuPopup.Builder droppyBuilder = new DroppyMenuPopup.Builder(getActivity(), anchorButton);
        final DroppyMenuItem reportMenuItem = new DroppyMenuItem(getString(R.string.report_story_context_menu_content));
        final DroppyMenuItem shareFacebookMenuItem = new DroppyMenuItem(getString(R.string.share_facebook_story_context_menu_content));
        final DroppyMenuItem shareTwitterMenuItem = new DroppyMenuItem(getString(R.string.share_twitter_story_context_menu_content));
        droppyBuilder.addMenuItem(reportMenuItem)
                     .addSeparator()
                     .addMenuItem(shareFacebookMenuItem)
                     .addMenuItem(shareTwitterMenuItem);
        droppyBuilder.setOnClick(new DroppyClickCallbackInterface() {
            @Override
            public void call(View view, int id) {
                switch (id) {
                    case REPORT_MENU_ITEM_POS: {
                        break;
                    }
                    case SHARE_FACEBOOK_MENU_ITEM_POS: {
                        break;
                    }
                    case SHARE_TWITTER_MENU_ITEM_POS: {
                        break;
                    }
                }
                LogUtils.LOGD(TAG, "Droppy Clicked On: " + String.valueOf(id));
            }
        });
        return droppyBuilder.build();
    }
}
