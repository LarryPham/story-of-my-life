/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/11/15 3:18 PM.
 **/

package co.soml.android.app.cells;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.HapticFeedbackConstants;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;

import co.soml.android.app.utils.LogUtils;

public class BaseCell extends View {

    public static final String TAG = LogUtils.makeLogTag(BaseCell.class.getSimpleName());

    private boolean mCheckForLongPress = false;
    private CheckForLongPress mPendingCheckForLongPress = null;
    private int mPressCount = 0;
    private CheckForTap mPendingCheckForTap = null;

    /**
     * A Runnable class represent to check the cell has been tapped or not
     */
    private final class CheckForTap implements Runnable {
        @Override
        public void run() {
            if (mPendingCheckForLongPress == null) {
                mPendingCheckForLongPress = new CheckForLongPress();
            }

            mPendingCheckForLongPress.mCurrentPressCount = ++mPressCount;
            LogUtils.LOGD(TAG, String.format("CurrentPressCount: %d", mPendingCheckForLongPress.mCurrentPressCount));
            postDelayed(mPendingCheckForLongPress, ViewConfiguration.getLongPressTimeout() - ViewConfiguration.getTapTimeout());
        }
    }

    class CheckForLongPress implements Runnable {
        public int mCurrentPressCount;

        @Override
        public void run() {
            if (mCheckForLongPress && getParent() != null && mCurrentPressCount == mPressCount) {
                mCheckForLongPress = false;
                MotionEvent event = MotionEvent.obtain(0, 0, MotionEvent.ACTION_CANCEL, 0, 0, 0);
                onTouchEvent(event);
                event.recycle();
                performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
                onLongPress();
            }
        }
    }

    public BaseCell(Context context) {
        super(context);
    }

    public BaseCell(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    protected void setDrawableBounds(Drawable drawable, int x, int y) {
        setDrawableBounds(drawable, x, y, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
    }

    protected void setDrawableBounds(Drawable drawable, int x, int y, int width, int height) {
        drawable.setBounds(x, y, x + width, y + height);
    }

    protected void startCheckLongPress() {
        if (mCheckForLongPress) {
            return;
        }

        mCheckForLongPress = true;
        if (mPendingCheckForTap == null) {
            mPendingCheckForTap = new CheckForTap();
        }
        postDelayed(mPendingCheckForTap, ViewConfiguration.getTapTimeout());
    }
    protected void cancelCheckLongPress() {
        mCheckForLongPress = false;
        LogUtils.LOGD(TAG, "Removing 2 callbacks for current base-cell");
        if (mPendingCheckForLongPress != null) {
            removeCallbacks(mPendingCheckForLongPress);
        }

        if (mPendingCheckForTap != null) {
            removeCallbacks(mPendingCheckForTap);
        }
    }

    protected void onLongPress() {
        LogUtils.LOGD(TAG, "Trigger to start the long press action");
    }
}
