package co.soml.android.app.utils;

import android.text.util.Linkify;
import android.widget.TextView;

public class CommentUtils {
	public static void displayHtmlComment(TextView textView, String content, int maxImageSize) {
		if (textView == null) {
			return;
		}

		if (content == null) {
			textView.setText(null);
			return;
		}

		if (!content.contains("<") && !content.contains("&")) {
			content = content.trim();
			textView.setText(content);
			if (content.contains("://")) {
				Linkify.addLinks(textView, Linkify.WEB_URLS);
			}
			return;
		}

	}
}
