package co.soml.android.app.widgets;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.DialogFragment;

import co.soml.android.app.controllers.BaseController;
import co.soml.android.app.utils.LogUtils;

public class ProgressCircleDialog extends DialogFragment {
    public static final String TAG = LogUtils.makeLogTag(ProgressCircleDialog.class.getSimpleName());

    private int mActionCount = 0;
    private BaseController mController = null;

    @Override
    public void onCancel(DialogInterface dialog) {
        LogUtils.LOGI(TAG, String.format("[GUN]Cancel acountCount[%d] reset[%s]", mActionCount, mController));
        super.onCancel(dialog);
    }

}
