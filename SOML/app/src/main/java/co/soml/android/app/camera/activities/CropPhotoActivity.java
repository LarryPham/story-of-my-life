/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/6/15 5:25 AM.
 **/

package co.soml.android.app.camera.activities;

import com.imagezoom.ImageViewTouch;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import co.soml.android.app.R;
import co.soml.android.app.StoryApp;
import co.soml.android.app.camera.CameraManager;
import co.soml.android.app.camera.utils.CameraHelper;
import co.soml.android.app.camera.utils.FileUtils;
import co.soml.android.app.utils.CommonUtil;
import co.soml.android.app.utils.ImageUtils;
import co.soml.android.app.utils.LogUtils;

public class CropPhotoActivity extends FragmentActivity {

    public static final String TAG = LogUtils.makeLogTag(CropPhotoActivity.class.getSimpleName());
    private static final boolean IN_MEMORY_CROP = Build.VERSION.SDK_INT < Build.VERSION_CODES.GINGERBREAD_MR1;
    private Uri mFileUri;
    private Bitmap mOriginalBitmap;
    private int mInitWidth, mInitHeight;
    private static final int MAX_WRAP_SIZE = 2048;

    private ImageViewTouch mCropImage;
    private ViewGroup mDrawArea;
    private View mWrapImage;
    private View mButtonCropType;
    private ImageView mImageCenter;
    private CameraHelper mCameraHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_crop);

        mCropImage = (ImageViewTouch) findViewById(R.id.crop_image);
        mDrawArea = (ViewGroup) findViewById(R.id.draw_area);
        mWrapImage = findViewById(R.id.wrap_image);
        mButtonCropType = findViewById(R.id.button_crop_type);
        mImageCenter = (ImageView) findViewById(R.id.image_center);
        mFileUri = getIntent().getData();

        mCameraHelper = new CameraHelper(this);
        CameraManager.getInstance().addActivity(this);
        initEvent();
        initView();
    }

    private void initEvent() {
        mButtonCropType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mButtonCropType.getVisibility() == View.VISIBLE) {
                    mButtonCropType.setSelected(true);
                    mCropImage.setVisibility(View.GONE);
                    mWrapImage.setVisibility(View.VISIBLE);
                } else {
                    mButtonCropType.setSelected(false);
                    mCropImage.setVisibility(View.VISIBLE);
                    mWrapImage.setVisibility(View.GONE);
                }
            }
        });

        mImageCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtils.LOGD(TAG, "Switchign the WrapImage button");
                mWrapImage.setSelected(!mWrapImage.isSelected());
            }
        });
        findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtils.LOGD(TAG, "Trigger to finish the current screen");
                finish();
            }
        });
        findViewById(R.id.picked).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Showing ProgressDialog
                new Thread() {
                    @Override
                    public void run() {
                        if (mButtonCropType.isSelected()) {
                            wrapImage();
                        } else {
                            cropImage();
                        }
                        //dismissDia
                    }
                }.start();
            }
        });
    }

    private void initView() {
        mDrawArea.getLayoutParams().height = CommonUtil.getScreenWidth(StoryApp.getContext());
        InputStream inStream = null;
        try {
            double rate = ImageUtils.getImageRatio(getContentResolver(), mFileUri);
            mOriginalBitmap = ImageUtils.decodeBitmapWithOrientationMax(mFileUri.getPath(),  CommonUtil.getScreenWidth(StoryApp
                    .getContext()), CommonUtil.getScreenHeight(StoryApp.getContext()));
            mInitWidth = mOriginalBitmap.getWidth();
            mInitHeight = mOriginalBitmap.getHeight();

            mCropImage.setImageBitmap(mOriginalBitmap, new Matrix(), (float) rate, 10);
            mImageCenter.setImageBitmap(mOriginalBitmap);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            FileUtils.closeStream(inStream);
        }
    }

    protected void cropImage() {
        Bitmap croppedImage;
        if (IN_MEMORY_CROP) {
            croppedImage = inMemoryCrop(mCropImage);
        } else {
            try {
                croppedImage = decodeRegionCrop(mCropImage);
            } catch (IllegalArgumentException ex) {
                croppedImage = inMemoryCrop(mCropImage);
            }
        }
        saveImageToCache(croppedImage);
    }

    protected void wrapImage() {
        int width = mInitWidth > mInitHeight ? mInitWidth : mInitHeight;
        int imageSize = width < MAX_WRAP_SIZE ? width : MAX_WRAP_SIZE;

        int move = (int)((mInitWidth - mInitHeight) / 2 / (float) width * (float) imageSize);
        int moveX = mInitWidth < mInitHeight ? move : 0;
        int moveY = mInitHeight < mInitWidth ? - move : 0;

        Bitmap croppedImage = null;
        try {
            croppedImage = Bitmap.createBitmap(imageSize, imageSize, Bitmap.Config.RGB_565);
            Canvas canvas = new Canvas(croppedImage);
            Paint paint = new Paint();
            paint.setColor(mWrapImage.isSelected() ? Color.BLACK : Color.WHITE);
            canvas.drawRect(0, 0, imageSize, imageSize, paint);
            Matrix matrix = new Matrix();
            matrix.postScale((float) imageSize/ (float) width, (float) imageSize/ (float)width);
            matrix.postTranslate(moveX, moveY);
            canvas.drawBitmap(mOriginalBitmap, matrix, null);
        } catch (OutOfMemoryError error) {
            LogUtils.LOGE(TAG, String.format("OutOfMemory Cropping Image: %s,%s" + error.getMessage(), error.toString()));
            System.gc();
        }
        saveImageToCache(croppedImage);
    }

    private void saveImageToCache(Bitmap croppedImage) {
        if (croppedImage != null) {
            try {
                ImageUtils.saveToFile(FileUtils.getInstance().getCacheDir() + "/croppedCache", false, croppedImage);
                Intent intent = new Intent();
                intent.setData(Uri.parse("file://" + FileUtils.getInstance().getCacheDir() + "/croppedCache"));
                setResult(RESULT_OK, intent);
                // dismiss dialog
                finish();
            } catch (Exception ex) {
                LogUtils.LOGD(TAG, String.format("Throwing new exception: %s",ex.toString()));
                ex.printStackTrace();
            }
        }
    }

    private float getImageRatio() {
        return Math.max((float) mInitWidth, (float) mInitHeight) / Math.min((float) mInitWidth, (float) mInitHeight);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        CameraManager.getInstance().removeActivity(this);
    }

    private Bitmap inMemoryCrop(ImageViewTouch cropImage) {
        int width = mInitWidth > mInitHeight ? mInitHeight : mInitWidth;
        int screenWidth = CommonUtil.getScreenWidth(StoryApp.getContext());
        System.gc();
        Bitmap croppedImage = null;
        try {
            croppedImage = Bitmap.createBitmap(width, width, Bitmap.Config.RGB_565);

            Canvas canvas = new Canvas(croppedImage);
            float scale = cropImage.getScale();
            RectF srcRect = cropImage.getBitmapRect();
            Matrix matrix = new Matrix();
            matrix.postScale(scale / getImageRatio(), scale / getImageRatio());
            matrix.postTranslate(srcRect.left * width / screenWidth, srcRect.top * width / screenWidth);

            canvas.drawBitmap(mOriginalBitmap, matrix, null);
        } catch (OutOfMemoryError error) {
            LogUtils.LOGE(TAG, String.format("OutOfMemoryError: Cropping Image: %s-%s",error.getMessage(), error.toString()));
            System.gc();
        }
        return croppedImage;
    }

    private Bitmap decodeRegionCrop(ImageViewTouch cropImage) {
        int width = mInitWidth > mInitHeight ? mInitHeight : mInitWidth;
        int screenWidth = CommonUtil.getScreenWidth(StoryApp.getContext());

        float scale = cropImage.getScale() / getImageRatio();
        RectF rectF = cropImage.getBitmapRect();

        int left = -(int) (rectF.left * width / screenWidth / scale);
        int top = -(int) (rectF.top * width / screenWidth / scale);
        int right = left + (int) (width / scale);
        int bottom = top + (int) (width / scale);

        Rect rect = new Rect(left, top, right, bottom);
        InputStream inStream = null;
        System.gc();
        Bitmap croppedImage = null;
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            mOriginalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            inStream = new ByteArrayInputStream(outputStream.toByteArray());
            BitmapRegionDecoder decoder = BitmapRegionDecoder.newInstance(inStream, false);
            croppedImage = decoder.decodeRegion(rect, new BitmapFactory.Options());
        } catch (Throwable throwable) {
            LogUtils.LOGE(TAG, String.format("Throws Error: %s", throwable.getMessage()));
        } finally {
            FileUtils.closeStream(inStream);
        }
        return croppedImage;
    }
}
