package co.soml.android.app.services.tasks;

import android.os.Handler;
import android.os.Message;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import co.soml.android.app.AppConstants;
import co.soml.android.app.controllers.ControllerMessage;
import co.soml.android.app.models.Error;
import co.soml.android.app.models.Story;
import co.soml.android.app.models.StorySettings;
import co.soml.android.app.models.User;
import co.soml.android.app.services.StoriesResult;
import co.soml.android.app.services.StoryResult;
import co.soml.android.app.services.UserListResult;
import co.soml.android.app.services.UserResult;
import co.soml.android.app.services.apis.AppRestApi;
import co.soml.android.app.services.rest.ItemTypeAdapterFactory;
import co.soml.android.app.utils.LogUtils;
import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its
 * documentation are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable
 * form without the prior written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with
 * respect to the contents, and assumes no responsibility for any errors that might appear in the software and
 * documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.02.2015
 */
public class ServerRequestTask implements Runnable {

    public static final String TAG = AppConstants.PREFIX + ServerRequestTask.class.getSimpleName();
    public static final int SUCCESSFULLY_CODE = 200;
    private int mApiVersion = 0;
    private String mApiAuthToken = null;

    private int mUserId = 0;
    private int mStoryId = 0;
    private int mId = 0;
    private int mStartIndex = 0;
    private int mLimit = 3;

    private String mQueries = null;
    private String mStoryScope = null;
    private String mState = null;
    private boolean mFollowed = false;
    private int mOffset = 0;
    private String mName = null;

    private Handler mServiceHandler = null;
    private String mAction = null;
    private RequestType mRequestType = RequestType.NONE;
    private int mRequestOwner = 0;
    private int mRequestMsg = 0;
    private boolean mCancelled = false;
    private String mEmail = null;
    private String mAccessToken;
    private Gson mGson;
    private StorySettings mStorySettings;
    private RequestInterceptor mInterceptor;
    private Callback mCallback;

    public enum RequestType {
        NONE, STORIES_FEATURED, STORIES_TRENDING, STORIES_FOLLOWING,
        STORIES_COLLABORATING, STORIES_LATEST, STORY, STORIES_SCOPE,
        MY_STORIES_PRIVATE, MY_STORIES_PUBLISHED,
        PHOTO, SONG_LIST, SONG,
        USER_SEARCH, USER_LIST, USER_DETAIL, USER_FOLLOWERS, USER_FOLLOWING,
        ACCOUNT_FOLLOWING, ACCOUNT_LIKED, ACCOUNT_NEW
    }

    public ServerRequestTask(String action, Handler handler) {
        this.mAction = action;
        this.mServiceHandler = handler;
        // Default Interceptor for first running time
        mInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade requestFacade) {
                requestFacade.addHeader("X_AUTH_TOKEN", "6XT57JEygAurVsEpUumu");
            }
        };
        mGson = new GsonBuilder()
                .setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'")
                .registerTypeAdapterFactory(new ItemTypeAdapterFactory()) // This is the important line ;)
                .create();
    }

    public RequestInterceptor getInterceptor() {
        return mInterceptor;
    }

    public void setRequestInterceptor(RequestInterceptor interceptor) {
        this.mInterceptor = interceptor;
    }

    public void run() {
        String fn = "run(): ";
        switch (mRequestType) {
            case STORIES_FEATURED:
            case STORIES_TRENDING:
            case STORIES_FOLLOWING:
            case STORIES_COLLABORATING:
            case STORIES_LATEST: {
                getStoriesByScope();
                break;
            }
            case MY_STORIES_PRIVATE: {
                getMyStories();
                break;
            }
            case MY_STORIES_PUBLISHED: {
                getMyStories();
                break;
            }
            case STORY: {
                getStory();
                break;
            }
            case USER_FOLLOWERS: {
                getFollowers();
                break;
            }
            case USER_FOLLOWING: {
                getFollowingUsers();
                break;
            }
            case USER_SEARCH:
            case USER_LIST: {
                getUsers();
                break;
            }
            case USER_DETAIL: {
                getUser();
                break;
            }
            case ACCOUNT_FOLLOWING: {
                checkAccountFollowing();
                break;
            }
            case ACCOUNT_LIKED: {
                checkAccountLiked();
                break;
            }
            case ACCOUNT_NEW: {
                fetchAccountInfo();
                break;
            }
            default:
                break;

        }
    }

    public int getApiVersion() {
        return mApiVersion;
    }

    public void setApiVersion(int apiVersion) {
        mApiVersion = apiVersion;
    }

    public String getApiAuthToken() {
        return mApiAuthToken;
    }

    public void setApiAuthToken(String apiAuthToken) {
        mApiAuthToken = apiAuthToken;
    }

    public int getUserId() {
        return mUserId;
    }

    public void setUserId(int userId) {
        mUserId = userId;
    }

    public int getStoryId() {
        return mStoryId;
    }

    public void setStoryId(int storyId) {
        mStoryId = storyId;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getStartIndex() {
        return mStartIndex;
    }

    public void setStartIndex(int startIndex) {
        mStartIndex = startIndex;
    }

    public int getLimit() {
        return mLimit;
    }

    public void setLimit(int limit) {
        mLimit = limit;
    }

    public String getStoryScope() {
        return mStoryScope;
    }

    public void setStoryScope(String storyScope) {
        mStoryScope = storyScope;
    }

    public String getQueries() {
        return mQueries;
    }

    public void setQueries(String queries) {
        mQueries = queries;
    }

    public boolean isCancelled() {
        return this.mCancelled;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

    public boolean isFollowed() {
        return mFollowed;
    }

    public void setFollowed(boolean followed) {
        mFollowed = followed;
    }

    public int getOffset() {
        return mOffset;
    }

    public void setOffset(int offset) {
        mOffset = offset;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public void setCancelled(boolean cancelled) {
        String fn = "setCancelled(): ";
        this.mCancelled = cancelled;
    }

    public String getAction() {
        return mAction;
    }

    public void setAction(String action) {
        mAction = action;
    }

    public RequestType getRequestType() {
        return mRequestType;
    }

    public void setRequestType(RequestType requestType) {
        mRequestType = requestType;
    }

    public int getRequestOwner() {
        return mRequestOwner;
    }

    public void setRequestOwner(int requestOwner) {
        mRequestOwner = requestOwner;
    }

    public int getRequestMessage() {
        return mRequestMsg;
    }

    public void setRequestMessage(int requestMessage) {
        mRequestMsg = requestMessage;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public void addXAPIAuthen() {
        if (getApiAuthToken() != null && getApiVersion() != 0) {
            mInterceptor = new RequestInterceptor() {
                @Override
                public void intercept(RequestFacade requestFacade) {
                    requestFacade.addHeader("X_API_VERSION", String.valueOf(getApiVersion()));
                    requestFacade.addHeader("X_AUTH_TOKEN", getApiAuthToken());
                }
            };
            // Changing the new interceptor with 2 params for headers
            setRequestInterceptor(mInterceptor);
        }
    }

    public void addAuthToken() {
        if (getApiAuthToken() != null) {
            mInterceptor = new RequestInterceptor() {
                @Override
                public void intercept(RequestFacade requestFacade) {
                    requestFacade.addHeader("X_API_VERSION", String.valueOf(getApiVersion()));
                }
            };
            // Changing the new interceptor with 2 params for headers
            setRequestInterceptor(mInterceptor);
        }
    }

    public void checkAccountFollowing() {
        final String fn = "[checkAccountFollowing]:";
        final UserResult result = new UserResult();
        final AppRestApi appRestApi = RestApiUtils.getAppRestApi();
        mCallback = new Callback<User>() {
            @Override
            public void success(User user, Response response) {
                result.setUser(user);
                result.setResultCode(response.getStatus());
                result.setResultMessage(response.getReason());
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_ACCOUNT_FOLLOWING_COMPLETED %s", fn, result));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_ACCOUNT_FOLLOWING_COMPLETED, mRequestOwner, mRequestMsg, result);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                final Error error = new Error();

                LogUtils.LOGD(TAG, String.format("Response: %s", retrofitError.getResponse()));
                final int errorCode = retrofitError.getResponse().getStatus();
                final String errorMessage = retrofitError.getMessage();
                final String errorException = retrofitError.getResponse().getReason();

                error.setErrorCode(errorCode);
                error.setErrorMessage(errorMessage);
                error.setErrorException(errorException);
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_ERROR --- %d,%s", fn, errorCode, errorMessage));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, mRequestOwner, mRequestMsg, error);
            }
        };
        appRestApi.checkAccountFollowingUser(mUserId, mCallback);
    }

    public void checkAccountLiked() {
        final String fn = "[checkAccountLiked]:";
        final StoryResult result = new StoryResult();
        final AppRestApi appRestApi = RestApiUtils.getAppRestApi();
        mCallback = new Callback<Story>() {
            @Override
            public void success(Story story, Response response) {
                result.setStory(story);
                result.setResultCode(response.getStatus());
                result.setResultMessage(response.getReason());
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_ACCOUNT_LIKED_STORY_COMPLETED %s", fn, result));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_ACCOUNT_LIKED_STORY_COMPLETED, mRequestOwner, mRequestMsg, result);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                final Error error = new Error();

                LogUtils.LOGD(TAG, String.format("Response: %s", retrofitError.getResponse()));
                final int errorCode = retrofitError.getResponse().getStatus();
                final String errorMessage = retrofitError.getMessage();
                final String errorException = retrofitError.getResponse().getReason();

                error.setErrorCode(errorCode);
                error.setErrorMessage(errorMessage);
                error.setErrorException(errorException);
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_ERROR --- %d,%s", fn, errorCode, errorMessage));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, mRequestOwner, mRequestMsg, error);
            }
        };
        appRestApi.checkAccountLikedStory(mStoryId, mCallback);
    }

    public void fetchAccountInfo() {
        final String fn = "[fetchAccountInfo]:";
        final AppRestApi appRestApi = RestApiUtils.getAppRestApi(mInterceptor);
        mCallback = new Callback<UserResult>() {
            @Override
            public void success(UserResult user, Response response) {
                UserResult result = user;
                result.setResultCode(response.getStatus());
                result.setResultMessage(response.getReason());
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_ACCOUNT_INFO_COMPLETED %s", fn, result));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_ACCOUNT_INFO_COMPLETED, mRequestOwner, mRequestMsg, result);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                final Error error = new Error();

                LogUtils.LOGD(TAG, String.format("Response: %s", retrofitError.getResponse()));
                final int errorCode = retrofitError.getResponse().getStatus();
                final String errorMessage = retrofitError.getMessage();
                final String errorException = retrofitError.getResponse().getReason();

                error.setErrorCode(errorCode);
                error.setErrorMessage(errorMessage);
                error.setErrorException(errorException);
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_ERROR --- %d,%s", fn, errorCode, errorMessage));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, mRequestOwner, mRequestMsg, error);
            }
        };
        appRestApi.fetchAccountInfo(mCallback);
    }

    /**
     * Gets the list of story which has included the chunk of story that had been uploaded into server.
     * Note that: using the params for request the chunk of stories that were included
     * - X_API_VERSION       Header param which will be upgraded for checking the version of api when first running time.
     * - X_API_AUTH_TOKEN    Current Logged User's token for sending certifications to server
     */
    public void getStoriesByScope() {
        final String fn = "[getStoriesByScope]:";
        final StoriesResult result = new StoriesResult();
        addXAPIAuthen();
        final AppRestApi appRestApi = RestApiUtils.getAppRestApi(getInterceptor(), new GsonConverter(mGson));
        mCallback = new Callback<StoriesResult>() {
            @Override
            public void success(StoriesResult storyList, Response response) {
                if (storyList != null) {
                    result.setStoryList(storyList.getStoryList());
                    result.setResultCode(response.getStatus());
                    result.setResultMessage(response.getReason());
                    if (mRequestType == RequestType.STORIES_FEATURED) {
                        LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_STORIES_FEATURED_COMPLETED %s", fn, result));
                        sendMessage(ControllerMessage.REQUEST_TO_SERVER_STORIES_FEATURED_COMPLETED, mRequestOwner, mRequestMsg, result);
                    } else if (mRequestType == RequestType.STORIES_LATEST) {
                        LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_STORIES_LATEST_COMPLETED %s", fn, result));
                        sendMessage(ControllerMessage.REQUEST_TO_SERVER_STORIES_LATEST_COMPLETED, mRequestOwner, mRequestMsg, result);
                    } else if (mRequestType == RequestType.STORIES_FOLLOWING) {
                        LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_STORIES_FOLLOWING_COMPLETED %s", fn, result));
                        sendMessage(ControllerMessage.REQUEST_TO_SERVER_STORIES_FOLLOWING_COMPLETED, mRequestOwner, mRequestMsg, result);
                    } else if (mRequestType == RequestType.STORIES_COLLABORATING) {
                        LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_STORIES_COLLABORATING_COMPLETED %s", fn, result));
                        sendMessage(ControllerMessage.REQUEST_TO_SERVER_STORIES_COLLABORATING_COMPLETED, mRequestOwner, mRequestMsg, result);
                    }
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                if (retrofitError.getResponse() != null) {
                    final Error error = new Error();
                    if (retrofitError.getResponse() != null)
                        LogUtils.LOGD(TAG, String.format("Response: %s", retrofitError.getResponse()));
                    final int errorCode = retrofitError.getResponse().getStatus();
                    final String errorMessage = retrofitError.getMessage();
                    final String errorException = retrofitError.getResponse().getReason();

                    error.setErrorCode(errorCode);
                    error.setErrorMessage(errorMessage);
                    error.setErrorException(errorException);
                    LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_ERROR --- %d,%s", fn, errorCode, errorMessage));
                    sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, mRequestOwner, mRequestMsg, error);
                }
            }
        };
        appRestApi.getStoriesByScope(mStoryScope, mStartIndex, mLimit, mCallback);
    }

    /**
     * Gets the list of current user's story which has included the published stories, private stories, and collaborated stories.
     * Note that: using the params for request the chunks of stories that were included
     * - X_API_VERSION      Header param which will be upgraded for checking the version of api when app's running first-time mode.
     * - X_API_AUTH_TOKEN   Current user's token for sending certifications to server
     * <p>
     * User's params: UserId - the current user's id
     * StartIndex - the started index for querying the list of stories
     * Limit      - the limitation of queried list
     * State      - the key for separating the list of stories via the published stories and private stories.
     */
    public void getMyStories() {
        final String fn = "[getMyStories]:";
        addXAPIAuthen();
        final AppRestApi appRestApi = RestApiUtils.getAppRestApi(getInterceptor(), new GsonConverter(mGson));
        mCallback = new Callback<StoriesResult>() {
            StoriesResult result = new StoriesResult();

            @Override
            public void success(StoriesResult storyList, Response response) {
                if (storyList != null && storyList.getStoryList().size() > 0) {
                    result.setStoryList(storyList.getStoryList());
                    LogUtils.LOGD(TAG, String.format("Stories's Size: %d", result.getStoryList().size()));
                }
                result.setResultCode(response.getStatus());
                result.setResultMessage(response.getReason());
                if (getRequestType() == RequestType.MY_STORIES_PRIVATE) {
                    LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_MY_STORY_PRIVATE_COMPLETED %s", fn, result));
                    sendMessage(ControllerMessage.REQUEST_TO_SERVER_MY_STORY_PRIVATE_COMPLETED, mRequestOwner, mRequestMsg, result);

                } else {
                    LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_MY_STORY_PUBLISHED_COMPLETED %s", fn, result));
                    sendMessage(ControllerMessage.REQUEST_TO_SERVER_MY_STORY_PUBLISHED_COMPLETED, mRequestOwner, mRequestMsg, result);
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                final Error error = new Error();

                if (retrofitError.getResponse() != null) {
                    LogUtils.LOGD(TAG, String.format("Response: %s", retrofitError.getResponse()));
                    final int errorCode = retrofitError.getResponse().getStatus();
                    final String errorMessage = retrofitError.getMessage();
                    final String errorException = retrofitError.getResponse().getReason();

                    error.setErrorCode(errorCode);
                    error.setErrorMessage(errorMessage);
                    error.setErrorException(errorException);
                    LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_ERROR --- %d,%s", fn, errorCode, errorMessage));
                    sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, mRequestOwner, mRequestMsg, error);
                }
            }
        };
        appRestApi.getMyStories(mUserId, mStartIndex, mLimit, mState, mCallback);
    }

    public void getStory() {
        final String fn = "[getStory]:";
        final StoryResult result = new StoryResult();
        final AppRestApi appRestApi = RestApiUtils.getAppRestApi();
        mCallback = new Callback<Story>() {
            @Override
            public void success(Story story, Response response) {
                result.setStory(story);
                result.setResultCode(response.getStatus());
                result.setResultMessage(response.getReason());
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_STORY_COMPLETED %s", fn, result));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_STORY_COMPLETED, mRequestOwner, mRequestMsg, result);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                final Error error = new Error();

                LogUtils.LOGD(TAG, String.format("Response: %s", retrofitError.getResponse()));
                final int errorCode = retrofitError.getResponse().getStatus();
                final String errorMessage = retrofitError.getMessage();
                final String errorException = retrofitError.getResponse().getReason();

                error.setErrorCode(errorCode);
                error.setErrorMessage(errorMessage);
                error.setErrorException(errorException);
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_ERROR --- %d,%s", fn, errorCode, errorMessage));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, mRequestOwner, mRequestMsg, error);
            }
        };
        appRestApi.getStory(mStoryId, mCallback);
    }

    public void getFollowers() {
        final String fn = "[getFollowers]:";
        addXAPIAuthen();

        final UserListResult result = new UserListResult();
        final AppRestApi appRestApi = RestApiUtils.getAppRestApi(getInterceptor(), new GsonConverter(mGson));
        mCallback = new Callback<UserListResult>() {
            @Override
            public void success(UserListResult userListResult, Response response) {
                if (userListResult != null && userListResult.getUserList().size() > 0) {
                    result.setUserList(userListResult.getUserList());
                    result.setResultCode(response.getStatus());
                    result.setResultMessage(response.getReason());
                    LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_USER_FOLLOWERS_COMPLETED %s", fn, result));
                    sendMessage(ControllerMessage.REQUEST_TO_SERVER_USER_FOLLOWERS_COMPLETED, mRequestOwner, mRequestMsg, result);
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                final Error error = new Error();

                LogUtils.LOGD(TAG, String.format("Response: %s", retrofitError.getResponse()));
                final int errorCode = retrofitError.getResponse().getStatus();
                final String errorMessage = retrofitError.getMessage();
                final String errorException = retrofitError.getResponse().getReason();

                error.setErrorCode(errorCode);
                error.setErrorMessage(errorMessage);
                error.setErrorException(errorException);
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_ERROR --- %d,%s", fn, errorCode, errorMessage));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, mRequestOwner, mRequestMsg, error);
            }
        };
        appRestApi.getFollowers(mUserId, mCallback);
    }

    public void getFollowingUsers() {
        final String fn = "[getFollowingUsers]:";
        addXAPIAuthen();

        final UserListResult result = new UserListResult();
        final AppRestApi appRestApi = RestApiUtils.getAppRestApi(getInterceptor(), new GsonConverter(mGson));
        mCallback = new Callback<UserListResult>() {
            @Override
            public void success(UserListResult listResult, Response response) {
                if (listResult != null && listResult.getUserList().size() > 0) {
                    result.setUserList(listResult.getUserList());
                    result.setResultCode(response.getStatus());
                    result.setResultMessage(response.getReason());
                    LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_USER_FOLLOWING_COMPLETED %s", fn, result));
                    sendMessage(ControllerMessage.REQUEST_TO_SERVER_USER_FOLLOWING_COMPLETED, mRequestOwner, mRequestMsg, result);
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                final Error error = new Error();

                LogUtils.LOGD(TAG, String.format("Response: %s", retrofitError.getResponse()));
                final int errorCode = retrofitError.getResponse().getStatus();
                final String errorMessage = retrofitError.getMessage();
                final String errorException = retrofitError.getResponse().getReason();

                error.setErrorCode(errorCode);
                error.setErrorMessage(errorMessage);
                error.setErrorException(errorException);
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_ERROR --- %d,%s", fn, errorCode, errorMessage));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, mRequestOwner, mRequestMsg, error);
            }
        };
        appRestApi.getFollowingUsers(mUserId, mCallback);
    }

    public void getUsers() {
        final String fn = "[getUsers]:";
        final UserListResult result = new UserListResult();
        final AppRestApi appRestApi = RestApiUtils.getAppRestApi();
        mCallback = new Callback<UserListResult>() {
            @Override
            public void success(UserListResult userListResult, Response response) {
                result.setUserList(userListResult.getUserList());
                result.setResultCode(response.getStatus());
                result.setResultMessage(response.getReason());
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_USERS_COMPLETED %s", fn, result));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_USERS_COMPLETED, mRequestOwner, mRequestMsg, result);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                final Error error = new Error();

                LogUtils.LOGD(TAG, String.format("Response: %s", retrofitError.getResponse()));
                final int errorCode = retrofitError.getResponse().getStatus();
                final String errorMessage = retrofitError.getMessage();
                final String errorException = retrofitError.getResponse().getReason();

                error.setErrorCode(errorCode);
                error.setErrorMessage(errorMessage);
                error.setErrorException(errorException);
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_ERROR --- %d,%s", fn, errorCode, errorMessage));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, mRequestOwner, mRequestMsg, error);
            }
        };
        appRestApi.getUsers(mName, mFollowed, mLimit, mOffset, mCallback);
    }

    public void getUser() {
        final String fn = "[getUser]:";
        final UserResult result = new UserResult();
        addXAPIAuthen();
        final AppRestApi appRestApi = RestApiUtils.getAppRestApi(getInterceptor(), new GsonConverter(mGson));;
        mCallback = new Callback<UserResult>() {
            @Override
            public void success(UserResult userResult, Response response) {
                result.setUser(userResult.getUser());
                result.setResultCode(response.getStatus());
                result.setResultMessage(response.getReason());
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_USER_COMPLETED %s", fn, result));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_USER_COMPLETED, mRequestOwner, mRequestMsg, result);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                final Error error = new Error();

                LogUtils.LOGD(TAG, String.format("Response: %s", retrofitError.getResponse()));
                if (retrofitError != null && retrofitError.getResponse() != null) {
                    final int errorCode = retrofitError.getResponse().getStatus();
                    final String errorMessage = retrofitError.getMessage();
                    final String errorException = retrofitError.getResponse().getReason();

                    error.setErrorCode(errorCode);
                    error.setErrorMessage(errorMessage);
                    error.setErrorException(errorException);
                    LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_ERROR --- %d,%s", fn, errorCode, errorMessage));
                    sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, mRequestOwner, mRequestMsg, error);
                }
            }
        };
        appRestApi.getUser(mUserId, mCallback);
    }

    public void sendMessage(int code, int requestOwner, int requestMessage, Object obj) {
        String fn = "sendMessage(): ";
        final Message msg = new Message();
        LogUtils.LOGD(TAG, fn + "code     = " + ControllerMessage.toString(code) + "(" + code + ")");
        LogUtils.LOGD(TAG, fn + "requestOwner  = " + requestOwner);
        LogUtils.LOGD(TAG, fn + "requestMsg  = " + ControllerMessage.toString(requestMessage) + "(" + requestMessage + ")");
        if (obj instanceof Error) {
            final Error error = (Error) obj;
            int errorCode = error.getErrorCode();
            LogUtils.LOGD(TAG, fn + "errorCode  = " + ControllerMessage.toString(error.getErrorCode()) + "(" + errorCode + ")");
        }
        msg.what = code;
        msg.arg1 = requestOwner;
        msg.arg2 = requestMessage;
        msg.obj = obj;
        if (this.isCancelled()) {
            LogUtils.LOGD(TAG, String.format("Task cancelled. do not message"));
        } else {
            mServiceHandler.sendMessage(msg);
        }
    }
    /**
     * Method getControllerMessage() which used to get the controller's message after sending the message to service component
     */
    private int getControllerMessage(String action) {
        int msg = 0;
        if (action.equals(Action.AUTO_UPDATE_LIST_STORY)) {
            msg = ControllerMessage.AUTO_UPDATE_LIST_FEED_COMPLETED;
        }

        return msg;
    }
}