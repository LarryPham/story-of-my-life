package co.soml.android.app.utils;


import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.widget.ImageView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.soml.android.app.StoryApp;
import co.soml.android.app.cache.BitmapWorkerTask;
import co.soml.android.app.camera.utils.FileUtils;
import co.soml.android.app.models.Album;
import co.soml.android.app.models.Photo;

public class ImageUtils {

	public static final String TAG = LogUtils.makeLogTag(ImageUtils.class.getSimpleName());

	public static int[] getImageSize(Uri uri, Context context) {
		String path = null;
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;

		if (uri.toString().contains("content: ")) {
			String[] projection = new String[] {MediaStore.Images.Media._ID, MediaStore.Images.Media.DATA};
			Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);
			if (cursor != null) {
				if (cursor.moveToFirst()) {
					int dataColumn = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
					path = cursor.getString(dataColumn);
				}
				cursor.close();
			}
		}

		if (TextUtils.isEmpty(path)) {
			path = uri.toString().replace("content://media", "");
			path = path.replace("file://", "");
		}

		BitmapFactory.decodeFile(path, options);
		int imageWidth = options.outWidth;
		int imageHeight = options.outHeight;
		return new int[] { imageWidth, imageHeight };
	}

	public static int getImageOrientation(Context context, String filePath) {
		Uri currentStream;
		int orientation = 0;

		filePath = filePath.replace("file://","");
		if (!filePath.contains("content://")) {
			currentStream = Uri.parse("content://media" + filePath);
		} else {
			currentStream = Uri.parse(filePath);
		}

		try {
			Cursor cursor = context.getContentResolver().query(currentStream, new String[] { MediaStore.Images.Media.ORIENTATION}, null,
					null, null);
			if (cursor != null) {
				if (cursor.moveToFirst()) {
					orientation = cursor.getInt(cursor.getColumnIndex(MediaStore.Images.Media.ORIENTATION));
				}
				cursor.close();
			}
		} catch (Exception ex) {
			LogUtils.LOGE(TAG, ex.getMessage());
		}

		if (orientation == 0) {
			orientation = getExifOrientation(filePath);
		}
		return orientation;
	}

	public static int getExifOrientation(String path) {
		ExifInterface exif;
		try {
			exif = new ExifInterface(path);
		} catch (IOException ex) {
			LogUtils.LOGE(TAG, ex.getMessage());
			return 0;
		}

		int exifOrientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
		switch (exifOrientation) {
			case ExifInterface.ORIENTATION_NORMAL:
				return 0;
			case ExifInterface.ORIENTATION_ROTATE_90:
				return 90;
			case ExifInterface.ORIENTATION_ROTATE_180:
				return 180;
			case ExifInterface.ORIENTATION_ROTATE_270:
				return 270;
			default:
				return 0;
		}
	}

	public static int calculateSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = -1;

		if (height > reqHeight || width > reqHeight) {
			final int heightRatio = Math.round((float) height / (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);

			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}
		return inSampleSize;
	}

	public interface BitmapWorkerCallback {
		public void onBitmapReady(String filePath, ImageView imageView, Bitmap bitmap);
	}

	public static class BitmapWorkerTask extends AsyncTask<String, Void, Bitmap> {
		private final WeakReference<ImageView> mImageViewRef;
		private final BitmapWorkerCallback mCallback;
		private int mTargetHeight;
		private int mTargetWidth;
		private String mPath;

		public BitmapWorkerTask(ImageView imageView, int width, int height, BitmapWorkerCallback callback) {
			// Use a weakReference to ensure the ImageView can be garbage collected
			mImageViewRef = new WeakReference<ImageView>(imageView);
			this.mCallback = callback;
			this.mTargetWidth = width;
			this.mTargetHeight = height;
		}

		@Override
		protected Bitmap doInBackground(String... params) {
			mPath = params[0];

			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			BitmapFactory.decodeFile(mPath, options);
			options.inSampleSize = calculateSampleSize(options, mTargetWidth, mTargetHeight);
			options.inJustDecodeBounds = false;

			int bitmapWidth = 0;
			int bitmapHeight = 0;

			try {
				File file = new File(mPath);
				ExifInterface exif = new ExifInterface(file.getPath());
				int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
				int angle = 0;
				if (orientation == ExifInterface.ORIENTATION_NORMAL) {
					return BitmapFactory.decodeFile(mPath, options);
				} else if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
					angle = 90;
				} else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
					angle = 180;
				} else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
					angle = 270;
				}

				Matrix matrix = new Matrix();
				matrix.postRotate(angle);

				try {
					Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(file), null, options);
					if (bitmap == null) {
						LogUtils.LOGE(TAG,"Can't decode bitmap: " + file.getPath());
						return null;
					}

					bitmapWidth = bitmap.getWidth();
					bitmapHeight = bitmap.getHeight();

					return Bitmap.createBitmap(bitmap, 0, 0, bitmapWidth, bitmapHeight, matrix, true);
				} catch (IOException ex) {
					LogUtils.LOGE(TAG, String.format("OutOfMemoryError Error in Setting Image " + ex.getMessage()));
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Bitmap bitmap) {
			if (mImageViewRef == null || bitmap == null) {
				return;
			}
			final ImageView imageView = mImageViewRef.get();
			if (mCallback != null) {
				mCallback.onBitmapReady(mPath, imageView, bitmap);
			}
		}

		public static String getTitleForStoyrImageSpan(Context context, String filePath) {
			if (filePath == null) {
				return null;
			}
			Uri curStream;
			String title;

			if (!filePath.contains("content://"))
				curStream = Uri.parse("content://media" + filePath);
			else
				curStream = Uri.parse(filePath);

			if (filePath.contains("audio")) {
				return "audio";
			} else {
				String[] projection = new String[] { MediaStore.Images.Thumbnails.DATA };

				Cursor cursor;
				try {
					cursor = context.getContentResolver().query(curStream, projection, null, null, null);
				} catch (Exception ex) {
					LogUtils.LOGE(TAG, ex.getMessage());
					return null;
				}
				File jpeg;
				if (cursor != null) {
					String thumbData = "";
					if (cursor.moveToFirst()) {
						int dataColumn = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
						thumbData = cursor.getString(dataColumn);
					}
					cursor.close();
					if (thumbData == null) {
						return null;
					}
					jpeg = new File(thumbData);
				} else {
					String path = filePath.toString().replace("file://","");
					jpeg = new File(path);
				}
				title = jpeg.getName();
				return title;
			}
		}
	}

	/**
	 * Resizes an image to be placed in the Post Content Editor
	 * @return bitmap
	 */
	public static Bitmap getStoryImageSpanThumbnailFromFilePath(Context context, String filePath, int targetWidth) {
		if (filePath == null || context == null) {
			return null;
		}

		Uri currentUri;
		if (!filePath.contains("content://")) {
			currentUri = Uri.parse("content://media" + filePath);
		} else {
			currentUri = Uri.parse(filePath);
		}

		if (filePath.contains("audio")) {
			int audioId = 0;
			try {
				audioId = Integer.parseInt(currentUri.getLastPathSegment());
			} catch (NumberFormatException ex) {

			}
			ContentResolver thumbContentResolver = context.getContentResolver();
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inSampleSize = 1;
			//Bitmap audioThumbnail = MediaStore.Audio
			return null;
		}
		return null;
	}

	public static Bitmap getScaleBitmapAtLongestSide(Bitmap bitmap, int targetSize) {
		if (bitmap.getWidth() <= targetSize && bitmap.getHeight() <= targetSize) {
			return bitmap;
		}

		int targetHeight, targetWidth;
		if (bitmap.getHeight() > bitmap.getWidth()) {
			targetHeight = targetSize;
			float percentage = (float) targetSize / bitmap.getHeight();
			targetWidth = (int) (bitmap.getWidth() * percentage);
		} else {
			targetWidth = targetSize;
			float percentage = (float) targetSize / bitmap.getWidth();
			targetHeight = (int)(bitmap.getHeight() * percentage);
		}
		return Bitmap.createScaledBitmap(bitmap, targetWidth, targetHeight, true);
	}

	public static Bitmap getCircularBitmap(final Bitmap bitmap) {
		if (bitmap == null) {
			return  null;
		}

		final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
		final Canvas canvas = new Canvas(output);
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
		final RectF rectF = new RectF(rect);

		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(Color.RED);
		canvas.drawOval(rectF, paint);

		paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);
		return output;
	}

	public static Bitmap getRoundedEdgeBitmap(final Bitmap bitmap, int radius) {
		if (bitmap == null) {
			return null;
		}

		final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
		final Canvas canvas = new Canvas(output);
		final Paint paint = new Paint();

		final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
		final RectF rectF = new RectF(rect);

		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(Color.RED);

		canvas.drawRoundRect(rectF, radius, radius, paint);
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeWidth(1f);
		paint.setColor(Color.DKGRAY);
		canvas.drawRoundRect(rectF, radius, radius, paint);
		return output;
	}

	public static int getMaximumThumbnailWithForEditor(final Context context) {
		int maximumThumbnailWidthForEditor;
		Point size = DisplayUtils.getDisplayPixelSize(context);
		int screenWidth = size.x;
		int screenHeight = size.y;
		maximumThumbnailWidthForEditor = (screenWidth > screenHeight) ? screenHeight : screenWidth;
		int padding = DisplayUtils.dpToPx(context, 48) * 2;
		maximumThumbnailWidthForEditor -= padding;
		return maximumThumbnailWidthForEditor;
	}

	public static boolean isSquare(Uri imageUri) {
		ContentResolver resolver = StoryApp.getContext().getContentResolver();

		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		try {
			BitmapFactory.decodeStream(resolver.openInputStream(imageUri), null, options);
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
		return options.outHeight == options.outWidth;
	}

	public static String saveToFile(String fileFolderStr, boolean isDir, Bitmap croppedImage) throws FileNotFoundException, IOException {
		File jpgFile;
		if (isDir) {
			File fileFolder = new File(fileFolderStr);
			Date date = new Date();
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
			String fileName = format.format(date) + ".jpg";

			if (!fileFolder.exists()) {
				FileUtils.getInstance().mkdir(fileFolder);
			}
			jpgFile = new File(fileFolder, fileName);
		} else {
			jpgFile = new File(fileFolderStr);
			if (!jpgFile.getParentFile().exists()) {
				FileUtils.getInstance().mkdir(jpgFile.getParentFile());
			}
		}
		FileOutputStream outStream = new FileOutputStream(jpgFile);
		croppedImage.compress(Bitmap.CompressFormat.JPEG, 70, outStream);
		FileUtils.closeStream(outStream);

		return jpgFile.getPath();
	}

    public static float getImageRatio(ContentResolver resolver, Uri fileUri) {
        InputStream inStream = null;
        try {
            inStream = resolver.openInputStream(fileUri);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(inStream, null, options);
            int initWidth = options.outWidth;
            int initHeight = options.outHeight;
            float ratio = initHeight > initWidth ? (float) initHeight / (float) initWidth : (float) initWidth / (float) initHeight;
            return ratio;
        } catch (Exception ex) {
            ex.printStackTrace();
            return 1;
        } finally {
            FileUtils.closeStream(inStream);
        }
    }

    public static Map<String,Album> findGalleries(Context context, List<String> paths, long babyId) {
        paths.clear();
        paths.add(FileUtils.getInstance().getSystemPhotoPath());
        String[] projection = { MediaStore.Images.Media._ID, MediaStore.Images.Media.DATA, MediaStore.Images.Media.DATE_ADDED};
        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection,
                MediaStore.Images.Media.SIZE + ">?", new String[] { "100000"}, MediaStore.Images.Media.DATE_ADDED + " desc");
        cursor.moveToFirst();
        Map<String, Album> galleries = new HashMap<>();
        while (cursor.moveToNext()) {
            String data = cursor.getString(1);
            if (data.lastIndexOf("/") < 1) {
                continue;
            }
            String sub = data.substring(0, data.lastIndexOf("/"));
            if (!galleries.keySet().contains(sub)) {
                String name = sub.substring(sub.lastIndexOf("/") + 1, sub.length());
                if (!paths.contains(sub)) {
                    paths.add(sub);
                }
                galleries.put(sub, new Album(name, sub, new ArrayList<Photo>()));
            }
            Date addedDate = new Date((long) (cursor.getInt(2) * 1000));
            galleries.get(sub).getPhotos().add(new Photo(data, addedDate));
        }
        cursor.close();

        ArrayList<Photo> sysPhotos = FileUtils.getInstance().findPicsInDir(FileUtils.getInstance().getSystemPhotoPath());
        if (!sysPhotos.isEmpty()) {
            galleries.put(FileUtils.getInstance().getSystemPhotoPath(), new Album("StickerCamera", FileUtils.getInstance()
                    .getSystemPhotoPath(), sysPhotos));
        } else {
            galleries.remove(FileUtils.getInstance().getSystemPhotoPath());
            paths.remove(FileUtils.getInstance().getSystemPhotoPath());
        }
        return galleries;
    }

    public static Bitmap decodeBitmapWithOrientation(String pathName, int width, int height) {
        return decodeBitmapWithSize(pathName, width, height, false);
    }

    public static Bitmap decodeBitmapWithOrientationMax(String pathName, int width, int height) {
        return decodeBitmapWithSize(pathName, width, height, true);
    }

    public static Bitmap decodeBitmapWithSize(String pathName, int width, int height, boolean useBigger) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inInputShareable = true;
        options.inPurgeable = true;
        BitmapFactory.decodeFile(pathName, options);

        int decodeWidth = width, decodeHeight = height;
        final int degrees = getImageDegrees(pathName);
        if (degrees == 90 || degrees == 270) {
            decodeWidth = height;
            decodeHeight = width;
        }

        if (useBigger) {
            options.inSampleSize = (int) Math.min(((float) options.outWidth / decodeWidth), ((float) options.outHeight/ decodeHeight));
        } else {
            options.inSampleSize = (int) Math.max(((float) options.outWidth / decodeWidth), ((float) options.outHeight / decodeHeight));
        }

        options.inJustDecodeBounds = false;
        Bitmap sourceBitmap = BitmapFactory.decodeFile(pathName, options);
        return imageWithFixedRotation(sourceBitmap, degrees);
    }

    public static Bitmap imageWithFixedRotation(Bitmap bitmap, int degrees) {
        if (bitmap == null || bitmap.isRecycled()) {
            return null;
        }

        if (degrees == 0) {
            return bitmap;
        }

        final Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        Bitmap result = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        if (result != bitmap) {
            bitmap.recycle();
        }
        return result;
    }

    public static int getImageDegrees(String pathName) {
        int degrees = 0;
        try {
            ExifInterface exifInterface = new ExifInterface(pathName);
            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90: {
                    degrees = 90;
                    break;
                }
                case ExifInterface.ORIENTATION_ROTATE_180: {
                    degrees = 180;
                    break;
                }
                case ExifInterface.ORIENTATION_ROTATE_270: {
                    degrees = 270;
                    break;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return degrees;
    }
}
