/**
 * Copyright (C)  2015 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 * Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 * @author: Larry Pham (email: larrypham.vn@gmail.com)
 * @date: 7/15/15 8:46 AM
 **/

package co.soml.android.app.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import co.soml.android.app.R;
import co.soml.android.app.StoryApp;
import co.soml.android.app.activities.InvalidateParam;
import co.soml.android.app.activities.UserProfileActivity;
import co.soml.android.app.controllers.ControllerMessage;
import co.soml.android.app.controllers.PeopleController;
import co.soml.android.app.controllers.UserProfileController;
import co.soml.android.app.datasets.StoryDatabase;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.models.StoryFeedList;
import co.soml.android.app.utils.CommonUtil;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.widgets.ProgressWheel;
import co.soml.android.app.widgets.StoryRecyclerView;
import co.soml.android.app.widgets.StoryTextView;

public class ProfileStoriesFragment extends BaseFragment {
	public static final String TAG = LogUtils.makeLogTag(ProfileStoriesFragment.class.getSimpleName());
	private UserProfileController mController;
	private StoryApp mApp;
	private StoryDataModel mDataModel;
	private UserProfileActivity mActivity;
    private Bundle mArguments;

	private View mRootView;
	private View mLoadingView;
	private ProgressWheel mLoadingMore;
	private View mEmptyView;


	private StoryTextView mEmptyTitleView;
	private StoryTextView mEmptyDescriptionView;
	private StoryRecyclerView mProfileRecyclerView;

	private float mDipScale;
	private int mDensity;
    private StoryFeedList mStories;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		this.mActivity = (UserProfileActivity) getActivity();
		this.mApp = (StoryApp) mActivity.getApplicationContext();
		this.mDataModel = (StoryDataModel) mApp.getAppDataModel();
        this.mArguments = getArguments();
		mDipScale = getResources().getDisplayMetrics().density;
		mDensity = CommonUtil.getDensity(mActivity);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		LogUtils.LOGD(TAG, "onCreateView()");
		if (mController == null) {
			mController = new UserProfileController(mActivity, this, mDataModel);
		}

		mRootView = inflater.inflate(R.layout.layout_profile_subview, container, false);
		mEmptyView = mRootView.findViewById(R.id.empty_view);
		mProfileRecyclerView = (StoryRecyclerView) mRootView.findViewById(R.id.user_profile_recyclerview);
		mLoadingView = mRootView.findViewById(R.id.loading_more_view);
		mLoadingMore = (ProgressWheel) mLoadingView.findViewById(R.id.loading_progress_view);
		mEmptyTitleView = (StoryTextView) mEmptyView.findViewById(R.id.empty_view_title);
		mEmptyDescriptionView = (StoryTextView) mEmptyView.findViewById(R.id.empty_view_description);
		return mRootView;
	}

	@Override
	public void invalidate() {

	}

	@Override
	public void invalidate(Object... params) {
		final InvalidateParam param = (InvalidateParam) params[0];
		mProfileRecyclerView = (StoryRecyclerView) mRootView.findViewById(R.id.user_profile_recyclerview);
		switch (param.getMessage()) {
			case ControllerMessage.REQUEST_TO_SERVER_LIST_STORIES: {
				LogUtils.LOGD(TAG, "Invalidating the recyclerview by using the list view");
				break;
			}
		}
	}
}
