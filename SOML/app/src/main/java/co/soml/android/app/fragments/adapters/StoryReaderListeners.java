/*
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 * @author: Larry Pham (email: larrypham.vn@gmail.com)
 */

package co.soml.android.app.fragments.adapters;

import android.view.View;

import co.soml.android.app.models.Story;

public class StoryReaderListeners {

    public static interface OnStorySelectedListener {
        public void onStorySelected(long storyId);
    }

    /**
     * Called when user choose one of their story's types
     */
    public static interface OnStoryTypeSelectedListener {
        public void onStoryTypeSelected(String typeName);
    }

    /**
     * Called from story detail fragment so toolbar can animate in/out when scrolling
     */
    public static interface AutoHideToolbarListener {
        public void onShowHideToolbar(boolean show);
    }

    /**
     * Called when user taps the dropdown arrow next to a story to show the popup menu
     */
    public static interface OnStoryPopupListener {
        public void onShowStoryPopup(View view, Story story);
    }

    /**
     * Used by adapters to notify when the data for StoryList has been loaded
     */
    public interface DataLoadedListener {
        public void onDataLoaded(boolean isEmpty);
    }

    /**
     * Used by StoryList & StoryList adapter when user asks to repost a existed story.
     */
    public interface RequestReportStoryListener {
        public void onRequestReport(Story story, View sourceView);
    }
}
