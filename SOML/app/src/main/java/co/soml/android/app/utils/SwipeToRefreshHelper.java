/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 * @author: Larry Pham (email: larrypham.vn@gmail.com)
 * Date: 6/9/15 2:29 PM
 **/

package co.soml.android.app.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.TypedValue;

import co.soml.android.app.R;

public class SwipeToRefreshHelper implements SwipeRefreshLayout.OnRefreshListener {
    public static final String TAG = LogUtils.makeLogTag(SwipeToRefreshHelper.class.getSimpleName());

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RefreshListener mRefreshListener;
    private boolean mRefreshing;

    public interface RefreshListener {
        void onRefreshStarted();
    }

    public SwipeToRefreshHelper(Context context, SwipeRefreshLayout swipeRefreshLayout, RefreshListener listener) {
        init(context, swipeRefreshLayout, listener);
    }

    public void init(Context context, SwipeRefreshLayout swipeRefreshLayout, RefreshListener listener) {
        mSwipeRefreshLayout = swipeRefreshLayout;
        mRefreshListener = listener;
        mSwipeRefreshLayout.setOnRefreshListener(this);
        final TypedArray styles = obtainStyledAttributesFromThemeAttr(context, R.attr.swipeToRefreshStyle, R.styleable.RefreshIndicator);
        int color = styles.getColor(R.styleable.RefreshIndicator_refreshIndicatorColor, R.color.color_primary);
        mSwipeRefreshLayout.setColorSchemeColors(color, color, color, color);
    }

    public void setRefreshing(boolean refreshing) {
        if (refreshing) {
            mSwipeRefreshLayout.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(mRefreshing);
                }
            }, 50);
        } else {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    public boolean isRefreshing() {
        return mSwipeRefreshLayout.isRefreshing();
    }

    @Override
    public void onRefresh() {
        mRefreshListener.onRefreshStarted();
    }

    public void setEnabled(boolean enabled) {
        mSwipeRefreshLayout.setEnabled(enabled);
    }

    public static TypedArray obtainStyledAttributesFromThemeAttr(Context context, int themeAttr, int[] styleAttrs) {
        TypedValue outValue = new TypedValue();
        context.getTheme().resolveAttribute(themeAttr, outValue, true);
        int styleResId = outValue.resourceId;
        return context.obtainStyledAttributes(styleResId, styleAttrs);
    }
}
