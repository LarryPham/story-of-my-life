package co.soml.android.app.utils;

import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

import java.util.Dictionary;
import java.util.Hashtable;
import java.util.regex.Pattern;

import co.soml.android.app.AppConstants;
import co.soml.android.app.StoryApp;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.17.2015
 * <p>
 * A assortment of UI helpers.
 */
public class UIUtils {
    private static final String TAG = AppConstants.PREFIX + UIUtils.class.getSimpleName();
    /**
     * Factor applied to session color to derive the background color on panels and when
     * a session photo could not be downloaded (or while it is being downloaded)
     */
    public static final float SESSION_BACKGROUND_COLOR_OR_SCALE_FACTOR = 0.75f;

    private static final float SESSION_PHOTO_SCRIM_ALPHA = 0.25f;
    private static final float SESSION_PHOTO_SCRIM_SATURATION = 0.2f;
    private static final String TARGET_FROM_FACTOR_ACTIVITY_METADATA = "co.soml.android.app.StoryLife.meta.TARGET_FROM_FACTOR";

    private static final String TARGET_FROM_FACTOR_HANDSET = "handset";
    private static final String TARGET_FROM_FACTOR_TABLET = "tablet";

    private static final int HIGHLIGHT_MODE_PRESSEd = 2;
    private static final int HIGHLIGHT_MODE_CHECKED = 4;
    private static final int HIGHLIGHT_MODE_SELECTED = 8;

    public static final int GLOW_MODE_PRESSED = 2;
    public static final int GLOW_MODE_CHECKED = 4;
    public static final int GLOW_MODE_SELECTED = 8;

    private static TypedValue sTypedValue = new TypedValue();
    private static int sActionBarHeight;

    private static Dictionary<Integer, Integer> sListViewItemHeights = new Hashtable<Integer, Integer>();
    private static Dictionary<Integer, Integer> sRecyclerViewItemHeights = new Hashtable<Integer, Integer>();

    public static Point sDisplaySize = new Point();
    public static DisplayMetrics mDisplayMetrics = new DisplayMetrics();
    private static final int TIME_FLAGS = DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_WEEKDAY | DateUtils.FORMAT_ABBREV_WEEKDAY;

    private static final Pattern REGEX_HTML_ESCAPE = Pattern.compile(".*&\\S;.*");
    private static float mDensity = 1;
    static {
        mDensity = StoryApp.getContext().getResources().getDisplayMetrics().density;
        checkDisplaySize();
    }

    public static void setAccessibilityIgnore(View view) {
        view.setClickable(false);
        view.setFocusable(false);
        view.setContentDescription("");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            view.setImportantForAccessibility(View.IMPORTANT_FOR_ACCESSIBILITY_NO);
        }
    }

    public static boolean checkBits(int status, int checkBit) {
        return (status & checkBit) == checkBit;
    }

    public static int getScrollY(RecyclerView rv, int columnCount) {
        View c = rv.getChildAt(0);
        if (c == null) {
            return 0;
        }

        LinearLayoutManager layoutManager = (LinearLayoutManager)rv.getLayoutManager();
        int firstVisiblePosition = layoutManager.findFirstVisibleItemPosition();
//        Log.d("", "getScrollY() : firstVisiblePosition - " + firstVisiblePosition);

        int scrollY = -(c.getTop());

//        Log.d("", "getScrollY() : scrollY - " + scrollY);

        if(columnCount > 1){
            sRecyclerViewItemHeights.put(firstVisiblePosition, c.getHeight() + UIUtils.dp2px(rv.getContext(), 8)/columnCount);
        } else {
            sRecyclerViewItemHeights.put(firstVisiblePosition, c.getHeight());
        }

        if(scrollY<0)
            scrollY = 0;

        for (int i = 0; i < firstVisiblePosition; ++i) {
            if (sRecyclerViewItemHeights.get(i) != null) // (this is a sanity check)
                scrollY += sRecyclerViewItemHeights.get(i); //add all heights of the views that are gone
        }

        return scrollY;
    }

    public static int px2dp(Context context, int px) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        DisplayMetrics displaymetrics = new DisplayMetrics();
        display.getMetrics(displaymetrics);

        return (int) (px / displaymetrics.density + 0.5f);
    }

    public static int dp2px(Context context, int dp) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        DisplayMetrics displaymetrics = new DisplayMetrics();
        display.getMetrics(displaymetrics);

        return (int) (dp * displaymetrics.density + 0.5f);
    }

    public static void checkDisplaySize() {
        try {
            WindowManager manager = (WindowManager) StoryApp.getContext().getSystemService(Context.WINDOW_SERVICE);
            if (manager != null) {
                Display display = manager.getDefaultDisplay();
                if (display != null) {
                    display.getMetrics(mDisplayMetrics);
                    if (Build.VERSION.SDK_INT < 13) {
                        sDisplaySize.set(display.getWidth(), display.getHeight());
                    } else {
                        display.getSize(sDisplaySize);
                    }
                    LogUtils.LOGE(TAG, "DisplaySize = " + sDisplaySize.x + " " + sDisplaySize.y + " " + mDisplayMetrics.xdpi + "x" +
                            mDisplayMetrics.ydpi);
                }
            }
        } catch (Exception ex) {
            LogUtils.LOGE(TAG, ex.toString());
        }
    }

    public static int dp(float value) {
        return (int) Math.ceil(mDensity * value);
    }

    public static float dpf2(float value) {
        return mDensity * value;
    }
}
