package co.soml.android.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import co.soml.android.app.AppConstants;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its
 * documentation are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable
 * form without the prior written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with
 * respect to the contents, and assumes no responsibility for any errors that might appear in the software and
 * documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.03.2015
 */
public class Story implements Parcelable, Serializable {
    public static final String TAG = AppConstants.PREFIX + Story.class.getSimpleName();

    @Expose
    @SerializedName("id")
    private int mId;
    @Expose
    @SerializedName("user_id")
    private int mUserId;
    @Expose
    @SerializedName("name")
    private String mName;
    @Expose
    @SerializedName("interval")
    private int mInterval;
    @Expose
    @SerializedName("description")
    private String mDescription;
    @Expose
    @SerializedName("latitude")
    private String mLatitude;
    @Expose
    @SerializedName("longitude")
    private String mLongitude;
    @Expose
    @SerializedName("state")
    private String mState;
    @Expose
    @SerializedName("created_at")
    private Date mCreatedAt;
    @Expose
    @SerializedName("updated_at")
    private Date mUpdatedAt;
    @Expose
    @SerializedName("deleted_at")
    private Date mDeletedAt;
    @Expose
    @SerializedName("location")
    private String mLocation;
    @Expose
    @SerializedName("photo_order")
    private int[] mPhotoOrder;
    @Expose
    @SerializedName("cover_id")
    private long mCoverId;
    @Expose
    @SerializedName("view_count")
    private int mViewCount;
    @Expose
    @SerializedName("slug")
    private String mSlug;
    @Expose
    @SerializedName("playback_interval")
    private int mPlaybackInterval;
    @Expose
    @SerializedName("song_id")
    private int mSongId;
    @Expose
    @SerializedName("likes_count")
    private int mLikesCount;
    @Expose
    @SerializedName("comments_count")
    private int mCommentsCount;
    @Expose
    @SerializedName("photos")
    private List<Photo> mPhoto;
    @Expose
    @SerializedName("user")
    private User mUser;
    @Expose
    @SerializedName("share_link")
    private String mShareLink;

    private boolean mIsFollowedByCurrentUser;
    @Expose
    @SerializedName("is_liked_by_current_user")
    private boolean mIsLikeByCurrentUser;

    private int mPhotosLength;
    public boolean mIsPrivate;
    public boolean mIsFollowing;
    public int mNumSubscribers;
    private boolean mIsUploading;
    private boolean mUploaded;

    public Story() {

    }

    public Story(Story story) {
        this.mId = story.getId();
        this.mUserId = story.getUserId();
        this.mName = story.getName();
        this.mInterval = story.getInterval();
        this.mDescription = story.getDescription();
        this.mLatitude = story.getLatitude();
        this.mLongitude = story.getLongitude();
        this.mCreatedAt = story.getCreatedAt();
        this.mUpdatedAt = story.getUpdatedAt();
        this.mDeletedAt = story.getDeletedAt();
        this.mLocation = story.getLocation();
        this.mPhotoOrder = story.getPhotoOrder();
        this.mCoverId = story.getCoverId();
        this.mViewCount = story.getViewCount();
        this.mSlug = story.getSlug();
        this.mPlaybackInterval = story.getPlaybackInterval();
        this.mSongId = story.getSongId();
        this.mLikesCount = story.getLikesCount();
        this.mCommentsCount = story.getCommentsCount();
        this.mPhoto = story.getPhoto();
        this.mUser = story.getUser();
        this.mShareLink = story.getShareLink();
        this.mIsLikeByCurrentUser = story.isLikeByCurrentUser();
        this.mPhotosLength = story.getPhoto().size();
    }

    public Story(Parcel source) {
        readFromParcel(source);
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getUserId() {
        return mUserId;
    }

    public void setUserId(int userId) {
        mUserId = userId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public int getInterval() {
        return mInterval;
    }

    public void setInterval(int interval) {
        mInterval = interval;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getLatitude() {
        return mLatitude;
    }

    public void setLatitude(String latitude) {
        mLatitude = latitude;
    }

    public String getLongitude() {
        return mLongitude;
    }

    public void setLongitude(String longitude) {
        mLongitude = longitude;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

    public Date getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(Date createdAt) {
        mCreatedAt = createdAt;
    }

    public Date getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        mUpdatedAt = updatedAt;
    }

    public Date getDeletedAt() {
        return mDeletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        mDeletedAt = deletedAt;
    }

    public String getLocation() {
        return mLocation;
    }

    public void setLocation(String location) {
        mLocation = location;
    }

    public int[] getPhotoOrder() {
        return mPhotoOrder;
    }

    public void setPhotoOrder(int[] photoOrder) {
        mPhotoOrder = photoOrder;
    }

    public long getCoverId() {
        return mCoverId;
    }

    public void setCoverId(long coverId) {
        mCoverId = coverId;
    }

    public int getViewCount() {
        return mViewCount;
    }

    public void setViewCount(int viewCount) {
        mViewCount = viewCount;
    }

    public String getSlug() {
        return mSlug;
    }

    public void setSlug(String slug) {
        mSlug = slug;
    }

    public int getPlaybackInterval() {
        return mPlaybackInterval;
    }

    public void setPlaybackInterval(int playbackInterval) {
        mPlaybackInterval = playbackInterval;
    }

    public int getSongId() {
        return mSongId;
    }

    public void setSongId(int songId) {
        mSongId = songId;
    }

    public int getLikesCount() {
        return mLikesCount;
    }

    public void setLikesCount(int likesCount) {
        mLikesCount = likesCount;
    }

    public int getCommentsCount() {
        return mCommentsCount;
    }

    public void setCommentsCount(int commentsCount) {
        mCommentsCount = commentsCount;
    }

    public List<Photo> getPhoto() {
        return mPhoto;
    }

    public void setPhoto(List<Photo> photo) {
        mPhoto = photo;
    }

    public User getUser() {
        return mUser;
    }

    public void setUser(User user) {
        mUser = user;
    }

    public String getShareLink() {
        return mShareLink;
    }

    public void setShareLink(String shareLink) {
        mShareLink = shareLink;
    }

    public boolean isLikeByCurrentUser() {
        return mIsLikeByCurrentUser;
    }

    public void setLikeByCurrentUser(boolean isLikeByCurrentUser) {
        mIsLikeByCurrentUser = isLikeByCurrentUser;
    }

    public boolean hasUserName() {
        return !TextUtils.isEmpty(mUser.getFullName());
    }

    public boolean hasName() {
        return !TextUtils.isEmpty(this.getName());
    }

    public boolean hasDescription() {
        return !TextUtils.isEmpty(this.getDescription());
    }

    public boolean isFollowedByCurrentUser() {
        return mIsFollowedByCurrentUser;
    }

    public void setIsFollowedByCurrentUser(boolean isFollowedByCurrentUser) {
        mIsFollowedByCurrentUser = isFollowedByCurrentUser;
    }

    public boolean isUploading() {
        return mIsUploading;
    }

    public void setUploading(boolean isUploading) {
        mIsUploading = isUploading;
    }

    public boolean isUploaded() {
        return mUploaded;
    }

    public void setUploaded(boolean uploaded) {
        mUploaded = uploaded;
    }

    public void increaseLikeCount() {
        this.mLikesCount = mLikesCount + 1;
    }

    public void decreaseLikeCount() {
        if (mLikesCount >= 1) {
            this.mLikesCount = mLikesCount - 1;
        }
    }

    public boolean isSameAs(Story story) {
        return story != null && this.mId == story.mId && this.mCoverId == story.mCoverId
                && this.mUserId == story.mUserId && this.getName().equals(story.getName())
                && this.getDescription().equals(story.getDescription())
                && this.getCreatedAt().equals(story.getCreatedAt())
                && this.getUpdatedAt().equals(story.getUpdatedAt())
                && this.getDeletedAt().equals(story.getDeletedAt());

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.mId);
        dest.writeLong(mUserId);
        dest.writeString(mName);
        dest.writeInt(mInterval);
        dest.writeString(mDescription);

        dest.writeString(mLatitude);
        dest.writeString(mLongitude);
        dest.writeString(mState);
        dest.writeSerializable(mCreatedAt);
        dest.writeSerializable(mUpdatedAt);
        dest.writeSerializable(mDeletedAt);
        dest.writeString(mLocation);
        dest.writeIntArray(mPhotoOrder);
        dest.writeLong(mCoverId);
        dest.writeInt(mViewCount);
        dest.writeString(mSlug);
        dest.writeInt(mPlaybackInterval);
        dest.writeInt(mSongId);
        dest.writeInt(mLikesCount);
        dest.writeInt(mCommentsCount);
        dest.writeInt(mPhotosLength);

        dest.writeByte((byte) (mIsFollowedByCurrentUser ? 1 : 0));
    }

    public void readFromParcel(Parcel source) {
        this.mId = source.readInt();
        this.mUserId = source.readInt();
        this.mName = source.readString();
        this.mInterval = source.readInt();
        this.mDescription = source.readString();

        this.mLatitude = source.readString();
        this.mLongitude = source.readString();
        this.mState = source.readString();

        this.mCreatedAt = (Date) source.readSerializable();
        this.mUpdatedAt = (Date) source.readSerializable();
        this.mDeletedAt = (Date) source.readSerializable();

        this.mLocation = source.readString();
        this.mPhotoOrder = source.createIntArray();
        this.mCoverId = source.readInt();
        this.mViewCount = source.readInt();
        this.mSlug = source.readString();
        this.mPlaybackInterval = source.readInt();
        this.mSongId = source.readInt();
        this.mLikesCount = source.readInt();
        this.mCommentsCount = source.readInt();
        this.mPhotosLength = source.readInt();
        int followedByUser = source.readByte();
        if (followedByUser == 1) {
            mIsFollowedByCurrentUser = true;
        } else {
            mIsFollowedByCurrentUser = false;
        }
    }

    public static final Creator<Story> CREATOR = new Creator<Story>() {
        @Override
        public Story createFromParcel(Parcel source) {
            return new Story(source);
        }

        @Override
        public Story[] newArray(int size) {
            return new Story[size];
        }
    };

    private transient long stableId;

    public long getStableId() {
        if (stableId == 0) {
            stableId = (mId != -1 ? (long) mId : 0);
        }
        return stableId;
    }
}
