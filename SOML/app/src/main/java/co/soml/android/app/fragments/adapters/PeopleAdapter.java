/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 * @author: Larry Pham (email: larrypham.vn@gmail.com)
 * Date: 6/18/15 3:51 PM
 **/

package co.soml.android.app.fragments.adapters;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.squareup.picasso.Picasso;

import co.soml.android.app.R;
import co.soml.android.app.StoryApp;
import co.soml.android.app.activities.AppInterfaces;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.models.StoryUserList;
import co.soml.android.app.models.User;
import co.soml.android.app.utils.CommonUtil;
import co.soml.android.app.widgets.CircleImageView;
import co.soml.android.app.widgets.StoryFollowButton;
import co.soml.android.app.widgets.StoryRecyclerView;
import co.soml.android.app.widgets.StoryTextView;

public class PeopleAdapter extends StoryRecyclerView.Adapter<PeopleAdapter.PeopleViewHolder> implements View.OnClickListener {

    public static final int VIEW_TYPE_DEFAULT = 1;
    public static final int VIEW_TYPE_LOADER = 2;

    private Context mContext;
    private StoryUserList mUsers = new StoryUserList();
    private boolean mShowLoadingView = false;
    private StoryDataModel.UsersType mUsersType;
    private StoryApp mStoryApp;

    public AppInterfaces.FollowUserListener mFollowUserListener;
    public AppInterfaces.UnfollowUserListener mUnfollowUserListener;

    public AppInterfaces.FollowUserListener getFollowUserListener() {
        return mFollowUserListener;
    }

    public void setFollowUserListener(AppInterfaces.FollowUserListener followUserListener) {
        mFollowUserListener = followUserListener;
    }

    public AppInterfaces.UnfollowUserListener getUnfollowUserListener() {
        return mUnfollowUserListener;
    }

    public void setUnfollowUserListener(AppInterfaces.UnfollowUserListener unfollowUserListener) {
        mUnfollowUserListener = unfollowUserListener;
    }

    public PeopleAdapter(Context context, StoryDataModel.UsersType type, StoryUserList users) {
        super();
        mContext = context;
        mStoryApp = (StoryApp) mContext.getApplicationContext();
        mUsers = users;

        mUsersType = type;
        int displayWidth = CommonUtil.getScreenWidth(mContext);
        int displayHeight = CommonUtil.getScreenHeight(mContext);
        setHasStableIds(true);
    }

    public StoryUserList getUsers() {
        return mUsers;
    }


    class PeopleViewHolder extends RecyclerView.ViewHolder {
        private final StoryTextView mUserName;
        private final CircleImageView mUserProfile;
        private final StoryFollowButton mUserFollow;
        private ViewGroup mPeopleContainer;

        public PeopleViewHolder(View itemView) {
            super(itemView);

            mUserName = (StoryTextView) itemView.findViewById(R.id.user_name);
            mUserProfile = (CircleImageView) itemView.findViewById(R.id.user_profile);
            mUserFollow = (StoryFollowButton) itemView.findViewById(R.id.user_follow);
            mPeopleContainer = (ViewGroup) itemView.findViewById(R.id.layout_people_container);
        }
    }

    @Override
    public PeopleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.story_users_short_item, parent, false);
        PeopleViewHolder cellViewHolder = new PeopleViewHolder(view);
        if (viewType == VIEW_TYPE_DEFAULT) {
            cellViewHolder.mPeopleContainer.setOnClickListener(this);
        } else if (viewType == VIEW_TYPE_LOADER) {
            View backgroundView = new View(mContext);
            backgroundView.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            backgroundView.setBackgroundColor(mContext.getResources().getColor(R.color.color_background));
            cellViewHolder.mPeopleContainer.addView(backgroundView);
        }
        return cellViewHolder;
    }

    @Override
    public void onBindViewHolder(PeopleViewHolder holder, int position) {
        runEnterAnimation(holder.itemView, position);
        final PeopleViewHolder viewHolder = (PeopleViewHolder) holder;
        if (getItemViewType(position) == VIEW_TYPE_DEFAULT) {
            bindDefaultPeopleItem(position, holder);
        } else if (getItemViewType(position) == VIEW_TYPE_LOADER) {
            bindPeopleItem(position, holder);
        }
    }

    private void bindPeopleItem(int position, PeopleViewHolder holder) {
        final User user = mUsers.get(position);
        StoryDataModel.UsersType usersType = getUsersType();
    }

    private void bindDefaultPeopleItem(int position, PeopleViewHolder holder) {
        final User user = mUsers.get(position);
        StoryDataModel.UsersType usersType = getUsersType();
        if (usersType == StoryDataModel.UsersType.FOLLOWING) {
            if (!TextUtils.isEmpty(user.getFullName())) {
                holder.mUserName.setText(user.getFullName());
            } else {
                holder.mUserName.setVisibility(View.GONE);
            }

            Picasso.with(mContext).load(user.getAvatarSmallUrl()).placeholder(R.drawable.person_image_empty).into(holder.mUserProfile);
        } else if (mUsersType == StoryDataModel.UsersType.FOLLOWERS) {
            if (!TextUtils.isEmpty(user.getFullName())) {
                holder.mUserName.setText(user.getFullName());
            } else {
                holder.mUserName.setVisibility(View.GONE);
            }
            Picasso.with(mContext).load(user.getAvatarSmallUrl()).placeholder(R.drawable.person_image_empty).into(holder.mUserProfile);
        }

        if (user.isFollowedByCurrentUser()) {
            holder.mUserFollow.setSelected(true);
            holder.mUserFollow.setIsFollowed(true);
        } else {
            holder.mUserFollow.setSelected(false);
            holder.mUserFollow.setIsFollowed(false);
        }
        holder.mUserFollow.setTag(user.getId());
        if (user.isFollowedByCurrentUser()) {
            holder.mUserFollow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mUnfollowUserListener != null) {
                        mUnfollowUserListener.onUnfollowUser(view);
                    }
                }
            });
        } else {
            holder.mUserFollow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mFollowUserListener != null) {
                        mFollowUserListener.onFollowUser(view);
                    }
                }
            });
        }
    }

    public StoryDataModel.UsersType getUsersType() {
        return mUsersType != null ? mUsersType : StoryDataModel.UsersType.FOLLOWERS;
    }

    public void runEnterAnimation(View view, int position) {

    }

    @Override
    public int getItemCount() {
        return mUsers.size();
    }

    @Override
    public long getItemId(int position) {
        return mUsers.get(position).getId();
    }

    @Override
    public int getItemViewType(int position) {
        if (mShowLoadingView && position == 0) {
            return VIEW_TYPE_LOADER;
        } else {
            return VIEW_TYPE_DEFAULT;
        }
    }

    public void showLoadingView() {
        mShowLoadingView = true;
        notifyItemChanged(0);
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }

    public void setUsers(StoryUserList users) {
        this.mUsers = users;
        this.notifyDataSetChanged();
    }

    public void clear() {
        if (mUsers.isEmpty()) {
            mUsers.clear();
            notifyDataSetChanged();
        }
    }


    @Override
    public void onClick(View view) {

    }

    private boolean mIsRunningTask = false;

    private class LoadingUsersTask extends AsyncTask<Void, Void, Boolean> {

        StoryUserList mUserList;

        @Override
        protected void onPreExecute() {
            mIsRunningTask = true;
        }

        @Override
        protected void onCancelled() {
            mIsRunningTask = false;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            switch (getUsersType()) {
                case FOLLOWING: {
                    mUserList = mStoryApp.getAppDataModel().getFollowingUsers();
                    return !mUsers.isSameList(mUserList);
                }
                case FOLLOWERS: {
                    mUserList = mStoryApp.getAppDataModel().getFollowers();
                    return !mUsers.isSameList(mUserList);
                }
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (result) {
                switch (getUsersType()) {
                    case FOLLOWERS: {
                        mUsers = mUserList;
                        break;
                    }
                    case FOLLOWING: {
                        mUsers = mUserList;
                        break;
                    }
                }
                notifyDataSetChanged();
            }
            mIsRunningTask = false;
        }
    }
}
