package co.soml.android.app.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ViewGroup;

import java.util.HashMap;

import co.soml.android.app.AppConstants;
import co.soml.android.app.activities.BaseActivity;
import co.soml.android.app.controllers.BaseController;
import co.soml.android.app.models.StoryDataModel;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its
 * documentation are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable
 * form without the prior written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with
 * respect to the contents, and assumes no responsibility for any errors that might appear in the software and
 * documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.01.2015
 */
public abstract class BaseFragment extends Fragment {
    public static final String TAG = AppConstants.PREFIX + BaseFragment.class.getSimpleName();
    protected BaseController mController = null;
    protected StoryDataModel mDataModel;
    protected BaseActivity mActivity;
    protected Bundle mBundle = new Bundle();
    protected HashMap<String, Object> mTags;

    protected String mFragmentKey;
    private boolean mReceiveKeyboardState;

    protected ViewGroup mRootView;


    protected boolean needElevatedActionBar() {
        return true;
    }

    /**
     * Sets the Extras Bundle to an instance of {@link co.soml.android.app.fragments.BaseFragment} class,
     * which will be assigned by something variables that instance used to handle something into this screen
     *
     * @param extras The Bundle object
     */
    public void setBundle(Bundle extras) {
        mBundle = extras;
    }

    /**
     * Gets the Extras Bundle object which was integrated into the current fragment object
     *
     * @return The Bundle Object.
     */
    public Bundle getBundle() {
        return this.mBundle;
    }

    /**
     * Sets the FragmentKey which has interacted with key that <code>BaseActivity</code> used to call the BaseFragment instance by them
     *
     * @param fragmentKey The FragmentKey String Type.
     */
    public void setFragmentKey(String fragmentKey) {
        mFragmentKey = fragmentKey;
    }

    /**
     * Gets the FragmentKey
     *
     * @return The Fragment's Key value
     */
    public String getFragmentKey() {
        return mFragmentKey;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    /**
     * Method used to display the keyboard onto current screen
     */
    protected void onKeyBoardShow() {

    }

    /**
     * Method used to hide the keyboard onto current screen, after checking the existence of keyboard's session onto current fragment screen.
     */
    protected void onKeyBoardHide() {

    }

    /**
     * Checking the Keyboard are showing or not.
     *
     * @return Boolean type.
     */
    public boolean isKeyboardShowing() {
        return false;
    }

    @Override
    public void onDestroy() {
        if (mController != null) {
            mController.onDestroy();
        }
        super.onDestroy();
    }

    /**
     * When the controller received the data as params that sent back from the application's object to them,
     * they will feed the new updates or something new after validating them to the BaseFragment screen by using this method,
     * By declaring the <code>InvalidateParams</code> for assigning new notified updates to current fragment
     */
    public abstract void invalidate();

    /**
     * Also same meaning with method above, but it will used with couple of objects that used to validate notificaitions from controller.
     *
     * @param params Objects
     */
    public abstract void invalidate(Object... params);
}
