package co.soml.android.app.models;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicReference;

import co.soml.android.app.AppConstants;
import co.soml.android.app.utils.LogUtils;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its
 * documentation are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable
 * form without the prior written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with
 * respect to the contents, and assumes no responsibility for any errors that might appear in the software and
 * documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.02.2015
 */
public class EntrySchema {
    public static final String TAG = AppConstants.PREFIX + EntrySchema.class.getSimpleName();
    public static final int TYPE_STRING = 0;
    public static final int TYPE_BOOLEAN = 1;
    public static final int TYPE_SHORT = 2;
    public static final int TYPE_INT = 3;
    public static final int TYPE_LONG = 4;
    public static final int TYPE_FLOAT = 5;
    public static final int TYPE_DOUBLE = 6;
    public static final int TYPE_BLOB = 7;
    public static final String SQLITE_TYPES[] = {"TEXT", "INTEGER", "INTEGER", "INTEGER", "INTEGER", "REAL", "REAL",
            "NONE"};
    public static final String FULL_TEXT_INDEX_SUFFIX = "_fulltext";
    private final String mTableName;
    private final ColumnInfo[] mColumnInfos;
    private final String[] mProjection;
    private final boolean mHasFullTextIndex;

    public EntrySchema(Class<? extends Entry> clazz) {
        ColumnInfo[] columns = parseColumnInfo(clazz);
        mTableName = parseTableName(clazz);
        mColumnInfos = columns;
        String[] projection = {};
        boolean hasFullTextIndex = false;
        if (columns != null) {
            projection = new String[columns.length];
            for (int i = 0; i != columns.length; i++) {
                ColumnInfo column = columns[i];
                projection[i] = column.mName;
                if (column.mFullText) {
                    hasFullTextIndex = true;
                }
            }
        }
        mProjection = projection;
        mHasFullTextIndex = hasFullTextIndex;
    }

    public String getTableName() {
        return mTableName;
    }

    public ColumnInfo[] getColumnInfos() {
        return mColumnInfos;
    }

    public String[] getProjection() {
        return mProjection;
    }

    private void logExecSql(SQLiteDatabase db, String sql) {
        db.execSQL(sql);
    }

    private String parseTableName(Class<? extends Object> clazz) {
        Entry.Table table = clazz.getAnnotation(Entry.Table.class);
        if (table == null) {
            return null;
        }
        return table.name();
    }

    private ColumnInfo[] parseColumnInfo(Class<? extends Object> clazz) {
        ArrayList<ColumnInfo> columnInfos = new ArrayList<ColumnInfo>();
        Field[] fields = clazz.getFields();
        for (Field field : fields) {
            Entry.Column info = ((AnnotatedElement) field).getAnnotation(Entry.Column.class);
            if (info == null) {
                continue;
            }
            int type;
            Class<?> fieldType = field.getType();
            if (fieldType == String.class) {
                type = TYPE_STRING;
            } else if (fieldType == boolean.class) {
                type = TYPE_BOOLEAN;
            } else if (fieldType == short.class) {
                type = TYPE_SHORT;
            } else if (fieldType == int.class) {
                type = TYPE_INT;
            } else if (fieldType == long.class) {
                type = TYPE_LONG;
            } else if (fieldType == float.class) {
                type = TYPE_FLOAT;
            } else if (fieldType == double.class) {
                type = TYPE_DOUBLE;
            } else if (fieldType == byte[].class) {
                type = TYPE_BLOB;
            } else {
                throw new IllegalArgumentException("Unknown type");
            }

            int index = columnInfos.size();
            columnInfos.add(new ColumnInfo(info.name(), type, info.indexed(), info.fullText(), field, index));
        }
        ColumnInfo[] columnList = new ColumnInfo[columnInfos.size()];
        columnInfos.toArray(columnList);
        return columnList;
    }

    public void logExecSQL(SQLiteDatabase db, String sql) {
        db.execSQL(sql);
    }

    public void cursorToObject(Cursor cursor, Entry object) {
        try {
            ColumnInfo[] columnInfos = mColumnInfos;
            for (int i = 0, size = columnInfos.length; i != size; i++) {
                ColumnInfo columnInfo = columnInfos[i];
                int columnIndex = columnInfo.mProjectionIndex;
                Field field = columnInfo.mField;
                switch (columnInfo.mType) {
                    case TYPE_STRING:
                        field.set(object, cursor.getString(columnIndex));
                        break;
                    case TYPE_BOOLEAN:
                        field.set(object, cursor.getShort(columnIndex) == 1);
                        break;
                    case TYPE_SHORT:
                        field.set(object, cursor.getShort(columnIndex));
                        break;
                    case TYPE_INT:
                        field.set(object, cursor.getInt(columnIndex));
                        break;
                    case TYPE_LONG:
                        field.setLong(object, cursor.getLong(columnIndex));
                        break;
                    case TYPE_FLOAT:
                        field.setFloat(object, cursor.getFloat(columnIndex));
                        break;
                    case TYPE_DOUBLE:
                        field.setDouble(object, cursor.getDouble(columnIndex));
                        break;
                    case TYPE_BLOB:
                        field.set(object, cursor.getBlob(columnIndex));
                        break;
                }
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            LogUtils.LOGE(TAG, "SchemasInfo.setFromCursor: object not of the right type");
        } catch (IllegalAccessException e) {
            LogUtils.LOGE(TAG, "SchemasInfo.setFromCursor: field not accessible");
        }
    }

    public void objectToValues(Entry object, ContentValues values) {
        try {
            ColumnInfo[] columnInfos = mColumnInfos;
            for (int i = 0, size = columnInfos.length; i != size; i++) {
                ColumnInfo column = columnInfos[i];
                String columnName = column.mName;
                Field field = column.mField;
                switch (column.mType) {
                    case TYPE_STRING:
                        values.put(columnName, (String) field.get(object));
                        break;
                    case TYPE_BOOLEAN:
                        values.put(columnName, field.getBoolean(object));
                        break;
                    case TYPE_SHORT:
                        values.put(columnName, field.getShort(object));
                        break;
                    case TYPE_INT:
                        values.put(columnName, field.getInt(object));
                        break;
                    case TYPE_LONG:
                        values.put(columnName, field.getLong(object));
                        break;
                    case TYPE_FLOAT:
                        values.put(columnName, field.getFloat(object));
                        break;
                    case TYPE_DOUBLE:
                        values.put(columnName, field.getDouble(object));
                        break;
                    case TYPE_BLOB:
                        values.put(columnName, (byte[]) field.get(object));
                        break;
                }
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            LogUtils.LOGE(TAG, "SchemasInfo.setFromCursor: object not of the right type");
        } catch (IllegalAccessException e) {
            LogUtils.LOGE(TAG, "SchemasInfo.setFromCursor: field not accessible");
        }
    }

    public Cursor queryAll(SQLiteDatabase db) {
        return db.query(mTableName, mProjection, null, null, null, null, null);
    }

    public boolean queryWidthId(SQLiteDatabase db, long id, Entry entry) {
        Cursor cursor = db.query(mTableName, mProjection, "_id=?", new String[]{Long.toString(id)}, null, null, null);
        boolean success = false;
        if (cursor.moveToFirst()) {
            cursorToObject(cursor, entry);
            success = true;
        }
        cursor.close();
        return success;
    }

    public long insertOrReplace(SQLiteDatabase db, Entry entry) {
        ContentValues values = new ContentValues();
        objectToValues(entry, values);
        if (entry.id == 0) {
            Log.i(TAG, String.format("Removing id(%d)before insert", entry.id));
            values.remove("_id");
        }

        long id = db.replace(mTableName, "_id", values);
        entry.id = id;
        return id;
    }

    public boolean deleteWithId(SQLiteDatabase db, long id) {
        return db.delete(mTableName, "_id=?", new String[]{Long.toString(id)}) == 1;
    }

    public void createTables(SQLiteDatabase db) {
        String tableName = mTableName;
        if (tableName == null) {
            return;
        }

        AtomicReference<StringBuilder> sql = new AtomicReference<>(new StringBuilder("CREATE TABLE"));
        sql.get().append(tableName);
        sql.get().append(" (_id INTEGER PRIMARY KEY");
        ColumnInfo[] columns = mColumnInfos;
        int numColumns = columns.length;
        for (int i = 0; i != numColumns; i++) {
            ColumnInfo column = columns[i];
            if (!column.isId()) {
                sql.get().append(",");
                sql.get().append(column.mName);
                sql.get().append(" ");
                sql.get().append(SQLITE_TYPES[column.mType]);
                sql.get().append(' ');
                sql.get().append(column.mExtraSql);
            }
        }
    }

    public static final class ColumnInfo {
        public final String mName;
        public final int mType;
        public final boolean mIndexed;
        public final boolean mFullText;
        public final String mExtraSql = "";
        public final Field mField;
        public final int mProjectionIndex;

        public ColumnInfo(String name, int type, boolean indexed, boolean fullText, Field field, int projectionIndex) {
            this.mName = name.toLowerCase();
            this.mType = type;
            this.mIndexed = indexed;
            this.mFullText = fullText;
            this.mField = field;
            this.mProjectionIndex = projectionIndex;
        }

        public boolean isId() {
            return mName.equals("_id");
        }
    }
}
