package co.soml.android.app.fragments;

import android.os.Bundle;

import co.soml.android.app.StoryApp;
import co.soml.android.app.controllers.PeopleController;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.utils.LogUtils;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.14.2015
 */
public class FollowerUserFragment extends BaseFragment {
	public static final String TAG = LogUtils.makeLogTag(FollowerUserFragment.class.getSimpleName());
	public StoryApp mApp;
	public StoryDataModel mDataModel;
	public PeopleController mController;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
    public void invalidate() {

    }

    @Override
    public void invalidate(Object... params) {

    }
}
