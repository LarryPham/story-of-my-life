package co.soml.android.app.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import co.soml.android.app.models.Story;
import co.soml.android.app.utils.LogUtils;

/**
 * The HeaderView was shown story name, description, and user profile image (if it exists) for a story - designed for use in
 * StoryListFragment when previewing stories in a stoy (story preview) but can be reused elsewhere - call loadStoryInfo() to show the
 * info for a specific blog
 */
public class StoryInfoView extends FrameLayout {

	public static final String TAG = LogUtils.makeLogTag(StoryInfoView.class.getSimpleName());

	public interface StoryInfoListener {
		void onStoryInfoLoaded(Story storyInfo);
		void onStoryInfoFailed();
	}

	private StoryInfoListener mInfoListener;
	private Story mStoryInfo;

	public StoryInfoView(Context context) {
		super(context);

	}

	public StoryInfoView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public StoryInfoView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	public void loadStoryInfo(long storyId, StoryInfoListener storyInfoListener) {
		mInfoListener = storyInfoListener;

	}

	private void showStoryInfo(final Story storyInfo, boolean animateIn) {
		// this is the layout containing the story info views

	}
}
