/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/20/15 6:40 AM.
 **/

package co.soml.android.app.models;

import java.util.ArrayList;

import co.soml.android.app.utils.LogUtils;

public class HashTagList extends ArrayList<HashTag> {
    public static final String TAG = LogUtils.makeLogTag(HashTagList.class.getSimpleName());

    @Override
    public Object clone() {
        return super.clone();
    }

    public int indexOfTagName(final String tagName) {
        if (tagName == null || isEmpty()) {
            return -1;
        }

        for (int index = 0; index < size(); index++) {
            if (tagName.equals(this.get(index).getHashTagName())) {
                return index;
            }
        }
        return -1;
    }

    public boolean hasSameTag(HashTag tag) {
        if (tag == null || isEmpty()) {
            return false;
        }

        for (HashTag thisTag : this) {
            if (HashTag.isSameHashTag(thisTag, tag)) {
                return true;
            }
        }
        return false;
    }

    public boolean isSameList(HashTagList tagList) {
        if (tagList == null || tagList.size() != this.size()) {
            return false;
        }

        for (HashTag thisTag: tagList) {
            if (!hasSameTag(thisTag)) {
                return false;
            }
        }
        return true;
    }

    public HashTagList getDeletions(HashTagList tagList) {
        HashTagList deletions = new HashTagList();
        if (tagList == null) {
            return deletions;
        }

        for (HashTag thisTag: this) {
            if (!tagList.hasSameTag(thisTag)) {
                deletions.add(thisTag);
            }
        }
        return deletions;
    }
}
