package co.soml.android.app.datasets;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import co.soml.android.app.StoryApp;
import co.soml.android.app.utils.LogUtils;

public class StoryDatabase extends SQLiteOpenHelper {
	public static final String TAG = LogUtils.makeLogTag(StoryDatabase.class.getSimpleName());

	private static final String DB_NAME = "StoryReader.db";
	private static final int DB_VERSION = 1;
	private static StoryDatabase mStoryDb;
	private static final Object mDbLock = new Object();

	public static StoryDatabase getDatabase() {
		if (mStoryDb == null) {
			synchronized (mDbLock) {
				if (mStoryDb == null) {
					mStoryDb = new StoryDatabase(StoryApp.getContext());
					mStoryDb.getWritableDatabase();
				}
			}
		}
		return mStoryDb;
	}

	public static SQLiteDatabase getReadableDB() {
		return getDatabase().getReadableDatabase();
	}

	public static SQLiteDatabase getWritableDB() {
		return getDatabase().getWritableDatabase();
	}

	public void onOpen(SQLiteDatabase db) {
		super.onOpen(db);
	}

	public static void reset() {
		SQLiteDatabase db = getWritableDB();
		getDatabase().reset();
	}


	public StoryDatabase(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		createAllTables(db);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		LogUtils.LOGI(TAG, "Upgrading database from version: " + oldVersion + " to version " + newVersion);
		reset(db);
	}

	private void createAllTables(SQLiteDatabase db) {
		CommentTable.createTables(db);
		StoryLikeTable.createTables(db);
	}

	private void dropAllTables(SQLiteDatabase db) {
		CommentTable.dropTables(db);
		StoryLikeTable.dropTables(db);

	}

	private void reset(SQLiteDatabase db) {
		db.beginTransaction();
		try {
			dropAllTables(db);
			createAllTables(db);
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}


}
