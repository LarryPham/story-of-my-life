/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 * @author: Larry Pham (email: larrypham.vn@gmail.com)
 * Date: 6/2/15 4:17 PM
 **/

package co.soml.android.app.controllers;

import android.content.Intent;
import android.os.Message;
import android.support.design.widget.Snackbar;
import android.util.Log;

import co.soml.android.app.R;
import co.soml.android.app.StoryApp;
import co.soml.android.app.activities.BaseActivity;
import co.soml.android.app.activities.StoryMainActivity;
import co.soml.android.app.fragments.BaseFragment;
import co.soml.android.app.fragments.CollectionsFragment;
import co.soml.android.app.fragments.MyStoryFragment;
import co.soml.android.app.fragments.PeopleFragment;
import co.soml.android.app.fragments.SearchFragment;
import co.soml.android.app.models.StoryBean;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.services.StoryService;
import co.soml.android.app.services.tasks.Action;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.widgets.ProgressWheel;

public class AccountProfileController extends BaseController {
    public static final String TAG = LogUtils.makeLogTag(AccountProfileController.class.getSimpleName());

	private StoryDataModel mDataModel;
	private BaseActivity mActivity;
	private StoryApp mApp;

	private ProgressWheel mWaitProgressWheel = null;
	private boolean mScrollState = false;

	private SearchFragment mSearchFragment;
	private CollectionsFragment mHomeFragment;
	private MyStoryFragment mMyStoryFragment;
	private PeopleFragment mSocialFragment;
	private StoryBean mBean = StoryMainActivity.getBean();

    public AccountProfileController(BaseActivity activity, StoryDataModel dataModel) {
        super(activity, dataModel);
	    mActivity = activity;
	    mApp = (StoryApp) activity.getApplicationContext();
	    mDataModel = mApp.getAppDataModel();
    }

    public AccountProfileController(BaseActivity activity, StoryDataModel dataModel, boolean isFirstRunningMode) {
        super(activity, dataModel, isFirstRunningMode);
	    this.mActivity = activity;
	    this.mApp = (StoryApp) activity.getApplicationContext();
	    mDataModel = mApp.getAppDataModel();
    }

    public AccountProfileController(BaseActivity activity, BaseFragment fragment, StoryDataModel dataModel) {
        super(activity, fragment, dataModel);
    }

    @Override
    protected void handleMessage(Message msg) {
	    final String fn = "handleMessage() - ";
        switch (msg.what) {
	        case ControllerMessage.ACTION_WAITING_START: {
		        break;
	        }
	        case ControllerMessage.ACTION_WAITING_END: {
		        LogUtils.LOGD(TAG, "[SIGNAL]");
		        break;
	        }
	        case ControllerMessage.REQUEST_TO_SERVER_ACCOUNT_INFO: {
		        LogUtils.LOGD(TAG, fn + "[RequestToServer] Getting AccountInfo");
		        final Intent intent = this.obtainIntent(msg.what);
		        intent.setAction(Action.REQUEST_TO_SERVER_USER_DETAIL);
		        mActivity.startService(intent);
		        getActionCounter().incrementActionCount();
		        break;
	        }
	        case ControllerMessage.REQUEST_TO_SERVER_CANCEL: {
		        final Intent intent = this.obtainIntent(msg.what);
		        intent.setAction(Action.REQUEST_TO_SERVER_CANCEL);
		        mActivity.startService(intent);
		        getActionCounter().reset();
		        this.mDataModel.setWaitForRequesting(false);
		        break;
	        }
	        case ControllerMessage.RESUME_BACKGROUND_TASK: {
		        mScrollState = false;
		        if (msg.obj == null) {
			        mFragment.invalidate();
		        } else if ((Boolean) msg.obj) {
			        mFragment.invalidate();
		        }
		        break;
	        }
	        case ControllerMessage.PAUSE_BACKGROUND_TASK: {
		        if (!mScrollState) {
			        mScrollState = true;
			        final Intent intent = new Intent(mActivity, StoryService.class);
			        intent.setAction(Action.REQUEST_IMAGE_FROM_SERVER_CANCEL);
			        mActivity.startService(intent);
		        }
		        break;
	        }

        }
    }

	public void incrementActionCount() {
		this.getActionCounter().incrementActionCount();
		if (this.getActionCounter().getCount() > 0) {
			mDataModel.setWaitForRequesting(true);
		}
	}

	public void decrementActionCount() {
		this.getActionCounter().decrementActionCount();
		if (this.getActionCounter().getCount() <= 0) {
			mDataModel.setWaitForRequesting(false);
		}
	}

	public void showCheckNetworkSnackBar() {
		String strCheckNetworkToast = mActivity.getResources().getString(R.string.msg_error_http_connect_failed);
		if (strCheckNetworkToast == null) {

		}
	}

}
