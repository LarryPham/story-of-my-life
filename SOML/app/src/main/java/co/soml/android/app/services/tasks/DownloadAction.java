package co.soml.android.app.services.tasks;

import android.os.Parcel;

import co.soml.android.app.AppConstants;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its
 * documentation are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable
 * form without the prior written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with
 * respect to the contents, and assumes no responsibility for any errors that might appear in the software and
 * documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.02.2015
 * <p>
 * The class <code>DownloadAction</code> which used to put the bundle of action for downloading content from server,
 * each of controllers can used it to notify the service component to understand what kind of things it must to
 * implement the <code>DownloadTask</code> to download content.
 */
public class DownloadAction extends Action {
    public static final String TAG = AppConstants.PREFIX + DownloadAction.class.getSimpleName();
    private Params mParams;

    public DownloadAction(String code, int id, Params params) {
        super(Action.DOWNLOAD_CONTENT, id, (Object) params);
    }

    public DownloadAction(Parcel source) {
        super(source);
        readFromParcel(source);
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public Params getParams() {
        return mParams;
    }

    public void setParams(Params params) {
        mParams = params;
    }

    static public class Params {
        public String mFrom;
        public String mTo;
        public long mReDownloadSize;

        public Params(String fromUrl, String toUrl, long reDownloadSize) {
            this.mFrom = fromUrl;
            this.mTo = toUrl;
            this.mReDownloadSize = reDownloadSize;
        }
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mId);
        dest.writeString(((Params) mObject).mFrom);
        dest.writeString(((Params) mObject).mTo);
    }

    public void readFromParcel(Parcel inSource) {
        this.mId = inSource.readInt();
        this.mParams.mFrom = inSource.readString();
        this.mParams.mTo = inSource.readString();
    }

    public static final Creator<DownloadAction> CREATOR = new Creator<DownloadAction>() {
        @Override
        public DownloadAction createFromParcel(Parcel source) {
            return new DownloadAction(source);
        }

        @Override
        public DownloadAction[] newArray(int size) {
            return new DownloadAction[size];
        }
    };
}
