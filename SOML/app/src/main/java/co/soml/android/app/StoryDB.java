package co.soml.android.app;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import co.soml.android.app.datasets.CommentTable;
import co.soml.android.app.models.User;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.utils.SqlUtils;
import co.soml.android.app.utils.StoryUtils;

/**
 *  Copyright (C) 2015 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation
 *  are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied,
 *  reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior
 *  written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with respect to the contents, and
 *  assumes no responsibility for any errors that might appear in the software and documents. This publication and the
 *  contents hereof are subject to change without notice.
 *  @author Larry Pham(Email: larrypham.vn@gmail.com)
 *  @since 6/24/15 4:29 PM
 **/
public class StoryDB{
    public static final String TAG = LogUtils.makeLogTag(StoryDB.class.getSimpleName());

    public static final String COLUMN_NAME_STORY_ID = "story_id";
    public static final String COLUMN_NAME_USER_ID = "user_id";
    public static final String COLUMN_NAME_FOLLOW_ID = "follow_id";
    public static final String COLUMN_NAME_FOLLOWABLE_ID = "followable_id";
    public static final String COLUMN_NAME_FOLLOWABLE_TYPE = "followable_type";

    public static final String COLUMN_NAME_FOLLOWER_ID = "follower_id";
    public static final String COLUMN_NAME_FOLLOWER_TYPE = "follower_type";
    public static final String COLUMN_NAME_BLOCKED = "blocked";

    public static final String COLUMN_NAME_STORY_NAME = "name";
    public static final String COLUMN_NAME_INTERVAL = "interval";

    public static final String COLUMN_NAME_DESCRIPTION = "description";
    public static final String COLUMN_NAME_LATITUDE = "latitude";
    public static final String COLUMN_NAME_LONGITUDE = "longitude";

    public static final String COLUMN_NAME_CREATED_AT = "created_at";
    public static final String COLUMN_NAME_UPDATED_AT = "updated_at";
    public static final String COLUMN_NAME_DELETED_AT = "deleted_at";

    public static final String COLUMN_NAME_LOCATION = "location";
    public static final String COLUMN_NAME_PHOTO_ORDER = "photo_order";
    public static final String COLUMN_NAME_COVER_ID = "cover_id";
    public static final String COLUMN_NAME_VIEW_COUNT = "view_count";

    public static final String COLUMN_NAME_SLUG = "slug";
    public static final String COLUMN_NAME_PLAYBACK_INTERVAL = "playback_interval";
    public static final String COLUMN_NAME_BODY = "body";
    public static final String COLUMN_NAME_ACCESS_LEVEL = "access_level";
    public static final String COLUMN_NAME_STATE = "state";

    public static final String COLUMN_NAME_CAPTION = "caption";
    public static final String COLUMN_NAME_POSITION = "position";
    public static final String COLUMN_NAME_DISPLAYED_TIME = "displayed_time";

    public static final String COLUMN_NAME_REPORT_ID = "report_id";
    public static final String COLUMN_NAME_REPORTABLE_ID = "reportable_id";
    public static final String COLUMN_NAME_REPORTABLE_TYPE = "reportable_type";

    public static final String COLUMN_NAME_REASON = "reason";
    public static final String COLUMN_NAME_MESSAGE = "message";

    public static final String COLUMN_NAME_HASH_TAG_ID = "hashtag_id";
    public static final String COLUMN_NAME_HASH_TAGABLE_ID = "hashtaggable_id";
    public static final String COLUMN_NAME_HASH_TAGABLE_TYPE = "hashtaggable_type";

    public static final String DATABASE_NAME = "StoryDB";
    public static final int DATABASE_VERSION = 1;

    private static final String CREATE_TABLE_STORIES = "CREATE TABLE IF NOT EXISTS "
            + " Stories(id INTEGER PRIMARY KEY AUTOINCREMENT, "
            + " StoryID INTEGER, UserID INTEGER, SongID INTEGER, CoverID INTEGER,"
            + " Title TEXT, Description TEXT,  Latitude TEXT, Longitude TEXT, State TEXT, CreatedAt DATETIME, UpdatedAt DATETIME,"
            + " DeletedAt DATETIME, Location TEXT, PhotoOrder TEXT, Slug TEXT, Interval INTEGER DEFAULT 0,"
            + " PlaybackInterval INTEGER DEFAULT 3000, Narration TEXT"
            + " ViewCount INTEGER, TotalVotes INTEGER);";

    private static final String CREATE_TABLE_SETTINGS = "CREATE TABLE IF NOT EXISTS "
            + " Accounts(id INTEGER PRIMARY KEY AUTOINCREMENT, UserID INTEGER, AccessToken TEXT, "
            + " Email TEXT, FullName TEXT, Description TEXT, ImageURL TEXT, CenterThumbnail BOOLEAN, "
            + " FullSizeImage BOOLEAN);";

    private static final String COMMENTS_TABLE = "Comments";
    private static final String PHOTOS_TABLE = "Photos";

    private static final String STORIES_TABLE = "Stories";
    private static final String SETTINGS_TABLE = "Accounts";

    private static final String UPDATE_USER_ID = "UPDATE Accounts SET UserID = 1;";

    private SQLiteDatabase db;
    private Context mContext;

    public StoryDB(Context context) {
        this.mContext = context;
        db = context.openOrCreateDatabase(DATABASE_NAME, 0, null);

        db.execSQL(CREATE_TABLE_STORIES);
        db.execSQL(CREATE_TABLE_SETTINGS);

        CommentTable.createTables(db);

        // Update tables for new installs and app updates
        db.setVersion(DATABASE_VERSION);
    }

    public SQLiteDatabase getDatabase() {
        return db;
    }

    public static void deleteDatabase(Context context) {
        context.deleteDatabase(DATABASE_NAME);
    }

    public boolean addAccount(User user) {
        ContentValues values = new ContentValues();
        values.put("UserID", user.getId());
        values.put("AccessToken", user.getAuthToken());
        values.put("Email", user.getEmail());
        values.put("FullName", user.getFullName());
        values.put("Description", user.getDescription());

        values.put("ImageURL", user.getAvatarOriginalUrl());
        values.put("CenterThumbnail", false);
        values.put("FullSizeImage", false);

        return db.insert(SETTINGS_TABLE, null, values) > -1;
    }

    public List<Integer> getAllAccountIDs() {
        Cursor cursor = db.rawQuery("SELECT DISTINCT id FROM " + SETTINGS_TABLE, null);
        try {

            List<Integer> ids = new ArrayList<Integer>();
            if (cursor.moveToFirst()) {
                do {
                    ids.add(cursor.getInt(0));
                } while (cursor.moveToNext());
            }
            return ids;
        } finally {
            SqlUtils.closeCursor(cursor);
        }
    }

    public List<Map<String, Object>> getAccountsBy(String byString, String[] extraFields) {
        return getAccountBy(byString, extraFields, 0);
    }

    public List<Map<String, Object>> getAccountBy(String byString, String[] extraFields, int limit) {
        if (db == null) {
            return new Vector<Map<String, Object>>();
        }

        String limitStr = null;
        if (limit != 0) {
            limitStr = String.valueOf(limit);
        }

        String[] baseFields = new String[] {"id", "UserID", "FullName", "Email"};
        String[] allFields = baseFields;
        if (extraFields != null) {
            //allFields = (String[]) ArrayUtils.addAll(baseFields, extraFields);
        }

        Cursor cursor = db.query(SETTINGS_TABLE, allFields, byString, null, null, null, null, limitStr);
        int numRows = cursor.getCount();
        cursor.moveToFirst();
        List<Map<String,Object>> accounts = new Vector<Map<String, Object>>();
        for (int index = 0; index < numRows; index++) {
            int id = cursor.getInt(0);
            long userID = cursor.getLong(1);
            String email = cursor.getString(3);
            String fullName = cursor.getString(4);

            if (id > 0) {
                Map<String, Object> thisHash = new HashMap<String, Object>();
                thisHash.put("Id", id);
                thisHash.put("UserId", userID);
                thisHash.put("Email", email);
                thisHash.put("FullName", fullName);

                int extraFieldsIndex = baseFields.length;
                if (extraFields != null) {
                    for (int j = 0; j < extraFields.length; j++) {
                        thisHash.put(extraFields[j], cursor.getString(extraFieldsIndex + j));
                    }
                }
                accounts.add(thisHash);
            }
            cursor.moveToNext();
        }
        cursor.close();
        Collections.sort(accounts, StoryUtils.UserNameComparator);
        return accounts;
    }

    public List<Map<String,Object>> getAllAccounts() {
        return getAccountsBy(null,null);
    }

    public boolean isLocalStoryIdInDatabase(int localStoryId) {
        String[] args = {Integer.toString(localStoryId)};
        return SqlUtils.boolForQuery(db, "SELECT 1 FROM " + SETTINGS_TABLE + " WHERE id=?", args);
    }

    protected void copyDatabase() {
        String copyFrom = db.getPath();
        String copyTo = StoryApp.getContext().getExternalFilesDir(null).getAbsolutePath() + "/" + DATABASE_NAME + ".db";
        try {
            InputStream inputStream = new FileInputStream(copyFrom);
            OutputStream outputStream = new FileOutputStream(copyTo);

            byte[] buffer = new byte[1024];
            int length;
            while ((length = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, length);
            }
            outputStream.flush();
            outputStream.close();
            inputStream.close();
        } catch (IOException ex) {
            LogUtils.LOGE(TAG, "Failed to copy database");
        }
    }
}
