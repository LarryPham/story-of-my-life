package co.soml.android.app.fragments;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since May.07.2015
 */
public interface EmailAuthPolicy {

    /**
     * Starts the new progressions for authenticating email's login or email's signup.
     *
     * @param message The action message which will be assigned into an instance of {@link SignInFragment} or {@link NewUserFragment}
     */
    void startProgress(String message);

    /**
     * Updates the current progression for authenticating email's login or email's signup.
     *
     * @param message The action-message which will be assigned to an instance {@link SignInFragment} or {@link NewUserFragment}
     */
    void updateProgress(String message);

    /**
     * Ends the current progression for authenticating email's login or email's signup.
     */
    void endProgress();

    void onDoneAction();

    boolean isUserDataValid();
}
