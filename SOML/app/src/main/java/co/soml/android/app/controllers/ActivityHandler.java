package co.soml.android.app.controllers;

import android.os.Message;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its
 * documentation are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable
 * form without the prior written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with
 * respect to the contents, and assumes no responsibility for any errors that might appear in the software and
 * documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.01.2015
 * <p>
 * The class <code>ActivityHandler</code> define as the {@link android.os.Handler} with its <code>{@link android.os.Handler.Callback}</code>
 * interface for instantiating a Handler to avoid having to implement your own subclass of Handler.
 */
public abstract class ActivityHandler {

    /**
     * Method used to call {@link co.soml.android.app.controllers.BaseController#handleMessage(android.os.Message)} method for processing
     * the sent message or received message
     * <p>
     * Callback interface you can use when instantiating a Handler to avoid
     * having to implement your own subclass of Handler.
     */
    public abstract void callBack(Message msg);
}