package co.soml.android.app.controllers;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;

import java.util.List;

import co.soml.android.app.StoryApp;
import co.soml.android.app.activities.BaseActivity;
import co.soml.android.app.activities.InvalidateParam;
import co.soml.android.app.activities.MyStoryActivity;
import co.soml.android.app.activities.adapters.StoryPostsAdapter;
import co.soml.android.app.fragments.BaseFragment;
import co.soml.android.app.fragments.adapters.StoryReaderListeners;
import co.soml.android.app.models.Story;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.models.StoryFeedList;
import co.soml.android.app.services.StoriesResult;
import co.soml.android.app.services.VoteResult;
import co.soml.android.app.services.tasks.Action;
import co.soml.android.app.utils.LogUtils;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.15.2015
 */
public class MyStoryController extends BaseController {

    private StoryDataModel mDataModel;
    private StoryApp mApp;
    private BaseActivity mActivity;

    // Listeners for controllers which has been interacted with the adapter or cell views
    // Listener for checking the story type selected
    public StoryReaderListeners.OnStoryTypeSelectedListener mOnStoryTypeSelectedListener;
    // Listener for checking the story was selected
    public StoryReaderListeners.OnStorySelectedListener mOnStorySelectedListener;
    // Listener for handling the popup show up or dismiss out
    public StoryReaderListeners.OnStoryPopupListener mOnStoryPopupListener;
    // Listener for checking the data for adapter has been loaded or not??
    public StoryReaderListeners.DataLoadedListener mDataLoadedListener;
    // Listener for report story
    public StoryReaderListeners.RequestReportStoryListener mRequestRepostStoryListener;

    public static class RequestStoriesParam {

        public int mApiVersion;
        public String mAuthToken;
        public int mUserId;
        public String mScopeName;
        public int mStartIndex;
        public int mLimit;
        public String mState;

        public RequestStoriesParam(int apiVersion, String authToken, int userId, String scopeName, int startIndex, int limit, String
                state) {
            this.mApiVersion = apiVersion;
            this.mAuthToken = authToken;
            this.mUserId = userId;
            this.mScopeName = scopeName;
            this.mStartIndex = startIndex;
            this.mLimit = limit;
            this.mState = state;
        }

        public RequestStoriesParam(int apiVersion, String authToken, int userId, int startIndex, int limit, String state) {
            this.mApiVersion = apiVersion;
            this.mAuthToken = authToken;
            this.mUserId = userId;
            this.mStartIndex = startIndex;
            this.mLimit = limit;
            this.mState = state;
        }
    }

    public static class StoryReqParam {

        public String mScopeName;
        public int userId;
        public int startIndex;
        public int limit;
        public String state;
    }

    public MyStoryController(BaseActivity activity, StoryDataModel dataModel) {
        super(activity, dataModel);
        mActivity = (MyStoryActivity) activity;
        mApp = (StoryApp) mActivity.getApplicationContext();
        mDataModel = dataModel;
    }

    public MyStoryController(BaseActivity activity, BaseFragment fragment, StoryDataModel dataModel) {
        super(activity, fragment, dataModel);
        mActivity = activity;
        mApp = (StoryApp) mActivity.getApplicationContext();
        mDataModel = dataModel;
        mFragment = fragment;
    }

    @Override
    protected void handleMessage(Message msg) {
        switch (msg.what) {
            case ControllerMessage.REQUEST_TO_SERVER_MY_STORY_PRIVATE: {
                final Intent intent = obtainIntent(msg.what);
                final RequestStoriesParam param = (RequestStoriesParam) msg.obj;
                // Authentication token and api version for header params
                intent.putExtra(Action.X_API_VERSION, param.mApiVersion);
                intent.putExtra(Action.X_AUTH_TOKEN, param.mAuthToken);
                // Put user_id and start_index, state
                intent.putExtra(Action.USER_ID, param.mUserId);

                intent.putExtra(Action.START_INDEX, param.mStartIndex);
                intent.putExtra(Action.LIMIT, param.mLimit);
                LogUtils.LOGD(TAG, String.format("State for request the private stories: %s", param.mState));
                intent.putExtra(Action.STORY_STATE, param.mState);

                intent.setAction(Action.REQUEST_TO_SERVER_MY_STORY);
                mActivity.startService(intent);
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_MY_STORY_PUBLISHED: {
                final Intent intent = obtainIntent(msg.what);
                final RequestStoriesParam param = (RequestStoriesParam) msg.obj;
                intent.putExtra(Action.X_API_VERSION, param.mApiVersion);
                intent.putExtra(Action.X_AUTH_TOKEN, param.mAuthToken);
                intent.putExtra(Action.USER_ID, param.mUserId);
                intent.putExtra(Action.START_INDEX, param.mStartIndex);
                intent.putExtra(Action.LIMIT, param.mLimit);
                LogUtils.LOGD(TAG, String.format("State for request the published stories: %s", param.mState));
                intent.putExtra(Action.STORY_STATE, param.mState);

                intent.setAction(Action.REQUEST_TO_SERVER_MY_STORY_PUBLISHED);
                mActivity.startService(intent);
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_MY_STORY_COLLABORATING: {
                final Intent intent = obtainIntent(msg.what);
                final RequestStoriesParam param = (RequestStoriesParam) msg.obj;
                // Authentication token and api version for header params
                intent.putExtra(Action.X_API_VERSION, param.mApiVersion);
                intent.putExtra(Action.X_AUTH_TOKEN, param.mAuthToken);
                // Put scope name and start_index, state
                intent.putExtra(Action.STORY_SCOPE, param.mScopeName);

                intent.putExtra(Action.START_INDEX, param.mStartIndex);
                intent.putExtra(Action.LIMIT, param.mLimit);
                LogUtils.LOGD(TAG, String.format("State for request the collaborating stories: %s", param.mState));
                intent.setAction(Action.REQUEST_TO_SERVER_MY_STORY_COLLABORATING);
                mActivity.startService(intent);
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_MY_STORY_PRIVATE_COMPLETED: {
                if (checkOwner(msg)) {
                    LogUtils.LOGD(TAG, "RequestToServer: Getting Stories From MyStory Function Was Completed");
                    final StoriesResult result = (StoriesResult) msg.obj;
                    final List<Story> stories = result.getStoryList();
                    final StoryFeedList targetedStories = mDataModel.getMyStoryPrivateList();

                    if (targetedStories != null) {
                        if (stories != null && stories.size() > 0) {
                            for (int index = 0; index < stories.size(); index++) {
                                targetedStories.add(new Story(stories.get(index)));
                                LogUtils.LOGD(TAG, String.format("[GUN]PrivateStory: [%08d:%s]",
                                        stories.get(index).getId(), stories.get(index).getName()));
                            }
                        }
                    }
                    final InvalidateParam param = new InvalidateParam(msg.what, msg.obj);
                    mFragment.invalidate(param);
                }
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_MY_STORY_PUBLISHED_COMPLETED: {
                if (checkOwner(msg)) {
                    LogUtils.LOGD(TAG, "RequestToServer: Getting Stories From MyStory Function Was Completed");
                    final StoriesResult result = (StoriesResult) msg.obj;
                    final List<Story> stories = result.getStoryList();
                    final StoryFeedList targetedStories = mDataModel.getMyStoryPublishedList();
                    if (targetedStories != null) {
                        if (stories != null && stories.size() > 0) {
                            for (int index = 0; index < stories.size(); index++) {
                                targetedStories.add(new Story(stories.get(index)));
                                LogUtils.LOGD(TAG, String.format("[GUN]PublishedStory: [%08d:%s]",
                                        stories.get(index).getId(), stories.get(index).getName()));
                            }
                        }
                    }
                    final InvalidateParam param = new InvalidateParam(msg.what, targetedStories);
                    mFragment.invalidate(param);
                }
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_STORIES_COLLABORATING_COMPLETED: {
                if (checkOwner(msg)) {
                    LogUtils.LOGD(TAG, "RequestToServer: Getting stories from MyStory function was completed!");
                    final StoriesResult result = (StoriesResult) msg.obj;
                    final List<Story> storyList = result.getStoryList();
                    final StoryFeedList targetedStories = mDataModel.getMyStoryCollaboratedList();
                    if (targetedStories != null) {
                        if (storyList != null && storyList.size() > 0) {
                            for (int index = 0; index < storyList.size(); index++) {
                                targetedStories.add(new Story(storyList.get(index)));
                                LogUtils.LOGD(TAG, String.format("[GUN]CollaboratingStory: [%08d:%s]",
                                        storyList.get(index).getId(), storyList.get(index).getName()));
                            }
                        }
                    }
                    final InvalidateParam param = new InvalidateParam(msg.what, msg.obj);
                    mFragment.invalidate(param);
                }
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_STORY_LIKE: {
                final Intent intent = obtainIntent(ControllerMessage.REQUEST_TO_SERVER_STORY_LIKE);
                final StoryPostsAdapter.RequestParam param = (StoryPostsAdapter.RequestParam) msg.obj;

                intent.putExtra(Action.STORY_ID, param.mId);
                intent.setAction(Action.REQUEST_TO_SERVER_STORY_LIKE);
                mActivity.startService(intent);
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_STORY_LIKE_COMPLETED: {
                if (checkOwner(msg)) {
                    final VoteResult result = (VoteResult) msg.obj;
                    final String vote = result.getVote();
                    final InvalidateParam invalidateParam = new InvalidateParam(msg.what, result);
                    mFragment.invalidate(invalidateParam);
                }
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_STORY_UNLIKE: {
                final Intent intent = obtainIntent(ControllerMessage.REQUEST_TO_SERVER_STORY_UNLIKE);
                final StoryPostsAdapter.RequestParam param = (StoryPostsAdapter.RequestParam) msg.obj;

                intent.putExtra(Action.STORY_ID, param.mId);
                intent.setAction(Action.REQUEST_TO_SERVER_STORY_UNLIKE);
                mActivity.startService(intent);
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_STORY_UNLIKE_COMPLETED: {
                if (checkOwner(msg)) {
                    final VoteResult result = (VoteResult) msg.obj;
                    final String vote = result.getVote();
                    final InvalidateParam param = new InvalidateParam(msg.what, result);
                    mFragment.invalidate(param);
                }
                break;
            }
        }
    }

    public StoryReaderListeners.OnStoryTypeSelectedListener getOnStoryTypeSelectedListener() {
        return mOnStoryTypeSelectedListener;
    }

    public void setOnStoryTypeSelectedListener(
            StoryReaderListeners.OnStoryTypeSelectedListener onStoryTypeSelectedListener) {
        mOnStoryTypeSelectedListener = onStoryTypeSelectedListener;
    }

    public StoryReaderListeners.OnStorySelectedListener getOnStorySelectedListener() {
        return mOnStorySelectedListener;
    }

    public void setOnStorySelectedListener(
            StoryReaderListeners.OnStorySelectedListener onStorySelectedListener) {
        mOnStorySelectedListener = onStorySelectedListener;
    }

    public StoryReaderListeners.OnStoryPopupListener getOnStoryPopupListener() {
        return mOnStoryPopupListener;
    }

    public void setOnStoryPopupListener(StoryReaderListeners.OnStoryPopupListener onStoryPopupListener) {
        mOnStoryPopupListener = onStoryPopupListener;
    }

    public StoryReaderListeners.DataLoadedListener getDataLoadedListener() {
        return mDataLoadedListener;
    }

    public void setDataLoadedListener(StoryReaderListeners.DataLoadedListener dataLoadedListener) {
        mDataLoadedListener = dataLoadedListener;
    }

    public StoryReaderListeners.RequestReportStoryListener getRequestRepostStoryListener() {
        return mRequestRepostStoryListener;
    }

    public void setRequestRepostStoryListener(
            StoryReaderListeners.RequestReportStoryListener requestRepostStoryListener) {
        mRequestRepostStoryListener = requestRepostStoryListener;
    }
}
