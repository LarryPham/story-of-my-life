/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 7/31/15 3:11 AM.
 **/

package co.soml.android.app.widgets;

import com.imagezoom.ImageViewTouch;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import co.soml.android.app.drawables.EditableDrawable;
import co.soml.android.app.drawables.FeatherDrawable;
import co.soml.android.app.utils.LogUtils;

public class ImageViewDrawableOverlay extends ImageViewTouch {
    public static final String TAG = LogUtils.makeLogTag(ImageViewDrawableOverlay.class.getSimpleName());

    public static interface OnDrawableEventListener {
        void onFocusChanged(HighLightView newFocus, HighLightView oldFocus);
        void onDown(HighLightView view);
        void onMove(HighLightView view);
        void onClick(HighLightView view);
        void onClick(StoryTextView labelView);
    }

    private List<HighLightView> mOverlayViews = new CopyOnWriteArrayList();
    private HighLightView mOverlayView;
    private OnDrawableEventListener mDrawableEventListener;
    private boolean mForceSingleSelection = true;

    private Paint mDropPaint;
    private Rect mTempRect = new Rect();
    private boolean mScaleWithContent = false;
    private List<StoryTextView> mLabels = new ArrayList<StoryTextView>();
    private StoryTextView mCurrentLabel;
    private float mLabelX, mLabelY, mDownLabelX, mDownLabelY;

    public void setCurrentLabel(StoryTextView label, float eventRawX, float eventRawY) {
        if (mLabels.contains(label)) {
            mCurrentLabel = label;
            int[] location = new int[2];

            label.getLocationOnScreen(location);
            mLabelX = eventRawX - location[0];
            mLabelY = eventRawY - location[1];
            mDownLabelX = eventRawX;
            mDownLabelY = eventRawY;
        } else if (label == null) {
            mCurrentLabel = null;
        }
    }

    public void addLabel(StoryTextView label) {
        mLabels.add(label);
    }

    public void removeLabel(StoryTextView label) {
        mCurrentLabel = null;
        mLabels.remove(label);
    }

    public ImageViewDrawableOverlay(Context context) {
        super(context);
    }

    public ImageViewDrawableOverlay(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ImageViewDrawableOverlay(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (mCurrentLabel != null) {
            mCurrentLabel.invalidate();
        }

        if (mCurrentLabel != null) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:
                    float upX = event.getRawX();
                    float upY = event.getRawY();
                    double distance = Math.sqrt(Math.abs(upX - mDownLabelX) * Math.abs(upX - mDownLabelX)
                                    + Math.abs(upY - mDownLabelY) * Math.abs(upY - mDownLabelY));
                    if (distance < 15) {
                        if (mDrawableEventListener != null) {
                            mDrawableEventListener.onClick(mCurrentLabel);
                        }
                    }
                    mCurrentLabel = null;
                    break;
                default:
                    break;
            }
            return true;
        }
        return super.onTouchEvent(event);
    }

    protected void panBy(double dx, double dy) {
        RectF rect = getBitmapRect();
        mScrollRect.set((float) dx, (float) dy, 0, 0);
        updateRect(rect, mScrollRect);
        center(true, true);
    }

    @Override
    protected void init(Context context, AttributeSet attrs, int defStyle) {
        super.init(context, attrs, defStyle);
        mTouchSlop = ViewConfiguration.get(context).getScaledDoubleTapSlop();
        mGestureDetector.setIsLongpressEnabled(false);
    }

    public void setScaleWithContent(boolean value) {
        mScaleWithContent = value;
    }

    public boolean getScaleWithContent() {
        return mScaleWithContent;
    }

    public void setForceSingleSelection(boolean value) {
        mForceSingleSelection = value;
    }

    public void setOnDrawableEventListener(OnDrawableEventListener listener) {
        mDrawableEventListener= listener;
    }

    @Override
    public void setImageDrawable(Drawable drawable, Matrix initialMatrix, float minZoom, float maxZoom) {
        super.setImageDrawable(drawable, initialMatrix, minZoom, maxZoom);
    }

    @Override
    protected void onLayoutChanged(int left, int top, int right, int bottom) {
        super.onLayoutChanged(left, top, right, bottom);

        if (getDrawable() != null) {
            Iterator<HighLightView> iterator = mOverlayViews.iterator();
            while (iterator.hasNext()) {
                HighLightView view = iterator.next();
                view.getMatrix().set(getImageMatrix());
                view.invalidate();
            }
        }
    }

    @Override
    public void postTranslate(float deltaX, float deltaY) {
        super.postTranslate(deltaX, deltaY);
        Iterator<HighLightView> iterator = mOverlayViews.iterator();
        while (iterator.hasNext()) {
            HighLightView view = iterator.next();
            if (getScale() != 1) {
                float[] mValues = new float[9];
                getImageMatrix().getValues(mValues);
                final float scale = mValues[Matrix.MSCALE_X];

                if (!mScaleWithContent) {
                    view.getCropRectF().offset(-deltaX/scale, -deltaY/scale);
                }
            }
            view.getMatrix().set(getImageMatrix());
            view.invalidate();
        }
    }

    @Override
    protected void postScale(float scale, float centerX, float centerY) {
        if (mOverlayViews.size() > 0) {
            Iterator<HighLightView> iterator = mOverlayViews.iterator();
            Matrix oldMatrix = new Matrix(getImageViewMatrix());
            super.postScale(scale, centerX, centerY);
            while (iterator.hasNext()) {
                HighLightView view = iterator.next();

                if (!mScaleWithContent) {
                    RectF cropRect = view.getCropRectF();
                    RectF rect1 = view.getDisplayRect(oldMatrix, view.getCropRectF());
                    RectF rect2 = view.getDisplayRect(getImageViewMatrix(), view.getCropRectF());

                    float[] mValues = new float[9];
                    getImageViewMatrix().getValues(mValues);
                    final float currentScale = mValues[Matrix.MSCALE_X];

                    cropRect.offset((rect1.left - rect2.left) / currentScale, (rect1.top - rect2.top) / currentScale);
                    cropRect.right += -(rect2.width() - rect1.width()) / currentScale;
                    cropRect.bottom += -(rect2.height() - rect1.height()) / currentScale;

                    view.getMatrix().set(getImageMatrix());
                    view.getCropRectF().set(cropRect);
                } else {
                    view.getMatrix().set(getImageMatrix());
                }
                view.invalidate();
            }
        } else {
            super.postScale(scale, centerX, centerY);
        }
    }

    private void ensureVisible(HighLightView highLightView, float deltaX, float deltaY) {
        RectF rect = highLightView.getDrawRect();
        int panDeltaX1 = 0, panDeltaX2 = 0;
        int panDeltaY1 = 0, panDeltaY2 = 0;

        if (deltaX > 0) {
            panDeltaX1 = (int) Math.max(0, getLeft() - rect.left);
        }

        if (deltaX < 0) {
            panDeltaX2 = (int) Math.min(0, getRight() - rect.right);
        }

        if (deltaY < 0) {
            panDeltaY1 = (int) Math.max(0, getTop() - rect.top);
        }

        if (deltaY > 0) {
            panDeltaY2 = (int) Math.min(0, getBottom() - rect.bottom);
        }

        int panDeltaX = panDeltaX1 != 0 ? panDeltaX1 : panDeltaX2;
        int panDeltaY = panDeltaY1 != 0 ? panDeltaY1 : panDeltaY2;

        if (panDeltaX != 0 || panDeltaY != 0) {
            panBy(panDeltaX, panDeltaY);
        }
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e) {
        // iterate the items and post a single tap event to the selected item
        Iterator<HighLightView> iterator = mOverlayViews.iterator();
        while (iterator.hasNext()) {
            HighLightView view = iterator.next();
            if (view.isSelected()) {
                view.onSingleTapConfirmed(e.getX(), e.getY());
                postInvalidate();
            }
        }
        return super.onSingleTapConfirmed(e);
    }

    boolean mScrollStarted;
    float mLastMotionScrollX, mLastMotionScrollY;

    @Override
    public boolean onDown(MotionEvent event) {
        LogUtils.LOGI(TAG, "onDown: ");
        mScrollStarted = false;

        mLastMotionScrollX = event.getX();
        mLastMotionScrollY = event.getY();
        // return the item being clicked
        HighLightView newSelection = checkSelection(event);
        HighLightView realNewSelection = newSelection;

        if (newSelection == null && mOverlayViews.size() == 1 && mForceSingleSelection) {
            newSelection = mOverlayViews.get(0);
        }
        setSelectedHighLightView(newSelection);
        if (realNewSelection != null && mScaleWithContent) {
            RectF displayRect = realNewSelection.getDisplayRect(realNewSelection.getMatrix(), realNewSelection.getCropRectF());
            boolean invalidSize = realNewSelection.getContent().validateSize(displayRect);
            LogUtils.LOGD(TAG, "invalidSize: " + invalidSize);

            if (!invalidSize) {
                LogUtils.LOGW(TAG, "Drawable Twoo Small!!!");

                float minWidth = realNewSelection.getContent().getMinWidth();
                float minHeight = realNewSelection.getContent().getMinHeight();

                LogUtils.LOGD(TAG, "MinWidth: " + minWidth);
                LogUtils.LOGD(TAG, "MinHeight: " + minHeight);

                float minSize = Math.min(minWidth, minHeight) * 1.1f;
                LogUtils.LOGD(TAG, "MinSize: " + minSize);

                float minRectSize = Math.min(displayRect.width(), displayRect.height());
                LogUtils.LOGD(TAG, "MinRectSize: " + minRectSize);

                float diff = minSize / minRectSize;
                LogUtils.LOGD(TAG, "Diff: " + diff);

                LogUtils.LOGD(TAG, "Min.Size: " + minWidth + "x" + minHeight);
                LogUtils.LOGD(TAG, "Current.Size: " + displayRect.width() + "x" + displayRect.height());
                LogUtils.LOGD(TAG, "ZoomingTo: " + (getScale() * diff));

                zoomTo(getScale() * diff, displayRect.centerX(), displayRect.centerY(), DEFAULT_ANIMATION_DURATION * 1.5f);
                return true;
            }
        }

        if (mOverlayView != null) {
            int edge = mOverlayView.getHit(event.getX(), event.getY());
            if (edge != HighLightView.NONE) {
                mOverlayView.setMode((edge == HighLightView.MOVE) ? HighLightView.MOVE : (edge == HighLightView.ROTATE ? HighLightView
                        .ROTATE : HighLightView.GROW));
                postInvalidate();
                if (mDrawableEventListener != null) {
                    mDrawableEventListener.onDown(mOverlayView);
                }
            }
        }
        return super.onDown(event);
    }

    @Override
    public boolean onUp(MotionEvent event) {
        LogUtils.LOGD(TAG, "onUp");
        if (mOverlayView != null) {
            mOverlayView.setMode(HighLightView.NONE);
            postInvalidate();
        }
        return super.onUp(event);
    }

    @Override
    public boolean onSingleTapUp(MotionEvent event) {
        LogUtils.LOGI(TAG, "onSingleTapUp");

        if (mOverlayView != null) {
            int edge = mOverlayView.getHit(event.getX(), event.getY());
            if ((edge & HighLightView.MOVE) == HighLightView.MOVE) {
                if (mDrawableEventListener != null) {
                    mDrawableEventListener.onClick(mOverlayView);
                }
                return true;
            }

            mOverlayView.setMode(HighLightView.NONE);
            postInvalidate();
            LogUtils.LOGD(TAG, "Selected Items: " + mOverlayViews.size());

            if (mOverlayViews.size() != 1) {
                setSelectedHighLightView(null);
            }
        }
        return super.onSingleTapUp(event);
    }

    @Override
    public boolean onScroll(MotionEvent event1, MotionEvent event2, float distanceX, float distanceY) {
        LogUtils.LOGI(TAG, "onScroll");
        float dx, dy;

        float x = event2.getX();
        float y = event2.getY();

        if (!mScrollStarted) {
            dx = 0;
            dy = 0;
            mScrollStarted= true;
        } else {
            dx = mLastMotionScrollX -x;
            dy = mLastMotionScrollY - y;
        }
        mLastMotionScrollX = x;
        mLastMotionScrollY = y;

        if (mOverlayView != null && mOverlayView.getMode() != HighLightView.NONE) {
            mOverlayView.onMouseMove(mOverlayView.getMode(), event2, -dx, -dy);
            postInvalidate();

            if (mDrawableEventListener != null) {
                mDrawableEventListener.onMove(mOverlayView);
            }

            if (mOverlayView.getMode() == HighLightView.MOVE) {
                if (!mScaleWithContent) {
                    ensureVisible(mOverlayView, distanceX, distanceY);
                }
            }
            return true;
        } else {
            return super.onScroll(event1, event2, distanceX, distanceY);
        }
    }

    @Override
    public boolean onFling(MotionEvent event1, MotionEvent event2, float velocityX, float velocityY) {
        LogUtils.LOGD(TAG, "onFling");
        if (mOverlayView != null && mOverlayView.getMode() != HighLightView.NONE) {
            return false;
        }
        return super.onFling(event1, event2, velocityX, velocityY);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        boolean shouldInvalidateAfter = false;
        for (int index = 0; index < mOverlayViews.size(); index++) {
            canvas.save(Canvas.MATRIX_SAVE_FLAG);

            HighLightView current = mOverlayViews.get(index);
            current.draw(canvas);

            if (!shouldInvalidateAfter) {
                FeatherDrawable content = current.getContent();
                if (content instanceof EditableDrawable) {
                    if (((EditableDrawable) content).isEditing()) {
                        shouldInvalidateAfter = true;
                    }
                }
            }
            canvas.restore();
        }

        if (null != mDropPaint) {
            getDrawingRect(mTempRect);
            canvas.drawRect(mTempRect, mDropPaint);
        }

        if (shouldInvalidateAfter) {
            postInvalidateDelayed(EditableDrawable.CURSOR_BLINK_TIME);
        }
    }

    public void clearOverlays() {
        LogUtils.LOGD(TAG, "ClearOverlays");
        setSelectedHighLightView(null);
        while (mOverlayViews.size() > 0) {
            HighLightView highLightView  = mOverlayViews.remove(0);
            highLightView.dispose();
        }

        mOverlayView = null;
    }

    public float getLastMotionScrollX() {
        return mLastMotionScrollX;
    }

    public float getLastMotionScrollY() {
        return mLastMotionScrollY;
    }

    private HighLightView checkSelection(MotionEvent event) {
        Iterator<HighLightView> iterator = mOverlayViews.iterator();
        HighLightView selection = null;
        while (iterator.hasNext()) {
            HighLightView view = iterator.next();
            int edge = view.getHit(event.getX(), event.getY());
            if (edge == HighLightView.NONE) {
                selection = view;
            }
        }
        return selection;
    }

    public void commit(Canvas canvas) {
        HighLightView view;
        for (int index = 0; index < getHighLightCount(); index++) {
            view = getHighLightViewAt(index);
            FeatherDrawable content = view.getContent();
            if (content instanceof EditableDrawable) {
                ((EditableDrawable) content).endEdit();
            }

            Matrix rotateMatrix = view.getCropRotationMatrix();
            Rect rect = view.getCropRect();

            int saveCount = canvas.save(Canvas.MATRIX_SAVE_FLAG);
            canvas.concat(rotateMatrix);
            content.setBounds(rect);
            content.draw(canvas);
            canvas.restoreToCount(saveCount);
        }
    }

    public int getHighLightCount() {
        return mOverlayViews.size();
    }

    public HighLightView getHighLightViewAt(int index) {
        return mOverlayViews.get(index);
    }

    public boolean addHighLightView(HighLightView highLightView) {
        for (int index = 0; index < mOverlayViews.size(); index++) {
            if (mOverlayViews.get(index).equals(highLightView)) {
                return false;
            }
        }
        mOverlayViews.add(highLightView);
        postInvalidate();

        if (mOverlayViews.size() == 1) {
            setSelectedHighLightView(highLightView);
        }

        return true;
    }

    public boolean removeHighLightView(HighLightView view) {
        LogUtils.LOGD(TAG, "removeHighLightView");
        for (int index = 0; index < mOverlayViews.size(); index++) {
            if (mOverlayViews.get(index).equals(view)) {
                HighLightView hv = mOverlayViews.remove(index);
                if (hv.equals(mOverlayView)) {
                    setSelectedHighLightView(null);
                }

                hv.dispose();
                return true;
            }
        }
        return false;
    }

    @Override
    protected void onZoomAnimationCompleted(float scale) {
        LogUtils.LOGD(TAG, "onZoomAnimationCompleted: " + scale);
        super.onZoomAnimationCompleted(scale);

        if (mOverlayView != null) {
            mOverlayView.setMode(HighLightView.MOVE);
            postInvalidate();
        }
    }

    public HighLightView getSelectedHighLightView() {
        return mOverlayView;
    }

    public void setSelectedHighLightView(HighLightView newView) {
        final HighLightView oldView = mOverlayView;

        if (mOverlayView != null && !mOverlayView.equals(newView)) {
            mOverlayView.setSelected(false);
        }

        if (newView != null) {
            newView.setSelected(true);
        }

        postInvalidate();
        mOverlayView = newView;

        if (mDrawableEventListener != null) {
            mDrawableEventListener.onFocusChanged(newView, oldView);
        }
    }
}
