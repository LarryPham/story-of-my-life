package co.soml.android.app.models;

import android.graphics.Bitmap;
import android.text.TextUtils;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its
 * documentation are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable
 * form without the prior written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with
 * respect to the contents, and assumes no responsibility for any errors that might appear in the software and
 * documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.02.2015
 */
public class Image {
    private String mURL = null;
    private String mTitle = null;
    private String mLink = null;
    private int mWidth = 0;
    private int mHeight = 0;
    private String mDescription = null;

    private Bitmap mBitmap = null;

    public Image() {

    }

    public Image(Bitmap bitmap) {
        mBitmap = bitmap;
        setHeight(bitmap.getHeight());
        setWidth(bitmap.getWidth());
    }

    public Image(String url, String title, String link) {
        this.mURL = url;
        this.mTitle = title;
        this.mLink = link;
    }

    public String getURL() {
        return mURL;
    }

    public void setURL(String URL) {
        mURL = URL;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getLink() {
        return mLink;
    }

    public void setLink(String link) {
        mLink = link;
    }

    public int getWidth() {
        return mWidth;
    }

    public void setWidth(int width) {
        mWidth = width;
    }

    public int getHeight() {
        return mHeight;
    }

    public void setHeight(int height) {
        mHeight = height;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public Bitmap getBitmap() {
        return mBitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        mBitmap = bitmap;
        setHeight(bitmap.getHeight());
        setWidth(bitmap.getWidth());
    }

    public void setWidth(String szWidth) {
        int width = 0;
        if (!TextUtils.isEmpty(szWidth)) {
            width = Integer.parseInt(szWidth) + 1;
        }
        this.mWidth = width;
    }

    public void setHeight(String szHeight) {
        int height = 0;
        if (!TextUtils.isEmpty(szHeight)) {
            height = Integer.parseInt(szHeight) + 1;
        }
        this.mHeight = height;
    }
}
