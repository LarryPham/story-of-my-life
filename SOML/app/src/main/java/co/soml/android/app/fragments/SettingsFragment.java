package co.soml.android.app.fragments;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.widget.ProgressBar;

import co.soml.android.app.R;
import co.soml.android.app.StoryApp;
import co.soml.android.app.activities.AboutActivity;
import co.soml.android.app.activities.HelpActivity;
import co.soml.android.app.activities.SessionsActivity;
import co.soml.android.app.activities.TermsPoliciesActivity;
import co.soml.android.app.utils.CommonUtil;
import co.soml.android.app.utils.LogUtils;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since May.18.2015
 */
public class SettingsFragment extends PreferenceFragment {
    public static final String TAG = LogUtils.makeLogTag(SettingsFragment.class.getSimpleName());
    private AlertDialog mDialog;
    private SharedPreferences mSettings = null;
    private PreferenceScreen mGeneralRoot = null;
    private PreferenceScreen mNotificationPref = null;
    private PreferenceScreen mAboutPref = null;
    private PreferenceScreen mHelpAndSupportPref = null;
    private PreferenceScreen mTermPoliciesPref = null;
    private PreferenceScreen mStorySignOutPref = null;
    private PreferenceScreen mAppLanguage = null;

    private StoryApp mStoryApp;
    private ProgressBar mProgressBar = null;

    public static final String KEY_ROOT = "story_pref_app_settings_root";
    public static final String KEY_NOTIFICATION = "story_pref_app_settings_notifications";
    public static final String KEY_APP_ABOUT = "story_pref_app_about";
    //public static final String KEY_TERM_AND_POLICIES = "terms_and_policies";
    public static final String KEY_HELP_AND_SUPPORT = "story_pref_app_help_and_support";
    public static final String KEY_SIGN_OUT = "story_pref_app_signout";
    public static final String KEY_APP_LANGUAGE = "story_pref_app_language";

    private final Preference.OnPreferenceClickListener mSignInPreferenceClickListener = new Preference.OnPreferenceClickListener() {
        @Override
        public boolean onPreferenceClick(Preference preference) {
            final Intent intent = new Intent(getActivity(), SessionsActivity.class);
            return false;
        }
    };

    private final Preference.OnPreferenceClickListener launchActivityClickListener = new Preference.OnPreferenceClickListener() {
        @Override
        public boolean onPreferenceClick(Preference preference) {
            Intent intent = new Intent(getActivity(), AboutActivity.class);
            if (getActivity().getString(R.string.pref_key_app_about).equals(preference.getKey())) {
                intent = new Intent(getActivity(), AboutActivity.class);
            } else if (getActivity().getString(R.string.pref_key_terms_and_policies).equals(preference.getKey())) {
                intent = new Intent(getActivity(), TermsPoliciesActivity.class);
            } else if (getActivity().getString(R.string.pref_key_help_and_support).equals(preference.getKey())) {
                intent = new Intent(getActivity(), HelpActivity.class);
            }
            startActivity(intent);
            return true;
        }
    };

    private final Preference.OnPreferenceChangeListener mPreferenceChangeListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            if (newValue != null) {
                preference.setSummary(newValue.toString());
            }
            CommonUtil.hideKeyboard(getActivity());
            return true;
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Resources resources = getResources();
        getPreferenceManager().setSharedPreferencesName("GeneralSettings");
        addPreferencesFromResource(R.xml.general_settings);
        mStoryApp = (StoryApp) getActivity().getApplicationContext();

        mGeneralRoot = (PreferenceScreen) findPreference(KEY_ROOT);
//        mNotificationPref = (PreferenceScreen) findPreference(KEY_NOTIFICATION);
        mAboutPref = (PreferenceScreen) findPreference(KEY_APP_ABOUT);
        //mTermPoliciesPref = (PreferenceScreen) findPreference(KEY_TERM_AND_POLICIES);
        mHelpAndSupportPref = (PreferenceScreen) findPreference(KEY_HELP_AND_SUPPORT);
        mStorySignOutPref = (PreferenceScreen) findPreference(KEY_SIGN_OUT);
        mAppLanguage = (PreferenceScreen) findPreference(KEY_APP_LANGUAGE);

        mNotificationPref.setOnPreferenceChangeListener(mPreferenceChangeListener);
        mAboutPref.setOnPreferenceChangeListener(mPreferenceChangeListener);
        //mTermPoliciesPref.setOnPreferenceChangeListener(mPreferenceChangeListener);
        mHelpAndSupportPref.setOnPreferenceChangeListener(mPreferenceChangeListener);
        mStorySignOutPref.setOnPreferenceChangeListener(mPreferenceChangeListener);
        mAppLanguage.setOnPreferenceChangeListener(mPreferenceChangeListener);
    }

}
