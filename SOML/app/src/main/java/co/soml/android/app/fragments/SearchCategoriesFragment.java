/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 7/23/15 3:19 PM.
 **/

package co.soml.android.app.fragments;

import android.os.Bundle;

import co.soml.android.app.StoryApp;
import co.soml.android.app.activities.StoryMainActivity;
import co.soml.android.app.models.StoryBean;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.utils.LogUtils;

/**
 * The class <code>SearchCategoriesFragment</code> represent to display the main screen for arranging the categories of stories, peoples,
 * or publications list. After input the query-keyword into the Search TextInput, we will have a process to search the contents that had
 * been suitable with this keyword and then displaying them into the list of some categories like 'stories, people, publications, or tags'.
 */
public class SearchCategoriesFragment extends BaseFragment {

    public static final String TAG = LogUtils.makeLogTag(SearchCategoriesFragment.class.getSimpleName());
    public StoryApp mApp;
    public StoryMainActivity mActivity;
    public StoryDataModel mDataModel;
    public StoryBean mBean;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (StoryMainActivity) this.getActivity();
        mApp = (StoryApp) mActivity.getApplicationContext();
        mDataModel = mApp.getAppDataModel();
        mBean = StoryMainActivity.getBean();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void invalidate() {

    }

    @Override
    public void invalidate(Object... params) {

    }
}
