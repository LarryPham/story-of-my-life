package co.soml.android.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import co.soml.android.app.services.tasks.Action;
import co.soml.android.app.utils.LogUtils;

public class StoryAppReceiver extends BroadcastReceiver {

	public static final String TAG = LogUtils.makeLogTag(StoryAppReceiver.class.getSimpleName());
	private SharedPreferences mPrefs;

	@Override
	public void onReceive(Context context, Intent arg1) {
		LogUtils.LOGD(TAG, "Received Action: " + arg1.getAction());
		String action = arg1.getAction();

		if (action.equals(Intent.ACTION_BOOT_COMPLETED) || action.equals(Intent.ACTION_EXTERNAL_APPLICATIONS_AVAILABLE)) {
			final Intent intent = new Intent();
			intent.setAction(Action.REQUEST_TO_SET_ALARM_FOR_AUTO_UPDATE);
			context.startService(intent);
		} else if (action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
			StoryApp app = (StoryApp) context.getApplicationContext();
			ConnectivityManager manager = (ConnectivityManager) app.getSystemService(Context.CONNECTIVITY_SERVICE);
			mPrefs = app.getSharedPreferences("GeneralSettings", Context.MODE_PRIVATE);
			boolean wifiOnly = mPrefs.getBoolean("wifi_only", false);
			LogUtils.LOGD(TAG, "CONNECTIVITY_ACTION");

			boolean isConnectWifi = false;
			boolean isConnect3G = false;
			boolean isConnectWimax = false;

			NetworkInfo networkInfo = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
			if (networkInfo != null && networkInfo.isConnected()) {
				isConnectWifi = true;
			} else if (networkInfo == null) {
				LogUtils.LOGE(TAG, "checkConnectivityMode() - TYPE_WIFI NetworkInfo is null!!");
			}
			networkInfo = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
			if (networkInfo != null && networkInfo.isConnected()) {
				isConnect3G = true;
			} else if (networkInfo != null) {
				LogUtils.LOGE(TAG, "checkConnectivityMode() - TYPE_MOBILE NetworkInfo is null!!");
			}
			networkInfo = manager.getNetworkInfo(ConnectivityManager.TYPE_WIMAX);
			if (networkInfo != null && networkInfo.isConnected()) {
				isConnectWimax = true;
			} else if (networkInfo == null) {
				LogUtils.LOGE(TAG, "checkConnectivityMode(): - TYPE_WIMAX NetworkInfo is null!!");
			}

			LogUtils.LOGD(TAG, String.format("checkConnectivityMode(): -isWifi = %s, is3G = %s, isWimax = %s",
					String.valueOf(isConnectWifi), String.valueOf(isConnect3G), String.valueOf(isConnectWimax)));

		}
	}
}
