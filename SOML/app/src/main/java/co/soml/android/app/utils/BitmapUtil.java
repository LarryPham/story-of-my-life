package co.soml.android.app.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.View;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import co.soml.android.app.AppConstants;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its
 * documentation are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable
 * form without the prior written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with
 * respect to the contents, and assumes no responsibility for any errors that might appear in the software and
 * documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.02.2015
 */
public class BitmapUtil {
    private static final String TAG = AppConstants.PREFIX + BitmapUtil.class.getSimpleName();

    public static class Size {
        private int mWidth;
        private int mHeight;

        public Size(int width, int height) {
            mWidth = width;
            mHeight = height;
        }

        public int getWidth() {
            return mWidth;
        }

        public void setWidth(int width) {
            mWidth = width;
        }

        public int getHeight() {
            return mHeight;
        }

        public void setHeight(int height) {
            mHeight = height;
        }
    }

    public static class ResourcesPool {
        private int mMaxItemCount = 30;
        private int mRecycleItemCount = 10;

        private static HashMap<String, ResourcesPool> mBitmapPoolMap = new HashMap<String, ResourcesPool>();
        private List<BitmapItem> mItemList = new ArrayList<BitmapItem>();

        public static ResourcesPool resolve(String key) {
            if (mBitmapPoolMap.containsKey(key)) {
                return mBitmapPoolMap.get(key);
            } else {
                ResourcesPool pool = new ResourcesPool();
                mBitmapPoolMap.put(key, pool);
                return pool;
            }
        }

        public ResourcesPool() {

        }

        public ResourcesPool(int maxItemCount, int recycleItemCount) {
            mMaxItemCount = maxItemCount;
            mRecycleItemCount = recycleItemCount;
        }

        public Bitmap getBitmap(String key) {
            BitmapItem item = null;
            for (Iterator<BitmapItem> iterator = mItemList.iterator(); iterator.hasNext(); ) {
                item = iterator.next();
                if (item.mKey.equals(key)) {
                    if (mItemList.remove(item)) {
                        mItemList.add(item);
                        Log.i(TAG, String.format("Bitmap(ID:%s) move first position, total(%d)", item.mKey, mItemList.size()));
                    }
                    return item.mBitmap;
                }
            }
            return null;
        }

        public void putBitmap(String key, Bitmap bitmap) {
            BitmapItem item = null;
            for (Iterator<BitmapItem> iterator = mItemList.iterator(); iterator.hasNext(); ) {
                item = iterator.next();
                if (item.mKey.equals(key)) {
                    return;
                }
            }
            item = new BitmapItem(key, bitmap);
            if (mItemList.size() >= mMaxItemCount) {
                for (int i = 0; i < mRecycleItemCount; i++) {
                    final BitmapItem oldItem = mItemList.get(0);
                    oldItem.mBitmap.recycle();
                    Log.i(TAG, String.format("Bitmap(ID:%s) recycled", oldItem.mKey));
                    mItemList.remove(0);
                }
            }
            mItemList.add(item);
        }

        public void clear() {
            for (Iterator<BitmapItem> iterator = mItemList.iterator(); iterator.hasNext(); ) {
                iterator.next().mBitmap.recycle();
            }

            mItemList.clear();
            Log.i(TAG, String.format("All bitmaps recycled"));
        }

        private class BitmapItem {
            public String mKey;
            public Bitmap mBitmap;

            private BitmapItem(String key, Bitmap bitmap) {
                mKey = key;
                mBitmap = bitmap;
            }
        }
    }

    public static Size getBitmapSize(String fileName) {
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(fileName, options);
            return new Size(options.outWidth, options.outHeight);
        } catch (Exception ex) {
            return null;
        }
    }

    public static Size getBitmapSize(byte[] bytes) {
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(bytes, 0, bytes.length, options);
            return new Size(options.outWidth, options.outHeight);
        } catch (Exception ex) {
            return null;
        }
    }

    public static int calculateSampleSize(InputStream inStream, Size optionSize) {
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(inStream, null, options);

            if (options.outWidth / 16 > optionSize.getWidth()) {
                return 16;
            } else if (options.outWidth / 8 > optionSize.getWidth()) {
                return 8;
            } else if (options.outWidth / 4 > optionSize.getWidth()) {
                return 4;
            } else if (options.outWidth / 2 > optionSize.getWidth()) {
                return 2;
            } else {
                return 1;
            }
        } catch (Exception ex) {
            return 1;
        }
    }

    public static int calculateSampleSize(byte buffers[], Size optionSize) {
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(buffers, 0, buffers.length, options);

            if (options.outWidth / 16 > optionSize.getWidth()) {
                return 16;
            } else if (options.outWidth / 8 > optionSize.getWidth()) {
                return 8;
            } else if (options.outWidth / 4 > optionSize.getWidth()) {
                return 4;
            } else if (options.outWidth / 2 > optionSize.getWidth()) {
                return 2;
            } else {
                return 1;
            }
        } catch (Exception ex) {
            return 1;
        }
    }

    public static void setBackgroundImage(Context context, View view, int resId) {
        InputStream is = context.getResources().openRawResource(resId);
        BitmapDrawable bitmapDrawable = new BitmapDrawable(is);
        view.setBackgroundDrawable(bitmapDrawable);
    }
}
