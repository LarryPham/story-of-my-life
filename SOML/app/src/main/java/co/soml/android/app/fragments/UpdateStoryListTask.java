/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 * @author: Larry Pham (email: larrypham.vn@gmail.com)
 * Date: 6/12/15 12:30 PM
 **/

package co.soml.android.app.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import co.soml.android.app.controllers.BaseController;
import co.soml.android.app.utils.LogUtils;

public class UpdateStoryListTask extends AsyncTask<Void, Void, List<Map<String, Object>>> {
    public static final String TAG = LogUtils.makeLogTag(UpdateStoryListTask.class.getSimpleName());
    protected int mErrorMessageId;
    protected Context mContext;
    protected boolean mStoryListChanged;
    protected static List<Map<String, Object>> mUserStoryList;
    protected BaseController mController;

    public UpdateStoryListTask(Context context, BaseController controller) {
        this.mContext = context;
        mController = controller;
    }

    @Override
    protected void onPreExecute() {

    }

    @Override
    protected List<Map<String, Object>> doInBackground(Void... params) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(mContext);
        final CountDownLatch countDownLatch = new CountDownLatch(1);

        return null;
    }

    public static class GenericUpdateStoryListTask extends UpdateStoryListTask {

        public GenericUpdateStoryListTask(Context context, BaseController controller) {
            super(context, controller);
        }

        @Override
        protected void onPostExecute(List<Map<String, Object>> maps) {
            if (mStoryListChanged) {
                //mController.sendMessage(ControllerMessage.);
            }
        }
    }
}
