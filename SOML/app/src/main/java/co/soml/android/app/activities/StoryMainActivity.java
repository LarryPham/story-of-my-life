package co.soml.android.app.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.lang.ref.WeakReference;

import co.soml.android.app.AppConstants;
import co.soml.android.app.R;
import co.soml.android.app.StoryApp;
import co.soml.android.app.controllers.BaseController;
import co.soml.android.app.controllers.ControllerMessage;
import co.soml.android.app.controllers.MainStoryController;
import co.soml.android.app.controllers.StoryMainController;
import co.soml.android.app.fragments.BaseFragment;
import co.soml.android.app.fragments.adapters.MainPagerAdapter;
import co.soml.android.app.models.Account;
import co.soml.android.app.models.StoryBean;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.models.StoryFeedList;
import co.soml.android.app.models.StoryTypes;
import co.soml.android.app.models.User;
import co.soml.android.app.services.StoryService;
import co.soml.android.app.services.tasks.Action;
import co.soml.android.app.services.tasks.WeakHandler;
import co.soml.android.app.utils.AccountUtils;
import co.soml.android.app.utils.CommonUtil;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.widgets.CircleImageView;
import co.soml.android.app.widgets.StoryTextView;
import co.soml.android.app.widgets.StoryViewPager;

public class StoryMainActivity extends BaseActivity {

    public static final String TAG = LogUtils.makeLogTag(StoryMainActivity.class.getSimpleName());

    public static final int REQUEST_TO_ENTER_URL = 0;
    public static final int REQUEST_TO_DISCLAIMER = 1;
    public static final int REQUEST_TO_ADD_SUBS = 2;
    public static final int REQUEST_TO_POPUP = 3;
    public static final int REQUEST_TO_DISPLAY_PROFILE = 4;
    public static final int REQUEST_TO_CONFIGURE_PREFS = 5;
	public static final int RESULT_SIGNED_OUT = 11;

    private static final String EMAIL_ACCOUNT_TYPE = "email";
    private static final String FACEBOOK_ACCOUNT_TYPE = "facebook";
    private static final String TWITTER_ACCOUNT_TYPE = "twitter";
    private static final String GUEST_ACCOUNT_TYPE = "guest";

    private StoryDataModel mDataModel;
    private MainHandler mMainHandler;

    private Intent mActionViewIntent = null;
    private TabLayout mMainPagerTab;
    private StoryViewPager mMainPager;

    private MainPagerAdapter mMainPagerAdapter;
    private MainStoryController mController;
    private boolean mForceClose = false;
    private BroadcastReceiver mConnectReceiver;

    private int mDefaultPageIndex;
    private StoryApp mStoryApp;

    private StoryFeedList mStoryFeedList;
    private Toolbar mActionToolBar;

    public static StoryBean sStoryBean = null;

    public Bundle mMainPageParams;
    public MainViewState mViewState;

    /*public static final int[] TAB_ICONS_DRAWABLES = new int[] {
            R.drawable.ic_tab_home,
            R.drawable.ic_tab_mystory
    };
*/
    public boolean mIsFeedsFragment = false;
    public boolean mIsSearchFragment = false;
    public boolean mIsMyStoryFragment = false;
    public boolean mIsSocialFragment = false;
    public BaseFragment mFragment;

    public static StoryBean getBean() {
        if (sStoryBean == null) {
            sStoryBean = new StoryBean();
        }
        return sStoryBean;
    }

    public static void setBean(StoryBean bean) {
        StoryMainActivity.sStoryBean = bean;
    }

    public class MainHandler extends Handler {
        public final static int mAlertNewVersion = 1;
        public final static int mCheckNewVersion = 2;
        public final static int mRequestNotice = 3;
        public final static int mRequestAccountDetail = 4;

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case mAlertNewVersion: {
                    onNotifyNewVersionAvailable();
                    break;
                }
                case mCheckNewVersion: {
                    sendRequestToCheckNewVersion();
                    break;
                }
                case mRequestNotice: {
                    sendRequestNotice();
                    break;
                }
                case mRequestAccountDetail: {
                    sendRequestAccountDetail();
                    break;
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_main);

        mStoryApp = (StoryApp) getApplicationContext();
        mDataModel = mStoryApp.getAppDataModel();
        if (mController == null) {
            mController = new MainStoryController(StoryMainActivity.this, mDataModel);
        }

        final Intent intent = getIntent();
        final Bundle param = intent != null ? intent.getExtras() : null;
        final User currentUser = param != null ? (User) param.get("CURRENT_USER") : null;

        sStoryBean = getBean();
        sStoryBean.setStoryDataModel(mDataModel);
        sStoryBean.setPrefs(getSharedPreferences("GeneralSettings", Context.MODE_PRIVATE));
        sStoryBean.setEditor(sStoryBean.getPrefs().edit());

        sStoryBean.setViewByPrefs(getSharedPreferences("GeneralSettings", Context.MODE_PRIVATE));
        sStoryBean.setViewByEditor(sStoryBean.getViewByPrefs().edit());
        onCreatePostExecute();

        getSupportFragmentManager().addOnBackStackChangedListener(this);
    }

    public void onBindingUI() {
        final MainPagerAdapter.MainFragment[] mainFragments = MainPagerAdapter.MainFragment.values();
        final String mainPagerType = getString(R.string.main_pager_type);
        mMainPagerAdapter = new MainPagerAdapter(StoryMainActivity.this, getSupportFragmentManager(), mainPagerType);
        for (MainPagerAdapter.MainFragment fragment : mainFragments) {
            if (fragment == MainPagerAdapter.MainFragment.HOME) {
                mMainPageParams = buildArguments(StoryDataModel.MainPagerType.HOME_MAIN_PAGE);
                mMainPagerAdapter.add(fragment.getFragmentClass(), mMainPageParams, R.string.nav_drawer_collections_item_content);
            } else if (fragment == MainPagerAdapter.MainFragment.STORY) {
                mMainPageParams = buildArguments(StoryDataModel.MainPagerType.MY_STORY_PAGE);
                mMainPagerAdapter.add(fragment.getFragmentClass(), mMainPageParams, R.string.nav_drawer_mystory_item_content);
            }
        }

        mMainPager = (StoryViewPager) findViewById(R.id.main_view_pager);
        mMainPager.setOffscreenPageLimit(mMainPagerAdapter.getCount() - 1);
        mMainPagerTab = (TabLayout) findViewById(R.id.main_tabs);

        mMainPagerTab.setTabMode(TabLayout.MODE_FIXED);
        mMainPagerTab.setTabGravity(TabLayout.GRAVITY_CENTER);
        mMainPager.setAdapter(mMainPagerAdapter);
        mMainPagerTab.setupWithViewPager(mMainPager);

        for (int index = 0; index < mMainPagerTab.getTabCount(); index++) {
            TabLayout.Tab tab = mMainPagerTab.getTabAt(index);
            View tabView = mMainPagerAdapter.getTabView(index);
            if (tab != null) {
                tab.setCustomView(tabView);
            }
        }
        mMainPagerTab.setOnTabSelectedListener(mTabSelectedListener);
    }

    private TabLayout.OnTabSelectedListener mTabSelectedListener = new TabLayout.OnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            sStoryBean.setLastTabPosition(tab.getPosition());
            if (mFragment == null) {
                mFragment = mMainPagerAdapter.getFragment(tab.getPosition());
            }
	        mMainPagerAdapter.setCurrentPage(tab.getPosition());
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {
            InputMethodManager manager = (InputMethodManager) StoryMainActivity.this.getSystemService(Activity.INPUT_METHOD_SERVICE);
            if (sStoryBean.getSearchView() != null) {
                manager.hideSoftInputFromWindow(sStoryBean.getSearchView().getWindowToken(), 0);
                mIsSearchFragment = true;
            }
            if (sStoryBean.getStoryFeedsFragment() != null) {
                mIsFeedsFragment = true;
            }
            if (sStoryBean.getMyStoryFragment() != null) {
                mIsMyStoryFragment = true;
            }
            if (sStoryBean.getPeopleFragment() != null) {
                mIsSocialFragment = true;
            }
        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {
        }
    };

    @Override
    public Toolbar getActionBarToolBar() {
        if (mActionToolBar == null) {
            mActionToolBar = (Toolbar) findViewById(R.id.main_toolbar);
            if (mActionToolBar != null) {
                setupAccountProfile();
            }
        }
        return mActionToolBar;
    }

    public void setupAccountProfile() {
        final CircleImageView avatarView = (CircleImageView) mActionToolBar.findViewById(R.id.image_avatar);
        final LinearLayout profileView = (LinearLayout) mActionToolBar.findViewById(R.id.avatar_profile_layout);

        final Account chosenAccount = AccountUtils.getActiveAccount(this);

	    final long userId = AccountUtils.getUserId(this);
	    final String authToken = AccountUtils.getAuthToken(this);

        final String name = AccountUtils.getActiveAccountName(this);
        final String accountType = AccountUtils.getActiveAccountType(this);

        if (TextUtils.isEmpty(name)) {
            mActionToolBar.findViewById(R.id.title).setVisibility(View.INVISIBLE);
        } else {
            ((StoryTextView) mActionToolBar.findViewById(R.id.title)).setText(name);
            mActionToolBar.findViewById(R.id.title).setVisibility(View.VISIBLE);
        }

        if (!TextUtils.isEmpty(accountType)) {
            if (accountType.equalsIgnoreCase(EMAIL_ACCOUNT_TYPE)) {
                final String emailProfileImageURL = AccountUtils.getEmailAccountImageURL(this, name);
                Picasso.with(this).load(emailProfileImageURL).placeholder(R.drawable.person_image_empty).into(avatarView);
            } else if (accountType.equalsIgnoreCase(FACEBOOK_ACCOUNT_TYPE)) {
                final String facebookProfileImageURL = AccountUtils.getFacebookAccountProfileImageURL(this, name);
                Picasso.with(this).load(facebookProfileImageURL).placeholder(R.drawable.person_image_empty).into(avatarView);
            } else if (accountType.equalsIgnoreCase(TWITTER_ACCOUNT_TYPE)) {
                final String twitterProfileImageURL = AccountUtils.getTwitterAccountProfileImageUrl(this, name);
                Picasso.with(this).load(twitterProfileImageURL).placeholder(R.drawable.person_image_empty).into(avatarView);
            } else {
                avatarView.setImageResource(R.drawable.person_image_empty);
            }
        }

        profileView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View avatar) {
                final Intent accountIntent = new Intent(StoryMainActivity.this, UserProfileActivity.class);
                final User currentUser = mStoryApp.getCurrentUser();
                final Bundle profileParams = new Bundle();
                if (currentUser != null) {
                    profileParams.putParcelable("CHOSEN_ACCOUNT", AccountUtils.getActiveAccount(StoryMainActivity.this));
                    profileParams.putParcelable("CURRENT_USER", currentUser);
                    profileParams.putString("EDITABLE", "Editable");
                    profileParams.putInt("USER_ID", currentUser.getId());
                }

                accountIntent.putExtras(profileParams);
                startActivityForResult(accountIntent, REQUEST_TO_DISPLAY_PROFILE);
            }
        });
    }

    public Bundle buildArguments(StoryDataModel.MainPagerType mainPagerType) {
        final Bundle params = new Bundle();
        final String accountAuthToken = AccountUtils.getAuthToken(StoryMainActivity.this);
        //final int userId = AccountUtils.getUserId(StoryMainActivity.this);

        params.putSerializable(AppConstants.MAIN_PAGE_VIEW_STATE, mViewState);
        params.putSerializable(AppConstants.MAIN_PAGE_VIEW_TYPE, mainPagerType);

        params.putInt(AppConstants.X_API_VERSION, 1);
        params.putString(AppConstants.X_API_TOKEN, AccountUtils.getAuthToken(this));

        if (mainPagerType == StoryDataModel.MainPagerType.HOME_MAIN_PAGE) {
            params.putInt(AppConstants.USER_ID, AccountUtils.getUserId(this));
            params.putString(AppConstants.SCOPE_NAME, getResources().getString(R.string.latest_scope_name));
            params.putInt(AppConstants.START_INDEX, 1);
            params.putInt(AppConstants.LIMIT, 10);
            params.putSerializable(AppConstants.STORY_POST_LIST_TYPE, StoryTypes.StoryPostListType.FEATURED_TYPE);
        }  else if (mainPagerType == StoryDataModel.MainPagerType.MY_STORY_PAGE) {
            LogUtils.LOGD(TAG, String.format("[USER_ID]: %d", AccountUtils.getUserId(this)));
            params.putInt(AppConstants.USER_ID, AccountUtils.getUserId(this));
            params.putInt(AppConstants.START_INDEX, 1);
            params.putInt(AppConstants.LIMIT, 10);
            params.putString(AppConstants.STATE, getResources().getString(R.string.published_scope_name));
        }
        return params;
    }

    private void onCreatePostExecute() {
        LogUtils.LOGD(TAG, "onCreatePostExecute(): ");
        mMainHandler = new MainHandler();

        setSupportActionBar(getActionBarToolBar());
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        mMainHandler.sendMessage(mMainHandler.obtainMessage(MainHandler.mRequestAccountDetail));
        onBindingUI();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (StoryApp.getInstance().isShouldBeRefreshScreen()) {
            final MainPagerAdapter.MainFragment[] mainFragments = MainPagerAdapter.MainFragment.values();
            for (int index = 0; index < mMainPagerAdapter.getCount(); index++) {
                mMainPagerAdapter.getFragment(index).invalidate();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings: {
                final Intent settingsIntent = new Intent(this, SettingsActivity.class);
                startActivityForResult(settingsIntent, REQUEST_TO_CONFIGURE_PREFS);
                return true;
            }
	        case R.id.action_signout: {
		        //Todo logging out the current account
		        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
		        dialogBuilder.setMessage(getString(R.string.sign_out_confirm));
		        dialogBuilder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
			        @Override
			        public void onClick(DialogInterface dialog, int which) {
				        setResult(RESULT_SIGNED_OUT);
				        StoryApp.signOutAsyncWithProgressBar(StoryMainActivity.this, new StoryApp.SignOutAsync.SignOutCallback() {
					        @Override
					        public void onSignOut() {
                                final Intent welcomeIntent = new Intent(StoryMainActivity.this, WelcomeActivity.class);
                                startActivity(welcomeIntent);
                                StoryApp.getInstance().setShouldBeRefreshScreen(true);
						        StoryMainActivity.this.finish();
					        }
				        });
			        }
		        });

		        dialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
			        @Override
			        public void onClick(DialogInterface dialog, int which) {
				       StoryApp.getInstance().setShouldBeRefreshScreen(false);
			        }
		        });
		        dialogBuilder.setCancelable(true);
		        dialogBuilder.create().show();
		        return true;
	        }
        }
        return super.onOptionsItemSelected(item);
    }

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		onBindingUI();
	}

	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_TO_ENTER_URL: {
                if (resultCode == RESULT_CANCELED) {
                    finish();
                }
                break;
            }
            case REQUEST_TO_DISCLAIMER: {
                if (resultCode == RESULT_OK) {
                    checkAgreeDisclaimer();
                } else {
                    finish();
                }
            }
            case REQUEST_TO_ADD_SUBS: {
                if (resultCode == RESULT_OK) {

                }
                break;
            }
            case REQUEST_TO_DISPLAY_PROFILE: {
                if (resultCode == 1) {

                }
                break;
            }
            case REQUEST_TO_POPUP: {
                boolean retValue = false;

                if (data != null) {
                    retValue = data.getBooleanExtra("ReturnValue" , false);
                    if (retValue) {
                        CommonUtil.setEnableMobileNetwork(StoryMainActivity.this, true);
                        sStoryBean.setEnableMobileNetwork(true);
                    } else {
                        CommonUtil.setEnableMobileNetwork(StoryMainActivity.this, false);
                        sStoryBean.setEnableMobileNetwork(false);
                    }
                }
                onCreatePostExecute();
                break;
            }
        }
    }

    public void checkAgreeDisclaimer() {
        boolean isDisclaimer = sStoryBean.getViewByPrefs().getBoolean("DISCLAIMER_CONFIRM", true);
        if (!isDisclaimer) {
            Intent disclaimerIntent = new Intent(this, AboutActivity.class);
            startActivityForResult(disclaimerIntent, REQUEST_TO_DISCLAIMER);
        } else {
            showConnectionPopup();
        }
    }

    public void showConnectionPopup() {
        int result = -1;
        if (CommonUtil.checkConnectivityMode(StoryMainActivity.this) != AppConstants.CONN_WIFI) {
            result = CommonUtil.checkConnectionFailedCase(this);
            if (result <= 4 && result >= 0) {
                PackageManager pm = getPackageManager();
                try {
                    ComponentName component = new ComponentName("co.soml.android.app.popupuireceiver",
                            "co.soml.android.app.popupreceiver.popupNetworkError");
                    pm.getActivityInfo(component, PackageManager.GET_META_DATA);
                    LogUtils.LOGD(TAG, "Exist Popup Package");
                    Intent intent = new Intent();
                    intent.setClassName("co.soml.android.app.popupreceiver", "co.soml.android.app.popupreceiver.popupNetworkError");
                    intent.putExtra("NetworkErrorType", result);
                    intent.putExtra("MobileDataOnly", false);
                    startActivityForResult(intent, REQUEST_TO_POPUP);
                } catch (PackageManager.NameNotFoundException ex) {
                    LogUtils.LOGE(TAG, "There are no popup package");
                    checkConnectViaMobileNetwork();
                }
            } else {
                checkConnectViaMobileNetwork();
            }
        } else {
            onCreatePostExecute();
        }
    }

    public void checkConnectViaMobileNetwork() {
        int connectMode = CommonUtil.checkConnectivityMode(this);
        if (connectMode == AppConstants.CONN_3G) {
            if (CommonUtil.isRoaming(this)) {
                if (CommonUtil.checkDoNotShowConnectMobileNetworkDialogRoaming(this)) {
                    CommonUtil.setEnableMobileNetwork(this, true);
                    sStoryBean.setEnableMobileNetwork(true);
                    LogUtils.LOGD(TAG, "checkConnectViaMobileNetwork() onCreatePostExecute()");
                    onCreatePostExecute();
                } else {
                    LinearLayout chargeView = (LinearLayout) View.inflate(this, R.layout.incur_charge_dialog, null);
                    LinearLayout checkLayout = (LinearLayout) chargeView.findViewById(R.id.check_no_more_layout);
                    StoryTextView tv = (StoryTextView) chargeView.findViewById(R.id.check_no_more_text);
                    final CheckBox checkBox = (CheckBox) chargeView.findViewById(R.id.check_no_more_dialog);
                    tv.setText(R.string.msg_body_data_roaming);
                    CommonUtil.setEnableMobileNetwork(this, false);
                    sStoryBean.setEnableMobileNetwork(false);

                    checkLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (checkBox.isChecked()) {
                                checkBox.setChecked(false);
                            } else {
                                checkBox.setChecked(true);
                            }
                        }
                    });

                    new AlertDialog.Builder(this).setView(chargeView)
                            .setTitle(getString(R.string.msg_header_connect_via_roaming_network))
                            .setPositiveButton(R.string.btn_connect, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (checkBox.isChecked()) {
                                        SharedPreferences.Editor editor = sStoryBean.getPrefs().edit();
                                        editor.putBoolean("DO_NOT_SHOW_NETWORK_DIALOG_ROAMING", true);
                                        editor.apply();
                                    }
                                    CommonUtil.setEnableMobileNetwork(StoryMainActivity.this, true);
                                    sStoryBean.setEnableMobileNetwork(true);
                                    LogUtils.LOGE(TAG, "checkConnecViaMobileNetwork() onCreatePostExecute()");
                                    onCreatePostExecute();
                                }
                            })
                            .setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    LogUtils.LOGE(TAG, "checkConnectViaMobileNetwork() onCreatePostExecute()");
                                    onCreatePostExecute();
                                }
                            })
                            .show();
                }
            } else {
                if (CommonUtil.checkDoNotShowConnectMobileNetworkDialog(this)) {
                    CommonUtil.setEnableMobileNetwork(this, true);
                    sStoryBean.setEnableMobileNetwork(true);
                    LogUtils.LOGE(TAG, "checkConnectViaMobileNetwork() onCreatePostExecute()");
                    onCreatePostExecute();
                } else  {
                    LinearLayout chargeView = (LinearLayout) View.inflate(this, R.layout.incur_charge_dialog, null);
                    LinearLayout checkLayout = (LinearLayout) chargeView.findViewById(R.id.check_no_more_layout);
                    StoryTextView tv = (StoryTextView) chargeView.findViewById(R.id.check_no_more_text);
                    final CheckBox checkBox = (CheckBox) chargeView.findViewById(R.id.check_no_more_dialog);
                    tv.setText(R.string.msg_body_data_roaming);
                    CommonUtil.setEnableMobileNetwork(this, false);
                    sStoryBean.setEnableMobileNetwork(false);

                    checkLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (checkBox.isChecked()) {
                                checkBox.setChecked(false);
                            } else {
                                checkBox.setChecked(true);
                            }
                        }
                    });

                    new AlertDialog.Builder(this).setView(chargeView)
                            .setTitle(getString(R.string.msg_header_connect_via_network))
                            .setPositiveButton(R.string.btn_connect, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (checkBox.isChecked()) {
                                        SharedPreferences.Editor editor = sStoryBean.getPrefs().edit();
                                        editor.putBoolean("DO_NOT_SHOW_NETWORK_DIALOG", true);
                                        editor.apply();
                                    }
                                    CommonUtil.setEnableMobileNetwork(StoryMainActivity.this, true);
                                    sStoryBean.setEnableMobileNetwork(true);
                                    LogUtils.LOGE(TAG, "checkConnecViaMobileNetwork() onCreatePostExecute()");
                                    onCreatePostExecute();
                                }
                            })
                            .setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    LogUtils.LOGE(TAG, "checkConnectViaMobileNetwork() onCreatePostExecute()");
                                    onCreatePostExecute();
                                }
                            })
                            .show();
                }
            }
        } else {
            onCreatePostExecute();
        }
    }


    @Override
    public void invalidate() {

    }

    @Override
    public void invalidate(Object... params) {
        final InvalidateParam param = (InvalidateParam) params[0];
        if (param.getMessage() == ControllerMessage.REQUEST_TO_SERVER_USER_COMPLETED) {
            if (param.getObj() != null) {
                StoryApp.getInstance().setCurrentUser((User) param.getObj());
            }
        }

    }

	public void cancelRequestContent() {

	}

	public void popupNoNetworkSnackbar() {
		onCreateSnackbar(DialogMsg.NO_NETWORK_CONNECTION, null).show();
	}

	public Snackbar onCreateSnackbar(int id, Bundle args) {
		Snackbar mDialogResult = null;
		switch (id) {
			case DialogMsg.NO_NETWORK_CONNECTION: {
				final View coordinatorLayoutView = findViewById(R.id.snackbar_position);
				mDialogResult = Snackbar.make(coordinatorLayoutView, getString(R.string.msg_no_network_connection), Snackbar.LENGTH_SHORT);
				break;
			}
		}
		return mDialogResult;
	}

    public void onNotifyNewVersionAvailable() {


    }

    public void sendRequestToCheckNewVersion() {
	    Thread storyVersionChecker = new Thread() {
		    public void run() {
			    mMainHandler.sendMessage(mMainHandler.obtainMessage(MainHandler.mRequestNotice));
		    }
	    };
	    storyVersionChecker.setName("StoryVersionChecker");
	    storyVersionChecker.start();
    }

    public void sendRequestNotice() {
        final Intent intent = new Intent(this, StoryService.class);
        intent.setAction(Action.REQUEST_TO_SERVER_NOTICE_LIST);
        intent.putExtra(Action.REQUEST_OWNER, this.hashCode());
        intent.putExtra(Action.REQUEST_MSG, ControllerMessage.REQUEST_TO_SERVER_NOTICE_LIST);
        startService(intent);
    }

    public void sendRequestAccountDetail() {
        if (mController != null) {
            final int userId = AccountUtils.getUserId(this);
            mController.sendMessage(ControllerMessage.REQUEST_TO_SERVER_USER_DETAIL, userId);
        } else {
            mController =  new MainStoryController(StoryMainActivity.this, mDataModel);
        }
    }


    public enum MainViewState implements Serializable {
        HOME, SEARCH, STORY, SOCIAL, SETTINGS
    }

	class DialogMsg {
		public static final int NO_NETWORK_CONNECTION = 11;
		public static final int NOT_CONNECT_TO_3G = 12;
		public static final int NOT_CONNECT_TO_WIFI = 13;
	}
}
