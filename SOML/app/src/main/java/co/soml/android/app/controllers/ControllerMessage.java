package co.soml.android.app.controllers;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its
 * documentation are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable
 * form without the prior written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with
 * respect to the contents, and assumes no responsibility for any errors that might appear in the software and
 * documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.01.2015
 */
public class ControllerMessage {

    public static final int BASE_CONTROL_MESSAGE = 100;

    public static final int REQUEST_OWNER_MESSAGE = BASE_CONTROL_MESSAGE + 1;
    public static final int REQUEST_DOWNLOAD_IMAGE = BASE_CONTROL_MESSAGE + 2;
    public static final int REQUEST_DOWNLOAD_IMAGE_COMPLETED = BASE_CONTROL_MESSAGE + 3;
    // Loading List of Story Page
    public static final int LOAD_LIST_OF_STORY = BASE_CONTROL_MESSAGE + 10;
    public static final int LOAD_LIST_OF_STORY_COMPLETED = BASE_CONTROL_MESSAGE + 11;
    // Image Load
    public static final int LOAD_IMAGE_OF_STORY = BASE_CONTROL_MESSAGE + 13;
    public static final int LOAD_IMAGE_OF_STORY_COMPLETED = BASE_CONTROL_MESSAGE + 14;
    // Image Size Load
    public static final int LOAD_SMALL_IMAGE_OF_STORY = BASE_CONTROL_MESSAGE + 15;
    public static final int LOAD_MEDIUM_IMAGE_OF_STORY = BASE_CONTROL_MESSAGE + 16;
    public static final int LOAD_HIGH_IMAGE_OF_STORY = BASE_CONTROL_MESSAGE + 17;
    public static final int LOAD_EXTRAS_IMAGE_OF_STORY = BASE_CONTROL_MESSAGE + 18;
    public static final int LOAD_SMALL_IMAGE_OF_STORY_COMPLETED = BASE_CONTROL_MESSAGE + 19;
    public static final int LOAD_MEDIUM_IMAGE_OF_STORY_COMPLETED = BASE_CONTROL_MESSAGE + 20;
    public static final int LOAD_HIGH_IMAGE_OF_STORY_COMPLETED = BASE_CONTROL_MESSAGE + 21;
    public static final int LOAD_EXTRA_IMAGE_OF_STORY_COMPLETED = BASE_CONTROL_MESSAGE + 22;

    public static final int REQUEST_STORY_IMAGE_FROM_SERVER_COMPLETED = 200;

    public static final int REQUEST_TO_SERVER_UPLOAD_PHOTO_STARTED = 1301;
    public static final int REQUEST_TO_SERVER_UPLOAD_PHOTO_COMPLETED = 1302;
    public static final int REQUEST_TO_SERVER_UPLOAD_PHOTO_FAILED = 1302;

    public static final int REQUEST_TO_SERVER_BASE = 300;
    public static final int REQUEST_TO_SERVER_TRENDING_STORIES = REQUEST_TO_SERVER_BASE + 1;

	public static final int AUTO_UPDATE_FEED = 75;
	public static final int AUTO_UPDATE_COMPLETED = 76;
	public static final int AUTO_UPDATE_FAILED = 77;

	public static final int AUTO_UPDATE_LIST_FEED = 78;
	public static final int AUTO_UPDATE_LIST_FEED_COMPLETED = 79;
	public static final int AUTO_UPDATE_COMMIT_COMPLETED = 80;

	public static final int CANCEL_WAITING_DIALOG = 195;
    public static final int RESET_REQUEST_COUNT = 196;

	public static final int REFRESH_VIEW_STATE = 94;
	public static final int APPLY_VIEW_STATE = 95;
	public static final int REQUEST_IS_WAITING = 97;

    // Server error reason
    public static final int REQUEST_ERROR_REASON_BASE = 400; // base error
    public static final int REQUEST_ERROR_REASON_PARAMS_MISSING = REQUEST_ERROR_REASON_BASE + 3; // Missing params
    public static final int REQUEST_ERROR_REASON_HTTP_CONNECT_FAIL = REQUEST_ERROR_REASON_BASE + 2; // Connection failed
    public static final int REQUEST_ERROR_REASON_HTTP_UNAUTHORIZED = REQUEST_ERROR_REASON_BASE + 1; // Not authorized
    public static final int REQUEST_ERROR_REASON_UNSUPPORTED_URL = REQUEST_ERROR_REASON_BASE + 4; // Unsupported URL
    public static final int REQUEST_ERROR_REASON_JSON_PARSE_EXCEPTION = REQUEST_ERROR_REASON_BASE + 5; // Parse Exception
    public static final int REQUEST_ERROR_REASON_IMAGE_DOWNLOAD_ERROR = REQUEST_ERROR_REASON_BASE + 6;
    public static final int REQUEST_ERROR_REASON_SERVER_TIMEOUT = REQUEST_ERROR_REASON_BASE + 12;
    public static final int REQUEST_ERROR_REASON_SERVER_CONNECT_TIMEOUT = REQUEST_ERROR_REASON_BASE + 7; // Timeout
    public static final int REQUEST_ERROR_REASON_SERVER_READ_TIMEOUT = REQUEST_ERROR_REASON_BASE + 8; // Read TimeOut
    public static final int REQUEST_ERROR_REASON_INTERNAL_SERVER_ERROR = REQUEST_ERROR_REASON_BASE + 9; // Interval Error
    public static final int REQUEST_ERROR_REASON_UNKNOWN_HOST = REQUEST_ERROR_REASON_BASE + 10; // Unknown Host
    public static final int REQUEST_ERROR_REASON_SERVER_DATA_CORRUPTION = REQUEST_ERROR_REASON_BASE + 11; //Data Corrupted
    // Common Issues
    public static final int REQUEST_ERROR_REASON_SERVER_NO_CONTENT = REQUEST_ERROR_REASON_BASE + 12; // No Content
    public static final int REQUEST_ERROR_REASON_SERVER_FORBIDDEN = REQUEST_ERROR_REASON_BASE + 13; // Forbidden
    public static final int REQUEST_ERROR_REASON_SERVER_NOT_FOUND = REQUEST_ERROR_REASON_BASE + 14;
    // Account errors
    public static final int REQUEST_ERROR_REASON_SERVER_INVALID_REGISTRATION = REQUEST_ERROR_REASON_BASE + 15;
    public static final int REQUEST_ERROR_REASON_SERVER_INVALID_GUEST_ACCOUNT = REQUEST_ERROR_REASON_BASE + 16;

    public static final int REQUEST_TO_SERVER_BASE_COMPLETED = 500;
    public static final int REQUEST_TO_SERVER_TRENDING_STORIES_COMPLETED = REQUEST_TO_SERVER_BASE_COMPLETED + 1;
    public static final int REQUEST_TO_SERVER_FOLLOWING_STORIES_COMPLETED = REQUEST_TO_SERVER_BASE_COMPLETED + 2;

    public static final int ACTION_WAITING_START = 700;
    public static final int ACTION_WAITING_END = 701;

    public static final int REQUEST_TO_SERVER_SIGN_IN_WITH_FACEBOOK = 750;
    public static final int REQUEST_TO_SERVER_SIGN_IN_WITH_TWITTER = 751;
    public static final int REQUEST_TO_SERVER_SIGN_IN_WITH_EMAIL_ACCOUNT = 752;
    public static final int REQUEST_TO_SERVER_SIGN_IN_GUEST = 753;

    public static final int REQUEST_TO_SERVER_NOTICE_LIST = 720;

    public static final int REQUEST_TO_SERVER_ACCOUNT_REGISTER = 754;
    public static final int REQUEST_TO_SERVER_ACCOUNT_FOLLOWING = 755;
    public static final int REQUEST_TO_SERVER_ACCOUNT_LIKED_STORY = 756;
    public static final int REQUEST_TO_SERVER_ACCOUNT_INFO = 757;

    //public static final int REQUEST_TO_SERVER_STORIES_BY_SCOPE = 758;
    public static final int REQUEST_TO_SERVER_STORIES_LATEST = 758;
    public static final int REQUEST_TO_SERVER_STORIES_FEATURED = 759;
    public static final int REQUEST_TO_SERVER_STORIES_FOLLOWING = 760;
	public static final int REQUEST_TO_SERVER_LIST_STORIES = 770;

    public static final int REQUEST_TO_SERVER_STORY_DETAIL = 783;

    public static final int REQUEST_TO_SERVER_USER_FOLLOWERS = 784;
    public static final int REQUEST_TO_SERVER_USER_FOLLOWING = 785;

    public static final int REQUEST_TO_SERVER_USER_LIST = 762;
    public static final int REQUEST_TO_SERVER_USER_DETAIL = 763;
    public static final int REQUEST_TO_SERVER_USER_FOLLOW = 764;
    public static final int REQUEST_TO_SERVER_USER_UNFOLLOW = 765;
    public static final int REQUEST_TO_SERVER_USER_REPORT = 766;

    public static final int REQUEST_TO_SERVER_STORY_REPORT = 767;
    public static final int REQUEST_TO_SERVER_STORY_LIKE = 768;
    public static final int REQUEST_TO_SERVER_STORY_UNLIKE = 769;
    public static final int REQUEST_TO_SERVER_STORY_POST = 770;
    public static final int REQUEST_TO_SERVER_STORY_UPDATE = 771;
    public static final int REQUEST_TO_SERVER_STORY_REMOVE = 772;

    public static final int REQUEST_TO_SERVER_COMMENT_REPLY = 773;
    public static final int REQUEST_TO_SERVER_COMMENT_LIKE = 774;
    public static final int REQUEST_TO_SERVER_COMMENT_UNLIKE = 775;
    public static final int REQUEST_TO_SERVER_COMMENT_STORY = 776;
    public static final int REQUEST_TO_SERVER_COMMENT_UPDATE = 777;
    public static final int REQUEST_TO_SERVER_COMMENT_DELETE = 778;
    public static final int REQUEST_TO_SERVER_COMMENT_REPORT = 779;

    public static final int REQUEST_TO_SERVER_MY_STORY_PRIVATE = 780;
    public static final int REQUEST_TO_SERVER_MY_STORY_PUBLISHED = 781;
    public static final int REQUEST_TO_SERVER_MY_STORY_COLLABORATING = 782;

    public static final int REQUEST_TO_SERVER_SIGN_IN_WITH_FACEBOOK_COMPLETED = 850;
    public static final int REQUEST_TO_SERVER_SIGN_IN_WITH_TWITTER_COMPLETED = 851;
    public static final int REQUEST_TO_SERVER_SIGN_IN_WITH_EMAIL_ACCOUNT_COMPLETED = 852;
    public static final int REQUEST_TO_SERVER_SIGN_IN_GUEST_COMPLETED = 853;
    public static final int REQUEST_TO_SERVER_REGISTER_USER_COMPLETED = 854;

    public static final int REQUEST_TO_SERVER_ACCOUNT_FOLLOWING_COMPLETED = 855;
    public static final int REQUEST_TO_SERVER_ACCOUNT_LIKED_STORY_COMPLETED = 856;
    public static final int REQUEST_TO_SERVER_ACCOUNT_INFO_COMPLETED = 857;

    public static final int REQUEST_TO_SERVER_STORIES_FEATURED_COMPLETED = 858;
    public static final int REQUEST_TO_SERVER_STORIES_FOLLOWING_COMPLETED = 890;
    public static final int REQUEST_TO_SERVER_STORIES_LATEST_COMPLETED = 891;
    public static final int REQUEST_TO_SERVER_STORIES_COLLABORATING_COMPLETED = 982;

    public static final int REQUEST_TO_SERVER_STORY_COMPLETED = 859;

    public static final int REQUEST_TO_SERVER_USER_FOLLOWERS_COMPLETED = 860;
    public static final int REQUEST_TO_SERVER_USER_FOLLOWING_COMPLETED = 861;
    public static final int REQUEST_TO_SERVER_USERS_COMPLETED = 862;
    public static final int REQUEST_TO_SERVER_USER_COMPLETED = 863;
    public static final int REQUEST_TO_SERVER_USER_FOLLOW_COMPLETED = 864;
    public static final int REQUEST_TO_SERVER_USER_UNFOLLOW_COMPLETED = 865;
    public static final int REQUEST_TO_SERVER_USER_REPORT_COMPLETED = 866;

    public static final int REQUEST_TO_SERVER_STORY_REPORT_COMPLETED = 867;
    public static final int REQUEST_TO_SERVER_STORY_LIKE_COMPLETED = 868;
    public static final int REQUEST_TO_SERVER_STORY_UNLIKE_COMPLETED = 869;
    public static final int REQUEST_TO_SERVER_STORY_POST_COMPLETED = 870;
    public static final int REQUEST_TO_SERVER_STORY_UPDATE_COMPLETED = 871;
    public static final int REQUEST_TO_SERVER_STORY_REMOVE_COMPLETED = 872;

    public static final int REQUEST_TO_SERVER_COMMENT_REPLY_COMPLETED = 873;
    public static final int REQUEST_TO_SERVER_COMMENT_LIKE_COMPLETED = 874;
    public static final int REQUEST_TO_SERVER_COMMENT_UNLIKE_COMPLETED = 875;
    public static final int REQUEST_TO_SERVER_COMMENT_STORY_COMPLETED = 876;
    public static final int REQUEST_TO_SERVER_COMMENT_UPDATE_COMPLETED = 877;
    public static final int REQUEST_TO_SERVER_COMMENT_DELETE_COMPLETED = 878;
    public static final int REQUEST_TO_SERVER_COMMENT_REPORT_COMPLETED = 879;

    public static final int REQUEST_TO_SERVER_MY_STORY_COMPLETED = 880;
    public static final int REQUEST_TO_SERVER_MY_STORY_PRIVATE_COMPLETED = 881;
    public static final int REQUEST_TO_SERVER_MY_STORY_PUBLISHED_COMPLETED = 882;

    public static final int BROWSING_STORY_ANIMATION = 670;

    public static final int RESUME_BACKGROUND_TASK = 950;
    public static final int PAUSE_BACKGROUND_TASK = 951;
    public static final int REQUEST_TO_REQUERY = 1000;

    public static final int REQUEST_TO_SERVER_ERROR = 600;
    public static final int REQUEST_TO_SERVER_ERROR_EMAIL_ACCOUNT_INVALID = REQUEST_TO_SERVER_ERROR + 2;
    public static final int REQUEST_TO_SERVER_ERROR_TIMEOUT = 620;
    // Extras for common
    public static final int AUTO_DELETE = 650;
    public static final int REQUEST_TO_SERVER_CANCEL = 800;
    public static final int NETWORK_DISCONNECT = 801;
    public static final int SHOW_PROGRESS = 900;
    public static final int DISMISS = 901;

	public static final int SHOW_SEARCH_AUTOCOMPLETE_LIST = 111;

    // Image Processing Messages
    public static final int REQUEST_TO_TAKE_PICTURE_EVENT = 1100;
    public static final int REQUEST_TO_TAKE_PICTURE_EVENT_ERROR = 2200;
    public static final int REQUEST_TO_DESTROY_CAMERA_CONTROLLER = 1110;

    public static final int UPLOADING_CONTENT_STARTED = 1209;
    public static final int UPLOADING_CONTENT_PROGRESS = 1210;
    public static final int UPLOADING_CONTENT_COMPLETED = 1211;
    public static final int UPLOADING_CONTENT_FAILED = 1212;
    public static final int UPLOADING_CONTENT_LOW_MEMORY = 1213;

    public static String toString(int msg) {
        String result = null;
        switch (msg) {
            case REQUEST_OWNER_MESSAGE: {
                result = "REQUEST_OWNER_MESSAGE";
                break;
            }
            case REQUEST_DOWNLOAD_IMAGE: {
                result = "REQUEST_DOWNLOAD_IMAGE";
                break;
            }
            case REQUEST_DOWNLOAD_IMAGE_COMPLETED: {
                result = "REQUEST_DOWNLOAD_IMAGE_COMPLETED";
                break;
            }
            case LOAD_LIST_OF_STORY: {
                result = "LOAD_LIST_OF_STORY";
                break;
            }
            case LOAD_LIST_OF_STORY_COMPLETED: {
                result = "LOAD_LIST_OF_STORY_COMPLETED";
                break;
            }
            case LOAD_IMAGE_OF_STORY: {
                result = "LOAD_IMAGE_OF_STORY";
                break;
            }
            case LOAD_IMAGE_OF_STORY_COMPLETED: {
                result = "LOAD_IMAGE_OF_STORY_COMPLETED";
                break;
            }
            case LOAD_SMALL_IMAGE_OF_STORY: {
                result = "LOAD_SMALL_IMAGE_OF_STORY";
                break;
            }
            case LOAD_SMALL_IMAGE_OF_STORY_COMPLETED: {
                result = "LOAD_SMALL_IMAGE_OF_STORY_COMPLETED";
                break;
            }
            case LOAD_MEDIUM_IMAGE_OF_STORY: {
                result = "LOAD_MEDIUM_IMAGE_OF_STORY";
                break;
            }
            case LOAD_MEDIUM_IMAGE_OF_STORY_COMPLETED: {
                result = "LOAD_MEDIUM_IMAGE_OF_STORY_COMPLETED";
                break;
            }
            case LOAD_HIGH_IMAGE_OF_STORY: {
                result = "LOAD_HIGH_IMAGE_OF_STORY";
                break;
            }
            case LOAD_HIGH_IMAGE_OF_STORY_COMPLETED: {
                result = "LOAD_HIGH_IMAGE_OF_STORY_COMPLETED";
                break;
            }
            case LOAD_EXTRAS_IMAGE_OF_STORY: {
                result = "LOAD_EXTRAS_IMAGE_OF_STORY";
                break;
            }
            case LOAD_EXTRA_IMAGE_OF_STORY_COMPLETED: {
                result = "LOAD_EXTRA_IMAGE_OF_STORY_COMPLETED";
                break;
            }
            case REQUEST_STORY_IMAGE_FROM_SERVER_COMPLETED: {
                result = "REQUEST_STORY_IMAGE_FROM_SERVER_COMPLETED";
                break;
            }
            case REQUEST_TO_SERVER_SIGN_IN_WITH_FACEBOOK: {
                result = "REQUEST_TO_SERVER_SIGN_IN_WITH_FACEBOOK";
                break;
            }
            case REQUEST_TO_SERVER_SIGN_IN_WITH_TWITTER: {
                result = "REQUEST_TO_SERVER_SIGN_IN_WITH_TWITTER";
                break;
            }
            case REQUEST_TO_SERVER_SIGN_IN_WITH_EMAIL_ACCOUNT: {
                result = "REQUEST_TO_SERVER_SIGN_IN_WITH_EMAIL_ACCOUNT";
                break;
            }
            case REQUEST_TO_SERVER_SIGN_IN_GUEST: {
                result = "REQUEST_TO_SERVER_SIGN_IN_GUEST";
                break;
            }
            case REQUEST_TO_SERVER_ACCOUNT_REGISTER: {
                result = "REQUEST_TO_SERVER_ACCOUNT_REGISTER";
                break;
            }
            case REQUEST_TO_SERVER_SIGN_IN_WITH_FACEBOOK_COMPLETED: {
                result = "REQUEST_TO_SERVER_SIGN_IN_WITH_FACEBOOK_COMPLETED";
                break;
            }
            case REQUEST_TO_SERVER_SIGN_IN_WITH_TWITTER_COMPLETED: {
                result = "REQUEST_TO_SERVER_SIGN_IN_WITH_TWITTER_COMPLETED";
                break;
            }
            case REQUEST_TO_SERVER_SIGN_IN_WITH_EMAIL_ACCOUNT_COMPLETED: {
                result = "REQUEST_TO_SERVER_SIGN_IN_WITH_EMAIL_ACCOUNT_COMPLETED";
                break;
            }
            case REQUEST_TO_SERVER_SIGN_IN_GUEST_COMPLETED: {
                result = "REQUEST_TO_SERVER_SIGN_IN_GUEST_COMPLETED";
                break;
            }
            case REQUEST_TO_SERVER_REGISTER_USER_COMPLETED: {
                result = "REQUEST_TO_SERVER_REGISTER_USER_COMPLETED";
                break;
            }
            case REQUEST_ERROR_REASON_PARAMS_MISSING: {
                result = "REQUEST_ERROR_REASON_PARAMS_MISSING";
                break;
            }
            case REQUEST_ERROR_REASON_HTTP_CONNECT_FAIL: {
                result = "REQUEST_ERROR_REASON_HTTP_CONNECT_FAIL";
                break;
            }
            case REQUEST_ERROR_REASON_HTTP_UNAUTHORIZED: {
                result = "REQUEST_ERROR_REASON_HTTP_UNAUTHORIZED";
                break;
            }
            case REQUEST_ERROR_REASON_UNSUPPORTED_URL: {
                result = "REQUEST_ERROR_REASON_UNSUPPORTED_URL";
                break;
            }
            case REQUEST_ERROR_REASON_JSON_PARSE_EXCEPTION: {
                result = "REQUEST_ERROR_REASON_JSON_PARSE_EXCEPTION";
                break;
            }
            case REQUEST_ERROR_REASON_IMAGE_DOWNLOAD_ERROR: {
                result = "REQUEST_ERROR_REASON_IMAGE_DOWNLOAD_ERROR";
                break;
            }
            case REQUEST_ERROR_REASON_SERVER_CONNECT_TIMEOUT: {
                result = "REQUEST_ERROR_REASON_SERVER_CONNECT_TIMEOUT";
                break;
            }
            case REQUEST_ERROR_REASON_SERVER_READ_TIMEOUT: {
                result = "REQUEST_ERROR_REASON_SERVER_READ_TIMEOUT";
                break;
            }
            case REQUEST_ERROR_REASON_INTERNAL_SERVER_ERROR: {
                result = "REQUEST_ERROR_REASON_INTERVAL_SERVER_ERROR";
                break;
            }
            case REQUEST_ERROR_REASON_UNKNOWN_HOST: {
                result = "REQUEST_ERROR_REASON_UNKNOWN_HOST";
                break;
            }
            case REQUEST_ERROR_REASON_SERVER_DATA_CORRUPTION: {
                result = "REQUEST_ERROR_REASON_SERVER_DATA_CORRUPTION";
                break;
            }
            case ACTION_WAITING_START: {
                result = "ACTION_WAITING_START";
                break;
            }
            case ACTION_WAITING_END: {
                result = "ACTION_WAITING_END";
                break;
            }
            case BROWSING_STORY_ANIMATION: {
                result = "BROWSING_STORY_ANIMATION";
                break;
            }
            case AUTO_DELETE: {
                result = "AUTO_DELETE";
                break;
            }
            case REQUEST_TO_SERVER_CANCEL: {
                result = "REQUEST_TO_SERVER_CANCEL";
                break;
            }
            case NETWORK_DISCONNECT: {
                result = "NETWORK_DISCONNECT";
                break;
            }
            case SHOW_PROGRESS: {
                result = "SHOW_PROGRESS";
                break;
            }
            case DISMISS: {
                result = "DISMISS";
                break;
            }
            default:
                break;
        }
        return result;
    }
}
