/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/24/15 3:32 AM.
 **/

package co.soml.android.app.controllers;

import android.content.Intent;
import android.os.Message;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;

import co.soml.android.app.AppConstants;
import co.soml.android.app.R;
import co.soml.android.app.StoryApp;
import co.soml.android.app.activities.BaseActivity;
import co.soml.android.app.activities.InvalidateParam;
import co.soml.android.app.activities.StoryMainActivity;
import co.soml.android.app.activities.UserProfileActivity;
import co.soml.android.app.fragments.BaseFragment;
import co.soml.android.app.models.Story;
import co.soml.android.app.models.StoryBean;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.models.StoryUserIdList;
import co.soml.android.app.models.StoryUserList;
import co.soml.android.app.models.User;
import co.soml.android.app.services.StatusResult;
import co.soml.android.app.services.StoryService;
import co.soml.android.app.services.UserListResult;
import co.soml.android.app.services.UserResult;
import co.soml.android.app.services.tasks.Action;
import co.soml.android.app.utils.CommonUtil;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.widgets.ProgressWheel;

public class UserProfileController extends BaseController {

    public static final String TAG = LogUtils.makeLogTag(UserProfileController.class.getSimpleName());

    private StoryDataModel mDataModel;
    private BaseActivity mActivity;
    private StoryApp mApp;
    private ProgressWheel mWaitProgressWheel = null;
    private boolean mScrollState = false;

    private Snackbar mCheckNetworkSnackBar;
    private StoryBean mBean = StoryMainActivity.getBean();

    public UserProfileController(BaseActivity activity, StoryDataModel dataModel) {
        super(activity, dataModel);
        mActivity = activity;
        mApp = (StoryApp) activity.getApplicationContext();
        mDataModel = mApp.getAppDataModel();
    }

    public UserProfileController(BaseActivity activity, StoryDataModel dataModel, boolean isFirstRunningMode) {
        super(activity, dataModel, isFirstRunningMode);
        this.mActivity = activity;
        this.mApp = (StoryApp) activity.getApplicationContext();
        mDataModel = mApp.getAppDataModel();
    }

    public UserProfileController(BaseActivity activity, BaseFragment fragment, StoryDataModel dataModel) {
        super(activity, fragment, dataModel);
        this.mActivity = activity;
        this.mFragment = fragment;
        this.mApp = (StoryApp) activity.getApplicationContext();
        mDataModel = mApp.getAppDataModel();
    }

    public UserProfileController(BaseActivity activity, BaseFragment fragment, StoryDataModel dataModel, boolean isFirstRunningMode) {
        super(activity, fragment, dataModel, isFirstRunningMode);
        this.mActivity = activity;
        this.mFragment = fragment;
        this.mDataModel = dataModel;
    }

    @Override
    protected void handleMessage(Message msg) {
        final String fn = "handleMessage() - ";
        switch (msg.what) {
            case ControllerMessage.CANCEL_WAITING_DIALOG: {
                LogUtils.LOGD(TAG, "[SIGNAL] UserProfileController handleMessage[CANCEL_WAITING_DIALOG]");
                Intent intent = this.obtainIntent(ControllerMessage.CANCEL_WAITING_DIALOG);
                intent.setAction(Action.REQUEST_TO_SERVER_CANCEL);
                mActivity.startService(intent);
                mWaitProgressWheel = null;
                break;
            }
            case ControllerMessage.ACTION_WAITING_END: {
                LogUtils.LOGD(TAG, "[SIGNAL] UserProfileController handleMessage[ACTION_WAITING_END]");
                final Intent intent = this.obtainIntent(ControllerMessage.ACTION_WAITING_END);
                intent.setAction(Action.REQUEST_TO_SERVER_CANCEL);
                mActivity.startService(intent);
                mWaitProgressWheel = null;
                break;
            }
            case ControllerMessage.RESET_REQUEST_COUNT: {
                this.getActionCounter().reset();
                this.mDataModel.setWaitForRequesting(false);
                break;
            }
            case ControllerMessage.PAUSE_BACKGROUND_TASK: {
                if (!mScrollState) {
                    mScrollState = true;
                    final Intent intent = new Intent(mActivity, StoryService.class);
                    mActivity.startService(intent);
                }
                break;
            }
            case ControllerMessage.RESUME_BACKGROUND_TASK: {
                mScrollState = false;
                if (msg.obj == null) {
                    mFragment.invalidate();
                }

                if ((Boolean) msg.obj) {
                    mFragment.invalidate();
                }
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_CANCEL: {
                final Intent intent = this.obtainIntent(msg.what);
                intent.setAction(Action.REQUEST_TO_SERVER_CANCEL);
                mActivity.startService(intent);
                getActionCounter().reset();
                this.mDataModel.setWaitForRequesting(false);
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_USER_DETAIL: {
                int connectMode = CommonUtil.checkConnectivityMode(mActivity);
                if (connectMode == AppConstants.CONN_DISCONNECTED) {
                    ((UserProfileActivity) mActivity).showPopupNoNetworkSnackBar();
                } else {
                    incrementActionCount();
                    final Intent intent = obtainIntent(ControllerMessage.REQUEST_TO_SERVER_USER_DETAIL);
                    final RequestParams params = (RequestParams) msg.obj;
                    intent.putExtra(Action.X_API_VERSION, params.mApiVersion);
                    intent.putExtra(Action.X_AUTH_TOKEN, params.mAuthToken);
                    intent.putExtra(Action.USER_ID, params.mUserId);

                    intent.setAction(Action.REQUEST_TO_SERVER_USER_DETAIL);
                    mActivity.startService(intent);
                }
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_USER_COMPLETED: {
                if (checkOwner(msg)) {
                    final UserResult result = (UserResult) msg.obj;
                    if (result != null && result.getUser() != null) {
                        final User user = new User(result.getUser());
                        final InvalidateParam params = new InvalidateParam(msg.what, user);
                        ((UserProfileActivity) mActivity).invalidate(params);
                    }
                }
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_USER_FOLLOWERS: {
                int connectMode = CommonUtil.checkConnectivityMode(mActivity);
                if (connectMode == AppConstants.CONN_DISCONNECTED) {
                    ((UserProfileActivity) mActivity).showPopupNoNetworkSnackBar();
                } else {
                    incrementActionCount();
                    final Intent intent = obtainIntent(ControllerMessage.REQUEST_TO_SERVER_USER_FOLLOWERS);
                    final RequestParams params = (RequestParams) msg.obj;
                    intent.putExtra(Action.X_API_VERSION, params.mApiVersion);
                    intent.putExtra(Action.X_AUTH_TOKEN, params.mAuthToken);
                    intent.putExtra(Action.USER_ID, params.mUserId);

                    intent.setAction(Action.REQUEST_TO_SERVER_USER_FOLLOWERS);
                    mActivity.startService(intent);
                }
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_USER_FOLLOWING: {
                int connectMode = CommonUtil.checkConnectivityMode(mActivity);
                if (connectMode == AppConstants.CONN_DISCONNECTED) {
                    ((UserProfileActivity) mActivity).showPopupNoNetworkSnackBar();
                } else {
                    incrementActionCount();
                    final Intent intent = obtainIntent(ControllerMessage.REQUEST_TO_SERVER_USER_FOLLOWING);
                    final RequestParams params = (RequestParams) msg.obj;
                    intent.putExtra(Action.X_API_VERSION, params.mApiVersion);
                    intent.putExtra(Action.X_AUTH_TOKEN, params.mAuthToken);
                    intent.putExtra(Action.USER_ID, params.mUserId);

                    intent.setAction(Action.REQUEST_TO_SERVER_USER_FOLLOWING);
                    mActivity.startService(intent);
                }
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_USER_FOLLOWERS_COMPLETED: {
                if (checkOwner(msg)) {
                    final UserListResult listResult = (UserListResult) msg.obj;
                    final StoryUserList users = new StoryUserList();

                    for (int index = 0; index < listResult.getUserList().size(); index++) {
                        final User user = new User(listResult.getUserList().get(index));
                        users.add(index,user);
                    }
                    final InvalidateParam params = new InvalidateParam(msg.what, users);
                    mFragment.invalidate(params);
                }
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_USER_FOLLOWING_COMPLETED: {
                if (checkOwner(msg)) {
                    final UserListResult listResult = (UserListResult) msg.obj;
                    final StoryUserList users = new StoryUserList();

                    for (int index = 0; index < listResult.getUserList().size(); index++) {
                        final User user = new User(listResult.getUserList().get(index));
                        users.add(index,user);
                    }
                    final InvalidateParam params = new InvalidateParam(msg.what, users);
                    mFragment.invalidate(params);
                }
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_USER_FOLLOW: {
                int connectMode = CommonUtil.checkConnectivityMode(mActivity);
                if (connectMode == AppConstants.CONN_DISCONNECTED) {
                    ((UserProfileActivity) mActivity).showPopupNoNetworkSnackBar();
                } else {
                    incrementActionCount();
                    final Intent intent = obtainIntent(ControllerMessage.REQUEST_TO_SERVER_USER_FOLLOW);
                    final RequestParams params = (RequestParams) msg.obj;
                    intent.putExtra(Action.X_API_VERSION, params.mApiVersion);
                    intent.putExtra(Action.X_AUTH_TOKEN, params.mAuthToken);
                    intent.putExtra(Action.USER_ID, params.mUserId);

                    intent.setAction(Action.REQUEST_TO_SERVER_USER_FOLLOW);
                    mActivity.startService(intent);
                }
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_USER_UNFOLLOW: {
                int connectMode = CommonUtil.checkConnectivityMode(mActivity);
                if (connectMode == AppConstants.CONN_DISCONNECTED) {
                    ((UserProfileActivity) mActivity).showPopupNoNetworkSnackBar();
                } else {
                    incrementActionCount();
                    final Intent intent = obtainIntent(ControllerMessage.REQUEST_TO_SERVER_USER_UNFOLLOW);
                    final RequestParams params = (RequestParams) msg.obj;
                    intent.putExtra(Action.X_API_VERSION, params.mApiVersion);
                    intent.putExtra(Action.X_AUTH_TOKEN, params.mAuthToken);
                    intent.putExtra(Action.USER_ID, params.mUserId);

                    intent.setAction(Action.REQUEST_TO_SERVER_USER_UNFOLLOW);
                    mActivity.startService(intent);
                }
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_USER_FOLLOW_COMPLETED: {
                if (mFragment != null) {
                    if (checkOwner(msg)) {
                        final StatusResult result = (StatusResult) msg.obj;
                        if (result != null && result.getStatus() != null) {
                            final InvalidateParam params = new InvalidateParam(msg.what, result);
                            mFragment.invalidate(params);
                        }
                    }
                } else {
                    if (checkOwner(msg)) {
                        final StatusResult result = (StatusResult) msg.obj;
                        if (result != null && result.getStatus() != null) {
                            final InvalidateParam params = new InvalidateParam(msg.what, result);
                            ((UserProfileActivity) mActivity).invalidate(params);
                        }
                    }
                }
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_USER_UNFOLLOW_COMPLETED: {
                if (mFragment != null) {
                    if (checkOwner(msg)) {
                        final StatusResult result = (StatusResult) msg.obj;
                        if (result != null && result.getStatus() != null) {
                            final InvalidateParam params = new InvalidateParam(msg.what, result);
                            mFragment.invalidate(params);
                        }
                    }
                } else {
                    if (checkOwner(msg)) {
                        final StatusResult result = (StatusResult) msg.obj;
                        if (result != null && result.getStatus() != null) {
                            final InvalidateParam params = new InvalidateParam(msg.what, result);
                            ((UserProfileActivity) mActivity).invalidate(params);
                        }
                    }
                }
                break;
            }
        }
    }

    protected void incrementActionCount() {
        this.getActionCounter().incrementActionCount();
        if (this.getActionCounter().getCount() > 0) {
            mDataModel.setWaitForRequesting(true);
        }
    }

    protected void decrementActionCount() {
        this.getActionCounter().decrementActionCount();
        if (this.getActionCounter().getCount() <= 0) {
            mDataModel.setWaitForRequesting(false);
        }
    }

    public void showCheckNetworkSnackBar() {
        String strCheckNetworkToast = mActivity.getString(R.string.msg_error_connection_failed);
        if (mCheckNetworkSnackBar == null) {
            View snackBarView = LayoutInflater.from(mActivity).inflate(R.layout.layout_snackbar, null);
            mCheckNetworkSnackBar = Snackbar.make(snackBarView, strCheckNetworkToast, Snackbar.LENGTH_SHORT);
        } else {
            mCheckNetworkSnackBar.setText(strCheckNetworkToast);
        }
        mCheckNetworkSnackBar.show();
    }

    public static class RequestParams {
        public int mApiVersion;
        public String mAuthToken;
        public int mUserId;

        public RequestParams(int apiVersion, String authToken) {
            this.mApiVersion = apiVersion;
            this.mAuthToken = authToken;
        }

        public RequestParams(int userId, int apiVersion, String authToken) {
            this.mUserId = userId;
            this.mApiVersion = apiVersion;
            this.mAuthToken = authToken;
        }
    }
}
