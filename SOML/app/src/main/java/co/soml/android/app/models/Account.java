package co.soml.android.app.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import co.soml.android.app.AppConstants;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its
 * documentation are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable
 * form without the prior written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with
 * respect to the contents, and assumes no responsibility for any errors that might appear in the software and
 * documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.03.2015
 * <p>
 * Value type that represents an Account in the
 */
public class Account implements Parcelable {
    public static final String TAG = AppConstants.PREFIX + Account.class.getSimpleName();
    public final String mName;
    public final String mType;

    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof Account)) return false;
        final Account other = (Account) obj;
        return mName.equals(other.mName) && mType.equals(other.mType);
    }

    public int hashCode() {
        int result = 17;
        result = 31 * result + mName.hashCode();
        result = 31 * result + mType.hashCode();
        return result;
    }

    public Account(String name, String type) {
        if (TextUtils.isEmpty(name)) {
            throw new IllegalArgumentException("The name must not be empty: " + name);
        }

        if (TextUtils.isEmpty(type)) {
            throw new IllegalArgumentException("The type must not empty: " + type);
        }
        this.mName = name;
        this.mType = type;
    }

    public Account(Parcel in) {
        this.mName = in.readString();
        this.mType = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mName);
        dest.writeString(mType);
    }

    public static final Creator<Account> CREATOR = new Creator<Account>() {
        @Override
        public Account createFromParcel(Parcel source) {
            return new Account(source);
        }

        @Override
        public Account[] newArray(int size) {
            return new Account[size];
        }
    };

    public String toString() {
        return "Account { name= " + mName + ", type= " + mType + "}";
    }
}
