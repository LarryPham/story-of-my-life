package co.soml.android.app.widgets;

import android.content.Context;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.widget.TextView;

public class SearchMatchedTextView extends TextView {

	public SearchMatchedTextView(Context context) {
		super(context);
	}

	public SearchMatchedTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public SearchMatchedTextView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	public void setText(String text, String matchedText) {
		if (TextUtils.isEmpty(text)) {
			text = "";
		} else {
			int index = text.toString().toLowerCase().indexOf(matchedText.toLowerCase());
			if  (index != -1) {
				int matchedColor = Color.parseColor("#00b7ff");
				SpannableString spannableString = new SpannableString(text);
				spannableString.setSpan(new ForegroundColorSpan(matchedColor), index, index + matchedText.length(),
						Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
				super.setText(spannableString);
				return;
			}
		}
		super.setText(text);

	}
}
