package co.soml.android.app.services.tasks;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.PowerManager;

import co.soml.android.app.AppConstants;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its
 * documentation are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable
 * form without the prior written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with
 * respect to the contents, and assumes no responsibility for any errors that might appear in the software and
 * documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.02.2015
 */
public class DownloadTask {
    public static final String TAG = AppConstants.PREFIX + DownloadTask.class.getSimpleName();

    class JobThread extends Thread {
        private DownloadAction mRunningAction = null;
        private Context mContext = null;
        private boolean mCancelFlag = false;
        private WifiManager.WifiLock mWifiLock = null;
        private PowerManager.WakeLock mWakeLock = null;
        private long mDownloadSize = 0;
        private boolean mMultiCancelFlag = false;

        public JobThread(Context context) {
            this.mContext = context;
        }

        public void sendProgressMessage(int id, String songURl, int progress, long downloadSize, long totalSize,
                                        String songTitle) {
        }
    }
}
