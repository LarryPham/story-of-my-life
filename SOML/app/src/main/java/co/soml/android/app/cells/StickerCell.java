/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/11/15 4:00 PM.
 **/

package co.soml.android.app.cells;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.widgets.FixedFrameLayout;

public class StickerCell extends FixedFrameLayout {
    public static final String TAG = LogUtils.makeLogTag(StickerCell.class.getSimpleName());
    private ImageView mImageView;

    public StickerCell(Context context) {
        super(context);
    }

    public StickerCell(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public StickerCell(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
