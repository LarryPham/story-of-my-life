/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 7/31/15 3:13 AM.
 **/

package co.soml.android.app.drawables;

import android.graphics.Paint;

public interface EditableDrawable {
    static final int CURSOR_BLINK_TIME = 400;

    void setOnSizeChangeListener(OnSizeChange sizeChangeListener);

    void beginEdit();

    void endEdit();

    boolean isEditing();

    CharSequence getText();

    void setText(CharSequence text);

    void setText(String textString);

    void setTextHint(CharSequence charSequence);

    void setTextHint(String hint);

    boolean isTextHint();

    void setBounds(float f1, float f2, float f3, float f4);

    void setTextColor(int color);

    int getTextColor();

    float getTextSize();

    float getFontMetrics(Paint.FontMetrics fontMetrics);

    void setTextColorStroke(int color);

    int getTextColorStroke();

    void setStrokeEnabled(boolean enabled);

    boolean getStrokeEnabled();

    int getNumLines();

    public static interface OnSizeChange {
        void onSizeChanged(EditableDrawable params, float floatParam1, float param1, float param2, float param3);
    }
}
