/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/15/15 3:15 PM.
 **/

package co.soml.android.app.services.tasks;

import android.os.Parcel;
import android.os.Parcelable;

public class UploadingAction extends Action {

    public UploadingAction(Integer id, Params params) {
        super(Action.REQUEST_TO_SERVER_STORY_POST, id, (Object) params);
    }

    public static class Params {
        public String mFrom;
        public String mTo;
        public long mUploadingSize;

        public Params(String from, String to, long uploadingSize) {
            this.mFrom = from;
            this.mTo = to;
            this.mUploadingSize = uploadingSize;
        }
    }

    public void setParams(Params params) {
        mObject = params;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mId);
        dest.writeString(((Params) mObject).mFrom);
        dest.writeString(((Params) mObject).mTo);
    }

    public static final Parcelable.Creator<UploadingAction> CREATOR = new Creator<UploadingAction>() {
        @Override
        public UploadingAction createFromParcel(Parcel source) {
            UploadingAction action = new UploadingAction(source.readInt(), null);
            UploadingAction.Params params = new UploadingAction.Params(source.readString(), source.readString(), source.readLong());
            action.setParams(params);
            return action;
        }

        @Override
        public UploadingAction[] newArray(int size) {
            return new UploadingAction[size];
        }
    };
}
