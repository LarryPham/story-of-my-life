package co.soml.android.app.activities;

import android.app.FragmentManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import co.soml.android.app.R;
import co.soml.android.app.controllers.ControllerMessage;
import co.soml.android.app.fragments.SettingsFragment;
import co.soml.android.app.utils.LogUtils;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.14.2015
 */
public class SettingsActivity extends ActionBarActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    public static final String TAG = LogUtils.makeLogTag(SettingsActivity.class);
    public static final String SETTINGS_PAGE_PARAM = "SettingsPage";
    public static final String SETTINGS_FRAGMENT_PAGE = "co.soml.android.app.fragments.SettingsFragment";
    public static final String SETTINGS_FRAGMENT_KEY = "SettingsPage";

    protected SettingsFragment mSettingsFragment;
    protected Bundle mArguments;
    protected String mSettingsPage;
    protected PreferenceFragment mContent;
    protected Toolbar mActionBarToolBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_layout);
        Toolbar toolbar = getActionBarToolBar();
        toolbar.setTitle(R.string.action_settings);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateUpToFromChild(SettingsActivity.this, IntentCompat.makeMainActivity
                        (new ComponentName(SettingsActivity.this, StoryMainActivity.class)));
            }
        });

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setElevation(0.15f);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        mSettingsFragment = new SettingsFragment();
        getFragmentManager().beginTransaction().add(R.id.frame_content, mSettingsFragment).commit();
    }

    public Toolbar getActionBarToolBar() {
        if (mActionBarToolBar == null) {
            mActionBarToolBar = (Toolbar) findViewById(R.id.nav_drawer_toolbar);
        }
        return mActionBarToolBar;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

    }

    @Override
    public void finish() {
        Intent data = new Intent();
        boolean currentUserChanged = false;
        super.finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // refreshing the authenticated user information
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    public void invalidate(Object param) {
        InvalidateParam invalidateParam = (InvalidateParam) param;
        if (invalidateParam.getMessage() == ControllerMessage.REQUEST_TO_SERVER_USER_COMPLETED) {

        }
    }
}
