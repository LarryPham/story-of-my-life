package co.soml.android.app.datasets;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import java.util.ArrayList;

import co.soml.android.app.StoryApp;
import co.soml.android.app.models.StoryUserIdList;
import co.soml.android.app.models.StoryUserList;
import co.soml.android.app.models.User;
import co.soml.android.app.utils.AccountUtils;
import co.soml.android.app.utils.SqlUtils;

public class UserTable {
	protected static void createTables(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE Users(user_id INTEGER PRIMARY KEY, facebook_id INTEGER, fabric_id INTEGER, " +
				" full_name TEXT, email TEXT, description TEXT, avatar_url TEXT, avatar_large_url TEXT, avatar_medium_url TEXT, " +
				"avatar_small_url TEXT, avatar_thumbnail_url TEXT" + "token TEXT, token_expired_at DATE, users_followed INTEGER, " +
				"users_following INTEGER, stories_count INTEGER);");
	}

	protected static void dropTables(SQLiteDatabase db) {
		db.execSQL("DROP TABLE IF EXISTS Users");
	}

	protected static void addOrUpdateUser(User user) {
		if (user == null) {
			return;
		}
		StoryUserList users = new StoryUserList();
		users.add(user);
		addOrUpdateUsers(users);
	}

	private static final String COLUMN_NAMES = "user_id," + "full_name," + "email," + "description," + "avatar_url"
			+ "avatar_large_url" + "avatar_medium_url" + "avatar_small_url" + "avatar_thumbnail_url" ;

	public static void addOrUpdateUsers(StoryUserList users) {
		if (users == null || users.size() == 0) {
			return;
		}

		SQLiteDatabase db = StoryDatabase.getWritableDB();
		db.beginTransaction();
		SQLiteStatement stmt = db.compileStatement("INSERT OR REPLACE INTO Users (" + COLUMN_NAMES
				+ ") VALUES (?1,?2,?3,?4,?5,?6,?7,?8,?9)");
		try {
			for (User user : users) {
				stmt.bindLong(1, user.mId);
				stmt.bindString(2, user.mFullName);
				stmt.bindString(3, user.mEmail);
				stmt.bindString(4, user.mDescription);
				stmt.bindString(5, user.getAvatarOriginalUrl());
				stmt.bindString(6, user.getAvatarLargeUrl());
				stmt.bindString(7, user.getAvatarMediumUrl());
				stmt.bindString(8, user.getAvatarSmallUrl());
				stmt.bindString(9, user.getAvatarThumbnailUrl());
				stmt.execute();
			}
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
			SqlUtils.closeStatement(stmt);
		}
	}

	public static ArrayList<String> getAvatarUrls(StoryUserIdList userIds, int max, int avatarSize) {
		ArrayList<String> avatars = new ArrayList<String>();
		if (userIds == null || userIds.size() == 0) {
			return avatars;
		}

		StringBuilder sb = new StringBuilder("SELECT user_id, avatar_url FROM Users WHERE user_id IN (");
		// make sure current user's avatar is returned if the passed list contains them - this is
		// important since it may not otherwise be returned when a "max" is passed, and we want
		// the current user to appear first in post detail when they like a post
		long currentUserId = AccountUtils.getUserId(StoryApp.getContext());
		boolean containsCurrentUser = userIds.contains(currentUserId);

		if (containsCurrentUser) {
			sb.append(currentUserId);
		}

		int numAdded = (containsCurrentUser ? 1 : 0);
		for (Integer id: userIds) {
			// skip current user since we added them already
			if (id != currentUserId) {
				if (numAdded > 0) {
					sb.append(",");
				}
				sb.append(id);
				numAdded++;
				if (max > 0 && numAdded >= max) {
					break;
				}
			}
			sb.append(")");
		}
		Cursor cursor = StoryDatabase.getReadableDB().rawQuery(sb.toString(), null);
		try {
			if (cursor.moveToFirst()) {
				do {
					long userId = cursor.getLong(0);
					String url = cursor.getString(1);
					if (userId == currentUserId) {
						avatars.add(0, url);
					} else {
						avatars.add(url);
					}
				} while (cursor.moveToNext());
			}
			return avatars;
		} finally {
			SqlUtils.closeCursor(cursor);
		}
	}

	public static User getUser(long userId) {
		String args[] = {Long.toString(userId)};
		Cursor cursor = StoryDatabase.getReadableDB().rawQuery("SELECT * FROM Users WHERE user_id=?", args);
		try {
			if (!cursor.moveToFirst()) {
				return null;
			}
			return getUserFromCursor(cursor);
		} finally {
			SqlUtils.closeCursor(cursor);
		}
	}

	public static String getAvatarForUser(long userId) {
		String args[] = { Long.toString(userId) };
		return SqlUtils.stringForQuery(StoryDatabase.getReadableDB(), "SElECT avatar_url FROM Users WHERE user_id = ?", args);
 	}

	public static StoryUserList getUsersWhoLikeComment(long storyId, long commentId, int max) {
		String[] args = { Long.toString(storyId), Long.toString(commentId) };
		String sql = "SELECT * FROM Users WHERE user_id IN " + " (SELECT user_id FROM CommentLikes WHERE story_id=? AND comment_id=?)"
				+ " ORDER BY full_name";
		if (max > 0) {
			sql += "LIMIT " + Integer.toString(max);
		}

		Cursor cursor = StoryDatabase.getReadableDB().rawQuery(sql, args);
		try {
			StoryUserList users = new StoryUserList();
			if (cursor.moveToFirst()) {
				do {
					users.add(getUserFromCursor(cursor));
				} while (cursor.moveToNext());
			}
			return users;
		} finally {
			SqlUtils.closeCursor(cursor);
		}
	}


	private static User getUserFromCursor(Cursor cursor) {
		User user = new User();
		user.setId(cursor.getInt(cursor.getColumnIndex("user_id")));
		user.setFullName(cursor.getString(cursor.getColumnIndex("full_name")));
		user.setDescription(cursor.getString(cursor.getColumnIndex("description")));
		user.setEmail(cursor.getString(cursor.getColumnIndex("email")));

		user.setAvatarOriginalUrl(cursor.getString(cursor.getColumnIndex("avatar_url")));
		user.setAvatarLargeUrl(cursor.getString(cursor.getColumnIndex("avatar_large_url")));
		user.setAvatarMediumUrl(cursor.getString(cursor.getColumnIndex("avatar_medium_url")));
		user.setAvatarSmallUrl(cursor.getString(cursor.getColumnIndex("avatar_small_url")));
		user.setAvatarThumbnailUrl(cursor.getString(cursor.getColumnIndex("avatar_thumbnail_url")));

		return user;
	}

	public static User getCurrentUser() {
		return getUser(AccountUtils.getUserId(StoryApp.getContext()));
	}
}
