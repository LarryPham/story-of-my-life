package co.soml.android.app.controllers;

import android.os.Message;

import co.soml.android.app.StoryApp;
import co.soml.android.app.activities.BaseActivity;
import co.soml.android.app.activities.CommentsActivity;
import co.soml.android.app.fragments.BaseFragment;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.utils.LogUtils;

/**
 *  Copyright (C) 2015 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation
 *  are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied,
 *  reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior
 *  written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with respect to the contents, and
 *  assumes no responsibility for any errors that might appear in the software and documents. This publication and the
 *  contents hereof are subject to change without notice.
 *  @author Larry Pham(Email: larrypham.vn@gmail.com)
 *  @since 6/23/15 11:15 AM
 **/

public class CommentsController extends BaseController {

    public static final String TAG = LogUtils.makeLogTag(CommentsController.class.getSimpleName());
    private StoryApp mStoryApp;
    private StoryDataModel mDataModel;
    private CommentsActivity mActivity;

    public CommentsController(BaseActivity activity, StoryDataModel dataModel) {
        super(activity, dataModel);
        this.mActivity = (CommentsActivity) activity;
        this.mStoryApp = (StoryApp) activity.getApplicationContext();
        this.mDataModel = dataModel;
    }

    public CommentsController(BaseActivity activity, BaseFragment fragment, StoryDataModel dataModel) {
        super(activity, fragment, dataModel);
        this.mActivity = (CommentsActivity) activity;
        this.mStoryApp = (StoryApp) activity.getApplicationContext();

        this.mDataModel = dataModel;
    }

    @Override
    protected void handleMessage(Message msg) {

    }
}
