/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 * @author: Larry Pham (email: larrypham.vn@gmail.com)
 * Date: 6/11/15 1:11 PM
 **/

package co.soml.android.app.cache;

import java.util.LinkedHashMap;
import java.util.Map;

public class LruCache<K, V> {
    private final LinkedHashMap<K, V> mMap;

    private final int mMaxSize;
    /* Size of this cache in units. Not necessarily the number of elements */
    private int mSize;

    private int mPutCount;

    private int mCreateCount;

    private int mEvictionCount;

    private int mHitCount;

    private int mMissCount;

    /**
     * @param maxSize for caches that do not override {@link #sizeOf}, this is the maximum number of entries int the cache.
     *                For all other caches, this is the maximum sum of the sizes of the entries in this cache.
     */
    public LruCache(final int maxSize) {
        if (maxSize <= 0) {
            throw new IllegalArgumentException("MaxSize <= 0");
        }
        this.mMaxSize = maxSize;
        this.mMap = new LinkedHashMap<K, V>(0, 0.75f, true);
    }

    /**
     * Returns the value for {@code  key} if it exists in the cache or can be created by {@code #create}. If a value was returned,
     * it is moved to the head of the queue. This returns null if a value is not cached and cannot be created.
     */
    public final V get(final K key) {
        if (key == null) {
            throw new NullPointerException("Key == null");
        }
        V mapValue;

        synchronized (this) {
            mapValue = mMap.get(key);
            if (mapValue != null) {
                this.mHitCount++;
                return mapValue;
            }
            this.mMissCount++;
        }

        final V createdValue = create(key);
        if (createdValue == null) {
            return null;
        }

        synchronized (this) {
            this.mCreateCount++;
            mapValue = mMap.put(key, createdValue);
            if (mapValue != null) {
                this.mMap.put(key, mapValue);
            } else {
                this.mSize += safeSizeOf(key, createdValue);
            }
        }

        if (mapValue != null) {
            entryRemoved(false, key, createdValue, mapValue);
            return mapValue;
        } else {
            trimToSize(mMaxSize);
            return createdValue;
        }
    }

    /**
     * Caches {@code value} for {@code key}. The value is moved to the head of the queue.
     *
     * @return the previous value mapped by {@code key}.
     */
    public final V put(final K key, final V value) {
        if (key == null || value == null) {
            throw new NullPointerException("Key == null || value == null");
        }

        V previous;
        synchronized (this) {
            this.mPutCount++;
            this.mSize += safeSizeOf(key, value);

            previous = this.mMap.put(key, value);
            if (previous != null) {
                this.mSize -= safeSizeOf(key, previous);
            }
        }

        if (previous != null) {
            entryRemoved(false, key, previous, value);
        }

        trimToSize(mMaxSize);
        return previous;
    }

    /**
     * @param maxSize the maximum size of the cache before returning. May be -1 to evict event 0-sized elements.
     */
    public void trimToSize(final int maxSize) {
        while (true) {
            K key;
            V value;
            synchronized (this) {
                if (this.mSize < 0 || this.mMap.isEmpty() && mSize != 0) {
                    throw new IllegalStateException(getClass().getName() + ".sizeOf is reporting inconsistent results.");
                }

                if (this.mSize <= maxSize || this.mMap.isEmpty()) {
                    break;
                }

                final Map.Entry<K, V> toEvict = this.mMap.entrySet().iterator().next();
                key = toEvict.getKey();
                value = toEvict.getValue();

                this.mMap.remove(key);
                this.mSize -= safeSizeOf(key, value);
                this.mEvictionCount++;
            }
            entryRemoved(true, key, value, null);
        }
    }

    /**
     * Removes the entry for {@code key} if it exists.
     *
     * @return the previous value mapped by {@code key}.
     */
    public final V remove(final K key) {
        if (key == null) {
            throw new NullPointerException("key == null");
        }

        V previous;
        synchronized (this) {
            previous = this.mMap.remove(key);
            if (previous != null) {
                this.mSize -= safeSizeOf(key, previous);
            }
        }

        if (previous != null) {
            entryRemoved(false, key, previous, null);
        }
        return previous;
    }

    /**
     * Called for entries that have been evicted or removed. This method is
     * invoked when a value is evicted to make space, removed by a call to
     * {@link #remove}, or replaced by a call to {@link #put}. The default
     * implementation does nothing.
     * <p>
     * The method is called without synchronization: other threads may access
     * the cache while this method is executing.
     *
     * @param evicted  true if the entry is being removed to make space, false if
     *                 the removal was caused by a {@link #put} or {@link #remove}.
     * @param newValue the new value for {@code key}, if it exists. If non-null,
     *                 this removal was caused by a {@link #put}. Otherwise it was
     *                 caused by an eviction or a {@link #remove}.
     */
    protected void entryRemoved(final boolean evicted, final K key, final V oldValue, final V newValue) {

    }

    /**
     * Called after a cache miss to compute a value for the corresponding key.
     * Returns the computed value or null if no value can be computed. The
     * default implementation returns null.
     * <p>
     * The method is called without synchronization: other threads may access
     * the cache while this method is executing.
     * <p>
     * If a value for {@code key} exists in the cache when this method returns,
     * the created value will be released with {@link #entryRemoved} and
     * discarded. This can occur when multiple threads request the same key at
     * the same time (causing multiple values to be created), or when one thread
     * calls {@link #put} while another is creating a value for the same key.
     */
    protected V create(final K key) {
        return null;
    }

    private int safeSizeOf(final K key, final V value) {
        final int result = sizeOf(key, value);
        if (result < 0) {
            throw new IllegalStateException("Negative Size: " + key + "=" + value);
        }
        return result;
    }

    /**
     * Returns the size of the entry for {@code key} and {@code value} in
     * user-defined units. The default implementation returns 1 so that size is
     * the number of entries and max size is the maximum number of entries.
     * <p>
     * An entry's size must not change while it is in the cache.
     */
    protected int sizeOf(final K key, final V value) {
        return 1;
    }

    /**
     * Clear the cache, calling {@link #entryRemoved} on each removed entry.
     */
    public final void evictAll() {
        trimToSize(-1); // -1 will evict 0-sized elements
    }

    /**
     * For caches that do not override {@link #sizeOf}, this returns the number of entries in the cache. For all other caches,
     * this returns the number of entries in the cache. For all other caches, this returns the sum of the sizes of the entries
     * in this cache.
     */
    public synchronized final int size() {
        return this.mSize;
    }

    /**
     * For caches that do not override {@link #sizeOf}, this returns the maximum number of entries in the cache. For all other caches,
     * this returns the maximum sum of the sizes of the entries in this cache.
     */
    public synchronized int maxSize() {
        return this.mMaxSize;
    }

    /**
     * Returns the number of times {@link #get} returned a value
     */
    public synchronized final int hitCount() {
        return this.mHitCount;
    }

    /**
     * Returns the number of times {@link #get} returned null or required a new value to be created.
     */
    public synchronized final int missCount() {
        return this.mMissCount;
    }

    /**
     * Returns the number of times {@link #create(Object)} returned a value.
     */
    public synchronized final int createCount() {
        return this.mCreateCount;
    }

    /**
     * Returns the number of times {@link #put} was called.
     */
    public synchronized final int putCount() {
        return this.mPutCount;
    }

    /**
     * Returns the number of value that have been evicted.
     */
    public synchronized final int evictionCount() {
        return this.mEvictionCount;
    }

    /**
     * Returns a copy of the current contents of the cache, ordered from least
     * recently accessed to most recently accessed.
     *
     * @return Map<K,V> Type.
     */
    public synchronized final Map<K, V> snapshot() {
        return new LinkedHashMap<K, V>(this.mMap);
    }

    public synchronized final String toString() {
        final int accesses = this.mHitCount + this.mMissCount;
        final int hitPercent = accesses != 0 ? 100 * this.mHitCount / accesses : 0;
        return String.format("LruCache[maxSize=%d, hits=%d, misses=%d, hitRate=%d]",
                this.mMaxSize, this.mHitCount, this.mMissCount, hitPercent);
    }
}
