package co.soml.android.app.utils;

import java.util.Date;
import java.util.Map;

/**
 *  Copyright (C) 2015 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation
 *  are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied,
 *  reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior
 *  written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with respect to the contents, and
 *  assumes no responsibility for any errors that might appear in the software and documents. This publication and the
 *  contents hereof are subject to change without notice.
 *  @author Larry Pham(Email: larrypham.vn@gmail.com)
 *  @since 6/25/15 3:28 PM
 **/

public class MapUtils {

    public static String getMapStr(final Map<?,?> map, final String key) {
        if (map == null || key == null || !map.containsKey(key) || map.get(key) == null) {
            return "";
        }
        return map.get(key).toString();
    }

    public static int getMapInt(final Map<?,?> map, final String key) {
        return getMapInt(map, key, 0);
    }

    public static int getMapInt(final Map<?,?> map, final String key, int defaultValue) {
        try {
            return Integer.parseInt(getMapStr(map, key));
        } catch (NumberFormatException ex) {
            return defaultValue;
        }
    }

    public static long getMapLong(final Map<?,?> map, final String key) {
        return getMapLong(map, key, 0);
    }

    public static long getMapLong(final Map<?,?> map, final String key, long defaultValue) {
        try {
            return Long.parseLong(getMapStr(map, key));
        } catch (NumberFormatException ex) {
            return defaultValue;
        }
    }

    public static Date getMapDate(final Map<?,?> map, final String key) {
        if (map == null || key == null || !map.containsKey(key)) {
           return null;
        }

        try {
            return (Date) map.get(key);
        } catch (ClassCastException ex) {
            return null;
        }
    }

    public static boolean getMapBool(final Map<?,?> map, final String key) {
        String value = getMapStr(map, key);
        if (value.isEmpty()) {
            return false;
        }

        if (value.startsWith("0")) {
            return false;
        }

        if (value.equalsIgnoreCase("false")) {
            return false;
        }

        return true;
    }
}
