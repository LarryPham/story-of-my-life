package co.soml.android.app.fragments.adapters;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Locale;

import co.soml.android.app.R;
import co.soml.android.app.activities.BaseActivity;
import co.soml.android.app.fragments.BaseFragment;
import co.soml.android.app.fragments.CollectionsFragment;
import co.soml.android.app.fragments.MyStoryFragment;
import co.soml.android.app.fragments.PeopleFragment;
import co.soml.android.app.fragments.SearchFragment;
import co.soml.android.app.models.StoryBean;
import co.soml.android.app.utils.Lists;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.widgets.StoryTextView;

/**
 *  Copyright (C) 2015 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation
 *  are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied,
 *  reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior
 *  written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with respect to the contents, and
 *  assumes no responsibility for any errors that might appear in the software and documents. This publication and the
 *  contents hereof are subject to change without notice.
 *  @author Larry Pham(Email: larrypham.vn@gmail.com)
 *  @since 6/29/15 2:24 PM
 **/

public class MainPagerAdapter extends FragmentPagerAdapter {

    public static final String TAG = LogUtils.makeLogTag(MainPagerAdapter.class.getSimpleName());

    public final SparseArray<WeakReference<BaseFragment>> mFragmentArray = new SparseArray<WeakReference<BaseFragment>>();

    public final List<Holder> mHolderList = Lists.newArrayList();

    public final Context mContext;

    public String mMainPagerType;

    public int mCurrentPage;

    public StoryBean mStoryBean;

    public MainPagerAdapter(Context context, FragmentManager fragmentManager, String mainPagerType) {
        super(fragmentManager);

        mContext = context;
        mMainPagerType = mainPagerType;
    }

    public void add(final Class<? extends BaseFragment> className, final Bundle params, final int titleResId) {
        final Holder holder = new Holder();
        holder.mClassName = className.getName();
        holder.mParams = params;
        holder.mTitleResId = titleResId;

        final int position = mHolderList.size();
        mHolderList.add(position, holder);
        notifyDataSetChanged();
    }

    @Override
    public BaseFragment getItem(int position) {
        final Holder currentHolder = mHolderList.get(position);
        return (BaseFragment) BaseFragment.instantiate(mContext, currentHolder.mClassName, currentHolder.mParams);
    }

    public BaseFragment getFragment(final int position) {
        final WeakReference<BaseFragment> mWeakFragment = mFragmentArray.get(position);
        if (mWeakFragment != null && mWeakFragment.get() != null) {
            return mWeakFragment.get();
        }
        return getItem(position);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public View getTabView(int position) {
        View tabView = LayoutInflater.from(mContext).inflate(R.layout.main_tab_indicator, null);
        StoryTextView tabIcon = (StoryTextView) tabView.findViewById(R.id.tab_title);
	    String title= mContext.getString(mHolderList.get(position).mTitleResId);
	    tabIcon.setText(title);

		if (position == getCurrentPage()) {
			tabIcon.setSelected(true);
		}
        return tabView;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        final BaseFragment fragment = (BaseFragment) super.instantiateItem(container, position);
        final WeakReference<BaseFragment> weakReference = mFragmentArray.get(position);
        if (weakReference != null) {
            weakReference.clear();
        }
        mFragmentArray.put(position, new WeakReference<BaseFragment>(fragment));
        return fragment;
    }

    @Override
    public int getCount() {
        return mHolderList.size();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
        final WeakReference<BaseFragment> weakRef = mFragmentArray.get(position);
        if (weakRef != null) {
            weakRef.clear();
        }
    }

    public CharSequence getPageTitle(int position) {
        /*
        if (mMainPagerType.equalsIgnoreCase(mContext.getString(R.string.main_pager_type))) {
            return mContext.getResources().getStringArray(R.array.main_page_titles)[position].toUpperCase(Locale.getDefault());
        }*/
        return null;
    }

    public int getCurrentPage() {
        return mCurrentPage;
    }


    public void setCurrentPage(int currentPage) {
        this.mCurrentPage = currentPage;
    }

    public enum MainFragment {

        HOME(CollectionsFragment.class),
        STORY(MyStoryFragment.class);
        /*SOCIAL(PeopleFragment.class);*/

        private Class<? extends BaseFragment> mFragmentClass;

        MainFragment(final Class<? extends BaseFragment> fragmentClass) {
            mFragmentClass = fragmentClass;
        }

        public Class<? extends BaseFragment> getFragmentClass() {
            return mFragmentClass;
        }
    }

    public static final class Holder {
        String mClassName;
        Bundle mParams;
        int mTitleResId;
    }
}
