package co.soml.android.app.activities;

import com.squareup.picasso.Picasso;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import butterknife.Bind;
import butterknife.ButterKnife;
import co.soml.android.app.AppConstants;
import co.soml.android.app.R;
import co.soml.android.app.StoryApp;
import co.soml.android.app.controllers.ControllerMessage;
import co.soml.android.app.controllers.UserProfileController;
import co.soml.android.app.fragments.adapters.ProfileTabsAdapter;
import co.soml.android.app.models.Account;
import co.soml.android.app.models.StoryBean;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.models.StoryFeedList;
import co.soml.android.app.models.User;
import co.soml.android.app.services.StatusResult;
import co.soml.android.app.utils.AccountUtils;
import co.soml.android.app.utils.EditTextUtils;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.widgets.CircleImageView;
import co.soml.android.app.widgets.StoryFollowButton;
import co.soml.android.app.widgets.StoryRecyclerView;
import co.soml.android.app.widgets.StoryTextView;
import co.soml.android.app.widgets.StoryViewPager;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.14.2015
 */
public class UserProfileActivity extends BaseActivity {

    public static final String TAG = AppConstants.PREFIX + UserProfileActivity.class.getSimpleName();

    public static final int FOLLOW_USER = 1;
    public static final int UNFOLLOW_USER = 2;

    public static final int EDIT_USER_PROFILE = 3;
    public static final int EDIT_NAME = 4;

    private UserProfileController mController;
    private StoryDataModel mDataModel;
    private StoryApp mApp;
    private Context mContext;

	private Intent mProfileIntent;
	private Account mActiveAccount;
	private User mCurrentUser;

	private ProfileTabsAdapter mProfileTabsAdapter;

	private Bundle mProfileParams;
	private Toolbar mActionToolBar;

	private int mStoriesNumber;
	private long mFollowingNumber;
	private long mFollowersNumber;
	private StoryBean mStoryBean;

    @Bind(R.id.user_full_name)
    protected StoryTextView mUserFullName;
    @Bind(R.id.user_description)
    protected StoryTextView mUserDescription;
    @Bind(R.id.account_profile_image)
    protected CircleImageView mUserProfileImage;
    @Bind(R.id.profile_tablayout)
    protected TabLayout mProfileTabs;
    @Bind(R.id.profile_view_pager)
    protected StoryViewPager mProfileViewPager;
    @Bind(R.id.user_follow_button)
    protected StoryFollowButton mUserFollowButton;
    @Bind(R.id.edit_profile_button)
    protected StoryTextView mEditProfileButton;
    protected StoryRecyclerView mProfileRecyclerView;

    protected int mOverScrollRow;
    protected Bundle mParams;

    protected StoryFeedList mStories = new StoryFeedList();
    protected int mUserId;
    protected User mProfileUser;
    protected String mEditableCommands;

    public AppInterfaces.UnfollowUserListener mUnFollowUserListener = new AppInterfaces.UnfollowUserListener() {
        @Override
        public void onUnfollowUser(View view) {
            final String fn = "[UNFOLLOW_USER]";
            final int userId = (Integer) view.getTag();
            if (mController != null) {
                final int apiVersion = 1;
                final String authToken = AccountUtils.getAuthToken(UserProfileActivity.this);
                LogUtils.LOGD(TAG, fn + String.format(" unfollowing the user[%d]", userId));
                final UserProfileController.RequestParams params = new UserProfileController.RequestParams(userId, apiVersion, authToken);
                mController.sendMessage(ControllerMessage.REQUEST_TO_SERVER_USER_UNFOLLOW, params);
            }
        }
    };
    public AppInterfaces.FollowUserListener mFollowUserListener = new AppInterfaces.FollowUserListener() {
        @Override
        public void onFollowUser(View view) {
            final String fn = "[FOLLOW_USER]";
            final int userId = (Integer) view.getTag();
            if (mController != null) {
                final int apiVersion = 1;
                final String authToken = AccountUtils.getAuthToken(UserProfileActivity.this);
                LogUtils.LOGD(TAG, fn + String.format(" following the user[%d]", userId));
                final UserProfileController.RequestParams params = new UserProfileController.RequestParams(userId, apiVersion, authToken);
                mController.sendMessage(ControllerMessage.REQUEST_TO_SERVER_USER_FOLLOW, params);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
	    setContentView(R.layout.layout_account_profile);
        ButterKnife.bind(this);

	    mProfileIntent = getIntent();
        final Bundle params = mProfileIntent.getExtras();
	    mActiveAccount = (Account) params.get("CHOSEN_ACCOUNT");
	    mCurrentUser = (User) params.get("CURRENT_USER");
        mUserId = params.getInt("USER_ID");
        mEditableCommands = params.getString("EDITABLE");

        mContext = this.getApplicationContext();
        mApp = (StoryApp) getApplicationContext();
        mDataModel = mApp.getAppDataModel();
        mController = new UserProfileController(UserProfileActivity.this, mDataModel);
	    mProfileTabsAdapter = new ProfileTabsAdapter(this, getSupportFragmentManager());
        onBindingUI();
    }

	public Bundle buildArguments(StoryDataModel.ProfilePageType pageType) {
		final Bundle params = new Bundle();
		final String accountAuthToken = AccountUtils.getAuthToken(UserProfileActivity.this);
		final long userId = AccountUtils.getUserId(UserProfileActivity.this);

		params.putInt(AppConstants.X_API_VERSION, 1);
		params.putString(AppConstants.X_API_TOKEN, AccountUtils.getAuthToken(this));
        params.putInt("USER_ID", mUserId);

		if (pageType == StoryDataModel.ProfilePageType.FOLLOWING_PROFILE) {
            params.putString("ListType", "FOLLOWING_USERS");
		} else if (pageType == StoryDataModel.ProfilePageType.FOLLOWERS_PROFILE) {
            params.putString("ListType","FOLLOWERS");
		}
		return params;
	}

	protected void onBindingUI() {
		final ProfileTabsAdapter.ProfileFragment[] profileFragments = ProfileTabsAdapter.ProfileFragment.values();
		for (ProfileTabsAdapter.ProfileFragment fragment: profileFragments) {
			if (fragment == ProfileTabsAdapter.ProfileFragment.STORIES) {
				mProfileParams = buildArguments(StoryDataModel.ProfilePageType.STORIES_PROFILE);
				mProfileTabsAdapter.add(fragment.getFragmentClass(), mProfileParams);
			} else if (fragment == ProfileTabsAdapter.ProfileFragment.FOLLOWING) {
				mProfileParams = buildArguments(StoryDataModel.ProfilePageType.FOLLOWING_PROFILE);
				mProfileTabsAdapter.add(fragment.getFragmentClass(), mProfileParams);
			} else if (fragment == ProfileTabsAdapter.ProfileFragment.FOLLOWERS) {
				mProfileParams = buildArguments(StoryDataModel.ProfilePageType.FOLLOWERS_PROFILE);
				mProfileTabsAdapter.add(fragment.getFragmentClass(), mProfileParams);
			}
		}
		mProfileTabs.setTabMode(TabLayout.MODE_FIXED);
		mProfileTabs.setTabGravity(TabLayout.GRAVITY_CENTER);
        mProfileViewPager.setAdapter(mProfileTabsAdapter);
        mProfileTabs.setupWithViewPager(mProfileViewPager);

		if (mCurrentUser != null) {
			mStoriesNumber = mCurrentUser.getStoryCount();
			mFollowersNumber = mCurrentUser.getFollowerAmount();
			mFollowingNumber = mCurrentUser.getFollowingAmount();
		}

        int currentUserID = AccountUtils.getUserId(this);
        final User currentUser = StoryApp.getInstance().getCurrentUser();

        if (!TextUtils.isEmpty(mEditableCommands)) {
            mEditProfileButton.setVisibility(View.VISIBLE);
            mEditProfileButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View editProfileButton) {
                    final String fn = "[EditProfile] Trigger to start the EditProfile Screen";
                    final Intent editProfileIntent = new Intent(UserProfileActivity.this, EditUserProfileActivity.class);

                    final Bundle param = new Bundle();
                    param.putParcelable("CurrentUser", currentUser);
                    editProfileIntent.putExtras(param);
                    startActivityForResult(editProfileIntent, EDIT_USER_PROFILE);
                    UserProfileActivity.this.finish();
                }
            });
        } else {
            mEditProfileButton.setVisibility(View.GONE);
        }

        if (mCurrentUser != null) {
            mUserFullName.setText(TextUtils.isEmpty(mCurrentUser.getFullName()) ? "" : mCurrentUser.getFullName());
            mUserDescription.setText(TextUtils.isEmpty(mCurrentUser.getDescription()) ? "" : mCurrentUser.getDescription());
            mUserFollowButton.setVisibility(View.GONE);
            Picasso.with(this).load(TextUtils.isEmpty(mCurrentUser.getAvatarOriginalUrl()) ? null
                    : mCurrentUser.getAvatarOriginalUrl()).placeholder(R.drawable.person_image_empty).into(mUserProfileImage);
        } else {
            if (currentUserID == mUserId) {
                mStoriesNumber = currentUser.getStoryCount();
                mFollowersNumber = currentUser.getFollowerAmount();
                mFollowingNumber = currentUser.getFollowingAmount();

                setupTabLayout();
                mUserFullName.setText(TextUtils.isEmpty(currentUser.getFullName()) ? "" : currentUser.getFullName());
                mUserDescription.setText(TextUtils.isEmpty(currentUser.getDescription()) ? "" : currentUser.getDescription());
                mUserFollowButton.setVisibility(View.GONE);
                Picasso.with(this).load(TextUtils.isEmpty(currentUser.getAvatarOriginalUrl()) ? null
                        : currentUser.getAvatarOriginalUrl()).placeholder(R.drawable.person_image_empty).into(mUserProfileImage);
            } else {

                final String authToken = AccountUtils.getAuthToken(this);
                LogUtils.LOGD(TAG, String.format("[REQUEST_TO_SERVER] Loading UserInfo[%d] From Server", mUserId));
                final UserProfileController.RequestParams requestParams = new UserProfileController.RequestParams(mUserId, 1, authToken);
                mController.sendMessage(ControllerMessage.REQUEST_TO_SERVER_USER_DETAIL, requestParams);
            }
        }
        setupTabLayout();
	}

    protected void setupTabLayout() {
        for (int index = 0;  index < mProfileTabs.getTabCount(); index++) {
            final TabLayout.Tab tab= mProfileTabs.getTabAt(index);
            View tabView = LayoutInflater.from(this).inflate(R.layout.layout_profile_tab, null);

            final StoryTextView profileTabLabel = (StoryTextView) tabView.findViewById(R.id.profile_tab_label);
            final StoryTextView profileTabNumber = (StoryTextView) tabView.findViewById(R.id.profile_tab_number);
            if (index == 0) {
                profileTabLabel.setText(getString(R.string.profile_stories_tabs_label));
                profileTabNumber.setText(String.valueOf(mStoriesNumber));
                profileTabNumber.setTag("StoriesNumber");
            }

            if (index == 1) {
                profileTabLabel.setText(getString(R.string.profile_following_tabs_label));
                profileTabNumber.setText(String.valueOf(mFollowingNumber));
                profileTabNumber.setTag("FollowingUsers");
            }

            if (index == 2) {
                profileTabLabel.setText(getString(R.string.profile_followers_tabs_label));
                profileTabNumber.setText(String.valueOf(mFollowersNumber));
                profileTabNumber.setTag("Followers");
            }
            profileTabLabel.setAllCaps(true);
            if (tab != null) {
                tab.setCustomView(tabView);
            }
        }
    }
	@Override
	public Toolbar getActionBarToolBar() {
		if (mActionToolBar == null) {
			mActionToolBar = (Toolbar) findViewById(R.id.profile_toolbar);
		}
		return mActionToolBar;
	}

	public void onPostCreate() {
		setSupportActionBar(getActionBarToolBar());
		if (getSupportActionBar() != null) {
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_close);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
        ButterKnife.unbind(this);
	}

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        onPostCreate();
    }

    @Override
    public void invalidate() {

    }

    @Override
    public void invalidate(Object... params) {
		if (params[0] instanceof InvalidateParam) {
			final InvalidateParam param = (InvalidateParam) params[0];
			LogUtils.LOGD(TAG, "Invalidating the views from the profile's screen which has interacted");
			switch (param.getMessage()) {
				case ControllerMessage.REQUEST_TO_SERVER_LIST_STORIES: {
					LogUtils.LOGD(TAG, "Invaliding the view when request to server for getting the list of current user's stories");
					break;
				}
                case ControllerMessage.REQUEST_TO_SERVER_USER_COMPLETED: {
                    LogUtils.LOGD(TAG, "Invalidating the view when request to server for getting the UserInfo from server");
                    final User user = (User) param.getObj();
                    mProfileUser = user;

                    LogUtils.LOGD(TAG, String.format("USER_DETAIL: %s", mProfileUser.toString()));
                    mUserFullName.setText(mProfileUser.getFullName());
                    mUserDescription.setText(mProfileUser.getDescription());
                    mUserFollowButton.setVisibility(View.VISIBLE);
                    if (mProfileUser.isFollowedByCurrentUser()) {
                        mUserFollowButton.setSelected(true);
                        mUserFollowButton.setIsFollowed(true);
                    } else {
                        mUserFollowButton.setSelected(false);
                        mUserFollowButton.setIsFollowed(false);
                    }
                    mUserFollowButton.setTag(mUserId);
                    mUserFollowButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            toggleFollowUser(mUserFollowButton, mProfileUser);
                        }
                    });
                    Picasso.with(this).load(TextUtils.isEmpty(mProfileUser.getAvatarOriginalUrl()) ? null
                            : mProfileUser.getAvatarOriginalUrl()).placeholder(R.drawable.person_image_empty).into(mUserProfileImage);

                    final StoryTextView mStoriesNumberTextView = (StoryTextView) mProfileTabs.findViewWithTag("StoriesNumber");
                    if (mStoriesNumberTextView != null) {
                        mStoriesNumberTextView.setText(String.valueOf(mProfileUser.getStoryCount()));
                    }
                    final StoryTextView mFollowingUsersTextView = (StoryTextView) mProfileTabs.findViewWithTag("FollowingUsers");
                    if (mFollowingUsersTextView != null) {
                        mFollowingUsersTextView.setText(String.valueOf(mProfileUser.getFollowingAmount()));
                    }
                    final StoryTextView mFollowersTextView = (StoryTextView) mProfileTabs.findViewWithTag("Followers");
                    if (mFollowersTextView != null) {
                        mFollowersTextView.setText(String.valueOf(mProfileUser.getFollowerAmount()));
                    }
                    break;
                }
                case ControllerMessage.REQUEST_TO_SERVER_USER_FOLLOW_COMPLETED: {
                    LogUtils.LOGD(TAG, "Invalidating the view when request to server for getting the UserInfo from server");
                    final StatusResult result = (StatusResult) param.getObj();
                    final int userId = result.getUserId();
                    final String status = result.getStatus();

                    if (status.equalsIgnoreCase("following")) {
                        if (mProfileUser != null) {
                            mProfileUser.setFollowedByCurrentUser(true);
                            LogUtils.LOGD(TAG, String.format("USER_DETAIL: %s", mProfileUser.toString()));
                            mUserFollowButton.setSelected(true);
                            mUserFollowButton.setIsFollowed(true);
                        }
                    }
                    break;
                }
                case ControllerMessage.REQUEST_TO_SERVER_USER_UNFOLLOW_COMPLETED: {
                    LogUtils.LOGD(TAG, "Invalidating the view when request to server for getting the UserInfo from server");
                    final StatusResult result = (StatusResult) param.getObj();
                    final int userId = result.getUserId();
                    final String status = result.getStatus();

                    if (status.equalsIgnoreCase("not_following")) {
                        if (mProfileUser != null) {
                            mProfileUser.setFollowedByCurrentUser(false);
                            LogUtils.LOGD(TAG, String.format("USER_DETAIL: %s", mProfileUser.toString()));
                            mUserFollowButton.setSelected(false);
                            mUserFollowButton.setIsFollowed(false);
                        }
                    }
                    break;
                }
			}
            mProfileTabsAdapter.notifyDataSetChanged();
        }
    }

    public void showPopupNoNetworkSnackBar() {
        onCreateSnackBar(StoryMainActivity.DialogMsg.NO_NETWORK_CONNECTION, null).show();
    }

    protected void toggleFollowUser(final View view,final User user) {
        if (user.isFollowedByCurrentUser()) {
            if (mUnFollowUserListener != null) {
                mUnFollowUserListener.onUnfollowUser(view);
            }
        } else {
            if (mFollowUserListener != null) {
                mFollowUserListener.onFollowUser(view);
            }
        }
    }

    protected Snackbar onCreateSnackBar(int id, Bundle args) {
        Snackbar mDialogResult = null;
        switch (id) {
            case StoryMainActivity.DialogMsg.NO_NETWORK_CONNECTION: {
                final View coordinateLayoutView = findViewById(R.id.snackbar_position);
                mDialogResult = Snackbar.make(coordinateLayoutView, getString(R.string.msg_no_network_connection), Snackbar.LENGTH_SHORT);
                break;
            }
        }
        return mDialogResult;
    }
}
