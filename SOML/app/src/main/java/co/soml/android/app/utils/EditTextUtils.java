package co.soml.android.app.utils;

import android.content.Context;
import android.text.TextUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.23.2015
 */
public class EditTextUtils {

    private EditTextUtils() {
        throw new AssertionError();
    }

    public static String getText(EditText editText) {
        if (editText.getText() == null) {
            return "";
        }
        return editText.getText().toString();
    }

    public static void moveToEnd(EditText editText) {
        if (editText.getText() == null) {
            return;
        }
        editText.setSelection(editText.getText().toString().length());
    }

    public static boolean isEmpty(EditText editText) {
        return TextUtils.isEmpty(getText(editText));
    }

    public static void hideSoftInput(EditText editText) {
        if (editText == null) {
            return;
        }
        InputMethodManager imm = getInputMethodManager(editText);
        if (imm != null) {
            imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
        }
    }

    public static void showSoftInput(EditText editText) {
        if (editText == null) {
            return;
        }
        editText.requestFocus();
        InputMethodManager imm = getInputMethodManager(editText);
        if (imm != null) {
            imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    private static InputMethodManager getInputMethodManager(EditText editText) {
        Context context = editText.getContext();
        return (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
    }

}
