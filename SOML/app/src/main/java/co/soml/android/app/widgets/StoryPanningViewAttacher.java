package co.soml.android.app.widgets;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.res.Configuration;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.view.ViewTreeObserver;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;


import java.lang.ref.WeakReference;

import co.soml.android.app.utils.LogUtils;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since May.13.2015
 */
public class StoryPanningViewAttacher implements ViewTreeObserver.OnGlobalLayoutListener {
    public static final String TAG = LogUtils.makeLogTag(StoryPanningViewAttacher.class);
    public static final int DEFAULT_PANNING_DURATION_IN_MS = 5000;

    private enum WAY {LEFT_TO_RIGHT, RIGHT_TO_LEFT, TOP_TO_BOTTOM, BOTTOM_TO_TOP}

    ;

    private WeakReference<ImageView> mImageView;
    private int mImageViewTop, mImageViewRight, mImageVewBottom, mImageViewLeft;
    private ViewTreeObserver mViewTreeObserver;
    private Matrix mMatrix;
    private RectF mDisplayRect = new RectF();
    private ValueAnimator mCurrentAnimator;
    private LinearInterpolator mLinearInterpolator;
    private boolean isPortrait;
    private long mDuration;
    private long mCurrentPlayTime;
    private long mTotalTime;
    private WAY mWay;
    private boolean isPanning;

    public StoryPanningViewAttacher(ImageView imageView, long duration) {
        if (imageView == null) {
            throw new IllegalArgumentException("ImageView must not be null");
        }
        if (!hasDrawable(imageView)) {
            throw new IllegalArgumentException("Drawable must not be null");
        }

        mLinearInterpolator = new LinearInterpolator();
        mDuration = duration;
        mImageView = new WeakReference<ImageView>(imageView);

        mViewTreeObserver = imageView.getViewTreeObserver();
        mViewTreeObserver.addOnGlobalLayoutListener(this);

        setImageViewScaleTypeMatrix(imageView);
        mMatrix = imageView.getImageMatrix();
        if (mMatrix == null) {
            mMatrix = new Matrix();
        }
        isPortrait = imageView.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
        update();
    }

    public final void cleanUp() {
        if (mImageView != null) {
            getImageView().getViewTreeObserver().removeOnGlobalLayoutListener(this);
        }

        mViewTreeObserver = null;
        stopSpanning();
        mImageView = null;
    }


    public void update() {
        mWay = null;
        mTotalTime = 0;
        mCurrentPlayTime = 0;
        getImageView().post(new Runnable() {
            @Override
            public void run() {
                scale();
                refreshDisplayRect();
            }
        });
    }

    public boolean isPanning() {
        return isPanning;
    }

    public void startSpanning() {
        if (isPanning) {
            return;
        }

        isPanning = true;
        final Runnable panningRunnable = new Runnable() {
            @Override
            public void run() {
                animate();
            }
        };
        getImageView().post(panningRunnable);
    }

    public void stopSpanning() {
        if (!isPanning) {
            return;
        }

        isPanning = false;
        LogUtils.LOGD(TAG, "Panning animation stopped by user");
        if (mCurrentAnimator != null) {
            mCurrentAnimator.removeAllListeners();
            mCurrentAnimator.cancel();
            mCurrentAnimator = null;
        }
        mTotalTime += mCurrentPlayTime;
        LogUtils.LOGD(TAG, "TotalTime: " + mTotalTime);
    }

    @Override
    public void onGlobalLayout() {
        ImageView imageView = getImageView();
        if (null != imageView) {
            final int top = imageView.getTop();
            final int right = imageView.getRight();
            final int left = imageView.getLeft();
            final int bottom = imageView.getBottom();

            if (top != mImageViewTop || bottom != mImageVewBottom || left != mImageViewLeft || right != mImageViewRight) {
                update();
                mImageViewTop = top;
                mImageViewLeft = left;
                mImageViewRight = right;
                mImageVewBottom = bottom;
            }
        }
    }

    public final ImageView getImageView() {
        ImageView imageView = null;
        if (mImageView != null) {
            imageView = mImageView.get();
        }

        if (imageView == null) {
            cleanUp();
            throw new IllegalStateException("ImageView no longer exists. You should not use this PlanningViewAttacher");
        }
        return imageView;
    }

    private int getDrawableIntrinsicHeight() {
        return getImageView().getDrawable().getIntrinsicHeight();
    }

    private int getDrawableIntrinsicWidth() {
        return getImageView().getDrawable().getIntrinsicWidth();
    }

    private int getImageViewWidth() {
        return getImageView().getWidth();
    }

    private int getImageViewHeight() {
        return getImageView().getHeight();
    }

    private static void setImageViewScaleTypeMatrix(ImageView imageView) {
        if (imageView != null && !(imageView instanceof StoryPanningView)) {
            imageView.setScaleType(ImageView.ScaleType.MATRIX);
        }
    }

    private static boolean hasDrawable(ImageView imageView) {
        return imageView != null && imageView.getDrawable() != null;
    }

    private void changeWay() {
        if (mWay == WAY.LEFT_TO_RIGHT) {
            mWay = WAY.RIGHT_TO_LEFT;
        } else if (mWay == WAY.RIGHT_TO_LEFT) {
            mWay = WAY.LEFT_TO_RIGHT;
        } else if (mWay == WAY.TOP_TO_BOTTOM) {
            mWay = WAY.BOTTOM_TO_TOP;
        } else if (mWay == WAY.BOTTOM_TO_TOP) {
            mWay = WAY.TOP_TO_BOTTOM;
        }
    }

    private void animate() {
        refreshDisplayRect();
        if (mWay == null) {
            mWay = isPortrait ? WAY.RIGHT_TO_LEFT : WAY.BOTTOM_TO_TOP;
        }

        LogUtils.LOGD(TAG, "mWay: " + mWay);
        LogUtils.LOGD(TAG, "mDisplayRect: " + mDisplayRect);
        long remainingDuration = mDuration - mTotalTime;
        if (isPortrait) {
            if (mWay == WAY.RIGHT_TO_LEFT) {
                animate(mDisplayRect.left, mDisplayRect.left - (mDisplayRect.right - getImageViewWidth()), remainingDuration);
            } else {
                animate(mDisplayRect.left, 0.0f, remainingDuration);
            }
        } else {
            if (mWay == WAY.BOTTOM_TO_TOP) {
                animate(mDisplayRect.top, mDisplayRect.top - (mDisplayRect.bottom - getImageViewHeight()), remainingDuration);
            } else {
                animate(mDisplayRect.top, 0.0f, remainingDuration);
            }
        }
    }

    private void animate(float start, float end, long duration) {
        LogUtils.LOGD(TAG, "StartSpanning: " + start + " to " + end + " , in " + duration + " ms ");
        mCurrentAnimator = ValueAnimator.ofFloat(start, end);
        mCurrentAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float value = (Float) valueAnimator.getAnimatedValue();
                mMatrix.reset();
                applyScaleOnMatrix();
                if (isPortrait) {
                    mMatrix.postTranslate(value, 0);
                } else {
                    mMatrix.postTranslate(0, value);
                }
                refreshDisplayRect();
                mCurrentPlayTime = valueAnimator.getCurrentPlayTime();
                setCurrentImageMatrix();
            }
        });

        mCurrentAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                LogUtils.LOGD(TAG, "Animation has finished, StartPanning in the other way");
                changeWay();
                animate();
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                LogUtils.LOGD(TAG, "Panning animation cancelled");
            }
        });
        mCurrentAnimator.setDuration(duration);
        mCurrentAnimator.setInterpolator(mLinearInterpolator);
        mCurrentAnimator.start();
    }

    private void setCurrentImageMatrix() {
        getImageView().setImageMatrix(mMatrix);
        getImageView().invalidate();
        getImageView().requestLayout();
    }

    private void refreshDisplayRect() {
        mDisplayRect.set(0, 0, getDrawableIntrinsicWidth(), getDrawableIntrinsicHeight());
        mMatrix.mapRect(mDisplayRect);
    }

    private void scale() {
        mMatrix.reset();
        applyScaleOnMatrix();
        setCurrentImageMatrix();
    }

    private void applyScaleOnMatrix() {
        int drawableSize = isPortrait ? getDrawableIntrinsicHeight() : getDrawableIntrinsicWidth();
        int imageViewSize = isPortrait ? getImageViewHeight() : getImageViewWidth();

        float scaleFactor = (float) (imageViewSize) / (float) drawableSize;
        mMatrix.postScale(scaleFactor, scaleFactor);
    }
}
