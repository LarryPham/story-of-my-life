/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 * @author: Larry Pham (email: larrypham.vn@gmail.com)
 * Date: 6/2/15 4:17 PM
 **/

package co.soml.android.app.controllers;

import android.content.Intent;
import android.os.Message;

import java.util.List;

import co.soml.android.app.StoryApp;
import co.soml.android.app.activities.BaseActivity;
import co.soml.android.app.activities.InvalidateParam;
import co.soml.android.app.fragments.BaseFragment;
import co.soml.android.app.fragments.PeopleFragment;
import co.soml.android.app.fragments.ProfileStoriesFragment;
import co.soml.android.app.models.JsonUser;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.models.StoryUserList;
import co.soml.android.app.models.User;
import co.soml.android.app.services.UserListResult;
import co.soml.android.app.services.tasks.Action;
import co.soml.android.app.utils.LogUtils;

public class PeopleController extends BaseController {

    public static final String TAG = LogUtils.makeLogTag(PeopleController.class.getSimpleName());
    private StoryDataModel mDataModel;
    private StoryApp mStoryApp;
    private BaseActivity mActivity;
    private PeopleFragment mPeopleFragment;

    public PeopleController(BaseActivity activity, StoryDataModel dataModel) {
        super(activity, dataModel);

        mActivity = activity;
        mStoryApp = (StoryApp) activity.getApplicationContext();
        mDataModel = dataModel;
    }

    public PeopleController(BaseActivity activity, StoryDataModel dataModel, boolean isFirstRunningMode) {
        super(activity, dataModel, isFirstRunningMode);
        mActivity = (BaseActivity) activity;
        mStoryApp = (StoryApp) activity.getApplicationContext();
        mDataModel = dataModel;
    }

    public PeopleController(BaseActivity activity, BaseFragment fragment, StoryDataModel dataModel) {
        super(activity, fragment, dataModel);
        mActivity = (BaseActivity) activity;
        mStoryApp = (StoryApp) activity.getApplicationContext();
        mDataModel = dataModel;
    }

    @Override
    protected void handleMessage(Message msg) {
        final String fn = "handleMessage() ";
        switch (msg.what) {
            case ControllerMessage.REQUEST_TO_SERVER_USER_FOLLOW: {
                LogUtils.LOGD(TAG, fn + "REQUEST_TO_SERVER_USER_FOLLOWING ");

                final Intent intent = obtainIntent(msg.what);
                final RequestParam requestParam = (RequestParam) msg.obj;

                intent.putExtra(Action.X_API_VERSION, requestParam.mAPIVersion);
                intent.putExtra(Action.X_AUTH_TOKEN, requestParam.mAuthToken);
                intent.putExtra(Action.USER_ID, requestParam.mUserId);

                intent.setAction(Action.REQUEST_TO_SERVER_USER_FOLLOW);
                mActivity.startService(intent);
                break;
            }

            case ControllerMessage.REQUEST_TO_SERVER_USER_FOLLOWERS: {
                LogUtils.LOGD(TAG, fn + "REQUEST_TO_SERVER_USER_FOLLOWERS");

                final Intent intent = obtainIntent(msg.what);
                final RequestParam requestParam = (RequestParam) msg.obj;

                intent.putExtra(Action.X_API_VERSION, requestParam.mAPIVersion);
                intent.putExtra(Action.X_AUTH_TOKEN, requestParam.mAuthToken);
                intent.putExtra(Action.USER_ID, requestParam.mUserId);

                intent.setAction(Action.REQUEST_TO_SERVER_USER_FOLLOWERS);
                mActivity.startService(intent);
                break;
            }

            case ControllerMessage.REQUEST_TO_SERVER_USER_FOLLOW_COMPLETED: {
                if (checkOwner(msg)) {
                    LogUtils.LOGD(TAG, fn + "REQUEST_TO_SERVER_USER_FOLLOW_COMPLETED");
                    final UserListResult result = (UserListResult) msg.obj;
                    final List<User> userList = result.getUserList();
                    StoryUserList users = mDataModel.getFollowingUsers();

                    if (users != null) {
                        if (userList != null && userList.size() > 0) {
                            for (int index = 0; index < userList.size(); index++) {
                                users.add(new User(userList.get(index)));
                                LogUtils.LOGD(TAG, String.format("[GUN]FollowingUser: [%08d:%s]", userList.get(index).getId(), userList.get(index).getFullName()));
                            }
                        }
                    } else {
                        LogUtils.LOGD(TAG, fn + "Non-Existing Following Users Into Currently DataModel");
                        users = new StoryUserList();
                        if (userList != null && userList.size() > 0) {
                            for (User user : userList) {
                                if (user != null) {
                                    users.add(new User(user));
                                }
                            }
                        }
                        mDataModel.setFollowingUsers(users);
                    }

                    // Invalidating the changes for PeopleFragment's instance
                    final InvalidateParam param = new InvalidateParam(msg.what, msg.obj);
                    if (mPeopleFragment != null) {
                        mPeopleFragment.invalidate(param);
                    }
                }
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_USER_FOLLOWERS_COMPLETED: {
                if (checkOwner(msg)) {
                    LogUtils.LOGD(TAG, fn + "REQUEST_TO_SERVER_USER_FOLLOWERS_COMPLETED");
                    final UserListResult result = (UserListResult) msg.obj;
                    final List<User> userList = result.getUserList();
                    // Get the existing list of followers currently...
                    StoryUserList users = mDataModel.getFollowers();
                    if (users != null) {
                        if (userList != null && userList.size() > 0) {
                            for (int index = 0; index < userList.size(); index++) {
                                users.add(new User(userList.get(index)));
                                LogUtils.LOGD(TAG, String.format("[GUN]Followers: [%08d:%s]", userList.get(index).getId(), userList.get(index).getFullName()));
                            }
                        }
                    } else {
                        LogUtils.LOGD(TAG, fn + "Non-Existing Followers Into Current DataModel");
                        users = new StoryUserList();
                        if (userList != null && userList.size() > 0) {
                            for (User user : userList) {
                                if (user != null) {
                                    users.add(new User(user));
                                }
                            }
                        }
                        mDataModel.setFollowers(users);
                    }

                    // Invalidating the changes for PeopleFragment's instance
                    final InvalidateParam param = new InvalidateParam(msg.what, msg.obj);
                    if (mPeopleFragment != null) {
                        mPeopleFragment.invalidate(param);
                    }
                }
                break;
            }
            case ControllerMessage.REQUEST_TO_REQUERY: {
                LogUtils.LOGD(TAG, fn + "REQUEST_TO_REQUERY_DATA_");
                if (mDataModel.getFollowers() != null && mDataModel.getFollowers().size() > 0) {
                    LogUtils.LOGD(TAG, "Refreshing the adapter for current fragment");
                    mFragment.invalidate();
                }
                break;
            }
        }
    }

    public static class RequestParam {
        public int mUserId;
        public String mAuthToken;
        public int mAPIVersion;

        public RequestParam(int userId, String authToken, int APIVersion) {
            mUserId = userId;
            mAuthToken = authToken;
            mAPIVersion = APIVersion;
        }
    }
}
