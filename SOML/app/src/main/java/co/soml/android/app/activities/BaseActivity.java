package co.soml.android.app.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TypefaceSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import co.soml.android.app.AppConstants;
import co.soml.android.app.R;
import co.soml.android.app.controllers.BaseController;
import co.soml.android.app.fragments.BaseFragment;
import co.soml.android.app.models.Image;
import co.soml.android.app.utils.BitmapUtil;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.utils.PreferenceUtils;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its
 * documentation are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable
 * form without the prior written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with
 * respect to the contents, and assumes no responsibility for any errors that might appear in the software and
 * documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.01.2015
 */
public abstract class BaseActivity extends AppCompatActivity implements FragmentManager.OnBackStackChangedListener {

    public static final String TAG = AppConstants.PREFIX + BaseActivity.class.getSimpleName();
    protected BaseController mController = null;
    private static final int MAIN_CONTENT_FADE_IN_DURATION = 250;

    private Toolbar mActionBarToolBar;
    private BaseFragment mContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!PreferenceUtils.isIntroductionDone(this)) {
            final Intent intent = new Intent(this, WelcomeActivity.class);
            startActivity(intent);
            finish();
        }
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            if (actionBar.getTitle() != null) {
                SpannableString spanString = new SpannableString(actionBar.getTitle());
                spanString.setSpan(new TypefaceSpan("NotoSans-Regular.ttf"), 0, spanString.length(),
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                actionBar.setTitle(spanString);
            }
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }
        getSupportFragmentManager().addOnBackStackChangedListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    protected boolean shouldAddToBackStack(FragmentManager fragmentManager, String fragmentKey, Bundle extras) {
        return true;
    }

    protected void handleBackStackChanged() {

    }

    @Override
    public void onBackStackChanged() {
        BaseFragment contentFragment = (BaseFragment) getSupportFragmentManager().findFragmentById(R.id.frame_content);
        if (contentFragment == null) {
            return;
        }

        mContent = contentFragment;
        handleBackStackChanged();
    }

    @Override
    protected void onDestroy() {
        if (mController != null) {
            mController.onDestroy();
        }
        super.onDestroy();
    }

    public Image getDefaultStoryImage(String key) {
        Bitmap bm = BitmapUtil.ResourcesPool.resolve("DefaultStoryImage").getBitmap(key);
        Image newImage = null;
        if (bm == null) {
            if (key.compareTo("blank") > 0) {
                bm = BitmapFactory.decodeResource(this.getResources(), android.R.drawable.screen_background_light);
            }
            if (bm != null) {
                BitmapUtil.ResourcesPool.resolve("DefaultStoryImage").putBitmap(key, bm);
            }
        }
        if (bm != null) {
            newImage = new Image(bm);
        }
        return newImage;
    }

    public Toolbar getActionBarToolBar() {
        if (mActionBarToolBar == null) {
            mActionBarToolBar = (Toolbar) findViewById(R.id.nav_drawer_toolbar);
            if (mActionBarToolBar != null) {
                setSupportActionBar(mActionBarToolBar);
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);
            }
        }
        return mActionBarToolBar;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        View mainContent = findViewById(R.id.frame_content);
        if (mainContent != null) {
            mainContent.setAlpha(0);
            mainContent.animate().alpha(1).setDuration(MAIN_CONTENT_FADE_IN_DURATION);
        } else {
            LogUtils.LOGD(TAG, "No View With ID frame_content to fade in");

        }
    }

    /**
     * Adds new Content's Fragment alter to the empty content into layout
     *
     * @param fragment    The BaseFragment such as SigninFragment,NewUserFragment, SocialSigninFragment
     * @param fragmentKey The FragmentKey
     * @param extras      The Bundle Object which will be added into the Fragment object.
     */
    public void addContent(BaseFragment fragment, String fragmentKey, Bundle extras) {
        if (fragment.equals(mContent)) {
            fragment.setBundle(extras);
            return;
        }
        mContent = fragment;
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        mContent.setFragmentKey(fragmentKey);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.replace(R.id.frame_content, mContent, fragmentKey);
        transaction.commit();
        fragment.setBundle(extras);
    }

    public void switchContent(BaseFragment fragment, String fragmentKey, Bundle extras, boolean showContent) {
        if (fragment.equals(mContent)) {
            return;
        }
        mContent = fragment;
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);

        if (shouldAddToBackStack(fragmentManager, fragmentKey, extras)) {
            transaction.addToBackStack(String.valueOf(fragmentKey));
        }
        transaction.replace(R.id.frame_content, fragment, fragmentKey);
        transaction.commit();
        fragment.setBundle(extras);
    }

    protected abstract void invalidate();

    protected abstract void invalidate(Object... params);
}
