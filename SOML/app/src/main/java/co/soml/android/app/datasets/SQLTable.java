package co.soml.android.app.datasets;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import java.util.Map;

import co.soml.android.app.utils.LogUtils;

/**
 *  Copyright (C) 2015 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation
 *  are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied,
 *  reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior
 *  written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with respect to the contents, and
 *  assumes no responsibility for any errors that might appear in the software and documents. This publication and the
 *  contents hereof are subject to change without notice.
 *  @author Larry Pham(Email: larrypham.vn@gmail.com)
 *  @since 6/24/15 4:29 PM
 *
 *  The class <code>SQLTable</code> which used to generate the SQL quotes for creating the Table into Database(DB) or retrieving the
 *  data from the given table, update, delete, insert data from them.
 **/

public abstract class SQLTable {
    public static final String TAG = LogUtils.makeLogTag(SQLTable.class.getSimpleName());

    protected abstract Map<String, String> getColumnsMapping();

    protected abstract String getUniqueConstraint();

    public abstract String getName();

    protected static class BaseColumns {
        protected final static String _ID = "_id";
    }

    public String toCreateQuery() {
        String createQuery = "CREATE TABLE IF NOT EXISTS " + getName() +" (";
        Map<String,String> columns = getColumnsMapping();

        for (String column : columns.keySet()) {
            createQuery += column + " " + columns.get(column) + ", ";
        }
        createQuery += getUniqueConstraint() + ");";
        return createQuery;
    }

    public abstract void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion);

    public Cursor query(final SQLiteDatabase database, final Uri uri, final String[] projection, final String selection, final String[]
            selectionArgs, final String sortOrder) {
        return database.query(getName(), projection, selection, selectionArgs, null, null, sortOrder);
    }

    public long insert(final SQLiteDatabase database, final Uri uri, final ContentValues values) {
        LogUtils.LOGD(TAG, String.format("Inserting new record[%s] to a given table[%s]", values, getName()));
        return database.insert(getName(), null, values);
    }

    public long insert(final SQLiteDatabase database, ContentValues values) {
        return insert(database, null, values);
    }

    public int update(final SQLiteDatabase database, final Uri uri, final ContentValues values, final String selection, final String[]
            selectionArgs) {
        LogUtils.LOGD(TAG, String.format("Updating record[%s] to a given table[%s] with value[%s]", values.toString(), getName(),
                selection));
        return database.update(getName(), values, selection, selectionArgs);
    }

    public int update(final SQLiteDatabase database, final ContentValues values, final String selection, final String[] selectionArgs) {
        return update(database, null, values, selection, selectionArgs);
    }

    public int delete(final SQLiteDatabase database, final Uri uri, final String selection, final String[] selectionArgs) {
        LogUtils.LOGD(TAG, String.format("Deleting the record[%s] from table[%s]", selection, getName()));
        return database.delete(getName(), selection, selectionArgs);
    }

    public int delete(final SQLiteDatabase database, final String selection, final String[] selectionArgs) {
        return delete(database, null, selection, selectionArgs);
    }
}
