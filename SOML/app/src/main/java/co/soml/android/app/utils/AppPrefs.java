package co.soml.android.app.utils;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import co.soml.android.app.StoryApp;

public class AppPrefs {
	public enum PrefKey {
		USER_ID, LAST_ACTIVITY_STR, TAG_NAME, TAG_TYPE, MIXPANEL_EMAIL_ADDRESS
	}

	public static SharedPreferences prefs() {
		return PreferenceManager.getDefaultSharedPreferences(StoryApp.getContext());
	}

	public static String getString(PrefKey key) {
		return getString(key, "");
	}

	public static String getString(PrefKey key, String defaultValue) {
		return prefs().getString(key.name(), defaultValue);
	}

	public static void setString(PrefKey key, String value) {
		SharedPreferences.Editor editor = prefs().edit();
		if (TextUtils.isEmpty(value)) {
			editor.remove(key.name());
		} else {
			editor.putString(key.name(), value);
		}
		editor.apply();
	}

	public static long getLong(PrefKey key) {
		try {
			String value = getString(key);
			return Long.parseLong(value);
		} catch (NumberFormatException ex) {
			return 0;
		}
	}

	private static void setLong(PrefKey key, long value) {
		setString(key, Long.toString(value));
	}

	private static int getInt(PrefKey key) {
		try {
			String value = getString(key);
			return Integer.parseInt(value);
		} catch (NumberFormatException ex) {
			return 0;
		}
	}

	private static void setInt(PrefKey key, int value) {
		setString(key, Integer.toString(value));
	}

	private static boolean getBoolean(PrefKey key, boolean def) {
		String value = getString(key, Boolean.toString(def));
		return Boolean.parseBoolean(value);
	}

	private static void setBoolean(PrefKey key, boolean value) {
		setString(key, Boolean.toString(value));
	}

	private static void remove(PrefKey key) {
		prefs().edit().remove(key.name()).apply();
	}

	public static void reset() {
		SharedPreferences.Editor editor = prefs().edit();
		for (PrefKey key : PrefKey.values()) {
			editor.remove(key.name());
		}
		editor.apply();
	}

	public static long getCurrentUserId() {
		return getLong(PrefKey.USER_ID);
	}

	public static void setCurrentUserId(long userId) {
		if (userId == 0) {
			remove(PrefKey.USER_ID);
		} else {
			setLong(PrefKey.USER_ID, userId);
		}
	}


	public static void setLastActivityStr(String value) {
		setString(PrefKey.LAST_ACTIVITY_STR, value);
	}

	public static void resetLastActivityStr() {
		remove(PrefKey.LAST_ACTIVITY_STR);
	}

	public static String getMixPanelUserEmail() {
		return getString(PrefKey.MIXPANEL_EMAIL_ADDRESS, null);
	}

	public static void setMixPanelUserEmail(String email) {
		setString(PrefKey.MIXPANEL_EMAIL_ADDRESS, email);
	}
}
