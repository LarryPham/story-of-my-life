package co.soml.android.app.controllers;

import android.content.Intent;
import android.os.Message;
import android.widget.Toast;

import co.soml.android.app.R;
import co.soml.android.app.StoryApp;
import co.soml.android.app.activities.BaseActivity;
import co.soml.android.app.activities.InvalidateParam;
import co.soml.android.app.fragments.BaseFragment;
import co.soml.android.app.models.JsonUser;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.models.User;
import co.soml.android.app.services.UserResult;
import co.soml.android.app.services.tasks.Action;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.widgets.ProgressWheel;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.08.2015
 * <p>
 * The <code>SessionController</code> class used to test the facebook's authentication and other.
 */
public class SessionController extends BaseController {
    private StoryDataModel mDataModel;
    private StoryApp mApp;
    private ProgressWheel mProgressWheelDialog = null;

    private Toast mConnectivityToast;
    private Toast mCheckNetworkToast = null;

    public SessionController(BaseActivity activity, StoryDataModel dataModel) {
        super(activity, dataModel);
        this.mApp = (StoryApp) activity.getApplicationContext();
        this.mDataModel = dataModel;
    }

    public SessionController(BaseActivity activity, BaseFragment fragment, StoryDataModel dataModel) {
        super(activity, fragment, dataModel);
        this.mApp = (StoryApp) activity.getApplicationContext();
        this.mDataModel = dataModel;
    }

    public static class FabricAuthReqParam {
        public String mAuthToken;
        public String mAuthTokenSecret;
        public long mFabricId;

        public FabricAuthReqParam(long fabricId, String authToken, String authTokenSecret) {
            this.mFabricId = fabricId;
            this.mAuthToken = authToken;
            this.mAuthTokenSecret = authTokenSecret;
        }
    }

    public static class AuthenticateReqParam {
        public String mAuthenToken;
        public int mUserId;
        public String mFullName;
        public String mEmail;
        public String mPassword;
        public String mPasswordConfirmation;
        public String mUserDescription;

        public AuthenticateReqParam() {

        }

        // Use for sign-in with facebook's access-token or twitter's access-token
        public AuthenticateReqParam(String authenToken) {
            this.mAuthenToken = authenToken;
        }

        // Use for sign-in with email account.
        public AuthenticateReqParam(String email, String password) {
            this.mEmail = email;
            this.mPassword = password;
        }

        public AuthenticateReqParam(String fullName, String email, String password, String passwordConfirmation, String userDescription) {
            this.mFullName = fullName;
            this.mEmail = email;
            this.mPassword = password;
            this.mPasswordConfirmation = passwordConfirmation;
            this.mUserDescription = userDescription;
        }
    }

    @Override
    protected void handleMessage(Message msg) {
        switch (msg.what) {
            case ControllerMessage.REQUEST_TO_SERVER_SIGN_IN_WITH_FACEBOOK: {
                final Intent intent = obtainIntent(ControllerMessage.REQUEST_TO_SERVER_SIGN_IN_WITH_FACEBOOK);
                AuthenticateReqParam params = (AuthenticateReqParam) msg.obj;
                intent.putExtra(Action.SOCIAL_NETWORK_ACCESS_TOKEN, params.mAuthenToken);
                intent.setAction(Action.REQUEST_TO_SERVER_AUTHENTICATE_FACEBOOK);
                mActivity.startService(intent);
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_SIGN_IN_WITH_TWITTER: {
                final Intent intent = obtainIntent(ControllerMessage.REQUEST_TO_SERVER_SIGN_IN_WITH_TWITTER);
                FabricAuthReqParam params = (FabricAuthReqParam) msg.obj;
                intent.putExtra(Action.FABRIC_AUTH_TOKEN, params.mAuthToken);
                intent.putExtra(Action.FABRIC_AUTH_TOKEN_SECRET, params.mAuthTokenSecret);
                intent.putExtra(Action.FABRIC_ID, params.mFabricId);
                intent.setAction(Action.REQUEST_TO_SERVER_AUTHENTICATE_TWITTER);
                mActivity.startService(intent);
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_SIGN_IN_WITH_EMAIL_ACCOUNT: {
                final Intent intent = obtainIntent(ControllerMessage.REQUEST_TO_SERVER_SIGN_IN_WITH_EMAIL_ACCOUNT);
                AuthenticateReqParam params = (AuthenticateReqParam) msg.obj;
                intent.putExtra(Action.USER_EMAIL_ADDRESS, params.mEmail);
                intent.putExtra(Action.USER_PASSWORD, params.mPassword);
                intent.setAction(Action.REQUEST_TO_SERVER_AUTHENTICATE_EMAIL_ACCOUNT);
                mActivity.startService(intent);
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_SIGN_IN_GUEST: {
                final Intent intent = obtainIntent(msg.what);
                intent.setAction(Action.REQUEST_TO_SERVER_AUTHENTICATE_GUEST);
                mActivity.startService(intent);
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_ACCOUNT_REGISTER: {
                final Intent intent = obtainIntent(msg.what);
                AuthenticateReqParam params = (AuthenticateReqParam) msg.obj;
                intent.putExtra(Action.USER_FULL_NAME, params.mFullName);
                intent.putExtra(Action.USER_EMAIL_ADDRESS, params.mEmail);
                intent.putExtra(Action.USER_PASSWORD, params.mPassword);
                intent.putExtra(Action.USER_PASSWORD_CONFIRMATION, params.mPasswordConfirmation);
                intent.putExtra(Action.USER_DESCRIPTION, params.mUserDescription);
                intent.setAction(Action.REQUEST_TO_SERVER_ACCOUNT_REGISTER);
                mActivity.startService(intent);
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_ACCOUNT_INFO: {
                final Intent intent = obtainIntent(msg.what);
                AuthenticateReqParam params = (AuthenticateReqParam) msg.obj;
                intent.putExtra(Action.USER_ID, params.mUserId);
                intent.setAction(Action.REQUEST_TO_SERVER_ACCOUNT_NEW);
                mActivity.startService(intent);
                break;
            }
            // When the controller received the message with completed sign-in processing
            case ControllerMessage.REQUEST_TO_SERVER_SIGN_IN_WITH_FACEBOOK_COMPLETED: {
                if (checkOwner(msg)) {
                    final UserResult result = (UserResult) msg.obj;
                    final User user = result.getUser();
                    LogUtils.LOGD(TAG, String.format("REQUEST_TO_SERVER_SIGN_IN_WITH_FACEBOOK_COMPLETED - %s", user.getEmail()));
                    /**
                     * Invalidating the changes from an instance of <code>BaseFragment</code>. But into some instance of {@link co.soml.android.app
                     * .fragments.BaseFragment}, must be implemented the customise <code>InvalidateParam</code> for specified cases.
                     */
                    final InvalidateParam param = new InvalidateParam(msg.what, user);
                    (mFragment).invalidate(param);
                }
                break;
            }

            case ControllerMessage.REQUEST_TO_SERVER_SIGN_IN_WITH_TWITTER_COMPLETED: {
                if (checkOwner(msg)) {
                    final UserResult result = (UserResult) msg.obj;
                    final User user = result.getUser();
                    LogUtils.LOGD(TAG, String.format("REQUEST_TO_SERVER_SIGN_IN_WITH_TWITTER_COMPLETED - %s", user.getFullName()));
                    final InvalidateParam param = new InvalidateParam(msg.what, user);
                    (mFragment).invalidate(param);
                }
                break;
            }

            case ControllerMessage.REQUEST_TO_SERVER_SIGN_IN_WITH_EMAIL_ACCOUNT_COMPLETED: {
                if (checkOwner(msg)) {
                    final UserResult result = (UserResult) msg.obj;
                    final User user = result.getUser();
                    LogUtils.LOGD(TAG, String.format("REQUEST_TO_SERVER_SIGN_IN_WITH_EMAIL_ACCOUNT_COMPLETED - %s", user.getEmail()));
                    final InvalidateParam param = new InvalidateParam(msg.what, user);
                    (mFragment).invalidate(param);
                }
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_SIGN_IN_GUEST_COMPLETED: {
                if (checkOwner(msg)) {
                    final UserResult result = (UserResult) msg.obj;
                    final User anonymousUser = new User(result.getUser());
                    LogUtils.LOGD(TAG, String.format("REQUEST_TO_SERVER_SIGN_IN_GUEST_COMPLETED - %s", anonymousUser.getId()));
                    final InvalidateParam param = new InvalidateParam(msg.what, anonymousUser);
                }
                // TODO:
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_REGISTER_USER_COMPLETED: {
                if (checkOwner(msg)) {
                    final UserResult result = (UserResult) msg.obj;
                    final User registeredUser = new User(result.getUser());
                    LogUtils.LOGD(TAG, String.format("REQUEST_TO_SERVER_REGISTER_USER_COMPLETED -%s", registeredUser.getId()));
                    final InvalidateParam param = new InvalidateParam(msg.what, registeredUser);
                    (mFragment).invalidate(param);
                }
                break;
            }

            case ControllerMessage.REQUEST_ERROR_REASON_SERVER_CONNECT_TIMEOUT: {
                if (checkOwner(msg)) {
                    LogUtils.LOGD(TAG, "REQUEST_ERROR_REASON_SERVER_CONNECT_TIMEOUT");
                    Toast.makeText(mActivity, mActivity.getResources().getString(R.string.msg_error_connection_time_out), Toast.LENGTH_SHORT).show();
                }
                break;
            }
            // Failed to authenticate to server
            case ControllerMessage.REQUEST_ERROR_REASON_HTTP_UNAUTHORIZED: {
                if (checkOwner(msg)) {
                    LogUtils.LOGD(TAG, String.format("REQUEST_ERROR_REASON_HTTP_UNAUTHORIZED - %d", msg.what));
                    final InvalidateParam param = new InvalidateParam(msg.what);
                    (mFragment).invalidate(param);
                }
                break;
            }
            // Unknown host supports
            case ControllerMessage.REQUEST_ERROR_REASON_UNKNOWN_HOST: {
                if (checkOwner(msg)) {
                    LogUtils.LOGD(TAG, String.format("Unknown host or protocol was not support host -%d", msg.what));
                    final InvalidateParam param = new InvalidateParam(msg.what);
                    (mFragment).invalidate(param);
                }
                break;
            }
            // Disconnected networks
            case ControllerMessage.REQUEST_ERROR_REASON_HTTP_CONNECT_FAIL: {
                if (checkOwner(msg)) {
                    LogUtils.LOGD(TAG, String.format("Networks disconnected -%d", msg.what));
                    final InvalidateParam param = new InvalidateParam(msg.what);
                    (mFragment).invalidate(param);
                }
                break;
            }

        }
    }
}
