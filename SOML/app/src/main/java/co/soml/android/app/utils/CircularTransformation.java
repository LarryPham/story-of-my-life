package co.soml.android.app.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.squareup.picasso.Transformation;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since May.29.2015
 * <p>
 * The animated transformation class represent to animate UI Component as circular transformation which has been supported into Lollipop.
 */
public class CircularTransformation implements Transformation {
    private static final int STROKE_WIDTH = 6;

    /**
     * Transform the squared bitmap to circular bitmap with outlined stroke(white stroke)
     *
     * @param source The original squared bitmap
     * @return The Circular Bitmap with outlined stroke
     */
    @Override
    public Bitmap transform(Bitmap source) {
        // Setting the longed size of bitmap via width and height
        int size = Math.min(source.getWidth(), source.getHeight());

        // Determining the point for triggering the animation
        int x = (source.getWidth() - size) / 2;
        int y = (source.getHeight() - size) / 2;
        // Creating the square bitmap
        final Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
        if (squaredBitmap != source) {
            source.recycle();
        }

        final Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());
        final Canvas canvas = new Canvas(bitmap);
        final Paint avatarPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

        final BitmapShader shader = new BitmapShader(squaredBitmap, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
        avatarPaint.setShader(shader);
        // Round outline stroke paint for avatar icon
        final Paint outlinePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        outlinePaint.setColor(Color.WHITE);
        outlinePaint.setStyle(Paint.Style.STROKE);
        outlinePaint.setStrokeWidth(STROKE_WIDTH);

        float radius = size / 2f;
        // Drawing the circle rounded up the avatar icon
        canvas.drawCircle(radius, radius, radius, avatarPaint);
        canvas.drawCircle(radius, radius, radius - STROKE_WIDTH / 2, outlinePaint);
        squaredBitmap.recycle();
        return bitmap;
    }

    @Override
    public String key() {
        return "CircleTransformation";
    }
}
