package co.soml.android.app.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import co.soml.android.app.services.tasks.WeakHandler;
import co.soml.android.app.utils.LogUtils;

/**
 *  Copyright (C) 2015 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation
 *  are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied,
 *  reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior
 *  written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with respect to the contents, and
 *  assumes no responsibility for any errors that might appear in the software and documents. This publication and the
 *  contents hereof are subject to change without notice.
 *  @author Larry Pham(Email: larrypham.vn@gmail.com)
 *  @since 6/26/15 3:07 PM
 *
 *  This class is the default empty state view for most listviews/fragments
 *  It allows the ability to set a main text, a main secondary text. By default this container has some strings loaded, but other classes
 *  can call the apis to change the text.
 **/

public class LoadingEmptyContainer extends FrameLayout {

    public static final String TAG = LogUtils.makeLogTag(LoadingEmptyContainer.class.getSimpleName());

    private WeakHandler mHandler;
    private Runnable mShowLoadingRunnable;

    public LoadingEmptyContainer(Context context) {
        super(context);
    }

    public LoadingEmptyContainer(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    public LoadingEmptyContainer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void hideAll() {

    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        hideAll();
    }

    public NoResultContainer getNoResultContainer() {
        return null;
    }
}
