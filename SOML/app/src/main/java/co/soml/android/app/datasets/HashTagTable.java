package co.soml.android.app.datasets;

import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.provider.ContactsContract;
import android.text.TextUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

import co.soml.android.app.models.HashTag;
import co.soml.android.app.models.HashTagList;
import co.soml.android.app.utils.DateTimeUtils;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.utils.SqlUtils;

/**
 *  Copyright (C) 2015 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation
 *  are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied,
 *  reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior
 *  written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with respect to the contents, and
 *  assumes no responsibility for any errors that might appear in the software and documents. This publication and the
 *  contents hereof are subject to change without notice.
 *  @author Larry Pham(Email: larrypham.vn@gmail.com)
 *  @since 6/25/15 11:02 AM
 *
 *  Table HashTag stores the list of tags the user subscribed to or has by default
 **/

public class HashTagTable {

    public static final String TAG = LogUtils.makeLogTag(HashTagTable.class.getSimpleName());

    protected static void createTables(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE HashTags( TagName TEXT, CreatedAt DATETIME, UpdatedAt DATETIME, PRIMARY KEY (TagName); ");

    }

    public static void dropTables(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF NOT EXISTS HashTags");
    }

    public static boolean isEmpty() {
        return (SqlUtils.getRowCount(StoryDatabase.getReadableDB(), "HashTags") == 0);
    }

    public static void replaceTags(HashTagList tags) {
        if (tags == null || tags.size() == 0) {
            return;
        }
        SQLiteDatabase db = StoryDatabase.getWritableDB();
        db.beginTransaction();

        try {
            try {
                db.execSQL("DELETE FROM HashTags");
                addOrUpdateTags(tags);
                db.setTransactionSuccessful();
            } catch (SQLException ex) {
                LogUtils.LOGE(TAG, ex.toString());
            }
        } finally {
            db.endTransaction();
        }
    }

    public static void addOrUpdateTag(HashTag tag) {
        if (tag == null) {
            return;
        }

        HashTagList tags = new HashTagList();
        tags.add(tag);
        addOrUpdateTags(tags);
    }

    private static void addOrUpdateTags(HashTagList tagList) {
        if (tagList == null || tagList.size() == 0) {
            return;
        }

        SQLiteStatement stmt = null;
        try {
            stmt = StoryDatabase.getWritableDB()
                    .compileStatement("INSERT OR REPLACE INTO HashTags(TagName, CreatedAt, UpdatedAt) VALUE (?1,?2,?3)");
            for (HashTag tag : tagList) {
                stmt.bindString(1, tag.getHashTagName());
                stmt.bindString(2, tag.getCreatedAt().toString());
                stmt.bindString(3, tag.getUpdatedAt().toString());
                stmt.execute();
            }
        } finally {
            SqlUtils.closeStatement(stmt);
        }
    }

    public static boolean tagExists(HashTag tag) {
        if (tag == null) {
            return false;
        }

        String[] args = {tag.getHashTagName(), tag.getCreatedAt().toString(), tag.getUpdatedAt().toString()};
        return SqlUtils.boolForQuery(StoryDatabase.getReadableDB(), "SELECT 1 FROM HashTags WHERE TagName = ?1 AND CreatedAt = ?2 && " +
                "UpdatedAt = ?3", args);
    }

    public static HashTag getTagFromCursor(Cursor cursor) {
        if (cursor == null) {
            throw new IllegalArgumentException("Null Tag's Cursor");
        }
        try {
            String tagName = cursor.getString(cursor.getColumnIndex("TagName"));
            Date createdAt = DateFormat.getInstance().parse(cursor.getString(cursor.getColumnIndex("CreatedAt")));
            Date updatedAt = DateFormat.getInstance().parse(cursor.getString(cursor.getColumnIndex("UpdatedAt")));

            return new HashTag(tagName, createdAt, updatedAt);
        } catch (ParseException ex) {
            LogUtils.LOGE(TAG, "Throws new parseException " + ex.getMessage());
        }
        return null;
    }

    public static HashTag getTag(String tagName, Date createdAt, Date updatedAt) {
        if (TextUtils.isEmpty(tagName)) {
            return null;
        }

        String[] args = { tagName, createdAt.toString(), updatedAt.toString() };
        Cursor cursor = StoryDatabase.getReadableDB().rawQuery("SELECT * FROM HashTags WHERE TagName = ? AND CreatedAt = ? AND UpdatedAt "
                + "=? LIMIT 1", args);
        try {
            if (!cursor.moveToFirst()) {
                return null;
            }
            return getTagFromCursor(cursor);
        } finally {
            SqlUtils.closeCursor(cursor);
        }
    }

    static HashTagList getAllTags() {
        Cursor cursor = StoryDatabase.getReadableDB().rawQuery("SELECT * FROM HashTags ORDER BY TagName", null);
        try {
            HashTagList tagList = new HashTagList();
            if (cursor.moveToFirst()) {
                do {
                    tagList.add(getTagFromCursor(cursor));
                } while (cursor.moveToNext());
            }
            return tagList;
        } finally {
            SqlUtils.closeCursor(cursor);
        }
    }

    public static void deleteTag(HashTag tag) {
        if (tag == null) {
            return;
        }

        String[] args = { tag.getHashTagName(), tag.getCreatedAt().toString(), tag.getUpdatedAt().toString() };
        StoryDatabase.getWritableDB().delete("HashTags", "TagName = ? AND CreatedAt = ? AND UpdatedAt = ?", args);
    }

    public static String getTagLastUpdated(HashTag tag) {
        if (tag == null) {
            return "";
        }

        String[] args = { tag.getHashTagName(), tag.getUpdatedAt().toString()};
        return SqlUtils.stringForQuery(StoryDatabase.getReadableDB(), "SELECT UpdatedAt FROM HashTags WHERE TagName = ? AND UpdatedAt = " +
                "?", args);
    }

    public static void setLastTagLastUpdated(HashTag tag) {
        if (tag == null) {
            return;
        }

        String date = DateTimeUtils.convertFromJavaDateToIso8601(new Date());
        String sql = "UPDATE HashTags SET UpdatedAt = ?1 WHERE TagName = ?2";
        SQLiteStatement stmt = StoryDatabase.getWritableDB().compileStatement(sql);

        try {
            stmt.bindString(1, tag.getHashTagName());
            stmt.bindString(2, date);
            stmt.execute();
        } finally {
            SqlUtils.closeStatement(stmt);
        }
    }
}
