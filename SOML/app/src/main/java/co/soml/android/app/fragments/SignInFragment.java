package co.soml.android.app.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.Bind;
import butterknife.ButterKnife;
import co.soml.android.app.AppConstants;
import co.soml.android.app.R;
import co.soml.android.app.StoryApp;
import co.soml.android.app.activities.BaseActivity;
import co.soml.android.app.activities.InvalidateParam;
import co.soml.android.app.activities.StoryMainActivity;
import co.soml.android.app.controllers.ControllerMessage;
import co.soml.android.app.controllers.SessionController;
import co.soml.android.app.models.JsonUser;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.models.StorySettings;
import co.soml.android.app.models.User;
import co.soml.android.app.utils.CommonUtil;
import co.soml.android.app.utils.EditTextUtils;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.widgets.ProgressWheel;
import co.soml.android.app.widgets.StoryTextView;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.23.2015
 */
public class SignInFragment extends BaseFragment implements EmailAuthPolicy, View.OnClickListener, TextWatcher {
    public static final String TAG = LogUtils.makeLogTag(SignInFragment.class);

    public static final String FRAGMENT_PARAMS = "SessionsParams";
    public static final String FRAGMENT_PAGE_PARAMS = "SessionPage";

    private SessionController mController;
    private StoryDataModel mDataModel;
    private BaseActivity mActivity;
    private StoryApp mStoryApp;
    private StorySettings mStorySettings;
    private boolean mEmailAutoCorrected;
    private String mUserAddress;
    private String mUserPassword;

    @Bind(R.id.signin_email_edittext)
    EditText mEmailEditText;
    @Bind(R.id.signin_password_edittext)
    EditText mPasswordEditText;
    @Bind(R.id.signin_progress_wheel)
    ProgressWheel mProgressWheel;
    @Bind(R.id.signing_progress_bar)
    RelativeLayout mProgressBarSignin;
    @Bind(R.id.signing_progress_text)
    StoryTextView mProgressTextSignin;
    @Bind(R.id.signin_email_button)
    StoryTextView mSignInButton;
    @Bind(R.id.signup_new_account)
    StoryTextView mSignupButton;

    public enum ErrorType {
        EMAIL_ADDRESS, PASSWORD, UNDEFINED
    }

    /**
     * Listener for signing in account action.
     */
    private final View.OnClickListener mSigninClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View signinButton) {
            signIn();
        }
    };

    private final TextView.OnEditorActionListener mEditorAction = new TextView.OnEditorActionListener() {

        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (mPasswordEditText == v) {
                return onDoneEvent(actionId, event);
            }
            return onDoneEvent(actionId, event);
        }
    };

    private final View.OnFocusChangeListener mEmailFocusListener = new View.OnFocusChangeListener() {

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus) {
                //autoCorrectUserName();
            }
        }
    };

    public SignInFragment() {
    }

    public static SignInFragment newInstance() {
        LogUtils.LOGD(TAG, "NewInstance(): ");
        SignInFragment signInFragment = new SignInFragment();
        Bundle args = new Bundle();
        args.putString(FRAGMENT_PAGE_PARAMS, AppConstants.SIGNIN_FRAGMENT_KEY);
        signInFragment.setArguments(args);
        return signInFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (BaseActivity) getActivity();
        mStoryApp = (StoryApp) mActivity.getApplicationContext();
        mDataModel = mStoryApp.getAppDataModel();
        mController = new SessionController(mActivity, this, mDataModel);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = (ViewGroup) inflater.inflate(R.layout.layout_signin_account, container, false);
        ButterKnife.bind(this, mRootView);
        mSignInButton.setOnClickListener(this);
        mSignupButton.setOnClickListener(this);
        mEmailEditText.setOnFocusChangeListener(mEmailFocusListener);
        mEmailEditText.addTextChangedListener(this);
        mPasswordEditText.addTextChangedListener(this);
        mPasswordEditText.setOnEditorActionListener(mEditorAction);
        return mRootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onClick(View view) {
        String fn = "handleSignInButton()";
        switch (view.getId()) {
            case R.id.signin_email_button: {
                if (mProgressBarSignin.getVisibility() == View.VISIBLE) return;
                signIn();
                break;
            }
            case R.id.signup_new_account: {
                // Calling signup fragment at here.
                LogUtils.LOGD(TAG, fn + "SignupEmail");
                final String fragmentKey = "co.som.android.app.fragments.NewUserFragment";
                final Bundle signupBundle = new Bundle();
                mActivity.switchContent(NewUserFragment.newInstance(), fragmentKey, signupBundle, false);
                break;
            }
        }
    }

    public void signIn() {
        if (!isUserDataValid()) {
            return;
        }

        if (!CommonUtil.isNetworkOnline(mActivity)) {
            return;
        }

        mUserAddress = EditTextUtils.getText(mEmailEditText).trim();
        mUserPassword = EditTextUtils.getText(mPasswordEditText).trim();
        SessionController.AuthenticateReqParam requestParam = new SessionController.AuthenticateReqParam(mUserAddress, mUserPassword);
        mController.sendMessage(ControllerMessage.REQUEST_TO_SERVER_SIGN_IN_WITH_EMAIL_ACCOUNT, requestParam);
        startProgress(getString(R.string.connecting_to_server));
    }

    @Override
    public void startProgress(String message) {
        mProgressBarSignin.setVisibility(View.VISIBLE);
        mSignInButton.setVisibility(View.GONE);
        mProgressWheel.spin();
        mEmailEditText.setEnabled(false);
        mProgressTextSignin.setVisibility(View.VISIBLE);
        mProgressTextSignin.setText(message);
        mProgressBarSignin.setEnabled(true);
        mPasswordEditText.setEnabled(false);
        mSignInButton.setEnabled(false);
        mSignupButton.setEnabled(false);
    }

    @Override
    public void updateProgress(String message) {

    }

    @Override
    public void endProgress() {
        stopProgressWheel();
        mProgressBarSignin.setVisibility(View.GONE);
        mProgressTextSignin.setVisibility(View.GONE);
        mEmailEditText.setEnabled(true);
        mPasswordEditText.setEnabled(true);
        mSignupButton.setEnabled(true);
        mSignInButton.setVisibility(View.VISIBLE);
        mSignInButton.setEnabled(true);
    }

    protected void stopProgressWheel() {
        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (mProgressWheel != null && mProgressWheel.isSpinning()) {
                    mProgressWheel.stopSpinning();
                }
            }
        };
        handler.postDelayed(runnable, 3000);
    }

    private void autoCorrectUserName() {
        if (mEmailAutoCorrected) {
            return;
        }

        final String email = EditTextUtils.getText(mEmailEditText).trim();
        final Pattern emailRegExPattern = Patterns.EMAIL_ADDRESS;
        Matcher matcher = emailRegExPattern.matcher(email);
        if (!matcher.find()) {
            return;
        }
    }

    @Override
    public void onDoneAction() {
        signIn();
    }

    public boolean onDoneEvent(int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE || event != null && (event.getAction() == KeyEvent.ACTION_DOWN
                && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
            if (!isUserDataValid()) {
                return true;
            }

            // hide keyboard before calling the done action
            InputMethodManager manager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            View view = getActivity().getCurrentFocus();
            if (view != null) {
                manager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }

            // call child action
            onDoneAction();
            return true;
        }
        return false;
    }

    @Override
    public boolean isUserDataValid() {
        final String email = EditTextUtils.getText(mEmailEditText).trim();
        final String password = EditTextUtils.getText(mPasswordEditText).trim();
        boolean resultValue = true;
        if (!isEmailValid(email)) {
            mEmailEditText.setError(getString(R.string.email_required_field));
            mEmailEditText.requestFocus();
            resultValue = false;
        }

        if (TextUtils.isEmpty(email)) {
            mEmailEditText.setError(getString(R.string.required_field));
            mEmailEditText.requestFocus();
            resultValue = false;
        }

        if (TextUtils.isEmpty(password)) {
            mPasswordEditText.setError(getString(R.string.password_required_field));
            mPasswordEditText.requestFocus();
            resultValue = false;
        }
        return resultValue;
    }

    public boolean isEmailValid(String emailAddress) {
        if (TextUtils.isEmpty(emailAddress)) {
            return false;
        }
        return Patterns.EMAIL_ADDRESS.matcher(emailAddress).matches();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (isFieldsFilled()) {
            mSignInButton.setEnabled(true);
        } else {
            mSignInButton.setEnabled(false);
        }

        mPasswordEditText.setError(null);
        mEmailEditText.setError(null);
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    protected void handleInvalidUserEmailOrPassword(int messageId) {
        showEmailAccountError(messageId);
        showPasswordError(messageId);
        endProgress();
    }

    private void showPasswordError(int messageId) {
        mPasswordEditText.setError(getString(messageId));
        mPasswordEditText.requestFocus();
    }

    private void showEmailAccountError(int messageId) {
        mEmailEditText.setError(getString(messageId));
        mEmailEditText.requestFocus();
    }

    protected void signInError(int messageId, String signinError) {

    }

    private boolean isFieldsFilled() {
        return EditTextUtils.getText(mEmailEditText).trim().length() > 0
                && EditTextUtils.getText(mPasswordEditText).trim().length() > 0;
    }

    @Override
    public void invalidate() {

    }

    @Override
    public void invalidate(Object... params) {
        InvalidateParam param = (InvalidateParam) params[0];
        if (param.getMessage() == ControllerMessage.REQUEST_TO_SERVER_SIGN_IN_WITH_EMAIL_ACCOUNT_COMPLETED) {
            User result = (User) param.getObj();
            if (result != null) {
                LogUtils.LOGD(TAG, String.format("Saving the received user[%s]", result.getEmail()));
                StoryApp.getInstance().setCurrentUser(result);

                // Navigating to the Collections Of Stories activity.
                final Intent mainContent = new Intent(mActivity, StoryMainActivity.class);
                startActivity(mainContent);
                mActivity.finish();
            }
        } else if (param.getMessage() == ControllerMessage.REQUEST_ERROR_REASON_HTTP_UNAUTHORIZED) {
            endProgress();
            int errorCode = param.getMessage();
            LogUtils.LOGD(TAG, String.format("raising issues to login to soml server[%d]", errorCode));
            handleInvalidUserEmailOrPassword(R.string.invalidated_email_or_password);
            if (mStoryApp.getCurrentUser() == null) {
                return;
            }
        } else if (param.getMessage() == ControllerMessage.REQUEST_ERROR_REASON_HTTP_CONNECT_FAIL) {
            endProgress();
            int errorCode = param.getMessage();
            LogUtils.LOGD(TAG, String.format("raising issues to login to somle server, connecting failed[%d]", errorCode));
            final Toast errorToast = Toast.makeText(getActivity(), getString(R.string.msg_error_connection_failed), Toast.LENGTH_LONG);
            errorToast.show();
            if (mStoryApp.getCurrentUser() == null) {
                return;
            }
        } else if (param.getMessage() == ControllerMessage.REQUEST_ERROR_REASON_UNKNOWN_HOST) {
            endProgress();
            int errorCode = param.getMessage();
            LogUtils.LOGD(TAG, String.format("raising issues to login to soml server, unknown host[%d]", errorCode));
            final Toast errorToast = Toast.makeText(getActivity(), getString(R.string.msg_error_unknown_host), Toast.LENGTH_LONG);
            errorToast.show();
            if (mStoryApp.getCurrentUser() == null) {
                return;
            }
        }
        invalidate();
    }

}
