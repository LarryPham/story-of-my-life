package co.soml.android.app.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.inputmethod.EditorInfo;
import android.widget.MultiAutoCompleteTextView;

import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.utils.SuggestionTokenizer;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since May.20.2015
 */
public class SuggestionAutoCompleteText extends MultiAutoCompleteTextView {
    public static final String TAG = LogUtils.makeLogTag(SuggestionAutoCompleteText.class.getSimpleName());


    public SuggestionAutoCompleteText(Context context) {
        super(context);
        init(context, null);
    }

    public SuggestionAutoCompleteText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public SuggestionAutoCompleteText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        LogUtils.LOGD(TAG, "Loading customized typeface");
        TypefaceCache.setCustomTypeface(context, this, attrs);
        setTokenizer(new SuggestionTokenizer());
        setThreshold(1);
        LogUtils.LOGD(TAG, "setRawInputType: TYPE_TEXT_FLAG_AUTO_COMPLETE");
        // When TYPE_TEXT_FLAG_AUTO_COMPLETE is set, auto-correction is disabled.
        setRawInputType(getInputType() & ~EditorInfo.TYPE_TEXT_FLAG_AUTO_COMPLETE);
    }



    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();


    }
}
