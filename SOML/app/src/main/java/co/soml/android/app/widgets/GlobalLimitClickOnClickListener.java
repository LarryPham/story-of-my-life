/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/6/15 2:12 PM.
 **/

package co.soml.android.app.widgets;

import android.view.View;

public class GlobalLimitClickOnClickListener implements View.OnClickListener {
    private static long mLastClick;
    private View.OnClickListener mListener;
    private long mIntervalClick;

    public GlobalLimitClickOnClickListener(View.OnClickListener listener, long intervalClick) {
        this.mIntervalClick = intervalClick;
        this.mListener = listener;
    }

    @Override
    public void onClick(View v) {
        if (System.currentTimeMillis() > mLastClick && System.currentTimeMillis() - mLastClick <= mIntervalClick) {
            return;
        }
        mListener.onClick(v);
        mLastClick = System.currentTimeMillis();
    }
}
