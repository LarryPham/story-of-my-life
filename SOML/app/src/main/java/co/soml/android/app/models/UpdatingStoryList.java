package co.soml.android.app.models;

import java.util.List;

import co.soml.android.app.utils.Lists;

public class UpdatingStoryList extends BaseModel {
	private List<String> mWaitingList = Lists.newArrayList();
	private List<String> mUpdatingList = Lists.newArrayList();

	private final int enabledUpdateNum = 5;
	private int updateAll = 0;

	public UpdatingStoryList() {

	}

	public void addWaitingList(String url) {
		mWaitingList.add(url);
	}

	public void setWaitingList(List<String> waitingList) {
		this.mWaitingList = waitingList;
	}

	public int getWaitingCount() {
		return mWaitingList.size();
	}

	public String waitToUpdate() {
		String updateItem = mWaitingList.get(0);
		mUpdatingList.add(updateItem);
		mWaitingList.remove(updateItem);
		return updateItem;
	}

	public void removeUpdatingItem(String url) {
		mUpdatingList.remove(url);
	}

	public int getUpdatingCount() {
		return mUpdatingList.size();
	}

	public int getUpdateAll() {
		return updateAll;
	}

	public void setUpdateAll(int state) {
		updateAll = state;
	}

	public boolean checkExistItemInUpdate(String url) {
		return mWaitingList.contains(url) | mUpdatingList.contains(url);
	}

	public boolean checkWaitingItem(String url) {
		return mWaitingList.contains(url);
	}

	public boolean checkUpdatingItem(String url) {
		return mUpdatingList.contains(url);
	}

	public int getEnabledUpdateNum() {
		return enabledUpdateNum;
	}

	public void clearUpdateList() {
		this.mWaitingList.clear();
		this.mUpdatingList.clear();
	}
}
