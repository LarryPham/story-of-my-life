package co.soml.android.app.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its
 * documentation are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable
 * form without the prior written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with
 * respect to the contents, and assumes no responsibility for any errors that might appear in the software and
 * documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.03.2015
 */
public class DownloadJob implements Parcelable {

    public static final int INVALID_ID = -1;
    private static int IDGEN = 1;
    private int mId;
    private String mFromURL;
    private String mToURL;
    private int mProgressValue = 0;
    private DownloadState mState = DownloadState.NONE;
    private long mDownloadSize = 0;
    private long mTotalDownloadSize = 0;

    public static enum DownloadState {
        NONE, WAITING, DOWNLOADING, CANCELLING, PAUSE
    }

    public DownloadJob(String fromURL, String toURL) {
        mId = IDGEN++;
        this.mFromURL = fromURL;
        this.mToURL = toURL;
        this.mState = DownloadState.WAITING;
    }

    public DownloadJob(Parcel inSource) {
        readFromParcel(inSource);
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getFromURL() {
        return mFromURL;
    }

    public void setFromURL(String fromURL) {
        mFromURL = fromURL;
    }

    public String getToURL() {
        return mToURL;
    }

    public void setToURL(String toURL) {
        mToURL = toURL;
    }

    public int getProgressValue() {
        return mProgressValue;
    }

    public void setProgressValue(int progressValue) {
        mProgressValue = progressValue;
    }

    public DownloadState getState() {
        return mState;
    }

    public void setState(DownloadState state) {
        mState = state;
    }

    public long getDownloadSize() {
        return mDownloadSize;
    }

    public void setDownloadSize(long downloadSize) {
        mDownloadSize = downloadSize;
    }

    public long getTotalDownloadSize() {
        return mTotalDownloadSize;
    }

    public void setTotalDownloadSize(long totalDownloadSize) {
        mTotalDownloadSize = totalDownloadSize;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mId);
        dest.writeString(mFromURL);
        dest.writeString(mToURL);
        dest.writeLong(mDownloadSize);
    }

    public void readFromParcel(Parcel inSource) {
        this.mId = inSource.readInt();
        this.mFromURL = inSource.readString();
        this.mToURL = inSource.readString();
        this.mDownloadSize = inSource.readLong();
    }

    public static final Creator<DownloadJob> CREATOR = new Creator<DownloadJob>() {
        @Override
        public DownloadJob createFromParcel(Parcel source) {
            return new DownloadJob(source);
        }

        @Override
        public DownloadJob[] newArray(int size) {
            return new DownloadJob[size];
        }
    };
}
