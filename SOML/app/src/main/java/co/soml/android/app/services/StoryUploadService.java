package co.soml.android.app.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.IntentCompat;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Random;

import co.soml.android.app.AppConstants;
import co.soml.android.app.R;
import co.soml.android.app.StoryApp;
import co.soml.android.app.activities.StoryMainActivity;
import co.soml.android.app.controllers.ControllerMessage;
import co.soml.android.app.models.*;
import co.soml.android.app.models.Error;
import co.soml.android.app.services.tasks.Action;
import co.soml.android.app.services.tasks.ServerSubscribeTask;
import co.soml.android.app.utils.LogUtils;
import retrofit.http.Body;
import retrofit.mime.TypedFile;

public class StoryUploadService extends Service {
	private static final String TAG = LogUtils.makeLogTag(StoryUploadService.class.getSimpleName());
	private Context mContext;
	private Story mCurrentLoadingStory = null;
	private static ArrayList<Story> mStories  = new ArrayList<>();
	private UploadStoryTask mCurrentTask = null;
    private StoryApp mApp;
    private Handler mMainHandler;

	public static void addStoryToUpload(Story currentStory) {
		synchronized (mStories) {
			mStories.add(currentStory);
			currentStory.setUploading(true);
            // Updating story to db
		}
	}

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this.getApplicationContext();
        mApp = (StoryApp) this.getApplicationContext();
        mMainHandler = mApp.getAppMainHandler();
        mApp.setUploadService(this);
        registerReceiver(mReceiver, new IntentFilter(Action.FINISH_APPLICATION));
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            LogUtils.LOGD(TAG, String.format("OnReceive: Action[%s]", action));
            if (action.equals(Action.FINISH_APPLICATION)) {
                LogUtils.LOGD(TAG, String.format("BroadcastReceiver() - Action.FINISH_APPLICATION"));
                StoryUploadService.this.stopSelf();
            }
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mCurrentTask != null) {
            LogUtils.LOGD(TAG, "Cancelling current UploadTask");
            mCurrentTask.cancel(true);
        }
    }

    @Override
	public IBinder onBind(Intent intent) {
        LogUtils.LOGD(TAG, "onBind(): ");
		return mIBinder;
	}

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        synchronized (mStories) {
            if (mStories.size() == 0 || mContext == null) {
                stopSelf();
                return START_NOT_STICKY;
            }
        }
        handleCommands(intent);
        uploadNextStory();
        return START_STICKY;
    }

    public void handleCommands(Intent intent) {
        String fn = "handleCommands(): ";
        if (intent == null) {
            return;
        }

        String action = intent.getAction();
        LogUtils.LOGD(TAG, fn + String.format("%s", action));
        if (action == null) {
            return;
        }

        switch (action) {
            case Action.ACTION_EXIT: {
                LogUtils.LOGD(TAG, fn + String.format("%s - Stopping Service", action));
                StoryUploadService.this.stopSelf();
                break;
            }
            case Action.REQUEST_TO_SERVER_STORY_POST: {
                LogUtils.LOGD(TAG, fn + String.format("UPLOADING STORY TO SERVER"));
                break;
            }
            case Action.REQUEST_TO_SERVER_UPLOAD_PHOTO: {
                LogUtils.LOGD(TAG, fn + String.format("UPLOADING PHOTO TO SERVER's STORY "));
                break;
            }
        }
    }
    private void uploadNextStory() {

    }

    protected void uploadStory(final Intent intent) {
        String fn = "uploadStory(): ";
        String action = intent.getAction();

        final int requestOwner = intent.getIntExtra(Action.REQUEST_OWNER, 0);
        final int requestMsg = intent.getIntExtra(Action.REQUEST_MSG, 0);
        final String storyName = intent.getStringExtra(Action.STORY_NAME);
        final int storyInterval = intent.getIntExtra(Action.STORY_INTERVAL, 0);
        final String storyDesc = intent.getStringExtra(Action.STORY_DESCRIPTION);
        final float storyLat = intent.getFloatExtra(Action.STORY_LATITUDE, 0);
        final float storyLng = intent.getFloatExtra(Action.STORY_LONGITUDE, 0);
        final String storyLoc = intent.getStringExtra(Action.STORY_LOCATION);

        LogUtils.LOGD(TAG, fn + String.format("RequestOwner: %d", requestOwner));
        LogUtils.LOGD(TAG, fn + String.format("RequestMsg: %d", requestMsg));
        if (requestMsg == 0) {
            final co.soml.android.app.models.Error error = new Error(ControllerMessage.REQUEST_ERROR_REASON_PARAMS_MISSING);
            // MainHandler will sending the message which notify other components that having exist error
            // when executing this request to server for posting the authentication's token
            sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, requestOwner, requestMsg, error);
            Log.e(TAG, fn + "Params are missing.");
            return;
        }

        ServerSubscribeTask task = new ServerSubscribeTask(action, mMainHandler);
        task.setSubscribeType(ServerSubscribeTask.SubscribeType.STORY_POST);
        task.setRequestOwner(requestOwner);
        task.setRequestMsg(requestMsg);
        task.setStoryName(storyName);
        task.setStoryInterval(storyInterval);
        task.setStoryDescription(storyDesc);
        task.setStoryLatitude(storyLat);
        task.setStoryLongtitude(storyLng);
        task.setStoryLocation(storyLoc);
        SerializedTaskManager.resolve("SERVER_SUBSCRIBER_TASK").add(task, SerializedTaskManager.STM_SERVER_REQUEST_TO_SUBSCRIBE);
    }

    protected void uploadPhoto(final Intent intent) {

    }
    private final IBinder mIBinder = new LocalUploadBinder();

    public class LocalUploadBinder extends Binder {
        public StoryUploadService getService() {
            return StoryUploadService.this;
        }
    }

    private void postUpLoaded() {
        synchronized (mStories) {
            mCurrentTask = null;
            mCurrentLoadingStory = null;
        }
        uploadNextStory();
    }

    private class UploadStoryTask extends AsyncTask<Story, Integer, Boolean> {
        private Story mStory;
        private StoryUploadNotifier mStoryUploadNotifier;

        private String mErrorMessage = "";
        private boolean mIsMediaError = false;
        private boolean mErrorUnavailableImagePress = false;
        private int mFeatureImageID = -1;

        @Override
        protected void onPostExecute(Boolean storyUploadedSuccessfully) {
            mStory.setUploading(false);

            if (storyUploadedSuccessfully) {

                mStoryUploadNotifier.cancelNotification();
            }
            super.onPostExecute(storyUploadedSuccessfully);
        }

        @Override
		protected Boolean doInBackground(Story... params) {
			return null;
		}
	}


    private File createTempUploadFile(String fileExtension) throws IOException {
        return File.createTempFile("tp-", fileExtension, mContext.getCacheDir());
    }

    private class StoryUploadNotifier {
        private final NotificationManager mNotificationManager;
        private final NotificationCompat.Builder mNotificationBuilder;

        private final long mNotificationId;
        private long mNotificationErrorId = 0;
        private int mTotalMediaItems;
        private int mCurrentMediaItem;
        private float mItemProgressSize;

        public StoryUploadNotifier(Story story) {
            mNotificationManager = (NotificationManager) SystemServiceFactory.get(mContext, Context.NOTIFICATION_SERVICE);
            mNotificationBuilder = new NotificationCompat.Builder(getApplicationContext());
            mNotificationBuilder.setSmallIcon(android.R.drawable.stat_sys_upload);

            Intent notificationIntent = new Intent(mContext, StoryMainActivity.class);
            notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat
                    .FLAG_ACTIVITY_CLEAR_TASK);
            notificationIntent.setAction(Intent.ACTION_MAIN);
            notificationIntent.addCategory(Intent.CATEGORY_LAUNCHER);
            //notificationIntent.setData(Uri.parse("custom://TimeUpNotificationIntent"  ))
            notificationIntent.putExtra(AppConstants.SCOPE_NAME, "LastTest");
            PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            mNotificationBuilder.setContentIntent(pendingIntent);

            mNotificationId = (new Random()).nextInt() + story.getId();
            startForeground((int)mNotificationId, mNotificationBuilder.build());
        }

        public void updateNotificationManager(String title, String message) {
            if (title != null) {
                mNotificationBuilder.setContentTitle(title);
            }
            if (message != null) {
                mNotificationBuilder.setContentText(message);
            }
            mNotificationManager.notify((int) mNotificationId, mNotificationBuilder.build());
        }

        public void updateNotificationIcon(Bitmap icon) {
            if (icon != null) {
                mNotificationBuilder.setLargeIcon(icon);
            }
            mNotificationManager.notify((int)mNotificationId, mNotificationBuilder.build());
        }

        public void cancelNotification() {
            mNotificationManager.cancel((int)mNotificationId);
        }

        public void updateNotificationWithError(String errorMessage, boolean isMediaError) {
            LogUtils.LOGD(TAG, "UpdateNotificationWithError: " + errorMessage);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getApplicationContext());
            Intent notificationIntent = new Intent(mContext, StoryMainActivity.class);
            notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK
                    | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);

            notificationIntent.setAction(Intent.ACTION_MAIN);
            notificationIntent.addCategory(Intent.CATEGORY_LAUNCHER);
            //notificationIntent.putExtra()
            notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            String errorText = getString(R.string.upload_failed_message);
            notificationBuilder.setSmallIcon(android.R.drawable.stat_notify_error);
            notificationBuilder.setContentTitle(errorText);
            notificationBuilder.setContentIntent(pendingIntent);
            if (mNotificationErrorId == 0) {
                mNotificationErrorId = mNotificationId + (new Random()).nextInt();
            }
            mNotificationManager.notify((int)mNotificationErrorId, notificationBuilder.build());
        }

        public void updateNotificationProgress(float progress) {
            if (mTotalMediaItems == 0) {
                return;
            }

            double currentChunkProgress = (mItemProgressSize * progress) / 100;
            if (mCurrentMediaItem > 1) {
                currentChunkProgress += mItemProgressSize * (mCurrentMediaItem - 1);
            }

            mNotificationBuilder.setProgress(100, (int) Math.ceil(currentChunkProgress), false);
            mNotificationManager.notify((int)mNotificationId, mNotificationBuilder.build());
        }

        public void setTotalMediaItems(int totalMediaItems) {
            if (totalMediaItems <= 0) {
                totalMediaItems = 1;
            }
            mTotalMediaItems = totalMediaItems;
            mItemProgressSize = 100.0f / mTotalMediaItems;
        }

        public void setCurrentMediaItem(int currenItem) {
            mCurrentMediaItem = currenItem;
            mNotificationBuilder.setContentText(String.format("Uploading %1$d of %2$d", mCurrentMediaItem, mTotalMediaItems));
        }
    }

    private void sendMessage(int code, int requestOwner, int requestMsg, Object obj) {
        String fn = "sendMessage(): ";
        final Message msg = new Message();
        LogUtils.LOGD(TAG, fn + "code     = " + ControllerMessage.toString(code) + "(" + code + ")");
        LogUtils.LOGD(TAG, fn + "requestOwner = " + requestOwner);
        LogUtils.LOGD(TAG, fn + "requestMsg = " + ControllerMessage.toString(requestMsg) + "(" + requestMsg + ")");
        // Checking if obj will be an instance of Error object. Creating the error and sending back the message
        // contains errorCode for other comps can understand about it.
        if (obj instanceof Error) {
            final Error error = (Error) obj;
            int errorCode = error.getErrorCode();
            LogUtils.LOGD(TAG, fn + "errorCode = " + ControllerMessage.toString(errorCode) + "(" + errorCode + ")");
        }

        msg.what = code;
        msg.arg1 = requestOwner;
        msg.arg2 = requestMsg;
        msg.obj = obj;
        // the MainHandler which has been assigned into the Service's attributes will be reused to sending the constructed message
        mMainHandler.sendMessage(msg);
    }

    public interface UploadProgressListener {
        void transferred(long number);
    }

    public class CountingTypedFile extends TypedFile {
        static final int BUFFER_SIZE = 4096;
        private final UploadProgressListener mListener;

        public CountingTypedFile(String mimeType, File file, UploadProgressListener listener) {
            super(mimeType, file);
            this.mListener = listener;
        }

        @Override
        public void writeTo(OutputStream out) throws IOException {
            byte[] buffer = new byte[BUFFER_SIZE];
            FileInputStream inputStream = new FileInputStream(super.file());
            long total =0;
            try {
                int read;
                while ((read = inputStream.read(buffer)) != -1) {
                    total += read;
                    this.mListener.transferred(total);
                    out.write(buffer, 0, read);
                }
            } finally {
                inputStream.close();
            }
        }
    }
}
