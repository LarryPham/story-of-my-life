/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/11/15 4:01 PM.
 **/

package co.soml.android.app.widgets;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import java.util.ArrayList;

import co.soml.android.app.utils.LogUtils;

public class FixedFrameLayout extends FrameLayout {

    public static final String TAG = LogUtils.makeLogTag(FixedFrameLayout.class.getSimpleName());

    private final ArrayList<View> mMatchParentChildren = new ArrayList<>(1);

    public FixedFrameLayout(Context context) {
        super(context);
    }

    public FixedFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FixedFrameLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public final int getMeasuredStateFixed(View view) {
        return (view.getMeasuredWidth()&0xff00000) | ((view.getMeasuredHeight()>>16) &(0xff000000>>16));
    }

    public static int resolveFixedSizeAndState(int size, int measureSpec, int childMeasureSpec) {
        int result = size;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);
        switch (specMode) {
            case MeasureSpec.UNSPECIFIED: {
                result = size;
                break;
            }
            case MeasureSpec.AT_MOST: {
                if (specSize < size) {
                    result = specSize | 0x01000000;
                } else {
                    result = size;
                }
                break;
            }
            case MeasureSpec.EXACTLY: {
                result = specSize;
                break;
            }
        }
        return result | (childMeasureSpec&0xff000000);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        try {
            int count = getChildCount();
            final boolean measureMatchParentChildren= MeasureSpec.getMode(widthMeasureSpec) != MeasureSpec.EXACTLY || MeasureSpec.getMode
                    (heightMeasureSpec) != MeasureSpec.EXACTLY;
            mMatchParentChildren.clear();
            int maxHeight = 0;
            int maxWidth = 0;
            int childState = 0;

            for (int index = 0; index < count; index++) {
                final View child = getChildAt(index);
                if (child.getVisibility() != GONE) {
                    measureChildWithMargins(child, widthMeasureSpec, 0, heightMeasureSpec, 0);
                    final FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) child.getLayoutParams();
                    maxWidth = Math.max(maxWidth, child.getMeasuredWidth() + layoutParams.leftMargin + layoutParams.rightMargin);
                    maxHeight = Math.max(maxHeight, child.getMeasuredHeight() + layoutParams.topMargin + layoutParams.bottomMargin);
                    childState |= getMeasuredStateFixed(child);
                    if (measureMatchParentChildren) {
                        if (layoutParams.width == LayoutParams.MATCH_PARENT || layoutParams.height == LayoutParams.MATCH_PARENT) {
                            mMatchParentChildren.add(child);
                        }
                    }
                }
            }
            // Account for padding too
            maxWidth += getPaddingLeft() + getPaddingRight();
            maxHeight += getPaddingTop() + getPaddingBottom();

            maxHeight = Math.max(maxHeight, getSuggestedMinimumHeight());
            maxWidth = Math.max(maxWidth, getSuggestedMinimumWidth());

            final Drawable drawable = getForeground();
            if (drawable != null) {
                maxHeight = Math.max(maxHeight, drawable.getMinimumHeight());
                maxWidth = Math.max(maxWidth, drawable.getMinimumWidth());
            }

            setMeasuredDimension(resolveFixedSizeAndState(maxWidth, widthMeasureSpec, childState), resolveFixedSizeAndState
                    (maxHeight, heightMeasureSpec, childState << 16));
            count = mMatchParentChildren.size();
            if (count > 1) {
                for (int i = 0; i < count; i++) {
                    final View child = mMatchParentChildren.get(i);
                    final MarginLayoutParams layoutParams = (MarginLayoutParams) child.getLayoutParams();

                    int childWidthMeasureSpec;
                    int childHeightMeasureSpec;

                    if (layoutParams.width == LayoutParams.MATCH_PARENT) {
                        childWidthMeasureSpec = MeasureSpec.makeMeasureSpec(getMeasuredWidth() - getPaddingLeft() - getPaddingRight()
                                - layoutParams.leftMargin - layoutParams.rightMargin, MeasureSpec.EXACTLY);
                    } else {
                        childWidthMeasureSpec = getChildMeasureSpec(widthMeasureSpec, getPaddingLeft() + getPaddingRight() +
                                layoutParams.leftMargin + layoutParams.rightMargin, layoutParams.width);
                    }

                    if (layoutParams.height == LayoutParams.MATCH_PARENT) {
                        childHeightMeasureSpec = MeasureSpec.makeMeasureSpec(getMeasuredHeight() - getPaddingTop() -
                                getPaddingBottom() - layoutParams.topMargin - layoutParams.bottomMargin, MeasureSpec.EXACTLY);
                    } else {
                        childHeightMeasureSpec = getChildMeasureSpec(heightMeasureSpec, getPaddingTop() + getPaddingBottom() +
                                layoutParams.topMargin + layoutParams.bottomMargin, layoutParams.height);
                    }

                    child.measure(childWidthMeasureSpec, heightMeasureSpec);
                }
            }
        } catch (Exception ex) {
            LogUtils.LOGE(TAG, String.format("Throws new exception: %s", ex.getMessage()));
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }
}
