package co.soml.android.app.activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.Serializable;

import co.soml.android.app.AppConstants;
import co.soml.android.app.R;
import co.soml.android.app.StoryApp;
import co.soml.android.app.activities.adapters.MyStoryPagerAdapter;
import co.soml.android.app.controllers.MyStoryController;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.models.StoryTypes;
import co.soml.android.app.utils.AccountUtils;
import co.soml.android.app.widgets.StoryViewPager;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.14.2015
 */
public class MyStoryActivity extends BaseActivity {
    public static final String TAG = AppConstants.PREFIX + MyStoryActivity.class.getSimpleName();

    public static final String USER_ID = "user_id";
    public static final String SCOPE_NAME = "ScopeName";
    public static final String START_INDEX = "StartIndex";
    public static final String LIMIT = "Limit";
    public static final String STATE = "State";

    public static final String STORY_POST_LIST_TYPE = "StoryFeedType";
    public static final String MY_STORY_VIEW_STATE = "MyStoryViewState";
    public static final String X_API_VERSION = "X_API_VERSION";
    public static final String X_API_TOKEN = "X_API_TOKEN";
    public static final int DEFAULT_MY_STORY_PAGE = 2;

    protected StoryViewPager mMyStoryPager;
    private MyStoryPagerAdapter mMyStoryPagerAdapter;
    private TabLayout mMyStoriesTab;

    private StoryApp mStoryApp;
    private StoryDataModel mStoryDataModel;
    private MyStoryController mController;

    private int mCurrentPage = 2; // Default Tab Position
    private String mScopeName = "Published";
    private int mStartIndex = 1; // Default Start Index
    private int mLimit = 10; // Default Limit

    private Bundle mParams;
    private MyStoryViewState mViewState;
    private Toolbar mActionBarToolBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mystory_layout);
        overridePendingTransition(0, 0);

        mStoryApp = (StoryApp) getApplicationContext();
        mStoryDataModel = mStoryApp.getAppDataModel();
        mController = new MyStoryController(this, mStoryDataModel);
        onBindingUI();
        overridePendingTransition(0, 0);
    }

    public boolean isExistedStoryFeedList() {
        return (mStoryDataModel.getMyStoryPrivateList() != null && mStoryDataModel.getMyStoryPrivateList().size() > 0)
                || (mStoryDataModel.getMyStoryPublishedList() != null && mStoryDataModel.getMyStoryPublishedList().size() > 0)
                || (mStoryDataModel.getMyStoryCollaboratedList() != null && mStoryDataModel.getMyStoryCollaboratedList().size() > 0);
    }

    public void onBindingUI() {
        final MyStoryPagerAdapter.MyStoryFragments[] myStoriesFragments = MyStoryPagerAdapter.MyStoryFragments.values();
        final String myStoryPagerType = getResources().getString(R.string.my_story_pager_type);
        mMyStoryPagerAdapter = new MyStoryPagerAdapter(MyStoryActivity.this, getSupportFragmentManager(), myStoryPagerType);

        for (MyStoryPagerAdapter.MyStoryFragments fragment : myStoriesFragments) {
            if (fragment == MyStoryPagerAdapter.MyStoryFragments.COLLABORATING) {
                mParams = buildArguments(StoryTypes.StoryPostListType.COLLABORATING_TYPE);
            } else if (fragment == MyStoryPagerAdapter.MyStoryFragments.PRIVATE) {

                mParams = buildArguments(StoryTypes.StoryPostListType.PRIVATE_TYPE);
            } else if (fragment == MyStoryPagerAdapter.MyStoryFragments.PUBLISHED) {
                mParams = buildArguments(StoryTypes.StoryPostListType.PUBLISHED_TYPE);
            }
            mMyStoryPagerAdapter.add(fragment.getFragmentClass(), mParams);
        }

        mMyStoryPager = (StoryViewPager) findViewById(R.id.story_view_pager);
        mMyStoryPager.setAdapter(mMyStoryPagerAdapter);
        mMyStoryPagerAdapter.setCurrentPage(1);
        mMyStoryPager.setOffscreenPageLimit(mMyStoryPagerAdapter.getCount() - 1);

        mMyStoriesTab = (TabLayout) findViewById(R.id.sliding_tabs);
        final View mTabView = LayoutInflater.from(MyStoryActivity.this).inflate(R.layout.my_story_tab_indicator, (ViewGroup) null);
        final TextView mTabTextView = (TextView) mTabView.findViewById(android.R.id.text1);
        mTabTextView.setMaxLines(1);
        mMyStoriesTab.addTab(mMyStoriesTab.newTab().setCustomView(mTabView));
        mMyStoriesTab.setupWithViewPager(mMyStoryPager);
    }

    public Bundle buildArguments(StoryTypes.StoryPostListType feedType) {
        final Bundle params = new Bundle();
        final String accountAuthToken = AccountUtils.getAuthToken(MyStoryActivity.this);
        final long userId = AccountUtils.getUserId(MyStoryActivity.this);
        params.putInt(X_API_VERSION, 1);
        params.putString(X_API_TOKEN, accountAuthToken);
        params.putSerializable(STORY_POST_LIST_TYPE, feedType);
        params.putSerializable(MY_STORY_VIEW_STATE, mViewState);

        params.putLong(USER_ID, userId);
        if (feedType == StoryTypes.StoryPostListType.COLLABORATING_TYPE) {
            mScopeName = getString(R.string.collaborating_scope_name);
            params.putString(SCOPE_NAME, mScopeName);
            params.putInt(START_INDEX, mStartIndex);
            params.putInt(LIMIT, mLimit);
        } else if (feedType == StoryTypes.StoryPostListType.PRIVATE_TYPE) {
            mScopeName = getString(R.string.private_scope_name);
            params.putInt(START_INDEX, mStartIndex);
            params.putInt(LIMIT, mLimit);
            params.putString(STATE, "private");

        } else if (feedType == StoryTypes.StoryPostListType.PUBLISHED_TYPE) {
            mScopeName = getString(R.string.published_scope_name);
            params.putInt(START_INDEX, mStartIndex);
            params.putInt(LIMIT, mLimit);
            params.putString(STATE, "published");
        }
        return params;
    }

    /**
     * Sends the request to server for getting data via params as below..
     *
     * @param controllerMessage The ControllerMessage's constant.
     */
    private void sendingRequest(int controllerMessage) {
        final MyStoryController.RequestStoriesParam requestStoriesParam;
        final int apiVersion = 1;
        final String accountAuthToken = AccountUtils.getAuthToken(this);
        final int userId = AccountUtils.getUserId(MyStoryActivity.this);
        requestStoriesParam = new MyStoryController.RequestStoriesParam(apiVersion,accountAuthToken, userId, mScopeName,
                mStartIndex,mLimit, null);
        mController.sendMessage(controllerMessage, requestStoriesParam);
    }
    @Override
    public void invalidate() {

    }

    @Override
    public void invalidate(Object... params) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        onBindingUI();
        invalidate();
    }

    public enum MyStoryViewState implements Serializable {
        PRIVATE, PUBLISHED, COLLABORATING
    }
}
