package co.soml.android.app.services;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import co.soml.android.app.AppConstants;
import co.soml.android.app.models.Sticker;
import co.soml.android.app.utils.LogUtils;

public class StickerResult extends BaseResult {
	public static final String TAG = AppConstants.PREFIX + StickerResult.class.getSimpleName();

	@Expose
	@SerializedName("sticker")
	private Sticker mSticker;

	public StickerResult() {
		LogUtils.LOGD(TAG, "Initializing new StickerResult");
	}

	public Sticker getSticker() {
		return mSticker;
	}

	public void setSticker(Sticker sticker) {
		mSticker = sticker;
	}
}
