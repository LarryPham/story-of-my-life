/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/26/15 10:00 AM.
 **/

package co.soml.android.app.activities.adapters;

import com.squareup.picasso.Picasso;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import co.soml.android.app.AppConstants;
import co.soml.android.app.R;
import co.soml.android.app.StoryApp;
import co.soml.android.app.activities.AppInterfaces;
import co.soml.android.app.controllers.BaseController;
import co.soml.android.app.controllers.ControllerMessage;
import co.soml.android.app.datasets.StoryTable;
import co.soml.android.app.models.Photo;
import co.soml.android.app.models.Story;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.models.StoryFeedList;
import co.soml.android.app.models.StoryTypes;
import co.soml.android.app.models.StoryTypes.StoryPostListType;
import co.soml.android.app.models.User;
import co.soml.android.app.utils.AccountUtils;
import co.soml.android.app.utils.ActivityLauncher;
import co.soml.android.app.utils.DateTimeUtils;
import co.soml.android.app.utils.DisplayUtils;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.utils.NetworkUtils;
import co.soml.android.app.utils.StoryAnimator;
import co.soml.android.app.utils.StringUtils;
import co.soml.android.app.widgets.CircleImageView;
import co.soml.android.app.widgets.KenBurnsSupportView;
import co.soml.android.app.widgets.StoryIconCountView;
import co.soml.android.app.widgets.StoryKenBurnsView;
import co.soml.android.app.widgets.StoryTextView;

import static co.soml.android.app.models.StoryTypes.StoryPostListType.*;
import static co.soml.android.app.models.StoryTypes.StoryPostListType.LATEST_TYPE;

public class StoryPostsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final String TAG = LogUtils.makeLogTag(StoryPostsAdapter.class.getSimpleName());

    private boolean mShowToolbarSpacer;
    private boolean mCanRequestMorePosts;
    private boolean mIsLoggedOutReader;

    private final int mPhotoWidth;
    private final int mPhotoHeight;

    public final StoryPostListType mPostListType;
    public StoryFeedList mStories = new StoryFeedList();

    public AppInterfaces.DataLoadedListener mOnDataLoadedListener;
    public AppInterfaces.OnPostPopupListener mOnPopupListener;
    public AppInterfaces.DataRequestedListener mDataRequestedListener;
    public AppInterfaces.ShowProfileListener mShowProfileListener;
    public AppInterfaces.ContributePhotosListener mContributePhotosListener;

    public BaseController mFeedController;
    public StoryApp mStoryApp;

    public static final int VIEW_TYPE_SPACER = 1;
    public static final int VIEW_TYPE_POST = 2;
    public static final int ITEM_ID_SPACER = -1;

    public void setFeedController(BaseController controller) {
        mFeedController = controller;
    }

    public BaseController getFeedController() {
        return mFeedController;
    }

    public AppInterfaces.DataLoadedListener getOnDataLoadedListener() {
        return mOnDataLoadedListener;
    }

    public void setOnDataLoadedListener(AppInterfaces.DataLoadedListener onDataLoadedListener) {
        mOnDataLoadedListener = onDataLoadedListener;
    }

    public AppInterfaces.OnPostPopupListener getOnPopupListener() {
        return mOnPopupListener;
    }

    public void setOnPopupListener(AppInterfaces.OnPostPopupListener onPopupListener) {
        mOnPopupListener = onPopupListener;
    }

    public AppInterfaces.DataRequestedListener getDataRequestedListener() {
        return mDataRequestedListener;
    }

    public void setDataRequestedListener(AppInterfaces.DataRequestedListener dataRequestedListener) {
        mDataRequestedListener = dataRequestedListener;
    }

    public AppInterfaces.ShowProfileListener getShowProfileListener() {
        return mShowProfileListener;
    }

    public void setShowProfileListener(AppInterfaces.ShowProfileListener showProfileListener) {
        mShowProfileListener = showProfileListener;
    }

    public AppInterfaces.ContributePhotosListener getContributePhotosListener() {
        return mContributePhotosListener;
    }

    public void setContributePhotosListener(AppInterfaces.ContributePhotosListener contributePhotosListener) {
        mContributePhotosListener = contributePhotosListener;
    }

    public StoryFeedList getStories() {
        return mStories;
    }

    public void setStories(StoryFeedList stories) {
        mStories = stories;
    }

    class StoryPostViewHolder extends RecyclerView.ViewHolder {
        private final CardView mCardView;
        private final StoryTextView mTitleView;
        private final StoryTextView mAuthorView;
        private final StoryTextView mLocationView;
        private final StoryTextView mDateTimeView;
        private final StoryKenBurnsView mPhotoSlide;
        private final Button mContributeButton;

        private final StoryIconCountView mLikeCountView;
        private final StoryIconCountView mCommentCountView;

        private final CircleImageView mAvatar;
        private final ImageView mOptionsMore;
        private final ViewGroup mHeaderView;

        public StoryPostViewHolder(View itemView) {
            super(itemView);
            mCardView = (CardView) itemView.findViewById(R.id.story_card_view);
            mTitleView = (StoryTextView) itemView.findViewById(R.id.story_card_title);
            mAuthorView = (StoryTextView) itemView.findViewById(R.id.story_card_by_user);
            mLocationView = (StoryTextView) itemView.findViewById(R.id.story_card_user_location);
            mDateTimeView = (StoryTextView) itemView.findViewById(R.id.story_card_status_date);
            mCommentCountView = (StoryIconCountView) itemView.findViewById(R.id.story_card_comment_button);
            mLikeCountView = (StoryIconCountView) itemView.findViewById(R.id.story_card_subscribe_button);
            mAvatar = (CircleImageView) itemView.findViewById(R.id.story_card_user_image);
            mOptionsMore = (ImageView) itemView.findViewById(R.id.story_card_option_more);
            mHeaderView = (ViewGroup) itemView.findViewById(R.id.story_card_header_container);
            mPhotoSlide = (StoryKenBurnsView) itemView.findViewById(R.id.story_card_slide_show);
            mContributeButton = (Button) itemView.findViewById(R.id.photo_contribute_button);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mOptionsMore.setBackgroundResource(R.drawable.ripple_oval);
            }
        }
    }

    class SpacerViewHolder extends RecyclerView.ViewHolder {
        public SpacerViewHolder(View itemView) {
            super(itemView);
        }
    }

    public StoryPostsAdapter(Context context, StoryPostListType postListType, StoryFeedList storyFeedList) {
        super();
        mPostListType = postListType;
        mIsLoggedOutReader = AccountUtils.isSignedOut(StoryApp.getContext());
        mStories = storyFeedList;
        int displayWidth = DisplayUtils.getDisplayPixelWidth(context);
        int displayHeight = DisplayUtils.getDisplayPixelHeight(context);
        int cardMargin = context.getResources().getDimensionPixelOffset(R.dimen.card_header_horizontal_margin);
        mPhotoWidth = displayWidth - (cardMargin * 2);
        mPhotoHeight = displayWidth - (cardMargin * 2);
        setHasStableIds(true);
    }

    public void setShowToolbarSpacer(boolean show) {
        mShowToolbarSpacer = show;
    }

    public void clear() {
        if (!mStories.isEmpty()) {
            mStories.clear();
            notifyDataSetChanged();
        }
    }

    public void refresh() {
        loadStories();
    }


    @Override
    public int getItemViewType(int position) {
        if (position == 0 && mShowToolbarSpacer) {
            return VIEW_TYPE_SPACER;
        }
        return VIEW_TYPE_POST;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        if (viewType == VIEW_TYPE_SPACER) {
            View spacerView = new View(context);
            int toolBarHeight = context.getResources().getDimensionPixelSize(R.dimen.toolbar_height);
            int dividerHeight = context.getResources().getDimensionPixelOffset(R.dimen.reader_card_gutters);
            int spacerHeight = toolBarHeight - dividerHeight;
            spacerView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, spacerHeight));
            return new SpacerViewHolder(spacerView);
        } else {
            View postView = LayoutInflater.from(context).inflate(R.layout.story_item_view, parent, false);
            return new StoryPostViewHolder(postView);
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof SpacerViewHolder) {
            return;
        }

        final Story story = getItem(position);
        final List<Photo> photos = story.getPhoto();
        final ArrayList<String> photoUrls = new ArrayList<String>();
        for (Photo photo: photos) {
            if (photo.getImageFile() != null && photo.getImageFile().mMediumImage != null) {
                final Photo.MediumImage mediumImage = photo.getImageFile().mMediumImage;
                if (!TextUtils.isEmpty(mediumImage.mUrl)) {
                    photoUrls.add(mediumImage.mUrl);
                }
            }
        }

        final StoryPostViewHolder postHolder = (StoryPostViewHolder) holder;
        StoryPostListType postListType = getPostListType();

        postHolder.mTitleView.setText(story.getName());
        if (story.hasUserName()) {
            postHolder.mAuthorView.setVisibility(View.VISIBLE);
            postHolder.mAuthorView.setText(story.getUser().getFullName());
        } else {
            postHolder.mAuthorView.setVisibility(View.GONE);
        }

        postHolder.mDateTimeView.setText(DateTimeUtils.dateToTimeSpan(StoryApp.getContext(), story.getCreatedAt()));
        final User user = new User(story.getUser());
        if (hasUserImage(user)) {
            final String avatarImageUrl = user.getAvatarOriginalUrl();
            Picasso.with(StoryApp.getContext()).load(avatarImageUrl)
                    .placeholder(R.drawable.person_image_empty).into(postHolder.mAvatar);
        } else {
            if (Build.VERSION.SDK_INT >= 17) {
                postHolder.mAvatar.setImageDrawable(StoryApp.getContext().getResources().getDrawable(R.drawable.person_image_empty));
            } else {
                postHolder.mAvatar.setImageResource(R.drawable.person_image_empty);
            }
        }
        postHolder.mAvatar.setTag(position);
        postHolder.mAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View avatarView) {
                if (mShowProfileListener != null) {
                    mShowProfileListener.onShowProfile(avatarView);
                }
            }
        });
        if (!TextUtils.isEmpty(story.getLocation())) {
            postHolder.mLocationView.setVisibility(View.VISIBLE);
            postHolder.mLocationView.setText(story.getLocation());
        } else {
            postHolder.mLocationView.setVisibility(View.GONE);
        }

        if (photoUrls.size() > 0) {
            postHolder.mPhotoSlide.setVisibility(View.VISIBLE);
            postHolder.mPhotoSlide.setImageUrls(photoUrls);
        } else {
            postHolder.mPhotoSlide.setDefaultImage(R.drawable.background);
        }

        postHolder.mContributeButton.setTag(position);
        postHolder.mContributeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View contributeView) {
                if (mContributePhotosListener != null) {
                    LogUtils.LOGD(TAG, "[ContributePhotos] Trigger to start the screen for contributing images");
                    mContributePhotosListener.onContributePhotos(contributeView);
                }
            }
        });
        showCounts(postHolder, story);
        boolean isCurrentlyLiked = story.isLikeByCurrentUser();
        if (isCurrentlyLiked) {
            postHolder.mLikeCountView.getImageView().setImageResource(R.drawable.ic_like_pressed);
        } else {
            postHolder.mLikeCountView.getImageView().setImageResource(R.drawable.ic_like);
        }
        postHolder.mLikeCountView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleLike(v.getContext(), postHolder, story);
            }
        });
        postHolder.mCommentCountView.setTag(position);
        postHolder.mCommentCountView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtils.LOGD(TAG, "Navigating the current screen to the comments screen");
                ActivityLauncher.showStoryComments(v.getContext(), story);
            }
        });
        postHolder.mOptionsMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnPopupListener != null) {
                    mOnPopupListener.onShowPopup(v, story);
                }
            }
        });

        if (mCanRequestMorePosts && mDataRequestedListener != null && (position >= getItemCount() - 1)) {
            mDataRequestedListener.onRequestData();
        }
    }

    public static boolean hasUserImage(User user) {
        return (!TextUtils.isEmpty(user.getAvatarLargeUrl())
                || !TextUtils.isEmpty(user.getAvatarMediumUrl())
                ||!TextUtils.isEmpty(user.getAvatarOriginalUrl())
                ||!TextUtils.isEmpty(user.getAvatarSmallUrl())
                ||!TextUtils.isEmpty(user.getAvatarThumbnailUrl()));
    }
    public StoryPostListType getPostListType() {
        return (mPostListType != null ? mPostListType : StoryTypes.DEFAULT_STORY_POST_LIST_TYPE);
    }
    @Override
    public int getItemCount() {
        if (mShowToolbarSpacer && mStories.size() > 0) {
            return mStories.size() + 1;
        }
        return mStories.size();
    }

    public boolean isEmpty() {
        return mStories == null || mStories.size() == 0;
    }

    protected void showCounts(StoryPostViewHolder holder, Story story) {
        holder.mLikeCountView.setCount(story.getLikesCount(), true);
        holder.mLikeCountView.getTextCount().setVisibility(View.VISIBLE);
        if (story.getCommentsCount() > 0) {
            holder.mCommentCountView.setCount(story.getCommentsCount(), true);
            holder.mCommentCountView.getTextCount().setVisibility(View.VISIBLE);
        } else {
            holder.mCommentCountView.getTextCount().setVisibility(View.GONE);
        }
    }

    protected void toggleLike(Context context, StoryPostViewHolder holder, Story story) {
        if (story == null || !NetworkUtils.checkConnection(context)) {
            return;
        }
        boolean isCurrentlyLiked = story.isLikeByCurrentUser();
        boolean isAskingToLike =!isCurrentlyLiked;
        StoryAnimator.animateLikeButton(holder.mLikeCountView.getImageView(), isAskingToLike);

        if (mFeedController != null && isAskingToLike) {
            final RequestParam param = new RequestParam(story.getId());
            mFeedController.sendMessage(ControllerMessage.REQUEST_TO_SERVER_STORY_LIKE, param);
        }

        if (mFeedController != null && !isAskingToLike) {
            final RequestParam param = new RequestParam(story.getId());
            mFeedController.sendMessage(ControllerMessage.REQUEST_TO_SERVER_STORY_UNLIKE, param);
        }

        int position = mStories.indexOfStory(story);
        if (story.isLikeByCurrentUser()) {
            holder.mLikeCountView.setSelected(true);
            showCounts(holder, story);
        }

        /*Story updatedStory = StoryTable.getStory(story.getId());

        if (updatedStory != null && position > -1) {
            mStories.set(position, updatedStory);
            holder.mLikeCountView.setSelected(updatedStory.isLikeByCurrentUser());
            showCounts(holder, updatedStory);
        }*/
    }

    public void updateHeartButton(final StoryPostViewHolder holder, boolean animated) {

    }
    private Story getItem(int position) {
        if (mShowToolbarSpacer) {
            return position == 0 ? null : mStories.get(position - 1);
        } else {
            return mStories.get(position);
        }
    }

    protected void loadStories() {
        if (mIsTaskRunning) {
            LogUtils.LOGD(TAG, "story post task already running");
            return;
        }

        new LoadStoriesTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public long getItemId(int position) {
        if (getItemViewType(position) == VIEW_TYPE_SPACER) {
            return ITEM_ID_SPACER;
        }
        return getItem(position).getStableId();
    }

    public class RequestParam {
        public int mId;
        public String mMessage;
        public String mReason;

        public RequestParam(int id) {
            this.mId = id;
        }

        public RequestParam(int id, String message, String reason) {
            this.mId = id;
            this.mReason = reason;
            this.mMessage = message;
        }
    }

    private boolean mIsTaskRunning = false;

    public class LoadStoriesTask extends AsyncTask<Void, Void, Boolean> {

        StoryFeedList allStories;

        @Override
        protected void onPreExecute() {
            mIsTaskRunning = true;
        }

        @Override
        protected void onCancelled() {
            mIsTaskRunning = false;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            switch (getPostListType()) {
                case LATEST_TYPE: {
                    allStories = mStoryApp.getAppDataModel().getLatestStoryList();
                    return !mStories.isSameList(allStories);
                }
                case FEATURED_TYPE: {
                    allStories = mStoryApp.getAppDataModel().getFeaturedStoryList();
                    return !mStories.isSameList(allStories);
                }
                case FOLLOWING_TYPE:
                    allStories = mStoryApp.getAppDataModel().getFollowingStoryList();
                    return !mStories.isSameList(allStories);
            }
            int numExisting = allStories.size();
            mCanRequestMorePosts = (numExisting < AppConstants.MAX_STORY_LIST_ITEMS_TO_DISPLAY);
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (result) {
                mStories.clear();
                mStories.addAll(allStories);
                notifyDataSetChanged();
            }
            mIsTaskRunning = false;

            if (mOnDataLoadedListener != null) {
                mOnDataLoadedListener.onDataLoaded(isEmpty());
            }
        }
    }
}
