/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/1/15 12:44 AM.
 **/

package co.soml.android.app.drawables;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;

import co.soml.android.app.utils.LogUtils;

public class StickerDrawable extends BitmapDrawable implements FeatherDrawable {
    public static final String TAG = LogUtils.makeLogTag(StickerDrawable.class.getSimpleName());

    private float mMinWidth = 0.0F;
    private float mMinHeight = 0.0F;

    protected BlurMaskFilter mBlurMaskFilter;
    protected Paint          mShadowPaint;
    protected Bitmap         mShadowBitmap;
    protected boolean        mDrawShadow = true;
    protected Rect           mTempRect = new Rect();

    public StickerDrawable(Resources resources, Bitmap bitmap) {
        super (resources, bitmap);

        this.mBlurMaskFilter = new BlurMaskFilter(5.0F, BlurMaskFilter.Blur.OUTER);
        this.mShadowPaint = new Paint(1);
        this.mShadowPaint.setMaskFilter(this.mBlurMaskFilter);

        int[] offsetXY = new int[2];
        this.mShadowBitmap = getBitmap().extractAlpha(this.mShadowPaint, offsetXY);
    }

    public int getBitmapWidth() {
        return getBitmap().getWidth();
    }

    public int getBitmapHeight() {
        return getBitmap().getHeight();
    }

    public void draw(Canvas canvas) {
        if (this.mDrawShadow) {
            copyBounds(this.mTempRect);
            canvas.drawBitmap(this.mShadowBitmap, null, this.mTempRect, null);
        }
        super.draw(canvas);
    }

    public void setDropShadow(boolean value) {
        this.mDrawShadow = value;
        invalidateSelf();
    }


    @Override
    public void setMinSize(float width, float height) {
        this.mMinHeight = height;
        this.mMinWidth = width;
    }

    @Override
    public float getMinWidth() {
        return this.mMinWidth;
    }

    @Override
    public float getMinHeight() {
        return this.mMinHeight;
    }

    @Override
    public boolean validateSize(RectF rectF) {
        return (rectF.width() >= this.mMinWidth) && (rectF.height() >= this.mMinHeight);
    }

    @Override
    public void unScheduleSelf(Runnable runnable) {

    }

    @Override
    public float getCurrentWidth() {
        return getIntrinsicWidth();
    }

    @Override
    public float getCurrentHeight() {
        return getIntrinsicHeight();
    }

    public void setMinWidth(float minWidth) {
        mMinWidth = minWidth;
    }

    public void setMinHeight(float minHeight) {
        mMinHeight = minHeight;
    }
}
