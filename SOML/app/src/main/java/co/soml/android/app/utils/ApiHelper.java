/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/21/15 9:13 AM.
 **/

package co.soml.android.app.utils;

import android.content.Context;
import android.os.AsyncTask;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

import co.soml.android.app.StoryApp;
import co.soml.android.app.models.Photo;

public class ApiHelper {
    public static final String TAG = LogUtils.makeLogTag(ApiHelper.class.getSimpleName());

    public enum ErrorType {
        NO_ERROR, UNKNOWN_ERROR, INVALID_CURRENT_STORY, NETWORK_CONNECTION, INVALID_CONTEXT, INVALID_RESULT,
        NO_UPLOAD_FILES_CAP, CAST_EXECPTION
    }
    public static abstract class HelperAsyncTask<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {

        protected String mErrorMessage;
        protected ErrorType mErrorType = ErrorType.NO_ERROR;
        protected Throwable mThrowable;

        protected void setError(ErrorType errorType, String errorMessage) {
            mErrorMessage = errorMessage;
            mErrorType = errorType;
            LogUtils.LOGE(TAG, mErrorType.name() + " - " + mErrorMessage);
        }

        protected void setError(ErrorType errorType, String errorMessage, Throwable throwable) {
            mErrorMessage = errorMessage;
            mErrorType = errorType;
            mThrowable = throwable;

            LogUtils.LOGE(TAG, mErrorType.name() + " - " + mErrorMessage, throwable);
        }
    }

    public interface GenericErrorCallback {
        void onFailure(ErrorType errorType, String errorMessage, Throwable throwable);
    }

    public interface GenericCallback extends GenericErrorCallback {
        void onSuccess();
    }

    public static class FetchSingleStoryTask extends HelperAsyncTask<java.util.List<?>, Boolean, Boolean> {
        public interface Callback extends GenericErrorCallback {
            void onSuccess();
        }

        private Callback mCallBack;
        private String mErrorMessage;

        public FetchSingleStoryTask(Callback callback) {
            mCallBack = callback;
        }


        @Override
        protected Boolean doInBackground(List<?>... params) {
            return null;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if (mCallBack != null) {
                if (success) {
                    mCallBack.onSuccess();
                } else {
                    mCallBack.onFailure(mErrorType, mErrorMessage, mThrowable);
                }
            }
        }

    }


    public static class UploadPhotoTask extends HelperAsyncTask<List<?>, Void, String> {

        public interface Callback extends GenericErrorCallback {
            void onSuccess(String id);
        }

        private Context mContext;
        private Callback mCallback;
        private Photo mPhoto;

        @Override
        protected String doInBackground(List<?>... params) {
            List<?> arguments = params[0];

            return null;
        }

        protected File getTempFile(Context context) {
            String tempFileName = "tm-" + System.currentTimeMillis();
            try {
                context.openFileOutput(tempFileName, Context.MODE_PRIVATE);
            } catch (FileNotFoundException ex) {
                return null;
            }

            return context.getFileStreamPath(tempFileName);
        }

        @Override
        protected void onPostExecute(String result) {
            if (mCallback != null) {
                if (result != null) {
                    mCallback.onSuccess(result);
                } else {
                    mCallback.onFailure(mErrorType, mErrorMessage, mThrowable);
                }
            }
        }
    }
}
