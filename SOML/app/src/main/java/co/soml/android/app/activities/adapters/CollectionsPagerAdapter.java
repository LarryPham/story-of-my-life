package co.soml.android.app.activities.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Locale;

import co.soml.android.app.R;
import co.soml.android.app.fragments.BaseFragment;
import co.soml.android.app.fragments.CollectionsFragment;
import co.soml.android.app.utils.Lists;
import co.soml.android.app.utils.LogUtils;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.27.2015
 * <p>
 * A {@link FragmentPagerAdapter} class for swiping between trending lists, latest lists, and
 * featured lists {@link BaseFragment } on phone.
 */
public class CollectionsPagerAdapter extends FragmentStatePagerAdapter {

    public static final String TAG = LogUtils.makeLogTag(CollectionsPagerAdapter.class);

    private final SparseArray<WeakReference<CollectionsFragment>> mFragmentArray = new SparseArray<WeakReference<CollectionsFragment>>();

    private final List<Holder> mHolderList = Lists.newArrayList();

    private final Context mContext;

    private int mCurrentPage;

    private String mCollectionsType;

    /**
     * Constructor of <code>PagerAdapter</code>
     *
     * @param context         The Application Context.
     * @param fragmentManager The Support Fragment Manager
     */
    public CollectionsPagerAdapter(Context context, final FragmentManager fragmentManager, String collectionsType) {
        super(fragmentManager);
        mContext = context;
        mCollectionsType = collectionsType;
    }

    /**
     * Adds a new BaseFragment class to the viewer (the BaseFragment is internally instantiate)
     *
     * @param className The full qualified name of fragment class.
     * @param params    The instantiate params.
     */
    public void add(final Class<? extends BaseFragment> className, final Bundle params) {
        final Holder holder = new Holder();
        holder.mClassName = className.getName();
        holder.mParams = params;
        final int position = mHolderList.size();
        mHolderList.add(position, holder);
        notifyDataSetChanged();
    }

    /**
     * Returns the {@link Fragment} in the argument position.
     *
     * @param position The position of the BaseFragment to return.
     * @return BaseFragment The {@link BaseFragment} in the fragment position.
     */
    public CollectionsFragment getFragment(final int position) {
        final WeakReference<CollectionsFragment> mWeakFragment = mFragmentArray.get(position);
        if (mWeakFragment != null && mWeakFragment.get() != null) {
            return mWeakFragment.get();
        }
        return getItem(position);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        final CollectionsFragment fragment = (CollectionsFragment) super.instantiateItem(container, position);
        final WeakReference<CollectionsFragment> weakFragment = mFragmentArray.get(position);
        if (weakFragment != null) {
            weakFragment.clear();
        }

        mFragmentArray.put(position, new WeakReference<CollectionsFragment>(fragment));
        return fragment;
    }

    @Override
    public int getCount() {
        return mHolderList.size();
    }

    @Override
    public CollectionsFragment getItem(int position) {
        final Holder currentHolder = mHolderList.get(position);
        final CollectionsFragment fragment = (CollectionsFragment) CollectionsFragment.instantiate(mContext, currentHolder.mClassName, currentHolder
                .mParams);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
        final WeakReference<CollectionsFragment> weakFragment = mFragmentArray.get(position);
        if (weakFragment != null) {
            weakFragment.clear();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CharSequence getPageTitle(int position) {
        if (mCollectionsType.equals(mContext.getString(R.string.collections_pager_type))) {
            return mContext.getResources().getStringArray(R.array.collection_page_titles)[position]
                    .toUpperCase(Locale.getDefault());
        }
        return null;
    }

    /**
     * Returns the current page position
     */
    public int getCurrentPage() {
        return mCurrentPage;
    }

    /**
     * Method that sets the current pgate position
     *
     * @param currentPage The current page
     */
    public void setCurrentPage(int currentPage) {
        this.mCurrentPage = currentPage;
    }

    /**
     * An enumeration of all the collections fragments supported.
     */
    public enum CollectionFragments {
        /**
         * The Featured Stories Fragment
         */
        FEATURED(CollectionsFragment.class),
        /**
         * The Trending Stories Fragment
         */
        LATEST(CollectionsFragment.class),
        /**
         * The Following Stories Fragment
         */
        FOLLOWING(CollectionsFragment.class);

        private Class<? extends BaseFragment> mFragmentClass;

        /**
         * Constructor of <code>CollectionsFragment</code>
         *
         * @param fragmentClass The BaseFragment class
         */
        private CollectionFragments(final Class<? extends BaseFragment> fragmentClass) {
            mFragmentClass = fragmentClass;
        }

        /**
         * Returns the BaseFragment class which have been interacted into the collections of stories screen
         *
         * @return The BaseFragment class
         */
        public Class<? extends BaseFragment> getFragmentClass() {
            return mFragmentClass;
        }
    }

    /**
     * A private class with information about fragment initialization
     */
    public final static class Holder {
        String mClassName;
        Bundle mParams;
    }
}
