/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/26/15 1:31 PM.
 **/

package co.soml.android.app.activities;

import android.view.View;

import co.soml.android.app.models.Comment;
import co.soml.android.app.models.Story;
import co.soml.android.app.services.StoryResult;

public class AppInterfaces {

    public interface OnStoryPostSelectedListener {
        void onStoryPostSelected(Story story);
    }

    public interface AutoHideToolbarListener {
        void onShowHideToolbar(boolean show);
    }

    public interface OnPostPopupListener {
        void onShowPopup(View view, Story story);
    }

    public interface DataLoadedListener {
        void onDataLoaded(boolean isEmpty);
    }

    public interface DataRequestedListener {
        void onRequestData();
    }

    public interface UpdateStoryInfoListener {
        void onResult(Story story);
    }

    public interface UpdateResultListener {
        void onUpdateResult(StoryResult result);
    }

    public interface CommentActionListener {
        void onActionResult(boolean succeeded, Comment comment);
    }

    public interface LoadMoreListener {
        void onLoadMore();
    }

    public interface ShowProfileListener {
        void onShowProfile(View view);
    }

    public interface ContributePhotosListener {
        void onContributePhotos(View view);
    }

    public interface UnfollowUserListener {
        void onUnfollowUser(View view);
    }

    public interface FollowUserListener {
        void onFollowUser(View view);
    }

    public interface EditProfileListener {
        void onEditProfile(View view);
    }
}
