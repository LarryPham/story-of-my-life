/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 7/31/15 3:12 AM.
 **/

package co.soml.android.app.widgets;

import com.imagezoom.ImageViewTouch;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import android.widget.ImageView;

import co.soml.android.app.R;
import co.soml.android.app.StoryApp;
import co.soml.android.app.camera.utils.Point2D;
import co.soml.android.app.drawables.EditableDrawable;
import co.soml.android.app.drawables.FeatherDrawable;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.utils.UIUtils;

public class HighLightView implements EditableDrawable.OnSizeChange {

    static final String TAG = LogUtils.makeLogTag(HighLightView.class.getSimpleName());

    public static enum AlignModeV {
        Top, Bottom, Center
    }

    public interface OnDeleteClickListener {
        void onDeleteClick();
    }

    private int  STATE_NONE = 1 << 0;
    private int  STATE_SELECTED = 1 << 1;
    private int  STATE_FOCUSED = 1 << 2;

    private OnDeleteClickListener mDeleteClickListener;

    public static final int NONE    = 1 << 0;
    public static final int GROW_LEFT_EDGE = 1 << 1;
    public static final int GROW_RIGHT_EDGE = 1 << 2;
    public static final int GROW_TOP_EDGE = 1 << 3;
    public static final int GROW_BOTTOM_EDGE = 1 << 4;
    public static final int ROTATE = 1 << 5;
    public static final int MOVE = 1 << 6;
    public static final int GROW = GROW_TOP_EDGE | GROW_LEFT_EDGE | GROW_RIGHT_EDGE | GROW_BOTTOM_EDGE;
    public static final float HIT_TOLERANCE = 40f;

    private boolean mHidden;
    private int mMode;
    private int mState = STATE_NONE;
    private RectF mDrawRect = new RectF();
    private final RectF mTempRect = new RectF();
    private RectF mCropRect;
    private Matrix mMatrix;
    private FeatherDrawable mContent;
    private EditableDrawable mEditableContent;

    private Drawable mAnchorRotate;
    private Drawable mAnchorDelete;
    private Drawable mBackgroundDrawable;

    private int mAnchorRotateWidth;
    private int mAnchorRotateHeight;
    private int mAnchorDeleteWidth;
    private int mAnchorDeleteHeight;
    private int mResizeEdgeMode;

    private boolean mRotateEnabled;
    private boolean mScaleEnabled;
    private boolean mMoveEnabled;

    private float mRotation = 0;
    private float mRatio = 1f;
    private Matrix mRotateMatrix = new Matrix();
    private final float mPointFs[] = new float[] {0, 0};

    private int mPadding = 0;
    private boolean mShowAnchors = true;
    private AlignModeV mAlignVerticalMode = AlignModeV.Center;
    private ImageViewTouch mContext;

    private static final int[] STATE_SET_NONE = new int[]{};
    private static final int[] STATE_SET_SELECTED = new int[] {android.R.attr.state_selected};
    private static final int[] STATE_SET_SELECTED_PRESSED = new int[] {
            android.R.attr.state_selected, android.R.attr.state_pressed
    };
    private static final int[] STATE_SET_SELECTED_FOCUSED = new int[] {android.R.attr.state_focused };

    public void setMovable(boolean movable) {
        this.mMoveEnabled = movable;
    }

    public void setScalable(boolean scalable) {
        this.mScaleEnabled = scalable;
        if (scalable) {
            mAnchorRotate = StoryApp.getContext().getResources().getDrawable(R.drawable.move_circle);
        } else {
            mAnchorRotate = null;
        }
    }

    public void setDeleteable(boolean deleteable) {
        if (deleteable) {
            mAnchorDelete = StoryApp.getContext().getResources().getDrawable(R.drawable.move_close);
        } else {
            mAnchorDelete = null;
        }
    }

    public HighLightView (ImageView context, int styleId, FeatherDrawable content) {
        this.mContent = content;
        if (content instanceof EditableDrawable) {
            mEditableContent = (EditableDrawable) content;
            mEditableContent.setOnSizeChangeListener(this);
        } else {
            mEditableContent = null;
        }

        float minSize = -1f;
        LogUtils.LOGI(TAG, "DrawableHightLightView. StyleId: " + styleId);
        mMoveEnabled = true;
        mRotateEnabled = true;
        mScaleEnabled = true;

        mAnchorRotate = StoryApp.getContext().getResources().getDrawable(R.drawable.move_circle);
        mAnchorDelete = StoryApp.getContext().getResources().getDrawable(R.drawable.move_close);

        if (null != mAnchorRotate) {
            mAnchorRotateWidth = mAnchorRotate.getIntrinsicWidth()/2;
            mAnchorRotateHeight = mAnchorRotate.getIntrinsicHeight()/2;
        }

        if (null != mAnchorDelete) {
            mAnchorDeleteWidth = mAnchorDelete.getIntrinsicWidth()/2;
            mAnchorDeleteHeight = mAnchorDelete.getIntrinsicHeight()/2;
        }

        updateRatio();
        if (minSize > 0) {
            setMinSize(minSize);
        }
    }

    public void setAlignModeV(AlignModeV mode) {
        mAlignVerticalMode = mode;
    }

    protected RectF computeLayout() {
        return getDisplayRect(mMatrix, mCropRect);
    }

    public void dispose() {
        mDeleteClickListener = null;
        mContext = null;
        mContent = null;
        mEditableContent = null;
    }

    public void copyBounds(RectF outerRect) {
        outerRect.set(mDrawRect);
        outerRect.inset(-mPadding, -mPadding);
    }

    public void draw(final Canvas canvas) {
        if (mHidden) {
            return;
        }

        final int saveCount = canvas.save();
        canvas.concat(mRotateMatrix);

        if (null != mBackgroundDrawable) {
            mBackgroundDrawable.setBounds((int) mTempRect.left, (int) mTempRect.top, (int) mTempRect.right, (int) mTempRect.bottom);
            mBackgroundDrawable.draw(canvas);
        }

        boolean isSelected = isSelected();
        boolean isFocused = isFocused();

        if (mEditableContent != null) {
            mEditableContent.setBounds(mDrawRect.left, mDrawRect.top, mDrawRect.right, mDrawRect.bottom);
        } else {
            mContent.setBounds((int) mDrawRect.left, (int) mDrawRect.top, (int) mDrawRect.right, (int) mDrawRect.bottom);
        }

        mContent.draw(canvas);
        if (isSelected || isFocused) {
            if (mShowAnchors) {
                final int left = (int) (mTempRect.left);
                final int right = (int) (mTempRect.right);
                final int top = (int) (mTempRect.top);
                final int bottom = (int) (mTempRect.bottom);

                if (mAnchorRotate != null) {
                    mAnchorRotate.setBounds( right - mAnchorRotateWidth, bottom - mAnchorRotateHeight, right + mAnchorRotateWidth, bottom
                            + mAnchorRotateHeight);
                    mAnchorRotate.draw(canvas);
                }

                if (mAnchorDelete != null) {
                    mAnchorDelete.setBounds(left - mAnchorDeleteWidth, top - mAnchorDeleteHeight, left + mAnchorDeleteWidth, top +
                            mAnchorDeleteHeight);
                }
            }
        }

        canvas.restoreToCount(saveCount);
    }

    public void draw(final Canvas canvas, final Matrix source) {
        final Matrix matrix = new Matrix(source);
        matrix.invert(matrix);

        final int saveCount = canvas.save();
        canvas.concat(matrix);
        canvas.concat(mRotateMatrix);

        mContent.setBounds((int) mDrawRect.left, (int) mDrawRect.top, (int) mDrawRect.right, (int) mDrawRect.bottom);
        mContent.draw(canvas);
        canvas.restoreToCount(saveCount);
    }

    @Override
    public void onSizeChanged(EditableDrawable content, float left, float top, float right, float bottom) {
        LogUtils.LOGI(TAG, "onSizeChanged: " + left + " , " + top + " , " + right + " , " + bottom);
        if (content.equals(mEditableContent) && null != mContext) {
            if (mDrawRect.left != left || mDrawRect.top != top || mDrawRect.right != right || mDrawRect.bottom != bottom) {
                if (forceUpdate()) {
                    mContext.invalidate(getInvalidateRect());
                } else {
                    mContext.postInvalidate();
                }
            }
        }
    }

    public boolean isSelected() {
        return (mState & STATE_SELECTED) == STATE_SELECTED;
    }

    public boolean isFocused() {
        return (mState & STATE_FOCUSED) == STATE_FOCUSED;
    }

    public boolean isPressed() {
        return isSelected() && mMode != NONE;
    }

    public void setFocused(final boolean focused) {
        LogUtils.LOGD(TAG, "setFocused(): " + focused);
        boolean isFocused = isFocused();
        if (isFocused != focused) {
            mState ^= STATE_FOCUSED;

            if (null != mEditableContent) {
                if (focused) {
                    mEditableContent.beginEdit();
                } else {
                    mEditableContent.endEdit();
                }
            }
            updateDrawableState();
        }
    }

    protected void updateDrawableState() {
        if (null == mBackgroundDrawable) {
            return;
        }

        boolean isSelected = isSelected();
        boolean isFocused = isFocused();

        if (isSelected) {
            if (mMode == NONE) {
                if (isFocused) {
                    mBackgroundDrawable.setState(STATE_SET_SELECTED_FOCUSED);
                } else {
                    mBackgroundDrawable.setState(STATE_SET_SELECTED);
                }
            } else {
                mBackgroundDrawable.setState(STATE_SET_SELECTED_PRESSED);
            }
        } else {
            mBackgroundDrawable.setState(STATE_SET_NONE);
        }
    }

    public void setSelected(final boolean selected) {
        LogUtils.LOGD(TAG, "setSelected(): " + selected);
        boolean isSelected = isSelected();
        if (isSelected != selected) {
            mState ^= STATE_SELECTED;
            updateDrawableState();
        }
    }

    public void setup(final Context context, final Matrix matrix, final Rect imageRect, final RectF cropRect, final boolean
            maintainAspectRatio) {
        mMatrix = new Matrix(matrix);
        mRotation = 0;
        mRotateMatrix = new Matrix();
        setMode(NONE);
        invalidate();
    }

    public void invalidate() {
        mDrawRect = computeLayout();
        LogUtils.LOGD(TAG, "computeLayout: " + mDrawRect);
        if (mDrawRect != null && mDrawRect.left > 1200) {
            LogUtils.LOGE(TAG, "computeLayout: " + mDrawRect);
        }

        mRotateMatrix.reset();
        mRotateMatrix.postTranslate(-mDrawRect.centerX(), -mDrawRect.centerY());
        mRotateMatrix.postRotate(mRotation);
        mRotateMatrix.postTranslate(mDrawRect.centerX(), mDrawRect.centerY());
    }

    public void moveBy(final float dx, final float dy) {
        if (mMoveEnabled) {
            mCropRect.offset(dx,dy);
            invalidate();
        }
    }

    public void rotateBy(final float dx, final float dy, float diffX, float diffY) {
        if (!mRotateEnabled && !mScaleEnabled) {
            return;
        }

        final float point1[] = new float[] { mDrawRect.centerX(), mDrawRect.centerY() };
        final float point2[] = new float[] { mDrawRect.right, mDrawRect.bottom };
        final float point3[] = new float[] { dx, dy};

        final double angle1 = Point2D.angleBetweenPoints(point2, point1);
        final double angle2 = Point2D.angleBetweenPoints(point3, point1);

        if (mRotateEnabled) {
            mRotation = -(float) (angle2 - angle1);
        }

        if (mScaleEnabled) {
            final Matrix rotateMatrix = new Matrix();
            rotateMatrix.postRotate(-mRotation);

            final float points[] = new float[] { diffX, diffY };
            rotateMatrix.mapPoints(points);

            diffX = points[0];
            diffY = points[1];

            final float xDelta = diffX * (mCropRect.width() / mDrawRect.width());
            final float yDelta = diffY * (mCropRect.height() / mDrawRect.height());

            final float point4[] = new float[] { mDrawRect.right + xDelta, mDrawRect.bottom + yDelta};
            final double distance1 = Point2D.distance(point1, point2);
            final double distance2 = Point2D.distance(point1, point4);
            final float distance = (float) (distance2 - distance1);

            growBy(distance);
        }
    }

    protected void growBy(final float dx) {
        growBy(dx, dx/mRatio, true);
    }

    protected void growBy(final float dx, final float dy, boolean checkMinSize) {
        if (!mScaleEnabled) {
            return;
        }

        final RectF r = new RectF(mCropRect);
        if (mAlignVerticalMode == AlignModeV.Center) {
            r.inset(-dx,-dy);
        } else if (mAlignVerticalMode == AlignModeV.Top) {
            r.inset(-dx, 0);
            r.bottom += dy * 2;
        } else {
            r.inset(-dx, 2);
            r.top -= dy * 2;
        }

        RectF testRect = getDisplayRect(mMatrix, r);
        if (!mContent.validateSize(testRect) && checkMinSize) {
            return;
        }

        mCropRect.set(r);
        invalidate();
    }

    public void onMouseMove(int edge, MotionEvent event, float dx, float dy) {
        if (edge == NONE) {
            return;
        }

        mPointFs[0] = dx;
        mPointFs[1] = dy;

        float xDelta;
        float yDelta;

        if (edge == NONE) {
            moveBy(dx * (mCropRect.width() / mDrawRect.width()), dy * (mCropRect.height() / mDrawRect.height()));
        } else if (edge == ROTATE) {
            dx = mPointFs[0];
            dy = mPointFs[1];

            xDelta = dx * (mCropRect.width() / mDrawRect.width());
            yDelta = dy * (mCropRect.height() / mDrawRect.height());
            rotateBy(event.getX(), event.getY(), dx, dy);
            invalidate();
        } else {
            Matrix rotateMatrix = new Matrix();
            rotateMatrix.postRotate(-mRotation);
            rotateMatrix.mapPoints(mPointFs);

            dx = mPointFs[0];
            dy = mPointFs[1];

            if (((GROW_LEFT_EDGE | GROW_RIGHT_EDGE) & edge) == 0)
                dx = 0;
            if (((GROW_TOP_EDGE | GROW_BOTTOM_EDGE) & edge) == 0)
                dy = 0;
            xDelta = dx * (mCropRect.width() / mDrawRect.width());
            yDelta = dy * (mCropRect.height() / mDrawRect.height());

            boolean isLeft = UIUtils.checkBits(edge, GROW_LEFT_EDGE);
            boolean isTop = UIUtils.checkBits(edge, GROW_TOP_EDGE);

            float delta;
            if (Math.abs(xDelta) >= Math.abs(yDelta)) {
                delta = xDelta;
                if (isLeft) {
                    delta *= -1;
                }
            } else {
                delta = yDelta;
                if (isTop) {
                    delta *= -1;
                }
            }

            LogUtils.LOGD(TAG, "x: " + xDelta + ", y: " + yDelta + ", final: " + delta);
            growBy(delta);
            invalidate();
        }
    }
    public RectF getDisplayRect(final Matrix matrix, final RectF supportRect) {
        final RectF r = new RectF(supportRect);
        matrix.mapRect(r);
        return r;
    }

    public RectF getDisplayRect() {
        final RectF r = new RectF(mDrawRect);
        mRotateMatrix.mapRect(r);
        return r;
    }

    public RectF getDrawRect() {
        return mDrawRect;
    }

    public int getHit(float x, float y) {
        final RectF rect = new RectF(mDrawRect);
        rect.inset(-mPadding, -mPadding);

        final float points[] = new float[] { x, y };
        final Matrix rotateMatrix = new Matrix();
        rotateMatrix.postTranslate(-rect.centerX(), -rect.centerY());
        rotateMatrix.postRotate(-mRotation);
        rotateMatrix.postTranslate(rect.centerX(), rect.centerY());
        rotateMatrix.mapPoints(points);

        x = points[0];
        y = points[1];

        int returnValue = NONE;
        final boolean verticalCheck = ( y >= (rect.top - HIT_TOLERANCE) && (y < (rect.bottom + HIT_TOLERANCE)));
        final boolean horizontalCheck = (x >= (rect.left - HIT_TOLERANCE)) && (x < (rect.right + HIT_TOLERANCE));

        // it horizontal and vertical checks are good then
        // at least the move edge is selected
        if (verticalCheck && horizontalCheck) {
            returnValue = MOVE;
        }

        if (mScaleEnabled) {
            LogUtils.LOGD(TAG, "Scale Enabled");
            if ((Math.abs(rect.left - x) < HIT_TOLERANCE) && verticalCheck && UIUtils.checkBits(mResizeEdgeMode, GROW_LEFT_EDGE)) {
                LogUtils.LOGD(TAG, "LEFT");
                returnValue |= GROW_LEFT_EDGE;
            }
            if ((Math.abs(rect.right - x) < HIT_TOLERANCE) && verticalCheck && UIUtils.checkBits(mResizeEdgeMode, GROW_RIGHT_EDGE)) {
                LogUtils.LOGD(TAG, "RIGHT");
                returnValue |= GROW_RIGHT_EDGE;
            }
            if ((Math.abs(rect.top - y) < HIT_TOLERANCE) && horizontalCheck && UIUtils.checkBits(mResizeEdgeMode, GROW_TOP_EDGE)) {
                LogUtils.LOGD(TAG, "TOP");
                returnValue |= GROW_TOP_EDGE;
            }
            if ((Math.abs(rect.bottom - y) < HIT_TOLERANCE) && horizontalCheck && UIUtils.checkBits(mResizeEdgeMode, GROW_BOTTOM_EDGE)) {
                LogUtils.LOGD(TAG, "BOTTOM");
                returnValue |= GROW_BOTTOM_EDGE;
            }
        }

        if ((mRotateEnabled || mScaleEnabled) && (Math.abs(rect.right - x) < HIT_TOLERANCE) && (Math.abs(rect.bottom - y) <
                HIT_TOLERANCE) && verticalCheck && horizontalCheck) {
            returnValue = ROTATE;
        }

        if (mMoveEnabled && (returnValue == NONE) && rect.contains((int) x, (int) y)) {
            returnValue = MOVE;
        }

        LogUtils.LOGD(TAG, "ReturnValue: " + returnValue);
        return returnValue;
    }

    public void onSingleTapConfirmed(float x, float y) {
        final RectF rect = new RectF(mDrawRect);
        rect.inset(-mPadding, -mPadding);

        final float points[] = new float[] { x,y };
        final Matrix rotateMatrix = new Matrix();
        rotateMatrix.postTranslate(-rect.centerX(), -rect.centerY());
        rotateMatrix.postRotate(-mRotation);

        rotateMatrix.postTranslate(rect.centerX(), rect.centerY());
        rotateMatrix.mapPoints(points);
        x = points[0];
        y = points[1];

        final boolean verticalCheck = (y >= (rect.top - HIT_TOLERANCE) && (y < (rect.bottom + HIT_TOLERANCE)));
        final boolean horizontalCheck = (x >= (rect.left - HIT_TOLERANCE) && (x < (rect.right + HIT_TOLERANCE)));

        if (mAnchorDelete != null) {
            if ((Math.abs(rect.left - x) < HIT_TOLERANCE) && (Math.abs(rect.top - y) < HIT_TOLERANCE) && verticalCheck && horizontalCheck) {
                if (mDeleteClickListener != null) {
                    mDeleteClickListener.onDeleteClick();
                }
            }
        }
    }

    protected RectF mInvalidateRectF = new RectF();
    protected Rect mInvalidateRect = new Rect();

    public Rect getInvalidateRect() {
        mInvalidateRectF.set(mDrawRect);
        mInvalidateRectF.inset(-mPadding, -mPadding);

        mInvalidateRect.set((int) mInvalidateRectF.left, (int) mInvalidateRectF.top, (int) mInvalidateRectF.right, (int) mInvalidateRectF
                .bottom);
        int w = Math.max(mAnchorRotateWidth, mAnchorDeleteWidth);
        int h = Math.max(mAnchorRotateHeight, mAnchorDeleteHeight);

        mInvalidateRect.inset(-w*2, -h*2);
        return mInvalidateRect;
    }

    public boolean forceUpdate() {
        LogUtils.LOGD(TAG, "forceUpdate");
        RectF cropRect = getCropRectF();
        RectF drawRect = getDrawRect();

        if (mEditableContent != null) {
            final float textWidth = mContent.getCurrentWidth();
            final float textHeight = mContent.getCurrentHeight();

            updateRatio();
            RectF textRect = new RectF(cropRect);
            getMatrix().mapRect(textRect);

            float dx = textWidth - textRect.width();
            float dy = textHeight - textRect.height();

            float[] floatPoints = new float[] { dx, dy};
            Matrix rotateMatrix = new Matrix();
            rotateMatrix.postRotate(-mRotation);

            dx = floatPoints[0];
            dy = floatPoints[1];

            float xDelta = dx * (cropRect.width() / drawRect.width());
            float yDelta = dy * (cropRect.height() / drawRect.height());

            if (xDelta != 0 || yDelta != 0) {
                growBy(xDelta/2, yDelta/2, false);
            }
            invalidate();
            return true;
        }

        return false;
    }

    private void updateRatio() {
        final float w = mContent.getCurrentWidth();
        final float h = mContent.getCurrentHeight();

        mRatio = w / h;
    }

    public Matrix getMatrix() {
        return mMatrix;
    }

    public int getMode() {
        return mMode;
    }

    public float getRotation() {
        return mRotation;
    }

    public Matrix getRotateMatrix() {
        return mRotateMatrix;
    }

    public RectF getCropRectF() {
        return mCropRect;
    }

    public Rect getCropRect() {
        return new Rect((int) mCropRect.left, (int) mCropRect.top, (int) mCropRect.right, (int) mCropRect.bottom);
    }

    public Matrix getCropRotationMatrix() {
        final Matrix matrix = new Matrix();
        matrix.postTranslate(-mCropRect.centerX(), -mCropRect.centerY());
        matrix.postRotate(mRotation);
        matrix.postTranslate(mCropRect.centerX(), mCropRect.centerY());
        return matrix;
    }

    public void setMinSize(final float size) {
        if (mRatio >= 1) {
            mContent.setMinSize(size, size / mRatio);
        } else {
            mContent.setMinSize(size * mRatio, size);
        }
    }

    public void setMode(final int mode) {
        LogUtils.LOGD(TAG, "setMode: " + mode);
        if (mode != mMode) {
            mMode = mode;
            updateDrawableState();
        }
    }

    public void update(final Matrix imageMatrix, final Rect imageRect) {
        setMode(NONE);
        mMatrix = new Matrix(imageMatrix);
        mRotation = 0;
        mRotateMatrix = new Matrix();
        invalidate();
    }

    void onMove(float dx, float dy) {
        moveBy(dx * (mCropRect.width() / mDrawRect.width()), dy * (mCropRect.height() / mDrawRect.height()));
    }

    public FeatherDrawable getContent() {
        return mContent;
    }

    public void setPadding(int padding) {
        mPadding = padding;
    }

    public void setOnDeleteClickListener(final OnDeleteClickListener listener) {
        mDeleteClickListener = listener;
    }
}
