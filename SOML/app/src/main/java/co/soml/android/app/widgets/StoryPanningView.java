package co.soml.android.app.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.widget.ImageView;

import co.soml.android.app.R;
import co.soml.android.app.utils.LogUtils;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since May.13.2015
 * <p>
 * It's used to improve the introduction screen which will be more effective UI.
 */
public class StoryPanningView extends ImageView {
    public static final String TAG = LogUtils.makeLogTag(StoryPanningView.class);
    private int mPanningDurationInMs;
    private final StoryPanningViewAttacher mAttacher;

    public StoryPanningView(Context context) {
        this(context, null);
    }

    public StoryPanningView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public StoryPanningView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        readStyleParameters(context, attrs);
        super.setScaleType(ScaleType.MATRIX);
        mAttacher = new StoryPanningViewAttacher(this, mPanningDurationInMs);
    }

    private void readStyleParameters(Context context, AttributeSet atts) {
        TypedArray array = context.obtainStyledAttributes(atts, R.styleable.StoryPanningView);
        try {
            mPanningDurationInMs = array.getInt(R.styleable.StoryPanningView_panningDurationInMs, StoryPanningViewAttacher
                    .DEFAULT_PANNING_DURATION_IN_MS);
        } finally {
            array.recycle();
        }
    }

    @Override
    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        stopUpdateStartIfNecessary();
    }

    @Override
    public void setImageResource(int resId) {
        super.setImageResource(resId);
        stopUpdateStartIfNecessary();
    }

    @Override
    public void setImageURI(Uri uri) {
        super.setImageURI(uri);
        stopUpdateStartIfNecessary();
    }

    private void stopUpdateStartIfNecessary() {
        if (mAttacher != null) {
            boolean wasPanning = mAttacher.isPanning();
            mAttacher.stopSpanning();
            mAttacher.update();
            if (wasPanning) {
                mAttacher.startSpanning();
            }
        }
    }

    @Override
    public void setScaleType(ScaleType scaleType) {
        throw new UnsupportedOperationException("Only matrix scaleType is supported");
    }

    @Override
    protected void onDetachedFromWindow() {
        mAttacher.cleanUp();
        super.onDetachedFromWindow();
    }

    public void startPanning() {
        mAttacher.startSpanning();
    }

    public void stopPanning() {
        mAttacher.stopSpanning();
    }
}
