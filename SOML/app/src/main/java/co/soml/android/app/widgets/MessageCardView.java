/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 * @author: Larry Pham (email: larrypham.vn@gmail.com)
 * Date: 6/2/15 9:34 AM
 **/

package co.soml.android.app.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import co.soml.android.app.R;
import co.soml.android.app.utils.LogUtils;

/**
 * A custom view for displaying the message card view. Building the card-view message type for application.
 */
public class MessageCardView extends CardView implements View.OnClickListener {
    public static final String TAG = LogUtils.makeLogTag(MessageCardView.class.getSimpleName());
    public TextView mTitleView;
    public TextView mMessageView;
    public Button[] mButtons;
    public String[] mButtonTags;
    private OnMessageCardButtonListener mListener = null;
    private View mRootView;
    public static final int ANIM_DURATION = 200;

    public interface OnMessageCardButtonListener {
        public void onMessageCardButtonClicked(String tag);
    }

    public MessageCardView(Context context) {
        super(context);
        initialize(context, null, 0);
    }

    public MessageCardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize(context, attrs, 0);
    }

    public MessageCardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(context, attrs, defStyleAttr);
    }

    private void initialize(Context context, AttributeSet attrs, int defStyle) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mRootView = inflater.inflate(R.layout.message_card_view, this, true);
        mTitleView = (TextView) mRootView.findViewById(R.id.title);
        mMessageView = (TextView) mRootView.findViewById(R.id.text);
        mButtons = new Button[]{(Button) mRootView.findViewById(R.id.button1), (Button) mRootView.findViewById(R.id.button2)};

        mButtonTags = new String[]{"", ""};
        for (Button button : mButtons) {
            button.setVisibility(View.GONE);
            button.setOnClickListener(this);
        }
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.MessageCard, defStyle, 0);
        try {
            String title = array.getString(R.styleable.MessageCard_messageTitle);
            setTitle(title);
            String text = array.getString(R.styleable.MessageCard_messageText);
            if (text != null) {
                setText(text);
            }

            String button1Text = array.getString(R.styleable.MessageCard_button1Text);
            boolean button1Emphasis = array.getBoolean(R.styleable.MessageCard_button1Emphasis, false);
            String button1Tag = array.getString(R.styleable.MessageCard_button1Tag);

            String button2Text = array.getString(R.styleable.MessageCard_button2Text);
            boolean button2Emphasis = array.getBoolean(R.styleable.MessageCard_button2Emphasis, false);
            String button2Tag = array.getString(R.styleable.MessageCard_button2Tag);

            int emphasisColor = array.getColor(R.styleable.MessageCard_emphasisColor, getResources().getColor(R.color.color_primary));
            if (button1Text != null) {
                setButton(0, button1Text, button1Tag, button1Emphasis, 0);
            }
            if (button2Text != null) {
                setButton(1, button2Text, button2Tag, button2Emphasis, emphasisColor);
            }
            setRadius(getResources().getDimensionPixelSize(R.dimen.cardview_default_radius));
            setCardElevation(getResources().getDimensionPixelSize(R.dimen.cardview_default_elevation));
            setPreventCornerOverlap(false);
        } finally {
            array.recycle();
        }
    }

    public void setTitle(CharSequence text) {
        if (TextUtils.isEmpty(text)) {
            mTitleView.setVisibility(View.GONE);
        } else {
            mTitleView.setVisibility(View.VISIBLE);
            mTitleView.setText(text);
        }
    }

    public void setText(CharSequence text) {
        mMessageView.setText(text);
    }

    public void setButton(int index, String text, String tag, boolean emphasis, int emphasisColor) {
        if (index < 0 || index >= mButtons.length) {
            LogUtils.LOGW(TAG, "Invalid button index: " + index);
            return;
        }
        mButtons[index].setText(text);
        mButtons[index].setVisibility(View.VISIBLE);
        mButtonTags[index] = tag;
        if (emphasis) {
            if (emphasisColor == 0) {
                emphasisColor = getResources().getColor(R.color.color_primary);
            }
            mButtons[index].setTextColor(emphasisColor);
            mButtons[index].setTypeface(null, Typeface.BOLD);
        }
    }

    @Override
    public void onClick(View source) {
        if (mListener == null) return;
        for (int i = 0; i < mButtons.length; i++) {
            if (mButtons[i] == source) {
                mListener.onMessageCardButtonClicked(mButtonTags[i]);
                break;
            }
        }
    }

    public void overrideBackground(int backgroundResId) {
        findViewById(R.id.card_root).setBackgroundResource(backgroundResId);
    }

    public void dismiss() {
        dismiss(false);
    }

    public void dismiss(boolean animate) {
        if (!animate) {
            setVisibility(View.GONE);
        } else {
            animate().scaleY(0.1f).alpha(0.1f).setDuration(ANIM_DURATION);
        }
    }

    public void show() {
        setVisibility(View.VISIBLE);
    }
}
