/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division.
 *    This software and its documentation are confidential and proprietary information of Sugar Ventures Inc.
 *    No part of the software and documents may be copied, reproduced, transmitted, translated, or reduced to
 *    any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility
 *    for any errors that might appear in the software and documents. This publication and the contents
 *    hereof are subject to change without notice.
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 7/22/15 2:34 PM.
 **/

package co.soml.android.app.fragments;


import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.ViewGroup;

import co.soml.android.app.StoryApp;
import co.soml.android.app.activities.BaseActivity;
import co.soml.android.app.controllers.BaseController;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.utils.LogUtils;

public class TakePhotoFragment extends BaseFragment {

    public static final String TAG = LogUtils.makeLogTag(TakePhotoFragment.class.getSimpleName());
    public static final String ARG_OUTPUT = "output";

    private BaseController mPhotoController;

    private StoryApp mApp;
    private StoryDataModel mDataModel;

    public static TakePhotoFragment newInstance(Uri output) {
        TakePhotoFragment fragment = new TakePhotoFragment();
        final Bundle args = new Bundle();
        args.putParcelable(ARG_OUTPUT, output);
        fragment.setArguments(args);
        return fragment;
    }

    public void setPhotoController(BaseController controller) {
        this.mPhotoController = controller;
    }

    public BaseController getPhotoController() {
        return mPhotoController;
    }

    /**
     * Standard fragment entry point
     * @param savedInstanceState State of a previous instance
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    /**
     * Standard lifecycle method, passed along to the CameraController
     */
    @Override
    public void onStart() {
        super.onStart();

        if (mPhotoController != null) {
            mPhotoController.onCreate();
        }
    }

    /**
     * Standard lifecycle method, for when the fragment moves into the stopped state. Passed a long to the CameraController
     */
    @Override
    public void onStop() {
        super.onStop();
        if (mPhotoController != null) {
            mPhotoController.onDestroy();
        }
    }

    @Override
    public void invalidate() {

    }

    @Override
    public void invalidate(Object... params) {

    }
}
