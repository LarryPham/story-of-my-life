package co.soml.android.app.services;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import co.soml.android.app.AppConstants;
import co.soml.android.app.services.tasks.WeakHandler;
import co.soml.android.app.utils.LogUtils;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.10.2015
 * <p>
 * This class creates pools of background threads for requesting the information, and subscribing the client-side params to server. With
 * it, we can eager to control the each of threads. This class is implemented as a singleton; the only way to get an ServiceTaskManager
 * instance is to call {@link #getInstance()}.
 */
public class ServiceTaskManager {
    static final int REQUEST_TO_SERVER_GET_FAILED = -1;
    static final int REQUEST_TO_SERVER_GET_STARTED = 1;
    static final int REQUEST_TO_SERVER_GET_COMPLETED = 2;

    static final int REQUEST_TO_SERVER_POST_FAILED = 11;
    static final int REQUEST_TO_SERVER_POST_STARTED = 12;
    static final int REQUEST_TO_SERVER_POST_COMPLETED = 13;

    static final int REQUEST_TO_SERVER_PATCH_FAILED = 21;
    static final int REQUEST_TO_SERVER_PATCH_STARTED = 22;
    static final int REQUEST_TO_SERVER_PATCH_COMPLETED = 23;

    static final int REQUEST_TO_SERVER_DELETE_FAILED = 31;
    static final int REQUEST_TO_SERVER_DELETE_STARTED = 32;
    static final int REQUEST_TO_SERVER_DELETE_COMPLETED = 33;
    static final int TASK_COMPLETED = 34;

    public static final int STM_REQUEST_TO_LOGIN_EMAIL_ACCOUNT = 101;
    public static final int STM_REQUEST_TO_LOGIN_FACEBOOK = 102;
    public static final int STM_REQUEST_TO_LOGIN_TWITTER = 103;
    public static final int STM_REQUEST_TO_GET_STORIES = 104;

    // Sets the amount of time an idle thread will wait for a task before terminating
    private static final int KEEP_ALIVE_TIME = 1;
    private static final TimeUnit KEEP_ALIVE_TIME_UNIT;
    private static final int CORE_POOL_SIZE = 20;
    private static final int MAXIMUM_POOL_SIZE = 20;

    private final BlockingQueue<Runnable> mServiceThreadsQueue;
    // A managed pool of background service threads
    private final ThreadPoolExecutor mThreadPoolExecutor;
    // An object that manages Messages in a Thread
    private WeakHandler mWeakHandler;
    private static ServiceTaskManager sInstance = null;
    /**
     * Note: this is the number of total available cores. On current versions of Android, with devices that use plug-andd-play cores,
     * this will return less than the total number of cores. The total number of cores is not available in current Android implementation
     */
    private static int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();

    static {
        KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS;
        sInstance = new ServiceTaskManager();
    }

    private ServiceTaskManager() {
        /**
         * Creates a service's threads queue for pool of Thread objects used for requesting data from server, or posting params from client side
         * to server, using a linked list queue that blocks when the queue is empty.
         */
        mServiceThreadsQueue = new LinkedBlockingQueue<Runnable>();
        /**
         * Creates a new pool of Thread objects for ServiceThreadsQueue
         */
        mThreadPoolExecutor = new ThreadPoolExecutor(CORE_POOL_SIZE, MAXIMUM_POOL_SIZE, KEEP_ALIVE_TIME, KEEP_ALIVE_TIME_UNIT,
                mServiceThreadsQueue);
        /**
         * Instantiates a new anonymous Handler object are defines it handleMessage() method. The Handler *must* run on the UI thread,
         * because it moves DataResult from the RequestTask or SubscribeTask to the View object.
         * To force the Handler to run on The UI Thread, it's defined as part of ServiceTaskManager constructor. The constructor is invoked
         * when the class is first referenced, and that happens when the View invokes startRequest. Since the View runs on the UI thread,
         * so does the constructor and the Handler.
         */
        mWeakHandler = new WeakHandler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
            }
        };
    }

    public static synchronized ServiceTaskManager getInstance() {
        if (sInstance == null) {
            sInstance = new ServiceTaskManager();
        }
        return sInstance;
    }

    class JobThread implements Runnable {
        public final String TAG = AppConstants.PREFIX + JobThread.class.getSimpleName();
        WeakHandler mJobHandler = null;
        int mRequestMsg = 0;
        Handler.Callback mJobHandlerCallback = new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                switch (msg.what) {
                    case STM_REQUEST_TO_LOGIN_FACEBOOK: {
                        break;
                    }
                    case STM_REQUEST_TO_LOGIN_TWITTER: {
                        break;
                    }
                    case STM_REQUEST_TO_LOGIN_EMAIL_ACCOUNT: {
                        break;
                    }
                    case STM_REQUEST_TO_GET_STORIES: {

                    }
                }
                return true;
            }
        };

        public JobThread() {

        }

        public WeakHandler getJobHandler() {
            return mJobHandler;
        }

        public int getRequestMsg() {
            return mRequestMsg;
        }

        public void setRequestMsg(int requestMsg) {
            mRequestMsg = requestMsg;
        }

        @Override
        public void run() {
            Looper.prepare();
            LogUtils.LOGD(TAG, String.format("Thread[%s] start. %d", this.getClass().getName(), mRequestMsg));
            /**
             * Create the child handler on the child thread so it is bound to the child's message queue
             */
            mJobHandler = new WeakHandler(mJobHandlerCallback);
            /**
             * Start looping the message queue of this thread.
             */
            Looper.loop();
            LogUtils.LOGD(TAG, String.format("Thread %s end. %d", this.getClass().getName(), mRequestMsg));
        }
    }
}
