package co.soml.android.app.models;

import co.soml.android.app.AppConstants;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its
 * documentation are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable
 * form without the prior written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with
 * respect to the contents, and assumes no responsibility for any errors that might appear in the software and
 * documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.03.2015
 */

public class Error {
    public static final String TAG = AppConstants.PREFIX + Error.class.getSimpleName();
    private int mErrorCode = 0;
    private String mErrorStatus = null;
    private String mErrorMessage = null;
    private String mErrorException = null;

    private int mStoryId = 0;
    private String mFeedURL = null;

    public Error() {

    }

    public Error(int errorCode) {
        mErrorCode = errorCode;
    }

    public Error(String errorMessage) {
        mErrorMessage = errorMessage;
    }

    public int getErrorCode() {
        return mErrorCode;
    }

    public void setErrorCode(int errorCode) {
        mErrorCode = errorCode;
    }

    public String getErrorStatus() {
        return mErrorStatus;
    }

    public void setErrorStatus(String errorStatus) {
        mErrorStatus = errorStatus;
    }

    public String getErrorMessage() {
        if (mErrorMessage != null) {
            return mErrorMessage;
        }
        return null;
    }

    public void setErrorMessage(String errorMessage) {
        mErrorMessage = errorMessage;
    }

    public String getErrorException() {
        return mErrorException;
    }

    public void setErrorException(String errorException) {
        mErrorException = errorException;
    }

    public int getStoryId() {
        return mStoryId;
    }

    public void setStoryId(int storyId) {
        mStoryId = storyId;
    }

    public String getFeedURL() {
        return mFeedURL;
    }

    public void setFeedURL(String feedURL) {
        mFeedURL = feedURL;
    }
}
