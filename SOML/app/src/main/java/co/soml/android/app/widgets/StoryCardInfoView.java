package co.soml.android.app.widgets;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import co.soml.android.app.R;
import co.soml.android.app.controllers.BaseController;
import co.soml.android.app.controllers.ControllerMessage;
import co.soml.android.app.controllers.StoryReaderActions;
import co.soml.android.app.models.Story;
import co.soml.android.app.models.User;
import co.soml.android.app.utils.DateTimeUtils;
import co.soml.android.app.utils.LogUtils;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since May.23.2015
 */
public class StoryCardInfoView extends RelativeLayout {

    public static final String TAG = LogUtils.makeLogTag(StoryCardInfoView.class.getSimpleName());

    public BaseController mStoryCardViewController;
    public Story mStory;
    private StoryCardInfoListener mStoryCardInfoListener;

    public interface StoryCardInfoListener {
        void onStoryCardLoaded(Story story);

        void onStoryCardFailed();
    }

    public StoryCardInfoView(Context context) {
        super(context);
        View storyCardView = LayoutInflater.from(context).inflate(R.layout.story_card_info_view, this, true);
        storyCardView.setId(R.id.story_card_layout_container);
    }

    public StoryCardInfoView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public StoryCardInfoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * Checks the adapted model for this current view has been null or not?
     *
     * @return Boolean Type.
     */
    public boolean isEmpty() {
        return this.mStory == null;
    }

    public BaseController getController() {
        return this.mStoryCardViewController;
    }

    public void setController(BaseController controller) {
        this.mStoryCardViewController = controller;
    }

    /**
     * Show Story Header with info from passed story filled in
     */
    private void showStoryInfo(final Story story, boolean animateIn) {
        // this is the layout containing the blog info views
        final ViewGroup layoutInner = (ViewGroup) findViewById(R.id.story_info_container);
        if (mStory == null) {
            layoutInner.setVisibility(View.INVISIBLE);
            return;
        }
        // Do nothing if story hasn't changed
        if (mStory.isSameAs(story)) {
            return;
        }

        boolean wasEmpty = (mStory == null);
        mStory = story;
        final User author = new User(story.getUser());

        final StoryTextView mByAuthor = (StoryTextView) layoutInner.findViewById(R.id.story_info_by_user);
        final StoryTextView mUserLocation = (StoryTextView) layoutInner.findViewById(R.id.story_info_user_location);
        final StoryTextView mStatusDate = (StoryTextView) layoutInner.findViewById(R.id.story_info_status_date);
        final StoryTextView mStoryTitle = (StoryTextView) layoutInner.findViewById(R.id.story_info_title);

        final CircleImageView mUserProfile = (CircleImageView) layoutInner.findViewById(R.id.story_info_user_profile);

        if (story.hasUserName()) {
            mByAuthor.setText(author.getFullName());
        }

        if (!TextUtils.isEmpty(story.getLocation())) {
            mUserLocation.setText(story.getLocation());
        }

        if (!TextUtils.isEmpty(String.valueOf(story.getCreatedAt()))) {
            mStatusDate.setText(DateTimeUtils.dateToTimeSpan(getContext(), story.getCreatedAt()));
        }

        if (story.hasName()) {
            mStoryTitle.setText(story.getName());
        }
    }

    public void loadStoryInfo(Story story, StoryCardInfoListener listener) {
        mStoryCardInfoListener = listener;
        showStoryInfo(story, false);
        requestStoryInfo(story);
    }

    private void requestStoryInfo(final Story story) {
        // Sending message to server for request a story from server
        mStoryCardViewController.sendMessage(ControllerMessage.REQUEST_TO_SERVER_STORY_DETAIL, null);
    }

    private final StoryReaderActions.UpdateStoryInfoListener mInfoListener = new StoryReaderActions.UpdateStoryInfoListener() {
        @Override
        public void onResult(Story story) {
            if (story != null) {
                boolean animateIn = isEmpty();
                showStoryInfo(story, animateIn);
            } else if (isEmpty() && mStoryCardInfoListener != null) {
                // Only fire the failed event if the StoryInfo is empty
                mStoryCardInfoListener.onStoryCardFailed();
            }
        }
    };
}
