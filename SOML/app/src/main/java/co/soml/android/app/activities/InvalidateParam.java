package co.soml.android.app.activities;

import co.soml.android.app.utils.LogUtils;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since May.05.2015
 */
public class InvalidateParam {
    public static final String TAG = LogUtils.makeLogTag(InvalidateParam.class);
    protected int mMessage;
    protected Object mObj;
	public String mString;
	public int mStoryId;

    public InvalidateParam(int msg) {
        this.mMessage = msg;
    }

	public InvalidateParam(int msg, int storyId) {
		this.mMessage = msg;
		this.mStoryId = storyId;
	}

    public InvalidateParam(int msg, Object obj) {
        this.mMessage = msg;
        this.mObj = obj;
    }

	public InvalidateParam(int msg, String str) {
		this.mMessage = msg;
		this.mString = str;
	}
    public int getMessage() {
        return mMessage;
    }

    public void setMessage(int message) {
        mMessage = message;
    }

    public Object getObj() {
        return mObj;
    }

    public void setObj(Object obj) {
        mObj = obj;
    }

	public String getString() {
		return mString;
	}

	public void setString(String string) {
		mString = string;
	}

	public int getStoryId() {
		return mStoryId;
	}

	public void setStoryId(int storyId) {
		mStoryId = storyId;
	}
}
