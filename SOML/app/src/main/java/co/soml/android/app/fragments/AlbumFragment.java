/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/6/15 3:27 PM.
 **/

package co.soml.android.app.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;

import co.soml.android.app.R;
import co.soml.android.app.activities.adapters.GalleryAdapter;
import co.soml.android.app.camera.CameraManager;
import co.soml.android.app.models.Photo;

public class AlbumFragment extends Fragment {
    private ArrayList<Photo> mPhotos = new ArrayList<Photo>();
    private GridView mAlbumGridViews;

    public AlbumFragment() {
        super();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public static Fragment newInstance(ArrayList<Photo> photos) {
        Fragment fragment = new AlbumFragment();
        Bundle args = new Bundle();
        args.putSerializable("photos", photos);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View root = inflater.inflate(R.layout.layout_album_fragment, null);
        if (getArguments() != null) {
            mPhotos = (ArrayList<Photo>) getArguments().getSerializable("photos");
        }
        mAlbumGridViews = (GridView) root.findViewById(R.id.albums);
        mAlbumGridViews.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Photo photo = mPhotos.get(position);
                CameraManager.getInstance().processPhotoItem(getActivity(), photo);
            }
        });
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        mAlbumGridViews.setAdapter(new GalleryAdapter(getActivity(), mPhotos));
    }
}
