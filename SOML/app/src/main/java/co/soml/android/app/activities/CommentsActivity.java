/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 * @author: Larry Pham (email: larrypham.vn@gmail.com)
 * Date: 6/18/15 9:20 AM
 **/

package co.soml.android.app.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.TypefaceSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import co.soml.android.app.AppConstants;
import co.soml.android.app.R;
import co.soml.android.app.StoryApp;
import co.soml.android.app.activities.adapters.SuggestionAdapter;
import co.soml.android.app.controllers.CommentsController;
import co.soml.android.app.controllers.StoryReaderActions;
import co.soml.android.app.fragments.adapters.StoryCommentsAdapter;
import co.soml.android.app.models.Comment;
import co.soml.android.app.models.CommentList;
import co.soml.android.app.models.Story;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.utils.CommonUtil;
import co.soml.android.app.utils.EditTextUtils;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.utils.NetworkUtils;
import co.soml.android.app.widgets.CircleImageView;
import co.soml.android.app.widgets.ProgressWheel;
import co.soml.android.app.widgets.StoryRecyclerView;
import co.soml.android.app.widgets.StoryTextView;
import co.soml.android.app.widgets.SuggestionAutoCompleteText;

/**
 * The class <code>CommentsActivity</code> represent to visualize the list of comments which other people have been already sent the comments and current
 * user can comment onto this list of stories..
 *
 * The current user will has been reinteracted with this screen, maybe for approving the comments from other or deleting them, we will
 * must declare other screen to manage the comments from other clients reply the current user-post.
 */
public class CommentsActivity extends BaseActivity {

    public static final String TAG = LogUtils.makeLogTag(CommentsActivity.class.getSimpleName());

    public static final String KEY_REPLY_TO_COMMENT_ID = "reply_to_comment_id";
    public static final String KEY_HAS_UPDATED_COMMENTS = "has_updated_comments";
    public static final String ARG_DRAWING_START_LOCATION = "arg_drawing_start_location";

    private Story mStory;
    private long mStoryId;

    private StoryApp mStoryApp;
    private StoryDataModel mDataModel;
    private CommentsController mController;

    private Context mContext;
    private StoryCommentsAdapter mCommentsAdapter;
    private SuggestionAdapter mSuggestionAdapter;

    private StoryRecyclerView mCommentsRecyclerView;
    private ImageView mImageSubmitComment;

    private ViewGroup mCommentBox;
    private View mCommentsEmptyView;

    private boolean mIsUpdatingComments;
    private boolean mHasUpdatedComments;
    private boolean mIsSubmittingComment;
    private long mReplyToCommentId;
    private int mRestorePosition;

    private Toolbar mActionBarToolBar;
    private ProgressWheel mProgress;

    private CommentList mComments;

    @Bind(R.id.comments_author_avatar)
    protected CircleImageView mAuthorAvatar;
    @Bind(R.id.comments_title)
    protected StoryTextView mCommentsTitle;
    @Bind(R.id.story_title)
    protected StoryTextView mStoryTitle;
    @Bind(R.id.recycler_view_comments)
    protected StoryRecyclerView mCommentRecyclerView;
    @Bind(R.id.empty_view)
    protected View mEmptyView;
    @Bind(R.id.progress_loading)
    protected ProgressWheel mLoadingProgress;
    @Bind(R.id.comment_details_bottom)
    protected View mCommentDetailsBottom;

    @Bind(R.id.edit_comment)
    protected SuggestionAutoCompleteText mEditComment;
    @Bind(R.id.image_post_comment)
    protected ImageView mPostComment;
    @Bind(R.id.progress_submit_comment)
    protected ProgressBar mProgressSubmitComment;

    public void setStory(Story story) {
        this.mStory = story;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments_detail);
        ButterKnife.bind(this);

        mContext = this.getApplicationContext();
        mStoryApp = (StoryApp) this.getApplicationContext();
        mDataModel = (StoryDataModel) mStoryApp.getAppDataModel();
        mComments = mDataModel.getComments();
        mController = new CommentsController(CommentsActivity.this, mDataModel);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            if (actionBar.getTitle() != null) {
                SpannableString spanString = new SpannableString(actionBar.getTitle());
                spanString.setSpan(new TypefaceSpan("NotoSans-Regular.ttf"), 0, spanString.length(),
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                actionBar.setTitle(spanString);
            }
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

        if (savedInstanceState != null) {
            mStoryId = savedInstanceState.getLong(AppConstants.STORY_ID);
            mRestorePosition = savedInstanceState.getInt(AppConstants.KEY_RESTORE_POSITION);
            mHasUpdatedComments = savedInstanceState.getBoolean(KEY_HAS_UPDATED_COMMENTS);
        } else {
            mStoryId = getIntent().getLongExtra(AppConstants.STORY_ID, 0);
            if (NetworkUtils.isNetworkAvailable(this)) {

            }
        }
        bindingUI(savedInstanceState);
    }

    public void bindingUI(Bundle savedInstanceState) {
        mProgress = (ProgressWheel) findViewById(R.id.progress_loading);
        mEmptyView = findViewById(R.id.empty_view);
        mCommentsRecyclerView = (StoryRecyclerView) findViewById(R.id.recycler_view_comments);

        int spacingHorizontal = getResources().getDimensionPixelOffset(R.dimen.margin_medium);
        int spacingVertical = getResources().getDimensionPixelOffset(R.dimen.margin_medium);

        mCommentsRecyclerView.addItemDecoration(new StoryRecyclerView.StoryItemDecoration(spacingHorizontal, spacingVertical));

        mCommentBox = (ViewGroup) findViewById(R.id.layout_comment_box);
        mEditComment = (SuggestionAutoCompleteText) mCommentBox.findViewById(R.id.edit_comment);
        mImageSubmitComment = (ImageView) mCommentBox.findViewById(R.id.image_post_comment);
        if (!loadStory()) {
            CoordinatorLayout coordinatorLayout = (CoordinatorLayout) LayoutInflater
                    .from(CommentsActivity.this).inflate(R.layout.layout_coordinator, null);

            finish();
            return;
        }
        mCommentsRecyclerView.setAdapter(getCommentsAdapter());
        if (savedInstanceState != null) {
            setReplyToCommentId(savedInstanceState.getLong(KEY_REPLY_TO_COMMENT_ID));
        }

        refreshComments();
        if (mSuggestionAdapter != null) {
            mEditComment.setAdapter(mSuggestionAdapter);
        }
    }

    public Toolbar getActionBarToolBar() {
        if (mActionBarToolBar == null) {
            mActionBarToolBar = (Toolbar) findViewById(R.id.toolbar_actionbar);
            if (mActionBarToolBar != null) {
                setSupportActionBar(mActionBarToolBar);
                if (getSupportActionBar() != null) {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                    getSupportActionBar().setDisplayShowTitleEnabled(true);
                }
            }
        }
        return mActionBarToolBar;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putLong(AppConstants.STORY_ID, mStoryId);
        outState.putInt(AppConstants.KEY_RESTORE_POSITION, getCurrentPosition());
        outState.putLong(KEY_REPLY_TO_COMMENT_ID, mReplyToCommentId);
        outState.putBoolean(KEY_HAS_UPDATED_COMMENTS, mHasUpdatedComments);
        super.onSaveInstanceState(outState);

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (mActionBarToolBar != null) {
            mActionBarToolBar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View navigationView) {
                    onBackPressed();
                }
            });
        }

        setSupportActionBar(getActionBarToolBar());
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

    public void setReplyToCommentId(long commentId) {
        mReplyToCommentId = commentId;
        mEditComment.setHint(mReplyToCommentId == 0 ? R.string.comment_edittext_hint : 0);
        if (mReplyToCommentId != 0) {
            mEditComment.requestFocus();
            EditTextUtils.showSoftInput(mEditComment);
        }
    }

    public Story getStory() {
        return mStory;
    }

    private boolean loadStory() {
        if (mStory == null) {
            return false;
        }

        final StoryTextView storyTitle = (StoryTextView) findViewById(R.id.story_title);
        final CircleImageView mAvatarImage = (CircleImageView) findViewById(R.id.comments_author_avatar);

        storyTitle.setText(mStory.getName());
        if (mStory.getUser() != null && mStory.getUser().getAvatar() != null) {
            Picasso.with(mContext).load(mStory.getUser().getAvatar().mUrl).placeholder(R.drawable.person_image_empty).into(mAvatarImage);
        }
        if (mStory.mIsPrivate) {
            mCommentBox.setVisibility(View.GONE);
            mEditComment.setEnabled(false);
        } else {
            mCommentBox.setVisibility(View.VISIBLE);
            mEditComment.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_SEND) {
                        submitComment();
                    }
                    return false;
                }
            });
            mImageSubmitComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View submitView) {
                    submitComment();
                }
            });
        }
        return true;
    }

    protected void submitComment() {
        final String commentText = EditTextUtils.getText(mEditComment);

        if (TextUtils.isEmpty(commentText)) {
            return;
        }

        if (!CommonUtil.isNetworkOnline(this)) {
            return;
        }

        mImageSubmitComment.setEnabled(false);
        mEditComment.setEnabled(false);
        mIsSubmittingComment = true;
        final long fakeCommentId = System.currentTimeMillis();
        StoryReaderActions.CommentActionListener actionListener = new StoryReaderActions.CommentActionListener() {
            @Override
            public void onActionResult(boolean succeeded, Comment newComment) {
                if (isFinishing()) {
                    return;
                }

                mIsSubmittingComment = false;
                mImageSubmitComment.setEnabled(true);
                mEditComment.setEnabled(true);

                if (succeeded) {
                    getCommentsAdapter().setHighLightCommentId(0, false);
                    getCommentsAdapter().replaceComment(fakeCommentId, newComment);
                    setReplyToCommentId(0);
                    //mEditComment.getAutoSaveTextHelper().clearSavedText(mEditComment);
                } else {
                    mEditComment.setText(commentText);
                    getCommentsAdapter().removeComment(fakeCommentId);
                    final View coordinatorLayoutView = LayoutInflater.from(CommentsActivity.this)
                            .inflate(R.layout.layout_coordinator,null);
                    Snackbar.make(coordinatorLayoutView, R.string.story_toast_error_comment_failed, Snackbar.LENGTH_LONG).show();
                }
                checkEmptyView();
            }
        };
    }

    private void checkEmptyView() {

        boolean isEmpty = hasCommentsAdapter() && getCommentsAdapter().isEmpty() && !mIsUpdatingComments;

        StoryTextView emptyTitleView = (StoryTextView) mEmptyView.findViewById(R.id.title_empty);
        if (isEmpty && !NetworkUtils.isNetworkAvailable(this)) {
            mEmptyView.setVisibility(View.VISIBLE);
            emptyTitleView.setText(getString(R.string.no_network_message));
        } else if (isEmpty && mHasUpdatedComments) {
            mEmptyView.setVisibility(View.VISIBLE);
            emptyTitleView.setText(R.string.comments_empty_list_content);
        } else {
            mEmptyView.setVisibility(View.GONE);
        }
    }

    private int getCurrentPosition() {
        if (mCommentsRecyclerView != null && hasCommentsAdapter()) {
            return ((LinearLayoutManager) mCommentsRecyclerView.getLayoutManager()).findFirstVisibleItemPosition();
        } else {
            return 0;
        }
    }

    public StoryCommentsAdapter getCommentsAdapter() {
        if (mCommentsAdapter == null) {
            mCommentsAdapter = new StoryCommentsAdapter(CommentsActivity.this, getStory());
            mCommentsAdapter.setReplyListener(new StoryCommentsAdapter.RequestReplyListener() {
                @Override
                public void onRequestReply(long commentId) {
                    setReplyToCommentId(commentId);
                }
            });
            mCommentsAdapter.setDataLoadedListener(new ReaderStoryInterfaces.DataLoadedListener() {
                @Override
                public void onDataLoaded(boolean isEmpty) {
                    if (!isFinishing()) {
                        if (isEmpty || !mHasUpdatedComments) {
                            updateComments(isEmpty, false);
                        } else if (mRestorePosition > 0) {
                            mCommentsRecyclerView.scrollToPosition(mRestorePosition);
                        }
                        mRestorePosition = 0;
                        checkEmptyView();
                    }
                }
            });

            mCommentsAdapter.setDataRequestedListener(new StoryReaderActions.DataRequestedListener() {
                @Override
                public void onRequestedData() {
                    if (!mIsUpdatingComments) {
                        LogUtils.LOGD(TAG, "Story Comments > Requesting NextPage Of Comments");
                        updateComments(true, true);
                    }
                }
            });
        }
        return mCommentsAdapter;
    }

    private void showProgress() {
        if (mProgress != null) {
            mProgress.setVisibility(View.VISIBLE);
        }
    }

    private void hideProgress() {
        if (mProgress != null) {
            mProgress.setVisibility(View.GONE);
        }
    }

    /**
     * Scrolls the passed comment to the top of the listview
     * @param commentId
     */
    private void scrollToCommentId(long commentId) {
        int position = getCommentsAdapter().indexOfCommentId(commentId);
        if (position > -1) {
            mCommentsRecyclerView.scrollToPosition(position);
        }
    }

    public void updateComments(boolean showProgress, boolean requestNextPage) {
        if (mIsUpdatingComments) {
            LogUtils.LOGD(TAG,  "Story Comments > Already Updating Comments");
            return;
        }

        if (!CommonUtil.isNetworkOnline(this)) {
            LogUtils.LOGD(TAG, "Story Comments > No Connection, Update Cancelled");
            return;
        }

        if (showProgress) {
            showProgress();
        }

        // Creating params and using controller to send request for update next page
    }

    private void refreshComments() {
        LogUtils.LOGD(TAG, "Story Comments > RefreshComments");
        getCommentsAdapter().refreshComments();
    }

    private boolean hasCommentsAdapter() {
        return (mCommentsAdapter != null);
    }

    protected boolean validateComment() {
        if (TextUtils.isEmpty(mEditComment.getText())) {
            mImageSubmitComment.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake_error));
            return false;
        }
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    @Override
    protected void invalidate() {

    }

    @Override
    protected void invalidate(Object... params) {

    }
}
