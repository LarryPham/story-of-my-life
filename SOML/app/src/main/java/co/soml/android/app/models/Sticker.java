package co.soml.android.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

import co.soml.android.app.utils.LogUtils;

public class Sticker implements Parcelable, Serializable {

	public static final String TAG = LogUtils.makeLogTag(Sticker.class.getSimpleName());

	@Expose
	@SerializedName("id")
	private long mId;

	@Expose
	@SerializedName("image")
	private String mImage;

	public long getId() {
		return mId;
	}

	public void setId(long id) {
		mId = id;
	}

	public String getImage() {
		return mImage;
	}

	public void setImage(String image) {
		mImage = image;
	}

	public Sticker() {

	}

	public Sticker(Sticker sticker) {
		this.mId = sticker.getId();
		this.mImage = sticker.getImage();
	}

	public Sticker(long id, String imageURI) {
		this.mId = id;
		this.mImage = imageURI;
	}

	public Sticker(Parcel source) {
		readFromParcel(source);
	}

	public void readFromParcel(Parcel source) {
		this.mId = source.readInt();
		this.mImage = source.readString();
	}

	public boolean isSameSticker(Sticker sticker) {
		if (sticker == null) {
			return false;
		}

		if (this.mId != sticker.getId()) {
			return false;
		}

		if (!this.mImage.equalsIgnoreCase(sticker.getImage())) {
			return false;
		}

		return true;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(this.mId);
		dest.writeString(this.mImage);
	}

	public static final Creator<Sticker> CREATOR = new Creator<Sticker>() {
		@Override
		public Sticker createFromParcel(Parcel source) {
			return new Sticker(source);
		}

		@Override
		public Sticker[] newArray(int size) {
			return new Sticker[size];
		}
	};
}
