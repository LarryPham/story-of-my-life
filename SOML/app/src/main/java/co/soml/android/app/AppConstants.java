package co.soml.android.app;

import android.os.Environment;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.01.2015
 */
public class AppConstants {

    public static final String PREFIX = "[SOMLApp].";
    public static final int DEBUG_MODE = 1;
    public static final int HASH_CODE_FOR_IMAGE = 100;
    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
 /* public static final String TWITTER_KEY = "lWV1V4ilkYiXDODvhu61jybsc";
  public static final String TWITTER_SECRET = "yK52fc43NT9ZULaeNrFYC2x8DBNQYu1RWRJn1H6fceM5RhJ4LJ";*/
    // Old key
    public static final String TWITTER_KEY = "0KVjV1jphbAVptO0YZhcNqcex";
    public static final String TWITTER_SECRET = "DyhXrrQxxEM1FE40B9wYufStQlnwVq3zviqXgsBKTWun5HEzxK";
    // Using the base url for connecting the client app to server and then getting the data from them
    public static final String BASE_URL = "https://staging.soml.co:443/api/";

    public static final String EMAIL_ACCOUNT_TYPE = "email";
    public static final String FACEBOOK_ACCOUNT_TYPE = "facebook";
    public static final String TWITTER_ACCOUNT_TYPE = "twitter";
    public static final String GUEST_ACCOUNT_TYPE = "guest";

    public static final int CONNECTION_TIMEOUT = 3000;
    public static final int READ_TIMEOUT = 3000;

    public static final int CONN_WIFI = 1;
    public static final int CONN_3G = 2;
    public static final int CONN_DISCONNECTED = 3;

    public static final int SUCCESSFULLY = 200;
    public static final int CREATED = 201;
    public static final int NOT_AUTHORIZED = 401;
    public static final int NO_CONTENT = 204;
    public static final int NOT_FOUND = 404;
    public static final int MISSING_PARAMS = 422;
    public static final int FORBIDDEN = 403;
    public static final String FACEBOOK_APP_ID = "1517177621888975";
    public static final boolean IS_DOGFOOD_BUILD = true;
    public static final int MAX_STORY_LIST_ITEMS_TO_DISPLAY = 200;

    public static final String TITLE = "title";
    public static final String STORY_ID =  "StoryID";
    public static final String KEY_RESTORE_POSITION = "RestoredPosition";

    public static final String MAIN_PAGE_VIEW_STATE = "MainViewState";
    public static final String MAIN_PAGE_VIEW_TYPE = "MainPageViewType";

    // Headers Params for request something
    public static final String X_API_VERSION = "X_API_VERSION";
    public static final String X_API_TOKEN = "X_API_TOKEN";

    public static final String X_AUTH_TOKEN = "X_AUTH_TOKEN";
    public static final String USER_ID = "UserId";

    public static final String SCOPE_NAME = "ScopeName";
    public static final String START_INDEX = "StartIndex";
    public static final String LIMIT = "Limit";
    public static final String STATE = "State";

    public static final String STORY_POST_LIST_TYPE = "StoryFeedType";
    public static final String COLLECTIONS_VIEW_STATE = "CollectionsViewState";

    public static final String SIGNIN_FRAGMENT_KEY = "co.soml.android.app.fragments.SigninFragment";
    public static final String SIGNUP_FRAGMENT_KEY = "co.soml.android.app.fragments.NewUserFragment";
    public static final String SOCIAL_SIGNIN_FRAGMENT_KEY = "co.soml.android.app.fragments.SocialSigninFragment";


    public static final String CRASHLYTICS_KEY_THEME = "theme";
    public static final String CRASHLYTICS_KEY_SESSION_ACTIVATED = "session_activated";
    public static final String CRASHLYTICS_KEY_CRASHES = "are_crashes_enabled";

    // Request code for crop activity.
    public static final int REQUEST_CROP = 6709;
    public static final int REQUEST_PICK = 9162;
    public static final int RESULT_ERROR = 404;

    public static final int ACTION_EDIT_LABEL = 8080;
    public static final int ACTION_EDIT_LABEL_POI = 9090;

    public static final String PARAM_EDIT_TEXT = "PARAM_EDIT_TEXT";
    public static final String PARAM_MAX_SIZE = "PARAM_MAX_SIZE";
    public static final float DEFAULT_PIXEL = 1242;

    public static final int POST_TYPE_POI = 1;
    public static final int POST_TYPE_TAG = 0;
    public static final int POST_TYPE_DEFAULT = 0;

    public static final String APP_DIR = Environment.getExternalStorageDirectory() + "/StoryApp";
    public static final String APP_TEMP = APP_DIR + "/temp";
    public static final String APP_IMAGES = APP_DIR + "/images";

}
