package co.soml.android.app.utils;


import android.net.Uri;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;
import android.webkit.URLUtil;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.IDN;
import java.net.URI;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.Charset;

public class UrlUtils {

	public static final String TAG = LogUtils.makeLogTag(UrlUtils.class.getSimpleName());

	public static String urlEncode(final String text) {
		try {
			return URLEncoder.encode(text, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			return text;
		}
	}

	public static String urlDecode(final String text) {
		try {
			return URLDecoder.decode(text, "UTF-8");
		} catch (UnsupportedEncodingException ex) {
			return text;
		}
	}

	public static String getDomainFromUrl(final String urlString) {
		if (urlString != null) {
			Uri uri = Uri.parse(urlString);
			if (uri.getHost() != null) {
				return uri.getHost();
			}
		}
		return "";
	}

	public static String convertUrlToPunycodeIfNeeded(String url) {
		if (!Charset.forName("US-ASCII").newEncoder().canEncode(url)) {
			if (url.toLowerCase().startsWith("http://")) {
				url = "http://" + IDN.toASCII(url.substring(7));
			} else if (url.toLowerCase().startsWith("https://")) {
				url = "https://" + IDN.toASCII(url.substring(8));
			} else {
				url = IDN.toASCII(url);
			}
		}
		return url;
	}

	public static String removeLeadingDoubleSlash(String url, String scheme) {
		if (url != null && url.startsWith("//")) {
			url = url.substring(2);
			if (scheme != null) {
				if (scheme.endsWith("://")) {
					url = scheme + url;
				} else {
					LogUtils.LOGE(TAG, "Invalid scheme used: " + scheme);
				}
			}
		}
		return url;
	}

	public static String addUrlSchemeIfNeeded(String url, boolean isHttps) {
		if (url == null) {
			return null;
		}

		url = removeLeadingDoubleSlash(url, isHttps ? "https" : "htt") + "://";
		if (!URLUtil.isValidUrl(url)) {
			if (!(url.toLowerCase().startsWith("http://")) && !(url.toLowerCase().startsWith("https://"))) {
				url = (isHttps ? "https" : "http") + "://" + url;
			}
		}
		return url;
	}

	public static String normalizeUrl(final String urlString) {
		if (urlString == null) {
			return null;
		}

		if (urlString.startsWith("http")) {
			if (urlString.endsWith("/")) {
				return urlString.substring(0, urlString.length() - 1);
			}
			return urlString;
		}

		try {
			URI uri = URI.create(urlString);
			return uri.normalize().toString();
		} catch (IllegalArgumentException ex) {
			return urlString;
		}
	}

	public static String removeQuery(final String urlString) {
		if (urlString == null) {
			return null;
		}

		int pos = urlString.indexOf("?");
		if (pos == -1) {
			return urlString;
		}
		return urlString.substring(0, pos);
	}

	public static boolean isHttps(final String urlString) {
		return (urlString != null && urlString.startsWith("https:"));
	}

	public static String makeHttps(final String urlString) {
		if (urlString == null || !urlString.startsWith("http:")) {
			return urlString;
		}
		return "https:" + urlString.substring(5, urlString.length());
	}

	public static String getUrlMimeType(final String urlString) {
		if (urlString == null) {
			return null;
		}

		String extension = MimeTypeMap.getFileExtensionFromUrl(urlString);
		if (extension == null) {
			return null;
		}

		MimeTypeMap mime = MimeTypeMap.getSingleton();
		String mimeType = mime.getMimeTypeFromExtension(extension);
		if (mimeType == null) {
			return null;
		}
		return mimeType;
	}

	public static boolean isValidUrlAndHostNotNull(String url) {
		try {
			URI uri = URI.create(url);
			if (uri.getHost() == null) {
				return false;
			}
		} catch (IllegalArgumentException ex) {
			return false;
		}
		return true;
	}

	public static boolean isImageUrl(String url) {
		if (TextUtils.isEmpty(url)) return false;
		String cleanedUrl = removeQuery(url.toLowerCase());
		return cleanedUrl.endsWith("jpg") || cleanedUrl.endsWith("jpeg") || cleanedUrl.endsWith("gif") || cleanedUrl.endsWith("png");
	}
}
