/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 7/21/15 9:15 AM.
 **/

package co.soml.android.app.widgets;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Interpolator;

import co.soml.android.app.utils.LogUtils;

public class RevealBackgroundView extends View {
    public static final String TAG = LogUtils.makeLogTag(RevealBackgroundView.class.getSimpleName());

    public static final int STATE_NOT_STARTED = 0;
    public static final int STATE_FILL_STARTED = 1;
    public static final int STATE_FINISHED = 2;

    private static final Interpolator INTERPOLATOR = new AccelerateInterpolator();
    private static final int FILL_TIME = 400;
    private int mState = STATE_NOT_STARTED;

    private Paint mFillPaint;
    private int mCurrentRadius;
    ObjectAnimator mRevealAnimator;

    private int mStartLocationX;
    private int mStartLocationY;
    private OnStateChangeListener mOnStateChangeListener;

    public static interface OnStateChangeListener {
        void onStateChanged(int state);
    }

    public void setOnStateChangedListener(OnStateChangeListener onStateChangedListener) {
        this.mOnStateChangeListener = onStateChangedListener;
    }

    public OnStateChangeListener getOnStateChangeListener() {
        return this.mOnStateChangeListener;
    }

    public void setCurrentRadius(int radius) {
        this.mCurrentRadius = radius;
        invalidate();
    }

    public RevealBackgroundView(Context context) {
        super(context);
        init();
    }

    public RevealBackgroundView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RevealBackgroundView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public RevealBackgroundView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void changeState(int state) {
        if (this.mState == state) {
            return;
        }
        this.mState = state;
        if (mOnStateChangeListener != null) {
            mOnStateChangeListener.onStateChanged(state);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (mState == STATE_FINISHED) {
            canvas.drawRect(0, 0, getWidth(), getHeight(), mFillPaint);
        } else {
            canvas.drawCircle(mStartLocationX, mStartLocationY, mCurrentRadius, mFillPaint);
        }
    }

    public void setToFinishedTime() {
        changeState(STATE_FINISHED);
        invalidate();
    }

    public void startFromLocation(int[] tapLocationOnScreen) {
        changeState(STATE_FILL_STARTED);
        mStartLocationX = tapLocationOnScreen[0];
        mStartLocationY = tapLocationOnScreen[1];
        mRevealAnimator = ObjectAnimator.ofInt(this, "mCurrentRadius", 0, getWidth() + getHeight()).setDuration(FILL_TIME);
        mRevealAnimator.setInterpolator(INTERPOLATOR);
        mRevealAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                changeState(STATE_FINISHED);
            }
        });
        mRevealAnimator.start();
    }

    public void init() {
        this.mFillPaint = new Paint();
        mFillPaint.setStyle(Paint.Style.FILL);
        mFillPaint.setColor(Color.WHITE);
    }

    public void setFillPaintColor(int color) {
        mFillPaint.setColor(color);
    }
}
