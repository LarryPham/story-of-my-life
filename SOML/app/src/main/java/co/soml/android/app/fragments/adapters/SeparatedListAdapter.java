package co.soml.android.app.fragments.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;

import java.util.LinkedHashMap;
import java.util.Map;

import co.soml.android.app.R;
import co.soml.android.app.utils.LogUtils;

/**
 *  Copyright (C) 2015 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation
 *  are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied,
 *  reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior
 *  written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with respect to the contents, and
 *  assumes no responsibility for any errors that might appear in the software and documents. This publication and the
 *  contents hereof are subject to change without notice.
 *  @author Larry Pham(Email: larrypham.vn@gmail.com)
 *  @since 6/26/15 2:26 PM
 **/

public class SeparatedListAdapter extends BaseAdapter {
    public static final String TAG = LogUtils.makeLogTag(SeparatedListAdapter.class.getSimpleName());

    public final Map<String, Adapter> mSections = new LinkedHashMap<String,Adapter>();
    public final ArrayAdapter<String> mHeaders;
    public final static int TYPE_SECTION_HEADER = 0;

    /**
     * Constructor of <code>SeparatedListAdapter</code>
     * @param context The {@link Context} to use.
     */
    public SeparatedListAdapter(final Context context) {
        mHeaders = new ArrayAdapter<String>(context, R.layout.people_list_headers);
    }

    @Override
    public int getCount() {
        int total = 1;
        for (final Adapter adapter: mSections.values()) {
            total += adapter.getViewTypeCount();
        }
        return total;
    }

    @Override
    public Object getItem(int position) {
        for (final Object section : mSections.keySet()) {
            final Adapter adapter = mSections.get(section);
            final int size = adapter.getCount() + 1;

            if (position == 0) {
                return section;
            }

            if (position < size) {
                return adapter.getItem(position - 1);
            }

            position -= size;
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        int total = 1;
        for (final Adapter adapter : mSections.values()) {
            total += adapter.getViewTypeCount();
        }
        return total;
    }

    @Override
    public int getItemViewType(int position) {
        int type = 1;
        for (final Object section : mSections.keySet()) {
            final Adapter adapter = mSections.get(section);
            final int size = adapter.getCount() + 1;

            if (position == 0) {
                return TYPE_SECTION_HEADER;
            }

            if (position <= size) {
                return type + adapter.getItemViewType(position - 1);
            }

            position -= size;
            type += adapter.getViewTypeCount();
        }
        return -1;
    }

    public boolean areAllItemSelected() {
        return false;
    }

    @Override
    public boolean isEnabled(int position) {
        return getItemViewType(position) != TYPE_SECTION_HEADER;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int sectionNum = 0;
        for (final Object section: mSections.keySet()) {
            final Adapter adapter = mSections.get(section);
            final int size = adapter.getCount() + 1;

            if (position == 0) {
                return mHeaders.getView(sectionNum, convertView, parent);
            }

            if (position < size) {
                return adapter.getView(position - 1, convertView, parent);
            }

            position -= size;
            sectionNum++;
        }
        return null;
    }

    public void addSection(final String section, final Adapter adapter) {
        mHeaders.add(section);
        mSections.put(section, adapter);
    }
}
