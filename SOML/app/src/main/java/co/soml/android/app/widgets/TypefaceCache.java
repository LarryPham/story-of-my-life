package co.soml.android.app.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import java.util.Hashtable;

import co.soml.android.app.R;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since May.08.2015
 * <p>
 * Using an instance of <code>TypefaceCache</code> to store the customized cached typeface for TextView or customise buttons when we want
 * to visualize some components into current app.
 */
public class TypefaceCache {
    public static final int VARIATION_NORMAL = 0;
    public static final int VARIATION_LIGHT = 1;
    public static final int VARIATION_SPECIAL = 2;
    public static final int VARIATION_THIN = 3;
    public static final int VARIATION_MEDIUM = 4;

    public static final int VARIATION_CONDENSED = 5;
    public static final int VARIATION_BLACK = 6;
    public static final int VARIATION_EXTRA_LIGHT= 7;
    public static final int VARIATION_SEMI = 8;

    private static final Hashtable<String, Typeface> mTypefaceCache = new Hashtable<String, Typeface>();

    public static Typeface getTypeface(Context context) {
        return getTypeface(context, Typeface.NORMAL, VARIATION_NORMAL);
    }

    public static Typeface getTypeface(Context context, int fontStyle, int variation) {
        if (context == null) {
            return null;
        }

        String typefaceName = "SourceSansPro-Regular.ttf";
        if (variation == VARIATION_LIGHT) {
            switch (fontStyle) {
                case Typeface.ITALIC:
                    typefaceName = "SourceSansPro-LightIt.ttf";
                    break;
                default:
                    typefaceName = "SourceSansPro-Light.ttf";
                    break;
            }
        } else if (variation == VARIATION_CONDENSED) {
            switch (fontStyle) {
                case Typeface.BOLD: {
                    typefaceName = "RobotoCondensed-Bold.ttf";
                    break;
                }
                case Typeface.BOLD_ITALIC: {
                    typefaceName = "RobotoCondensed-BoldItalic.ttf";
                    break;
                }
                case Typeface.ITALIC: {
                    typefaceName = "RobotoCondensed-Italic.ttf";
                    break;
                }
                default: {
                    typefaceName = "RobotoCondensed-Regular.ttf";
                    break;
                }
            }
        } else if (variation == VARIATION_THIN) {
            switch (fontStyle) {
                case Typeface.ITALIC: {
                    typefaceName = "Roboto-ThinItalic.ttf";
                    break;
                }
                default: {
                    typefaceName = "Roboto-Thin.ttf";
                    break;
                }
            }
        } else if (variation == VARIATION_MEDIUM) {
            switch (fontStyle) {
                case Typeface.ITALIC: {
                    typefaceName = "Roboto-MediumItalic.ttf";
                    break;
                }
                default: {
                    typefaceName = "Roboto-Medium.ttf";
                    break;
                }
            }
        } else if (variation == VARIATION_SPECIAL) {
            switch (fontStyle) {
                case Typeface.ITALIC: {
                    typefaceName = "SourceSansPro-It.ttf";
                    break;
                }
                case Typeface.BOLD_ITALIC: {
                    typefaceName = "SourceSansPro-BoldIt.ttf";
                    break;
                }
                case Typeface.BOLD: {
                    typefaceName = "SourceSansPro-Bold.ttf";
                    break;
                }
                case Typeface.NORMAL: {
                    typefaceName = "SlaboSmaller-Regular.ttf";
                    break;
                }
            }
        } else if (variation == VARIATION_BLACK) {
            switch (fontStyle) {
                case Typeface.NORMAL: {
                    typefaceName = "SourceSansPro-Black.ttf";
                    break;
                }
                case Typeface.ITALIC: {
                    typefaceName = "SourceSansPro-BlackIt.ttf";
                    break;
                }
            }
        } else if (variation == VARIATION_EXTRA_LIGHT) {
            switch (fontStyle) {
                case Typeface.NORMAL: {
                    typefaceName = "SourceSansPro-ExtraLight.ttf";
                    break;
                }
                case Typeface.ITALIC: {
                    typefaceName = "SourceSansPro-ExtraLightIt.ttf";
                    break;
                }
            }
        } else if (variation == VARIATION_EXTRA_LIGHT) {
            switch (fontStyle) {
                case Typeface.BOLD: {
                    typefaceName = "SourceSansPro-Semibold.ttf";
                    break;
                }
                case Typeface.BOLD_ITALIC: {
                    typefaceName = "SourceSansPro-SemiboldIt.ttf";
                }
            }
        } else {
            switch (fontStyle) {
                case Typeface.NORMAL: {
                    typefaceName = "SlaboBigger-Regular.ttf";
                    break;
                }
                case Typeface.BOLD_ITALIC: {
                    typefaceName = "SourceSansPro-BoldIt.ttf";
                    break;
                }
                case Typeface.ITALIC: {
                    typefaceName = "SourceSansPro-It.ttf";
                    break;
                }
                case Typeface.BOLD: {
                    typefaceName = "SourceSansPro-Bold.ttf";
                    break;
                }
            }
        }
        return getTypefaceForTypefaceName(context, typefaceName);
    }

    protected static Typeface getTypefaceForTypefaceName(Context context, String typefaceName) {
        if (!mTypefaceCache.containsKey(typefaceName)) {
            Typeface typeface = Typeface.createFromAsset(context.getApplicationContext().getAssets(), "fonts/" + typefaceName);
            if (typeface != null) {
                mTypefaceCache.put(typefaceName, typeface);
            }
        }
        return mTypefaceCache.get(typefaceName);
    }

    /**
     * Sets the typeface for a TextView (or TextView descendant such as EditText or Button) based on the passed attributes, defaults to
     * normal typeface
     */
    public static void setCustomTypeface(Context context, TextView view, AttributeSet attributeSet) {
        if (context == null || view == null) {
            return;
        }
        // Skip at design-time (edit mode)
        if (view.isInEditMode()) {
            return;
        }
        // read custom fontVariation from attributes, default to normal
        int variation = TypefaceCache.VARIATION_NORMAL;
        if (attributeSet != null) {
            TypedArray array = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.StoryTextView, 0, 0);
            if (array != null) {
                try {
                    variation = array.getInteger(R.styleable.StoryTextView_fontVariation, TypefaceCache.VARIATION_NORMAL);
                } finally {
                    array.recycle();
                }
            }
        }

        final int fontStyle;
        if (view.getTypeface() != null) {
            boolean isBold = view.getTypeface().isBold();
            boolean isItalic = view.getTypeface().isItalic();
            if (isBold && isItalic) {
                fontStyle = Typeface.BOLD_ITALIC;
            } else if (isBold) {
                fontStyle = Typeface.BOLD;
            } else if (isItalic) {
                fontStyle = Typeface.ITALIC;
            } else {
                fontStyle = Typeface.NORMAL;
            }
        } else {
            fontStyle = Typeface.NORMAL;
        }

        Typeface typeface = getTypeface(context, fontStyle, variation);
        if (typeface != null) {
            view.setTypeface(typeface);
        }
    }
}
