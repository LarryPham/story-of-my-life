package co.soml.android.app.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.20.2015
 * <p>
 * A simple {@link android.widget.TextView} subclass that uses {@link android.text.TextUtils#ellipsize(CharSequence,
 * android.text.TextPaint, float, android.text.TextUtils.TruncateAt)} to truncate the displayed text.
 */
public class EllipsizedTextView extends TextView {

    private static final int MAX_ELLIPSIZE_LINES = 100;
    private int mMaxLines;

    public EllipsizedTextView(Context context) {
        super(context);
    }

    public EllipsizedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EllipsizedTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        // Attributes initialization
        final TypedArray array = context.obtainStyledAttributes(attrs, new int[]{android.R.attr.maxLines, defStyleAttr, 0});
        mMaxLines = array.getInteger(0, 1);
        array.recycle();
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        CharSequence newText = getWidth() == 0 || mMaxLines > MAX_ELLIPSIZE_LINES ? text :
                TextUtils.ellipsize(text, getPaint(), getWidth() * mMaxLines, TextUtils.TruncateAt.END, false, null);
        super.setText(text, type);
    }

    @Override
    protected void onSizeChanged(int width, int height, int oldWidth, int oldHeight) {
        super.onSizeChanged(width, height, oldWidth, oldHeight);
        if (width > 0 && oldWidth != width) {
            setText(getText());
        }
    }

    @Override
    public void setMaxLines(int maxLines) {
        super.setMaxLines(maxLines);
        mMaxLines = maxLines;
    }
}
