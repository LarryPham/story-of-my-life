package co.soml.android.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Toast;

import co.soml.android.app.R;
import co.soml.android.app.StoryApp;
import co.soml.android.app.controllers.ActivityHandler;
import co.soml.android.app.controllers.ControllerMessage;
import co.soml.android.app.controllers.SessionController;
import co.soml.android.app.models.JsonUser;
import co.soml.android.app.models.StoryBean;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.models.User;
import co.soml.android.app.services.StoryService;
import co.soml.android.app.services.UserResult;
import co.soml.android.app.services.tasks.Action;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.utils.PreferenceUtils;
import co.soml.android.app.widgets.ProgressWheel;
import co.soml.android.app.widgets.StoryPanningView;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.23.2015
 */
public class WelcomeActivity extends Activity {
    public static final String TAG = LogUtils.makeLogTag(WelcomeActivity.class);
    // This params will be sent to SessionActivity for navigating this screen as Signin or just pass-away this step
    static final String SESSIONS_TYPE_PARAMS = "SessionsParams";
    static final String SESSIONS_FRAGMENT_PAGE = "SessionPage";

    static final String SESSIONS_SIGNIN_TYPE_PARAMS = "SignIn";
    static final String SESSIONS_ANONYMOUS_PARAMS = "Anonymous";

    private SessionController mController;
    private StoryDataModel mDataModel;
    private StoryApp mStoryApp;
    private User mAnonymousUser;
    protected WelcomeActivityHandler mHandler = new WelcomeActivityHandler();
    protected ProgressWheel mProgressWheel;
    protected StoryPanningView mBackgroundView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_introduction);
        mStoryApp = (StoryApp) getApplicationContext();
        mDataModel = mStoryApp.getAppDataModel();
        mStoryApp.addHandler(mHandler);
        mBackgroundView = (StoryPanningView) findViewById(R.id.introduction_background);
        mBackgroundView.startPanning();
    }

    @Override
    protected void onResume() {
        super.onResume();

        findViewById(R.id.button_explore_content).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(WelcomeActivity.this, SessionsActivity.class);
                // Assigning the intent with bundle for navigating the welcome to signin screen
                final Bundle signinBundle = new Bundle();
                signinBundle.putString(SESSIONS_FRAGMENT_PAGE, SessionsActivity.SOCIAL_FRAGMENT_PAGE);
                intent.putExtra(SESSIONS_TYPE_PARAMS, signinBundle);
                PreferenceUtils.markIntroductionDone(WelcomeActivity.this);
                intent.putExtras(signinBundle);
                startActivity(intent);
                finish();
            }
        });

        findViewById(R.id.button_take_look).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Message signInGuestMessage = new Message();
                signInGuestMessage.what = ControllerMessage.REQUEST_TO_SERVER_SIGN_IN_GUEST;
                handleMessage(signInGuestMessage);
            }
        });
        mProgressWheel = (ProgressWheel) findViewById(R.id.anonymous_progress_wheel);
        endProgress();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        String fn = "onDestroy(): ";
        LogUtils.LOGD(TAG, fn + String.format("removeHandler"));
        if (mBackgroundView != null) {
            mBackgroundView.stopPanning();
        }
        mStoryApp.removeHandler(mHandler);
    }

    protected void startProgress() {
        mProgressWheel.setVisibility(View.VISIBLE);
        mProgressWheel.setEnabled(true);
    }

    protected void endProgress() {
        mProgressWheel.setVisibility(View.GONE);
        mProgressWheel.setEnabled(false);
    }

    protected void stopProgressWheel() {
        final Handler uiHandler = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (mProgressWheel != null && mProgressWheel.isSpinning()) {
                    mProgressWheel.stopSpinning();
                }
            }
        };
        uiHandler.postDelayed(runnable, 3000);
    }

    protected Intent obtainIntent(int msg) {
        final Intent intent = new Intent(WelcomeActivity.this, StoryService.class);
        intent.putExtra(Action.REQUEST_OWNER, this.hashCode());
        intent.putExtra(Action.REQUEST_MSG, msg);
        return intent;
    }

    protected boolean checkOwner(Message msg) {
        if (msg.arg1 == this.hashCode()) {
            return true;
        } else {
            LogUtils.LOGD(TAG, String.format("[Controller] checkOwner false [%d:%d]", msg.arg1, this.hashCode()));
            return false;
        }
    }

    protected void handleMessage(Message msg) {
        switch (msg.what) {
            case ControllerMessage.REQUEST_TO_SERVER_SIGN_IN_GUEST: {
                // Sending message for signing in the anonymous user account
                SessionController.AuthenticateReqParam reqParam = new SessionController.AuthenticateReqParam();
                final Intent intent = obtainIntent(ControllerMessage.REQUEST_TO_SERVER_SIGN_IN_GUEST);
                intent.setAction(Action.REQUEST_TO_SERVER_AUTHENTICATE_GUEST);
                // Running the thread for displaying the progress bar and signning to server by guest account
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        startProgress();
                        mProgressWheel.spin();
                        WelcomeActivity.this.startService(intent);
                    }
                });
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_SIGN_IN_GUEST_COMPLETED: {
                if (checkOwner(msg)) {
                    LogUtils.LOGD(TAG, String.format("Unknown sign-in guest completed - %d", msg.what));
                    final InvalidateParam param = new InvalidateParam(msg.what, msg.obj);
                    invalidate(param);
                }
                break;
            }
            // Unknown host issues when login to server by using anonymous user account
            case ControllerMessage.REQUEST_ERROR_REASON_UNKNOWN_HOST: {
                if (checkOwner(msg)) {
                    LogUtils.LOGD(TAG, String.format("Unknown host supports - %d", msg.what));
                    final InvalidateParam param = new InvalidateParam(msg.what);
                    invalidate(param);
                }
                break;
            }
            // Case: failed to connecting to server
            case ControllerMessage.REQUEST_ERROR_REASON_HTTP_CONNECT_FAIL: {
                if (checkOwner(msg)) {
                    LogUtils.LOGD(TAG, String.format("Raising issue: failed to connect to server -%d", msg.what));
                    final InvalidateParam param = new InvalidateParam(msg.what);
                    invalidate(param);
                }
                break;
            }
            // Case: network disconnected
            case ControllerMessage.NETWORK_DISCONNECT: {
                if (checkOwner(msg)) {
                    LogUtils.LOGD(TAG, String.format("Raising issue: network disconnected - %d", msg.what));
                    final InvalidateParam param = new InvalidateParam(msg.what);
                    invalidate(param);
                }
                break;
            }
        }
    }

    /**
     * Method invalidate(<params>params</params>) which will used to invalidate the current screen that have changed something into this
     * layout and workflows will be notified to acitivyt component.
     *
     * @param params The InvalidateParam objects.
     */
    protected void invalidate(Object... params) {
        InvalidateParam param = (InvalidateParam) params[0];
        if (param.getMessage() == ControllerMessage.REQUEST_ERROR_REASON_UNKNOWN_HOST) {
            stopProgressWheel();
            Toast.makeText(this, getString(R.string.msg_error_unknown_host), Toast.LENGTH_LONG).show();
            endProgress();
        } else if (param.getMessage() == ControllerMessage.REQUEST_ERROR_REASON_HTTP_CONNECT_FAIL) {
            mProgressWheel.stopSpinning();
            LogUtils.LOGD(TAG, "REQUEST_ERROR_REASON_HTTP_CONNECT_FAIL");
            Toast.makeText(this, getString(R.string.msg_error_connection_failed), Toast.LENGTH_LONG).show();
            endProgress();
        } else if (param.getMessage() == ControllerMessage.NETWORK_DISCONNECT) {
            stopProgressWheel();
            LogUtils.LOGD(TAG, "NETWORK_DISCONNECT");
            Toast.makeText(this, getString(R.string.msg_error_network_disconnected), Toast.LENGTH_LONG).show();
            endProgress();
        } else if (param.getMessage() == ControllerMessage.REQUEST_TO_SERVER_SIGN_IN_GUEST_COMPLETED) {
            mProgressWheel.stopSpinning();
            LogUtils.LOGD(TAG, "REQUEST_TO_SERVER_SIGN_IN_GUEST_COMPLETED");
            UserResult result = (UserResult) param.getObj();
            LogUtils.LOGD(TAG, String.format("Saving the received user[%s]", result.getUser().getFullName()));
            if (result.getUser() != null) {
                final User currentUser = new User(result.getUser());
                StoryApp.getInstance().setCurrentUser(currentUser);
                StoryBean.getInstance().setStoryDataModel(mDataModel);
            }
            endProgress();
            PreferenceUtils.markIntroductionDone(WelcomeActivity.this);
            final Intent collectionsIntent = new Intent(WelcomeActivity.this, StoryMainActivity.class);
            final Bundle collectionParams = new Bundle();
            collectionParams.putParcelable("CURRENT_USER", mStoryApp.getCurrentUser());
            collectionsIntent.putExtras(collectionParams);
            startActivity(collectionsIntent);
            finish();
        }
    }

    public class WelcomeActivityHandler extends ActivityHandler {
        @Override
        public void callBack(Message msg) {
            handleMessage(msg);
        }
    }
}
