/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/19/15 10:18 AM.
 **/

package co.soml.android.app.cells;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.ImageView;

import co.soml.android.app.StoryApp;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.utils.UIUtils;
import co.soml.android.app.widgets.FixedFrameLayout;
import co.soml.android.app.widgets.StoryTextView;
import co.soml.android.app.widgets.TypefaceCache;

public class PhotoEditToolCell extends FixedFrameLayout {

    public static final String TAG = LogUtils.makeLogTag(PhotoEditToolCell.class.getSimpleName());

    private ImageView mIconImageView;
    private StoryTextView mNameTextView;
    private StoryTextView mValueTextView;

    public PhotoEditToolCell(Context context) {
        super(context);
        mIconImageView = new ImageView(context);
        mIconImageView.setScaleType(ImageView.ScaleType.CENTER);
        addView(mIconImageView);

        LayoutParams layoutParams = (LayoutParams) mIconImageView.getLayoutParams();
        layoutParams.width = LayoutParams.MATCH_PARENT;
        layoutParams.height = LayoutParams.MATCH_PARENT;

        layoutParams.bottomMargin = UIUtils.px2dp(context, 12);
        mIconImageView.setLayoutParams(layoutParams);

        mNameTextView = new StoryTextView(context);
        mNameTextView.setGravity(Gravity.CENTER);
        mNameTextView.setTextColor(0xffffffff);
        mNameTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
        mNameTextView.setMaxLines(1);
        mNameTextView.setSingleLine(true);
        mNameTextView.setEllipsize(TextUtils.TruncateAt.END);
        addView(mNameTextView);

        layoutParams = (LayoutParams) mNameTextView.getLayoutParams();
        layoutParams.width = LayoutParams.MATCH_PARENT;
        layoutParams.height = LayoutParams.WRAP_CONTENT;
        layoutParams.gravity = Gravity.LEFT | Gravity.BOTTOM;

        layoutParams.leftMargin = UIUtils.px2dp(context, 4);
        layoutParams.rightMargin = UIUtils.px2dp(context, 4);
        mNameTextView.setLayoutParams(layoutParams);

        mValueTextView = new StoryTextView(context);
        mValueTextView.setTextColor(Color.parseColor("#585858"));
        mValueTextView.setTypeface(TypefaceCache.getTypeface(context, 0, 2));
        addView(mValueTextView);

        layoutParams = (LayoutParams) mValueTextView.getLayoutParams();
        layoutParams.width = LayoutParams.MATCH_PARENT;
        layoutParams.height = LayoutParams.WRAP_CONTENT;
        layoutParams.gravity = Gravity.LEFT | Gravity.TOP;
        layoutParams.leftMargin = UIUtils.px2dp(context, 57);
        layoutParams.topMargin = UIUtils.px2dp(context, 3);

        mValueTextView.setLayoutParams(layoutParams);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(MeasureSpec.makeMeasureSpec(UIUtils.px2dp(StoryApp.getContext(), 86), MeasureSpec.EXACTLY), MeasureSpec
                .makeMeasureSpec(UIUtils.px2dp(getContext(), 60), MeasureSpec.EXACTLY));
    }

    public void setIconAndTextAndValue(int resId, String text, float value) {
        mIconImageView.setImageResource(resId);
        mNameTextView.setText(text.toUpperCase());
        if (value == 0) {
            mValueTextView.setText("");
        } else if (value > 0) {
            mValueTextView.setText("+" + (int) value);
        } else {
            mValueTextView.setText("" + (int) value);
        }
    }

    public void setIconAndTextAndValue(int resId, String text, String value) {
        mIconImageView.setImageResource(resId);
        mNameTextView.setText(text.toUpperCase());
        mValueTextView.setText(value);
    }
}
