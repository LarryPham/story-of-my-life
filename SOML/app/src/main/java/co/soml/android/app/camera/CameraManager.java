/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/6/15 5:11 AM.
 **/

package co.soml.android.app.camera;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import java.util.Stack;

import co.soml.android.app.AppConstants;
import co.soml.android.app.camera.activities.CameraActivity;
import co.soml.android.app.camera.activities.CropPhotoActivity;
import co.soml.android.app.camera.activities.PhotoProcessActivity;
import co.soml.android.app.models.Photo;
import co.soml.android.app.utils.ImageUtils;
import co.soml.android.app.utils.LogUtils;

public class CameraManager {
    public static final String TAG = LogUtils.makeLogTag(CameraManager.class.getSimpleName());

    private static CameraManager mInstance;
    private Stack<FragmentActivity> mCameras = new Stack<>();

    public static CameraManager getInstance() {
        if (mInstance == null) {
            synchronized (CameraManager.class) {
                if (mInstance == null) {
                    mInstance = new CameraManager();
                }
            }
        }
        return mInstance;
    }

    public void openCamera(Context context) {
        final Intent intent = new Intent(context, CameraActivity.class);
        context.startActivity(intent);

    }

    public void processPhotoItem(FragmentActivity activity, Photo photo) {
        Uri uri = photo.getOriginalUrl().startsWith("file: ") ? Uri.parse(photo.getOriginalUrl()) : Uri.parse("file://" + photo.getOriginalUrl());
        if (ImageUtils.isSquare(Uri.parse(photo.getOriginalUrl()))) {
            Intent newIntent = new Intent(activity, PhotoProcessActivity.class);
            newIntent.setData(uri);
            activity.startActivity(newIntent);
        } else {
            Intent intent = new Intent(activity, CropPhotoActivity.class);
            intent.setData(uri);
            activity.startActivityForResult(intent, AppConstants.REQUEST_CROP);
        }
    }

    public void addActivity(FragmentActivity activity) {
        mCameras.add(activity);
    }

    public void removeActivity(FragmentActivity activity) {
        mCameras.remove(activity);
    }

    /**
     * Closes all of activity into the stack of activities. When using the instance of <code>CameraManager</code> which will be clear all
     * of activity screen into this stack. And then clear the stack of activity.
     */
    public void close() {
        for (FragmentActivity activity:  mCameras) {
            try {
                activity.finish();
            } catch (Exception ex) {
                LogUtils.LOGD(TAG, "Throws an exception" + ex);
            }
        }
        mCameras.clear();
    }
}
