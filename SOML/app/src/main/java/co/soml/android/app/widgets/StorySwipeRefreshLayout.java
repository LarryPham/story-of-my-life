/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 * @author: Larry Pham (email: larrypham.vn@gmail.com)
 * Date: 6/9/15 2:30 PM
 **/

package co.soml.android.app.widgets;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;
import android.view.MotionEvent;

import co.soml.android.app.utils.LogUtils;

public class StorySwipeRefreshLayout extends SwipeRefreshLayout {
    public static final String TAG = LogUtils.makeLogTag(StorySwipeRefreshLayout.class.getSimpleName());

    public StorySwipeRefreshLayout(Context context) {
        super(context);
    }

    public StorySwipeRefreshLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        try {
            return super.onTouchEvent(ev);
        } catch (IllegalArgumentException ex) {
            LogUtils.LOGE(TAG, ex.getMessage());
            return true;
        }
    }
}
