/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 * @author: Larry Pham (email: larrypham.vn@gmail.com)
 * Date: 6/15/15 5:45 PM
 **/

package co.soml.android.app.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import co.soml.android.app.R;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.utils.StoryAnimator;

public class CommentIconCountView extends LinearLayout {

    public static final String TAG = LogUtils.makeLogTag(CommentIconCountView.class.getSimpleName());

    private ImageView mImageView;
    private StoryTextView mTextContent;
    private String mCurrentContent;

    private static final int ICON_COMMENT_LIKE = 0;
    private static final int ICON_COMMENT_REPLY = 1;
    private static final int ICON_COMMENT_DISLIKE = 2;
    private int mIconMode = 0;

    public CommentIconCountView(Context context) {
        super(context);
        initView(context, null);
    }

    public CommentIconCountView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context, attrs);
    }

    public CommentIconCountView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void initView(Context context, AttributeSet attrs) {
        inflate(context, R.layout.comment_icon_count_view, this);
        mImageView = (ImageView) findViewById(R.id.comment_icon);
        mTextContent = (StoryTextView) findViewById(R.id.action_text);

        if (attrs != null) {
            TypedArray array = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CommentIconCountView, 0, 0);
            try {
                int icon = array.getInteger(R.styleable.CommentIconCountView_commentIcon, ICON_COMMENT_LIKE);
                switch (icon) {
                    case ICON_COMMENT_LIKE: {
                        mImageView.setImageResource(R.drawable.ic_comment_like);
                        mTextContent.setVisibility(View.VISIBLE);
                        mTextContent.setText(getResources().getString(R.string.comment_like_content));
                        mCurrentContent = getResources().getString(R.string.comment_like_content);
                        mIconMode = ICON_COMMENT_LIKE;
                        break;
                    }
                    case ICON_COMMENT_REPLY: {
                        mImageView.setImageResource(R.drawable.ic_comment_reply);
                        mTextContent.setVisibility(View.VISIBLE);
                        mTextContent.setText(getResources().getString(R.string.comment_reply_content));
                        mCurrentContent = getResources().getString(R.string.comment_reply_content);
                        mIconMode = ICON_COMMENT_REPLY;
                        break;
                    }
                    case ICON_COMMENT_DISLIKE: {
                        mImageView.setImageResource(R.drawable.ic_comment_dislike);
                        mTextContent.setVisibility(View.VISIBLE);
                        mTextContent.setText(getResources().getString(R.string.comment_dislike_content));
                        mCurrentContent = getResources().getString(R.string.comment_dislike_content);
                        mIconMode = ICON_COMMENT_DISLIKE;
                        break;
                    }
                }
            } finally {
                array.recycle();
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mImageView.setBackgroundResource(R.drawable.ripple_oval);
            }
        }
    }

    public ImageView getImageView() {
        return mImageView;
    }

    public void setSelected(boolean selected) {
        this.mImageView.setSelected(selected);
    }

    public void setContent(String content, boolean animateChanges) {
        if (!TextUtils.isEmpty(content)) {
            mTextContent.setVisibility(VISIBLE);
            mTextContent.setText(content);
        } else {
            mTextContent.setVisibility(GONE);
        }

        if (animateChanges && !content.equalsIgnoreCase(mCurrentContent)) {
            if (mTextContent.getVisibility() == View.VISIBLE) {
                StoryAnimator.scaleOut(mTextContent, View.GONE, StoryAnimator.Duration.LONG, null);
            } else {
                StoryAnimator.scaleIn(mTextContent, StoryAnimator.Duration.LONG);
            }
        }
        mCurrentContent = content;
    }
}
