package co.soml.android.app.controllers;

import android.os.Message;

import co.soml.android.app.StoryApp;
import co.soml.android.app.activities.BaseActivity;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.utils.LogUtils;

/**
 *  Copyright (C) 2015 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation
 *  are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied,
 *  reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior
 *  written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with respect to the contents, and
 *  assumes no responsibility for any errors that might appear in the software and documents. This publication and the
 *  contents hereof are subject to change without notice.
 *  @author Larry Pham(Email: larrypham.vn@gmail.com)
 *  @since 6/29/15 10:17 AM
 **/

public class StoryMainController extends BaseController {

    public static final String TAG = LogUtils.makeLogTag(StoryMainController.class.getSimpleName());
    public BaseActivity mActivity;
    public StoryApp mApp;
    public StoryDataModel mDataModel;

    public StoryMainController(BaseActivity activity, StoryDataModel dataModel) {
        super(activity, dataModel);

        this.mActivity = activity;
        this.mDataModel = dataModel;
        this.mApp = (StoryApp) mActivity.getApplicationContext();

    }

    public StoryMainController(BaseActivity activity, StoryDataModel dataModel, boolean isFirstRunningMode) {
        super(activity, dataModel, isFirstRunningMode);
    }

    @Override
    protected void handleMessage(Message msg) {

    }
}
