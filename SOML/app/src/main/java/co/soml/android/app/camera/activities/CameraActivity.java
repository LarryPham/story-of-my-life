/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/6/15 5:13 AM.
 **/

package co.soml.android.app.camera.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Matrix;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.FloatMath;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.ScaleAnimation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import butterknife.Bind;
import butterknife.ButterKnife;
import co.soml.android.app.R;
import co.soml.android.app.StoryApp;
import co.soml.android.app.activities.BaseActivity;
import co.soml.android.app.camera.CameraManager;
import co.soml.android.app.camera.utils.CameraHelper;
import co.soml.android.app.camera.utils.DistanceUtil;
import co.soml.android.app.camera.utils.FileUtils;
import co.soml.android.app.models.Photo;
import co.soml.android.app.services.tasks.WeakHandler;
import co.soml.android.app.utils.CommonUtil;
import co.soml.android.app.utils.ImageUtils;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.utils.StringUtils;
import co.soml.android.app.widgets.CameraGrid;

public class CameraActivity extends BaseActivity {

    public static final String TAG = LogUtils.makeLogTag(CameraActivity.class.getSimpleName());
    private CameraHelper mCameraHelper;
    private Camera.Parameters mParameters = null;
    private Camera mCameraInstance = null;
    private Bundle mBundle = null;
    private int mPhotoWidth = DistanceUtil.getCameraPhotoWidth();
    private int mPhotoNumber = 4;
    private int mPhotoMargin = CommonUtil.dp2px(1);
    private float mPointX,mPointY;

    public static final int FOCUS = 1;
    public static final int ZOOM = 2;
    private int mMode;

    private float mDistance;
    private int mPhotoSize = 2000;
    private int mCurrentCameraId = 0;

    private WeakHandler mHandler = new WeakHandler();

    @Bind(R.id.masking)
    protected CameraGrid mCameraGrid;
    @Bind(R.id.photo_area)
    protected LinearLayout mPhotoArea;
    @Bind(R.id.panel_take_photo)
    protected View mTakePhotoPanel;
    @Bind(R.id.takePicture)
    protected View mTakePhotoView;

    @Bind(R.id.action_flash)
    protected ImageButton mFlashButton;
    @Bind(R.id.action_change)
    protected ImageButton mChangeButton;
    @Bind(R.id.action_show_grid)
    protected ImageButton mShowGrid;

    @Bind(R.id.back)
    protected ImageButton mBackButton;
    @Bind(R.id.choose_gallery)
    protected ImageButton mGalleryButton;
    @Bind(R.id.focus_index)
    protected View mFocusIndexView;

    @Bind(R.id.surfaceView)
    protected SurfaceView mSurfaceView;

    private static final int MIN_PREVIEW_PIXELS = 480 * 320;
    private static final double MAX_ASPECT_DISTORTION = 0.15;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_layout);
        ButterKnife.bind(this);

        mCameraHelper = new CameraHelper(this);
        CameraManager.getInstance().addActivity(this);

        initView();
        initEvent();
    }

    protected void initView() {
        SurfaceHolder surfaceHolder = mSurfaceView.getHolder();
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        surfaceHolder.setKeepScreenOn(true);

        mSurfaceView.setFocusable(true);
        mSurfaceView.setBackgroundColor(TRIM_MEMORY_BACKGROUND);
        mSurfaceView.getHolder().addCallback(new SurfaceCallback());

       /* ViewGroup.LayoutParams params = mCameraGrid.getLayoutParams();
        params.height = CommonUtil.getScreenWidth();
        mCameraGrid.setLayoutParams(params);*/

        /*params = mPhotoArea.getLayoutParams();
        params.height = DistanceUtil.getCameraPhotoAreaHeight();*/

        /*params = mTakePhotoPanel.getLayoutParams();
        params.height = CommonUtil.getScreenHeight() - CommonUtil.getScreenWidth() - DistanceUtil.getCameraPhotoAreaHeight();*/
        /*ArrayList<Photo> sysPhotos = FileUtils.getInstance().findPicsInDir(FileUtils.getInstance().getSystemPhotoPath());
        int showNumber = sysPhotos.size() > mPhotoNumber ? mPhotoNumber : sysPhotos.size();
        for (int i = 0; i < showNumber; i++) {
            addPhoto(sysPhotos.get(showNumber - 1 - i));
        }*/
    }

    private void addPhoto(Photo photo) {
        ImageView picture = new ImageView(this);
        if (StringUtils.isNotBlank(photo.getOriginalUrl())) {
            CommonUtil.displayLocalImage(photo.getOriginalUrl(), picture, null);
        } else {
            picture.setImageResource(R.drawable.default_img);
        }

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(mPhotoWidth, mPhotoWidth);
        params.leftMargin = mPhotoMargin;
        params.rightMargin = mPhotoMargin;
        params.gravity = Gravity.CENTER;

        picture.setScaleType(ImageView.ScaleType.CENTER_CROP);
        picture.setTag(photo.getOriginalUrl());

        if (mPhotoArea.getChildCount() >= mPhotoNumber) {
            mPhotoArea.removeViewAt(mPhotoArea.getChildCount() - 1);
            mPhotoArea.addView(picture, 0, params);
        } else {
            mPhotoArea.addView(picture, 0, params);
        }

        picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v instanceof ImageView && v.getTag() instanceof String) {
                    Date currentDate = new Date(System.currentTimeMillis());
                    CameraManager.getInstance().processPhotoItem(CameraActivity.this, new Photo((String) v.getTag(), currentDate));
                }
            }
        });
    }

    private void initEvent() {
        mTakePhotoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    mCameraInstance.takePicture(null, null, new CameraPictureCallback());
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                    // Showing Toast
                    try {
                        mCameraInstance.startPreview();
                    } catch (Throwable ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });

        mFlashButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtils.LOGD(TAG, "Trigger to turn the light option");
                turnLight(mCameraInstance);
            }
        });

        boolean canSwitch = false;
        try {
            canSwitch = mCameraHelper.hasFrontCamera() && mCameraHelper.hasBackCamera();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        if (!canSwitch) {
            mChangeButton.setVisibility(View.GONE);
        } else {
            mChangeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LogUtils.LOGD(TAG, "Trigger to switch camera option");
                    switchCamera();
                }
            });
        }

        mShowGrid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final boolean showGrid = mCameraGrid.isShowGrid();
                if (!showGrid) {
                    mCameraGrid.setShowGrid(true);
                } else {
                    mCameraGrid.setShowGrid(false);
                }
                mCameraGrid.invalidate();
            }
        });
        mGalleryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtils.LOGD(TAG, "Trigger to start the AlbumActivity screen");
                startActivity(new Intent(CameraActivity.this, AlbumActivity.class));
            }
        });

        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtils.LOGD(TAG, "Trigger to close the current camera screen");
                finish();
            }
        });

        mSurfaceView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN: {
                        mPointX = event.getX();
                        mPointY = event.getY();
                        mMode = FOCUS;
                        break;
                    }
                    case MotionEvent.ACTION_POINTER_DOWN: {
                        mDistance = spacing(event);
                        if (spacing(event) > 10f) {
                            mMode = ZOOM;
                        }
                        break;
                    }
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_POINTER_UP:
                        mMode = FOCUS;
                        break;
                    case MotionEvent.ACTION_MOVE:
                        if (mMode == FOCUS) {

                        } else if (mMode == ZOOM) {
                            float newDistance = spacing(event);
                            if (newDistance > 10f) {
                                float tScale = (newDistance - mDistance) / mDistance;
                                if (tScale < 0) {
                                    tScale = tScale * 10;
                                }
                                addZoomIn((int) tScale);
                            }
                        }
                }
                return false;
            }
        });
        mSurfaceView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    pointFocus((int) mPointX, (int) mPointY);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(mFocusIndexView.getLayoutParams());
                layoutParams.setMargins((int) mPointX - 60, (int) mPointY - 60, 0, 0);
                mFocusIndexView.setLayoutParams(layoutParams);
                mFocusIndexView.setVisibility(View.VISIBLE);
                ScaleAnimation scaleAnimation = new ScaleAnimation(3f, 1f, 3f, 1f, ScaleAnimation.RELATIVE_TO_SELF, 0.5f
                        , ScaleAnimation.RELATIVE_TO_SELF, 0.5f);
                scaleAnimation.setDuration(800);
                mFocusIndexView.startAnimation(scaleAnimation);
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mFocusIndexView.setVisibility(View.INVISIBLE);
                    }
                }, 800);

                mTakePhotoPanel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
            }
        });
    }

    private float spacing(MotionEvent event) {
        if (event == null) {
            return 0;
        }

        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return  (float)Math.sqrt(x*x + y*y);
    }

    private int mCurrentZoomValue = 0;

    private void addZoomIn(int delta) {
        try {
            Camera.Parameters params = mCameraInstance.getParameters();
            LogUtils.LOGD(TAG, "Is Support Zoom " + params.isZoomSupported());

            if (!params.isZoomSupported()) {
                return;
            }

            mCurrentZoomValue += delta;
            if (mCurrentZoomValue < 0) {
                mCurrentZoomValue = 0;
            } else if (mCurrentZoomValue > params.getMaxZoom()) {
                mCurrentZoomValue = params.getMaxZoom();
            }

            if (!params.isSmoothZoomSupported()) {
                params.setZoom(mCurrentZoomValue);
                mCameraInstance.setParameters(params);
            } else {
                mCameraInstance.startSmoothZoom(mCurrentZoomValue);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void pointFocus(int x, int y) {
        mCameraInstance.cancelAutoFocus();
        mParameters = mCameraInstance.getParameters();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            showPoint(x, y);
        }
        mCameraInstance.setParameters(mParameters);
        autoFocus();
    }

    private void showPoint(int x, int y) {
        if (mParameters.getMaxNumMeteringAreas() > 0) {
            List<Camera.Area> areas = new ArrayList<>();
            int rectY = -x * 2000 / CommonUtil.getScreenWidth(StoryApp.getContext()) + 1000;
            int rectX = y * 2000 / CommonUtil.getScreenWidth(StoryApp.getContext()) - 1000;

            int left = rectX < -900 ? -1000 : rectX - 100;
            int top = rectY < -900 ? -1000 : rectY - 100;
            int right = rectX > 900 ? 1000 : rectX + 100;
            int bottom = rectY > 900 ? 1000 : rectY + 100;

            Rect area = new Rect(left, top, right, bottom);
            areas.add(new Camera.Area(area, 800));
            mParameters.setMeteringAreas(areas);
        }

        mParameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
    }

    private void autoFocus() {
        new Thread() {
            @Override
            public void run() {
                try {
                    sleep(1000);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }

                if (mCameraInstance == null) {
                    return;
                }
                mCameraInstance.autoFocus(new Camera.AutoFocusCallback() {
                    @Override
                    public void onAutoFocus(boolean success, Camera camera) {
                        if (success) {
                            initCamera();
                        }
                    }
                });
            }
        };
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
        CameraManager.getInstance().removeActivity(this);
    }

    @Override
    protected void invalidate() {

    }

    @Override
    protected void invalidate(Object... params) {

    }

    private final class CameraPictureCallback implements Camera.PictureCallback {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            mBundle = new Bundle();
            mBundle.putByteArray("bytes", data);
            new SavePictureTask(data).execute();
            camera.startPreview();
        }
    }

    private final class SurfaceCallback implements SurfaceHolder.Callback {

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            if (null == mCameraInstance) {
                try {
                    mCameraInstance = Camera.open();
                    mCameraInstance.setPreviewDisplay(holder);
                    initCamera();
                    mCameraInstance.startPreview();

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            autoFocus();
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            try {
                if (mCameraInstance != null) {
                    mCameraInstance.stopPreview();
                    mCameraInstance.release();
                    mCameraInstance = null;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private Camera.Size adapterSize = null;
    private Camera.Size previewSize = null;

    private void initCamera() {
        mParameters = mCameraInstance.getParameters();
        mParameters.setPictureFormat(PixelFormat.JPEG);

        setUpPicSize(mParameters);
        setUpPreviewSize(mParameters);

        if (adapterSize != null) {
            mParameters.setPictureSize(adapterSize.width, adapterSize.height);
        }

        if (previewSize != null) {
            mParameters.setPreviewSize(previewSize.width, previewSize.height);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            mParameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        } else {
            mParameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
        }
        setDisplay(mParameters, mCameraInstance);

        try {
            mCameraInstance.setParameters(mParameters);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        mCameraInstance.startPreview();
        mCameraInstance.cancelAutoFocus();
    }

    private void setUpPicSize(Camera.Parameters params) {
        if (adapterSize != null) {
            return;
        } else {
            adapterSize = findBestPictureResolution();
        }
    }

    private void setUpPreviewSize(Camera.Parameters parameters) {
        if (previewSize != null) {
            return;
        } else {
            previewSize = findBestPreviewResolution();
        }
    }
    private class SavePictureTask extends AsyncTask<Void, Void, String> {
        private byte[] data;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        SavePictureTask(byte[] data) {
            this.data = data;
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                return saveToSDCard(data);
            } catch (IOException ex) {
                ex.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (StringUtils.isNotEmpty(result)) {
                // Dismiss dialog
                Date currentDate = new Date(System.currentTimeMillis() * 1000);
                CameraManager.getInstance().processPhotoItem(CameraActivity.this, new Photo(result, currentDate));
            } else {
                // Showing Toast
            }
        }
    }

    /**
     * Finds the best resolution for preview ImageView
     * @return Camera.Size
     */
    private Camera.Size findBestPreviewResolution() {
        Camera.Parameters cameraParams = mCameraInstance.getParameters();
        Camera.Size defaultPreviewResolution = cameraParams.getPreviewSize();

        List<Camera.Size> rawSupportedSizes = cameraParams.getSupportedPreviewSizes();
        if (rawSupportedSizes == null) {
            return defaultPreviewResolution;
        }

        List<Camera.Size> supportedPreviewResolutions = new ArrayList<Camera.Size>(rawSupportedSizes);
        Collections.sort(supportedPreviewResolutions, new Comparator<Camera.Size>() {
            @Override
            public int compare(Camera.Size lhs, Camera.Size rhs) {
                int aPixels = lhs.height * lhs.width;
                int bPixels = rhs.height * rhs.width;

                if (bPixels < aPixels) {
                    return -1;
                }

                if (bPixels > aPixels) {
                    return 1;
                }
                return 0;
            }
        });
        StringBuilder previewResolutionSb = new StringBuilder();
        for (Camera.Size supportedPreviewResolution : supportedPreviewResolutions) {
            previewResolutionSb.append(supportedPreviewResolution.width).append('x').append(supportedPreviewResolution.height).append(' ');
        }

        double screenAspectRatio = (double) CommonUtil.getScreenWidth(StoryApp.getContext()) / (double) CommonUtil.getScreenHeight
                (StoryApp.getContext());
        Iterator<Camera.Size> iterator = supportedPreviewResolutions.iterator();
        while (iterator.hasNext()) {
            Camera.Size supportedPreviewResolution = iterator.next();
            int width = supportedPreviewResolution.width;
            int height = supportedPreviewResolution.height;

            if (width * height < MIN_PREVIEW_PIXELS) {
                iterator.remove();
                continue;
            }

            boolean isCandidatePortrait = width > height;
            int maybeFlippedWidth = isCandidatePortrait ? height : width;
            int maybeFlipperHeight = isCandidatePortrait ? width : height;
            double aspectRatio = (double) maybeFlippedWidth / (double) maybeFlipperHeight;
            double distortion = Math.abs(aspectRatio - screenAspectRatio);

            if (distortion > MAX_ASPECT_DISTORTION) {
                iterator.remove();
            }

            if (maybeFlippedWidth == CommonUtil.getScreenWidth(StoryApp.getContext()) && maybeFlipperHeight == CommonUtil.getScreenHeight
                    (StoryApp.getContext())) {
                return supportedPreviewResolution;
            }
        }

        if (!supportedPreviewResolutions.isEmpty()) {
            Camera.Size largestPreview = supportedPreviewResolutions.get(0);
            return largestPreview;
        }

        return defaultPreviewResolution;
    }

    /**
     * Finds the best resolution for displaying the picture
     * @return Camera.Size
     */
    private Camera.Size findBestPictureResolution() {
        Camera.Parameters cameraParams = mCameraInstance.getParameters();
        List<Camera.Size> supportedPicResolutions = cameraParams.getSupportedPictureSizes();
        StringBuilder picResolutionBuffer = new StringBuilder();
        for (Camera.Size supportedPicResolution : supportedPicResolutions) {
            picResolutionBuffer.append(supportedPicResolution.width).append('x').append(supportedPicResolution.height).append(" ");
        }
        LogUtils.LOGD(TAG, "Supported picture resolutions: " + picResolutionBuffer);
        Camera.Size defaultPictureResolution = cameraParams.getPictureSize();
        LogUtils.LOGD(TAG, "Default picture resolution: " + defaultPictureResolution.width + "x" + defaultPictureResolution.height);
        List<Camera.Size> sortedSupportedPicResolutions = new ArrayList<Camera.Size>(supportedPicResolutions);
        Collections.sort(sortedSupportedPicResolutions, new Comparator<Camera.Size>() {
            @Override
            public int compare(Camera.Size lhs, Camera.Size rhs) {
                int aPixels = lhs.height * lhs.width;
                int bPixels = rhs.height * rhs.width;
                if (bPixels < aPixels) {
                    return -1;
                }

                if (bPixels > aPixels) {
                    return 1;
                }
                return 0;
            }
        });

        double screenAspectRatio = (double) CommonUtil.getScreenWidth(StoryApp.getContext())
                / (double) CommonUtil.getScreenHeight(StoryApp.getContext());
        Iterator<Camera.Size> iterator = sortedSupportedPicResolutions.iterator();
        while (iterator.hasNext()) {
            Camera.Size supportedPreviewResolution = iterator.next();
            int width = supportedPreviewResolution.width;
            int height = supportedPreviewResolution.height;

            boolean isCandidatePortrait = width > height;
            int maybeFlippedWidth = isCandidatePortrait ? height : width;
            int maybeFlippedHeight = isCandidatePortrait ? width : height;
            double aspectRatio = (double) maybeFlippedWidth / (double) maybeFlippedHeight;
            double distortion = Math.abs(aspectRatio - screenAspectRatio);

            if (distortion > MAX_ASPECT_DISTORTION) {
                iterator.remove();
            }
        }

        if (!sortedSupportedPicResolutions.isEmpty()) {
            return sortedSupportedPicResolutions.get(0);
        }

        return defaultPictureResolution;
    }

    private void setDisplay(Camera.Parameters params, Camera camera) {
        if (Build.VERSION.SDK_INT >= 8) {
            setDisplayOrientation(camera, 90);
        } else {
            params.setRotation(90);
        }
    }

    private void setDisplayOrientation(Camera camera, int degrees) {
        Method downPolymorphic;
        try {
            downPolymorphic = camera.getClass().getMethod("setDisplayOrientation", new Class[] {int.class});
            if (downPolymorphic != null) {
                downPolymorphic.invoke(camera, new Object[]{degrees});
            }
        } catch (Exception ex) {
            LogUtils.LOGE(TAG, "throws new exception" + ex);
        }
    }

    public String saveToSDCard(byte[] data) throws IOException {
        Bitmap croppedImage;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(data, 0, data.length, options);
        mPhotoSize = options.outHeight > options.outWidth ? options.outWidth : options.outHeight;
        int height = options.outHeight > options.outWidth ? options.outHeight : options.outWidth;
        options.inJustDecodeBounds = false;
        Rect rect;
        if (mCurrentCameraId == 1) {
            rect = new Rect(height - mPhotoSize, 0, height, mPhotoSize);
        } else {
            rect = new Rect(0, 0, mPhotoSize, mPhotoSize);
        }

        try {
            croppedImage = decodeRegionCrop(data, rect);
        } catch (Exception ex) {
            return null;
        }

        String imagePath = ImageUtils.saveToFile(FileUtils.getInstance().getSystemPhotoPath(), true, croppedImage);
        croppedImage.recycle();
        return imagePath;
    }

    private Bitmap decodeRegionCrop(byte[] data, Rect rect) {
        InputStream inputStream = null;
        System.gc();
        Bitmap croppedImage = null;

        try {
            inputStream = new ByteArrayInputStream(data);
            BitmapRegionDecoder decoder = BitmapRegionDecoder.newInstance(inputStream, false);

            try {
                croppedImage = decoder.decodeRegion(rect, new BitmapFactory.Options());
            } catch (IllegalArgumentException e) {

            }
        } catch (Throwable ex) {
            ex.printStackTrace();
        } finally {
            FileUtils.closeStream(inputStream);
        }

        Matrix matrix = new Matrix();
        matrix.setRotate(90, mPhotoSize/2, mPhotoSize/2);
        if (mCurrentCameraId == 1) {
            matrix.postScale(1,-1);
        }
        Bitmap rotatedImage = Bitmap.createBitmap(croppedImage, 0, 0, mPhotoSize, mPhotoSize, matrix, true);
        if (rotatedImage != croppedImage) {
            croppedImage.recycle();
        }
        return rotatedImage;
    }

    private void turnLight(Camera camera) {
        if (camera == null || camera.getParameters() == null || camera.getParameters().getSupportedFlashModes() == null) {
            return;
        }

        Camera.Parameters params = camera.getParameters();
        String flashMode = camera.getParameters().getFlashMode();
        List<String> supportedModes = camera.getParameters().getSupportedFlashModes();
        if (Camera.Parameters.FLASH_MODE_OFF.equals(flashMode) && supportedModes.contains(Camera.Parameters.FLASH_MODE_ON)) {
            params.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
            camera.setParameters(params);
            mFlashButton.setImageResource(R.drawable.ic_flash_on);
        } else if (Camera.Parameters.FLASH_MODE_ON.equals(flashMode)) {
            if (supportedModes.contains(Camera.Parameters.FLASH_MODE_AUTO)) {
                params.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
                mFlashButton.setImageResource(R.drawable.ic_flash_auto);
                camera.setParameters(params);
            } else if (supportedModes.contains(Camera.Parameters.FLASH_MODE_OFF)) {
                params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                mFlashButton.setImageResource(R.drawable.ic_flash_off);
                camera.setParameters(params);
            }
        } else if (Camera.Parameters.FLASH_MODE_AUTO.equals(flashMode) && supportedModes.contains(Camera.Parameters.FLASH_MODE_OFF)) {
            params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            camera.setParameters(params);
            mFlashButton.setImageResource(R.drawable.ic_flash_off);
        }
    }

    private void switchCamera() {
        mCurrentCameraId = (mCurrentCameraId + 1) % mCameraHelper.getNumberOfCameras();
        releaseCamera();
        LogUtils.LOGD(TAG, "switchCamera -- mCurrentCameraId: " + mCurrentCameraId);
        setUpCamera(mCurrentCameraId);
    }

    private void releaseCamera() {
        if (mCameraInstance != null) {
            mCameraInstance.setPreviewCallback(null);
            mCameraInstance.release();
            mCameraInstance = null;
        }

        adapterSize = null;
        previewSize = null;
    }

    private void setUpCamera(int currentCameraId) {
        mCameraInstance = getCameraInstance(currentCameraId);
        if (mCameraInstance != null) {
            try {
                mCameraInstance.setPreviewDisplay(mSurfaceView.getHolder());
                initCamera();
                mCameraInstance.startPreview();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } else {
            //Toast.makeText("Opening camera preview!!!", Toast.LENGTH_LONG);
        }
    }

    private Camera getCameraInstance(final int id) {
        Camera camera = null;
        try {
            camera = mCameraHelper.openCamera(id);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return camera;
    }
}
