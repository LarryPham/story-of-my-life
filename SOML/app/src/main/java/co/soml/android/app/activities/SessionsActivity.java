package co.soml.android.app.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.Window;

import co.soml.android.app.R;
import co.soml.android.app.controllers.BaseController;
import co.soml.android.app.fragments.BaseFragment;
import co.soml.android.app.fragments.NewUserFragment;
import co.soml.android.app.fragments.SignInFragment;
import co.soml.android.app.fragments.SocialSigninFragment;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.utils.LogUtils;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.24.2015
 */
public class SessionsActivity extends BaseActivity {
    public static final String TAG = LogUtils.makeLogTag(SessionsActivity.class);

    public static final String FRAGMENT_PARAMS = "SessionsParams";
    public static final String FRAGMENT_PAGE_PARAMS = "SessionPage";

    public static final String SOCIAL_FRAGMENT_PAGE = "co.soml.android.app.fragments.SocialSigninFragment";
    public static final String SIGN_IN_FRAGMENT_PAGE = "co.soml.android.app.fragments.SigninFragment";
    public static final String SIGN_UP_FRAGMENT_PAGE = "co.soml.android.app.fragments.NewUserFragment";

    /**
     * Declaring the framgnet's tag names for each of fragments which has interacted with <code>SessionsActivity</code> screens
     */
    public static final String SOCIAL_SIGNIN_FRAGMENT_KEY = "SocialPage";
    public static final String SIGNIN_FRAGMENT_KEY = "SigninPage";
    public static final String SIGNUP_FRAGMENT_KEY = "SignupPage";
    /**
     * Params which has been responded to assign in to the {@link android.app.Activity#onActivityResult(int, int,
     * android.content.Intent)} which has specified intent and request code, result code
     */
    public static final int SIGN_IN_REQUEST = 1;
    public static final int CREATE_ACCOUNT_REQUEST = 2;
    public static final int SOCIAL_ACCOUNT_REQUEST = 3;

    public static final int REQUEST_CODE = 5000;

    /**
     * Declaring the <code>BaseController</code> for sending messages which has interacted with signals into {@link co.soml.android.app
     * .activities.SessionsActivity} and the service component will received message from it.
     */
    protected BaseController mController;
    /**
     * Declaring the <code>BaseDataModel</code> for capturing all of models which has been used into the current fragments into this screens.
     */
    protected StoryDataModel mDataModel;

    protected BaseFragment mContent;
    /**
     * Declaring the <code>SocialSigninFragment</code> for handling the signin's process with email-signin, social network signin
     */
    private SocialSigninFragment mSocialSigninFragment;
    /**
     * Declaring the <code>SigninFragment</code> for handling the signin's process.
     */
    private SignInFragment mSignInFragment;
    /**
     * Declaring the <code>SignnupFragment</code> for handling the signup's process.
     */
    private NewUserFragment mNewUserFragment;

    private FragmentTransaction mTransaction;

    private Bundle mArguments;

    private String mSessionPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sessions_layout);
        mSignInFragment = new SignInFragment();
        mNewUserFragment = new NewUserFragment();
        mSocialSigninFragment = SocialSigninFragment.newInstance();

        if (getIntent() != null) {
            handleFragmentPages(getIntent());
        }
    }

    public void handleFragmentPages(final Intent sessionIntent) {
        mArguments = sessionIntent.getExtras();
        if (mArguments != null && !TextUtils.isEmpty(mArguments.getString(FRAGMENT_PAGE_PARAMS))) {
            mSessionPage = mArguments.getString(FRAGMENT_PAGE_PARAMS);
        }

        if (mSessionPage.equalsIgnoreCase(SIGN_IN_FRAGMENT_PAGE)) {
            final Bundle signInBundle = new Bundle();
            addContent(mSignInFragment, SIGNIN_FRAGMENT_KEY, signInBundle);
        } else if (mSessionPage.equalsIgnoreCase(SIGN_UP_FRAGMENT_PAGE)) {
            final Bundle signUpBundle = new Bundle();
            addContent(mNewUserFragment, SIGNUP_FRAGMENT_KEY, signUpBundle);
        } else if (mSessionPage.equalsIgnoreCase(SOCIAL_FRAGMENT_PAGE)) {
            final Bundle socialPageBundle = new Bundle();
            addContent(mSocialSigninFragment, SOCIAL_SIGNIN_FRAGMENT_KEY, socialPageBundle);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        handleFragmentPages(getIntent());
    }

    @Override
    protected boolean shouldAddToBackStack(FragmentManager fragmentManager, String fragmentKey, Bundle extras) {
        if (fragmentKey.equalsIgnoreCase(SIGNIN_FRAGMENT_KEY)) {
            return true;
        } else if (fragmentKey.equalsIgnoreCase(SIGNUP_FRAGMENT_KEY)) {
            return true;
        } else if (fragmentKey.equalsIgnoreCase(SOCIAL_SIGNIN_FRAGMENT_KEY)) {
            return true;
        }
        return super.shouldAddToBackStack(fragmentManager, fragmentKey, extras);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        BaseFragment fragment = (BaseFragment) getSupportFragmentManager().findFragmentByTag(SOCIAL_SIGNIN_FRAGMENT_KEY);
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void invalidate() {

    }

    @Override
    public void invalidate(Object... params) {

    }
}
