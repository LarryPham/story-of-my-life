/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/14/15 1:52 PM.
 **/

package co.soml.android.app.models;

import java.util.ArrayList;

import co.soml.android.app.utils.LogUtils;

public class StickerPackList {
    public static final String TAG = LogUtils.makeLogTag(StickerPackList.class.getSimpleName());

    public ArrayList<StickerPack> mStickerPacks = new ArrayList<>();

    public StickerPackList() {

    }

    public ArrayList<StickerPack> getStickerPacks() {
        return mStickerPacks;
    }

    public void setStickerPacks(ArrayList<StickerPack> stickerPacks) {
        mStickerPacks = stickerPacks;
    }

    public StickerPackList(ArrayList<StickerPack> stickerPacks) {
        mStickerPacks = stickerPacks;
    }

    public StickerPack getStickerPackAtIndex(int index) {
        if (index >= 0 && index < mStickerPacks.size()) {
            return mStickerPacks.get(index);
        }
        return null;
    }

    public void removeStickerPack(StickerPack stickerPack) {
        int removeIndex = -1;
        int count = mStickerPacks.size();
        for (int index = 0; index < count; index++) {
            StickerPack scanPack = mStickerPacks.get(index);
            if (scanPack.getStickerPackId() ==  stickerPack.getStickerPackId()) {
                removeIndex = index;
                break;
            }
        }

        if (removeIndex >= 0) {
            mStickerPacks.remove(removeIndex);
        }
    }

    public void addStickerPack(StickerPack stickerPack) {
        if (mStickerPacks != null) {
            for (StickerPack pack : mStickerPacks) {
                if (pack.getStickerPackId() != stickerPack.getStickerPackId()) {
                    mStickerPacks.add(stickerPack);
                }
            }
        }
    }

    public boolean containStickerPack(StickerPack stickerPack) {
        if (mStickerPacks != null) {
            for (StickerPack pack : mStickerPacks) {
                if (pack.getStickerPackId() == stickerPack.getStickerPackId()) {
                    return true;
                }
            }
            return false;
        }
        return false;
    }
}
