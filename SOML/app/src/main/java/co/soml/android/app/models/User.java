package co.soml.android.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.Date;

import co.soml.android.app.AppConstants;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its
 * documentation are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable
 * form without the prior written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with
 * respect to the contents, and assumes no responsibility for any errors that might appear in the software and
 * documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.03.2015
 */
public class User implements Parcelable, Serializable {
    public static final String TAG = AppConstants.PREFIX + User.class.getSimpleName();

    @Expose
    @SerializedName("id")
    public int mId;

    @Expose
    @SerializedName("fb_id")
	public long mFacebookId;

    @Expose
    @SerializedName("fabric_id")
	public long mFabricId;

    @Expose
    @SerializedName("auth_token")
	public String mAuthToken;

    @Expose
    @SerializedName("fb_token")
	public String mFacebookAuthToken;

    @Expose
    @SerializedName("fabric_auth_token")
	public String mFabricAuthToken;

    @Expose
    @SerializedName("sharing_facebook_token")
	public String mSharingFacebookAuthToken;

    @Expose
    @SerializedName("sharing_twitter_token")
	public String mSharingTwitterAuthToken;

    @Expose
    @SerializedName("token_expired_at")
	public Date mTokenExpiredAt;

    @Expose
    @SerializedName("fabric_auth_token_secret")
	public String mFabricAuthTokenSecret;

    @Expose
    @SerializedName("email")
    public String mEmail;

    @Expose
    @SerializedName("guest_email")
	public String mGuestEmail;

    @Expose
    @SerializedName("name")
    public String mFullName;

    @Expose
    @SerializedName("guest")
	public String mGuest;

    @Expose
    @SerializedName("fabric_screen_name")
	public String mFabricScreenName;

    @Expose
    @SerializedName("verified")
	public boolean mVerified;

    @Expose
    @SerializedName("description")
    public String mDescription;

    @Expose
    @SerializedName("place")
	public String mPlace;

    @Expose
    @SerializedName("phone")
	public String mPhone;

    @Expose
    @SerializedName("roles")
	public String[] mRoles;

    @Expose
    @SerializedName("avatar")
    public Avatar mAvatar;

    @Expose
    @SerializedName("lang")
    public String mLanguage;
    
    @Expose
    @SerializedName("stories_count")
    public int mStoryCount;

    @Expose
    @SerializedName("following")
    public boolean mFollowing;

    @Expose
    @SerializedName("users_following")
	public long mFollowingAmount;

    @Expose
    @SerializedName("users_followed")
	public long mFollowerAmount;

    @Expose
    @SerializedName("is_followed_by_current_user")
	public boolean mFollowedByCurrentUser;

    private String mAvatarOriginalUrl;
    private String mAvatarThumbnailUrl;
    private String mAvatarSmallUrl;
    private String mAvatarMediumUrl;
    private String mAvatarLargeUrl;

    public User() {

	}
    public User(Parcel source) {
        readFromParcel(source);
    }

    public User(User user) {
        this.mId = user.mId;
        this.mEmail = user.mEmail;
        this.mFabricId = user.mFabricId;
        this.mFacebookId = user.mFacebookId;

        this.mAuthToken = user.mAuthToken;
        this.mFacebookAuthToken = user.mFacebookAuthToken;
        this.mFabricAuthToken = user.mFabricAuthToken;
        this.mFabricAuthTokenSecret = user.mFabricAuthTokenSecret;
        this.mSharingFacebookAuthToken = user.mSharingFacebookAuthToken;
        this.mSharingTwitterAuthToken = user.mSharingTwitterAuthToken;
        this.mTokenExpiredAt = user.mTokenExpiredAt;

        this.mFabricScreenName = user.mFabricScreenName;
        this.mGuest = user.mGuest;
        this.mGuestEmail = user.mGuestEmail;

        this.mFullName = user.mFullName;
        this.mDescription = user.mDescription;

        this.mAvatarOriginalUrl = ((user.mAvatar != null) ? user.getAvatar().mUrl : null);
        this.mAvatarThumbnailUrl = ((user.getAvatar() != null) ? (user.getAvatar().mThumb != null
                ? user.getAvatar().mThumb.mUrl : null) : null);
        this.mAvatarSmallUrl = ((user.getAvatar() != null) ? (user.getAvatar().mSmall != null
                ? user.getAvatar().mSmall.mUrl : null) : null);
        this.mAvatarMediumUrl = ((user.getAvatar() != null) ? (user.getAvatar().mMedium != null
                ? user.getAvatar().mMedium.mUrl : null) : null);
        this.mAvatarLargeUrl = ((user.getAvatar() != null) ? (user.getAvatar().mLarge != null
                ? user.getAvatar().mLarge.mUrl : null) : null);

        this.mPlace = user.mPlace;
        this.mPhone = user.mPhone;
        this.mRoles = user.mRoles;
        this.mVerified = user.mVerified;
        this.mFollowedByCurrentUser = user.mFollowedByCurrentUser;

        this.mLanguage = user.mLanguage;
        this.mStoryCount = user.mStoryCount;
        this.mFollowing = user.mFollowing;

	    this.mFollowerAmount = user.mFollowerAmount;
	    this.mFollowingAmount = user.mFollowingAmount;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getFullName() {
        return mFullName;
    }

    public void setFullName(String fullName) {
        mFullName = fullName;
    }

    public String getAvatarOriginalUrl() {
        return mAvatarOriginalUrl;
    }

    public void setAvatarOriginalUrl(String avatarOriginalUrl) {
        mAvatarOriginalUrl = avatarOriginalUrl;
    }

    public String getAvatarThumbnailUrl() {
        return mAvatarThumbnailUrl;
    }

    public void setAvatarThumbnailUrl(String avatarThumbnailUrl) {
        mAvatarThumbnailUrl = avatarThumbnailUrl;
    }

    public String getAvatarSmallUrl() {
        return mAvatarSmallUrl;
    }

    public void setAvatarSmallUrl(String avatarSmallUrl) {
        mAvatarSmallUrl = avatarSmallUrl;
    }

    public String getAvatarMediumUrl() {
        return mAvatarMediumUrl;
    }

    public void setAvatarMediumUrl(String avatarMediumUrl) {
        mAvatarMediumUrl = avatarMediumUrl;
    }

    public String getAvatarLargeUrl() {
        return mAvatarLargeUrl;
    }

    public void setAvatarLargeUrl(String avatarLargeUrl) {
        mAvatarLargeUrl = avatarLargeUrl;
    }

    public String getLanguage() {
        return mLanguage;
    }

    public void setLanguage(String language) {
        mLanguage = language;
    }

    public int getStoryCount() {
        return mStoryCount;
    }

    public void setStoryCount(int storyCount) {
        mStoryCount = storyCount;
    }

    public String getAuthToken() {
        return mAuthToken;
    }

    public void setAuthToken(String authToken) {
        mAuthToken = authToken;
    }

    public boolean isFollowing() {
        return mFollowing;
    }

    public void setFollowing(boolean following) {
        mFollowing = following;
    }

	public long getFollowingAmount() {
		return mFollowingAmount;
	}

	public void setFollowingAmount(long followingAmount) {
		mFollowingAmount = followingAmount;
	}

	public long getFollowerAmount() {
		return mFollowerAmount;
	}

	public void setFollowerAmount(long followerAmount) {
		mFollowerAmount = followerAmount;
	}

	public boolean isFollowedByCurrentUser() {
		return mFollowedByCurrentUser;
	}

	public void setFollowedByCurrentUser(boolean followedByCurrentUser) {
		mFollowedByCurrentUser = followedByCurrentUser;
	}

	public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public Avatar getAvatar() {
        return mAvatar;
    }

    public void setAvatar(Avatar avatar) {
        mAvatar = avatar;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mId);
        dest.writeString(mEmail);
        dest.writeString(mFullName);
        dest.writeString(mDescription);

        dest.writeString(mAvatarOriginalUrl);
        dest.writeString(mAvatarThumbnailUrl);
        dest.writeString(mAvatarSmallUrl);
        dest.writeString(mAvatarMediumUrl);
        dest.writeString(mAvatarLargeUrl);
        dest.writeString(mLanguage);
        dest.writeInt(mStoryCount);
        dest.writeSerializable(mAuthToken);
        dest.writeByte((byte) (mFollowing ? 1 : 0));
	    dest.writeLong(mFollowingAmount);
	    dest.writeLong(mFollowerAmount);
        dest.writeParcelable(mAvatar, flags);
    }

    public void readFromParcel(Parcel source) {
        this.mId = source.readInt();
        this.mEmail = source.readString();
        this.mFullName = source.readString();
        this.mDescription = source.readString();

        this.mAvatarOriginalUrl = source.readString();
        this.mAvatarThumbnailUrl = source.readString();
        this.mAvatarSmallUrl = source.readString();
        this.mAvatarMediumUrl = source.readString();
        this.mAvatarLargeUrl = source.readString();
        this.mLanguage = source.readString();
        this.mStoryCount = source.readInt();
        this.mAuthToken = source.readString();
        this.mFollowing = source.readByte() != 0;
	    this.mFollowingAmount =  source.readLong();
	    this.mFollowerAmount = source.readLong();
        this.mAvatar = source.readParcelable(Avatar.class.getClassLoader());
    }

    public boolean isSameUser(User user) {
        if (user == null) {
            return false;
        }

        if (this.mId != user.mId) {
            return false;
        }

        if (!this.mAvatar.equals(user.mAvatar)) {
            return false;
        }

        if (!this.mFullName.equals(user.mFullName)) {
            return false;
        }

        if (!this.mAuthToken.equals(user.mAuthToken)) {
            return false;
        }

        return true;
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
    
    public static class Avatar implements Parcelable, Serializable {
        @Expose
        @SerializedName("url")
        public String mUrl;

        @Expose
        @SerializedName("thumb")
        public Thumb mThumb;

        @Expose
        @SerializedName("small")
        public Small mSmall;
        
        @Expose
        @SerializedName("medium")
        public Medium mMedium;
        
        @Expose
        @SerializedName("large")
        public Large mLarge;

        protected Avatar(Parcel in) {
            mUrl = in.readString();
            mThumb = in.readParcelable(Thumb.class.getClassLoader());
            mSmall = in.readParcelable(Small.class.getClassLoader());
            mMedium = in.readParcelable(Medium.class.getClassLoader());
            mLarge = in.readParcelable(Large.class.getClassLoader());
        }

        public static final Creator<Avatar> CREATOR = new Creator<Avatar>() {
            @Override
            public Avatar createFromParcel(Parcel in) {
                return new Avatar(in);
            }

            @Override
            public Avatar[] newArray(int size) {
                return new Avatar[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(mUrl);
            dest.writeParcelable(mThumb, flags);
            dest.writeParcelable(mSmall, flags);
            dest.writeParcelable(mMedium, flags);
            dest.writeParcelable(mLarge, flags);
        }
    }

    public static class Small implements Parcelable, Serializable {
        @Expose
        @SerializedName("url")
        public String mUrl;

        protected Small(Parcel in) {
            mUrl = in.readString();
        }

        public static final Creator<Small> CREATOR = new Creator<Small>() {
            @Override
            public Small createFromParcel(Parcel in) {
                return new Small(in);
            }

            @Override
            public Small[] newArray(int size) {
                return new Small[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(mUrl);
        }
    }
    
    public static class Thumb implements Parcelable, Serializable {

        @Expose
        @SerializedName("url")
        public String mUrl;

        protected Thumb(Parcel in) {
            mUrl = in.readString();
        }

        public static final Creator<Thumb> CREATOR = new Creator<Thumb>() {
            @Override
            public Thumb createFromParcel(Parcel in) {
                return new Thumb(in);
            }

            @Override
            public Thumb[] newArray(int size) {
                return new Thumb[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(mUrl);
        }
    }

    public static class Medium implements Parcelable, Serializable {

        @Expose
        @SerializedName("url")
        public String mUrl;

        protected Medium(Parcel in) {
            mUrl = in.readString();
        }

        public static final Creator<Medium> CREATOR = new Creator<Medium>() {
            @Override
            public Medium createFromParcel(Parcel in) {
                return new Medium(in);
            }

            @Override
            public Medium[] newArray(int size) {
                return new Medium[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(mUrl);
        }
    }

    public static class Large implements Parcelable, Serializable {

        @Expose
        @SerializedName("url")
        public String mUrl;


        protected Large(Parcel in) {
            mUrl = in.readString();
        }

        public static final Creator<Large> CREATOR = new Creator<Large>() {
            @Override
            public Large createFromParcel(Parcel in) {
                return new Large(in);
            }

            @Override
            public Large[] newArray(int size) {
                return new Large[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(mUrl);
        }
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("*******  User Details *******\n");
        builder.append("ID= ").append(mId).append("\n");
        builder.append("Email= ").append(mEmail).append("\n");
        builder.append("FullName= ").append(mFullName).append("\n");
        builder.append("Description= ").append(mDescription).append("\n");
        builder.append("Token= ").append(mAuthToken).append("\n");
        builder.append("Followers= ").append(mFollowerAmount).append("\n");
        builder.append("Following Users= ").append(mFollowingAmount).append("\n");
        builder.append("Original ImageUrl= ").append(mAvatarOriginalUrl).append("\n");
        builder.append("Medium ImageUrl= ").append(mAvatarMediumUrl).append("\n");
        builder.append("Thumbnail ImageUrl= ").append(mAvatarThumbnailUrl).append("\n");
        builder.append("Large ImageUrl= ").append(mAvatarLargeUrl).append("\n");
        builder.append("******************************");
        return builder.toString();
    }
}
