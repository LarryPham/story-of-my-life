package co.soml.android.app.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;

import co.soml.android.app.AppConstants;
import co.soml.android.app.R;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.08.2015
 */
public class ProgressWheel extends View {
    public static final String TAG = AppConstants.PREFIX + ProgressWheel.class.getSimpleName();

    private int mCircleRadius = 28;
    private int mBarWidth = 4;
    private int mRimWidth = 4;
    private final int mBarLength = 16;
    private final int mBarMaxLength = 270;
    private boolean mFilledRadius = false;
    private double mTimeStartGrowing = 0;
    private double mBarSpinCycleTime = 460;
    private float mBarExtrasLength = 0;
    private boolean mBarGrowingFromFront = true;
    private long mPausedTimeWithoutGrowing = 0;
    private final long mPausedGrowingTime = 200;

    private int mBarColor = 0xFF5588FF;
    private int mRimColor = 0x00fefefe;

    private RectF mCircleBounds = new RectF();
    private Paint mBarPaint = new Paint();
    private Paint mRimPaint = new Paint();

    private float mSpinSpeed = 230.0f;
    private long mLastTimeAnimated = 0;
    private boolean mLinearProgress;

    private float mProgress = 0.0f;
    private float mTargetProgress = 0.0f;
    private boolean mIsSpinning = false;
    private ProgressCallback mCallback;

    public ProgressWheel(Context context) {
        this(context, null);
    }

    public ProgressWheel(Context context, AttributeSet attrs) {
        super(context, attrs);
        parseAttributes(context.obtainStyledAttributes(R.styleable.ProgressWheel));
    }

    public ProgressWheel(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int viewWidth = mCircleRadius + this.getPaddingLeft() + this.getPaddingRight();
        int viewHeight = mCircleRadius + this.getPaddingTop() + this.getPaddingBottom();

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int width;
        int height;
        if (widthMode == MeasureSpec.EXACTLY) {
            width = widthSize;
        } else if (widthMode == MeasureSpec.AT_MOST) {
            width = Math.min(viewWidth, widthSize);
        } else {
            width = viewWidth;
        }

        if (heightMode == MeasureSpec.EXACTLY || widthMode == MeasureSpec.EXACTLY) {
            height = heightSize;
        } else if (heightMode == MeasureSpec.AT_MOST) {
            height = Math.min(viewHeight, heightSize);
        } else {
            height = viewHeight;
        }

        setMeasuredDimension(width, height);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        setupBounds(w, h);
        setupPaints();
        invalidate();
    }

    private void setupPaints() {
        mBarPaint.setColor(mBarColor);
        mBarPaint.setAntiAlias(true);
        mBarPaint.setStyle(Paint.Style.STROKE);
        mBarPaint.setStrokeWidth(mBarWidth);

        mRimPaint.setColor(mRimColor);
        mRimPaint.setAntiAlias(true);
        mRimPaint.setStyle(Paint.Style.STROKE);
        mRimPaint.setStrokeWidth(mRimWidth);
    }

    private void setupBounds(int layoutWidth, int layoutHeight) {
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();
        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();

        if (!mFilledRadius) {
            int minValue = Math.min(layoutWidth - paddingLeft - paddingRight, layoutHeight - paddingBottom - paddingTop);
            int circleDiameter = Math.min(minValue, mCircleRadius * 2 - mBarWidth * 2);
            int xOffset = (layoutWidth - paddingLeft - paddingRight - circleDiameter) / 2 + paddingLeft;
            int yOffset = (layoutHeight - paddingTop - paddingBottom - circleDiameter) / 2 + paddingTop;

            mCircleBounds = new RectF(xOffset + mBarWidth, yOffset + mBarWidth, xOffset + circleDiameter - mBarWidth,
                    yOffset + circleDiameter - mBarWidth);
        } else {
            mCircleBounds = new RectF(paddingLeft + mBarWidth, paddingTop + mBarWidth,
                    layoutWidth - paddingRight - mBarWidth,
                    layoutHeight - paddingBottom - mBarWidth);
        }
    }

    public void parseAttributes(TypedArray array) {
        DisplayMetrics metrics = getContext().getResources().getDisplayMetrics();
        mBarWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, mBarWidth, metrics);
        mRimWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, mRimWidth, metrics);

        mCircleRadius = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, mCircleRadius, metrics);
        mCircleRadius = (int) array.getDimension(R.styleable.ProgressWheel_circleRadius, mCircleRadius);
        mFilledRadius = array.getBoolean(R.styleable.ProgressWheel_fillRadius, false);

        mBarWidth = (int) array.getDimension(R.styleable.ProgressWheel_barWidth, mBarWidth);
        mRimWidth = (int) array.getDimension(R.styleable.ProgressWheel_rimWidth, mRimWidth);

        float baseSpinSpeed = array.getFloat(R.styleable.ProgressWheel_spinSpeed, mSpinSpeed / 360.0f);
        mSpinSpeed = baseSpinSpeed * 360;

        mBarSpinCycleTime = array.getInt(R.styleable.ProgressWheel_barSpinCycleTime, (int) mBarSpinCycleTime);
        mBarColor = array.getColor(R.styleable.ProgressWheel_barColor, mBarColor);
        mRimColor = array.getColor(R.styleable.ProgressWheel_rimColor, mRimColor);

        mLinearProgress = array.getBoolean(R.styleable.ProgressWheel_linearProgress, false);
        if (array.getBoolean(R.styleable.ProgressWheel_progressIndeterminate, false)) {
            spin();
        }
        array.recycle();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawArc(mCircleBounds, 360, 360, false, mRimPaint);
        boolean mustInvalidate = false;
        if (mIsSpinning) {
            mustInvalidate = true;
            long deltaTime = (SystemClock.uptimeMillis() - mLastTimeAnimated);
            float deltaNormalized = deltaTime * mSpinSpeed / 1000.0f;
            updateBarLength(deltaTime);
            mProgress += deltaNormalized;
            if (mProgress > 360) {
                mProgress -= 360f;
                // A full turn has been completed
                // we run the call back with -1 in case we want to do something, like changing the color
                runCallback(-1.0f);
            }
            mLastTimeAnimated = SystemClock.uptimeMillis();
            float from = mProgress - 90;
            float length = mBarLength + mBarExtrasLength;

            if (isInEditMode()) {
                from = 0;
                length = 135;
            }

            canvas.drawArc(mCircleBounds, from, length, false, mBarPaint);
        } else {
            float oldProgress = mProgress;
            if (mProgress != mTargetProgress) {
                mustInvalidate = true;
                float deltaTime = (float) (SystemClock.uptimeMillis() - mLastTimeAnimated) / 1000;
                float deltaNormalized = deltaTime * mSpinSpeed;

                mProgress = Math.min(mProgress + deltaNormalized, mTargetProgress);
                mLastTimeAnimated = SystemClock.uptimeMillis();
            }

            if (oldProgress != mProgress) {
                runCallback();
            }

            float offset = 0.0f;
            float progress = mProgress;
            if (!mLinearProgress) {
                float factor = 2.0f;
                offset = (float) (1.0f - Math.pow(1.0f - mProgress / 360.0f, 2.0f * factor)) * 360.0f;
                progress = (float) (1.0f - Math.pow(1.0f - mProgress / 360.0f, factor)) * 360.0f;
            }

            if (isInEditMode()) {
                progress = 360;
            }
            canvas.drawArc(mCircleBounds, offset - 90, progress, false, mBarPaint);
        }

        if (mustInvalidate) {
            invalidate();
        }
    }

    @Override
    protected void onVisibilityChanged(View changedView, int visibility) {
        super.onVisibilityChanged(changedView, visibility);
        if (visibility == VISIBLE) {
            mLastTimeAnimated = SystemClock.uptimeMillis();
        }
    }

    private void updateBarLength(long deltaTimeMillisSeconds) {
        if (mPausedTimeWithoutGrowing >= mPausedGrowingTime) {
            mTimeStartGrowing += deltaTimeMillisSeconds;

            if (mTimeStartGrowing > mBarSpinCycleTime) {
                mTimeStartGrowing -= mBarSpinCycleTime;
                mPausedTimeWithoutGrowing = 0;
                mBarGrowingFromFront = !mBarGrowingFromFront;
            }

            float distance = (float) Math.cos((mTimeStartGrowing / mBarSpinCycleTime + 1) * Math.PI) / 2 + 0.5f;
            float destLength = (mBarMaxLength - mBarLength);
            if (mBarGrowingFromFront) {
                mBarExtrasLength = distance * destLength;
            } else {
                float newLength = destLength * (1 - distance);
                mProgress += (mBarExtrasLength - newLength);
                mBarExtrasLength = newLength;
            }
        } else {
            mPausedTimeWithoutGrowing += deltaTimeMillisSeconds;
        }
    }

    public boolean isSpinning() {
        return mIsSpinning;
    }

    public void resetCount() {
        mProgress = 0.0f;
        mTargetProgress = 0.0f;
        invalidate();
    }

    public void stopSpinning() {
        mIsSpinning = false;
        mProgress = 0.0f;
        mTargetProgress = 0.0f;
        invalidate();
    }

    public void setCallback(ProgressCallback progressCallback) {
        mCallback = progressCallback;
        if (!isSpinning()) {
            runCallback();
        }
    }

    public void spin() {
        mLastTimeAnimated = SystemClock.uptimeMillis();
        mIsSpinning = true;
        invalidate();
    }

    public void setProgress(float progress) {
        if (mIsSpinning) {
            mProgress = 0.0f;
            mIsSpinning = false;
            runCallback();
        }
        if (progress > 1.0f) {
            progress -= 1.0f;
        } else if (progress < 0) {
            progress = 0;
        }

        if (progress == mTargetProgress) {
            return;
        }

        if (mProgress == mTargetProgress) {
            mLastTimeAnimated = SystemClock.uptimeMillis();
        }

        mTargetProgress = Math.min(progress * 360.0f, 360.0f);
        invalidate();
    }

    public void setInstantProgress(float progress) {
        if (mIsSpinning) {
            mProgress = 0.0f;
            mIsSpinning = false;
        }
        if (progress > 1.0f) {
            progress -= 1.0f;
        } else if (progress < 0) {
            progress = 0;
        }
        if (progress == mTargetProgress) {
            return;
        }

        mTargetProgress = Math.min(progress * 360.0f, 360.0f);
        mProgress = mTargetProgress;
        mLastTimeAnimated = SystemClock.uptimeMillis();
        invalidate();
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        WheelSavedState ss = new WheelSavedState(superState);

        ss.mProgress = this.mProgress;
        ss.mTargetProgress = this.mTargetProgress;
        ss.mIsSpinning = this.mIsSpinning;
        ss.mSpinSpeed = this.mSpinSpeed;
        ss.mBarWidth = this.mBarWidth;
        ss.mBarColor = this.mBarColor;
        ss.mRimWidth = this.mRimWidth;
        ss.mRimColor = this.mRimColor;
        ss.mCircleRadius = this.mCircleRadius;
        ss.mLinearProgress = this.mLinearProgress;
        ss.mFilledRadius = this.mFilledRadius;

        return ss;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        if (!(state instanceof WheelSavedState)) {
            super.onRestoreInstanceState(state);
            return;
        }
        WheelSavedState ss = (WheelSavedState) state;
        super.onRestoreInstanceState(ss.getSuperState());

        this.mProgress = ss.mProgress;
        this.mTargetProgress = ss.mTargetProgress;
        this.mIsSpinning = ss.mIsSpinning;
        this.mSpinSpeed = ss.mSpinSpeed;
        this.mBarWidth = ss.mBarWidth;
        this.mBarColor = ss.mBarColor;
        this.mRimWidth = ss.mRimWidth;
        this.mRimColor = ss.mRimColor;
        this.mCircleRadius = ss.mCircleRadius;
        this.mLinearProgress = ss.mLinearProgress;
        this.mFilledRadius = ss.mFilledRadius;

        this.mLastTimeAnimated = SystemClock.uptimeMillis();
    }

    public float getProgress() {
        return mIsSpinning ? -1 : mProgress / 360.0f;
    }

    public void setLinearProgress(boolean linearProgress) {
        mLinearProgress = linearProgress;
        if (!mIsSpinning) {
            invalidate();
        }
    }

    public int getCircleRadius() {
        return mCircleRadius;
    }

    public void setCircleRadius(int circleRadius) {
        this.mCircleRadius = circleRadius;
        if (!mIsSpinning) {
            invalidate();
        }
    }

    public int getBarWidth() {
        return mBarWidth;
    }

    public void setBarWidth(int barWidth) {
        this.mBarWidth = barWidth;
        if (!mIsSpinning) {
            invalidate();
        }
    }

    public int getBarColor() {
        return this.mBarColor;
    }

    public void setBarColor(int barColor) {
        this.mBarColor = barColor;
        setupPaints();
        if (!mIsSpinning) {
            invalidate();
        }
    }

    public int getRimColor() {
        return this.mRimColor;
    }

    public void setRimColor(int rimColor) {
        this.mRimColor = rimColor;
        setupPaints();
        if (!mIsSpinning) {
            invalidate();
        }
    }

    public float getSpinSpeed() {
        return mSpinSpeed / 360.0f;
    }

    public void setSpinSpeed(float spinSpeed) {
        this.mSpinSpeed = spinSpeed * 360.0f;
    }

    public int getRimWidth() {
        return this.mRimWidth;
    }

    public void setRimWidth(int rimWidth) {
        this.mRimWidth = rimWidth;
        if (!mIsSpinning) {
            invalidate();
        }
    }


    public void runCallback(float value) {
        if (mCallback != null) {
            mCallback.onProgressUpdate(value);
        }
    }

    public void runCallback() {
        if (mCallback != null) {
            float normalizedProgress = (float) Math.round(mProgress * 100 / 360.0f) / 100;
            mCallback.onProgressUpdate(normalizedProgress);
        }
    }

    static class WheelSavedState extends BaseSavedState {
        float mProgress;
        float mTargetProgress;
        boolean mIsSpinning;
        float mSpinSpeed;
        int mBarWidth;
        int mBarColor;
        int mRimWidth;
        int mRimColor;
        int mCircleRadius;

        boolean mLinearProgress;
        boolean mFilledRadius;

        public WheelSavedState(Parcel source) {
            super(source);
            this.mProgress = source.readFloat();
            this.mTargetProgress = source.readFloat();
            this.mIsSpinning = source.readByte() != 0;
            this.mSpinSpeed = source.readFloat();
            this.mBarWidth = source.readInt();
            this.mBarColor = source.readInt();
            this.mRimWidth = source.readInt();
            this.mRimColor = source.readInt();
            this.mCircleRadius = source.readInt();
            this.mLinearProgress = source.readByte() != 0;
            this.mFilledRadius = source.readByte() != 0;
        }

        public WheelSavedState(Parcelable superState) {
            super(superState);
        }

        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeFloat(this.mProgress);
            out.writeFloat(this.mTargetProgress);
            out.writeByte((byte) (mIsSpinning ? 1 : 0));
            out.writeFloat(this.mSpinSpeed);
            out.writeInt(this.mBarWidth);
            out.writeInt(this.mBarColor);
            out.writeInt(this.mRimWidth);
            out.writeInt(this.mRimColor);
            out.writeInt(this.mCircleRadius);
            out.writeByte((byte) (mLinearProgress ? 1 : 0));
            out.writeByte((byte) (mFilledRadius ? 1 : 0));
        }

        public static final Parcelable.Creator<WheelSavedState> CREATOR = new Parcelable.Creator<WheelSavedState>() {

            @Override
            public WheelSavedState createFromParcel(Parcel source) {
                return new WheelSavedState(source);
            }

            @Override
            public WheelSavedState[] newArray(int size) {
                return new WheelSavedState[size];
            }
        };
    }

    public interface ProgressCallback {

        /**
         * Method to call when the progress reaches a value in order to avoid float precision issues,
         * the progress is rounded to a float with two decimals.
         * <p>
         * In indeterminate mode, the callback is called each time the wheel completes an animation cycle,
         * with, the progress value is -1.0f
         *
         * @param progress a double value between 0.00 and 1.00 both
         */
         void onProgressUpdate(float progress);
    }
}
