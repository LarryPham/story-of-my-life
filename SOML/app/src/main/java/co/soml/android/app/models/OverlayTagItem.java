/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/3/15 9:26 AM.
 **/

package co.soml.android.app.models;

import java.io.Serializable;

/**
 * The class <code>OverlayTagItem</code> which represent to describe the content for tagging the text into the overlay-ImageView
 */
public class OverlayTagItem implements Serializable {
    private static final long serialVersionUID = 2342947293472934L;
    private long mId;
    private int mType;
    private String mName;
    private double mX = -1;
    private double mY = -1;

    private int mRecordCount;
    private boolean mLeft = true;

    public boolean isLeft() {
        return mLeft;
    }

    public void setLeft(boolean left) {
        this.mLeft = left;
    }

    public int getRecordCount() {
        return mRecordCount;
    }

    public void setRecordCount(int recordCount) {
        this.mRecordCount = recordCount;
    }

    public OverlayTagItem() {

    }

    public OverlayTagItem(int type, String label) {
        this.mType = type;
        this.mName = label;
    }

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        this.mId = id;
    }

    public int getType() {
        return mType;
    }

    public void setType(int type) {
        this.mType = type;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public double getX() {
        return mX;
    }

    public double getY() {
        return mY;
    }

    public void setX(double x) {
        this.mX = x;
    }

    public void setY(double y) {
        this.mY = y;
    }
}
