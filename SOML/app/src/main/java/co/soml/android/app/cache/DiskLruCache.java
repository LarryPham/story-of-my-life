/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 * @author: Larry Pham (email: larrypham.vn@gmail.com)
 * Date: 6/11/15 2:23 PM
 **/

package co.soml.android.app.cache;

import java.io.Closeable;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.LinkedHashMap;
import java.util.Map;

import co.soml.android.app.utils.LogUtils;

public class DiskLruCache implements Closeable {
    public static final String TAG = LogUtils.makeLogTag(DiskLruCache.class.getSimpleName());

    public static final String JOURNAL_FILE = "journal";
    public static final String JOURNAL_FILE_TEMP = "journal.temp";

    public static final String MAGIC = "libcore.io.DiskLruCache";
    public static final String VERSION_1 = "1";

    public static final long ANY_SEQUENCE_NUMBER = -1;

    public static final String CLEAN = "CLEAN";
    public static final String DIRTY = "DIRTY";
    public static final String REMOVE = "REMOVE";
    public static final String READ = "READ";

    public static final Charset UTF_8 = Charset.forName("UTF-8");
    // 8MB for using as buffer memory
    private static final int IO_BUFFER_SIZE = 8 * 1024;

    /*private final File mDirectory;

    private final File mJournalFile;
    private final File mJournalFileTemp;
    private final int mAppVersion;
    private final long mMaxSize;

    private final int mValueCount;*/
    private long mSize = 0;

    private Writer mJournalWriter;
    private final LinkedHashMap<String, Map.Entry> mLruEntries = new LinkedHashMap<String, Map.Entry>(0, 0.75f, true);

    @Override
    public void close() throws IOException {

    }
}
