/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/3/15 1:42 PM.
 **/

package co.soml.android.app.camera;

import java.util.ArrayList;
import java.util.List;

import co.soml.android.app.camera.effect.FilterEffect;
import co.soml.android.app.camera.utils.GPUImageFilterTools;
import co.soml.android.app.utils.Lists;
import co.soml.android.app.utils.LogUtils;
import jp.co.cyberagent.android.gpuimage.GPUImage;
import jp.co.cyberagent.android.gpuimage.GPUImageFilter;

/**
 * The class <code>EffectManager</code> represent to manage the effects from ImageFilter types.
 */
public class EffectManager {

    public static final String TAG = LogUtils.makeLogTag(EffectManager.class.getSimpleName());
    private static EffectManager mInstance;

    public static EffectManager getInstance() {
        if (mInstance == null) {
            synchronized (EffectManager.class) {
                if (mInstance == null) {
                    mInstance = new EffectManager();
                }
            }
        }
        return mInstance;
    }

    private EffectManager() {

    }

    public List<FilterEffect> getLocalFilters() {
        List<FilterEffect> filters = Lists.newArrayList();
        filters.add(new FilterEffect("Normal", GPUImageFilterTools.FilterType.NORMAL, 0));
        filters.add(new FilterEffect("Blend Color", GPUImageFilterTools.FilterType.BLEND_COLOR, 0));
        filters.add(new FilterEffect("Saturation", GPUImageFilterTools.FilterType.SATURATION, 0));
        filters.add(new FilterEffect("Opacity", GPUImageFilterTools.FilterType.OPACITY, 0));
        filters.add(new FilterEffect("Pixelization", GPUImageFilterTools.FilterType.PIXELATION, 0));
        filters.add(new FilterEffect("Sepia", GPUImageFilterTools.FilterType.SEPIA, 0));
        filters.add(new FilterEffect("Sobel", GPUImageFilterTools.FilterType.SOBEL_EDGE_DETECTION, 0));
        filters.add(new FilterEffect("Swirl", GPUImageFilterTools.FilterType.SWIRL, 0));
        filters.add(new FilterEffect("Overlay", GPUImageFilterTools.FilterType.BLEND_OVERLAY, 0));
        filters.add(new FilterEffect("Tone curve", GPUImageFilterTools.FilterType.TONE_CURVE, 0));
        filters.add(new FilterEffect("Color Dodge", GPUImageFilterTools.FilterType.BLEND_COLOR_DODGE, 0));
        filters.add(new FilterEffect("Luminosity", GPUImageFilterTools.FilterType.BLEND_LUMINOSITY, 0));
        filters.add(new FilterEffect("Monochrome", GPUImageFilterTools.FilterType.MONOCHROME, 0));
        filters.add(new FilterEffect("Hard Light", GPUImageFilterTools.FilterType.BLEND_HARD_LIGHT, 0));
        filters.add(new FilterEffect("Lighten", GPUImageFilterTools.FilterType.BLEND_LIGHTEN, 0));
        filters.add(new FilterEffect("Exposure", GPUImageFilterTools.FilterType.EXPOSURE, 0));
        filters.add(new FilterEffect("Sharpen", GPUImageFilterTools.FilterType.SHARPEN, 0));
        filters.add(new FilterEffect("Shadow", GPUImageFilterTools.FilterType.HIGHLIGHT_SHADOW, 0));
        filters.add(new FilterEffect("Amatorka", GPUImageFilterTools.FilterType.LOOKUP_AMATORKA, 0));

        return filters;
    }
}
