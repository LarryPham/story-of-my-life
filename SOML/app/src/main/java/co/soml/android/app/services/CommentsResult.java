/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 * @author: Larry Pham (email: larrypham.vn@gmail.com)
 * Date: 6/8/15 10:54 AM
 **/

package co.soml.android.app.services;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import co.soml.android.app.AppConstants;
import co.soml.android.app.models.JsonComment;

public class CommentsResult {
    public static final String TAG = AppConstants.PREFIX + CommentsResult.class.getSimpleName();

    @Expose
    @SerializedName("comments")
    private List<JsonComment> mUserList = new ArrayList<JsonComment>();

    public CommentsResult() {

    }

    public List<JsonComment> getUserList() {
        return mUserList;
    }

    public void setUserList(List<JsonComment> userList) {
        mUserList = userList;
    }
}
