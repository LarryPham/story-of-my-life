/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/3/15 9:24 AM.
 **/

package co.soml.android.app.widgets;

import android.content.Context;
import android.text.Layout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import co.soml.android.app.R;
import co.soml.android.app.StoryApp;
import co.soml.android.app.models.OverlayTagItem;
import co.soml.android.app.models.Story;
import co.soml.android.app.utils.CommonUtil;
import co.soml.android.app.utils.LogUtils;

public class LabelView extends LinearLayout {
    public static final String TAG = LogUtils.makeLogTag(LabelView.class.getSimpleName());
    private String mStickyText;

    private float mParentWidth = 0;
    private float mParentHeight = 0;
    private StoryTextView mTagContent;
    private int mLeft = -1;
    private int mTop = -1;
    private int mImageWidth = 0;

    private static final int ANIMATION_EACH_OFFSET = 600;
    private boolean mEmptyItem = false;

    public String getStickyText() {
        return mStickyText;
    }

    public void setStickyText(String content) {
        this.mStickyText = content;
    }

    public LabelView(Context context) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.layout_overlay_labelview, this);
        mTagContent = (StoryTextView) findViewById(R.id.label_text_view);
    }

    public LabelView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.layout_overlay_labelview, this);
        mTagContent = (StoryTextView) findViewById(R.id.label_text_view);
    }

    public LabelView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.layout_overlay_labelview, this);
        mTagContent = (StoryTextView) findViewById(R.id.label_text_view);
    }

    public void init(String stickyText) {
        mTagContent.setText(stickyText);
    }

    public void draw(ViewGroup parent, final int left, final int top, boolean isLeft) {
        this.mParentWidth = parent.getWidth();
        if (mParentWidth <= 0) {
            final Context appContext = StoryApp.getContext();
            mParentWidth = CommonUtil.getScreenWidth(appContext);
        }

        this.mParentHeight = mParentWidth;
        if (isLeft) {
        }
    }

    private void setupLocation(int leftLocation, int topLocation) {
        this.mLeft = leftLocation;
    }
}
