/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/6/15 9:28 AM.
 **/

package co.soml.android.app.models;

import java.util.ArrayList;

import co.soml.android.app.utils.StringUtils;

public class Album {
    private static final long serialVersionUID = 234729792375935L;
    private String mAlbumUri;

    private String mTitle;
    private ArrayList<Photo> mPhotos;

    public Album(String title, String uri, ArrayList<Photo> photos) {
        this.mTitle = title;
        this.mAlbumUri = uri;
        this.mPhotos = photos;
    }

    public String getAlbumUri() {
        return mAlbumUri;
    }

    public void setAlbumUri(String albumUri) {
        mAlbumUri = albumUri;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public ArrayList<Photo> getPhotos() {
        return mPhotos;
    }

    public void setPhotos(ArrayList<Photo> photos) {
        mPhotos = photos;
    }

    @Override
    public int hashCode() {
        if (mAlbumUri == null) {
            return super.hashCode();
        } else {
            return mAlbumUri.hashCode();
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof Album)) {
            return StringUtils.equals(mAlbumUri, ((Album) obj).getAlbumUri());
        }
        return false;
    }
}
