package co.soml.android.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its
 * documentation are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable
 * form without the prior written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with
 * respect to the contents, and assumes no responsibility for any errors that might appear in the software and
 * documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.03.2015
 */
public class JsonUser implements Serializable {
    private static final long serialVersionUID = 123345236372323L;
	/**
	 * Id Fields
	 */
    @Expose
    @SerializedName("id")
    private int mId;

	/**
	 * FacebookId Fields
	 */
	@Expose
	@SerializedName("fb_id")
	private long mFacebookId;

	/**
	 * Twitter's Id Fields
	 */
	@Expose
	@SerializedName("fabric_id")
	private long mFabricId;

	/**
	 * Email address field
	 */
    @Expose
    @SerializedName("email")
    private String mEmail;

	/**
	 * Guest check field
	 */
	@Expose
	@SerializedName("guest")
	private boolean mGuest;
	/**
	 * Guest Email Field
	 */
	@Expose
	@SerializedName("guest_email")
	private String mGuestEmail;
	/**
	 * Full Name field
	 */
	@Expose
    @SerializedName("name")
    private String mFullName;
	/**
	 * Fabric Screen Name
	 */
	@Expose
	@SerializedName("fabric_screen_name")
	private String mFabricScreenName;
	/**
	 * Description
	 */
    @Expose
    @SerializedName("description")
    private String mDescription;
	/**
	 * Verified Variable
	 */
	@Expose
	@SerializedName("verified")
	private boolean mVerified;
	/**
	 * Avatar Icon Field
	 */
    @Expose
    @SerializedName("avatar")
    private Avatar mAvatar;
	/**
	 * Language Field
	 */
    @Expose
    @SerializedName("lang")
    private String mLanguage;
	/**
	 * Amount of stories Field
	 */
    @Expose
    @SerializedName("stories_count")
    private int mStoryCount;
	/**
	 * Authenticated Token Field
	 */
    @Expose
    @SerializedName("auth_token")
    private String mAuthToken;
	/**
	 * Fabric Authenticated Token Field
	 */
	@Expose
	@SerializedName("fabric_auth_token")
	private String mFabricAuthToken;
	/**
	 * Facebook Authenticated Token Field
	 */
	@Expose
	@SerializedName("fb_token")
	private String mFaceBookToken;
	/**
	 * Fabric Authenticated Token Secret
	 */
	@Expose
	@SerializedName("fabric_auth_token_secret")
	private String mFabricAuthTokenSecret;
	/**
	 * Sharing Facebook Authenticated Token
	 */
	@Expose
	@SerializedName("sharing_facebook_token")
	private String mSharingFacebookToken;
	/**
	 * Sharing Twitter Token Field
	 */
	@Expose
	@SerializedName("sharing_twitter_token")
	private String mSharingTwitterToken;
	/**
	 * Place Field
	 */
	@Expose
	@SerializedName("place")
	private String mPlace;
	/**
	 * Phone Number Field
	 */
	@Expose
	@SerializedName("phone")
	private String mPhone;
	/**
	 * Roles Field
	 */
	@Expose
	@SerializedName("roles")
	private String[] mRoles;
	/**
	 * Following checkable variable field
	 */
    @Expose
    @SerializedName("following")
    private boolean mFollowing;
	/**
	 * Follower's Amount
	 */
	@Expose
	@SerializedName("users_followed")
	private long mFollowers;
	/**
	 * Following's Amount
	 */
	@Expose
	@SerializedName("users_following")
	private long mFollowings;

	@Expose
	@SerializedName("token")
	private String mToken;

	@Expose
	@SerializedName("token_expired_at")
	private String mTokenExpiredAt;

	@Expose
	@SerializedName("is_followed_by_current_user")
	private boolean mFollowedByCurrentUser;

    public JsonUser() {
    }

    public JsonUser(int id) {
        mId = id;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getFullName() {
        return mFullName;
    }

    public void setFullName(String fullName) {
        mFullName = fullName;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public Avatar getAvatar() {
        return mAvatar;
    }

    public void setAvatar(Avatar avatar) {
        mAvatar = avatar;
    }

    public String getLanguage() {
        return mLanguage;
    }

    public void setLanguage(String language) {
        mLanguage = language;
    }

    public int getStoryCount() {
        return mStoryCount;
    }

    public void setStoryCount(int storyCount) {
        mStoryCount = storyCount;
    }

    public String getAuthToken() {
        return mAuthToken;
    }

    public void setAuthToken(String authToken) {
        mAuthToken = authToken;
    }

    public boolean isFollowing() {
        return mFollowing;
    }

    public void setFollowing(boolean following) {
        mFollowing = following;
    }

	public long getFollowers() {
		return mFollowers;
	}

	public void setFollowers(long followers) {
		mFollowers = followers;
	}

	public long getFollowings() {
		return mFollowings;
	}

	public void setFollowings(long followings) {
		mFollowings = followings;
	}

	public long getFacebookId() {
		return mFacebookId;
	}

	public void setFacebookId(long facebookId) {
		mFacebookId = facebookId;
	}

	public long getFabricId() {
		return mFabricId;
	}

	public void setFabricId(long fabricId) {
		mFabricId = fabricId;
	}

	public boolean isGuest() {
		return mGuest;
	}

	public void setGuest(boolean guest) {
		mGuest = guest;
	}

	public String getGuestEmail() {
		return mGuestEmail;
	}

	public void setGuestEmail(String guestEmail) {
		mGuestEmail = guestEmail;
	}

	public String getFabricScreenName() {
		return mFabricScreenName;
	}

	public void setFabricScreenName(String fabricScreenName) {
		mFabricScreenName = fabricScreenName;
	}

	public boolean isVerified() {
		return mVerified;
	}

	public void setVerified(boolean verified) {
		mVerified = verified;
	}

	public String getFabricAuthToken() {
		return mFabricAuthToken;
	}

	public void setFabricAuthToken(String fabricAuthToken) {
		mFabricAuthToken = fabricAuthToken;
	}

	public String getFaceBookToken() {
		return mFaceBookToken;
	}

	public void setFaceBookToken(String faceBookToken) {
		mFaceBookToken = faceBookToken;
	}

	public String getFabricAuthTokenSecret() {
		return mFabricAuthTokenSecret;
	}

	public void setFabricAuthTokenSecret(String fabricAuthTokenSecret) {
		mFabricAuthTokenSecret = fabricAuthTokenSecret;
	}

	public String getSharingFacebookToken() {
		return mSharingFacebookToken;
	}

	public void setSharingFacebookToken(String sharingFacebookToken) {
		mSharingFacebookToken = sharingFacebookToken;
	}

	public String getSharingTwitterToken() {
		return mSharingTwitterToken;
	}

	public void setSharingTwitterToken(String sharingTwitterToken) {
		mSharingTwitterToken = sharingTwitterToken;
	}

	public String getPlace() {
		return mPlace;
	}

	public void setPlace(String place) {
		mPlace = place;
	}

	public String getPhone() {
		return mPhone;
	}

	public void setPhone(String phone) {
		mPhone = phone;
	}

	public String[] getRoles() {
		return mRoles;
	}

	public void setRoles(String[] roles) {
		mRoles = roles;
	}

	public String getToken() {
		return mToken;
	}

	public void setToken(String token) {
		mToken = token;
	}

	public String getTokenExpiredAt() {
		return mTokenExpiredAt;
	}

	public void setTokenExpiredAt(String tokenExpiredAt) {
		mTokenExpiredAt = tokenExpiredAt;
	}

	public boolean isFollowedByCurrentUser() {
		return mFollowedByCurrentUser;
	}

	public void setFollowedByCurrentUser(boolean followedByCurrentUser) {
		mFollowedByCurrentUser = followedByCurrentUser;
	}

	public class Avatar {
        @Expose
        @SerializedName("url")
        public String mUrl;
        @Expose
        @SerializedName("thumb")
        public Thumb mThumb;
        @Expose
        @SerializedName("small")
        public Small mSmall;
        @Expose
        @SerializedName("medium")
        public Medium mMedium;
        @Expose
        @SerializedName("large")
        public Large mLarge;
    }

    public class Small {
        @Expose
        @SerializedName("url")
        public String mUrl;
    }

    public class Thumb {
        @Expose
        @SerializedName("url")
        public String mUrl;
    }

    public class Medium {
        @Expose
        @SerializedName("url")
        public String mUrl;
    }

    public class Large {
        @Expose
        @SerializedName("url")
        public String mUrl;
    }
}
