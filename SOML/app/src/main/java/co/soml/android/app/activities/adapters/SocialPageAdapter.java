/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 * @author: Larry Pham (email: larrypham.vn@gmail.com)
 * Date: 6/17/15 11:56 AM
 **/

package co.soml.android.app.activities.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import java.lang.ref.WeakReference;
import java.util.List;

import co.soml.android.app.R;
import co.soml.android.app.fragments.BaseFragment;
import co.soml.android.app.fragments.PeopleFragment;
import co.soml.android.app.utils.Lists;
import co.soml.android.app.utils.LogUtils;

public class SocialPageAdapter extends FragmentStatePagerAdapter {

    public static final String TAG = LogUtils.makeLogTag(SocialPageAdapter.class.getSimpleName());

    private final SparseArray<WeakReference<PeopleFragment>> mFragmentArray = new SparseArray<WeakReference<PeopleFragment>>();
    // The List of holder which will restored the fragment's container till they're recleaned it again.
    private List<Holder> mHolderList = Lists.newArrayList();

    private Context mContext;
    private int mCurrentPage;

    private String mUsersType;

    public SocialPageAdapter(FragmentManager fm) {
        super(fm);
    }

    public SocialPageAdapter(Context context, final FragmentManager fragmentManager) {
        super(fragmentManager);
        mContext = context;
    }

    public void add(final Class<? extends BaseFragment> className, final Bundle params) {
        final Holder holder = new Holder();
        holder.mClassName = className.getName();
        holder.mParam = params;
        final int position = mHolderList.size();
        mHolderList.add(position, holder);
        notifyDataSetChanged();
    }

    public PeopleFragment getFragment(final int position) {
        final WeakReference<PeopleFragment> mWeakFragment = mFragmentArray.get(position);
        if (mWeakFragment != null && mWeakFragment.get() != null) {
            return mWeakFragment.get();
        }
        return getItem(position);
    }

    @Override
    public PeopleFragment getItem(int position) {
        final Holder currentHolder = mHolderList.get(position);
        // Instantiating the new fragment with given param such as: ClassName, Param
        return (PeopleFragment) PeopleFragment.instantiate(mContext, currentHolder.mClassName, currentHolder.mParam);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        final PeopleFragment fragment = (PeopleFragment) super.instantiateItem(container, position);
        final WeakReference<PeopleFragment> weakReference = mFragmentArray.get(position);
        if (weakReference != null) {
            weakReference.clear();
        }
        mFragmentArray.put(position, new WeakReference<PeopleFragment>(fragment));
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
        final WeakReference<PeopleFragment> weakFragment = mFragmentArray.get(position);
        if (weakFragment != null) {
            weakFragment.clear();
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getStringArray(R.array.social_page_titles)[position];
    }

    @Override
    public int getCount() {
        return mHolderList.size();
    }

    public int getCurrentPage() {
        return mCurrentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.mCurrentPage = currentPage;
    }

    public enum SocialFragments {
        /**
         * The Following Users Fragment
         */
        FOLLOWING(PeopleFragment.class),
        /**
         * The Followed Users Fragment
         */
        FOLLOWERS(PeopleFragment.class);

        private Class<? extends BaseFragment> mFragmentClass;

        private SocialFragments(final Class<? extends BaseFragment> fragmentClass) {
            mFragmentClass = fragmentClass;
        }

        public Class<? extends BaseFragment> getFragmentClass() {
            return mFragmentClass;
        }
    }

    public final static class Holder {
        String mClassName;
        Bundle mParam;
    }
}
