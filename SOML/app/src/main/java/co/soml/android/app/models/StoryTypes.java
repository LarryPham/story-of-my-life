package co.soml.android.app.models;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since May.29.2015
 * <p>
 * The StoryTypes which represent to separate the kind of stories into feed screen or list of feeditem..
 */
public class StoryTypes {

    public static final StoryPostListType DEFAULT_STORY_POST_LIST_TYPE = StoryPostListType.TRENDING_TYPE;

    public static enum StoryPostListType {
        FOLLOWING_TYPE, TRENDING_TYPE, COLLABORATING_TYPE, LATEST_TYPE, FEATURED_TYPE,
        PUBLISHED_TYPE, PRIVATE_TYPE;

        public boolean isTrendingType() {
            return this.equals(TRENDING_TYPE);
        }

        public boolean isFollowingType() {
            return this.equals(FOLLOWING_TYPE);
        }

        public boolean isCollaboratingType() {
            return this.equals(COLLABORATING_TYPE);
        }

        public boolean isLatestType() {
            return this.equals(LATEST_TYPE);
        }

        public boolean isFeaturedType() {
            return this.equals(FEATURED_TYPE);
        }

        public boolean isPublishedType() {
            return this.equals(PUBLISHED_TYPE);
        }

        public boolean isPrivateType() {
            return this.equals(PRIVATE_TYPE);
        }
    }
}
