/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/7/15 9:56 AM.
 **/

package co.soml.android.app.camera.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.Environment;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;

import co.soml.android.app.StoryApp;
import co.soml.android.app.models.Photo;
import co.soml.android.app.utils.StringUtils;

public class FileUtils {
    private static String BASE_PATH;
    private static String STICKER_BASE_PATH;

    private static FileUtils mInstance;

    public static FileUtils getInstance() {
        if (mInstance == null) {
            synchronized (FileUtils.class) {
                if (mInstance == null) {
                    mInstance = new FileUtils();
                }
            }
        }
        return mInstance;
    }

    public File getExtFile(String path) {
        return new File(BASE_PATH + path);
    }

    public long getFolderSize(File file) {
        try {
            long size = 0;
            if (!file.exists()) {
                return size;
            } else if (!file.isDirectory()) {
                return file.length() / 1024;
            }
            File[] files = file.listFiles();
            for (File elem : files) {
                if (elem.isDirectory()) {
                    size = size + getFolderSize(elem);
                } else {
                    size = size + elem.length();
                }
            }
            return size / 1024;
        } catch (Exception ex) {
            return 0;
        }
    }

    public String getBasePath(int packageId) {
        return STICKER_BASE_PATH + packageId + "/";
    }

    private String getImageFilePath(int packageId, String imageUrl) {
        String md5Str = getMD5(imageUrl).replace("-", "mm");
        return getBasePath(packageId) + md5Str;
    }

    public String readFromAsset(String fileName) {
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;

        try {
            inputStream = StoryApp.getContext().getAssets().open(fileName);
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String addOnStr = "";
            String line = bufferedReader.readLine();
            while (line != null) {
                addOnStr = addOnStr + line;
                line = bufferedReader.readLine();
            }
            return addOnStr;
        } catch (Exception ex) {
            return null;
        } finally {
            closeStream(bufferedReader);
            closeStream(inputStream);
        }
    }

    public void removeAddOnFolder(int packageId) {
        String fileName = getBasePath(packageId);
        File file = new File(fileName);
        if (file.exists()) {
            delete(file);
        }
    }

    public void delete(File file) {
        if (file.isFile()) {
            file.delete();
            return;
        }

        if (file.isDirectory()) {
            File[] childFiles = file.listFiles();
            if (childFiles == null || childFiles.length == 0) {
                file.delete();
                return;
            }

            for (int i = 0; i < childFiles.length; i++) {
                delete(childFiles[i]);
            }
            file.delete();
        }
    }

    public String getPhotoSavedPath() {
        return BASE_PATH + "StickerPhotos";
    }

    public String getPhotoTempPath() {
        return BASE_PATH + "StickerPhotos";
    }

    public String getSystemPhotoPath() {
        return Environment.getExternalStorageDirectory().getAbsolutePath() + "/DCIM/Camera";
    }

    private FileUtils() {
        String sdCardState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(sdCardState)) {
            BASE_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + "/StickerPhotos/";
        } else {
            BASE_PATH = StoryApp.getInstance().getCacheDirPath();
        }
        STICKER_BASE_PATH = BASE_PATH + "/Stickers/";
    }

    public boolean createFile(File file) {
        try {
            if (!file.getParentFile().exists()) {
                mkdir(file.getParentFile());
            }
            return file.createNewFile();
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public boolean mkdir(File file) {
        while (!file.getParentFile().exists()) {
            mkdir(file.getParentFile());
        }
        return file.mkdir();
    }

    public static void closeStream(Closeable stream) {
        try {
            if (stream != null) {
                stream.close();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public File getCacheDir() {
        return StoryApp.getInstance().getCacheDir();
    }

    public boolean writeSimpleString(File file, String string) {
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(string.getBytes());
            return true;
        } catch (Throwable ex) {
            ex.printStackTrace();
            return false;
        } finally {
            closeStream(fileOutputStream);
        }
    }

    public String readSimpleString(File file) {
        StringBuffer buffer = new StringBuffer();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            String line = reader.readLine();
            if (StringUtils.isNotEmpty(line)) {
                buffer.append(line.trim());
                line = reader.readLine();
            }
        } catch (Throwable ex) {
            ex.printStackTrace();
            return "";
        } finally {
            closeStream(reader);
        }
        return buffer.toString();
    }

    public boolean copyAssetDirToFiles(Context context, String dirName) {
        try {
            AssetManager assetManager = context.getAssets();
            String[] children = assetManager.list(dirName);
            for (String child : children) {
                child = dirName + '/' + child;
                String[] grandChildren = assetManager.list(child);
                if (0 == grandChildren.length) {
                    copyAssetDirToFiles(context, child);
                } else {
                    copyAssetDirToFiles(context, child);
                }
            }
            return true;
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public boolean copyAssetFileToFiles(Context context, String fileName) {
        return copyAssetFileToFiles(context, fileName, getExtFile("/" + fileName));
    }

    private boolean copyAssetFileToFiles(Context context, String fileNames, File of) {
        InputStream is = null;
        FileOutputStream os = null;
        try {
            is = context.getAssets().open(fileNames);
            createFile(of);
            os = new FileOutputStream(of);

            int readBytes;
            byte[] buffer = new byte[1024];
            while ((readBytes = is.read(buffer)) > 0) {
                os.write(buffer, 0, readBytes);
            }
            os.flush();
            return true;
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        } finally {
            closeStream(is);
            closeStream(os);
        }
    }

    public boolean renameDir(String oldDir, String newDir) {
        File of = new File(oldDir);
        File nf = new File(newDir);
        return of.exists() && !nf.exists() && of.renameTo(nf);
    }

    public static String getMD5(String value) {
        MessageDigest md5;
        try {
            md5 = MessageDigest.getInstance("MD5");
            md5.update(value.getBytes());
            byte[] m = md5.digest();
            return getMD5String(m);
        } catch (NoSuchAlgorithmException ex) {
            return value;
        }
    }

    private static String getMD5String(byte[] bytes) {
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < buffer.length(); i++) {
            buffer.append(bytes[i]);
        }
        return buffer.toString();
    }

    public ArrayList<Photo> findPicsInDir(final String path) {
        ArrayList<Photo> photos = new ArrayList<>();
        File dir = new File(path);
        if (dir.exists() && dir.isDirectory()) {
            for (File file : dir.listFiles(new FileFilter() {
                @Override
                public boolean accept(File pathname) {
                    String filePath = pathname.getAbsolutePath();
                    return (filePath.endsWith(".png") || filePath.endsWith(".jpg") || filePath.endsWith(".jepg"));
                }
            })) {
                //photos.add(new Photo(file.getAbsolutePath(), file.lastModified()));
            }
        }
        Collections.sort(photos);
        return photos;
    }
}
