package co.soml.android.app.models;

import android.content.Context;
import android.content.SharedPreferences;

import co.soml.android.app.AppConstants;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.09.2015
 * <p>
 * The class <code>StoreySettings</code> used to store the preferences key or also used to store the social network's tokens for every
 * components will be accessible for it.
 */
public class StorySettings {
    public static final String TAG = AppConstants.PREFIX + StorySettings.class.getSimpleName();
    public static final String PREFS_ACCESS_TOKEN = "ACCESS_TOKEN";
    private SharedPreferences mSettings;
    private SharedPreferences.Editor mSettingsEditor;

    private static final String PREFS_NAME = "Settings";
    private static final String AUTO_UPDATE = "AutoUpdate";

    public StorySettings(Context context) {
        this.mSettings = context.getSharedPreferences(StorySettings.PREFS_NAME, Context.MODE_PRIVATE);
        this.mSettingsEditor = this.mSettings.edit();
    }

    public void resetToDefault() {
        mSettingsEditor.clear();
        mSettingsEditor.commit();
    }

    public int getAutoUpdate() {
        return this.mSettings.getInt(AUTO_UPDATE, 30);
    }

    public void setAutoUpdate(int value) {
        mSettingsEditor.putInt(AUTO_UPDATE, value);
        mSettingsEditor.commit();
    }

    // Get access token to preferences to current app
    public String getAccessToken() {
        return this.mSettings.getString(PREFS_ACCESS_TOKEN, null);
    }

    // Saving access token to preferences of current app
    public void setAccessToken(String accessToken) {
        mSettingsEditor.putString(PREFS_ACCESS_TOKEN, accessToken);
        mSettingsEditor.commit();
    }

}
