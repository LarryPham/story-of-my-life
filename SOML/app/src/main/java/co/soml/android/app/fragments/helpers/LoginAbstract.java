package co.soml.android.app.fragments.helpers;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since May.07.2015
 * <p>
 * LoginAbstract, an abstract class which describe the blueprint for other derived classes which will uses them as the methods for
 * handling the authentication's cases.
 */
public abstract class LoginAbstract {
    protected String mUserEmail;
    protected String mPassword;
    protected Callback mCallback;

    /**
     * The Callback interface which will be included 2 method for feeding back the result of authentication's processes.
     * For success signal of authentication, it will be implemented the onSuccess() method and otherwise, into onError method we will
     * process the error signals when authenticating the user account by email and password.
     */
    public interface Callback {
        void onSuccess();

        void onError(int errorMessageId);
    }

    public LoginAbstract(String userEmail, String password) {
        mUserEmail = userEmail;
        mPassword = password;
    }

    public void execute(Callback callback) {
        mCallback = callback;
        new Thread() {
            @Override
            public void run() {
                login();
            }
        }.start();
    }

    protected abstract void login();
}
