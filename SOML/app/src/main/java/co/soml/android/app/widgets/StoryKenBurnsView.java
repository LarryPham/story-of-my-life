/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 * @author: Larry Pham (email: larrypham.vn@gmail.com)
 * Date: 6/8/15 11:30 AM
 **/

package co.soml.android.app.widgets;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.webkit.URLUtil;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;

import co.soml.android.app.R;
import co.soml.android.app.services.tasks.WeakHandler;
import co.soml.android.app.utils.LogUtils;

public class StoryKenBurnsView extends FrameLayout implements View.OnClickListener {
    public static final String TAG = LogUtils.makeLogTag(StoryKenBurnsView.class.getSimpleName());

    private final WeakHandler mWeakHandler;
    private ArrayList<String> mImageUrls = new ArrayList<String>();
    private ImageView[] mImageViews;
    private int mActiveImageIndex = -1;
    private int mActiveUrlIndex = -1;

    private final Random mRandom = new Random();

    private int mSwapMilliSeconds = 10000;
    private int mFadeInOutMilliSeconds = 400;

    private float mMaxScaleFactor = 1.5F;
    private float mMinScaleFactor = 1.0F;
    private ImageButton mPlayButton;
    private boolean mIsPlaying = false;
    private SquaredFrameLayout mKenBurnsViewFrame;

    private Runnable mSwapImagesRunnable = new Runnable() {
        @Override
        public void run() {
            onSwapImages();
            mWeakHandler.postDelayed(mSwapImagesRunnable, mSwapMilliSeconds - mFadeInOutMilliSeconds * 2);
        }
    };

    private OnClickListener mPlayClickListener = new OnClickListener() {
        @Override
        public void onClick(View playButton) {
            if (mIsPlaying) {
                mIsPlaying = false;
                mPlayButton.setVisibility(View.VISIBLE);
                stopKenBurnsEffect();
            } else {
                mIsPlaying = true;
                mPlayButton.setVisibility(View.GONE);
                startKenBurnsEffect();
            }
        }
    };

    public int getSwapMilliSeconds() {
        return mSwapMilliSeconds;
    }

    public void setSwapMilliSeconds(int swapMilliSeconds) {
        mSwapMilliSeconds = swapMilliSeconds;
    }

    public int getFadeInOutMilliSeconds() {
        return mFadeInOutMilliSeconds;
    }

    public void setFadeInOutMilliSeconds(int fadeInOutMilliSeconds) {
        mFadeInOutMilliSeconds = fadeInOutMilliSeconds;
    }

    public float getMaxScaleFactor() {
        return mMaxScaleFactor;
    }

    public void setMaxScaleFactor(float maxScaleFactor) {
        mMaxScaleFactor = maxScaleFactor;
    }

    public float getMinScaleFactor() {
        return mMinScaleFactor;
    }

    public void setMinScaleFactor(float minScaleFactor) {
        mMinScaleFactor = minScaleFactor;
    }

    public StoryKenBurnsView(Context context) {
        this(context, null);
    }

    public StoryKenBurnsView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public StoryKenBurnsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mWeakHandler = new WeakHandler();
    }

    public void setImageUrls(ArrayList<String> imageUrls) {
        if (mImageUrls != null) {
            mImageUrls.clear();
            mImageUrls.addAll(imageUrls);
        }
        onFillImageViews();
    }

    public void setDefaultImage(int resId) {
        mImageViews[0].setImageResource(resId);
        mWeakHandler.removeCallbacks(mSwapImagesRunnable);
    }

    public void onSwapImages() {
        LogUtils.LOGD(TAG, "Swapping Active: " + mActiveImageIndex);
        if (mActiveImageIndex == -1) {
            mActiveImageIndex = 1;
            mActiveUrlIndex = 1;

            if (mImageUrls.size() > 0) {
                String uri = mImageUrls.get(0);

                if (URLUtil.isValidUrl(uri)) {
                    Picasso.with(getContext()).load(uri).into(mImageViews[mActiveImageIndex]);
                } else {
                    Picasso.with(getContext()).load(new File(uri)).into(mImageViews[mActiveImageIndex]);
                }
            }
            animate(mImageViews[mActiveImageIndex]);

            return;
        }

        int inActiveIndex = mActiveImageIndex;
        mActiveImageIndex = (1 + mActiveImageIndex) % mImageViews.length;

        LogUtils.LOGD(TAG, "New Active: " + mActiveImageIndex);

        final ImageView activeImageView = mImageViews[mActiveImageIndex];
        activeImageView.setAlpha(0.0f);

        if (mActiveUrlIndex < mImageUrls.size() - 1) {
            mActiveUrlIndex++;
        } else {
            mActiveUrlIndex = 0;
        }

        String uri = mImageUrls.get(mActiveUrlIndex);
        if (URLUtil.isValidUrl(uri)) {
            Picasso.with(getContext()).load(uri).into(activeImageView);
        } else {
            Picasso.with(getContext()).load(new File(uri)).into(activeImageView);
        }

        final ImageView inActiveImageView = mImageViews[inActiveIndex];
        animate(activeImageView);

        final AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(mFadeInOutMilliSeconds);
        // Hiding inactive image, and displaying the active image
        animatorSet.playTogether(ObjectAnimator.ofFloat(inActiveImageView, "alpha", 1.0f, 0.0f),
                ObjectAnimator.ofFloat(activeImageView, "alpha", 0.0f, 1.0f));
        animatorSet.start();
    }

    public void start(View view, long duration, float fromScale, float toScale, float fromTranslationX, float fromTranslationY,
                      float toTranslationX, float toTranslationY) {
        view.setScaleX(fromScale);
        view.setScaleY(fromScale);
        view.setTranslationX(fromTranslationX);
        view.setTranslationY(fromTranslationY);

        ViewPropertyAnimator propertyAnimator = view.animate().translationX(toTranslationX).translationY(toTranslationY).scaleX(toScale)
                .scaleY(toScale).setDuration(duration);
        propertyAnimator.start();
        LogUtils.LOGD(TAG, "Starting KenBurns Effect Animation " + propertyAnimator);
    }

    private float pickScale() {
        return this.mMinScaleFactor + this.mRandom.nextFloat() * (this.mMaxScaleFactor - this.mMinScaleFactor);
    }

    private float pickTranslation(int value, float ratio) {
        return value * (ratio - 1.0f) * (this.mRandom.nextFloat() - 0.5f);
    }

    public void animate(View view) {
        float fromScale = pickScale();
        float toScale = pickScale();

        float fromTranslationX = pickTranslation(view.getWidth(), fromScale);
        float fromTranslationY = pickTranslation(view.getHeight(), fromScale);

        float toTranslationX = pickTranslation(view.getWidth(), toScale);
        float toTranslationY = pickTranslation(view.getHeight(), toScale);

        start(view, this.mSwapMilliSeconds, fromScale, toScale, fromTranslationX, fromTranslationY, toTranslationX, toTranslationY);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (mPlayClickListener != null) {
            mPlayButton.setOnClickListener(mPlayClickListener);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        stopKenBurnsEffect();
    }

    public void startKenBurnsEffect() {
        // Starting to swap images into array of imageviews
        mWeakHandler.post(mSwapImagesRunnable);
    }

    public void stopKenBurnsEffect() {
        // Removing the callbacks for swap images into array of imageviews
        mWeakHandler.removeCallbacks(mSwapImagesRunnable);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        View kenView = inflate(getContext(), R.layout.view_kenburns, this);
        mPlayButton = (ImageButton) kenView.findViewById(R.id.play_button);
        mImageViews = new ImageView[2];
        mImageViews[0] = (ImageView) kenView.findViewById(R.id.previous_images);
        mImageViews[1] = (ImageView) kenView.findViewById(R.id.next_images);
    }

    public void onFillImageViews() {
        //final Bitmap imageBitmap= Picasso.with(getContext()).load(mImageUrls.get(i)).get();
        //mImageViews[modulo].setImageBitmap(imageBitmap);
        //mImageViews[modulo].invalidate();
        for (int i = 0; i < mImageViews.length; i++) {
            if (i < mImageUrls.size()) {
                String uri = mImageUrls.get(i);
                if (URLUtil.isValidUrl(uri)) {
                    Picasso.with(getContext()).load(uri).into(mImageViews[i]);
                } else {
                    Picasso.with(getContext()).load(new File(uri)).into(mImageViews[i]);
                }
            } else {
                String uri = mImageUrls.get(0);
                if (URLUtil.isValidUrl(uri)) {
                    Picasso.with(getContext()).load(uri).into(mImageViews[i]);
                } else {
                    Picasso.with(getContext()).load(new File(uri)).into(mImageViews[i]);
                }
            }
        }
    }

    @Override
    public void onClick(View kenBurnsView) {
        if (mPlayButton.getVisibility() == View.GONE) {
            mPlayButton.setVisibility(View.VISIBLE);
            mPlayClickListener.onClick(mPlayButton);
        } else {
            mPlayButton.setVisibility(View.GONE);
            mPlayClickListener.onClick(mPlayButton);
        }
    }
}
