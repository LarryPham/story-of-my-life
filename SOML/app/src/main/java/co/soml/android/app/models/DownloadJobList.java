package co.soml.android.app.models;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import co.soml.android.app.AppConstants;
import co.soml.android.app.models.DownloadJob.DownloadState;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its
 * documentation are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable
 * form without the prior written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with
 * respect to the contents, and assumes no responsibility for any errors that might appear in the software and
 * documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.03.2015
 */
public class DownloadJobList extends BaseModel {
    public static final String TAG = AppConstants.PREFIX + DownloadJobList.class.getSimpleName();
    private static List<DownloadJob> mDownloadJobList = new ArrayList<DownloadJob>();
    private static int mInsertPosition = 0;

    public DownloadJobList() {

    }

    public DownloadJob add(String origin, String dest) {
        final DownloadJob downloadJob = new DownloadJob(origin, dest);
        mDownloadJobList.add(downloadJob);
        return downloadJob;
    }

    public void addEnd(DownloadJob pausedJob) {
        mDownloadJobList.add(pausedJob);
    }

    public void addSequential(DownloadJob pausedJob) {
        mDownloadJobList.add(mInsertPosition, pausedJob);
        mInsertPosition++;
    }

    public int findLocation(int id) {
        DownloadJob job = null;
        int location = 0;
        for (Iterator<DownloadJob> iterator = mDownloadJobList.iterator(); iterator.hasNext(); ) {
            job = iterator.next();
            if (job.getId() == id) {
                return location;
            }
            location++;
        }
        return -1;
    }

    public void remove(int id) {
        int location;
        if (id != 0) {
            location = findLocation(id);
            if (location != -1) {
                mDownloadJobList.remove(location);
            }
        }
    }

    public DownloadJob getLastDownloadJob() {
        DownloadJob downloadJob = null;
        downloadJob = mDownloadJobList.get(mDownloadJobList.size() - 1);
        return downloadJob;
    }

    public DownloadJob getAt(Integer key) {
        int location = findLocation(key);
        if (location != -1) {
            return mDownloadJobList.get(location);
        }
        return null;
    }

    public List<DownloadJob> getInnerList() {
        return mDownloadJobList;
    }

    public int getDownloadProgress(int dbId) {
        DownloadJob job = null;
        for (Iterator<DownloadJob> iterator = mDownloadJobList.iterator(); iterator.hasNext(); ) {
            job = iterator.next();
            //Todo checking if job has story with special id equals to id
            // Then getting progress value from it.
        }
        return -1;
    }

    public DownloadState getDownloadState(int dbId) {
        DownloadJob job = null;
        for (Iterator<DownloadJob> iterator = mDownloadJobList.iterator(); iterator.hasNext(); ) {
            job = iterator.next();
            // Todo checking the guid for story or photo
            // then return the job.getDownloadState
        }
        return DownloadState.NONE;
    }

    public int getDownloadJobId(int dbId) {
        DownloadJob downloadJob = null;
        for (Iterator<DownloadJob> iterator = mDownloadJobList.iterator(); iterator.hasNext(); ) {
            downloadJob = iterator.next();
            // Todo checking the enclosure id and then return id of this job
        }
        return -1;
    }

    public int getDownloadJobId(String guid) {
        return -1;
    }

    public int getDownloadJobCount() {
        return mDownloadJobList.size();
    }
}
