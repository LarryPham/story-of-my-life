package co.soml.android.app.services;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

import co.soml.android.app.AppConstants;
import co.soml.android.app.StoryApp;
import co.soml.android.app.controllers.ControllerMessage;
import co.soml.android.app.models.Error;
import co.soml.android.app.services.tasks.Action;
import co.soml.android.app.services.tasks.ServerRequestTask;
import co.soml.android.app.services.tasks.ServerSubscribeTask;
import co.soml.android.app.utils.AccountUtils;
import co.soml.android.app.utils.LogUtils;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its
 * documentation are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable
 * form without the prior written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with
 * respect to the contents, and assumes no responsibility for any errors that might appear in the software and
 * documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.01.2015
 * <p>
 * The Service component represent to trigger the background task for subscribing customized information from client to server or getting
 * the data from server to client (subscribe or get).
 */
public class StoryService extends Service {

    private static final String TAG = AppConstants.PREFIX + StoryService.class.getSimpleName();
    private Handler mMainHandler;
    private StoryApp mStoryApp;

    private static final int THREE_MINUTES_UPDATE_INTERVAL = 3 * 1000 * 60;
    private static final int TEN_MINUTES_UPDATE_INTERVAL = 10 * 1000 * 60;
    private static final int FIFTHTEEN_MINUTES_UPDATE_INTERVAL = 15 * 1000 * 60;
    private static final int THIRDTY_MINUTES_UPDATE_INTERVAL = 30 * 1000 * 60;

    private static final int ONE_HOUR_UPDATE_INTERVAL = 1000 * 60 * 60;
    private static final int SIX_HOURS_UPDATE_INTERVAL = 1000 * 60 * 60 * 6;
    private static final int ONE_DAY_UPDATE_INTERVAL = 1000 * 60 * 60 * 24;
    private static final int THREE_DAYS_UPDATE_INTERVAL = 1000 * 60 * 60 * 24 * 3;

    @Override
    public void onCreate() {
        super.onCreate();
        mStoryApp = (StoryApp) this.getApplicationContext();
        mMainHandler = mStoryApp.getAppMainHandler();
        mStoryApp.setStoryService(this);
        registerReceiver(mReceiver, new IntentFilter(Action.FINISH_APPLICATION));
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            LogUtils.LOGD(TAG, String.format("OnReceive: Action[%s]", action));
            if (action.equals(Action.FINISH_APPLICATION)) {
                LogUtils.LOGD(TAG, String.format("BroadcastReceiver() - Action.FINISH_APPLICATION"));
                StoryService.this.stopSelf();
            }
        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        LogUtils.LOGD(TAG, "onBind(): ");
        return mIBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if ((flags & START_FLAG_RETRY) == 0) {
            // Todo: handling the retrying to start service
        } else {
            // Todo: handling for running service.
        }
        handleCommands(intent);
        return Service.START_STICKY;
    }

    public void handleCommands(Intent intent) {
        String fn = "handleCommands(): ";
        if (intent == null) {
            return;
        }

        String action = intent.getAction();
        LogUtils.LOGD(TAG, fn + String.format("%s", action));
        if (action == null) return;
        switch (action) {
            case Action.ACTION_EXIT: {
                LogUtils.LOGD(TAG, fn + String.format(" %s - Stopping service", action));
                StoryService.this.stopSelf();
                break;
            }
            case Action.REQUEST_TO_SERVER_AUTHENTICATE_FACEBOOK: {
                LogUtils.LOGD(TAG, fn + String.format(" %s - Logging to SOML with facebook access-token", action));
                signInWithFacebook(intent);
                break;
            }
            case Action.REQUEST_TO_SERVER_AUTHENTICATE_TWITTER: {
                LogUtils.LOGD(TAG, fn + String.format(" %s - Logging to SOML with twitter access-token", action));
                signInWithTwitter(intent);
                break;
            }
            case Action.REQUEST_TO_SERVER_AUTHENTICATE_EMAIL_ACCOUNT: {
                LogUtils.LOGD(TAG, fn + String.format(" %s - Logging to SOML with email account", action));
                signInWithEmailAccount(intent);
                break;
            }
            case Action.REQUEST_TO_SERVER_AUTHENTICATE_GUEST: {
                signInGuest(intent);
                break;
            }
            case Action.REQUEST_TO_SERVER_ACCOUNT_REGISTER: {
                registerAccount(intent);
                break;
            }
            case Action.REQUEST_TO_SERVER_ACCOUNT_FOLLOWING: {
                checkAccountFollowingUser(intent);
                break;
            }
            case Action.REQUEST_TO_SERVER_ACCOUNT_LIKED: {
                checkAccountLikedStory(intent);
                break;
            }
            case Action.REQUEST_TO_SERVER_ACCOUNT_NEW: {
                fetchAccountInfo(intent);
                break;
            }
            // Request to server for getting the list of story by scope
            case Action.REQUEST_TO_SERVER_STORY_FEATURED: {
                getStories(intent, ServerRequestTask.RequestType.STORIES_FEATURED);
                break;
            }
            case Action.REQUEST_TO_SERVER_STORY_FOLLOWING: {
                getStories(intent, ServerRequestTask.RequestType.STORIES_FOLLOWING);
                break;
            }
            case Action.REQUEST_TO_SERVER_STORY_LATEST: {
                getStories(intent, ServerRequestTask.RequestType.STORIES_LATEST);
                break;
            }
            case Action.REQUEST_TO_SERVER_STORY_COLLABORATING: {
                getStories(intent, ServerRequestTask.RequestType.STORIES_COLLABORATING);
                break;
            }
            case Action.REQUEST_TO_SERVER_MY_STORY_PRIVATE: {
                getMyStories(intent, ServerRequestTask.RequestType.MY_STORIES_PRIVATE);
                break;
            }
            case Action.REQUEST_TO_SERVER_MY_STORY_PUBLISHED: {
                getMyStories(intent, ServerRequestTask.RequestType.MY_STORIES_PUBLISHED);
                break;
            }
            case Action.REQUEST_TO_SERVER_STORY_LIKE: {
                likeStory(intent);
                break;
            }
            case Action.REQUEST_TO_SERVER_STORY_UNLIKE: {
                unlikeStory(intent);
                break;
            }
            case Action.REQUEST_TO_SERVER_STORY_POST: {
                postStory(intent);
                break;
            }
            case Action.REQUEST_TO_SERVER_STORY_DETAIL: {
                getStory(intent);
                break;
            }
            case Action.REQUEST_TO_SERVER_STORY_UPDATE: {
                updateStory(intent);
                break;
            }
            case Action.REQUEST_TO_SERVER_STORY_DELETE: {
                deleteStory(intent);
                break;
            }
            case Action.REQUEST_TO_SERVER_USER_FOLLOWERS: {
                getFollowers(intent);
                break;
            }
            case Action.REQUEST_TO_SERVER_USER_FOLLOW: {
                followUser(intent);
                break;
            }
            case Action.REQUEST_TO_SERVER_USER_UNFOLLOW: {
                unfollowUser(intent);
                break;
            }
            case Action.REQUEST_TO_SERVER_USER_FOLLOWING: {
                getFollowingUsers(intent);
                break;
            }
            case Action.REQUEST_TO_SERVER_USER_REPORT: {
                reportUser(intent);
                break;
            }
            case Action.REQUEST_TO_SERVER_USER_LIST: {
                getUsers(intent);
                break;
            }
            case Action.REQUEST_TO_SERVER_USER_DETAIL: {
                getUser(intent);
                break;
            }
            case Action.REQUEST_TO_SERVER_COMMENT_REPLY: {
                replyComment(intent);
                break;
            }
            case Action.REQUEST_TO_SERVER_COMMENT_LIKE: {
                likeComment(intent);
                break;
            }
            case Action.REQUEST_TO_SERVER_COMMENT_UNLIKE: {
                unlikeComment(intent);
                break;
            }
            case Action.REQUEST_TO_SERVER_COMMENT_STORY: {
                commentStory(intent);
                break;
            }
            case Action.REQUEST_TO_SERVER_COMMENT_UPDATE: {
                updateComment(intent);
                break;
            }
            case Action.REQUEST_TO_SERVER_COMMENT_DELETE: {
                deleteComment(intent);
                break;
            }
            case Action.REQUEST_TO_SERVER_COMMENT_REPORT: {
                reportComment(intent);
                break;
            }
            default:
                break;
        }
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(mReceiver);
        super.onDestroy();
    }


    private final IBinder mIBinder = new LocalBinder();

    public class LocalBinder extends Binder {
        public StoryService getService() {
            return StoryService.this;
        }
    }

    /**
     * Method signInWithEmailAccount used to post email, password to server for current user.
     * The params will be collected before into controllers, activity and then putting them into an instance
     * of {@link android.content.Intent} with specified action, and what
     * <p>
     * An instance of {@link android.os.Message} will wrap constants like Action, ContollerMessage
     * and MainHandler will send this message to targeted object.
     *
     * @param intent The received intent(an instance of {@link android.content.Intent}
     */
    public void signInWithEmailAccount(Intent intent) {
        String fn = "subscribeWithEmailAccount(): ";
        String action = intent.getAction();
        String email = intent.getStringExtra(Action.USER_EMAIL_ADDRESS);
        String password = intent.getStringExtra(Action.USER_PASSWORD);
        // Getting requestOwner, requestMessage from intent and then assigning them again to a message
        final int requestOwner = intent.getIntExtra(Action.REQUEST_OWNER, 0);
        final int requestMsg = intent.getIntExtra(Action.REQUEST_MSG, 0);
        if (requestMsg == 0) {
            final Error error = new Error(ControllerMessage.REQUEST_ERROR_REASON_PARAMS_MISSING);
            sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, requestOwner, requestMsg, error);
            Log.e(TAG, fn + "Params are missing...");
            return;
        }
        // Constructing the task or thread for wrapping the constants and resolving this task into an instance
        // of SerialTaskManager with specified key
        ServerSubscribeTask task = new ServerSubscribeTask(action, mMainHandler);
        task.setSubscribeType(ServerSubscribeTask.SubscribeType.SUBSCRIBE_EMAIL);
        task.setRequestOwner(requestOwner);
        task.setRequestMsg(requestMsg);
        task.setUserEmail(email);
        task.setUserPassword(password);
        SerializedTaskManager.resolve("SERVER_SUBSCRIBER_TASK").add(task, SerializedTaskManager.STM_SERVER_REQUEST_TO_SUBSCRIBE);
    }

    public void signInWithTwitter(Intent intent) {
        String fn = "subscribeWithTwitter(): ";
        String action = intent.getAction();
        String twitterAuthToken = intent.getStringExtra(Action.FABRIC_AUTH_TOKEN);
        String twitterAuthSecret = intent.getStringExtra(Action.FABRIC_AUTH_TOKEN_SECRET);
        long twitterUserId = intent.getLongExtra(Action.FABRIC_ID, 0);
        LogUtils.LOGD(TAG, fn + String.format(" AuthToken: %s, Secret: %s, FabricId: %s", twitterAuthToken, twitterAuthSecret, twitterUserId));
        final int requestOwner = intent.getIntExtra(Action.REQUEST_OWNER, 0);
        final int requestMsg = intent.getIntExtra(Action.REQUEST_MSG, 0);
        LogUtils.LOGD(TAG, fn + String.format("RequestOwner: %d", requestOwner));
        LogUtils.LOGD(TAG, fn + String.format("RequestMsg: %d", requestMsg));
        // Checking if received Intent don't have any requestMsg or missing authentication's token
        if (requestMsg == 0 || TextUtils.isEmpty(twitterAuthToken)) {
            final Error error = new Error(ControllerMessage.REQUEST_ERROR_REASON_PARAMS_MISSING);
            sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, requestOwner, requestMsg, error);
            Log.e(TAG, fn + "Params missing...");
            return;
        }

        ServerSubscribeTask task = new ServerSubscribeTask(action, mMainHandler);
        task.setRequestOwner(requestOwner);
        task.setRequestMsg(requestMsg);

        task.setAuthToken(twitterAuthToken);
        task.setAuthTokenSecret(twitterAuthSecret);
        task.setAuthFabricId(twitterUserId);

        task.setSubscribeType(ServerSubscribeTask.SubscribeType.SUBSCRIBE_TWITTER);
        // Adding current task to HashMap of Thread into SerialTaskManager with name "SERVER_SUBSCRIBER_TASK"
        // and also with key equals to STM_SERVER_REQUEST_LOGIN_TWITTER
        SerializedTaskManager.resolve("SERVER_SUBSCRIBER_TASK").add(task, SerializedTaskManager.STM_SERVER_REQUEST_TO_SUBSCRIBE);
    }

    /**
     * Method signInWithFacebook() will triggered an instance of SubscriberTask to post the Authentication's Token
     * to server for verifying the social network's account.
     *
     * @param intent
     */
    public void signInWithFacebook(Intent intent) {
        String fn = "subscribeWithFacebook(): ";
        String action = intent.getAction();
        // Getting requestOwner and requestMsg from current intent.
        String fbAccessToken = intent.getStringExtra(Action.SOCIAL_NETWORK_ACCESS_TOKEN);
        final int requestOwner = intent.getIntExtra(Action.REQUEST_OWNER, 0);
        final int requestMsg = intent.getIntExtra(Action.REQUEST_MSG, 0);
        LogUtils.LOGD(TAG, fn + String.format("RequestOwner: %d", requestOwner));
        LogUtils.LOGD(TAG, fn + String.format("RequestMsg: %d", requestMsg));
        if (requestMsg == 0 || TextUtils.isEmpty(fbAccessToken)) {
            final Error error = new Error(ControllerMessage.REQUEST_ERROR_REASON_PARAMS_MISSING);
            // MainHandler will sending the message which notify other components that having exist error
            // when executing this request to server for posting the authentication's token
            sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, requestOwner, requestMsg, error);
            Log.e(TAG, fn + "Params are missing.");
            return;
        }

        ServerSubscribeTask task = new ServerSubscribeTask(action, mMainHandler);
        task.setAccessToken(fbAccessToken);
        task.setSubscribeType(ServerSubscribeTask.SubscribeType.SUBSCRIBE_FACEBOOK);
        task.setRequestOwner(requestOwner);
        task.setRequestMsg(requestMsg);
        SerializedTaskManager.resolve("SERVER_SUBSCRIBER_TASK").add(task, SerializedTaskManager.STM_SERVER_REQUEST_TO_SUBSCRIBE);
    }

    public void signInGuest(Intent intent) {
        String fn = "signInGuest(): ";
        String action = intent.getAction();
        // Getting requestOwner and requestMsg from current intent.
        final int requestOwner = intent.getIntExtra(Action.REQUEST_OWNER, 0);
        final int requestMsg = intent.getIntExtra(Action.REQUEST_MSG, 0);
        LogUtils.LOGD(TAG, fn + String.format("RequestOwner: %d", requestOwner));
        LogUtils.LOGD(TAG, fn + String.format("RequestMsg: %d", requestMsg));
        if (requestMsg == 0) {
            final Error error = new Error(ControllerMessage.REQUEST_ERROR_REASON_PARAMS_MISSING);
            // MainHandler will sending the message which notify other components that having exist error
            // when executing this request to server for posting the authentication's token
            sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, requestOwner, requestMsg, error);
            Log.e(TAG, fn + "Params are missing.");
            return;
        }

        ServerSubscribeTask task = new ServerSubscribeTask(action, mMainHandler);
        task.setSubscribeType(ServerSubscribeTask.SubscribeType.GUEST_ACCOUNT);
        task.setRequestOwner(requestOwner);
        task.setRequestMsg(requestMsg);
        SerializedTaskManager.resolve("SERVER_SUBSCRIBER_TASK").add(task, SerializedTaskManager.STM_SERVER_REQUEST_TO_SUBSCRIBE);
    }

    public void registerAccount(Intent intent) {
        String fn = "registerAccount(): ";
        String action = intent.getAction();
        // Getting requestOwner and requestMsg from current intent.
        final int requestOwner = intent.getIntExtra(Action.REQUEST_OWNER, 0);
        final int requestMsg = intent.getIntExtra(Action.REQUEST_MSG, 0);
        final String fullName = intent.getStringExtra(Action.USER_FULL_NAME);
        final String email = intent.getStringExtra(Action.USER_EMAIL_ADDRESS);
        final String password = intent.getStringExtra(Action.USER_PASSWORD);
        final String passwordConfirmation = intent.getStringExtra(Action.USER_PASSWORD_CONFIRMATION);
        final String description = intent.getStringExtra(Action.USER_DESCRIPTION);
        LogUtils.LOGD(TAG, fn + String.format("RequestOwner: %d", requestOwner));
        LogUtils.LOGD(TAG, fn + String.format("RequestMsg: %d", requestMsg));
        if (requestMsg == 0) {
            final Error error = new Error(ControllerMessage.REQUEST_ERROR_REASON_PARAMS_MISSING);
            // MainHandler will sending the message which notify other components that having exist error
            // when executing this request to server for posting the authentication's token
            sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, requestOwner, requestMsg, error);
            Log.e(TAG, fn + "Params are missing.");
            return;
        }

        ServerSubscribeTask task = new ServerSubscribeTask(action, mMainHandler);
        task.setSubscribeType(ServerSubscribeTask.SubscribeType.ACCOUNT_REGISTER);
        task.setRequestOwner(requestOwner);
        task.setRequestMsg(requestMsg);
        task.setUserFullName(fullName);
        task.setUserEmail(email);
        task.setUserPassword(password);
        task.setUserPasswordConfirmation(passwordConfirmation);
        task.setDescription(description);
        SerializedTaskManager.resolve("SERVER_SUBSCRIBER_TASK").add(task, SerializedTaskManager.STM_SERVER_REQUEST_TO_SUBSCRIBE);
    }

    public void checkAccountFollowingUser(Intent intent) {
        String fn = "checkAccountFollowingUser(): ";
        String action = intent.getAction();
        // Getting requestOwner and requestMsg from current intent.
        final int requestOwner = intent.getIntExtra(Action.REQUEST_OWNER, 0);
        final int requestMsg = intent.getIntExtra(Action.REQUEST_MSG, 0);
        final int userId = intent.getIntExtra(Action.USER_ID, -1);
        LogUtils.LOGD(TAG, fn + String.format("RequestOwner: %d", requestOwner));
        LogUtils.LOGD(TAG, fn + String.format("RequestMsg: %d", requestMsg));
        if (requestMsg == 0) {
            final Error error = new Error(ControllerMessage.REQUEST_ERROR_REASON_PARAMS_MISSING);
            // MainHandler will sending the message which notify other components that having exist error
            // when executing this request to server for posting the authentication's token
            sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, requestOwner, requestMsg, error);
            Log.e(TAG, fn + "Params are missing.");
            return;
        }

        ServerRequestTask task = new ServerRequestTask(action, mMainHandler);
        task.setRequestType(ServerRequestTask.RequestType.ACCOUNT_FOLLOWING);
        task.setRequestOwner(requestOwner);
        task.setRequestMessage(requestMsg);
        task.setUserId(userId);
        SerializedRequestTaskManager.resolve("SERVER_REQUEST_TASK").add(task, SerializedRequestTaskManager.STM_SERVER_REQUEST_TO_GET);
    }

    public void checkAccountLikedStory(Intent intent) {
        String fn = "checkAccountLikedStory(): ";
        String action = intent.getAction();
        // Getting requestOwner and requestMsg from current intent.
        final int requestOwner = intent.getIntExtra(Action.REQUEST_OWNER, 0);
        final int requestMsg = intent.getIntExtra(Action.REQUEST_MSG, 0);
        final int storyId = intent.getIntExtra(Action.STORY_ID, -1);
        LogUtils.LOGD(TAG, fn + String.format("RequestOwner: %d", requestOwner));
        LogUtils.LOGD(TAG, fn + String.format("RequestMsg: %d", requestMsg));
        if (requestMsg == 0) {
            final Error error = new Error(ControllerMessage.REQUEST_ERROR_REASON_PARAMS_MISSING);
            // MainHandler will sending the message which notify other components that having exist error
            // when executing this request to server for posting the authentication's token
            sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, requestOwner, requestMsg, error);
            Log.e(TAG, fn + "Params are missing.");
            return;
        }

        ServerRequestTask task = new ServerRequestTask(action, mMainHandler);
        task.setRequestType(ServerRequestTask.RequestType.ACCOUNT_LIKED);
        task.setRequestOwner(requestOwner);
        task.setRequestMessage(requestMsg);
        task.setStoryId(storyId);
        SerializedRequestTaskManager.resolve("SERVER_REQUEST_TASK").add(task, SerializedRequestTaskManager.STM_SERVER_REQUEST_TO_GET);
    }

    public void fetchAccountInfo(Intent intent) {
        String fn = "fetchAccountInfo(): ";
        String action = intent.getAction();
        // Getting requestOwner and requestMsg from current intent.
        final int requestOwner = intent.getIntExtra(Action.REQUEST_OWNER, 0);
        final int requestMsg = intent.getIntExtra(Action.REQUEST_MSG, 0);
        final int userId = intent.getIntExtra(Action.USER_ID, -1);
        LogUtils.LOGD(TAG, fn + String.format("RequestOwner: %d", requestOwner));
        LogUtils.LOGD(TAG, fn + String.format("RequestMsg: %d", requestMsg));
        if (requestMsg == 0) {
            final Error error = new Error(ControllerMessage.REQUEST_ERROR_REASON_PARAMS_MISSING);
            // MainHandler will sending the message which notify other components that having exist error
            // when executing this request to server for posting the authentication's token
            sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, requestOwner, requestMsg, error);
            Log.e(TAG, fn + "Params are missing.");
            return;
        }

        ServerRequestTask task = new ServerRequestTask(action, mMainHandler);
        task.setRequestType(ServerRequestTask.RequestType.ACCOUNT_NEW);
        task.setRequestOwner(requestOwner);
        task.setRequestMessage(requestMsg);
        task.setUserId(userId);
        SerializedRequestTaskManager.resolve("SERVER_REQUEST_TASK").add(task, SerializedRequestTaskManager.STM_SERVER_REQUEST_TO_GET);
    }

    /*************************************************************************************************
     * Story api
     ************************************************************************************************/
    public void getStories(Intent intent, ServerRequestTask.RequestType requestType) {
        String fn = "getStories(): ";
        String action = intent.getAction();
        // Getting requestOwner and requestMsg from current intent.
        final int requestOwner = intent.getIntExtra(Action.REQUEST_OWNER, 0);
        final int requestMsg = intent.getIntExtra(Action.REQUEST_MSG, 0);

        final int apiVersion = intent.getIntExtra(Action.X_API_VERSION, 1);
        final String apiAuthToken = intent.getStringExtra(Action.X_AUTH_TOKEN);

        final String storyScope = intent.getStringExtra(Action.STORY_SCOPE);
        final String keyword = intent.getStringExtra(Action.KEYWORD);
        final int userId = intent.getIntExtra(Action.USER_ID, -1);
        final int startIndex = intent.getIntExtra(Action.START_INDEX, 0);
        final int limit = intent.getIntExtra(Action.LIMIT, 8);
        final String state = intent.getStringExtra(Action.STORY_STATE);
        LogUtils.LOGD(TAG, fn + String.format("RequestOwner: %d", requestOwner));
        LogUtils.LOGD(TAG, fn + String.format("RequestMsg: %d", requestMsg));
        if (requestMsg == 0) {
            final Error error = new Error(ControllerMessage.REQUEST_ERROR_REASON_PARAMS_MISSING);
            sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, requestOwner, requestMsg, error);
            Log.e(TAG, fn + "Params are missing.");
            return;
        }

        ServerRequestTask task = new ServerRequestTask(action, mMainHandler);
        task.setRequestType(requestType);
        task.setRequestOwner(requestOwner);
        task.setRequestMessage(requestMsg);

        task.setApiVersion(apiVersion);
        task.setApiAuthToken(apiAuthToken);
        task.setStoryScope(storyScope);

        task.setQueries(keyword);
        task.setUserId(userId);
        task.setStartIndex(startIndex);
        task.setLimit(limit);
        task.setState(state);
        SerializedRequestTaskManager.resolve("SERVER_REQUEST_TASK").add(task, SerializedRequestTaskManager.STM_SERVER_REQUEST_TO_GET);
    }

    public void getMyStories(Intent intent, ServerRequestTask.RequestType requestType) {
        String fn = "getMyStories(): ";
        String action = intent.getAction();
        // Getting requestOwner and requestMsg from current intent.
        final int requestOwner = intent.getIntExtra(Action.REQUEST_OWNER, 0);
        final int requestMsg = intent.getIntExtra(Action.REQUEST_MSG, 0);
        final int apiVersion = intent.getIntExtra(Action.X_API_VERSION, 1);
        final String apiAuthToken = intent.getStringExtra(Action.X_AUTH_TOKEN);

        final int userId = intent.getIntExtra(Action.USER_ID, 0);
        final int startIndex = intent.getIntExtra(Action.START_INDEX, 0);
        final int limit = intent.getIntExtra(Action.LIMIT, 8);
        final String state = intent.getStringExtra(Action.STORY_STATE);

        LogUtils.LOGD(TAG, fn + String.format("RequestOwner: %d", requestOwner));
        LogUtils.LOGD(TAG, fn + String.format("RequestMsg: %d", requestMsg));
        if (requestMsg == 0) {
            final Error error = new Error(ControllerMessage.REQUEST_ERROR_REASON_PARAMS_MISSING);
            // MainHandler will sending the message which notify other components that having exist error
            // when executing this request to server for posting the authentication's token
            sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, requestOwner, requestMsg, error);
            Log.e(TAG, fn + "Params are missing.");
            return;
        }

        ServerRequestTask task = new ServerRequestTask(action, mMainHandler);
        task.setRequestType(requestType);
        task.setRequestOwner(requestOwner);
        task.setRequestMessage(requestMsg);

        task.setApiAuthToken(apiAuthToken);
        task.setApiVersion(apiVersion);
        task.setUserId(userId);
        task.setStartIndex(startIndex);
        task.setLimit(limit);
        task.setState(state);
        SerializedRequestTaskManager.resolve("SERVER_REQUEST_TASK").add(task, SerializedRequestTaskManager.STM_SERVER_REQUEST_TO_GET);
    }

    public void getStory(Intent intent) {
        String fn = "getStory(): ";
        String action = intent.getAction();
        // Getting requestOwner and requestMsg from current intent.
        final int requestOwner = intent.getIntExtra(Action.REQUEST_OWNER, 0);
        final int requestMsg = intent.getIntExtra(Action.REQUEST_MSG, 0);
        final int storyId = intent.getIntExtra(Action.STORY_ID, -1);
        LogUtils.LOGD(TAG, fn + String.format("RequestOwner: %d", requestOwner));
        LogUtils.LOGD(TAG, fn + String.format("RequestMsg: %d", requestMsg));
        if (requestMsg == 0) {
            final Error error = new Error(ControllerMessage.REQUEST_ERROR_REASON_PARAMS_MISSING);
            // MainHandler will sending the message which notify other components that having exist error
            // when executing this request to server for posting the authentication's token
            sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, requestOwner, requestMsg, error);
            Log.e(TAG, fn + "Params are missing.");
            return;
        }

        ServerRequestTask task = new ServerRequestTask(action, mMainHandler);
        task.setRequestType(ServerRequestTask.RequestType.STORIES_SCOPE);
        task.setRequestOwner(requestOwner);
        task.setRequestMessage(requestMsg);
        task.setStoryId(storyId);
        SerializedRequestTaskManager.resolve("SERVER_REQUEST_TASK").add(task, SerializedRequestTaskManager.STM_SERVER_REQUEST_TO_GET);
    }

    public void likeStory(Intent intent) {
        String fn = "likeStory(): ";
        String action = intent.getAction();
        // Getting requestOwner and requestMsg from current intent.
        final int requestOwner = intent.getIntExtra(Action.REQUEST_OWNER, 0);
        final int requestMsg = intent.getIntExtra(Action.REQUEST_MSG, 0);
        final int storyId = intent.getIntExtra(Action.STORY_ID, -1);
        LogUtils.LOGD(TAG, fn + String.format("RequestOwner: %d", requestOwner));
        LogUtils.LOGD(TAG, fn + String.format("RequestMsg: %d", requestMsg));
        if (requestMsg == 0) {
            final Error error = new Error(ControllerMessage.REQUEST_ERROR_REASON_PARAMS_MISSING);
            // MainHandler will sending the message which notify other components that having exist error
            // when executing this request to server for posting the authentication's token
            sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, requestOwner, requestMsg, error);
            Log.e(TAG, fn + "Params are missing.");
            return;
        }

        ServerSubscribeTask task = new ServerSubscribeTask(action, mMainHandler);
        task.setSubscribeType(ServerSubscribeTask.SubscribeType.STORY_LIKE);
        task.setRequestOwner(requestOwner);
        task.setRequestMsg(requestMsg);
        task.setAuthToken(AccountUtils.getAuthToken(StoryApp.getContext()));
        task.setApiVersion(1);
        task.setStoryId(storyId);
        SerializedTaskManager.resolve("SERVER_SUBSCRIBER_TASK").add(task, SerializedTaskManager.STM_SERVER_REQUEST_TO_SUBSCRIBE);
    }

    public void unlikeStory(Intent intent) {
        String fn = "likeStory(): ";
        String action = intent.getAction();
        // Getting requestOwner and requestMsg from current intent.
        final int requestOwner = intent.getIntExtra(Action.REQUEST_OWNER, 0);
        final int requestMsg = intent.getIntExtra(Action.REQUEST_MSG, 0);
        final int storyId = intent.getIntExtra(Action.STORY_ID, -1);
        LogUtils.LOGD(TAG, fn + String.format("RequestOwner: %d", requestOwner));
        LogUtils.LOGD(TAG, fn + String.format("RequestMsg: %d", requestMsg));
        if (requestMsg == 0) {
            final Error error = new Error(ControllerMessage.REQUEST_ERROR_REASON_PARAMS_MISSING);
            // MainHandler will sending the message which notify other components that having exist error
            // when executing this request to server for posting the authentication's token
            sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, requestOwner, requestMsg, error);
            Log.e(TAG, fn + "Params are missing.");
            return;
        }

        ServerSubscribeTask task = new ServerSubscribeTask(action, mMainHandler);
        task.setSubscribeType(ServerSubscribeTask.SubscribeType.STORY_UNLIKE);
        task.setRequestOwner(requestOwner);
        task.setRequestMsg(requestMsg);
        task.setStoryId(storyId);
        task.setAuthToken(AccountUtils.getAuthToken(StoryApp.getContext()));
        task.setApiVersion(1);

        SerializedTaskManager.resolve("SERVER_SUBSCRIBER_TASK").add(task, SerializedTaskManager.STM_SERVER_REQUEST_TO_SUBSCRIBE);
    }

    public void postStory(Intent intent) {
        String fn = "postStory(): ";
        String action = intent.getAction();
        // Getting requestOwner and requestMsg from current intent.
        final int requestOwner = intent.getIntExtra(Action.REQUEST_OWNER, 0);
        final int requestMsg = intent.getIntExtra(Action.REQUEST_MSG, 0);
        final String storyName = intent.getStringExtra(Action.STORY_NAME);
        final int storyInterval = intent.getIntExtra(Action.STORY_INTERVAL, 0);
        final String storyDesc = intent.getStringExtra(Action.STORY_DESCRIPTION);
        final float storyLat = intent.getFloatExtra(Action.STORY_LATITUDE, 0);
        final float storyLng = intent.getFloatExtra(Action.STORY_LONGITUDE, 0);
        final String storyLoc = intent.getStringExtra(Action.STORY_LOCATION);

        LogUtils.LOGD(TAG, fn + String.format("RequestOwner: %d", requestOwner));
        LogUtils.LOGD(TAG, fn + String.format("RequestMsg: %d", requestMsg));
        if (requestMsg == 0) {
            final Error error = new Error(ControllerMessage.REQUEST_ERROR_REASON_PARAMS_MISSING);
            // MainHandler will sending the message which notify other components that having exist error
            // when executing this request to server for posting the authentication's token
            sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, requestOwner, requestMsg, error);
            Log.e(TAG, fn + "Params are missing.");
            return;
        }

        ServerSubscribeTask task = new ServerSubscribeTask(action, mMainHandler);
        task.setSubscribeType(ServerSubscribeTask.SubscribeType.STORY_POST);
        task.setRequestOwner(requestOwner);
        task.setRequestMsg(requestMsg);
        task.setStoryName(storyName);
        task.setStoryInterval(storyInterval);
        task.setStoryDescription(storyDesc);
        task.setStoryLatitude(storyLat);
        task.setStoryLongtitude(storyLng);
        task.setStoryLocation(storyLoc);
        SerializedTaskManager.resolve("SERVER_SUBSCRIBER_TASK").add(task, SerializedTaskManager.STM_SERVER_REQUEST_TO_SUBSCRIBE);
    }

    public void updateStory(Intent intent) {
        String fn = "updateStory(): ";
        String action = intent.getAction();
        final int requestOwner = intent.getIntExtra(Action.REQUEST_OWNER, 0);
        final int requestMsg = intent.getIntExtra(Action.REQUEST_MSG, 0);
        final int storyId = intent.getIntExtra(Action.STORY_ID, -1);
        final String storyName = intent.getStringExtra(Action.STORY_NAME);
        final int storyInterval = intent.getIntExtra(Action.STORY_INTERVAL, 0);
        final String storyDesc = intent.getStringExtra(Action.STORY_DESCRIPTION);
        final float storyLat = intent.getFloatExtra(Action.STORY_LATITUDE, 0);
        final float storyLng = intent.getFloatExtra(Action.STORY_LONGITUDE, 0);
        final String storyLoc = intent.getStringExtra(Action.STORY_LOCATION);

        LogUtils.LOGD(TAG, fn + String.format("RequestOwner: %d", requestOwner));
        LogUtils.LOGD(TAG, fn + String.format("RequestMsg: %d", requestMsg));
        if (requestMsg == 0) {
            final Error error = new Error(ControllerMessage.REQUEST_ERROR_REASON_PARAMS_MISSING);
            // MainHandler will sending the message which notify other components that having exist error
            // when executing this request to server for posting the authentication's token
            sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, requestOwner, requestMsg, error);
            Log.e(TAG, fn + "Params are missing.");
            return;
        }

        ServerSubscribeTask task = new ServerSubscribeTask(action, mMainHandler);
        task.setSubscribeType(ServerSubscribeTask.SubscribeType.STORY_UPDATE);
        task.setRequestOwner(requestOwner);
        task.setRequestMsg(requestMsg);
        task.setStoryId(storyId);
        task.setStoryName(storyName);
        task.setStoryInterval(storyInterval);
        task.setStoryDescription(storyDesc);
        task.setStoryLatitude(storyLat);
        task.setStoryLongtitude(storyLng);
        task.setStoryLocation(storyLoc);
        SerializedTaskManager.resolve("SERVER_SUBSCRIBER_TASK").add(task, SerializedTaskManager.STM_SERVER_REQUEST_TO_SUBSCRIBE);
    }

    public void deleteStory(Intent intent) {
        String fn = "deleteStory(): ";
        String action = intent.getAction();
        // Getting requestOwner and requestMsg from current intent.
        final int requestOwner = intent.getIntExtra(Action.REQUEST_OWNER, 0);
        final int requestMsg = intent.getIntExtra(Action.REQUEST_MSG, 0);
        final int storyId = intent.getIntExtra(Action.STORY_ID, -1);

        LogUtils.LOGD(TAG, fn + String.format("RequestOwner: %d", requestOwner));
        LogUtils.LOGD(TAG, fn + String.format("RequestMsg: %d", requestMsg));
        if (requestMsg == 0) {
            final Error error = new Error(ControllerMessage.REQUEST_ERROR_REASON_PARAMS_MISSING);
            // MainHandler will sending the message which notify other components that having exist error
            // when executing this request to server for posting the authentication's token
            sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, requestOwner, requestMsg, error);
            Log.e(TAG, fn + "Params are missing.");
            return;
        }

        ServerSubscribeTask task = new ServerSubscribeTask(action, mMainHandler);
        task.setSubscribeType(ServerSubscribeTask.SubscribeType.STORY_DELETE);
        task.setRequestOwner(requestOwner);
        task.setRequestMsg(requestMsg);
        task.setStoryId(storyId);
        SerializedTaskManager.resolve("SERVER_SUBSCRIBER_TASK").add(task, SerializedTaskManager.STM_SERVER_REQUEST_TO_SUBSCRIBE);
    }

    /*************************************************************************************************
     * Comment api
     ************************************************************************************************/
    public void replyComment(Intent intent) {
        String fn = "replyComment(): ";
        String action = intent.getAction();
        // Getting requestOwner and requestMsg from current intent.
        final int requestOwner = intent.getIntExtra(Action.REQUEST_OWNER, 0);
        final int requestMsg = intent.getIntExtra(Action.REQUEST_MSG, 0);
        final int storyId = intent.getIntExtra(Action.STORY_ID, -1);
        final int commentId = intent.getIntExtra(Action.COMMENT_ID, -1);
        final String commentBody = intent.getStringExtra(Action.COMMENT_BODY);

        LogUtils.LOGD(TAG, fn + String.format("RequestOwner: %d", requestOwner));
        LogUtils.LOGD(TAG, fn + String.format("RequestMsg: %d", requestMsg));
        if (requestMsg == 0) {
            final Error error = new Error(ControllerMessage.REQUEST_ERROR_REASON_PARAMS_MISSING);
            sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, requestOwner, requestMsg, error);
            Log.e(TAG, fn + "Params are missing.");
            return;
        }

        ServerSubscribeTask task = new ServerSubscribeTask(action, mMainHandler);
        task.setSubscribeType(ServerSubscribeTask.SubscribeType.COMMENT_REPLY);
        task.setRequestOwner(requestOwner);
        task.setRequestMsg(requestMsg);
        task.setStoryId(storyId);
        task.setCommentId(commentId);
        task.setCommentBody(commentBody);
        SerializedTaskManager.resolve("SERVER_SUBSCRIBER_TASK").add(task, SerializedTaskManager.STM_SERVER_REQUEST_TO_SUBSCRIBE);
    }

    public void likeComment(Intent intent) {
        String fn = "likeComment(): ";
        String action = intent.getAction();
        // Getting requestOwner and requestMsg from current intent.
        final int requestOwner = intent.getIntExtra(Action.REQUEST_OWNER, 0);
        final int requestMsg = intent.getIntExtra(Action.REQUEST_MSG, 0);
        final int storyId = intent.getIntExtra(Action.STORY_ID, -1);
        final int commentId = intent.getIntExtra(Action.COMMENT_ID, -1);

        LogUtils.LOGD(TAG, fn + String.format("RequestOwner: %d", requestOwner));
        LogUtils.LOGD(TAG, fn + String.format("RequestMsg: %d", requestMsg));
        if (requestMsg == 0) {
            final Error error = new Error(ControllerMessage.REQUEST_ERROR_REASON_PARAMS_MISSING);
            // MainHandler will sending the message which notify other components that having exist error
            // when executing this request to server for posting the authentication's token
            sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, requestOwner, requestMsg, error);
            Log.e(TAG, fn + "Params are missing.");
            return;
        }

        ServerSubscribeTask task = new ServerSubscribeTask(action, mMainHandler);
        task.setSubscribeType(ServerSubscribeTask.SubscribeType.COMMENT_LIKE);
        task.setRequestOwner(requestOwner);
        task.setRequestMsg(requestMsg);
        task.setStoryId(storyId);
        task.setCommentId(commentId);
        SerializedTaskManager.resolve("SERVER_SUBSCRIBER_TASK").add(task, SerializedTaskManager.STM_SERVER_REQUEST_TO_SUBSCRIBE);
    }

    public void unlikeComment(Intent intent) {
        String fn = "unlikeComment(): ";
        String action = intent.getAction();
        // Getting requestOwner and requestMsg from current intent.
        final int requestOwner = intent.getIntExtra(Action.REQUEST_OWNER, 0);
        final int requestMsg = intent.getIntExtra(Action.REQUEST_MSG, 0);
        final int storyId = intent.getIntExtra(Action.STORY_ID, -1);
        final int commentId = intent.getIntExtra(Action.COMMENT_ID, -1);

        LogUtils.LOGD(TAG, fn + String.format("RequestOwner: %d", requestOwner));
        LogUtils.LOGD(TAG, fn + String.format("RequestMsg: %d", requestMsg));
        if (requestMsg == 0) {
            final Error error = new Error(ControllerMessage.REQUEST_ERROR_REASON_PARAMS_MISSING);
            // MainHandler will sending the message which notify other components that having exist error
            // when executing this request to server for posting the authentication's token
            sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, requestOwner, requestMsg, error);
            Log.e(TAG, fn + "Params are missing.");
            return;
        }

        ServerSubscribeTask task = new ServerSubscribeTask(action, mMainHandler);
        task.setSubscribeType(ServerSubscribeTask.SubscribeType.COMMENT_UNLIKE);
        task.setRequestOwner(requestOwner);
        task.setRequestMsg(requestMsg);
        task.setStoryId(storyId);
        task.setCommentId(commentId);
        SerializedTaskManager.resolve("SERVER_SUBSCRIBER_TASK").add(task, SerializedTaskManager.STM_SERVER_REQUEST_TO_SUBSCRIBE);
    }

    public void commentStory(Intent intent) {
        String fn = "commentStory(): ";
        String action = intent.getAction();
        // Getting requestOwner and requestMsg from current intent.
        final int requestOwner = intent.getIntExtra(Action.REQUEST_OWNER, 0);
        final int requestMsg = intent.getIntExtra(Action.REQUEST_MSG, 0);
        final int storyId = intent.getIntExtra(Action.STORY_ID, -1);
        final String commentBody = intent.getStringExtra(Action.COMMENT_BODY);

        LogUtils.LOGD(TAG, fn + String.format("RequestOwner: %d", requestOwner));
        LogUtils.LOGD(TAG, fn + String.format("RequestMsg: %d", requestMsg));
        if (requestMsg == 0) {
            final Error error = new Error(ControllerMessage.REQUEST_ERROR_REASON_PARAMS_MISSING);
            // MainHandler will sending the message which notify other components that having exist error
            // when executing this request to server for posting the authentication's token
            sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, requestOwner, requestMsg, error);
            Log.e(TAG, fn + "Params are missing.");
            return;
        }

        ServerSubscribeTask task = new ServerSubscribeTask(action, mMainHandler);
        task.setSubscribeType(ServerSubscribeTask.SubscribeType.COMMENT_STORY);
        task.setRequestOwner(requestOwner);
        task.setRequestMsg(requestMsg);
        task.setStoryId(storyId);
        task.setCommentBody(commentBody);
        SerializedTaskManager.resolve("SERVER_SUBSCRIBER_TASK").add(task, SerializedTaskManager.STM_SERVER_REQUEST_TO_SUBSCRIBE);
    }

    public void updateComment(Intent intent) {
        String fn = "updateComment(): ";
        String action = intent.getAction();
        // Getting requestOwner and requestMsg from current intent.
        final int requestOwner = intent.getIntExtra(Action.REQUEST_OWNER, 0);
        final int requestMsg = intent.getIntExtra(Action.REQUEST_MSG, 0);
        final int storyId = intent.getIntExtra(Action.STORY_ID, -1);
        final int commentId = intent.getIntExtra(Action.COMMENT_ID, -1);
        final String commentBody = intent.getStringExtra(Action.COMMENT_BODY);

        LogUtils.LOGD(TAG, fn + String.format("RequestOwner: %d", requestOwner));
        LogUtils.LOGD(TAG, fn + String.format("RequestMsg: %d", requestMsg));
        if (requestMsg == 0) {
            final Error error = new Error(ControllerMessage.REQUEST_ERROR_REASON_PARAMS_MISSING);
            // MainHandler will sending the message which notify other components that having exist error
            // when executing this request to server for posting the authentication's token
            sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, requestOwner, requestMsg, error);
            Log.e(TAG, fn + "Params are missing.");
            return;
        }

        ServerSubscribeTask task = new ServerSubscribeTask(action, mMainHandler);
        task.setSubscribeType(ServerSubscribeTask.SubscribeType.COMMENT_UPDATE);
        task.setRequestOwner(requestOwner);
        task.setRequestMsg(requestMsg);
        task.setStoryId(storyId);
        task.setCommentId(commentId);
        task.setCommentBody(commentBody);
        SerializedTaskManager.resolve("SERVER_SUBSCRIBER_TASK").add(task, SerializedTaskManager.STM_SERVER_REQUEST_TO_SUBSCRIBE);
    }

    public void deleteComment(Intent intent) {
        String fn = "deleteComment(): ";
        String action = intent.getAction();
        // Getting requestOwner and requestMsg from current intent.
        final int requestOwner = intent.getIntExtra(Action.REQUEST_OWNER, 0);
        final int requestMsg = intent.getIntExtra(Action.REQUEST_MSG, 0);
        final int storyId = intent.getIntExtra(Action.STORY_ID, -1);
        final int commentId = intent.getIntExtra(Action.COMMENT_ID, -1);

        LogUtils.LOGD(TAG, fn + String.format("RequestOwner: %d", requestOwner));
        LogUtils.LOGD(TAG, fn + String.format("RequestMsg: %d", requestMsg));
        if (requestMsg == 0) {
            final Error error = new Error(ControllerMessage.REQUEST_ERROR_REASON_PARAMS_MISSING);
            // MainHandler will sending the message which notify other components that having exist error
            // when executing this request to server for posting the authentication's token
            sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, requestOwner, requestMsg, error);
            Log.e(TAG, fn + "Params are missing.");
            return;
        }

        ServerSubscribeTask task = new ServerSubscribeTask(action, mMainHandler);
        task.setSubscribeType(ServerSubscribeTask.SubscribeType.COMMENT_DELETE);
        task.setRequestOwner(requestOwner);
        task.setRequestMsg(requestMsg);
        task.setStoryId(storyId);
        task.setCommentId(commentId);
        SerializedTaskManager.resolve("SERVER_SUBSCRIBER_TASK").add(task, SerializedTaskManager.STM_SERVER_REQUEST_TO_SUBSCRIBE);
    }

    public void reportComment(Intent intent) {
        String fn = "replyComment(): ";
        String action = intent.getAction();
        // Getting requestOwner and requestMsg from current intent.
        final int requestOwner = intent.getIntExtra(Action.REQUEST_OWNER, 0);
        final int requestMsg = intent.getIntExtra(Action.REQUEST_MSG, 0);
        final int storyId = intent.getIntExtra(Action.STORY_ID, -1);
        final int commentId = intent.getIntExtra(Action.COMMENT_ID, -1);
        final String reason = intent.getStringExtra(Action.REASON);
        final String message = intent.getStringExtra(Action.MESSAGE);

        LogUtils.LOGD(TAG, fn + String.format("RequestOwner: %d", requestOwner));
        LogUtils.LOGD(TAG, fn + String.format("RequestMsg: %d", requestMsg));
        if (requestMsg == 0) {
            final Error error = new Error(ControllerMessage.REQUEST_ERROR_REASON_PARAMS_MISSING);
            // MainHandler will sending the message which notify other components that having exist error
            // when executing this request to server for posting the authentication's token
            sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, requestOwner, requestMsg, error);
            Log.e(TAG, fn + "Params are missing.");
            return;
        }

        ServerSubscribeTask task = new ServerSubscribeTask(action, mMainHandler);
        task.setSubscribeType(ServerSubscribeTask.SubscribeType.COMMENT_REPORT);
        task.setRequestOwner(requestOwner);
        task.setRequestMsg(requestMsg);
        task.setStoryId(storyId);
        task.setCommentId(commentId);
        task.setReason(reason);
        task.setMessage(message);
        SerializedTaskManager.resolve("SERVER_SUBSCRIBER_TASK").add(task, SerializedTaskManager.STM_SERVER_REQUEST_TO_SUBSCRIBE);
    }

    /*************************************************************************************************
     * User api
     ************************************************************************************************/
    public void getFollowers(Intent intent) {
        String fn = "getFollowers(): ";
        String action = intent.getAction();
        // Getting requestOwner and requestMsg from current intent.
        final int requestOwner = intent.getIntExtra(Action.REQUEST_OWNER, 0);
        final int requestMsg = intent.getIntExtra(Action.REQUEST_MSG, 0);
        final int userId = intent.getIntExtra(Action.USER_ID, -1);
        final int apiVersion = intent.getIntExtra(Action.X_API_VERSION, 1);
        final String authToken = intent.getStringExtra(Action.X_AUTH_TOKEN);

        LogUtils.LOGD(TAG, fn + String.format("RequestOwner: %d", requestOwner));
        LogUtils.LOGD(TAG, fn + String.format("RequestMsg: %d", requestMsg));
        if (requestMsg == 0) {
            final Error error = new Error(ControllerMessage.REQUEST_ERROR_REASON_PARAMS_MISSING);
            // MainHandler will sending the message which notify other components that having exist error
            // when executing this request to server for posting the authentication's token
            sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, requestOwner, requestMsg, error);
            Log.e(TAG, fn + "Params are missing.");
            return;
        }

        ServerRequestTask task = new ServerRequestTask(action, mMainHandler);
        task.setRequestType(ServerRequestTask.RequestType.USER_FOLLOWERS);
        task.setRequestOwner(requestOwner);
        task.setRequestMessage(requestMsg);

        task.setApiVersion(apiVersion);
        task.setApiAuthToken(authToken);
        task.setUserId(userId);
        SerializedRequestTaskManager.resolve("SERVER_REQUEST_TASK").add(task, SerializedRequestTaskManager.STM_SERVER_REQUEST_TO_GET);
    }

    public void unfollowUser(Intent intent) {
        String fn = "unfollowUser(): ";
        String action = intent.getAction();
        // Getting requestOwner and requestMsg from current intent.
        final int requestOwner = intent.getIntExtra(Action.REQUEST_OWNER, 0);
        final int requestMsg = intent.getIntExtra(Action.REQUEST_MSG, 0);
        final int userId = intent.getIntExtra(Action.USER_ID, -1);
        final int apiVersion = intent.getIntExtra(Action.X_API_VERSION, 1);
        final String apiAuthToken = intent.getStringExtra(Action.X_AUTH_TOKEN);
        LogUtils.LOGD(TAG, fn + String.format("RequestOwner: %d", requestOwner));
        LogUtils.LOGD(TAG, fn + String.format("RequestMsg: %d", requestMsg));
        if (requestMsg == 0) {
            final Error error = new Error(ControllerMessage.REQUEST_ERROR_REASON_PARAMS_MISSING);
            // MainHandler will sending the message which notify other components that having exist error
            // when executing this request to server for posting the authentication's token
            sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, requestOwner, requestMsg, error);
            Log.e(TAG, fn + "Params are missing.");
            return;
        }

        ServerSubscribeTask task = new ServerSubscribeTask(action, mMainHandler);
        task.setSubscribeType(ServerSubscribeTask.SubscribeType.USER_UNFOLLOW);
        task.setRequestOwner(requestOwner);
        task.setRequestMsg(requestMsg);
        task.setApiVersion(apiVersion);
        task.setAuthToken(apiAuthToken);
        task.setUserId(userId);
        SerializedTaskManager.resolve("SERVER_SUBSCRIBER_TASK").add(task, SerializedTaskManager.STM_SERVER_REQUEST_TO_SUBSCRIBE);
    }

    public void followUser(Intent intent) {
        String fn = "followUser(): ";
        String action = intent.getAction();
        // Getting requestOwner and requestMsg from current intent.
        final int requestOwner = intent.getIntExtra(Action.REQUEST_OWNER, 0);
        final int requestMsg = intent.getIntExtra(Action.REQUEST_MSG, 0);
        final int userId = intent.getIntExtra(Action.USER_ID, -1);
        final int apiVersion = intent.getIntExtra(Action.X_API_VERSION, 1);
        final String apiAuthToken = intent.getStringExtra(Action.X_AUTH_TOKEN);
        LogUtils.LOGD(TAG, fn + String.format("RequestOwner: %d", requestOwner));
        LogUtils.LOGD(TAG, fn + String.format("RequestMsg: %d", requestMsg));
        if (requestMsg == 0) {
            final Error error = new Error(ControllerMessage.REQUEST_ERROR_REASON_PARAMS_MISSING);
            // MainHandler will sending the message which notify other components that having exist error
            // when executing this request to server for posting the authentication's token
            sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, requestOwner, requestMsg, error);
            Log.e(TAG, fn + "Params are missing.");
            return;
        }

        ServerSubscribeTask task = new ServerSubscribeTask(action, mMainHandler);
        task.setSubscribeType(ServerSubscribeTask.SubscribeType.USER_FOLLOW);
        task.setRequestOwner(requestOwner);
        task.setRequestMsg(requestMsg);
        task.setApiVersion(apiVersion);
        task.setAuthToken(apiAuthToken);
        task.setUserId(userId);
        SerializedTaskManager.resolve("SERVER_SUBSCRIBER_TASK").add(task, SerializedTaskManager.STM_SERVER_REQUEST_TO_SUBSCRIBE);
    }

    public void getFollowingUsers(Intent intent) {
        String fn = "getFollowingUsers(): ";
        String action = intent.getAction();
        // Getting requestOwner and requestMsg from current intent.
        final int requestOwner = intent.getIntExtra(Action.REQUEST_OWNER, 0);
        final int requestMsg = intent.getIntExtra(Action.REQUEST_MSG, 0);
        final int userId = intent.getIntExtra(Action.USER_ID, -1);

        final String apiXAuthToken = intent.getStringExtra(Action.X_AUTH_TOKEN);
        final int apiVersion = intent.getIntExtra(Action.X_API_VERSION, 0);

        LogUtils.LOGD(TAG, fn + String.format("RequestOwner: %d", requestOwner));
        LogUtils.LOGD(TAG, fn + String.format("RequestMsg: %d", requestMsg));
        if (requestMsg == 0) {
            final Error error = new Error(ControllerMessage.REQUEST_ERROR_REASON_PARAMS_MISSING);
            // MainHandler will sending the message which notify other components that having exist error
            // when executing this request to server for posting the authentication's token
            sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, requestOwner, requestMsg, error);
            Log.e(TAG, fn + "Params are missing.");
            return;
        }

        ServerRequestTask task = new ServerRequestTask(action, mMainHandler);
        task.setRequestType(ServerRequestTask.RequestType.USER_FOLLOWING);
        task.setRequestOwner(requestOwner);
        task.setRequestMessage(requestMsg);

        task.setApiAuthToken(apiXAuthToken);
        task.setApiVersion(apiVersion);
        task.setUserId(userId);
        SerializedRequestTaskManager.resolve("SERVER_REQUEST_TASK").add(task, SerializedRequestTaskManager.STM_SERVER_REQUEST_TO_GET);
    }

    public void reportUser(Intent intent) {
        String fn = "reportUser(): ";
        String action = intent.getAction();
        // Getting requestOwner and requestMsg from current intent.
        final int requestOwner = intent.getIntExtra(Action.REQUEST_OWNER, 0);
        final int requestMsg = intent.getIntExtra(Action.REQUEST_MSG, 0);
        final String reason = intent.getStringExtra(Action.REASON);
        final String message = intent.getStringExtra(Action.MESSAGE);

        LogUtils.LOGD(TAG, fn + String.format("RequestOwner: %d", requestOwner));
        LogUtils.LOGD(TAG, fn + String.format("RequestMsg: %d", requestMsg));
        if (requestMsg == 0) {
            final Error error = new Error(ControllerMessage.REQUEST_ERROR_REASON_PARAMS_MISSING);
            // MainHandler will sending the message which notify other components that having exist error
            // when executing this request to server for posting the authentication's token
            sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, requestOwner, requestMsg, error);
            Log.e(TAG, fn + "Params are missing.");
            return;
        }

        ServerSubscribeTask task = new ServerSubscribeTask(action, mMainHandler);
        task.setSubscribeType(ServerSubscribeTask.SubscribeType.USER_REPORT);
        task.setRequestOwner(requestOwner);
        task.setRequestMsg(requestMsg);
        task.setReason(reason);
        task.setMessage(message);
        SerializedTaskManager.resolve("SERVER_SUBSCRIBER_TASK").add(task, SerializedTaskManager.STM_SERVER_REQUEST_TO_SUBSCRIBE);
    }

    public void getUsers(Intent intent) {
        String fn = "getUsers(): ";
        String action = intent.getAction();
        // Getting requestOwner and requestMsg from current intent.
        final int requestOwner = intent.getIntExtra(Action.REQUEST_OWNER, 0);
        final int requestMsg = intent.getIntExtra(Action.REQUEST_MSG, 0);
        final String name = intent.getStringExtra(Action.USER_FULL_NAME);
        final boolean followed = intent.getBooleanExtra(Action.USER_FOLLOWED, false);
        final int limit = intent.getIntExtra(Action.LIMIT, 8);
        final int offset = intent.getIntExtra(Action.OFFSET, 0);

        LogUtils.LOGD(TAG, fn + String.format("RequestOwner: %d", requestOwner));
        LogUtils.LOGD(TAG, fn + String.format("RequestMsg: %d", requestMsg));
        if (requestMsg == 0) {
            final Error error = new Error(ControllerMessage.REQUEST_ERROR_REASON_PARAMS_MISSING);
            // MainHandler will sending the message which notify other components that having exist error
            // when executing this request to server for posting the authentication's token
            sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, requestOwner, requestMsg, error);
            Log.e(TAG, fn + "Params are missing.");
            return;
        }

        ServerRequestTask task = new ServerRequestTask(action, mMainHandler);
        task.setRequestType(ServerRequestTask.RequestType.USER_LIST);
        task.setRequestOwner(requestOwner);
        task.setRequestMessage(requestMsg);
        task.setName(name);
        task.setFollowed(followed);
        task.setLimit(limit);
        task.setOffset(offset);
        SerializedRequestTaskManager.resolve("SERVER_REQUEST_TASK").add(task, SerializedRequestTaskManager.STM_SERVER_REQUEST_TO_GET);
    }

    public void getUser(Intent intent) {
        String fn = "getUser(): ";
        String action = intent.getAction();
        // Getting requestOwner and requestMsg from current intent.
        final int requestOwner = intent.getIntExtra(Action.REQUEST_OWNER, 0);
        final int requestMsg = intent.getIntExtra(Action.REQUEST_MSG, 0);
        final int userId = intent.getIntExtra(Action.USER_ID, -1);

        LogUtils.LOGD(TAG, fn + String.format("RequestOwner: %d", requestOwner));
        LogUtils.LOGD(TAG, fn + String.format("RequestMsg: %d", requestMsg));
        if (requestMsg == 0) {
            final Error error = new Error(ControllerMessage.REQUEST_ERROR_REASON_PARAMS_MISSING);
            // MainHandler will sending the message which notify other components that having exist error
            // when executing this request to server for posting the authentication's token
            sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, requestOwner, requestMsg, error);
            Log.e(TAG, fn + "Params are missing.");
            return;
        }

        ServerRequestTask task = new ServerRequestTask(action, mMainHandler);
        task.setRequestType(ServerRequestTask.RequestType.USER_DETAIL);
        task.setRequestOwner(requestOwner);
        task.setRequestMessage(requestMsg);
        task.setApiVersion(1);
        task.setApiAuthToken(AccountUtils.getAuthToken(getApplicationContext()));
        task.setUserId(userId);
        SerializedRequestTaskManager.resolve("SERVER_REQUEST_TASK").add(task, SerializedRequestTaskManager.STM_SERVER_REQUEST_TO_GET);
    }

    /**
     * Method sendMessage() describe how the Task component was triggered which used the Handler to send {@link android.os.Message}
     * to an instance of Application.
     *
     * @param code         The ControllerMessage's constants is similar to the code for controllers used to sending message.
     *                     let's review {@link android.os.Message}'s what variable.
     * @param requestOwner The original controller's hashcode.
     *                     let's review into derived instance of {@link co.soml.android.app.controllers.BaseController}'s hashcode.
     * @param requestMsg   The Action's constant that be reused into controllers to assign it into handler.
     *                     let's review in {@link android.os.Message}'s Action(Same similar meaning).
     * @param obj          The target object which will received this message.
     *                     let's review in {@link android.os.Message}'s Object(Same similar meaning).
     */
    private void sendMessage(int code, int requestOwner, int requestMsg, Object obj) {
        String fn = "sendMessage(): ";
        final Message msg = new Message();
        LogUtils.LOGD(TAG, fn + "code     = " + ControllerMessage.toString(code) + "(" + code + ")");
        LogUtils.LOGD(TAG, fn + "requestOwner = " + requestOwner);
        LogUtils.LOGD(TAG, fn + "requestMsg = " + ControllerMessage.toString(requestMsg) + "(" + requestMsg + ")");
        // Checking if obj will be an instance of Error object. Creating the error and sending back the message
        // contains errorCode for other comps can understand about it.
        if (obj instanceof Error) {
            final Error error = (Error) obj;
            int errorCode = error.getErrorCode();
            LogUtils.LOGD(TAG, fn + "errorCode = " + ControllerMessage.toString(errorCode) + "(" + errorCode + ")");
        }

        msg.what = code;
        msg.arg1 = requestOwner;
        msg.arg2 = requestMsg;
        msg.obj = obj;
        // the MainHandler which has been assigned into the Service's attributes will be reused to sending the constructed message
        mMainHandler.sendMessage(msg);

    }
}
