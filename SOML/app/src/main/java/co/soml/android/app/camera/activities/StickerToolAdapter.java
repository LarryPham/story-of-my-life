/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/10/15 4:21 PM.
 **/

package co.soml.android.app.camera.activities;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.List;

import co.soml.android.app.camera.utils.EffectUtil;
import co.soml.android.app.models.Sticker;
import co.soml.android.app.models.StickerList;
import co.soml.android.app.utils.LogUtils;

public class StickerToolAdapter extends BaseAdapter {
    public static final String TAG = LogUtils.makeLogTag(StickerToolAdapter.class.getSimpleName());
    protected List<Sticker> mStickers;
    protected Context mContext;

    public StickerToolAdapter(Context context, List<Sticker> stickers) {
        this.mStickers = stickers;
        this.mContext = context;
    }
    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }

    class EffectHolder {
        ImageView mLogo;
        ImageView mContainer;
    }
}
