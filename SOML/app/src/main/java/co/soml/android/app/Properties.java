package co.soml.android.app;

import java.io.File;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its
 * documentation are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable
 * form without the prior written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with
 * respect to the contents, and assumes no responsibility for any errors that might appear in the software and
 * documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.06.2015
 * <p>
 * The global shared class for storing the constants, url and something for checking the server side or client-side
 * can used them to access the components.
 * can
 */
public class Properties {
    public static final String BASE_SERVER_URL = "https://staging.soml.co:443/api/";
    public static final String STORIES_FOLLOWING_URL = "stories/following";
    public static final String STORIES_TRENDING_URL = "stories/trending";
    public static final String STORIES_LIST_URL = "stories";
    public static final String PHOTOS_URL = "stories/{story_id}/photos";
    public static final String SONG_LIST_URL = "songs";
    public static final String SONG_URL = "songs";
    public static final String USER_SEARCH = "users/search";
    public static final String USERS_LIST = "users";
    public static final String ACCOUNT_URL = "/account";
	public static final int MIN_SEARCH_KEYWORD_LENGTH = 50;

    public static final String INTERNAL_MEMORY_PATH = "/mnt/sdcard";
    public static final String EXTERNAL_MEMORY_PATH = "/mnt/sdcard/external_sd";

    public static final String DEFAULT_CONTENT_PATH = INTERNAL_MEMORY_PATH + "/timeup";
    public static final String EXTERNAL_CONTENT_PATH = EXTERNAL_MEMORY_PATH + "/timeup";

    public static final String IMAGE_CACHE_FOLDER = "cache";
    public static final String TIMEUP_CACHE_IMAGE_PATH = DEFAULT_CONTENT_PATH + File.separator + IMAGE_CACHE_FOLDER + File.separator;

    public static final String TIMEUP_SMALL_IMAGE_FOLDER = DEFAULT_CONTENT_PATH + File.separator + ".small";
    public static final String TIMEUP_MEDIUM_IMAGE_FOLDER = DEFAULT_CONTENT_PATH + File.separator + ".medium";
    public static final String TIMEUP_LARGE_IMAGE_FOLDER =  DEFAULT_CONTENT_PATH + File.separator + ".large";
    public static final String TIMEUP_ORIGINAL_IMAGE_FOLDER = DEFAULT_CONTENT_PATH  + File.separator + ".origin";
    public static final String TIMEUP_THUMBNAIL_IMAGE_FOLDER =  DEFAULT_CONTENT_PATH  + File.separator + ".thumb";

    public static final String INTERNAL_MEMORY_INUSE = DEFAULT_CONTENT_PATH + File.separator + ".inuse";
    public static final String EXTERNAL_MEMORY_INUSE = EXTERNAL_CONTENT_PATH  + File.separator + ".inuse";

    public static final int MAX_SIZE_INIT_VALUE = -1;
    public static final int PAGE_SIZE_INIT_VALUE = -1;
    public static final int ID_INIT_VALUE = 0;
    public static final int HASH_CODE_FOR_IMAGE = 0;
    public static final int CONNECTION_TIME_OUT = 20*1000;
    public static final int READ_TIMEOUT = 30*1000;
    public static final int PULL_TO_UPDATE_MARGIN = 20;

}
