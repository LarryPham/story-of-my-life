/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/3/15 5:07 PM.
 **/

package co.soml.android.app.camera.utils;

import com.imagezoom.ImageViewTouch;
import com.squareup.picasso.Picasso;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.ViewGroup;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import co.soml.android.app.R;
import co.soml.android.app.drawables.StickerDrawable;
import co.soml.android.app.models.Sticker;
import co.soml.android.app.utils.Lists;
import co.soml.android.app.widgets.HighLightView;
import co.soml.android.app.widgets.ImageViewDrawableOverlay;
import co.soml.android.app.widgets.LabelView;
import co.soml.android.app.widgets.StoryTextView;

public class EffectUtil {
    public static List<Sticker> mStickerList = Lists.newArrayList();
    private static List<HighLightView> mHighLightViews = new CopyOnWriteArrayList<>();

    public static void clear() {
        mHighLightViews.clear();
    }

    public interface StickerCallback {
        void onRemoveSticker(Sticker sticker);
    }

    public static HighLightView addStickerImage(final ImageViewTouch processImage, Context context, final Sticker sticker, final
                                                StickerCallback callback) {
        // Get bitmap from downloaded images
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_search);
        if (bitmap == null) {
            return null;
        }

        StickerDrawable drawable = new StickerDrawable(context.getResources(), bitmap);
        drawable.setAntiAlias(true);
        drawable.setMinSize(30, 30);
        final HighLightView hv = new HighLightView(processImage, R.style.StoryLife_Theme, drawable);
        hv.setPadding(80);
        hv.setOnDeleteClickListener(new HighLightView.OnDeleteClickListener() {
            @Override
            public void onDeleteClick() {
                ((ImageViewDrawableOverlay) processImage).removeHighLightView(hv);
                mHighLightViews.remove(hv);
                ((ImageViewDrawableOverlay) processImage).invalidate();
                callback.onRemoveSticker(sticker);
            }
        });

        Matrix mImageMatrix = processImage.getImageViewMatrix();
        int cropWidth, cropHeight;
        int x, y;

        final int width = processImage.getWidth();
        final int height = processImage.getHeight();

        cropWidth = (int) drawable.getCurrentWidth();
        cropHeight = (int) drawable.getCurrentHeight();

        final int cropSize = Math.max(cropWidth, cropHeight);
        final int screenSize = Math.min(processImage.getWidth(), processImage.getHeight());

        RectF positionRect = null;
        if (cropSize > screenSize) {
            float ratio;
            float widthRatio = (float) processImage.getWidth() / cropWidth;
            float heightRatio = (float) processImage.getHeight() / cropHeight;

            if (widthRatio < heightRatio) {
                ratio = widthRatio;
            } else {
                ratio = heightRatio;
            }

            cropWidth = (int) ((float) cropWidth * (ratio / 2));
            cropHeight = (int) ((float) cropHeight * (ratio / 2));

            int w = processImage.getWidth();
            int h = processImage.getHeight();

            positionRect = new RectF(w/2 - cropWidth/2, h/2 - cropHeight/2, w/2 + cropWidth/2, h/2 + cropHeight/2);
            positionRect.inset((positionRect.width() - cropWidth)/2, (positionRect.height() - cropHeight)/2);
        }

        if (positionRect != null) {
            x = (int) positionRect.left;
            y = (int) positionRect.top;
        } else {
            x = (width - cropWidth) / 2;
            y = (height - cropHeight) / 2;
        }

        Matrix matrix = new Matrix(mImageMatrix);
        matrix.invert(matrix);

        float[] points = new float[] { x, y , x + cropWidth, y + cropHeight };
        MatrixUtils.mapPoints(matrix, points);

        RectF cropRect = new RectF(points[0], points[1], points[2], points[3]);
        Rect imageRect = new Rect(0, 0, width, height);

        hv.setup(context, mImageMatrix, imageRect, cropRect, false);
        ((ImageViewDrawableOverlay) processImage).addHighLightView(hv);
        ((ImageViewDrawableOverlay) processImage).setSelectedHighLightView(hv);

        mHighLightViews.add(hv);
        return hv;
    }

    public static void addLabelEditable(ImageViewDrawableOverlay overlay, ViewGroup container, StoryTextView label, int left, int top) {

    }

    private static void addLabel(ViewGroup container, StoryTextView label, int left, int top) {

    }

    public static void removeLabelEditable(ImageViewDrawableOverlay overlay, ViewGroup container, LabelView labelView) {
        container.removeView(labelView);
        //overlay.removeLabel(labelView);
    }


}
