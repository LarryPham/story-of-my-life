/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/10/15 7:05 PM.
 **/

package co.soml.android.app.models;

import java.util.ArrayList;

import co.soml.android.app.utils.LogUtils;

public class StickerList extends ArrayList<Sticker> {
    public static final String TAG = LogUtils.makeLogTag(StickerList.class.getSimpleName());

    public StickerIdList getStickerIds() {
        StickerIdList ids = new StickerIdList();
        for (Sticker sticker : this) {
            ids.add(sticker.getId());
        }
        return ids;
    }

    public int indexOfStickerId(long stickerId) {
        for (int index = 0; index < this.size(); index++) {
            if (stickerId == this.get(index).getId()) {
                return index;
            }
        }
        return -1;
    }

    public Sticker getStickerAtIndex(int index) {
        if (index >=0 && index < this.size()) {
            return this.get(index);
        }
        return null;
    }

    public boolean isSameList(StickerList stickers) {
        if (stickers == null || stickers.size() != this.size()) {
            return false;
        }

        for (Sticker sticker : stickers) {
            int index = indexOfSticker(sticker);
            if (index == -1 || !sticker.isSameSticker(this.get(index))) {
                return false;
            }
        }
        return true;
    }

    public int indexOfSticker(Sticker sticker) {
        if (sticker == null) {
            return -1;
        }

        for (int index = 0; index < size(); index++) {
            if (this.get(index).getId() == sticker.getId()) {
                return index;
            }
        }
        return -1;
    }
}
