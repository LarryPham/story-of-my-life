package co.soml.android.app.utils;

import android.graphics.Bitmap;

import co.soml.android.app.cache.LruCache;
import co.soml.android.mediapicker.*;
import co.soml.android.mediapicker.MediaUtils;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its
 * documentation are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable
 * form without the prior written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with
 * respect to the contents, and assumes no responsibility for any errors that might appear in the software and
 * documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.06.2015
 */
public class BitmapLruCache extends LruCache<String, Bitmap> implements MediaUtils.ImageCache {

    public BitmapLruCache(int maxSize) {
        super(maxSize);
    }

    @Override
    protected int sizeOf(String key, Bitmap value) {
        // The cache size will be measured in kilobytes rather than
        // number of items.
        int bytes = (value.getRowBytes() * value.getHeight());
        return (bytes / 1024); //value.getByteCount() introduced in HONEYCOMB_MR1 or higher.
    }

    @Override
    public Bitmap getBitmap(String url) {
        return this.get(url);
    }

    @Override
    public void putBitmap(String url, Bitmap bitmap) {
        this.put(url, bitmap);
    }
}
