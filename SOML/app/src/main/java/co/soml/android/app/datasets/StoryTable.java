/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/19/15 4:19 PM.
 **/

package co.soml.android.app.datasets;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import co.soml.android.app.AppConstants;
import co.soml.android.app.models.Story;
import co.soml.android.app.models.StoryFeedList;
import co.soml.android.app.models.StoryTypes;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.utils.SqlUtils;

public class StoryTable {
    public static final String TAG = LogUtils.makeLogTag(StoryTable.class.getSimpleName());

    private static final String COLUMN_NAMES = "story_id," + "user_id";
    private static final String COLUMN_NAME_NO_TEXT = "tbl_stories.story_id,";

    protected static void createTables(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE tbl_stories(");
        db.execSQL("CREATE INDEX idx_stories_timestamp ON tbl_stories(timestamp)");
        db.execSQL("CREATE TABLE tbl_story_tags(");
    }

    protected static void dropTables(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS tbl_stories");
        db.execSQL("DROP TABLE IF EXISTS tbl_story_tags");
    }

    protected static void reset(SQLiteDatabase db) {
        dropTables(db);
        createTables(db);
    }

    protected static int purge(SQLiteDatabase db) {
        int numDeleted = db.delete("tbl_story_tags", "tag_name NOT IN(SELECT DISTINCT tag_name FROM tbl_tags)", null);
        return 0;
    }

    public static Story getStory(int storyId) {
        String sql = "SELECT * FROM tbl_stories WHERE story_id=? LIMIT 1";
        String[] args = new String[] { Integer.toString(storyId) };
        Cursor cursor = StoryDatabase.getReadableDB().rawQuery(sql, args);
        try {
            if (!cursor.moveToFirst()) {
                return null;
            }
            return getStoryFromCursor(cursor);
        } finally {
            SqlUtils.closeCursor(cursor);
        }
    }

    public static Story getStoryFromCursor(Cursor cursor) {
        if (cursor == null) {
            throw new IllegalArgumentException("getStoryFromCursor > null cursor");
        }

        Story story = new Story();
        story.setId(cursor.getInt(cursor.getColumnIndex("story_id")));
        story.setUserId(cursor.getInt(cursor.getColumnIndex("user_id")));
        story.setCoverId(cursor.getLong(cursor.getColumnIndex("cover_id")));
        story.setName(cursor.getString(cursor.getColumnIndex("title")));
        story.setDescription(cursor.getString(cursor.getColumnIndex("description")));
        story.setInterval(cursor.getInt(cursor.getColumnIndex("interval")));
        story.setLikesCount(cursor.getInt(cursor.getColumnIndex("num_likes")));
        story.setCommentsCount(cursor.getInt(cursor.getColumnIndex("num_comments")));
        story.setLikeByCurrentUser(SqlUtils.sqlToBool(cursor.getInt(cursor.getColumnIndex("is_liked"))));
        story.setIsFollowedByCurrentUser(SqlUtils.sqlToBool(cursor.getInt(cursor.getColumnIndex("is_followed"))));
        return story;
    }

    public static StoryFeedList getStoriesWithListType(StoryTypes.StoryPostListType type, int maxPosts) {
        if (type == null) {
            return new StoryFeedList();
        }

        String sql = "SELECT tbl_stories.* FROM tbl_stories";
        sql += "ORDER BY tbl_stories.timestamp DESC";
        if (maxPosts > 0) {
            sql += " LIMIT " + Integer.toString(maxPosts);
        }
        Cursor cursor = StoryDatabase.getReadableDB().rawQuery(sql, null);
        try {
            return getStoryFeedListFromCursor(cursor);
        } finally {
            SqlUtils.closeCursor(cursor);
        }
    }

    public static int getNumStories() {
        return SqlUtils.intForQuery(StoryDatabase.getReadableDB(), "SELECT count(*) FROM tbl_stories", null);
    }

    public static StoryFeedList getStoryFeedListFromCursor(Cursor cursor) {
        StoryFeedList stories = new StoryFeedList();
        try {
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    stories.add(getStoryFromCursor(cursor));
                } while (cursor.moveToNext());
            }
        } catch (IllegalStateException ex) {
            LogUtils.LOGE(TAG, "Throws new exception: " + ex.getMessage());
        }
        return stories;
    }
}
