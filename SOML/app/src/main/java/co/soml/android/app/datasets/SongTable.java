/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/20/15 8:27 AM.
 **/

package co.soml.android.app.datasets;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import java.text.DateFormat;
import java.util.Date;

import co.soml.android.app.models.File;
import co.soml.android.app.models.Song;
import co.soml.android.app.models.SongList;
import co.soml.android.app.utils.DateTimeUtils;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.utils.SqlUtils;

public class SongTable {

    public static final String TAG = LogUtils.makeLogTag(SongTable.class.getSimpleName());

    protected static void createTables(SQLiteDatabase db) {
        LogUtils.LOGD(TAG, "Create the table[Songs]");
        db.execSQL("CREATE TABLE Songs(song_id INTEGER PRIMARY KEY, title TEXT, artist TEXT, length INTEGER, " +
                "file TEXT, created_at DATE, updated_at DATE, cover TEXT, url TEXT);");
    }

    protected static void dropTables(SQLiteDatabase db) {
        LogUtils.LOGD(TAG, "Drop the table[Songs]");
        db.execSQL("DROP TABLE IF EXISTS Songs");
    }

    protected static void addOrUpdateSong(Song song) {
        if (song == null) {
            return;
        }

        SongList songs = new SongList();
        songs.add(song);
        addOrUpdateSongs(songs);
    }

    private static final String COLUMN_NAMES = "song_id, title, artist, length, file, created_at, updated_at, cover";

    protected static void addOrUpdateSongs(SongList songs) {
        if (songs == null || songs.size() == 0) {
            return;
        }

        SQLiteDatabase db = StoryDatabase.getWritableDB();
        db.beginTransaction();
        SQLiteStatement stmt = db.compileStatement("INSERT OF REPLACE INTO Songs( "
                + COLUMN_NAMES + " ) VALUES (?1,?2,?3,?4,?5,?6,?7,?8)");
        try {
            for (Song song : songs) {
                stmt.bindLong(1, song.getSongId());
                stmt.bindString(2, song.getTitle());
                stmt.bindString(3, song.getArtist());
                stmt.bindLong(4, song.getLength());
                stmt.bindString(5, song.getFile().getUri());
                stmt.bindString(6, song.getCreatedAt().toString());
                stmt.bindString(7, song.getUpdatedAt().toString());
                stmt.bindString(8, song.getCover());

                stmt.execute();
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
            SqlUtils.closeStatement(stmt);
        }
    }

    public static Song getSong(long songId) {
        String args[] = {Long.toString(songId)};
        Cursor cursor = StoryDatabase.getReadableDB().rawQuery("SELECT * FROM Songs WHERE song_id=?", args);
        try {
            if (!cursor.moveToFirst()) {
                return null;
            }
            return getSongFromCursor(cursor);
        } finally {
            SqlUtils.closeCursor(cursor);
        }
    }

    private static Song getSongFromCursor(Cursor cursor) {
        Song song = new Song();
        try {
            song.setSongId(cursor.getLong(cursor.getColumnIndex("song_id")));
            song.setTitle(cursor.getString(cursor.getColumnIndex("title")));
            song.setArtist(cursor.getString(cursor.getColumnIndex("artist")));
            song.setLength(cursor.getInt(cursor.getColumnIndex("length")));
            song.setCreatedAt(DateFormat.getInstance().parse(cursor.getString(cursor.getColumnIndex("created_at"))));
            song.setUpdatedAt(DateFormat.getInstance().parse(cursor.getString(cursor.getColumnIndex("updated_at"))));
            song.setCover(cursor.getString(cursor.getColumnIndex("cover")));

            final String songFileURi = cursor.getString(cursor.getColumnIndex("url"));
            final File songFile = new File(songFileURi);
            song.setFile(songFile);
        } catch (Exception ex) {
            LogUtils.LOGD(TAG, "Throwing new exception: " + ex.toString());
            ex.printStackTrace();
        }
        return song;
    }

    public static SongList getAllSongs() {
        Cursor cursor = StoryDatabase.getReadableDB().rawQuery("SELECT * FROM Songs ORDER BY title", null);
        try {
            SongList songs=  new SongList();
            if (cursor.moveToFirst()) {
                do {
                    songs.add(getSongFromCursor(cursor));
                } while (cursor.moveToNext());
            }
            return songs;
        } finally {
            SqlUtils.closeCursor(cursor);
        }
    }

    public static void deleteSong(Song song) {
        if (song == null) {
            return;
        }

        String[] args = { song.getTitle(), song.getArtist(), String.valueOf(song.getLength()), song.getCreatedAt().toString(),
                song.getUpdatedAt().toString(), song.getCover(), song.getFile().getUri()};
        StoryDatabase.getWritableDB().delete("Songs", "Title = ? AND CreatedAt = ? AND UpdatedAt = ?", args);
    }

    public static void setLastSongUpdated(Song song) {
        if (song == null) {
            return;
        }

        String date = DateTimeUtils.convertFromJavaDateToIso8601(new Date());
        String sql = "UPDATE Songs SET UpdatedAt = ?1 WHERE title = ?2";
        SQLiteStatement stmt = StoryDatabase.getWritableDB().compileStatement(sql);

        try {
            stmt.bindString(1, song.getTitle());
            stmt.bindString(2, date);
            stmt.execute();
        } finally {
            SqlUtils.closeStatement(stmt);
        }
    }
}
