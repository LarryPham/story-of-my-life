/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 * @author: Larry Pham (email: larrypham.vn@gmail.com) Date: 6/9/15 2:44 PM
 **/

package co.soml.android.app.utils;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import java.util.Timer;
import java.util.TimerTask;

public class StoryLocationHelper {

    public static final String TAG = LogUtils.makeLogTag(StoryLocationHelper.class.getSimpleName());

    protected Timer mLocationTimer;
    protected LocationManager mLocationManager;
    protected LocationResult mLocationResult;

    protected boolean mGPSEnabled = false;
    protected boolean mNetworkEnabled = false;

    public boolean getLocation(Context context, LocationResult result) {
        mLocationResult = result;
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        }
        try {
            mGPSEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
            LogUtils.LOGD(TAG, String.format("Throwing new location's exception: %s", ex.getCause()));
        }
        try {
            mNetworkEnabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
            LogUtils.LOGD(TAG, String.format("Throwing new location's exception: %s", ex.getCause()));
        }

        if (!mGPSEnabled && !mNetworkEnabled) {
            return false;
        }

        if (mGPSEnabled) {
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, mGPSLocationListener);
        }

        if (mNetworkEnabled) {
            mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, mNetworkLocationListener);
        }

        mLocationTimer = new Timer();
        mLocationTimer.schedule(new GetLastLocation(), 30000);
        return true;
    }

    protected LocationListener mGPSLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            mLocationTimer.cancel();
            mLocationResult.gotLocation(location);
            mLocationManager.removeUpdates(this);
            mLocationManager.removeUpdates(mNetworkLocationListener);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    protected LocationListener mNetworkLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            mLocationTimer.cancel();
            mLocationResult.gotLocation(location);
            mLocationManager.removeUpdates(this);
            mLocationManager.removeUpdates(mGPSLocationListener);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    class GetLastLocation extends TimerTask {

        @Override
        public void run() {
            mLocationManager.removeUpdates(mGPSLocationListener);
            mLocationManager.removeUpdates(mNetworkLocationListener);

            Location netLoc = null, gpsLoc = null;
            if (mGPSEnabled) {
                gpsLoc = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            }

            if (mNetworkEnabled) {
                netLoc = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }

            if (gpsLoc != null && netLoc != null) {
                if (gpsLoc.getTime() > netLoc.getTime()) {
                    mLocationResult.gotLocation(gpsLoc);
                } else {
                    mLocationResult.gotLocation(netLoc);
                }
            }

            if (gpsLoc != null) {
                mLocationResult.gotLocation(gpsLoc);
                return;
            }

            if (netLoc != null) {
                mLocationResult.gotLocation(netLoc);
                return;
            }
        }
    }

    public static abstract class LocationResult {

        public abstract void gotLocation(Location location);
    }

    public void cancelTimer() {
        if (mLocationTimer != null) {

        }
    }
}
