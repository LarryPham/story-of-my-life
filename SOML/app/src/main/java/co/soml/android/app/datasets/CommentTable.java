package co.soml.android.app.datasets;

import android.database.sqlite.SQLiteDatabase;

import co.soml.android.app.utils.LogUtils;

/**
 *  Copyright (C) 2015 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation
 *  are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied,
 *  reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior
 *  written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with respect to the contents, and
 *  assumes no responsibility for any errors that might appear in the software and documents. This publication and the
 *  contents hereof are subject to change without notice.
 *  @author Larry Pham(Email: larrypham.vn@gmail.com)
 *  @since 6/24/15 4:42 PM
 **/

public class CommentTable {

    public static final String TAG = LogUtils.makeLogTag(CommentTable.class.getSimpleName());

    public static final String COMMENTS_TABLE = "Comments";

    public static void createTables(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + COMMENTS_TABLE + " ("
                + " comment_id   INTEGER DEFAULT 0,"
                + " story_id INTEGER DEFAULT 0,"
                + " user_id INTEGER DEFAULT 0,"
                + " body TEXT,"
                + " created_at TEXT,"
                + " updated_at TEXT,"
                + " deleted_at TEXT,"
                + " ancestry TEXT,"
                + " votes_total INTEGER DEFAULT 0"
                + " PRIMARY KEY (comment_id, story_id, user_id)"
                + ");");
    }

    public static void dropTables(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + COMMENTS_TABLE);
    }

    public static void reset(SQLiteDatabase db) {
        LogUtils.LOGD(TAG, "Resetting comment table");
        dropTables(db);
        createTables(db);
    }

    private static SQLiteDatabase getReadableDB() {
        return null;
    }

    private static SQLiteDatabase getWritableDB() {
        return null;
    }

    /**
     * Purge comments attached to stories that no longer exist, and remove older comments
     * TODO: call after hiding or deleting stories
     */
    private static final int MAX_COMMENTS = 1000;
    public static int purge(SQLiteDatabase db) {
        int numDeleted = 0;
        String sql = " story_id NOT IN (SELECT DISTINCT id FROM " ;
        return 0;
    }
}
