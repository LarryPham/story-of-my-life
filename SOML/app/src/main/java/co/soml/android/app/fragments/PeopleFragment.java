/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 * @author: Larry Pham (email: larrypham.vn@gmail.com)
 * Date: 6/12/15 6:39 PM
 **/

package co.soml.android.app.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import co.soml.android.app.AppConstants;
import co.soml.android.app.R;
import co.soml.android.app.StoryApp;
import co.soml.android.app.activities.BaseActivity;
import co.soml.android.app.activities.InvalidateParam;
import co.soml.android.app.activities.StoryMainActivity;
import co.soml.android.app.controllers.ControllerMessage;
import co.soml.android.app.controllers.PeopleController;
import co.soml.android.app.models.StoryBean;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.models.StoryUserList;
import co.soml.android.app.services.tasks.Action;
import co.soml.android.app.utils.CommonUtil;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.utils.SwipeToRefreshHelper;
import co.soml.android.app.widgets.StoryRecyclerView;

import static co.soml.android.app.models.StoryDataModel.UsersType;

public class PeopleFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    public static final String TAG = LogUtils.makeLogTag(PeopleFragment.class.getSimpleName());
    private static final String USER_SOCIAL_TYPE = "UsersType";

    private BaseActivity mActivity;
    private StoryApp mStoryApp;
    private StoryDataModel mDataModel;
    private PeopleController mController;

    private Bundle mFragmentParams;

    // Params fields
    private int mUserId;
    private int mApiVersion;
    private String mAuthToken;
    private UsersType mUsersType;

    private ViewGroup mRootView;
    private View mEmptyView;

    private StoryRecyclerView mRecyclerView;
    private SwipeToRefreshHelper mSwipeToRefreshHelper;
    private SwipeRefreshLayout mSwipeRefreshLayout;

	private StoryUserList mUsers;
    private boolean mNeedRequery = false;
    private Context mContext;

	private int mDensity;
	private float mDipScale;

	private SharedPreferences mViewByPref = null;
	private SharedPreferences.Editor mViewByEdit = null;

	private int mLastTabPosition = 0;
	private InvalidateParam mInvalidateParam;

	private static PeopleFragment sInstance;
	static Intent mPeopleIntent;
	public StoryBean mBean;

	public static PeopleFragment newInstance(Intent intent) {
		LogUtils.LOGD(TAG, "newInstance()");
		sInstance = new PeopleFragment();
		mPeopleIntent = intent;
		return sInstance;
	}

	public static PeopleFragment getInstance() {
		return sInstance;
	}

	public InvalidateParam getInvalidateParam() {
		return mInvalidateParam;
	}

	public void setInvalidateParam(InvalidateParam invalidateParam) {
		mInvalidateParam = invalidateParam;
	}

	@Override
	public void onAttach(Activity activity) {
		LogUtils.LOGD(TAG, "onAttach()");
		super.onAttach(activity);
		mActivity = (StoryMainActivity) getActivity();
		mStoryApp = (StoryApp) getActivity().getApplicationContext();
		mDipScale = getResources().getDisplayMetrics().density;
		mDataModel = mStoryApp.getAppDataModel();
		mViewByPref = getActivity().getSharedPreferences("GeneralSettings", Context.MODE_PRIVATE);
		mViewByEdit = mViewByPref.edit();
		mDensity = CommonUtil.getDensity(mStoryApp);
		mFragmentParams = getArguments();
	}

	@Override
    public void onCreate(Bundle savedInstanceState) {
		LogUtils.LOGD(TAG, "OnCreate()");
        super.onCreate(savedInstanceState);
		mActivity.registerReceiver(actionReceiver, new IntentFilter(Action.FINISH_APPLICATION));
    }

	private final BroadcastReceiver actionReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			LogUtils.LOGD(TAG, String.format("OnReceive: Action[%s]", action));

			if (action.equals(Action.FINISH_APPLICATION)) {
				mActivity.finish();
			} else if (action.equalsIgnoreCase(Action.SHOW_PROGRESS)) {
				//showProgressDialog();
			} else if (action.equalsIgnoreCase(Action.DISMISS)) {
				//dismiss();
			}
		}
	};

    public boolean isExistedUsers() {
        getUsersDataModel();
        if (mUsers != null && mUsers.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	    LogUtils.LOGD(TAG, "onCreateView():...");
	    // Initializing the controllers and getting the passed arguments
	    if (mController == null) {
		    mController = new PeopleController(mActivity, this, mDataModel);

		    if (mFragmentParams != null) {
			    mUserId = mFragmentParams.getInt(AppConstants.USER_ID);
			    mApiVersion = mFragmentParams.getInt(AppConstants.X_API_VERSION, 1);
			    mAuthToken = mFragmentParams.getString(AppConstants.X_AUTH_TOKEN, null);
			    mUsersType = (UsersType) mFragmentParams.getSerializable(USER_SOCIAL_TYPE);
			    if (mUsersType == null) {
				    mUsersType = UsersType.FOLLOWING;
			    }
			    if (!isExistedUsers()) {
				    if (mUsersType != null && mUsersType == UsersType.FOLLOWING) {
					    final PeopleController.RequestParam requestParam = new PeopleController.RequestParam(mUserId, mAuthToken, mApiVersion);
					    mController.sendMessage(ControllerMessage.REQUEST_TO_SERVER_USER_FOLLOWING, requestParam);
				    } else if (mUsersType != null && mUsersType == UsersType.FOLLOWERS) {
					    final PeopleController.RequestParam requestParam = new PeopleController.RequestParam(mUserId, mAuthToken, mApiVersion);
					    mController.sendMessage(ControllerMessage.REQUEST_TO_SERVER_USER_FOLLOWERS, requestParam);
				    }
			    }
		    }
	    }

	    mBean = StoryMainActivity.getBean();
	    mLastTabPosition = mBean.getLastTabPosition();

	    // Inflating the view and also handling the events for view.
        mRootView = (ViewGroup) inflater.inflate(R.layout.layout_people_fragment, container, false);
        mSwipeRefreshLayout = (SwipeRefreshLayout) mRootView.findViewById(R.id.swipe_refresh_layout);
        mRecyclerView = (StoryRecyclerView) mRootView.findViewById(R.id.recycler_view);
        mEmptyView = (View) mRootView.findViewById(R.id.empty_view);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mRecyclerView.addItemDecoration(new StoryRecyclerView.StoryItemDecoration(CommonUtil.dpToPx(-5), CommonUtil.dpToPx(10)));

        return mRootView;
    }

    public void getUsersDataModel() {
        switch (mUsersType) {
            case FOLLOWERS: {
                mUsers = mDataModel.getFollowers();
                LogUtils.LOGD(TAG, String.format("List of Users into PeopleFragment: %d", mUsers.size()));
                break;
            }
            case FOLLOWING: {
                mUsers = mDataModel.getFollowingUsers();
                LogUtils.LOGD(TAG, String.format("List of FollowingUsers Into PeopleFragment's: %d", mUsers.size()));
                break;
            }
            default:
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
	    LogUtils.LOGD(TAG, "onResume()");
	    if (mController == null) {
		    mController = new PeopleController(mActivity, this, mDataModel);
		    LogUtils.LOGD(TAG, "Controller = " + mController);
	    }

	    if (mBean != null) {
		    mBean.setCurrentFragment(sInstance);
		    LogUtils.LOGI(TAG, "Bean.getCurrentFragment() = " + mBean.getCurrentFragment());
	    }
	    LogUtils.LOGD(TAG, String.format("Registering BroadcastReceiver[%s]", actionReceiver));
	    mActivity.registerReceiver(actionReceiver, new IntentFilter(Action.FINISH_APPLICATION));
	    mActivity.registerReceiver(actionReceiver, new IntentFilter(Action.SHOW_PROGRESS));
	    mActivity.registerReceiver(actionReceiver, new IntentFilter(Action.DISMISS));

	    invalidate();
    }

	@Override
	public void onPause() {
		LogUtils.LOGD(TAG, "onPause()");
		super.onPause();
		LogUtils.LOGD(TAG, String.format("Unregister the broadcast receiver[%s]", actionReceiver.toString()));
		mActivity.unregisterReceiver(actionReceiver);
	}

	@Override
    public void onDestroy() {
        LogUtils.LOGD(TAG, String.format("onDestroy(): UserList's Size: %d", mUsers.size()));
        if (mRecyclerView != null) {
            mRecyclerView.clearAnimation();
            mRecyclerView.removeAllViewsInLayout();
            mRecyclerView.setAdapter(null);
        }
        // Recycling all items
        super.onDestroy();
    }

    @Override
    public void onRefresh() {

    }

    @Override
    public void invalidate() {

    }

    @Override
    public void invalidate(Object... params) {

    }

	public void showProgressDialog() {

	}

	public void dismiss() {

	}
}
