/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 7/26/15 3:02 PM.
 **/

package co.soml.android.app.fragments;

import co.soml.android.app.utils.LogUtils;

public class SearchAuthorsFragment extends BaseFragment {

    public static final String TAG = LogUtils.makeLogTag(SearchAuthorsFragment.class.getSimpleName());

    @Override
    public void invalidate() {

    }

    @Override
    public void invalidate(Object... params) {

    }
}
