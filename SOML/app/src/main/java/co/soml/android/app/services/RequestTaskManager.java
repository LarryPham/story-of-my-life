package co.soml.android.app.services;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import java.util.WeakHashMap;

import co.soml.android.app.services.tasks.ServerRequestTask;
import co.soml.android.app.services.tasks.WeakHandler;
import co.soml.android.app.utils.LogUtils;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.21.2015
 */
public class RequestTaskManager {
    static final String TAG = LogUtils.makeLogTag(RequestTaskManager.class);
    static int INNER_MESSAGE_JOB_DO = 1;
    private static final int STM_SERVER_REQUEST_TO_GET = 200;
    private static final int STM_SERVER_REQUEST_TO_GET_COLLECTIONS = 201;
    private static final int STM_SERVER_REQUEST_TO_GET_STORY = 202;
    private static final int STM_SERVER_REQUEST_TO_GET_PHOTOS = 203;

    GetJobThread mGetJobThread = null;
    Thread mCurrentGetJob = null;
    Object mCurrentGetJobObject = null;
    int mRequestMessage = 0;
    static WeakHashMap<String, RequestTaskManager> sRequestTaskManagerWeakHashMap = new WeakHashMap<String, RequestTaskManager>();

    public static RequestTaskManager resolve(String key) {
        RequestTaskManager manager = sRequestTaskManagerWeakHashMap.get(key);
        if (manager == null) {
            manager = new RequestTaskManager();
            sRequestTaskManagerWeakHashMap.put(key, manager);
        }
        return manager;
    }

    private RequestTaskManager() {
        mGetJobThread = new GetJobThread();
        int priority = mGetJobThread.getPriority();
        if (priority - 1 >= Thread.MIN_PRIORITY) {
            mGetJobThread.setPriority(priority - 1);
        }
        mGetJobThread.start();
        while (mGetJobThread.getGetJobHandler() == null) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException ex) {
                LogUtils.LOGE(TAG, String.format("RequestTaskManager throws Exception: %s", ex.toString()));
                ex.printStackTrace();
            }
        }
    }

    /**
     * Adding new instance of {@code Thread} to the HashMap of {@link java.lang.Thread} for processing the workflows for current process.
     *
     * @param newTask The specified Thread
     */
    public void add(Thread newTask) {
        final Message msg = Message.obtain();
        this.mRequestMessage = INNER_MESSAGE_JOB_DO;
        msg.what = INNER_MESSAGE_JOB_DO;
        msg.obj = newTask;
        mGetJobThread.getGetJobHandler().sendMessage(msg);
        LogUtils.LOGD(TAG, String.format("New Job added. %s", newTask.getClass().getSimpleName()));
    }

    public void add(Object newTask, int requestMessage) {
        this.mRequestMessage = requestMessage;
        final Message message = Message.obtain();
        message.what = requestMessage;
        message.obj = newTask;
        mGetJobThread.setRequestedMessage(mRequestMessage);
        mGetJobThread.getGetJobHandler().sendMessage(message);
        LogUtils.LOGD(TAG, String.format("New Job Added. %s", newTask.getClass().getSimpleName()));
    }

    public void cancel() {
        String fn = "cancel(): ";
        if (mGetJobThread == null) {
            LogUtils.LOGE(TAG, fn + String.format("Error: JobThread is null."));
            return;
        }
        mGetJobThread.getGetJobHandler().removeMessages(this.mRequestMessage);
        LogUtils.LOGD(TAG, fn + String.format("Cancel JobThread %s", mGetJobThread.getClass().getSimpleName()));
        mGetJobThread.interrupt();

        if (mCurrentGetJobObject != null) {
            /**
             * Cancelling the current instance of {@link co.soml.android.app.services.tasks.ServerRequestTask} by setting the cancelled
             * variables will be equaled to true. Maybe some of instances, we should setle more conditions for this task
             */
            switch (mRequestMessage) {
                case STM_SERVER_REQUEST_TO_GET: {
                    LogUtils.LOGD(TAG, String.format("Cancelling JobThread %s", mGetJobThread.getClass().getSimpleName()));
                    ((ServerRequestTask) mCurrentGetJobObject).setCancelled(true);
                    break;
                }
                case STM_SERVER_REQUEST_TO_GET_COLLECTIONS: {
                    LogUtils.LOGD(TAG, String.format("Cancelling JobThread %s", mGetJobThread.getClass().getSimpleName()));
                    ((ServerRequestTask) mCurrentGetJobObject).setCancelled(true);
                    break;
                }
                case STM_SERVER_REQUEST_TO_GET_PHOTOS: {
                    LogUtils.LOGD(TAG, String.format("Cancelling JobThread %s", mGetJobThread.getClass().getSimpleName()));
                    ((ServerRequestTask) mCurrentGetJobObject).setCancelled(true);
                    break;
                }
                case STM_SERVER_REQUEST_TO_GET_STORY: {
                    LogUtils.LOGD(TAG, String.format("Cancelling JobThread %s", mGetJobThread.getClass().getSimpleName()));
                    ((ServerRequestTask) mCurrentGetJobObject).setCancelled(true);
                    break;
                }
            }
        } else if (mCurrentGetJob != null) {
            LogUtils.LOGD(TAG, fn + String.format("Interrupted currentJob! %s", mCurrentGetJob.getClass().getSimpleName()));
            mCurrentGetJob.interrupt();
        }
    }


    class GetJobThread extends Thread {
        public final String TAG = LogUtils.makeLogTag(GetJobThread.class);
        WeakHandler mGetJobHandler = null;
        // Message for request to get the data from server using the WeakHandler instance.
        int mRequestedMessage = 0;
        /**
         * Declaring the {@code Handler.Callback} interface for an instance of {@link co.soml.android.app.services.tasks.WeakHandler
         * .ExecHandler} implements this callback and navigating the flows for sending the requested signals to server for getting the data
         * from server which will be respond to the Get-Method into API package.
         * <p>
         * For avoiding the memory-leak, we should used the {@link co.soml.android.app.services.tasks.WeakHandler} for just keeping the weak
         * references of {@link android.os.Handler} object to implement the functions into the Handler.
         */
        Handler.Callback mGetJobHandlerCallback = new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                // Checking the what-content for received message for switching the flows of processors.
                switch (msg.what) {
                    case STM_SERVER_REQUEST_TO_GET: {
                        break;
                    }
                    case STM_SERVER_REQUEST_TO_GET_COLLECTIONS: {
                        break;
                    }
                    case STM_SERVER_REQUEST_TO_GET_STORY: {
                        break;
                    }
                    case STM_SERVER_REQUEST_TO_GET_PHOTOS: {
                        break;
                    }
                    default: {
                        break;
                    }
                }
                return true;
            }
        };

        public WeakHandler getGetJobHandler() {
            return mGetJobHandler;
        }

        public GetJobThread() {

        }

        public int getRequestedMessage() {
            return mRequestedMessage;
        }

        public void setRequestedMessage(int requestedMessage) {
            mRequestedMessage = requestedMessage;
        }

        @Override
        public synchronized void run() {
            Looper.prepare();
            LogUtils.LOGD(TAG, String.format("Thread %s start. %d", this.getName(), mRequestedMessage));
            /**
             * Instantiating the JobHandler with specified Handler.Callback interface for it weak-reference for implementing methods of this
             * callback. For checking the flows for executing the methods into the Callback see {@link co.soml.android.app.services.tasks
             * .WeakHandler.ExecHandler#handleMessage}
             */
            mGetJobHandler = new WeakHandler(mGetJobHandlerCallback);
            Looper.loop();
            LogUtils.LOGD(TAG, String.format("Thread %s end. %d", this.getName(), mRequestedMessage));
        }
    }
}
