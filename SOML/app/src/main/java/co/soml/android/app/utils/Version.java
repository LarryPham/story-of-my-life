/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 * @author: Larry Pham (email: larrypham.vn@gmail.com)
 * Date: 6/9/15 2:59 PM
 **/

package co.soml.android.app.utils;

import android.text.TextUtils;

public class Version {
    private String mVersion;

    public final String get() {
        return this.mVersion;
    }

    public Version(String version) {
        if (TextUtils.isEmpty(version)) {
            throw new IllegalArgumentException("Version can not be null or empty");
        }

        if (!version.matches("[0-9]]+(\\.[0-9]+)*")) {
            throw new IllegalArgumentException("Invalid Version Format");
        }

        this.mVersion = version;
    }

    public int compareTo(Version that) {
        if (that == null) {
            return 1;
        }
        String[] thisParts = this.get().split("\\.");
        String[] thatParts = that.get().split("\\.");

        int length = Math.max(thisParts.length, thatParts.length);
        for (int index = 0; index < length; index++) {
            int thisPart = index < thisParts.length ? Integer.parseInt(thisParts[index]) : 0;
            int thatPart = index < thatParts.length ? Integer.parseInt(thatParts[index]) : 0;
            if (thisPart < thatPart) {
                return -1;
            }
            if (thisPart > thatPart) {
                return 1;
            }
        }
        return 0;
    }

    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }

        if (that == null) {
            return false;
        }

        if (this.getClass() != that.getClass()) {
            return false;
        }

        return this.compareTo((Version) that) == 0;
    }
}
