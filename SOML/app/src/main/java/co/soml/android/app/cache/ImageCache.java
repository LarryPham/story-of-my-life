/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 * @author: Larry Pham (email: larrypham.vn@gmail.com)
 * Date: 6/11/15 1:11 PM
 **/

package co.soml.android.app.cache;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;

import co.soml.android.app.fragments.BaseFragment;
import co.soml.android.app.utils.CommonUtil;
import co.soml.android.app.utils.LogUtils;

public final class ImageCache {

    private static final String TAG = LogUtils.makeLogTag(ImageCache.class.getSimpleName());

    private static final Uri mImageUri;

    /**
     * Default memory cache size as a percent of device memory class.
     */
    private static final float NEW_CACHE_DIVIDER = 0.25f;

    /**
     * Default disk cache size 20MB
     */
    private static final int DISK_CACHE_SIZE = 1024 * 1024 * 20;
    /**
     * Compression Settings when writing images to disk cache
     */
    private static final Bitmap.CompressFormat COMPRESS_FORMAT = Bitmap.CompressFormat.JPEG;

    /**
     * Disk cache index to read from
     */
    private static final int DISK_CACHE_INDEX = 0;
    /**
     * Image compression quality
     */
    private static final int COMPRESS_QUALITY = 98;

    /**
     * LRU cache
     */
    private MemoryCache mDiskCache;

 /* *//**
     * Disk LRU Cache
     *//*
  private DiskCache mDiskCache;*/

    /**
     * Listeners to the cache state
     */
    private HashSet<ICacheListener> mListeners = new HashSet<ICacheListener>();

    private static ImageCache sInstance;
    public boolean mPauseDiskAccess = false;
    private Object mPauseLock = new Object();

    static {
        mImageUri = Uri.EMPTY;
    }

    public ImageCache(Context context) {
        init(context);
    }

    /**
     * Used to create a singleton of {@link ImageCache}
     *
     * @param context The {@link Context} to use
     * @return A new instance of this class.
     */
    public static ImageCache getInstance(final Context context) {
        if (sInstance == null) {
            sInstance = new ImageCache(context.getApplicationContext());
        }
        return sInstance;
    }

    private void init(final Context context) {
        // Executing the AsyncTask for init DiskCache
        CommonUtil.execute(false, new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                // Initialize the disk cache in a background thread
                initDiskCache(context);
                return null;
            }
        }, (Void[]) null);
        // setup memory cache
    }

    public synchronized void initDiskCache(final Context context) {
    }

    public void removeCacheListener(ICacheListener listener) {
    }

    /**
     * Gets a usable cache directory (external if available, internal otherwise)
     *
     * @param context    The {@link Context} to use
     * @param uniqueName A unique directory name to append to the cache
     * @return The cache directory
     */
    public static File getDiskCacheDir(final Context context, final String uniqueName) {
        final String cachePath = getExternalCacheDir(context) != null ?
                getExternalCacheDir(context).getPath() : context.getCacheDir().getPath();
        return new File(cachePath);
    }

    /**
     * Check if external storage is built-in or removable
     *
     * @return True if external storage is removable(likes an SD-Card), false otherwise
     */
    public static boolean isExternalStorageRemovable() {
        return Environment.isExternalStorageRemovable();
    }

    /**
     * Gets the external app cache directory
     *
     * @param context The {@link Context} to use
     * @return The external cache directory
     */
    public static File getExternalCacheDir(final Context context) {
        return context.getExternalCacheDir();
    }

    /**
     * Check how much usable space is available at a given path.
     *
     * @param path The path to check
     * @return The space available in bytes.
     */
    public static long getUsableSpace(final File path) {
        return path.getUsableSpace();
    }

    /**
     * A hashing method that changes a string (like a URL) into a hash suitable
     * for using as a disk filename.
     *
     * @param key They key used to store the file
     */
    public static String hashKeyForDisk(final String key) {
        String cacheKey;
        try {
            // choose MD5 for generating the hash-key, maybe we can choose SHA1'
            final MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(key.getBytes());
            cacheKey = byteToHexString(digest.digest());
        } catch (final NoSuchAlgorithmException ex) {
            cacheKey = String.valueOf(key.hashCode());
        }
        return cacheKey;
    }

    /**
     * Converts the array of byte to the hex-string format
     *
     * @param bytes the array of byte
     * @return Hex-String format.
     */
    private static final String byteToHexString(final byte[] bytes) {
        final StringBuilder builder = new StringBuilder();
        for (final byte elem : bytes) {
            final String hex = Integer.toHexString(0xFF & elem);
            if (hex.length() == 1) {
                builder.append('0');// space characters
            }
            builder.append(hex);
        }
        return builder.toString();
    }

    /**
     * A simple non-UI Fragment that stores a single Object an is retained over configuration changes.
     * In this sample it will be used to retain an {@link ImageCache} object.
     */
    public static final class RetainBaseFragment extends BaseFragment {

        /**
         * The object to be stored
         */
        private Object mObject;

        /**
         * Empty constructor as per the {@link BaseFragment} documentation
         */
        public RetainBaseFragment() {

        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setRetainInstance(true);
        }

        /**
         * Store a single object in this {@link BaseFragment}
         *
         * @param object The object to store
         */
        public void setObject(final Object object) {
            mObject = object;
        }

        /**
         * Gets the stored object
         *
         * @return Object, stored object.
         */
        public Object getObject() {
            return mObject;
        }

        @Override
        public void invalidate() {

        }

        @Override
        public void invalidate(Object... params) {

        }
    }

    public static final class MemoryCache extends LruCache<String, Bitmap> {

        /**
         * @param maxSize for caches that do not override {@link #sizeOf}, this is the maximum number of entries int the cache. For all other caches, this is the
         *                maximum sum of the sizes of the entries in this cache.
         */
        public MemoryCache(int maxSize) {
            super(maxSize);
        }

        /**
         * Gets the size in bytes of a bitmap
         */
        public static int getBitmapSize(final Bitmap bitmap) {
            return bitmap.getByteCount();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        protected int sizeOf(String stringParam, Bitmap bitmapParam) {
            return getBitmapSize(bitmapParam);
        }
    }
}
