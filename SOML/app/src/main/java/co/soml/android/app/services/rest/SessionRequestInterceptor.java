package co.soml.android.app.services.rest;

import retrofit.RequestInterceptor;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.08.2015
 */
public class SessionRequestInterceptor implements RequestInterceptor {

    @Override
    public void intercept(RequestFacade requestFacade) {
        requestFacade.addHeader("Header name", "Header value");
    }
}
