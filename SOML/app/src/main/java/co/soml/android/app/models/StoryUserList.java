/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 * @author: Larry Pham (email: larrypham.vn@gmail.com)
 * Date: 6/11/15 12:09 AM
 **/

package co.soml.android.app.models;

import java.util.ArrayList;

import co.soml.android.app.utils.LogUtils;

public class StoryUserList extends ArrayList<User> {
    public static final String TAG = LogUtils.makeLogTag(StoryUserList.class.getSimpleName());

    public StoryUserIdList getUserIds() {
        StoryUserIdList ids = new StoryUserIdList();
        for (User user : this) {
            ids.add(user.getId());
        }
        return ids;
    }

    public int indexOfUserId(int userId) {
        for (int index = 0; index < this.size(); index++) {
            if (userId == this.get(index).getId()) {
                return index;
            }
        }
        return -1;
    }

    public User findUserById(final int userId) {
        for (int index = 0; index < size(); index++) {
            if (this.get(index).getId() == userId) {
                return this.get(index);
            }
        }
        return null;
    }

    public boolean isSameList(StoryUserList userList) {
        if (userList == null || userList.size() != this.size()) {
            return false;
        }

        for (User user : userList) {
            int index = indexOfUser(user);
            if (index == -1 || !user.isSameUser(this.get(index))) {
                return false;
            }
        }
        return true;
    }

    public int indexOfUser(User user) {
        if (user == null) {
            return -1;
        }

        for (int index = 0; index < size(); index++) {
            if (this.get(index).getId() == user.getId()) {
                return index;
            }
        }
        return -1;
    }

}
