package co.soml.android.app.controllers;

import android.os.Message;

import co.soml.android.app.StoryApp;
import co.soml.android.app.activities.BaseActivity;
import co.soml.android.app.activities.EditStoryActivity;
import co.soml.android.app.fragments.BaseFragment;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.utils.LogUtils;

public class EditStoryController extends BaseController {

	public static final String TAG = LogUtils.makeLogTag(EditStoryController.class.getSimpleName());

	public StoryApp mApp;
	public StoryDataModel mDataModel;
	public EditStoryActivity mActivity;

	public EditStoryController(BaseActivity activity, StoryDataModel dataModel) {
		super(activity, dataModel);

		this.mActivity = (EditStoryActivity) activity;
		this.mDataModel = dataModel;
		this.mStoryApp = (StoryApp) activity.getApplicationContext();
	}

	public EditStoryController(BaseActivity activity, StoryDataModel dataModel, boolean isFirstRunningMode) {
		super(activity, dataModel, isFirstRunningMode);
		if (isFirstRunningMode) {
			this.mActivity = (EditStoryActivity) activity;
			this.mDataModel = dataModel;
			this.mStoryApp = (StoryApp) activity.getApplicationContext();
		}
	}

	public EditStoryController(BaseActivity activity, BaseFragment fragment, StoryDataModel dataModel) {
		super(activity, fragment, dataModel);
	}

	@Override
	protected void handleMessage(Message msg) {

	}
}
