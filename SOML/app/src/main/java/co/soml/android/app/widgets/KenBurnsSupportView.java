package co.soml.android.app.widgets;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.webkit.URLUtil;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import co.soml.android.app.R;
import co.soml.android.app.models.JsonPhoto;

public class KenBurnsSupportView extends FrameLayout {

    // private static final String TAG = "KenBurnsView";
    private final Handler mHandler;
    private final Random mRandom = new Random();

    public ImageView[] mImageViews;
    private View mPlayButton;
    private int mActiveImageIndex = -1;
    private int mActiveUrlIndex = -1;

    private int mSwapMs = 5000;
    private int mFadeInOutMs = 400;
    private float maxScaleFactor = 1.5F;
    private float minScaleFactor = 1.2F;

    private List<String> mImageUrlList = new ArrayList<String>();

    private List<JsonPhoto> mPhotos;

    private boolean isPlaying = false;

    private AnimatorSet mAnimatorSet;
    private ViewPropertyAnimator mPropertyAnimator;

    private Runnable mSwapImageRunnable = new Runnable() {
        @Override
        public void run() {
            swapImage();
            mHandler.postDelayed(mSwapImageRunnable, mSwapMs - mFadeInOutMs * 2);
        }
    };

    private OnClickListener mOnPlayPauseSlideShowHandler = new OnClickListener() {
        @Override
        public void onClick(View view) {
            if (isPlaying) {
                isPlaying = false;
                mPlayButton.setVisibility(VISIBLE);
                stopStory();
            } else {
                isPlaying = true;
                mPlayButton.setVisibility(GONE);
                startStory();
            }
        }
    };

    public KenBurnsSupportView(Context context) {
        this(context, null);
    }

    public KenBurnsSupportView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public KenBurnsSupportView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mHandler = new Handler();
    }

    private void swapImage() {
        if (mImageViews.length <= 0) {
            return;
        }

        if (mActiveImageIndex == -1) {
            mActiveImageIndex = 1;
            animate(mImageViews[mActiveImageIndex]);
            return;
        }

        int inactiveIndex = mActiveImageIndex;
        mActiveImageIndex = (1 + mActiveImageIndex) % mImageViews.length;

        if (mActiveImageIndex >= mImageViews.length) {
            mActiveImageIndex = 1;
        }

        final ImageView activeImageView = mImageViews[mActiveImageIndex];
        activeImageView.setAlpha(0.0f);

        ImageView inactiveImageView = mImageViews[inactiveIndex];
        animate(activeImageView);

        mAnimatorSet = new AnimatorSet();
        mAnimatorSet.setDuration(mFadeInOutMs);
        mAnimatorSet.playTogether(ObjectAnimator.ofFloat(inactiveImageView, "alpha", 1.0f, 0.0f),
                ObjectAnimator.ofFloat(activeImageView, "alpha", 0.0f, 1.0f));
        mAnimatorSet.start();
    }

    private void start(View view, long duration, float fromScale,
                       float toScale, float fromTranslationX, float fromTranslationY,
                       float toTranslationX, float toTranslationY) {
        /*ViewHelper.setScaleX(view, fromScale);
        ViewHelper.setScaleY(view, fromScale);
        ViewHelper.setTranslationX(view, fromTranslationX);
        ViewHelper.setTranslationY(view, fromTranslationY);*/

      /*  mPropertyAnimator = ViewPropertyAnimator.animate(view)
                .translationX(toTranslationX).translationY(toTranslationY)
                .scaleX(toScale).scaleY(toScale).setDuration(duration);*/

        mPropertyAnimator.start();
    }

    private float pickScale() {
        return this.minScaleFactor + this.mRandom.nextFloat()
                * (this.maxScaleFactor - this.minScaleFactor);
    }

    private float pickTranslation(int value, float ratio) {
        return value * (ratio - 1.0f) * (this.mRandom.nextFloat() - 0.5f);
    }

    public void animate(View view) {
        float fromScale = pickScale();
        float toScale = pickScale();
        float fromTranslationX = pickTranslation(view.getWidth(), fromScale);
        float fromTranslationY = pickTranslation(view.getHeight(), fromScale);
        float toTranslationX = pickTranslation(view.getWidth(), toScale);
        float toTranslationY = pickTranslation(view.getHeight(), toScale);

        start(view, this.mSwapMs, fromScale, toScale, fromTranslationX,
                fromTranslationY, toTranslationX, toTranslationY);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        startKenBurnsAnimation();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        stopKenBurnsAnimation();
        //HANDLER.removeCallbacks(mSwapImageRunnable);
    }

    private void startKenBurnsAnimation() {
        mHandler.post(mSwapImageRunnable);
    }

    private void stopKenBurnsAnimation() {
        if (mPropertyAnimator != null) {
            mPropertyAnimator.cancel();
        }

        if (mAnimatorSet != null) {
            mAnimatorSet.cancel();
        }
        mHandler.removeCallbacks(mSwapImageRunnable);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        View view = inflate(getContext(), R.layout.view_kenburns, this);

        mImageViews = new ImageView[2];
        mImageViews[0] = (ImageView) view.findViewById(R.id.previous_images);
        mImageViews[1] = (ImageView) view.findViewById(R.id.next_images);
        mPlayButton = view.findViewById(R.id.play_button);
        mPlayButton.setOnClickListener(mOnPlayPauseSlideShowHandler);
    }

    public void setDefaultImage(int resId) {
        mImageViews[0].setImageResource(resId);
        mHandler.removeCallbacks(mSwapImageRunnable);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }

    public void setImageResIds(int... resourceIds) {
        for (int i = 0; i < mImageViews.length; i++) {
            mImageViews[i].setImageResource(resourceIds[i]);
        }
    }

    public void setImageUrls(List<String> imageList) {
        mImageUrlList.clear();

        // Set new URLs list
        if (imageList.size() > 0) {
            mImageUrlList.addAll(imageList);
        }
    }

    public void setPhotos(List<JsonPhoto> photos) {
        this.mPhotos = photos;
    }

    public void startStory() {
        for (int i = 0; i < mImageViews.length; i++) {
            if (i < mImageUrlList.size()) {
                String uri = mImageUrlList.get(i);
                if (URLUtil.isValidUrl(uri)) {
                    Picasso.with(getContext()).load(uri).into(mImageViews[i]);
                } else {
                    Picasso.with(getContext()).load(new File(uri))
                            .into(mImageViews[i]);
                }
            } else {
                String uri = mImageUrlList.get(0);
                if (URLUtil.isValidUrl(uri)) {
                    Picasso.with(getContext()).load(uri).into(mImageViews[i]);
                } else {
                    Picasso.with(getContext()).load(new File(uri))
                            .into(mImageViews[i]);
                }
            }
        }

        startKenBurnsAnimation();
    }

    public void stopStory() {
        stopKenBurnsAnimation();
    }
}
