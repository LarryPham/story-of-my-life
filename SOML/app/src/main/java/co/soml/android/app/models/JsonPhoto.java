package co.soml.android.app.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its
 * documentation are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable
 * form without the prior written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with
 * respect to the contents, and assumes no responsibility for any errors that might appear in the software and
 * documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.03.2015
 */
public class JsonPhoto implements Serializable {
    public static final long serialVersionUID = 242947293479237492L;

    @SerializedName("id")
    private int mId;
    @SerializedName("story_id")
    private int mStoryId;
    @SerializedName("created_at")
    private Date mCreatedAt;
    @SerializedName("updated_at")
    private Date mUpdatedAt;
    @SerializedName("deleted_at")
    private Date mDeletedAt;
    @SerializedName("longitude")
    private String mLongitude;
    @SerializedName("latitude")
    private String mLatitude;
    @SerializedName("url")
    private String mOriginalUrl;
    @SerializedName("thumb")
    private String mThumbUrl;
    @SerializedName("small")
    private String mSmallUrl;
    @SerializedName("medium")
    private String mMediumUrl;
    @SerializedName("large")
    private String mLargeUrl;

    private String mUserId;
    @SerializedName("caption")
    private String mCaption;
    @SerializedName("file")
    private ImageFile mImageFile;

    public JsonPhoto() {

    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getStoryId() {
        return mStoryId;
    }

    public void setStoryId(int storyId) {
        mStoryId = storyId;
    }

    public Date getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(Date createdAt) {
        mCreatedAt = createdAt;
    }

    public Date getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        mUpdatedAt = updatedAt;
    }

    public Date getDeletedAt() {
        return mDeletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        mDeletedAt = deletedAt;
    }

    public String getLongitude() {
        return mLongitude;
    }

    public void setLongitude(String longitude) {
        mLongitude = longitude;
    }

    public String getLatitude() {
        return mLatitude;
    }

    public void setLatitude(String latitude) {
        mLatitude = latitude;
    }

    public String getOriginalUrl() {
        if (mImageFile != null) {
            mOriginalUrl = mImageFile.mUrl;
        }
        return this.mOriginalUrl;
    }

    public void setOriginalUrl(String originalUrl) {
        if (mImageFile != null) {
            mImageFile.mUrl = originalUrl;
        }
        mOriginalUrl = originalUrl;
    }

    public String getThumbUrl() {
        if (mImageFile != null && mImageFile.mThumbImage != null) {
            mThumbUrl = mImageFile.mThumbImage.url;
        }
        return mThumbUrl;
    }

    public void setThumbUrl(String thumbUrl) {
        if (mImageFile != null && mImageFile.mThumbImage != null) {
            mThumbUrl = mImageFile.mThumbImage.url;
        }
        mThumbUrl = thumbUrl;
    }

    public String getSmallUrl() {
        if (mImageFile != null && mImageFile.mSmallImage != null) {
            mSmallUrl = mImageFile.mSmallImage.url;
        }
        return mSmallUrl;
    }

    public void setSmallUrl(String smallUrl) {
        if (mImageFile != null && mImageFile.mSmallImage != null) {
            mSmallUrl = mImageFile.mSmallImage.url;
        }
        mSmallUrl = smallUrl;
    }

    public String getMediumUrl() {
        if (mImageFile != null && mImageFile.mMediumImage != null) {
            mMediumUrl = mImageFile.mMediumImage.url;
        }
        return mMediumUrl;
    }

    public void setMediumUrl(String mediumUrl) {
        if (mImageFile != null && mImageFile.mMediumImage != null) {
            mImageFile.mMediumImage.url = mediumUrl;
        }
        mMediumUrl = mediumUrl;
    }

    public String getLargeUrl() {
        if (mImageFile != null && mImageFile.mLargeImage != null) {
            mImageFile.mLargeImage.url = mLargeUrl;
        }
        return mLargeUrl;
    }

    public void setLargeUrl(String largeUrl) {
        if (mImageFile != null && mImageFile.mLargeImage != null) {
            mImageFile.mLargeImage.url = mLargeUrl;
        }
        mLargeUrl = largeUrl;
    }

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String userId) {
        mUserId = userId;
    }

    public String getCaption() {
        return mCaption;
    }

    public void setCaption(String caption) {
        mCaption = caption;
    }

    public ImageFile getImageFile() {
        return mImageFile;
    }

    public void setImageFile(ImageFile imageFile) {
        mImageFile = imageFile;
    }

    static class ImageFile {
        @SerializedName("url")
        public String mUrl;
        @SerializedName("thumb")
        public ThumbImage mThumbImage;
        @SerializedName("small")
        public SmallImage mSmallImage;
        @SerializedName("medium")
        public MediumImage mMediumImage;
        @SerializedName("large")
        public LargeImage mLargeImage;
    }

    static class ThumbImage {
        @SerializedName("url")
        public String url;
    }

    static class SmallImage {
        @SerializedName("url")
        public String url;
    }

    static class MediumImage {
        @SerializedName("url")
        public String url;
    }

    static class LargeImage {
        @SerializedName("url")
        public String url;
    }
}
