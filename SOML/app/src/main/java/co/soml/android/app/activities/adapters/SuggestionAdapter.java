package co.soml.android.app.activities.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import co.soml.android.app.R;
import co.soml.android.app.models.Suggestion;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.widgets.CircleImageView;
import co.soml.android.app.widgets.StoryTextView;

/**
 *  Copyright (C) 2015 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation
 *  are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied,
 *  reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior
 *  written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with respect to the contents, and
 *  assumes no responsibility for any errors that might appear in the software and documents. This publication and the
 *  contents hereof are subject to change without notice.
 *  @author Larry Pham(Email: larrypham.vn@gmail.com)
 *  @since 6/23/15 4:17 AM
 **/

public class SuggestionAdapter extends BaseAdapter implements Filterable {

    private static final String TAG = LogUtils.makeLogTag(SuggestionAdapter.class.getSimpleName());
    private final LayoutInflater mInflater;
    private Filter mSuggestionFilter;

    private List<Suggestion> mSuggestionList;
    private List<Suggestion> mOriginalSuggestionList;
    private Context mContext;

    public SuggestionAdapter(Context context) {
        this.mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    public void setSuggestionList(List<Suggestion> suggestionList) {
        mOriginalSuggestionList = suggestionList;
    }

    private class SuggestionFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (mOriginalSuggestionList == null) {
                results.values = null;
                results.count = 0;
            } else  if (constraint == null || constraint.length() == 0) {
                results.values = mOriginalSuggestionList;
                results.count = mOriginalSuggestionList.size();
            } else {
                List<Suggestion> suggestions = new ArrayList<Suggestion>();
                for (Suggestion suggestion : mOriginalSuggestionList) {
                    String lowerCaseConstraint = constraint.toString().toLowerCase();
                    if (suggestion.getEmailAddress().toLowerCase().startsWith(lowerCaseConstraint) || suggestion.getDisplayName()
                            .startsWith(lowerCaseConstraint) || suggestion.getDisplayName().toLowerCase().contains(" " + lowerCaseConstraint)) {
                        suggestions.add(suggestion);
                    }
                }
                results.values = suggestions;
                results.count = suggestions.size();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results.count == 0) {
                notifyDataSetChanged();
            } else {
                mSuggestionList = (List<Suggestion>) results.values;
                notifyDataSetChanged();
            }
        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            Suggestion suggestion = (Suggestion) resultValue;
            return suggestion.getEmailAddress();
        }
    }

    private class SuggestionViewHolder {
        private final CircleImageView mAvatarImage;
        private final StoryTextView mEmailAddress;
        private final StoryTextView mFullName;

        public SuggestionViewHolder(View row) {
            mAvatarImage = (CircleImageView) row.findViewById(R.id.suggest_list_row_avatar);
            mEmailAddress = (StoryTextView) row.findViewById(R.id.suggest_list_row_email_address);
            mFullName = (StoryTextView) row.findViewById(R.id.suggestion_list_row_display_name);
        }
    }

    @Override
    public int getCount() {
        if (mSuggestionList == null) {
            return 0;
        }
        return mSuggestionList.size();
    }

    @Override
    public Suggestion getItem(int position) {
        if (mSuggestionList == null) {
            return null;
        }
        return mSuggestionList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final SuggestionViewHolder holder;
        if (convertView == null || convertView.getTag() == null) {
            convertView = mInflater.inflate(R.layout.suggestion_list_row, parent, false);
            holder = new SuggestionViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (SuggestionViewHolder) convertView.getTag();
        }

        Suggestion suggestion = getItem(position);
        if (suggestion != null) {
            String avatarImageURL = suggestion.getImageURL();
            Picasso.with(mContext).load(avatarImageURL).placeholder(R.drawable.person_image_empty).into(holder.mAvatarImage);
            holder.mEmailAddress.setText("@" + suggestion.getEmailAddress());
            holder.mFullName.setText(suggestion.getDisplayName());
        }
        return convertView;
    }

    @Override
    public Filter getFilter() {
        if (mSuggestionFilter == null) {
            mSuggestionFilter = new SuggestionFilter();
        }
        return mSuggestionFilter;
    }

}
