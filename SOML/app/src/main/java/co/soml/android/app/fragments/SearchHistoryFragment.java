package co.soml.android.app.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;

import java.util.ArrayList;

import co.soml.android.app.R;
import co.soml.android.app.StoryApp;
import co.soml.android.app.activities.InvalidateParam;
import co.soml.android.app.activities.StoryMainActivity;
import co.soml.android.app.controllers.ControllerMessage;
import co.soml.android.app.controllers.SearchController;
import co.soml.android.app.models.StoryBean;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.widgets.SearchMatchedTextView;

public class SearchHistoryFragment extends BaseFragment {

	public static final String TAG = LogUtils.makeLogTag(SearchHistoryFragment.class.getSimpleName());
	private StoryMainActivity mActivity = null;

	private View mSearchHistoryView = null;
	private static String mAutoComplete = "";
	private static String mAutoCompleteSmall = "";

	private StoryApp mApp = null;
	private StoryDataModel mDataModel = null;
	private SearchController mController = null;

	private ArrayList<String> mAutoArray = null;
	private ListView mListView = null;
	private ArrayAdapter<String> mAdapter = null;

	private StoryBean mBean = null;
	private boolean mIsDuplication = false;

	private static SearchHistoryFragment sInstance = null;

	public static SearchHistoryFragment newInstance(Bundle params) {
		mAutoComplete = params.getString("AutoCompletedString");
		if (sInstance == null) {
			sInstance = new SearchHistoryFragment();
		}
		return sInstance;
	}

	public static SearchHistoryFragment getInstance() {
		if (sInstance != null) {
			return sInstance;
		} else {
			final SearchHistoryFragment fragment = new SearchHistoryFragment();
			return fragment;
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		LogUtils.LOGD(TAG, "onCreate()");
		mActivity = (StoryMainActivity) getActivity();
		mApp = (StoryApp) mActivity.getApplication();
		mDataModel = mApp.getAppDataModel();
		mAutoArray = new ArrayList<String>();
		mBean = StoryMainActivity.getBean();
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		LogUtils.LOGD(TAG, "onCreateView()");
		mSearchHistoryView = (View) inflater.inflate(R.layout.search_history, null);
		return mSearchHistoryView;
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		LogUtils.LOGD(TAG, "onActivityCreated() ");
		if (mController == null) {
			mController = new SearchController(mActivity, this, mDataModel);
		}

		mListView = (ListView) mSearchHistoryView.findViewById(R.id.search_history_list);
		mAdapter = new ArrayAdapter<String>(mActivity, R.layout.custom_search_history_item, mAutoArray) {
			private LayoutInflater mLayoutInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				convertView = this.mLayoutInflater.inflate(R.layout.custom_search_history_item, parent, false);
				SearchMatchedTextView textView = (SearchMatchedTextView) convertView.findViewById(R.id.search_history_list_text);
				textView.setText(mAutoArray.get(position), mAutoComplete);
				return convertView;
			}
		};
		mListView.setAdapter(mAdapter);
	}

	@Override
	public void onResume() {
		super.onResume();
		LogUtils.LOGD(TAG, "onResume()");
		if (mController == null) {
			mController = new SearchController(mActivity, this, mDataModel);
		}
		invalidate();
	}

	@Override
	public void onPause() {
		super.onPause();
		LogUtils.LOGD(TAG, "onPause");
	}

	@Override
	public void invalidate() {
		if (mAutoArray != null && !mAutoArray.isEmpty()) {
			mAutoArray.clear();
		}

	}

	@Override
	public void invalidate(Object... params) {
		InvalidateParam param = (InvalidateParam) params[0];
		if (param.getMessage() == ControllerMessage.SHOW_SEARCH_AUTOCOMPLETE_LIST) {
			mAutoComplete = (String) param.getObj();
			invalidate();
		}
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		LogUtils.LOGD(TAG, "onDestroyView");
		mController.onDestroy();;

		if (mSearchHistoryView != null) {
			((ViewGroup) mSearchHistoryView.getParent()).removeView(mSearchHistoryView);
			mSearchHistoryView = null;
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		LogUtils.LOGD(TAG, "onDestroy()");
		//mBean.setSearchHistoryFragment(null);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {

		}
		super.onConfigurationChanged(newConfig);
	}
}
