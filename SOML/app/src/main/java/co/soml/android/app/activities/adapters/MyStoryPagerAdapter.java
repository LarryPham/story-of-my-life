/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 * @author: Larry Pham (email: larrypham.vn@gmail.com)
 * Date: 6/11/15 9:39 AM
 **/

package co.soml.android.app.activities.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Locale;

import co.soml.android.app.R;
import co.soml.android.app.fragments.BaseFragment;
import co.soml.android.app.fragments.MyStoryFragment;
import co.soml.android.app.utils.Lists;
import co.soml.android.app.utils.LogUtils;

public class MyStoryPagerAdapter extends FragmentPagerAdapter {

    public static final String TAG = LogUtils.makeLogTag(MyStoryPagerAdapter.class.getSimpleName());

    public final SparseArray<WeakReference<MyStoryFragment>> mFragmentArray = new SparseArray<WeakReference<MyStoryFragment>>();

    public final List<Holder> mHolderList = Lists.newArrayList();

    public final Context mContext;

    public String mMyStoryType;

    public int mCurrentPage;

    public MyStoryPagerAdapter(Context context, FragmentManager fragmentManager, String myStoryType) {
        super(fragmentManager);
        mContext = context;
        mMyStoryType = myStoryType;
    }

    public void add(final Class<? extends BaseFragment> className, final Bundle params) {
        final Holder holder = new Holder();
        holder.mClassName = className.getName();
        holder.mParam = params;
        final int position = mHolderList.size();
        mHolderList.add(position, holder);
        notifyDataSetChanged();
    }

    @Override
    public MyStoryFragment getItem(int position) {
        final Holder currentHolder = mHolderList.get(position);
        return (MyStoryFragment) MyStoryFragment.instantiate(mContext, currentHolder.mClassName, currentHolder.mParam);
    }

    public MyStoryFragment getFragment(final int position) {
        final WeakReference<MyStoryFragment> mWeakFragment = mFragmentArray.get(position);
        if (mWeakFragment != null && mWeakFragment.get() != null) {
            return mWeakFragment.get();
        }
        return getItem(position);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        final MyStoryFragment fragment = (MyStoryFragment) super.instantiateItem(container, position);
        final WeakReference<MyStoryFragment> weakReference = mFragmentArray.get(position);
        if (weakReference != null) {
            weakReference.clear();
        }

        mFragmentArray.put(position, new WeakReference<MyStoryFragment>(fragment));
        return fragment;
    }

    @Override
    public int getCount() {
        return mHolderList.size();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
        final WeakReference<MyStoryFragment> weakFragment = mFragmentArray.get(position);
        if (weakFragment != null) {
            weakFragment.clear();
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (mMyStoryType.equalsIgnoreCase(mContext.getResources().getString(R.string.my_story_pager_type))) {
            return mContext.getResources().getStringArray(R.array.my_story_page_titles)[position].toUpperCase(Locale.getDefault());
        }
        return null;
    }

    public int getCurrentPage() {
        return mCurrentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.mCurrentPage = currentPage;
    }

    public enum MyStoryFragments {
        /**
         * The Private Stories Fragment
         */
        PRIVATE(MyStoryFragment.class),
        /**
         * The Published Stories Fragment
         */
        PUBLISHED(MyStoryFragment.class),
        /**
         * The Collaborating Stories Fragment
         */
        COLLABORATING(MyStoryFragment.class);
        private Class<? extends BaseFragment> mFragmentClass;

        /**
         * Constructor of <code>CollectionsFragment</code>
         *
         * @param fragmentClass The BaseFragment class
         */
        private MyStoryFragments(final Class<? extends BaseFragment> fragmentClass) {
            mFragmentClass = fragmentClass;
        }

        /**
         * Returns the BaseFragment class which have been interacted into the collections of stories screen
         *
         * @return The BaseFragment class
         */
        public Class<? extends BaseFragment> getFragmentClass() {
            return mFragmentClass;
        }
    }

    public static final class Holder {
        String mClassName;
        Bundle mParam;
    }
}
