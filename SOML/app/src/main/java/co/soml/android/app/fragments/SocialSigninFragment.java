package co.soml.android.app.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import java.util.Arrays;
import java.util.List;

import co.soml.android.app.AppConstants;
import co.soml.android.app.R;
import co.soml.android.app.StoryApp;
import co.soml.android.app.activities.InvalidateParam;
import co.soml.android.app.activities.SessionsActivity;
import co.soml.android.app.activities.StoryMainActivity;
import co.soml.android.app.controllers.ControllerMessage;
import co.soml.android.app.controllers.SessionController;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.models.User;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.widgets.StoryTextView;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.24.2015
 */
public class SocialSigninFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = LogUtils.makeLogTag(SocialSigninFragment.class);

    public static final String FRAGMENT_PARAMS = "SessionsParams";
    public static final String FRAGMENT_PAGE_PARAMS = "SessionPage";

    public static final String FACEBOOK_USER_FRIEND_PERMISSION = "user_friends";
    public static final String FACEBOOK_USER_PHOTOS_PERMISSION = "user_photos";
    public static final String FACEBOOK_USER_EMAIL_PERMISSION = "email";
    public static final String FACEBOOK_USER_BIRTHDAY_PERMISSION = "user_birthday";
    public List<String> mNeedsPermissions = Arrays.asList(FACEBOOK_USER_FRIEND_PERMISSION, FACEBOOK_USER_PHOTOS_PERMISSION,
            FACEBOOK_USER_EMAIL_PERMISSION, FACEBOOK_USER_BIRTHDAY_PERMISSION);

    private StoryTextView mEmailSignInButton;
    private StoryTextView mTwitterLoginButton;
    private StoryTextView mFacebookLoginButton;
    private StoryTextView mEmailSignupButton;

    public SessionsActivity mActivity;
    public SessionController mController;
    public StoryDataModel mDataModel;
    public StoryApp mStoryApp;

    private String mTwitterUserName;
    private long mTwitterUserId;
    private String mTwitterToken;
    private String mTwitterTokenSecret;

    private LoginManager mLoginManager;
    private CallbackManager mCallbackManager;
    private TwitterAuthClient mTwitterAuthClient;

    public static SocialSigninFragment newInstance() {
        LogUtils.LOGD(TAG, "NewInstance()");

        SocialSigninFragment fragment = new SocialSigninFragment();
        return fragment;
    }

    public void setTwitterToken(String token) {
        this.mTwitterToken = token;
    }

    public String getTwitterToken() {
        return this.mTwitterToken;
    }

    public String getTwitterTokenSecret() {
        return mTwitterTokenSecret;
    }

    public void setTwitterTokenSecret(String twitterTokenSecret) {
        mTwitterTokenSecret = twitterTokenSecret;
    }

    public String getTwitterUserName() {
        return mTwitterUserName;
    }

    public void setTwitterUserName(String twitterUserName) {
        mTwitterUserName = twitterUserName;
    }

    public long getTwitterUserId() {
        return mTwitterUserId;
    }

    public void setTwitterUserId(long twitterUserId) {
        mTwitterUserId = twitterUserId;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtils.LOGD(TAG, String.format("OnCreate(): "));
        mActivity = (SessionsActivity) getActivity();
        mStoryApp = (StoryApp) getActivity().getApplicationContext();
        mDataModel = mStoryApp.getAppDataModel();
        mController = new SessionController(mActivity, this, mDataModel);
        FacebookSdk.setApplicationId(AppConstants.FACEBOOK_APP_ID);
        FacebookSdk.sdkInitialize(mActivity);
        mTwitterAuthClient = new TwitterAuthClient();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.layout_signin_socials, null);
        mTwitterLoginButton = (StoryTextView) rootView.findViewById(R.id.twitter_login_button);
        mFacebookLoginButton = (StoryTextView) rootView.findViewById(R.id.facebook_login);
        mEmailSignInButton = (StoryTextView) rootView.findViewById(R.id.signin_email_account_button);
        mEmailSignupButton = (StoryTextView) rootView.findViewById(R.id.email_signup);

        mFacebookLoginButton.setOnClickListener(this);
        mTwitterLoginButton.setOnClickListener(this);
        mEmailSignInButton.setOnClickListener(this);
        mEmailSignupButton.setOnClickListener(this);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        float fbIconScale = 1.45F;
    }

    @Override
    public void onClick(View buttonView) {
        String fn = "SocialSignIn Click: ";
        switch (buttonView.getId()) {
            case R.id.signin_email_account_button: {
                LogUtils.LOGD(TAG, fn + "Email Button");
                final String fragmentKey = "co.soml.android.app.fragments.SigninFragment";
                final Bundle signInBundle = new Bundle();
                mActivity.switchContent(SignInFragment.newInstance(), fragmentKey, signInBundle, false);
                startProgress(R.id.signin_email_account_button);
                break;
            }
            case R.id.twitter_login_button: {
                mTwitterAuthClient.authorize(mActivity, new com.twitter.sdk.android.core.Callback<TwitterSession>() {
                    @Override
                    public void success(Result<TwitterSession> result) {
                        String fn = "Logging Twitter Success - ";
                        LogUtils.LOGD(TAG, fn + String.format("Result: %s", result.toString()));
                        setTwitterUserName(result.data.getUserName());
                        setTwitterUserId(result.data.getId());
                        // Getting the twitter-token and token-secret
                        setTwitterToken(result.data.getAuthToken().token);
                        setTwitterTokenSecret(result.data.getAuthToken().secret);

                        final SessionController.FabricAuthReqParam requestParam = new SessionController
                                .FabricAuthReqParam(getTwitterUserId(), getTwitterToken(), getTwitterTokenSecret());
                        // Getting the token, user_id, and token-secret and putting them into request-params
                        mController.sendMessage(ControllerMessage.REQUEST_TO_SERVER_SIGN_IN_WITH_TWITTER, requestParam);
                        startProgress(R.id.twitter_login_button);
                    }

                    @Override
                    public void failure(TwitterException ex) {
                        LogUtils.LOGD(TAG, String.format("Twitter's Authentication exception: %s", ex));
                        Toast.makeText(getActivity(), getResources().getString(R.string.twitter_authentication_issues), Toast.LENGTH_SHORT).show();
                    }
                });
                break;
            }
            case R.id.facebook_login: {
                LogUtils.LOGD(TAG, ("Logging in with facebook account"));
                mLoginManager = LoginManager.getInstance();
                mCallbackManager = CallbackManager.Factory.create();
                mLoginManager.logInWithReadPermissions(this, mNeedsPermissions);
                final FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        LogUtils.LOGD(TAG, "Success to get the token from facebook's authentication");
                        String fbToken = loginResult.getAccessToken().getToken();
                        SessionController.AuthenticateReqParam facebookRequestParams = new SessionController.AuthenticateReqParam(fbToken);
                        LogUtils.LOGD(TAG, String.format("Facebook's RequestParams-Token: %s", fbToken));
                        mController.sendMessage(ControllerMessage.REQUEST_TO_SERVER_SIGN_IN_WITH_FACEBOOK, facebookRequestParams);
                        startProgress(R.id.facebook_login);
                    }

                    @Override
                    public void onCancel() {
                        LogUtils.LOGD(TAG, "Logging with facebook's authentication access-token cancelled");
                    }

                    @Override
                    public void onError(FacebookException error) {
                        LogUtils.LOGE(TAG, "Cannot logging with facebook's authentication access-token");
                    }
                };
                mLoginManager.registerCallback(mCallbackManager, callback);
                break;
            }
            case R.id.email_signup: {
                LogUtils.LOGD(TAG, "Signup new account by using email");
                final String fragmentKey = "co.soml.android.app.fragments.NewUserFragment";
                final Bundle signInBundle = new Bundle();
                mActivity.switchContent(NewUserFragment.newInstance(), fragmentKey, signInBundle, false);
                startProgress(R.id.email_signup);
                break;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mCallbackManager != null) {
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
        if (mTwitterAuthClient != null) {
            mTwitterAuthClient.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void invalidate() {

    }

    protected void startProgress(int viewId) {
        switch (viewId) {
            case R.id.facebook_login: {
                mEmailSignupButton.setEnabled(false);
                mEmailSignInButton.setEnabled(false);
                mTwitterLoginButton.setEnabled(false);
                break;
            }
            case R.id.twitter_login_button: {
                mEmailSignInButton.setEnabled(false);
                mEmailSignupButton.setEnabled(false);
                mFacebookLoginButton.setEnabled(false);
                break;
            }
            case R.id.email_signup: {
                mEmailSignInButton.setEnabled(false);
                mFacebookLoginButton.setEnabled(false);
                mTwitterLoginButton.setEnabled(false);
                break;
            }
            case R.id.signin_email_account_button: {
                mFacebookLoginButton.setEnabled(false);
                mTwitterLoginButton.setEnabled(false);
                mEmailSignupButton.setEnabled(false);
                break;
            }
        }
    }

    @Override
    public void invalidate(Object... params) {
        InvalidateParam param = (InvalidateParam) params[0];
        if (param.getMessage() == ControllerMessage.REQUEST_TO_SERVER_SIGN_IN_WITH_FACEBOOK_COMPLETED) {
            User subscribedUser = (User) param.getObj();
            if (subscribedUser != null) {
                LogUtils.LOGD(TAG, String.format("Saving the received user[%s]", subscribedUser.getEmail()));
                StoryApp.getInstance().setCurrentUser(subscribedUser);
                // Navigating to the collection of stories activity.
                final Intent collectionsIntent = new Intent(mActivity, StoryMainActivity.class);
                startActivity(collectionsIntent);
                mActivity.finish();
            }
        } else if (param.getMessage() == ControllerMessage.REQUEST_TO_SERVER_SIGN_IN_WITH_TWITTER_COMPLETED) {
            User twitterSubscribedUser = (User) param.getObj();
            if (twitterSubscribedUser != null) {
                LogUtils.LOGD(TAG, String.format("Saving the received user[%s]", twitterSubscribedUser.getFullName()));
                StoryApp.getInstance().setCurrentUser(twitterSubscribedUser);

                // Navigating to the collection of stories activity
                final Intent collectionsIntent = new Intent(mActivity, StoryMainActivity.class);
                startActivity(collectionsIntent);
                mActivity.finish();
            }
        }
        invalidate();
    }

}
