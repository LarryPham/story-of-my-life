/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 7/30/15 2:38 PM.
 **/

package co.soml.android.app.widgets;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import co.soml.android.app.utils.LogUtils;

public class CameraGrid extends View {

    public static final String TAG = LogUtils.makeLogTag(CameraGrid.class.getSimpleName());

    private int mTopBannerWidth = 0;
    private Paint mPaint;
    private boolean mShowGrid = true;

    public CameraGrid(Context context) {
        this(context, null);
    }

    public CameraGrid(Context context, AttributeSet attrs) {
        super (context, attrs);
        init();
    }

    public void init() {
        mPaint = new Paint();
        mPaint.setColor(Color.WHITE);
        mPaint.setAlpha(120);
        mPaint.setStrokeWidth(1f);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int width = this.getWidth();
        int height = this.getHeight();

        if (width < height) {
            mTopBannerWidth = height - width;
        }

        if (isShowGrid()) {
            canvas.drawLine(width / 3, 0, width / 3, height, mPaint);
            canvas.drawLine(width * 2 / 3, 0, width * 2 / 3, height, mPaint);
            canvas.drawLine(0, height / 3, width, height / 3, mPaint);
            canvas.drawLine(0, height * 2 / 3, width, height * 2 / 3, mPaint);
        }
    }

    public boolean isShowGrid() {
        return mShowGrid;
    }

    public void setShowGrid(boolean showGrid) {
        mShowGrid = showGrid;
    }

    public int getTopWidth() {
        return mTopBannerWidth;
    }
}
