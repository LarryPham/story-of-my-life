package co.soml.android.app.datasets;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import co.soml.android.app.StoryApp;
import co.soml.android.app.models.Comment;
import co.soml.android.app.models.Story;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.models.StoryUserIdList;
import co.soml.android.app.models.StoryUserList;
import co.soml.android.app.utils.AccountUtils;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.utils.SqlUtils;

public class StoryLikeTable {

	public static final String TAG = LogUtils.makeLogTag(StoryLikeTable.class.getSimpleName());

	protected static void createTables(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE + StoryLike( story_id INTEGER DEFAULT 0, user_id INTEGER DEFAULT 0, PRIMARY KEY(story_id,"
		 + "user_id))");
		db.execSQL("CREATE TABLE + CommentLike(comment_id INTEGER DEFAULT 0, story_id INTEGER DEFAULT 0, user_id INTEGER DEFAULT 0," +
				" PRIMARY KEY(comment_id, story_id, user_id))");
	}

	protected static void dropTables(SQLiteDatabase db) {
		db.execSQL("DROP TABLE IF EXISTS StoryLike");
		db.execSQL("DROP TABLE IF EXISTS CommentLike");
	}

	protected static void reset(SQLiteDatabase db) {
		dropTables(db);
		createTables(db);
	}

	/**
	 * Purge likes attached to stories/comments that no longer exist
	 * @param db an instance of <code>SQLiteDatabase</code>
	 * @return Integer type.
	 */
	protected static int purge(SQLiteDatabase db) {
		int numDeleted = db.delete("StoryLike", "story_id NOT IN ( SELECT DISTINCT story_id FROM Stories)", null);
		numDeleted += db.delete("CommentLike", "comment_id NOT IN (SELECT DISTINCT comment_id FROM comments)", null);
		return numDeleted;
	}

	public static StoryUserIdList getLikesForStory(Story story) {
		StoryUserIdList userIds = new StoryUserIdList();
		if (story == null) {
			return userIds;
		}

		String[] args = { Long.toString(story.getId())};
		Cursor cursor = StoryDatabase.getReadableDB().rawQuery("SELECT user_id FROM StoryLikes WHERE story_id = ? ",
				args);
		try {
			if (cursor.moveToFirst()) {
				do {
					userIds.add(cursor.getInt(0));
				} while (cursor.moveToNext());
			}
			return userIds;
		} finally {
			SqlUtils.closeCursor(cursor);
		}
	}

	public static int getNumLikesForStory(Story story) {
		if (story == null) {
			return 0;
		}
		String[] args = {Long.toString(story.getId())};
		return SqlUtils.intForQuery(StoryDatabase.getReadableDB(), "SELECT count(*) FROM StoryLikes WHERE story_id=?", args);
	}

	public static void setCurrentUserLikeStory(Story story, boolean isLiked) {
		if (story == null) {
			return;
		}

		long currentUserId = AccountUtils.getUserId(StoryApp.getContext());
		if (isLiked) {
			ContentValues values = new ContentValues();
			values.put("story_id", story.getId());
			values.put("user_id", currentUserId);
			StoryDatabase.getWritableDB().insert("StoryLikes", null, values);
		} else {
			String[] args = { Long.toString(story.getId()), Long.toString(currentUserId)};
			StoryDatabase.getWritableDB().delete("StoryLikes", "story_id=? AND user_id=?", args);
		}
	}

	public static void setLikesForStory(Story story, StoryUserIdList userIds) {
		if (story == null) {
			return;
		}

		SQLiteDatabase db = StoryDatabase.getWritableDB();
		db.beginTransaction();
		SQLiteStatement stmt = db.compileStatement("INSERT INTO StoryLikes (story_id, user_id) VALUES (?1, ?2)");
		try {
			String[] args = { Long.toString(story.getId()) };
			db.delete("StoryLikes", "story_id = ?", args);
			if (userIds != null) {
				stmt.bindLong(1, story.getId());
				for (Integer userId: userIds) {
					stmt.bindLong(2, userId);
					stmt.execute();
				}
			}
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
			SqlUtils.closeStatement(stmt);
		}
	}

	public static StoryUserIdList getLikesForComment(Comment comment) {
		StoryUserIdList userIds = new StoryUserIdList();
		if (comment == null) {
			return userIds;
		}

		String[] args = { Long.toString(comment.getStoryId()), Long.toString(comment.getId())};
		Cursor cursor = StoryDatabase.getReadableDB().rawQuery("SELECT user_id FROM CommentLikes WHERE story_id = ? AND comment_id = ?",
				args);
		try {
			if (cursor.moveToFirst()) {
				do {
					userIds.add(cursor.getInt(0));
				} while (cursor.moveToNext());
			}
			return userIds;
		} finally {
			SqlUtils.closeCursor(cursor);
		}
	}

	/**
	 * Gets the number of likes for the specified comment
	 * @param comment The specified comment
	 * @return Integer type.
	 */
	public static int getNumLikesForComment(Comment comment) {
		if (comment == null) {
			return 0;
		}
		String[] args = {Long.toString(comment.getStoryId()), Long.toString(comment.getId())};
		return SqlUtils.intForQuery(StoryDatabase.getReadableDB(), "SELECT Count(*) FROM CommentLikes WHERE story_id=? AND comment_id=?",
				args);
	}

	/**
	 * Sets likes for the specified comment which current user has been subscribed onto this
	 * @param comment The specified comment
	 * @param isLiked Boolean type. True if the current user has been subscribed onto this comment, false otherwise.
	 */
	public static void setCurrentUserLikesComment(Comment comment, boolean isLiked) {
		if (comment == null) {
			return;
		}

		long currentUserId = AccountUtils.getUserId(StoryApp.getContext());
		if (isLiked) {
			ContentValues values = new ContentValues();
			values.put("story_id", comment.getStoryId());
			values.put("comment_id", comment.getId());
			values.put("user_id", currentUserId);
			StoryDatabase.getWritableDB().insert("CommentLikes", null, values);
		} else {
			String args[] = { Long.toString(comment.getStoryId()), Long.toString(comment.getId()), Long.toString(currentUserId)};
			StoryDatabase.getWritableDB().delete("CommentLikes", "story_id = ? AND comment_id = ? and user_id = ?", args);
		}
	}

	/**
	 * Sets the number of likes for a specified comment
	 * @param comment The specified comment which need to be settled with likes-number
	 * @param userIds The user's id who has been liked this comment
	 */
	public static void setLikesForComment(Comment comment, StoryUserIdList userIds) {
		if (comment == null) {
			return;
		}

		SQLiteDatabase db = StoryDatabase.getWritableDB();
		db.beginTransaction();
		SQLiteStatement stmt = db.compileStatement("INSERT INTO CommentLikes (story_id, comment_id, user_id) VALUES (?1, ?2, ?3)");

		try {
			String[] args = { Long.toString(comment.getStoryId()), Long.toString(comment.getId())};
			db.delete("CommentLikes", "story_id = ? AND comment_id = ?" , args);
			if (userIds != null) {
				stmt.bindLong(1, comment.getStoryId());
				stmt.bindLong(2, comment.getId());
				for (Integer userId: userIds) {
					stmt.bindLong(3, userId);
					stmt.execute();
				}
			}
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
			SqlUtils.closeStatement(stmt);
		}
	}
}
