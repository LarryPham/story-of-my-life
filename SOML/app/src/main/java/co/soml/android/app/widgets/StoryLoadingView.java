package co.soml.android.app.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import co.soml.android.app.R;
import co.soml.android.app.utils.LogUtils;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since May.20.2015
 * <p>
 * The customized View represent to display the middle view for intuitive visualizing the Loading progression, when user input the
 * Signin/Signup form, it will appear after submiting the signin/signup processes.
 */
public class StoryLoadingView extends ViewSwitcher {

    public static final String TAG = LogUtils.makeLogTag(StoryLoadingView.class.getSimpleName());
    protected InnerView mLoadingView;
    protected boolean mDisLoading;

    public StoryLoadingView(Context context) {
        super(context);
        init(null);
    }

    public StoryLoadingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public void init(AttributeSet attrs) {
        if (!isInEditMode()) {
            mLoadingView = new InnerView(getContext());
            addView(mLoadingView);

            if (attrs != null) {
                LoadingConfig config = LoadingConfig.getDefault();
                TypedArray array = getContext().obtainStyledAttributes(attrs, R.styleable.StoryLoadingView, 0, 0);
                try {
                    mDisLoading = array.getBoolean(R.styleable.StoryLoadingView_loading, config.mIsLoading);
                    int color = array.getColor(R.styleable.StoryLoadingView_loadingViewColor, config.mTextColor == null ? 0 : config.mTextColor);
                    if (color != 0) {
                        ((TextView) mLoadingView.findViewById(R.id.loadingViewText)).setTextColor(color);
                    }

                    String str = array.getString(R.styleable.StoryLoadingView_loadingViewTitle);
                    if (str == null) {
                        str = config.mResourceStringId != -1 ? getContext().getString(config.mResourceStringId) : config.mLoading;
                    }
                    setText(str);
                } finally {
                    array.recycle();
                }
            }
        }
    }

    @Override
    public void addView(@NonNull View child, int index, ViewGroup.LayoutParams params) {
        super.addView(child, index, params);
        if (getChildCount() == 2) {
            setLoading(mDisLoading);
        }
    }

    public boolean getLoading() {
        return getCurrentView() == mLoadingView;
    }

    public void setLoading(boolean state) {
        if ((getCurrentView() == mLoadingView) != state) {
            showNext();
        }
    }

    public void setText(int stringResourceId) {
        mLoadingView.setText(stringResourceId);
    }

    public void setText(String loadingText) {
        mLoadingView.setText(loadingText);
    }

    public static class InnerView extends LinearLayout {

        public InnerView(Context context) {
            super(context);
            setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            setOrientation(HORIZONTAL);
            LayoutInflater.from(context).inflate(R.layout.layout_loading_view, this, true);
        }

        public void setText(int stringResourceId) {
            findViewById(R.id.loadingViewText).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.loadingViewText)).setText(stringResourceId);
        }

        public void setText(String loadingText) {
            if (loadingText == null) {
                findViewById(R.id.loadingViewText).setVisibility(View.GONE);
            } else {
                findViewById(R.id.loadingViewText).setVisibility(View.VISIBLE);
                ((TextView) findViewById(R.id.loadingViewText)).setText(loadingText);
            }
        }
    }
}
