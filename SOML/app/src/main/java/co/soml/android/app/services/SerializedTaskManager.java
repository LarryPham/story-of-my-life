package co.soml.android.app.services;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import java.util.WeakHashMap;

import co.soml.android.app.AppConstants;
import co.soml.android.app.services.tasks.ServerSubscribeTask;
import co.soml.android.app.services.tasks.WeakHandler;
import co.soml.android.app.utils.LogUtils;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its
 * documentation are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable
 * form without the prior written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with
 * respect to the contents, and assumes no responsibility for any errors that might appear in the software and
 * documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.02.2015
 * <p>
 * The SerializedTaskManager used to manage each of threads to handle parsing json file, subscribing data
 */
public class SerializedTaskManager {
    static final String TAG = AppConstants.PREFIX + SerializedTaskManager.class.getSimpleName();
    static int INNER_MESSAGE_JOB_DO = 1;
    public static final int STM_SERVER_REQUEST_TO_SUBSCRIBE = 202;

    JobThread mJobThread = null;
    Thread mCurrentJob = null;
    Object mCurrentJobObject = null;
    int mRequestMsg = 0;
    static WeakHashMap<String, SerializedTaskManager> sTaskManagerHashMap = new WeakHashMap<String, SerializedTaskManager>();

    public static SerializedTaskManager resolve(String key) {
        SerializedTaskManager manager = sTaskManagerHashMap.get(key);
        if (manager == null) {
            manager = new SerializedTaskManager();
            sTaskManagerHashMap.put(key, manager);
        }
        return manager;
    }

    private SerializedTaskManager() {
        mJobThread = new JobThread();
        int priority = mJobThread.getPriority();
        if (priority - 1 >= Thread.MIN_PRIORITY) {
            mJobThread.setPriority(priority - 1);
        }
        mJobThread.start();
        while (mJobThread.getJobHandler() == null) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                LogUtils.LOGE(TAG, String.format("SerializedTaskManager throws Exception: %s", e.toString()));
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void finalize() throws Throwable {
        LogUtils.LOGD(TAG, String.format("SerializedTaskManager %s finalized", this));
        if (!mJobThread.isInterrupted()) {
            mJobThread.interrupt();
        }
        super.finalize();
    }

    public void add(Thread newTask) {
        final Message msg = Message.obtain();
        this.mRequestMsg = INNER_MESSAGE_JOB_DO;
        msg.what = INNER_MESSAGE_JOB_DO;
        msg.obj = newTask;
        mJobThread.getJobHandler().sendMessage(msg);
        LogUtils.LOGD(TAG, String.format("New Job added. %s", newTask.getClass().getSimpleName()));
    }

    public void add(Object newTask, int requestMsg) {
        this.mRequestMsg = requestMsg;
        final Message msg = Message.obtain();
        msg.what = requestMsg;
        msg.obj = newTask;
        mJobThread.setRequestMsg(requestMsg);
        mJobThread.getJobHandler().sendMessage(msg);

        LogUtils.LOGD(TAG, String.format("New Job Added. %s", newTask.getClass().getSimpleName()));
    }

    public void cancel() {
        String fn = "cancel(): ";
        if (mJobThread == null) {
            LogUtils.LOGE(TAG, fn + String.format("Error: JobThread is null."));
            return;
        }
        mJobThread.getJobHandler().removeMessages(this.mRequestMsg);
        LogUtils.LOGD(TAG, fn + String.format("Cancel JobThread %s", mJobThread.getClass().getSimpleName()));
        mJobThread.interrupt();

        if (mCurrentJobObject != null) {
            LogUtils.LOGD(TAG, fn + String.format("Cancelling JobThread %s", mJobThread.getClass().getSimpleName()));
            ((ServerSubscribeTask) mCurrentJobObject).setCancelled(true);
        } else if (mCurrentJob != null) {
            LogUtils.LOGD(TAG, fn + String.format("Interrupted currentJob! %s", mCurrentJob.getClass().getSimpleName()));
            mCurrentJob.interrupt();
        }
    }

    class JobThread extends Thread {
        public final String TAG = AppConstants.PREFIX + JobThread.class.getSimpleName();
        WeakHandler mJobHandler = null;
        int mRequestMsg = 0;
        Handler.Callback mJobHandlerCallback = new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                switch (msg.what) {
                    case STM_SERVER_REQUEST_TO_SUBSCRIBE: {
                        mCurrentJobObject = (ServerSubscribeTask) msg.obj;
                        LogUtils.LOGD(TAG, String.format("STM_SERVER_REQUEST_LOGIN_EMAIL_ACCOUNT + %s, %d", currentThread().getName(), mRequestMsg));
                        ServerSubscribeTask task = (ServerSubscribeTask) mCurrentJobObject;
                        task.run();
                        LogUtils.LOGD(TAG, String.format("STM_SERVER_REQUEST_LOGIN_EMAIL_ACCOUNT + %s, %d", currentThread().getName(), mRequestMsg));
                        break;
                    }
                    default: {
                        mCurrentJob = (Thread) msg.obj;
                        LogUtils.LOGD(TAG, String.format("New Job %s + .%s", mCurrentJob.getName(), mCurrentJob.getClass().getSimpleName()));
                        mCurrentJob.start();
                        try {
                            mCurrentJob.join();
                        } catch (InterruptedException ex) {
                            LogUtils.LOGE(TAG, String.format("handleMessage() : Exception: %s", ex.toString()));
                            ex.printStackTrace();
                        }
                        LogUtils.LOGD(TAG, String.format("New Job %s - .%s", mCurrentJob.getName(), mCurrentJob.getClass().getSimpleName()));
                        break;
                    }
                }
                // Releasing the current job
                mCurrentJob = null;
                return true;
            }
        };

        public WeakHandler getJobHandler() {
            return mJobHandler;
        }

        public JobThread() {
        }

        public int getRequestMsg() {
            return mRequestMsg;
        }

        public void setRequestMsg(int requestMsg) {
            mRequestMsg = requestMsg;
        }

        @Override
        public synchronized void run() {
            Looper.prepare();
            LogUtils.LOGD(TAG, String.format("thread %s start. %d", this.getName(), mRequestMsg));
            mJobHandler = new WeakHandler(mJobHandlerCallback);
            Looper.loop();
            LogUtils.LOGD(TAG, String.format("thread %s end. %d", this.getName(), this.getRequestMsg()));
        }
    }
}
