package co.soml.android.app.services;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import java.util.WeakHashMap;

import co.soml.android.app.services.tasks.ServerRequestTask;
import co.soml.android.app.services.tasks.WeakHandler;
import co.soml.android.app.utils.LogUtils;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since May.27.2015
 * <p>
 * The <code>SerializedRequestTaskManager</code> used to manege each of threads to handle request the data from server and patch data
 */
public class SerializedRequestTaskManager {
    static final String TAG = LogUtils.makeLogTag(SerializedRequestTaskManager.class.getSimpleName());
    static int INNER_MESSAGE_JOB_DO = 1;
    public static final int STM_SERVER_REQUEST_TO_GET = 201;
    public static final int STM_SERVER_REQUEST_TO_GET_IMAGES = 202;

    RequestGetJobThread mJobThread = null;
    Thread mCurrentJob = null;
    Object mCurrentJobObject = null;
    int mRequestMessage = 0;
    static WeakHashMap<String, SerializedRequestTaskManager> sTaskManagerWeakHashMap = new WeakHashMap<String, SerializedRequestTaskManager>();

    /**
     * Adds the thread to the instance of current <code>TaskManager</code> if It has been existed, otherwise, allocating new TaskManager
     * object and putting them to this hashmap for storing these thread.
     *
     * @param key The Thread-Key (The key-value for task which has been assigned into this hashmap)
     * @return SerializedRequestTaskManager Object.
     */
    public static SerializedRequestTaskManager resolve(String key) {
        SerializedRequestTaskManager manager = sTaskManagerWeakHashMap.get(key);
        if (manager == null) {
            manager = new SerializedRequestTaskManager();
            sTaskManagerWeakHashMap.put(key, manager);
        }
        return manager;
    }

    /**
     * Private constructor method, used to initializing the job thread and starting it with minimum priority.
     * It's started into the background thread when the service component calling it
     */
    private SerializedRequestTaskManager() {
        mJobThread = new RequestGetJobThread();
        int priority = mJobThread.getPriority();
        if (priority - 1 >= Thread.MIN_PRIORITY) {
            mJobThread.setPriority(priority - 1);
        }
        mJobThread.start();
        while (mJobThread.getJobHandler() == null) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException ex) {
                LogUtils.LOGE(TAG, String.format("SerializedRequestTaskManager throws Exception: %s", ex.toString()));
                ex.printStackTrace();
            }
        }
    }

    /**
     * Finalizing the current instance of <code>TaskRequestManager</code> class, if the current object has stored any thread that they're
     * still alive. They should be interrupted before finalizing this object.
     *
     * @throws Throwable The throwable exception issue maybe raised
     */
    @Override
    protected void finalize() throws Throwable {
        LogUtils.LOGD(TAG, String.format("SerializedRequestTaskManager %s finalized", this));
        if (!mJobThread.isInterrupted()) {
            mJobThread.interrupt();
        }
        super.finalize();
    }

    /**
     * Adds new <code>ServerRequestTask</code>'s instance at here. When triggering a new task for this current manager, we should add new
     * ServerRequestTask which have different action-message for it.
     *
     * @param newTask The ServerRequestTask instance.
     */
    public void add(Object newTask) {
        final Message msg = Message.obtain();
        this.mRequestMessage = INNER_MESSAGE_JOB_DO;
        msg.what = INNER_MESSAGE_JOB_DO;
        msg.obj = newTask;
        mJobThread.getJobHandler().sendMessage(msg);
        LogUtils.LOGD(TAG, String.format("New Job added. %s", newTask.getClass().getSimpleName()));
    }

    /**
     * Adds new <code>ServerRequestTask</code>'s instance at here with specified action-message. When triggering a new task for this
     * current manager, we should add new ServerRequestTask instance which has specified message for it.
     *
     * @param newTask        The ServerRequestTask instance. It will be triggered later.
     * @param requestMessage The Action Message which has been correspond to trigger the task should be called.
     */
    public void add(Object newTask, int requestMessage) {
        this.mRequestMessage = requestMessage;
        final Message msg = new Message();
        msg.what = requestMessage;
        msg.obj = newTask;
        mJobThread.setRequestMessage(requestMessage);
        mJobThread.getJobHandler().sendMessage(msg);
        LogUtils.LOGD(TAG, String.format("New Job added. %s", newTask.getClass().getSimpleName()));
    }

    /**
     * Cancels the current JobThread object, it will interrupt all of tasks into current manager object. when triggering this method, maybe
     * exceptions occurred..
     */
    public void cancel() {
        String fn = "cancel(): ";
        if (mJobThread == null) {
            LogUtils.LOGE(TAG, fn + "Error: JobThread is null..");
            return;
        }
        // removing the action message for current job handler
        mJobThread.getJobHandler().removeMessages(this.mRequestMessage);
        LogUtils.LOGD(TAG, fn + String.format("Canceling JobThread %s", mJobThread.getClass().getSimpleName()));
        mJobThread.interrupt();

        if (mCurrentJobObject != null) {
            LogUtils.LOGD(TAG, String.format("Cancelling JobThread %s", mJobThread.getClass().getSimpleName()));
            ((ServerRequestTask) mCurrentJobObject).setCancelled(true);
        } else if (mCurrentJob != null) {
            LogUtils.LOGD(TAG, String.format("Interrupted currentJob! %s", mCurrentJob.getClass().getSimpleName()));
            mCurrentJob.interrupt();
        }
    }

    /**
     * The class <code>RequestGetJobThread</code>'s instance will trigger an instance of <code>ServerRequestTask</code> to execute the
     * specialized functions.
     */
    class RequestGetJobThread extends Thread {
        public final String JOB_TAG = LogUtils.makeLogTag(RequestGetJobThread.class.getSimpleName());
        WeakHandler mJobHandler = null;
        int mRequestMessage = 0;
        Handler.Callback mJobHandlerCallback = new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                switch (msg.what) {
                    case STM_SERVER_REQUEST_TO_GET: {
                        mCurrentJobObject = (ServerRequestTask) msg.obj;
                        LogUtils.LOGD(TAG, String.format("STM_SERVER_REQUEST_TO_GET + %s, %d", currentThread().getName(), mRequestMessage));
                        ServerRequestTask task = (ServerRequestTask) mCurrentJobObject;
                        task.run();
                        LogUtils.LOGD(TAG, String.format("STM_SERVER_REQUEST_TO_GET - %s, %d", currentThread().getName(), mRequestMessage));
                        break;
                    }
                    default: {
                        mCurrentJob = (Thread) msg.obj;
                        LogUtils.LOGD(TAG, String.format("New Job: %s + .%s", mCurrentJob.getName(), mCurrentJob.getClass().getSimpleName()));
                        mCurrentJob.start();
                        try {
                            mCurrentJob.join();
                        } catch (InterruptedException ex) {
                            LogUtils.LOGE(TAG, String.format("handleMessage() raised exception: %s", ex.getMessage()));
                            ex.printStackTrace();
                        }
                        LogUtils.LOGD(TAG, String.format("New Job: %s - .%s", mCurrentJob.getName(), mCurrentJob.getClass().getSimpleName()));
                        break;
                    }
                }
                // Releasing the current job
                mCurrentJob = null;
                return true;
            }
        };

        public WeakHandler getJobHandler() {
            return this.mJobHandler;
        }

        public RequestGetJobThread() {

        }

        public int getRequestMessage() {
            return this.mRequestMessage;
        }

        public void setRequestMessage(int requestMessage) {
            this.mRequestMessage = requestMessage;
        }

        @Override
        public synchronized void run() {
            Looper.prepare();
            LogUtils.LOGD(JOB_TAG, String.format("Thread %s start. %d ", this.getName(), mRequestMessage));
            mJobHandler = new WeakHandler(mJobHandlerCallback);
            Looper.loop();
            LogUtils.LOGD(JOB_TAG, String.format("Thread %s end. %d ", this.getName(), this.getRequestMessage()));
        }
    }
}
