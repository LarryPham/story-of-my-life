/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 * @author: Larry Pham (email: larrypham.vn@gmail.com)
 * Date: 6/11/15 11:16 PM
 **/

package co.soml.android.app.activities;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.services.tasks.WeakHandler;
import co.soml.android.app.utils.AnimUtils;
import co.soml.android.app.utils.LogUtils;

/**
 * Animation transitions between <em>no content</em> screens (image and caption) and <em>loading</em> screens (caption only).
 */
public class EmptyViewAnimationHandler implements ObjectAnimator.AnimatorListener {
    public static final String TAG = LogUtils.makeLogTag(EmptyViewAnimationHandler.class.getSimpleName());

    /**
     * Interface used to update the calling activity on the animation's progress.
     */
    public interface OnAnimationProgressListener {
        void onSequenceStarted(StoryDataModel.EmptyViewMessageType emptyViewMessageType);

        void onNewTextFadingIn();
    }

    public enum AnimationStage {
        PRE_ANIMATION, FADE_OUT_IMAGE, SLIDE_TEXT_UP, FADE_OUT_CONTENT_TEXT, FADE_IN_LOADING_TEXT,
        IN_BETWEEN, FADE_OUT_LOADING_TEXT, FADE_IN_NO_CONTENT_TEXT, FADE_OUT_NO_CONTENT_TEXT, SLIDE_TEXT_DOWN, FADE_IN_IMAGE,
        FINISHED;

        public AnimationStage next() {
            return AnimationStage.values()[ordinal() + 1];
        }

        public boolean isLowerThan(AnimationStage animationStage) {
            return (ordinal() < animationStage.ordinal());
        }

        public int stagesRemaining() {
            return (IN_BETWEEN.ordinal() - ordinal());
        }
    }

    private static final int ANIMATION_DURATION = 150;
    private static final int MINIMUM_LOADING_DURATION = 500;

    private final View mEmptyViewImage;
    private final TextView mEmptyViewTitle;
    private final OnAnimationProgressListener mListener;

    private AnimationStage mAnimationStage = AnimationStage.PRE_ANIMATION;
    private boolean mHasDisplayedLoadingSequence;
    private ObjectAnimator mObjectAnimator;
    private final int mSlideDistance;

    /**
     * Create a new <code>EmptyViewAnimationHandler</code> with the given UI elements to animate.
     *
     * @param emptyViewImage the <em>no content</em> image <code>View</code>
     * @param emptyViewTitle the caption <code>TextView</code> shared between the <em>no content</em>  and <em>loading</em> screens
     * @param listener       the listener for UI change callbacks
     */
    public EmptyViewAnimationHandler(View emptyViewImage, TextView emptyViewTitle,
                                     OnAnimationProgressListener listener) {
        mEmptyViewImage = emptyViewImage;
        mEmptyViewTitle = emptyViewTitle;
        mListener = listener;
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) mEmptyViewImage.getLayoutParams();
        mSlideDistance = -((layoutParams.height + layoutParams.bottomMargin + layoutParams.topMargin) / 2);
    }

    public boolean isShowingLoadingAnimation() {
        return (mAnimationStage != AnimationStage.PRE_ANIMATION && mAnimationStage.isLowerThan(AnimationStage.IN_BETWEEN));
    }

    public boolean isBetweenSequence() {
        return (mAnimationStage == AnimationStage.IN_BETWEEN);
    }

    public void clear() {
        mAnimationStage = AnimationStage.PRE_ANIMATION;
        mHasDisplayedLoadingSequence = false;
    }

    public void startAnimation(Object target, String propertyName, float fromValue, float toValue) {
        if (mObjectAnimator != null) {
            mObjectAnimator.removeAllListeners();
        }
        mObjectAnimator = ObjectAnimator.ofFloat(target, propertyName, fromValue, toValue);
        mObjectAnimator.setDuration(ANIMATION_DURATION);
        mObjectAnimator.addListener(this);
        mObjectAnimator.start();
    }

    public void showLoadingSequence() {
        mAnimationStage = AnimationStage.FADE_OUT_IMAGE;
        mListener.onSequenceStarted(StoryDataModel.EmptyViewMessageType.LOADING);
        startAnimation(mEmptyViewImage, "alpha", 1f, 0f);
    }

    public void showNoContentSequence() {
        mHasDisplayedLoadingSequence = (isShowingLoadingAnimation() || isBetweenSequence());
        if (isShowingLoadingAnimation()) {
            int delayTime = (mAnimationStage.stagesRemaining() * ANIMATION_DURATION) + MINIMUM_LOADING_DURATION;
            new WeakHandler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mAnimationStage = AnimationStage.FADE_IN_LOADING_TEXT;
                    mListener.onSequenceStarted(StoryDataModel.EmptyViewMessageType.NO_CONTENT);
                    startAnimation(mEmptyViewTitle, "alpha", 1f, 0.1f);
                }
            }, delayTime);
        } else {
            mAnimationStage = AnimationStage.FADE_OUT_LOADING_TEXT;
            mListener.onSequenceStarted(StoryDataModel.EmptyViewMessageType.NO_CONTENT);
            startAnimation(mEmptyViewTitle, "alpha", 1f, 0.1f);
        }
    }

    @Override
    public void onAnimationStart(Animator animation) {

    }

    @Override
    public void onAnimationEnd(Animator animation) {
        mAnimationStage = mAnimationStage.next();
        switch (mAnimationStage) {
            case SLIDE_TEXT_UP: {
                startAnimation(mEmptyViewTitle, "translationY", 0, mSlideDistance);
                break;
            }
            case FADE_OUT_NO_CONTENT_TEXT:
                startAnimation(mEmptyViewTitle, "alpha", 1f, 0.1f);
                break;
            case FADE_IN_LOADING_TEXT:
                mListener.onNewTextFadingIn();
                startAnimation(mEmptyViewTitle, "alpha", 0.1f, 1f);
                break;
            case FADE_IN_NO_CONTENT_TEXT:
                mListener.onNewTextFadingIn();
                startAnimation(mEmptyViewTitle, "alpha", 0.1f, 1f);
                break;
            case SLIDE_TEXT_DOWN:
                startAnimation(mEmptyViewTitle, "translationY", mSlideDistance, 0);
                if (!mHasDisplayedLoadingSequence) {
                    mEmptyViewImage.setVisibility(View.INVISIBLE);
                }
                break;
            case FADE_IN_IMAGE:
                if (!mHasDisplayedLoadingSequence) {
                    AnimUtils.startAnimation(mEmptyViewImage, android.R.anim.fade_in, ANIMATION_DURATION);
                    mEmptyViewImage.setVisibility(View.VISIBLE);
                } else {
                    startAnimation(mEmptyViewImage, "alpha", 0f, 1f);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onAnimationCancel(Animator animation) {
        mAnimationStage = AnimationStage.PRE_ANIMATION;
    }

    @Override
    public void onAnimationRepeat(Animator animation) {

    }
}
