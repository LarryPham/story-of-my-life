package co.soml.android.app.models;

import android.text.TextUtils;

import java.util.ArrayList;

import co.soml.android.app.utils.CommonUtil;
import co.soml.android.app.utils.LogUtils;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its
 * documentation are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable
 * form without the prior written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with
 * respect to the contents, and assumes no responsibility for any errors that might appear in the software and
 * documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.03.2015
 */
public class CommentList extends ArrayList<Comment> {
    public static final String TAG = LogUtils.makeLogTag(CommentList.class.getSimpleName());

    private boolean commenIdExist(long commentId) {
        return (indexOfCommentId(commentId) > -1);
    }

    /**
     * Return index of comment into this current list which has specified id
     *
     * @param commentId The specified id of comment that needed to be seek
     * @return Integer type.
     */
    public int indexOfCommentId(long commentId) {
        for (int index = 0; index < this.size(); index++) {
            if (commentId == this.get(index).getId()) {
                return index;
            }
        }
        return -1;
    }

    public int replaceComment(final long commentId, Comment newComment) {
        if (newComment == null) {
            return -1;
        }

        int index = indexOfCommentId(commentId);

        if (index == -1) {
            return -1;
        }

        newComment.level = this.get(index).level;
        this.set(index, newComment);
        return index;
    }

    /**
     * Replace comments in this list that match the passed list
     *
     * @param comments List of comments that needed to be replaced.
     */
    public void replaceComments(final CommentList comments) {
        if (comments == null || comments.size() == 0) {
            return;
        }

        for (Comment comment : comments) {
            int index = indexOfCommentId(comment.getId());
            if (index > 1) {
                set(index, comment);
            }
        }
    }

    /**
     * Delete the list of comment which has contained specified comment. Into this list of comment, It've already contained comments which
     * has id into the current list
     *
     * @param comments The list of comment
     */
    public void deleteComments(final CommentList comments) {
        if (comments == null || comments.size() == 0) {
            return;
        }
        for (Comment comment : comments) {
            int index = indexOfCommentId(comment.getId());
            if (index > -1) {
                remove(index);
            }
        }
    }

    public Object clone() {
        return super.clone();
    }

    /**
     * Checks the list which has already contained the comment into this list has same instance with current list of comment
     *
     * @param comments The list of comment which need to be checked.
     * @return Boolean type.
     */
    public boolean isSameList(CommentList comments) {
        if (comments == null || comments.size() != this.size()) {
            return false;
        }

        for (Comment comment : comments) {
            if (!commenIdExist(comment.getId()))
                return false;
        }
        return true;
    }


    public static CommentList getLevelList(CommentList thisList) {
        if (thisList == null) {
            return new CommentList();
        }

        boolean hasChildComments = false;
        for (Comment comment : thisList) {
            if (comment.getAncestry() != null) {
                hasChildComments = true;
                break;
            }
        }

        if (!hasChildComments) {
            return thisList;
        }
        // reset all levels, and add root comments to result
        CommentList result = new CommentList();
        for (Comment comment : thisList) {
            comment.level = 0;
            if (TextUtils.isEmpty(comment.getAncestry())) {
                result.add(comment);
            }
        }

        // add child comments under thre parents
        boolean done;
        do {
            done = true;
            for (Comment comment: thisList) {
                // only look at comments that have a parentId but no level assigned yet
                if (!TextUtils.isEmpty(comment.getAncestry()) && comment.level == 0) {
                    int parentIndex = result.indexOfCommentId(CommonUtil.getParentId(comment));
                    if (parentIndex > -1) {
                        comment.level = result.get(parentIndex).level + 1;
                    }

                    //int parentIndex = result.indexOfCommentId(comment.getAncestry());
                    int commentIndex = parentIndex + 1;
                    while (commentIndex < result.size()) {
                        if (result.get(commentIndex).level != comment.level || CommonUtil.getParentId(result.get(commentIndex)) !=
                                CommonUtil.getParentId(comment)) {
                            break;
                        }
                        commentIndex++;
                    }
                    result.add(commentIndex, comment);
                    done = false;

                }
            }
        } while (!done);

        for (Comment comment: thisList) {
            if (comment.level == 0 && !TextUtils.isEmpty(comment.getAncestry())) {
                comment.level = 1;  // give it a non-zero level so it's indented by ReaderCommentAdapter
                result.add(comment);
            }
        }
        return result;
    }
}
