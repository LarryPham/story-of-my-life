/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/14/15 2:19 PM.
 **/

package co.soml.android.app.models;

import java.util.Iterator;
import java.util.List;

import co.soml.android.app.utils.Lists;
import co.soml.android.app.utils.LogUtils;

public class UploadJobList extends StoryDataModel {

    public static final String TAG = LogUtils.makeLogTag(UploadJobList.class.getSimpleName());

    private static List<UploadJob> mUploadJobList = Lists.newArrayList();

    private static int mInsertPosition = 0;

    public UploadJobList() {

    }

    public UploadJob add(String from, String dest) {
        final UploadJob uploadJob = new UploadJob(from, dest);
        mUploadJobList.add(uploadJob);
        return uploadJob;
    }

    public void addEnd(UploadJob pausedJob) {
        mUploadJobList.add(pausedJob);
    }

    public void sequetailAdd(UploadJob pausedJob) {
        mUploadJobList.add(mInsertPosition, pausedJob);
        mInsertPosition++;
    }

    public int findLocation(int id) {
        UploadJob job = null;
        int location = 0;

        for (Iterator<UploadJob> iter = mUploadJobList.iterator(); iter.hasNext();) {
            job = iter.next();
            if (job.getId() == id) {
                return location;
            }
            location++;
        }
        return -1;
    }

    public void remove(int id) {
        int location;
        if (id != 0) {
            location = findLocation(id);
            if (location != -1) {
                mUploadJobList.remove(location);
            }
        }
    }

    public UploadJob getLastUploadJob() {
        UploadJob uploadJob = null;
        uploadJob = mUploadJobList.get(mUploadJobList.size() - 1);
        return uploadJob;
    }

    public UploadJob getAt(Integer key) {
        int location = findLocation(key);

        if (location != -1) {
            return mUploadJobList.get(location);
        }
        return null;
    }

    public List<UploadJob> getInnerList() {
        return mUploadJobList;
    }

    public int getUploadProgress(int dbId) {
        UploadJob job = null;
        for (UploadJob aMUploadJobList : mUploadJobList) {
            job = aMUploadJobList;
            if (job.getStory().getId() == dbId) {
                return job.getProgressValue();
            }
        }
        return -1;
    }

    public int getUploadSize(int id) {
        UploadJob job = null;
        for (Iterator<UploadJob> iter = mUploadJobList.iterator(); iter.hasNext();) {
            job = iter.next();
            if (id != -1 && job.getStory().getId() == id) {
                return job.getProgressValue();
            }
        }
        return -1;
    }

    public UploadJob.UploadState getUploadState(int dbId) {
        UploadJob job = null;

        for (UploadJob aMUploadJobList : mUploadJobList) {
            job = aMUploadJobList;

            if (job.getStory().getId() == dbId) {
                return job.getState();
            }
        }
        return UploadJob.UploadState.NONE;
    }

    public UploadJob.UploadState getUploadState(long id) {
        UploadJob job = null;
        for (UploadJob uploadJob : mUploadJobList) {
            job = uploadJob;
            if (job.getStory().getId() == id) {
                return  job.getState();
            }
        }
        return UploadJob.UploadState.NONE;
    }

    public int getUploadJobId(int id) {
        UploadJob job = null;
        for (Iterator<UploadJob> iter = mUploadJobList.iterator(); iter.hasNext();) {
            job = iter.next();
            if (job.getStory().getId() == id) {
                return job.getId();
            }
        }
        return -1;
    }

    public UploadJob getUploadJob(int id) {
        UploadJob job = null;
        for (Iterator<UploadJob> iter = mUploadJobList.iterator(); iter.hasNext();) {
            job = iter.next();
            if (id != -1 && job.getStory().getId() == id) {
                return job;
            }
        }
        return job;
    }

    public int getUploadJobStoryProgress(int storyId) {
        UploadJob job = null;

        for (Iterator<UploadJob> iter = mUploadJobList.iterator(); iter.hasNext();) {
            job = iter.next();

            if (job.getStory().getId() == storyId) {
                LogUtils.LOGD(TAG, "StoryID: " + storyId + " ----------- jobStoryId : " + job.getStory().getId());
                LogUtils.LOGD(TAG, "JobList: ProgressValue " + job.getProgressValue());
                return job.getProgressValue();
            }
        }
        return -1;
    }

    public int getUploadJobCount() {
        return mUploadJobList.size();
    }
}
