/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 * @author: Larry Pham (email: larrypham.vn@gmail.com)
 * Date: 6/11/15 1:08 PM
 **/

package co.soml.android.app.cache;

import android.content.Context;
import android.support.v8.renderscript.RenderScript;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import co.soml.android.app.utils.LogUtils;

public abstract class ImageWorker {

    public static final String TAG = LogUtils.makeLogTag(ImageWorker.class.getSimpleName());

    public static RenderScript sRenderScript = null;

    public static Set<String> sKeys = Collections.synchronizedSet(new HashSet<String>());

    public static final int FADE_IN_TIME = 200;

    public static final int FADE_IN_TIME_SLOW = 1000;

  /*public final Resources mResources;

  public final ColorDrawable mTransparentDrawable;*/

    protected Context mContext;

    //protected Imag
}
