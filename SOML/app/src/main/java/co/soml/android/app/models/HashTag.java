/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/19/15 4:50 PM.
 **/

package co.soml.android.app.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

import co.soml.android.app.utils.StringUtils;

public class HashTag implements Parcelable, Comparable<HashTag> {
    private String mHashTagName;
    private Date mCreatedAt;
    private Date mUpdatedAt;

    public HashTag(String hashTagName, Date createdAt, Date updatedAt) {
        this.mHashTagName = hashTagName;
        this.mCreatedAt = createdAt;
        this.mUpdatedAt = updatedAt;
    }

    protected HashTag(Parcel in) {

    }

    public String getHashTagName() {
        return StringUtils.notNullStr(mHashTagName);
    }

    public void setHashTagName(String hashTagName) {
        this.mHashTagName = StringUtils.notNullStr(hashTagName);
    }

    public Date getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(Date createdAt) {
        mCreatedAt = createdAt;
    }

    public Date getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        mUpdatedAt = updatedAt;
    }

    public static final Creator<HashTag> CREATOR = new Creator<HashTag>() {
        @Override
        public HashTag createFromParcel(Parcel in) {
            return new HashTag(in);
        }

        @Override
        public HashTag[] newArray(int size) {
            return new HashTag[size];
        }
    };

    public void readFromParcel(Parcel source) {
        mHashTagName = source.readString();
    }

    @Override
    public int compareTo(HashTag another) {
        return 0;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mHashTagName);
    }

    public static boolean isSameHashTag(HashTag hashTag1, HashTag hashTag2) {
        if (hashTag1 == null || hashTag2 == null) {
            return false;
        }
        return hashTag1.mHashTagName.equalsIgnoreCase(hashTag2.mHashTagName)
                && hashTag1.mCreatedAt == hashTag2.mCreatedAt
                && hashTag1.mUpdatedAt == hashTag2.mUpdatedAt;
    }

}
