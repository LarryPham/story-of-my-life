package co.soml.android.app.fragments;

import com.techiedb.libs.droppy.DroppyClickCallbackInterface;
import com.techiedb.libs.droppy.DroppyMenuItem;
import com.techiedb.libs.droppy.DroppyMenuPopup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import co.soml.android.app.R;
import co.soml.android.app.StoryApp;
import co.soml.android.app.activities.AppInterfaces;
import co.soml.android.app.activities.BaseActivity;
import co.soml.android.app.activities.InvalidateParam;
import co.soml.android.app.activities.StoryMainActivity;
import co.soml.android.app.activities.TakePhotoActivity;
import co.soml.android.app.activities.adapters.StoryPostsAdapter;
import co.soml.android.app.camera.activities.CameraActivity;
import co.soml.android.app.controllers.ControllerMessage;
import co.soml.android.app.controllers.MyStoryController;
import co.soml.android.app.fragments.adapters.FeedStoriesAdapter;
import co.soml.android.app.models.Story;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.models.StoryFeedList;
import co.soml.android.app.models.StoryTypes;
import co.soml.android.app.services.VoteResult;
import co.soml.android.app.utils.CommonUtil;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.widgets.StoryRecyclerView;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.14.2015
 */
public class MyStoryFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener,
        AppInterfaces.OnPostPopupListener, AppInterfaces.DataLoadedListener, AppInterfaces.ContributePhotosListener{
    public static final String TAG = LogUtils.makeLogTag(MyStoryFragment.class.getSimpleName());

    public static final String USER_ID = "UserId";
    public static final String SCOPE_NAME = "ScopeName";
    public static final String START_INDEX = "StartIndex";
    public static final String LIMIT = "Limit";
    public static final String STATE = "State";

    public static final String STORY_POST_LIST_TYPE = "StoryFeedType";
    public static final String MY_STORY_VIEW_STATE = "MyStoryViewState";
    public static final String X_API_VERSION = "X_API_VERSION";
    public static final String X_API_TOKEN = "X_API_TOKEN";
    public static final int DEFAULT_MY_STORY_PAGE = 2;

    private Story mStory;
    private StoryApp mStoryApp;
    private BaseActivity mActivity;
    private StoryPostsAdapter mFeedStoriesAdapter;
    private MyStoryController mController;
    private Context mContext;
    private Bundle mParams;
    private ViewGroup mRootView;
    private StoryDataModel mStoryDataModel;
    private StoryFeedList mStories = new StoryFeedList();
    private boolean mNeedRequery = false;

    private StoryTypes.StoryPostListType mStoryFeedType;

    // Params for requesting the list of stories via using Url Params
    private int mApiVersion;
    private String mXAuthToken;
    private int mUserId;
    private int mLimit;
    private String mState;
    private int mStartIndex;
    private String mScopeName;

    @Bind(R.id.mystory_recycler_view)
    StoryRecyclerView mRecyclerView;
    @Bind(R.id.mystory_swipe_refresh_layout)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @Bind(R.id.mystory_empty_view)
    View mEmptyView;

    public DroppyMenuPopup mFeedDroppyMenu;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mStoryApp = (StoryApp) (this.getActivity()).getApplicationContext();
        mContext = this.getActivity().getApplicationContext();
        mStoryDataModel = mStoryApp.getAppDataModel();
        mActivity = (BaseActivity) this.getActivity();
        mParams = getArguments();
        mController = new MyStoryController(mActivity, this, mStoryDataModel);
        if (mParams != null) {
            mStoryFeedType = (StoryTypes.StoryPostListType) mParams.get("StoryFeedType");
            // Token and api version had been passed into Argument bundle for fragment
            mApiVersion = mParams.getInt(X_API_VERSION);
            mXAuthToken = mParams.getString(X_API_TOKEN);

            mScopeName = mParams.getString(SCOPE_NAME);
            mUserId = mParams.getInt(USER_ID);
            mLimit = mParams.getInt(LIMIT);
            mStartIndex = mParams.getInt(START_INDEX);
            mState = mParams.getString(STATE);

            if (mStoryFeedType == null) {
                mStoryFeedType = StoryTypes.StoryPostListType.PUBLISHED_TYPE;
            }
            if (!isExistedData()) {
                if (mStoryFeedType != null && mStoryFeedType == StoryTypes.StoryPostListType.COLLABORATING_TYPE) {
                    final MyStoryController.RequestStoriesParam requestStoriesParam = new MyStoryController
                            .RequestStoriesParam(mApiVersion, mXAuthToken, mUserId, mScopeName, mStartIndex, mLimit, mState);
                    mController.sendMessage(ControllerMessage.REQUEST_TO_SERVER_MY_STORY_COLLABORATING, requestStoriesParam);
                } else {
                    final MyStoryController.RequestStoriesParam requestStoriesParam = new MyStoryController
                            .RequestStoriesParam(mApiVersion, mXAuthToken, mUserId, mStartIndex, mLimit, mState);
                    if (mState != null && mState.equalsIgnoreCase("Private")) {
                        mController.sendMessage(ControllerMessage.REQUEST_TO_SERVER_MY_STORY_PRIVATE, requestStoriesParam);
                    } else {
                        mController.sendMessage(ControllerMessage.REQUEST_TO_SERVER_MY_STORY_PUBLISHED, requestStoriesParam);
                    }
                }
            } else {

            }
        }

        mFeedStoriesAdapter = new StoryPostsAdapter(mContext, mStoryFeedType, mStories);
        mFeedStoriesAdapter.setFeedController(mController);
        mFeedStoriesAdapter.setOnPopupListener(this);
        mFeedStoriesAdapter.setOnDataLoadedListener(this);
        mFeedStoriesAdapter.setContributePhotosListener(this);
    }

    public boolean isExistedData() {
        getStoriesDataModel();
        if (mStories != null && mStories.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public void checkExistedData() {
        if (mStories != null && mStories.size() > 0) {
            LogUtils.LOGD(TAG, "StoryFeedList's not empty");
            mRecyclerView.setVisibility(View.VISIBLE);
            mEmptyView.setVisibility(View.GONE);
        } else {
            LogUtils.LOGD(TAG, "StoryFeedList's empty");
            mRecyclerView.setVisibility(View.GONE);
            mEmptyView.setVisibility(View.VISIBLE);
        }
    }

    public void getStoriesDataModel() {
        switch (mStoryFeedType) {
            case COLLABORATING_TYPE: {
                mStories = mStoryDataModel.getMyStoryCollaboratedList();
                LogUtils.LOGD(TAG, String.format("CollaboratingList's Size Into MyStorisFragment: %d", mStories.size()));
                break;
            }
            case PRIVATE_TYPE: {
                mStories = mStoryDataModel.getMyStoryPrivateList();
                LogUtils.LOGD(TAG, String.format("PrivateList's Size Into MyStoriesFragment: %d", mStories.size()));
                break;
            }
            case PUBLISHED_TYPE: {
                mStories = mStoryDataModel.getMyStoryPublishedList();
                LogUtils.LOGD(TAG, String.format("PublishedList's Size Into MyStoriesFragment: %d", mStories.size()));
                break;
            }
            default:
                break;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = (ViewGroup) inflater.inflate(R.layout.layout_mystory_fragment, container, false);
        ButterKnife.bind(this, mRootView);

        mSwipeRefreshLayout.setOnRefreshListener(this);
        mRecyclerView.addItemDecoration(new StoryRecyclerView.StoryItemDecoration(CommonUtil.dpToPx(-5), CommonUtil.dpToPx(10)));
        mRecyclerView.setAdapter(mFeedStoriesAdapter);
        return mRootView;
    }

    @Override
    public void invalidate() {
        //mFeedStoriesAdapter.notifyDataSetChanged();
        //mRecyclerView.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void invalidate(Object... params) {
        final String fn = "[SIGNAL] Invalidate Params";
        final InvalidateParam param = (InvalidateParam) params[0];
        if (param.getMessage() == ControllerMessage.REQUEST_TO_SERVER_MY_STORY_PUBLISHED_COMPLETED) {
            final StoryFeedList targetedStories = (StoryFeedList) param.getObj();
            if (mStories != null && mStories.size() == 0) {
                if (targetedStories != null && targetedStories.size() > 0) {
                    mStories = targetedStories;
                }
            }
            mEmptyView.setVisibility(View.GONE);
            mSwipeRefreshLayout.setRefreshing(false);
            mFeedStoriesAdapter.setStories(mStories);
            mRecyclerView.getAdapter().notifyDataSetChanged();
        } else if (param.getMessage() == ControllerMessage.REQUEST_TO_SERVER_STORY_LIKE_COMPLETED) {
            final VoteResult result = (VoteResult) param.getObj();

            mFeedStoriesAdapter.getStories().findStoryById(result.getId())
                    .setLikeByCurrentUser(true);
            mFeedStoriesAdapter.getStories().findStoryById(result.getId())
                    .increaseLikeCount();
            mFeedStoriesAdapter.notifyDataSetChanged();
            mRecyclerView.getAdapter().notifyItemChanged(result.getId());
        } else if (param.getMessage() == ControllerMessage.REQUEST_TO_SERVER_STORY_UNLIKE_COMPLETED) {
            final VoteResult result = (VoteResult) param.getObj();
            mFeedStoriesAdapter.getStories().findStoryById(result.getId())
                    .setLikeByCurrentUser(false);
            mFeedStoriesAdapter.getStories().findStoryById(result.getId())
                    .decreaseLikeCount();
            mFeedStoriesAdapter.notifyDataSetChanged();
            mRecyclerView.getAdapter().notifyItemChanged(result.getId());
        }
    }

    private void trySetupSwipeRefresh() {
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setColorSchemeResources(R.color.color_primary_dark, R.color.color_blur_primary, R.color.color_primary);
            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    requestDataRefresh();
                }
            });
        }
    }

    protected void requestDataRefresh() {

    }

    private void updateSwipeRefreshProgressBarTop() {
        if (mSwipeRefreshLayout == null) {
            return;
        }

    }

    public void updateAction() {

    }

    @Override
    public void onResume() {
        super.onResume();
        /*getStoriesDataModel();
        checkExistedData();*/
    }

    @Override
    public void onRefresh() {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onDestroy() {
        LogUtils.LOGD(TAG, String.format("onDestroy(): FeedList's Size: %d", mStories.size()));
        if (mRecyclerView != null) {
            mRecyclerView.clearAnimation();
            mRecyclerView.removeAllViewsInLayout();
            mRecyclerView.setAdapter(null);
        }
        // Recycling all items
        ButterKnife.unbind(this);
        super.onDestroy();
    }

    protected DroppyMenuPopup buildDroppyMenu(final View anchorButton, final Story story) {
        final DroppyMenuPopup.Builder droppyBuilder = new DroppyMenuPopup.Builder(getActivity(), anchorButton);
        final DroppyMenuItem reportMenuItem = new DroppyMenuItem(getString(R.string.report_story_context_menu_content));

        final DroppyMenuItem shareFacebookMenuItem = new DroppyMenuItem(getString(R.string.share_facebook_story_context_menu_content));
        final DroppyMenuItem shareTwitterMenuItem = new DroppyMenuItem(getString(R.string.share_twitter_story_context_menu_content));
        droppyBuilder.addMenuItem(reportMenuItem)
                     .addSeparator()
                     .addMenuItem(shareFacebookMenuItem)
                     .addMenuItem(shareTwitterMenuItem);
        droppyBuilder.setOnClick(new DroppyClickCallbackInterface() {
            @Override
            public void call(View view, int id) {
                final String fn = "[TriggerDroppy]";
                LogUtils.LOGD(TAG, fn + String.format(" Item[%d] was clicked on", id));
            }
        });
        return droppyBuilder.build();
    }

    @Override
    public void onShowPopup(View view, Story story) {
        final DroppyMenuPopup droppyMenuPopup = buildDroppyMenu(view, story);
        droppyMenuPopup.show();
    }

    @Override
    public void onDataLoaded(boolean isEmpty) {

    }

    @Override
    public void onContributePhotos(View view) {
        final int position = (Integer) view.getTag();
        final Intent takePhotoIntent = new Intent(getActivity(), CameraActivity.class);
        getActivity().startActivityForResult(takePhotoIntent, 1);
    }
}
