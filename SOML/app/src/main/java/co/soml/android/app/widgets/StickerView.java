/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/11/15 2:03 PM.
 **/

package co.soml.android.app.widgets;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.FloatMath;
import android.view.MotionEvent;
import android.view.View;

import co.soml.android.app.R;
import co.soml.android.app.utils.DisplayUtils;
import co.soml.android.app.utils.LogUtils;

/**
 * The <code>StickerView</code> represent to display the customized view for translating the overlay-image or rotating the overlay-image
 * above the original image. When user choose the pattern for overlaying the cute-sticker above the original image, It will make the new
 * image from original image.
 */
public class StickerView extends View {
    public static final String TAG = LogUtils.makeLogTag(StickerView.class.getSimpleName());
    private static final float MAX_SCALE_SIZE = 3.2f;
    private static final float MIN_SCALE_SIZE = 0.6f;

    private float[] mOriginPoints;
    private float[] mPoints;
    private RectF mOriginContentRect;
    private RectF mContentRect;
    private RectF mViewRect;
    private float mLastPointX, mLastPointY;
    private Bitmap mBitmap;
    private Bitmap mControllerBitmap, mDeleteBitmap;
    private Matrix mMatrix;

    private Paint mPaint, mBorderPaint;
    private float mControllerWidth, mControllerHeight, mDeleteWidth, mDeleteHeight;

    private boolean mInController, mInMove;
    private boolean mDrawController = true;

    private float mStickerScaleSize = 1.0f;
    private OnStickerDeleteListener mOnStickerDeleteListener;

    public StickerView(Context context) {
        this (context, null);
    }

    public StickerView(Context context, AttributeSet attrs) {
        this (context, attrs, 0);
    }

    public StickerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    protected void init() {
        this.mPaint = new Paint();
        this.mPaint.setAntiAlias(true);
        this.mPaint.setStyle(Paint.Style.STROKE);
        this.mPaint.setStrokeWidth(4.0f);
        this.mPaint.setColor(Color.WHITE);

        mBorderPaint = new Paint(mPaint);
        mBorderPaint.setColor(Color.parseColor("#B2ffffff"));
        mBorderPaint.setShadowLayer(DisplayUtils.dip2px(getContext(), 2.0f), 0, 0, Color.parseColor("#33000000"));

        mControllerBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_sticker_control);
        mControllerWidth = mControllerBitmap.getWidth();
        mControllerHeight = mControllerBitmap.getHeight();

        mDeleteBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_sticker_delete);
        mDeleteWidth = mDeleteBitmap.getWidth();
        mDeleteHeight = mDeleteBitmap.getHeight();
    }

    public void setWaterMark(@NonNull Bitmap bitmap) {
        mBitmap = bitmap;
        mStickerScaleSize = 1.0f;

        setFocusable(true);
        try {
            float px = mBitmap.getWidth();
            float py = mBitmap.getHeight();

            mOriginPoints = new float[] {0, 0, px, 0, px, py, 0, py, px / 2, py / 2};
            mOriginContentRect = new RectF(0, 0, px, py);
            mPoints = new float[10];
            mContentRect = new RectF();

            mMatrix = new Matrix();
            float translateToLeft = ((float) DisplayUtils.getDisplayPixelWidth(getContext()) - mBitmap.getWidth()) / 2;
            float translateToTop = ((float) DisplayUtils.getDisplayPixelHeight(getContext()) - mBitmap.getHeight() / 2);

            mMatrix.postTranslate(translateToLeft, translateToTop);
        } catch (Exception ex) {
            LogUtils.LOGE(TAG, "Throws new exception: %s", ex.getCause());
            ex.printStackTrace();
        }
        postInvalidate();
    }

    public Matrix getMarkMatrix() {
        return mMatrix;
    }

    @Override
    public void setFocusable(boolean focusable) {
        super.setFocusable(focusable);
        postInvalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (mBitmap == null || mMatrix == null) {
            return;
        }

        mMatrix.mapPoints(mPoints, mOriginPoints);
        mMatrix.mapRect(mContentRect, mOriginContentRect);
        canvas.drawBitmap(mBitmap, mMatrix, mPaint);
        if (mDrawController && isFocusable()) {
            canvas.drawLine(mPoints[0], mPoints[1], mPoints[2], mPoints[3], mBorderPaint);
            canvas.drawLine(mPoints[2], mPoints[3], mPoints[4], mPoints[5], mBorderPaint);
            canvas.drawLine(mPoints[4], mPoints[5], mPoints[6], mPoints[7], mBorderPaint);
            canvas.drawLine(mPoints[6], mPoints[7], mPoints[0], mPoints[1], mBorderPaint);

            canvas.drawBitmap(mControllerBitmap, mPoints[4] - mControllerWidth / 2, mPoints[5] - mControllerHeight / 2, mBorderPaint);
            canvas.drawBitmap(mDeleteBitmap, mPoints[0] - mDeleteWidth / 2, mPoints[1] - mDeleteHeight / 2, mBorderPaint);
        }
    }

    public Bitmap getBitmap() {
        Bitmap bitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        mDrawController = false;
        draw(canvas);
        mDrawController = true;
        canvas.save();
        return bitmap;
    }

    public void setShowDrawController(boolean show) {
        mDrawController = show;
    }

    private boolean isInController(float x, float y) {
        int position = 4;
        float radiusX = mPoints[position];
        float radiusY = mPoints[position + 1];
        RectF rectF = new RectF(radiusX - mControllerWidth / 2, radiusY - mControllerHeight / 2, radiusX + mControllerWidth / 2,
                radiusY + mControllerHeight / 2);
        if (rectF.contains(x,y)) {
            return true;
        }
        return false;
    }

    private boolean isInDelete(float x, float y) {
        int position = 0;
        float radiusX = mPoints[position];
        float radiusY = mPoints[position + 1];
        RectF rectF = new RectF(radiusX - mDeleteWidth / 2, radiusY - mDeleteHeight / 2, radiusX + mDeleteWidth / 2, radiusY +
                mDeleteHeight / 2);
        if (rectF.contains(x,y)) {
            return true;
        }
        return false;
    }

    private boolean mInDelete = false;
    @Override
    public boolean dispatchTouchEvent(@NonNull MotionEvent event) {
        if (!isFocusable()) {
            return super.dispatchTouchEvent(event);
        }
        if (mViewRect == null) {
            mViewRect = new RectF(0f, 0f, getMeasuredWidth(), getMeasuredHeight());
        }

        float x = event.getX();
        float y = event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                if (isInController(x,y)) {
                    mInController = true;
                    mLastPointX = x;
                    mLastPointY = y;
                    break;
                }

                if (isInDelete(x,y)) {
                    mInDelete = true;
                    break;
                }

                if (mContentRect.contains(x,y)) {
                    mLastPointX = x;
                    mLastPointY = y;
                    mInMove = true;
                }
                break;
            }
            case MotionEvent.ACTION_UP: {
                if (isInDelete(x, y) && mInDelete) {
                    doDeleteSticker();
                }
                break;
            }
            case MotionEvent.ACTION_CANCEL: {
                mLastPointX = 0;
                mLastPointY = 0;

                mInController = false;
                mInMove = false;
                mInDelete = false;
                break;
            }
            case MotionEvent.ACTION_MOVE:
                if (mInController) {
                    mMatrix.postRotate(rotate(event),mPoints[8], mPoints[9]);
                    float nowLength = calculateLength(mPoints[0], mPoints[1]);
                    float touchLength = calculateLength(event.getX(), event.getY());

                    if (((float)Math.sqrt((nowLength - touchLength) * (nowLength - touchLength)) > 0.0f)) {
                        float scale = touchLength / nowLength;
                        float nowScale = mStickerScaleSize * scale;
                        if (nowScale >= MIN_SCALE_SIZE && nowScale <= MAX_SCALE_SIZE) {
                            mMatrix.postScale(scale, scale, mPoints[8], mPoints[9]);
                            mStickerScaleSize = nowScale;
                        }
                    }
                    invalidate();
                    mLastPointX = x;
                    mLastPointY = y;
                    break;
                }

                if (mInMove) {
                    float cx = x - mLastPointX;
                    float cy = y - mLastPointY;
                    mInController = false;

                    if (((float) Math.sqrt(cx * cx + cy * cy) > 2.0f) && canStickerMove(cx,cy)) {
                        mMatrix.postTranslate(cx,cy);
                        postInvalidate();
                        mLastPointX = x;
                        mLastPointY = y;
                    }
                    break;
                }
                return true;
            }
        return true;
    }

    private void doDeleteSticker() {
        setWaterMark(null);
        if (mOnStickerDeleteListener != null) {
            mOnStickerDeleteListener.onDelete();
        }
    }

    private boolean canStickerMove(float cx, float cy) {
        float px = cx + mPoints[8];
        float py = cy + mPoints[9];
        if (mViewRect.contains(px, py)) {
            return true;
        } else {
            return false;
        }
    }

    private float calculateLength(float x, float y) {
        float ex = x - mPoints[8];
        float ey = y - mPoints[9];
        return (float) Math.sqrt(ex*ex - ey*ey);
    }

    private float rotate(MotionEvent event) {
        float originDegree = calculateDegree(mLastPointX, mLastPointY);
        float nowDegree = calculateDegree(event.getX(), event.getY());
        return nowDegree - originDegree;
    }

    private float calculateDegree(float x, float y) {
        double deltaX = x - mPoints[8];
        double deltaY = y - mPoints[9];
        double radians = Math.atan2(deltaY, deltaX);
        return (float) Math.toDegrees(radians);
    }

    public interface OnStickerDeleteListener {
        void onDelete();
    }

    public void setOnStickerDeleteListener(OnStickerDeleteListener listener) {
        mOnStickerDeleteListener = listener;
    }
}
