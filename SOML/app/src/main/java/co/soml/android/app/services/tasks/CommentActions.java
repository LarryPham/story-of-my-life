/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 7/27/15 11:40 AM.
 **/

package co.soml.android.app.services.tasks;

import co.soml.android.app.StoryApp;
import co.soml.android.app.models.CommentList;
import co.soml.android.app.models.Story;

/**
 * Actions related to comments - replies, moderating, etc.
 * methods below do network calls in the background & update local DB upon success all methods below MUST be called from UI Thread
 */
public class CommentActions {

    private CommentActions() {

    }

    /**
     * Listener when a comment action is performed
     */
    public interface CommentActionListener {
        public void onActionResult(boolean succeeded);
    }

    /**
     * Listener when comments are moderated or deleted.
     */
    public interface OnCommentsModeratedListener {
        public void onCommentsModerated(final CommentList moderatedComments);
    }

    public enum ChangedFrom {
        COMMENT_LIST,
        COMMENT_DETAIL
    }

    public enum ChangedType {
        EDITED, STATUS, REPLIED, TRASHED, SPAMMED
    }

    public interface OnCommentActionListener {
        void onCommentChanged(ChangedFrom changedFrom, ChangedType changedType);
    }
}
