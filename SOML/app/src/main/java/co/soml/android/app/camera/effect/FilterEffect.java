/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/3/15 1:42 PM.
 **/

package co.soml.android.app.camera.effect;

import co.soml.android.app.camera.utils.GPUImageFilterTools;

public class FilterEffect {
    private String mTitle;
    private GPUImageFilterTools.FilterType mType;
    private int mDegree;

    public FilterEffect(String title, GPUImageFilterTools.FilterType type, int degree) {
        this.mType = type;
        this.mDegree = degree;
        this.mTitle = title;
    }

    public GPUImageFilterTools.FilterType getType() {
        return mType;
    }

    public String getTitle() {
        return mTitle;
    }

    public int getDegree() {
        return mDegree;
    }
}
