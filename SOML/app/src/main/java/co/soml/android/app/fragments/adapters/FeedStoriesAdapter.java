package co.soml.android.app.fragments.adapters;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.soml.android.app.AppConstants;
import co.soml.android.app.R;
import co.soml.android.app.StoryApp;
import co.soml.android.app.activities.AppInterfaces;
import co.soml.android.app.controllers.BaseController;
import co.soml.android.app.models.Photo;
import co.soml.android.app.models.Story;
import co.soml.android.app.models.StoryFeedList;
import co.soml.android.app.models.StoryTypes;
import co.soml.android.app.models.User;
import co.soml.android.app.utils.ActivityLauncher;
import co.soml.android.app.utils.CommonUtil;
import co.soml.android.app.utils.DateTimeUtils;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.widgets.CircleImageView;
import co.soml.android.app.widgets.StoryIconCountView;
import co.soml.android.app.widgets.StoryKenBurnsView;
import co.soml.android.app.widgets.StoryRecyclerView;
import co.soml.android.app.widgets.StoryTextView;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since May.29.2015
 */
public class FeedStoriesAdapter extends StoryRecyclerView.Adapter<FeedStoriesAdapter.StoryPostViewHolder> implements View.OnClickListener {
    public static final String TAG = LogUtils.makeLogTag(FeedStoriesAdapter.class.getSimpleName());

    private static final int VIEW_TYPE_DEFAULT = 1;
    private static final int VIEW_TYPE_LOADER = 2;
    private int mCurrentStoryId;

    private static final DecelerateInterpolator DECELERATE_INTERPOLATOR = new DecelerateInterpolator();
    private static final AccelerateInterpolator ACCELERATE_INTERPOLATOR = new AccelerateInterpolator();
    private static final OvershootInterpolator OVERSHOOT_INTERPOLATOR = new OvershootInterpolator();

    private static final int ANIMATED_ITEMS_COUNT = 2;
    private Context mContext;
    private StoryApp mStoryApp;
    private BaseController mController;

    private int mLastAnimatedPosition = -1;
    private int mItemsCount = 0;
    private boolean mAnimatedItems = false;


    private StoryFeedList mStories = new StoryFeedList();
    private StoryTypes.StoryPostListType mStoryPostListType;
    private final Map<Integer, Integer> mLikesCount = new HashMap<Integer, Integer>();
    private final Map<StoryPostViewHolder, AnimatorSet> mLikedAnimations = new HashMap<StoryPostViewHolder, AnimatorSet>();
    private final ArrayList<Integer> mLikedPositions = new ArrayList<Integer>();

    private final Map<Integer, Integer> mCommentsCount = new HashMap<Integer, Integer>();
    private final ArrayList<Integer> mCommentsPosition = new ArrayList<Integer>();

    private boolean mShowLoadingView = false;

    public AppInterfaces.DataLoadedListener mDataLoadedListener;
    public AppInterfaces.OnPostPopupListener mOnPopupListener;
    public AppInterfaces.DataRequestedListener mDataRequestedListener;

    // Size for loading view
    private int mLoadingViewSize = CommonUtil.dpToPx(200);

    protected LayoutInflater mLayoutInflater;

    class StoryPostViewHolder extends RecyclerView.ViewHolder {
        private final StoryTextView mTitle;
        private final StoryTextView mByAuthor;
        private final CircleImageView mUserAvatarImage;
        private final StoryTextView mStatusDate;
        private final StoryTextView mStoryLocation;
        private final StoryIconCountView mCommentCountView;
        private final StoryIconCountView mLikesCountView;
        private final ImageView mOptionsMoreButton;
        private final StoryKenBurnsView mStoryPhotosSlide;
        private View mProgressBackgroundView;
        private Button mContributeButton;

        private FrameLayout mStorySlideFrame;

        private static final boolean EXCLUDE_DESCRIPTION_COLUMN = true;
        // Maximum of the feed-items which will be displayed into the list of stories.
        private static final int MAX_ROWS = AppConstants.MAX_STORY_LIST_ITEMS_TO_DISPLAY;

        public StoryPostViewHolder(View itemView) {
            super(itemView);
            mTitle = (StoryTextView) itemView.findViewById(R.id.story_card_title);
            mByAuthor = (StoryTextView) itemView.findViewById(R.id.story_card_by_user);
            mUserAvatarImage = (CircleImageView) itemView.findViewById(R.id.story_card_user_image);
            mStatusDate = (StoryTextView) itemView.findViewById(R.id.story_card_status_date);
            mStoryLocation = (StoryTextView) itemView.findViewById(R.id.story_card_user_location);
            mCommentCountView = (StoryIconCountView) itemView.findViewById(R.id.story_card_comment_button);
            mLikesCountView = (StoryIconCountView) itemView.findViewById(R.id.story_card_subscribe_button);
            mOptionsMoreButton = (ImageView) itemView.findViewById(R.id.story_card_option_more);
            mStoryPhotosSlide = (StoryKenBurnsView) itemView.findViewById(R.id.story_card_slide_show);
            mContributeButton = (Button) itemView.findViewById(R.id.photo_contribute_button);
        }
    }

    @Override
    public StoryPostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.story_item_view, parent, false);
        StoryPostViewHolder cellViewHolder = new StoryPostViewHolder(view);
        if (viewType == VIEW_TYPE_DEFAULT) {
            cellViewHolder.mLikesCountView.setOnClickListener(this);
            cellViewHolder.mCommentCountView.setOnClickListener(this);
            cellViewHolder.mOptionsMoreButton.setOnClickListener(this);
            cellViewHolder.mUserAvatarImage.setOnClickListener(this);
            cellViewHolder.mContributeButton.setOnClickListener(this);
        } else if (viewType == VIEW_TYPE_LOADER) {
            View backgroundView = new View(mContext);
            backgroundView.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            backgroundView.setBackgroundColor(0x77ffffff);
            cellViewHolder.mStoryPhotosSlide.addView(backgroundView);
            cellViewHolder.mProgressBackgroundView = backgroundView;

            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(mLoadingViewSize, mLoadingViewSize);
            params.gravity = Gravity.CENTER;
        }
        return cellViewHolder;
    }

    @Override
    public void onBindViewHolder(StoryPostViewHolder holder, int position) {
        runEnterAnimation(holder.itemView, position);
        final StoryPostViewHolder viewHolder = (StoryPostViewHolder) holder;
        if (getItemViewType(position) == VIEW_TYPE_DEFAULT) {
            bindFeedItem(position, holder);
        } else if (getItemViewType(position) == VIEW_TYPE_LOADER) {
            bindDefaultFeedItem(position, holder);
        }
    }

    private void runEnterAnimation(View view, int position) {
        if (!mAnimatedItems || position >= ANIMATED_ITEMS_COUNT - 1) {
            return;
        }
        if (position > mLastAnimatedPosition) {
            mLastAnimatedPosition = position;
            view.setTranslationY(CommonUtil.getScreenHeight(mContext));
            view.animate().translationY(0)
                    .setInterpolator(new DecelerateInterpolator(3.f))
                    .setDuration(700)
                    .start();
        }
    }

    private void bindFeedItem(int position, StoryPostViewHolder holder) {
        final Story story = mStories.get(position);
        final List<Photo> mPhotos = story.getPhoto();
        final User user = new User(story.getUser());

        StoryTypes.StoryPostListType storyPostListType = getStoryPostListType();
        holder.mTitle.setText(!TextUtils.isEmpty(story.getName()) ? story.getName() : "");
        if (!(TextUtils.isEmpty(user.getFullName()))) {
            holder.mByAuthor.setVisibility(View.VISIBLE);
            SpannableString byAuthorSpan = new SpannableString("By " + user.getFullName());
            byAuthorSpan.setSpan(new ForegroundColorSpan(Color.BLACK), 0, 2, 0);
            byAuthorSpan.setSpan(new ForegroundColorSpan(Color.parseColor("#0088d3")), 3, byAuthorSpan.length(), 0);
            holder.mByAuthor.setText(byAuthorSpan, TextView.BufferType.SPANNABLE);
        } else {
            holder.mByAuthor.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(story.getLocation())) {
            holder.mStoryLocation.setVisibility(View.VISIBLE);
            holder.mStoryLocation.setText(story.getLocation());
        } else {
            holder.mStoryLocation.setVisibility(View.GONE);
        }
        final String avatarImageUrl = user.getAvatarOriginalUrl();
        Picasso.with(mContext).load(avatarImageUrl)
                .placeholder(R.drawable.person_image_empty).into(holder.mUserAvatarImage);
        holder.mStatusDate.setText(DateTimeUtils.dateToTimeSpan(mContext, story.getCreatedAt()));
        mLikesCount.put(holder.getAdapterPosition(), story.getLikesCount());

        if (storyPostListType == StoryTypes.StoryPostListType.FOLLOWING_TYPE) {
            holder.mLikesCountView.setVisibility(View.VISIBLE);
            holder.mLikesCountView.setCount(story.getLikesCount(), true);
            holder.mCommentCountView.setVisibility(View.VISIBLE);
            holder.mCommentCountView.setCount(story.getCommentsCount(), true);
        } else if (storyPostListType == StoryTypes.StoryPostListType.PRIVATE_TYPE) {
            holder.mCommentCountView.setVisibility(View.GONE);
            holder.mLikesCountView.setVisibility(View.GONE);
        } else {
            holder.mLikesCountView.setVisibility(View.VISIBLE);
            holder.mLikesCountView.setCount(story.getLikesCount(), true);
            holder.mCommentCountView.setVisibility(View.VISIBLE);
            holder.mCommentCountView.setCount(story.getCommentsCount(), true);
        }
        holder.mOptionsMoreButton.setVisibility(View.VISIBLE);

        ArrayList<String> mUrls = new ArrayList<String>();
        for (Photo photo : mPhotos) {
            if (photo.getImageFile() != null && photo.getImageFile().mMediumImage != null) {
                final Photo.MediumImage mediumImage = photo.getImageFile().mMediumImage;
                if (!TextUtils.isEmpty(mediumImage.mUrl)) {
                    mUrls.add(mediumImage.mUrl);
                }
            }
        }

        if (mUrls.size() > 0) {
            holder.mStoryPhotosSlide.setVisibility(View.VISIBLE);
            holder.mStoryPhotosSlide.setImageUrls(mUrls);
            //buildKenBurnsEffect(holder, mUrls);
            //holder.mStoryPhotosSlide.startStory();
        } else {
            holder.mStoryPhotosSlide.setDefaultImage(R.drawable.background);
        }
        updateLikesCounter(holder, false);
        updateHeartButton(holder, false);
        holder.mLikesCountView.setPressed(story.isLikeByCurrentUser());

        holder.mUserAvatarImage.setTag(position);
        holder.mLikesCountView.setTag(position);
        holder.mCommentCountView.setTag(position);
        holder.mOptionsMoreButton.setTag(position);

        holder.mLikesCountView.setTag(holder);
        holder.mStoryPhotosSlide.setTag(holder);
        if (mLikedAnimations.containsKey(holder)) {
            mLikedAnimations.get(holder).cancel();
        }
    }

    private void bindDefaultFeedItem(final int position, final StoryPostViewHolder holder) {
        final Story story = mStories.get(position);
        final List<Photo> mPhotos = story.getPhoto();

        //holder.mStoryPhotosSlide.setPhotos(mPhotos);
    }

    @Override
    public int getItemViewType(int position) {
        if (mShowLoadingView && position == 0) {
            return VIEW_TYPE_LOADER;
        } else {
            return VIEW_TYPE_DEFAULT;
        }
    }

    public void showLoadingView() {
        mShowLoadingView = true;
        notifyItemChanged(0);
    }

    public void setController(BaseController controller) {
        mController = controller;
    }

    public BaseController getController() {
        return this.mController;
    }

    public Story getItem(int position) {
        if (isValidPosition(position)) {
            return mStories.get(position);
        } else {
            return null;
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }

    @Override
    public int getItemCount() {
        return mStories.size();
    }

    @Override
    public long getItemId(int position) {
        return mStories.get(position).getId();
    }

    public FeedStoriesAdapter(Context context, StoryTypes.StoryPostListType postListType, StoryFeedList stories) {
        super();
        mContext = context;
        mStoryApp = (StoryApp) mContext.getApplicationContext();
        mStories = stories;

        mStoryPostListType = postListType;
        int displayWidth = CommonUtil.getScreenWidth(context);
        int displayHeight = CommonUtil.getScreenHeight(context);
        int cardSpacing = context.getResources().getDimensionPixelOffset(R.dimen.story_card_spacing);
        setHasStableIds(true);
    }

    @Override
    public void onClick(View view) {
        final int viewId = view.getId();
        switch (viewId) {
            case R.id.story_card_subscribe_button: {
                StoryPostViewHolder holder = (StoryPostViewHolder) view.getTag();
                if (!mLikedPositions.contains(holder.getAdapterPosition())) {
                    mLikedPositions.add(holder.getAdapterPosition());
                    updateLikesCounter(holder, true);
                    updateHeartButton(holder, false);
                }

                final int position = holder.getAdapterPosition();
                final Story story = getItem(position);

                if (mOnPopupListener != null) {
                    mOnPopupListener.onShowPopup(view, story);
                }

                break;
            }
            case R.id.story_card_comment_button: {
                ActivityLauncher.showStoryComments(view.getContext(), mStories.get(mCurrentStoryId));
                break;
            }
            case R.id.story_card_option_more: {
                if (mOnPopupListener != null) {

                }
                break;
            }
            case R.id.story_card_user_image: {

                break;
            }
            case R.id.photo_contribute_button: {

                break;
            }
        }
    }

    private void updateLikesCounter(StoryPostViewHolder holder, boolean animated) {
        int currentLikesCount = mLikesCount.get(holder.getAdapterPosition()) + 1;
        String likesCountText = mContext.getResources().getQuantityString(R.plurals.like_counts, currentLikesCount, currentLikesCount);
        if (animated) {
            holder.mLikesCountView.setCount(currentLikesCount, true);
        } else {
            holder.mLikesCountView.setCount(currentLikesCount, false);
        }
        mLikesCount.put(holder.getAdapterPosition(), currentLikesCount);
    }

    private void updateHeartButton(final StoryPostViewHolder holder, boolean animated) {
        if (animated) {
            if (!mLikedAnimations.containsKey(holder)) {
                AnimatorSet animatorSet = new AnimatorSet();
                mLikedAnimations.put(holder, animatorSet);

                final ObjectAnimator rotationAnim = ObjectAnimator.ofFloat(holder.mLikesCountView, "rotation", 0f, 360f);
                rotationAnim.setDuration(300);
                rotationAnim.setInterpolator(ACCELERATE_INTERPOLATOR);

                final ObjectAnimator bounceAnimX = ObjectAnimator.ofFloat(holder.mLikesCountView, "scaleX", 0.2f, 1f);
                bounceAnimX.setDuration(300);
                bounceAnimX.setInterpolator(OVERSHOOT_INTERPOLATOR);

                final ObjectAnimator bounceAnimY = ObjectAnimator.ofFloat(holder.mLikesCountView, "scaleY", 0.2f, 1f);
                bounceAnimY.setDuration(300);
                bounceAnimY.setInterpolator(OVERSHOOT_INTERPOLATOR);
                bounceAnimY.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        holder.mLikesCountView.getImageView().setImageResource(R.drawable.ic_like_pressed);
                    }
                });

                animatorSet.play(rotationAnim);
                animatorSet.play(bounceAnimX).with(bounceAnimY).after(rotationAnim);

                animatorSet.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        resetLikeAnimationState(holder);
                    }
                });
                animatorSet.start();
            }
            if (mLikedPositions.contains(holder.getAdapterPosition())) {
                holder.mLikesCountView.getImageView().setImageResource(R.drawable.ic_like_pressed);
            } else {
                holder.mLikesCountView.getImageView().setImageResource(R.drawable.ic_like);
            }
        } else {
            if (mLikedPositions.contains(holder.getAdapterPosition())) {
                holder.mLikesCountView.getImageView().setImageResource(R.drawable.ic_like_pressed);
            } else {
                holder.mLikesCountView.getImageView().setImageResource(R.drawable.ic_like);
            }
        }
    }

    private void resetLikeAnimationState(StoryPostViewHolder holder) {
        mLikedAnimations.remove(holder);
        holder.mLikesCountView.getImageView().setVisibility(View.GONE);
    }

    public StoryTypes.StoryPostListType getStoryPostListType() {
        return (mStoryPostListType != null ? mStoryPostListType : StoryTypes.DEFAULT_STORY_POST_LIST_TYPE);
    }

    private void clear() {
        if (!mStories.isEmpty()) {
            mStories.clear();
            notifyDataSetChanged();
        }
    }

    public void refresh() {
        if (mIsRunningTask) {
            LogUtils.LOGD(TAG, "Loading stories task is already running");
            return;
        }
        new LoadingStoriesTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    /**
     * Removes the specified story which has been had specified index or position
     *
     * @param story The specified story which need to be removed.
     */
    public void removeStory(Story story) {
        removeItem(indexOfStory(story));
    }

    /**
     * Removes an item of this list via the input-position
     *
     * @param position The item's position which need to used for removing the item into the list of stories.
     */
    public void removeItem(int position) {
        if (isValidPosition(position)) {
            mStories.remove(position);
            notifyItemRemoved(position);
        }
    }

    /**
     * Returns true if the input position has been valid, other else will be return true.
     *
     * @param position The input position
     * @return Boolean Type.
     */
    public boolean isValidPosition(int position) {
        return (position >= 0 && position < getItemCount());
    }

    public int indexOfStory(Story story) {
        return mStories.indexOf(story);
    }

    public void updateItems(boolean animated) {
        mItemsCount = 0;
        mAnimatedItems = animated;
        notifyDataSetChanged();
    }

    public void setDataLoadedListener(AppInterfaces.DataLoadedListener listener) {
        mDataLoadedListener = listener;
    }

    private boolean mIsRunningTask = false;

    private class LoadingStoriesTask extends AsyncTask<Void, Void, Boolean> {

        StoryFeedList mStoryFeedList;

        @Override
        protected void onPreExecute() {
            mIsRunningTask = true;
        }

        @Override
        protected void onCancelled() {
            mIsRunningTask = false;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            switch (getStoryPostListType()) {
                case FEATURED_TYPE: {
                    mStoryFeedList = mStoryApp.getAppDataModel().getFeaturedStoryList();
                    return !mStories.isSameList(mStoryFeedList);
                }
                case FOLLOWING_TYPE: {
                    mStoryFeedList = mStoryApp.getAppDataModel().getFollowingStoryList();
                    return !mStories.isSameList(mStoryFeedList);
                }
                case LATEST_TYPE: {
                    mStoryFeedList = mStoryApp.getAppDataModel().getLatestStoryList();
                    return !mStories.isSameList(mStoryFeedList);
                }
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (result) {
                switch (getStoryPostListType()) {
                    case FEATURED_TYPE: {
                        mStories = (StoryFeedList) mStoryFeedList;
                        break;
                    }
                    case FOLLOWING_TYPE: {
                        mStories = (StoryFeedList) mStoryFeedList;
                        break;
                    }
                    case LATEST_TYPE: {
                        mStories = (StoryFeedList) mStoryFeedList;
                        break;
                    }
                }
                notifyDataSetChanged();
            }
            mIsRunningTask = false;

            if (mDataLoadedListener != null) {
                mDataLoadedListener.onDataLoaded(isEmpty());
            }
        }
    }

    public void reloadStories(Story story) {
        int index = indexOfStory(story);
        if (index == -1) {
            return;
        }

    }
    protected boolean mIsTaskRunning = false;

    public class LoadStoriesTask extends AsyncTask<Void, Void, Boolean> {
        StoryFeedList mAllStories;

        @Override
        protected void onPreExecute() {
            mIsTaskRunning = true;
        }

        @Override
        protected void onCancelled() {
            mIsTaskRunning = false;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            return null;
        }
    }

}
