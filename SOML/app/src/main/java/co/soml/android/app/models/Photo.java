package co.soml.android.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

import co.soml.android.app.AppConstants;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its
 * documentation are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable
 * form without the prior written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with
 * respect to the contents, and assumes no responsibility for any errors that might appear in the software and
 * documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.01.2015
 */
public class Photo implements Parcelable, Comparable<Photo>, Serializable {
    public static final String TAG = AppConstants.PREFIX + Photo.class.getSimpleName();
    
    @Expose
    @SerializedName("id")
    private long mId;
    @Expose
    @SerializedName("story_id")
    private long mStoryId;
    
    @Expose
    @SerializedName("created_at")
    private Date mCreatedAt;
    
    @Expose
    @SerializedName("updated_at")
    private Date mUpdatedAt;
    
    @Expose
    @SerializedName("deleted_at")
    private Date mDeletedAt;
    
    @Expose
    @SerializedName("longitude")
    private String mLongitude;
    
    @Expose
    @SerializedName("latitude")
    private String mLatitude;
    
    @Expose
    @SerializedName("caption")
    private String mCaption;
    
    @Expose
    @SerializedName("url")
    private String mOriginalUrl;
    
    @Expose
    @SerializedName("thumb")
    private String mThumbUrl;
    
    @Expose
    @SerializedName("small")
    private String mSmallUrl;
    
    @Expose
    @SerializedName("medium")
    private String mMediumUrl;
    
    @Expose
    @SerializedName("large")
    private String mLargeUrl;

    @Expose
    @SerializedName("file")
    private ImageFile mImageFile;

    private boolean mChecked;
    private boolean mUploaded;

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public long getStoryId() {
        return mStoryId;
    }

    public void setStoryId(long storyId) {
        mStoryId = storyId;
    }

    public Date getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(Date createdAt) {
        mCreatedAt = createdAt;
    }

    public Date getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        mUpdatedAt = updatedAt;
    }

    public Date getDeletedAt() {
        return mDeletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        mDeletedAt = deletedAt;
    }

    public String getLongitude() {
        return mLongitude;
    }

    public void setLongitude(String longitude) {
        mLongitude = longitude;
    }

    public String getLatitude() {
        return mLatitude;
    }

    public void setLatitude(String latitude) {
        mLatitude = latitude;
    }

    public String getCaption() {
        return mCaption;
    }

    public void setCaption(String caption) {
        mCaption = caption;
    }

    public boolean isChecked() {
        return mChecked;
    }

    public void setChecked(boolean checked) {
        mChecked = checked;
    }

    public boolean isUploaded() {
        return mUploaded;
    }

    public void setUploaded(boolean uploaded) {
        mUploaded = uploaded;
    }

    public String getOriginalUrl() {
        return mOriginalUrl;
    }

    public void setOriginalUrl(String originalUrl) {
        if (mImageFile != null) {
            mImageFile.mUrl = originalUrl;
        }
        mOriginalUrl = originalUrl;
    }

    public String getThumbUrl() {
        return mThumbUrl;
    }

    public void setThumbUrl(String thumbUrl) {
        if (mImageFile != null) {
            mImageFile.mThumbImage.mUrl = thumbUrl;
        }
        mThumbUrl = thumbUrl;
    }

    public String getSmallUrl() {
        final SmallImage smallImage = mImageFile.mSmallImage;
        if (smallImage != null && smallImage.mUrl != null) {
            return smallImage.mUrl;
        }
        return mSmallUrl;
    }

    public void setSmallUrl(String smallUrl) {
        final SmallImage smallImage = new SmallImage();
        if (mImageFile != null) {
            smallImage.mUrl = smallUrl;
            mImageFile.mSmallImage = smallImage;
        }
        mSmallUrl = smallUrl;
    }

    public String getMediumUrl() {
        return mMediumUrl;
    }

    public void setMediumUrl(String mediumUrl) {
        final MediumImage mediumImage = new MediumImage();
        mediumImage.mUrl = mediumUrl;
        if (mImageFile != null) {
            mImageFile.mMediumImage = mediumImage;
        }
        mMediumUrl = mediumUrl;
    }

    public String getLargeUrl() {
        return mLargeUrl;
    }

    public void setLargeUrl(String largeUrl) {
        if (mImageFile != null) {
            mImageFile.mLargeImage.mUrl = largeUrl;
        }
        mLargeUrl = largeUrl;
    }

    public ImageFile getImageFile() {
        return mImageFile;
    }

    public void setImageFile(ImageFile imageFile) {
        mImageFile = imageFile;
    }

    public Photo(Parcel in) {
        readFromParcel(in);
    }

    public Photo(String data, Date createdAt) {
        this.mOriginalUrl = data;
        this.mCreatedAt = createdAt;
    }
    public Photo(long id, String caption, String imageUrl, Date createdAt) {
        this.mId = id;
        this.mCaption = caption;
        this.mOriginalUrl = imageUrl;
        this.mCreatedAt = createdAt;
    }

    public Photo(long id, long storyId, String caption, String imageUrl, Date createdAt, Date updatedAt,
                 Date deletedAt, String longitude, String latitude) {
        this.mId  = id;
        this.mStoryId = storyId;
        this.mCaption = caption;
        this.mOriginalUrl = imageUrl;
        this.mCreatedAt = createdAt;
        this.mUpdatedAt = updatedAt;
        this.mDeletedAt = deletedAt;
        this.mLongitude = longitude;
        this.mLatitude = latitude;
    }

    public Photo(Photo photo) {
        this.mId = photo.getId();
        this.mStoryId = photo.getStoryId();
        this.mCreatedAt = photo.getCreatedAt();
        this.mUpdatedAt = photo.getUpdatedAt();
        this.mDeletedAt = photo.getDeletedAt();

        this.mOriginalUrl = photo.getOriginalUrl();
        this.mLargeUrl = photo.getLargeUrl();
        this.mMediumUrl = photo.getMediumUrl();
        this.mSmallUrl = photo.getSmallUrl();
        this.mImageFile = photo.getImageFile();

        this.mLongitude = photo.getLongitude();
        this.mLatitude = photo.getLatitude();
        this.mCaption = photo.getCaption();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.mId);
        dest.writeLong(this.mStoryId);
        dest.writeSerializable(mCreatedAt);
        dest.writeSerializable(mUpdatedAt);
        dest.writeSerializable(mDeletedAt);
        dest.writeString(mLongitude);
        dest.writeString(mLatitude);
        dest.writeString(mCaption);
    }

    public void readFromParcel(Parcel source) {
        this.mId = source.readLong();
        this.mStoryId = source.readLong();
        this.mCreatedAt = (Date) source.readSerializable();
        this.mUpdatedAt = (Date) source.readSerializable();
        this.mDeletedAt = (Date) source.readSerializable();

        this.mLongitude = source.readString();
        this.mLatitude = source.readString();
        this.mCaption = source.readString();

        this.mOriginalUrl = source.readString();
        this.mSmallUrl = source.readString();
        this.mMediumUrl = source.readString();
        this.mLargeUrl = source.readString();
        this.mImageFile = (ImageFile) source.readSerializable();
    }

    public static final Creator<Photo> CREATOR = new Creator<Photo>() {
        @Override
        public Photo createFromParcel(Parcel in) {
            return new Photo(in);
        }

        @Override
        public Photo[] newArray(int size) {
            return new Photo[size];
        }
    };

    @Override
    public int compareTo(Photo another) {
        if (another == null) {
            return 1;
        }
        return 0;
    }

    public static class ImageFile implements Parcelable, Serializable {
        
        @Expose
        @SerializedName("url")
        public String mUrl;
        
        @Expose
        @SerializedName("thumb")
        public ThumbImage mThumbImage;
        
        @Expose
        @SerializedName("small")
        public SmallImage mSmallImage;
        
        @Expose
        @SerializedName("medium")
        public MediumImage mMediumImage;
        
        @Expose
        @SerializedName("large")
        public LargeImage mLargeImage;

        protected ImageFile(Parcel in) {
            mUrl = in.readString();
            mThumbImage = in.readParcelable(ThumbImage.class.getClassLoader());
            mSmallImage = in.readParcelable(SmallImage.class.getClassLoader());
            mMediumImage = in.readParcelable(MediumImage.class.getClassLoader());
            mLargeImage = in.readParcelable(LargeImage.class.getClassLoader());
        }

        public static final Creator<ImageFile> CREATOR = new Creator<ImageFile>() {
            @Override
            public ImageFile createFromParcel(Parcel in) {
                return new ImageFile(in);
            }

            @Override
            public ImageFile[] newArray(int size) {
                return new ImageFile[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(mUrl);
            dest.writeParcelable(mThumbImage, flags);
            dest.writeParcelable(mSmallImage, flags);
            dest.writeParcelable(mMediumImage, flags);
            dest.writeParcelable(mLargeImage, flags);
        }
    }
    
    static class ThumbImage implements Parcelable, Serializable {
        
        @Expose
        @SerializedName("url")
        public String mUrl;

        protected ThumbImage(Parcel in) {
            mUrl = in.readString();
        }

        public static final Creator<ThumbImage> CREATOR = new Creator<ThumbImage>() {
            @Override
            public ThumbImage createFromParcel(Parcel in) {
                return new ThumbImage(in);
            }

            @Override
            public ThumbImage[] newArray(int size) {
                return new ThumbImage[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(mUrl);
        }
    }
    
    public static class SmallImage implements Parcelable, Serializable {
     
        @Expose
        @SerializedName("url")
        public String mUrl;

        public SmallImage() {

        }
        protected SmallImage(Parcel in) {
            mUrl = in.readString();
        }

        public static final Creator<SmallImage> CREATOR = new Creator<SmallImage>() {
            @Override
            public SmallImage createFromParcel(Parcel in) {
                return new SmallImage(in);
            }

            @Override
            public SmallImage[] newArray(int size) {
                return new SmallImage[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(mUrl);
        }
    }
    
    public static class MediumImage implements Parcelable, Serializable {
        
        @Expose
        @SerializedName("url")
        public String mUrl;

        public MediumImage() {

        }

        protected MediumImage(Parcel in) {
            mUrl = in.readString();
        }

        public static final Creator<MediumImage> CREATOR = new Creator<MediumImage>() {
            @Override
            public MediumImage createFromParcel(Parcel in) {
                return new MediumImage(in);
            }

            @Override
            public MediumImage[] newArray(int size) {
                return new MediumImage[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(mUrl);
        }
    }
    
    public static class LargeImage implements Parcelable, Serializable{
        
        @Expose
        @SerializedName("url")
        public String mUrl;

        protected LargeImage(Parcel in) {
            mUrl = in.readString();
        }

        public static final Creator<LargeImage> CREATOR = new Creator<LargeImage>() {
            @Override
            public LargeImage createFromParcel(Parcel in) {
                return new LargeImage(in);
            }

            @Override
            public LargeImage[] newArray(int size) {
                return new LargeImage[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(mUrl);
        }
    }
}
