/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/26/15 9:13 PM.
 **/

package co.soml.android.app.services;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VoteResult extends BaseResult {

    public int mId;

    @Expose
    @SerializedName("vote")
    public String mVote;

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getVote() {
        return mVote;
    }

    public void setVote(String vote) {
        mVote = vote;
    }
}
