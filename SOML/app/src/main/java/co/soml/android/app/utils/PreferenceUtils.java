package co.soml.android.app.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.26.2015
 * <p>
 * Utilities and constants related to app preferences.
 */
public class PreferenceUtils {
    /**
     * Boolean indicating whether we performed the (one-time) introduction flow.
     **/
    public static final String PREF_INTRODUCTION_DONE = "pref_introduction_done";

    public static PreferenceUtils sInstance;

    public static void init(final Context context) {
        // Check what year we're configured for
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

    }

    public static boolean isIntroductionDone(final Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getBoolean(PREF_INTRODUCTION_DONE, false);
    }

    public static void markIntroductionDone(final Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        sp.edit().putBoolean(PREF_INTRODUCTION_DONE, true).apply();
    }

    public static int getUserId(final Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getInt("user_id", -1);
    }

    public static void setUserId(final Context context, int userId) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        sp.edit().putInt("user_id", userId).apply();
    }

    public static void registerOnSharedPreferenceChangedListener(final Context context, SharedPreferences.OnSharedPreferenceChangeListener
            listener) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        sp.registerOnSharedPreferenceChangeListener(listener);
    }

    public static void unregisterOnSharedPreferenceChangeListener(final Context context, SharedPreferences.OnSharedPreferenceChangeListener
            listener) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        sp.unregisterOnSharedPreferenceChangeListener(listener);
    }

    public static void setMyStoryStartPage(final Context context, int startPage) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        sp.edit().putInt("MyStory_StartPage", startPage).apply();
    }

    public static void setCollectionsStartPage(final Context context, int startPage) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        sp.edit().putInt("Collections_StartPage", startPage).apply();
    }

    public static int getCollectionsStartPage(final Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getInt("Collections_StartPage", -1);
    }

    public static int getMyStoryStartPage(final Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getInt("MyStory_StartPage", -1);
    }
}
