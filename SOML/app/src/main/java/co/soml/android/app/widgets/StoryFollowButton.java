package co.soml.android.app.widgets;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.LinearLayout;


import co.soml.android.app.R;
import co.soml.android.app.utils.LogUtils;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since May.26.2015
 * <p>
 * The Follow Button used throughout the story functions
 */
public class StoryFollowButton extends LinearLayout {
    public static final String TAG = LogUtils.makeLogTag(StoryFollowButton.class.getSimpleName());
    private StoryTextView mFollowText;
    private boolean mIsFollowed;

    public StoryFollowButton(Context context) {
        super(context);
        initView(context);
    }

    public StoryFollowButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public StoryFollowButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        inflate(context, R.layout.layout_story_follow_button, this);
        mFollowText = (StoryTextView) findViewById(R.id.text_follow_button);
    }

    public void updateFollowText() {
        mFollowText.setSelected(mIsFollowed);
        mFollowText.setText(mIsFollowed ? R.string.story_unfollow_button_content : R.string.story_follow_button_content);
    }

    public void setIsFollowed(boolean isFollowed) {
        setIsFollowed(isFollowed, true);
    }

    public void setIsFollowedAnimated(boolean isFollowed) {
        setIsFollowed(isFollowed, true);
    }

    private void setIsFollowed(boolean isFollowed, boolean animateChanges) {
        if (isFollowed == mIsFollowed && mFollowText.isSelected() == isFollowed) {
            return;
        }

        mIsFollowed = isFollowed;
        if (animateChanges) {
            ObjectAnimator anim = ObjectAnimator.ofFloat(mFollowText, View.SCALE_Y, 1f, 0f);
            anim.setRepeatMode(ValueAnimator.REVERSE);
            anim.setRepeatCount(1);
            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationRepeat(Animator animation) {
                    updateFollowText();
                }
            });
            long duration = getContext().getResources().getInteger(android.R.integer.config_shortAnimTime);
            android.animation.AnimatorSet set = new android.animation.AnimatorSet();
            set.play(anim);
            set.setDuration(duration);
            set.setInterpolator(new AccelerateDecelerateInterpolator());
            set.start();
        } else {
            updateFollowText();
        }
    }
}
