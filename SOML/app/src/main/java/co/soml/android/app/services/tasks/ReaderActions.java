/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 7/27/15 4:27 PM.
 **/

package co.soml.android.app.services.tasks;

import co.soml.android.app.models.Comment;
import co.soml.android.app.models.Story;

/**
 * Method into some classes like actions classes which change state (like, follow, etc) are generally optimistic and work like this:
 * 1. Caller asks method to send a network request which changes state
 * 2. Method changes state in local data and resturns to caller *before* network request completes
 * 3. Caller can access local state change without waiting for the network request.
 * 4. If the network request fails, the method restores the previous state of the local data
 * 5. If caller passes a listener, it can be alerted to the actual success/failure of the request
 *
 * Note that all method MUST be called from the UI thread in order to guarantee that listeners are alerted on the UI thread.
 */
public class ReaderActions {

    private ReaderActions() {
        throw new AssertionError();
    }

    public interface ActionListener {
        void onActionResult(boolean succeeded);
    }

    public interface CommentActionListener {
        void onActionResult(boolean succeeded, Comment newComment);
    }

    public enum UpdateResult {
        HAS_NEW,
        CHANGED,
        UNCHANGED,
        FAILED;

        public boolean isNewOrChanged() {
            return (this == HAS_NEW || this == CHANGED);
        }
    }

    public interface UpdateResultListener {
        void onUpdateResult(UpdateResult result);
    }

    public interface UpdateStoryInfoListener {
        void onResult(Story story);
    }
}
