/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/6/15 6:59 AM.
 **/

package co.soml.android.app.camera.utils;

import android.graphics.Matrix;

public class MatrixUtils {
    public static void mapPoints(Matrix matrix, float[] points)
    {
        float[] m = { 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F };
        matrix.getValues(m);

        points[0] = (points[0] * m[0] + m[2]);
        points[1] = (points[1] * m[4] + m[5]);

        if (points.length == 4) {
            points[2] = (points[2] * m[0] + m[2]);
            points[3] = (points[3] * m[4] + m[5]);
        }
    }

    public static float[] getScale(Matrix matrix)
    {
        float[] points = new float[9];
        matrix.getValues(points);
        return new float[] { points[0], points[4] };
    }

    public static float[] getValues(Matrix m)
    {
        float[] values = new float[9];
        m.getValues(values);
        return values;
    }
}
