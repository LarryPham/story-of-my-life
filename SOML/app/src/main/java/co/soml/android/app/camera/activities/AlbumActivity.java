/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/6/15 5:24 AM.
 **/

package co.soml.android.app.camera.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import co.soml.android.app.AppConstants;
import co.soml.android.app.R;
import co.soml.android.app.camera.utils.FileUtils;
import co.soml.android.app.fragments.AlbumFragment;
import co.soml.android.app.models.Album;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.utils.StringUtils;
import co.soml.android.app.widgets.StoryViewPager;
public class AlbumActivity extends AppCompatActivity {
    public static final String TAG = LogUtils.makeLogTag(AlbumActivity.class.getSimpleName());
    private Map<String,Album> mAlbumMap;
    private List<String> mPaths = new ArrayList<String>();

    private TabLayout mTab;
    private StoryViewPager mPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_layout);

        mTab = (TabLayout) findViewById(R.id.indicator);
        mPager = (StoryViewPager) findViewById(R.id.album_pager);
        FragmentPagerAdapter adapter = new TabAlbumPageIndicatorAdapter(getSupportFragmentManager());
        mPager.setAdapter(adapter);
        mTab.setupWithViewPager(mPager);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent result) {
        if (requestCode == AppConstants.REQUEST_CROP && resultCode == RESULT_OK) {
            final Intent intent = new Intent(this, PhotoProcessActivity.class);
            intent.setData(result.getData());
        }
    }

    class TabAlbumPageIndicatorAdapter extends FragmentPagerAdapter {

        public TabAlbumPageIndicatorAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            return AlbumFragment.newInstance(mAlbumMap.get(mPaths.get(position)).getPhotos());
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Album album = mAlbumMap.get(mPaths.get(position % mPaths.size()));
            if (StringUtils.equalsIgnoreCase(FileUtils.getInstance().getSystemPhotoPath(), album.getAlbumUri())){
                return getString(R.string.app_name);
            } else if (album.getTitle().length() > 13){
                return album.getTitle().substring(0,11) + "...";
            }
            return album.getTitle();
        }

        @Override
        public int getCount() {
            return mPaths.size();
        }
    }
}
