/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/14/15 1:29 PM.
 **/

package co.soml.android.app.models;

import java.util.ArrayList;

import co.soml.android.app.utils.LogUtils;

public class StickerPack {
    public static final String TAG = LogUtils.makeLogTag(StickerPack.class.getSimpleName());

    protected String mName;
    protected int mStickerPackId;
    protected String mImageUrl;
    protected int mNumbersOfStickers = 0;
    protected ArrayList<Sticker> mStickers;

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        mImageUrl = imageUrl;
    }

    public int getNumbersOfStickers() {
        return mNumbersOfStickers;
    }

    public void setNumbersOfStickers(int numbersOfStickers) {
        mNumbersOfStickers = numbersOfStickers;
    }

    public ArrayList<Sticker> getStickers() {
        return mStickers;
    }

    public void setStickers(ArrayList<Sticker> stickers) {
        mStickers = stickers;
    }

    public int getStickerPackId() {
        return mStickerPackId;
    }

    public void setStickerPackId(int stickerPackId) {
        mStickerPackId = stickerPackId;
    }

    public StickerPack(int stickerPackId, String name, String imageUrl, ArrayList<Sticker> sticker) {
        this.mStickerPackId = stickerPackId;
        this.mName = name;
        this.mImageUrl = imageUrl;
        this.mStickers = sticker;
        if (mStickers.size() > 0) {
            mNumbersOfStickers = mStickers.size();
        }
    }

    public Sticker getStickerAtIndex(int index) {
        if (index >= 0 && index < mStickers.size()) {
            return mStickers.get(index);
        }
        return null;
    }

    public int getStickerCount() {
        return mStickers.size();
    }

    public boolean hasFullDetail() {
        if (mStickers.size() == mNumbersOfStickers) {
            return true;
        }
        return false;
    }

    public void copyStickerFrom(StickerPack others) {
        mStickers = others.getStickers();
    }
}
