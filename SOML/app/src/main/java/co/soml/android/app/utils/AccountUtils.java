package co.soml.android.app.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import co.soml.android.app.AppConstants;
import co.soml.android.app.models.Account;
import co.soml.android.app.models.User;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.17.2015
 * <p>
 * Account and login utilities. This class manages a local shared preferences object that stores which account is currently active,
 * and can store associated information such as Email Profile info(name, image URL, cover URL) and also the auth token associated with
 * the account.
 */
public class AccountUtils {
    private static final String TAG = LogUtils.makeLogTag(AccountUtils.class);

    private static final String PREF_ACTIVE_ACCOUNT_NAME = "choosen_account_name";
    private static final String PREF_ACTIVE_ACCOUNT_TYPE = "choosen_account_type";

    private static final String PREFIX_PREF_USER_ID = "user_id";
    private static final String PREFIX_PREF_AUTH_TOKEN = "auth_token";
	private static final String PREFIX_PREF_USER_DESCRIPTION = "user_description";

    private static final String PREFIX_PREF_FACEBOOK_AUTH_TOKEN = "facebook_auth_token";
    private static final String PREFIX_PREF_TWITTER_AUTH_TOKEN = "fabric_auth_token";
    private static final String PREFIX_PREF_FACEBOOK_AUTH_TOKEN_SECRET = "facebook_auth_token_secret";
    private static final String PREFIX_PREF_TWITTER_AUTH_TOKEN_SECRET = "fabric_auth_token_secret";
    // Kind of profiles account
    private static final String PREFIX_PREF_EMAIL_PROFILE_ID = "email_profile_id_";
    private static final String PREFIX_PREF_FACEBOOK_PROFILE_ID = "facebook_profile_id_";
    private static final String PREFIX_PREF_TWITTER_PROFILE_ID = "twitter_profile_id_";
    // Email Profile Account
    private static final String PREFIX_PREF_EMAIL_NAME = "account_email_name";
    private static final String PREFIX_PREF_EMAIL_IMAGE_URL = "account_email_image_url";
    private static final String PREFIX_PREF_EMAIL_COVER_URL = "account_email_cover_url";
    // Facebook Profile Account
    private static final String PREFIX_PREF_FACEBOOK_NAME = "account_facebook_name";
    private static final String PREFIX_PREF_FACEBOOK_IMAGE_URL = "account_facebook_image_url";
    private static final String PREFIX_PREF_FACEBOOK_COVER_URL = "account_facebook_cover_url";
    // Twitter Profile Account
    private static final String PREFIX_PREF_TWITTER_NAME = "account_twitter_name";
    private static final String PREFIX_PREF_TWITTER_IMAGE_URL = "account_twitter_image_url";
    private static final String PREFIX_PREF_TWITTER_COVER_URL = "account_twitter_cover_url";

	private static final String PREF_CURRENT_USER_INFO = "current_user_info";

    /**
     * Returns the default shared preferences object of current application. At the first time, application will allocated new preferences
     * object for storing all settings for other screens or components can use this preferences as global-access points,
     * We can use it to store manual customized settings
     *
     * @param context The Application Context
     * @return SharedPreferences. The <code>SharedPreferences</code> instance
     */
    private static SharedPreferences getSharedPreferences(final Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    /**
     * Checks user has account into our app which has been activated or not
     *
     * @param context The Application Context
     * @return Boolean Type.
     */
    public static boolean hasActiveAccount(final Context context) {
        return !TextUtils.isEmpty(getActiveAccountName(context));
    }

    /**
     * Returns the active account's name which had already stored into the <code>Preference</code> Object.
     *
     * @param context The Application Context
     * @return String Type. The active account-name
     */
    public static String getActiveAccountName(final Context context) {
        SharedPreferences sp = getSharedPreferences(context);
        return sp.getString(PREF_ACTIVE_ACCOUNT_NAME, null);
    }

    /**
     * Returns the active account's type which had already stored into the <code>Preference</code> Object.
     *
     * @param context The Application Context.
     * @return String Type. The active account-type.
     */
    public static String getActiveAccountType(final Context context) {
        SharedPreferences sp = getSharedPreferences(context);
        return sp.getString(PREF_ACTIVE_ACCOUNT_TYPE, null);
    }

    public static Account getActiveAccount(final Context context) {
        final String accountName = getActiveAccountName(context);
        final String accountType = getActiveAccountType(context);

        if (accountName != null && accountType != null) {
            return new Account(accountName, accountType);
        } else {
            return null;
        }
    }

    /**
     * Saves the active account-name which we want to store into the <code>Preference</code> object.
     *
     * @param context     The Application Context
     * @param accountName The User Account Name
     * @param accountType The User Account Type
     * @return Boolean Type.
     */
    public static boolean setActiveAccount(final Context context, final String accountName, final String accountType) {
        LogUtils.LOGD(TAG, "Set Active Account to: " + accountName);
        SharedPreferences sp = getSharedPreferences(context);
        sp.edit().putString(PREF_ACTIVE_ACCOUNT_TYPE, accountType).apply();
        sp.edit().putString(PREF_ACTIVE_ACCOUNT_NAME, accountName).apply();
        return true;
    }

    public static String makeAccountSpecificPrefKey(final Context context, final String prefix) {
        return hasActiveAccount(context) ? makeAccountSpecificPrefKey(getActiveAccountName(context), prefix) : null;
    }

    /**
     * Returns new string which was combined prefix tag and account name
     *
     * @param accountName The User Account Name
     * @param prefix      The Prefix
     */
    public static String makeAccountSpecificPrefKey(String accountName, String prefix) {
        return prefix + accountName.trim();
    }

    /**
     * Returns the current Authentication's token for signing into our app with user-email address,
     * or signing with social networks such as facebook of twitter.
     *
     * @param context The Application Context
     * @return String Type. The current Authentication's Token String.
     */
    public static String getAuthToken(final Context context) {
        SharedPreferences sp = getSharedPreferences(context);
        return hasActiveAccount(context) ? sp.getString(makeAccountSpecificPrefKey(context, PREFIX_PREF_AUTH_TOKEN), null) : null;
    }

    public static int getUserId(final Context context) {
        SharedPreferences sp = getSharedPreferences(context);
        return hasActiveAccount(context) ? sp.getInt(makeAccountSpecificPrefKey(context, PREFIX_PREF_USER_ID), 0) : 0;
    }

    /**
     * Saves the user's id with specified user id for users, It will be stored into the Preferences object
     *
     * @param context The Application Context.
     * @param userId  The UserId
     */
    public static void setUserId(final Context context, final int userId) {
        LogUtils.LOGD(TAG, String.format("UserId: %d", userId));
        SharedPreferences sp = getSharedPreferences(context);
        sp.edit().putInt(makeAccountSpecificPrefKey(context, PREFIX_PREF_USER_ID), userId).apply();
        LogUtils.LOGV(TAG, "UserId: " + userId);
    }

	public static void setUserDescription(final Context context, final String userDescription) {
		LogUtils.LOGD(TAG, String.format("User's Description: %s", userDescription));
		SharedPreferences sp = getSharedPreferences(context);
		sp.edit().putString(makeAccountSpecificPrefKey(context, PREFIX_PREF_USER_DESCRIPTION), userDescription).apply();
		LogUtils.LOGV(TAG, "UserDescription: " + userDescription);
	}

	public static String getUserDescription(final Context context) {
		SharedPreferences sp = getSharedPreferences(context);
		return hasActiveAccount(context) ? sp.getString(makeAccountSpecificPrefKey(context, PREFIX_PREF_USER_DESCRIPTION), null) : null;
	}

    /**
     * Saves the Authentication's token with specified AccountName for users, It will be stored into the Preferences object
     *
     * @param context     The Application Context
     * @param accountName The User Account Name
     * @param authToken   String Type. The Authentication's token.
     */
    public static void setAuthToken(final Context context, final String accountName, final String authToken) {
        LogUtils.LOGI(TAG, "Auth token of length " + (TextUtils.isEmpty(authToken) ? 0 : authToken.length() + " for " + accountName));
        SharedPreferences sp = getSharedPreferences(context);
        sp.edit().putString(makeAccountSpecificPrefKey(accountName, PREFIX_PREF_AUTH_TOKEN), authToken).apply();
        LogUtils.LOGV(TAG, "Auth Token: " + authToken);
    }

    /**
     * Saves the Facebook's Authentication-Token with specified AccountName for users, It will be stored into the default preferences object
     *
     * @param context     The Application Context
     * @param accountName The User Account Name
     * @param authToken   String Type. The Authentication's token.
     */
    public static void setFacebookAuthToken(final Context context, final String accountName, final String authToken) {
        LogUtils.LOGD(TAG, String.format("Facebook Auth Token Of Length: %d for %s", (TextUtils.isEmpty(authToken)) ? 0 : authToken.length(),
                accountName));
        SharedPreferences sp = getSharedPreferences(context);
        sp.edit().putString(makeAccountSpecificPrefKey(accountName, PREFIX_PREF_FACEBOOK_AUTH_TOKEN), authToken).apply();
        LogUtils.LOGV(TAG, String.format("Facebook AuthToken: %s", authToken));
    }

    public static void setFacebookAccountProfileImageURL(final Context context, final String accountName, final String imageUrl) {
        LogUtils.LOGD(TAG, String.format("Facebook Profile Image URL: %s", (TextUtils.isEmpty(imageUrl)) ? null : imageUrl));
        SharedPreferences sp = getSharedPreferences(context);
        sp.edit().putString(makeAccountSpecificPrefKey(accountName, PREFIX_PREF_FACEBOOK_IMAGE_URL), imageUrl).apply();
        LogUtils.LOGV(TAG, String.format("Facebook Profile ImageURL: %s", imageUrl));
    }

    public static String getFacebookAccountProfileImageURL(final Context context, final String accountName) {
        SharedPreferences sp = getSharedPreferences(context);
        return hasActiveAccount(context) ? sp.getString(makeAccountSpecificPrefKey(accountName, PREFIX_PREF_FACEBOOK_IMAGE_URL), null) : null;
    }

    public static void setFacebookAccountCoverImageURL(final Context context, final String accountName, final String coverImageURL) {
        LogUtils.LOGD(TAG, String.format("Facebook Cover Image URL: %s", (TextUtils.isEmpty(coverImageURL)) ? null : coverImageURL));
        SharedPreferences sp = getSharedPreferences(context);
        sp.edit().putString(makeAccountSpecificPrefKey(accountName, PREFIX_PREF_FACEBOOK_COVER_URL), coverImageURL).apply();
        LogUtils.LOGV(TAG, String.format("Facebook Cover Image URL: %s", coverImageURL));
    }

    public static String getFacebookAccountCoverImageURL(final Context context, final String accountName) {
        SharedPreferences sp = getSharedPreferences(context);
        return hasActiveAccount(context) ? sp.getString(makeAccountSpecificPrefKey(accountName, PREFIX_PREF_FACEBOOK_COVER_URL), null) : null;
    }

    public static void setTwitterAccountProfileImageURL(final Context context, final String accountName, final String imageUrl) {
        LogUtils.LOGD(TAG, String.format("Twitter Profile Image URL: %s", (TextUtils.isEmpty(imageUrl)) ? null : imageUrl));
        SharedPreferences sp = getSharedPreferences(context);
        sp.edit().putString(makeAccountSpecificPrefKey(accountName, PREFIX_PREF_TWITTER_IMAGE_URL), imageUrl).apply();
        LogUtils.LOGD(TAG, String.format("Twitter Profile ImageURL: %s", imageUrl));
    }

    public static String getTwitterAccountProfileImageUrl(final Context context, final String accountName) {
        SharedPreferences sp = getSharedPreferences(context);
        return hasActiveAccount(context) ? sp.getString(makeAccountSpecificPrefKey(accountName, PREFIX_PREF_TWITTER_IMAGE_URL), null) : null;
    }

    public static void setTwitterAccountCoverImageURL(final Context context, final String accountName, final String coverImageURL) {
        LogUtils.LOGD(TAG, String.format("Twitter Cover Image URL: %s", (TextUtils.isEmpty(coverImageURL)) ? null : coverImageURL));
        SharedPreferences sp = getSharedPreferences(context);
        sp.edit().putString(makeAccountSpecificPrefKey(accountName, PREFIX_PREF_TWITTER_COVER_URL), coverImageURL).apply();
        LogUtils.LOGV(TAG, String.format("Twitter Cover Image URL: %s", coverImageURL));
    }

    public static String getTwitterCoverImageURL(final Context context, final String accountName) {
        SharedPreferences sp = getSharedPreferences(context);
        return hasActiveAccount(context) ? sp.getString(makeAccountSpecificPrefKey(accountName, PREFIX_PREF_TWITTER_COVER_URL), null) : null;
    }

    /**
     * Saves the Twitter's Authentication-Token with specified AccountName for users, It will be stored into the default preferences object
     *
     * @param context     The Application Context
     * @param accountName The User Account Name
     * @param authToken   String Type. The Authentication's token.
     */
    public static void setTwitterAuthToken(final Context context, final String accountName, final String authToken) {
        LogUtils.LOGD(TAG, String.format("Twitter Auth Token of Length: %d for %s", (TextUtils.isEmpty(authToken)) ? 0 : authToken.length(),
                accountName));
        SharedPreferences sp = getSharedPreferences(context);
        sp.edit().putString(makeAccountSpecificPrefKey(accountName, PREFIX_PREF_TWITTER_AUTH_TOKEN), authToken).apply();
        LogUtils.LOGV(TAG, String.format("Twitter AuthToken: %s", authToken));
    }

    /**
     * Saves the Twitter's AuthenticationToken Secret with specified Account Name for users. It will be stored into the default preferences
     * object
     *
     * @param context         The Application Context
     * @param accountName     The User Account Name
     * @param authTokenSecret String Type. The Authentication's Token Secret string.
     */
    public static void setTwitterAuthTokenSecret(final Context context, final String accountName, final String authTokenSecret) {
        LogUtils.LOGD(TAG, String.format("Twitter Auth Token of Length: %d for %s", (TextUtils.isEmpty(authTokenSecret)) ? 0 :
                authTokenSecret.length(), accountName));
        SharedPreferences sp = getSharedPreferences(context);
        sp.edit().putString(makeAccountSpecificPrefKey(accountName, PREFIX_PREF_FACEBOOK_AUTH_TOKEN_SECRET), authTokenSecret).apply();
        LogUtils.LOGV(TAG, String.format("Twitter AuthTokenSecret: %s", authTokenSecret));
    }

    /**
     * Saves the Twitter's UserId to the {@link android.content.SharedPreferences} instance
     *
     * @param context     The Application Context
     * @param accountName The User Account Name
     * @param profileId   The Twitter's UserId
     */
    public static void setTwitterUserId(final Context context, final String accountName, final long profileId) {
        LogUtils.LOGD(TAG, String.format("Storing the Twitter user-id: %d", profileId));
        SharedPreferences sp = getSharedPreferences(context);
        sp.edit().putString(makeAccountSpecificPrefKey(accountName, PREFIX_PREF_TWITTER_PROFILE_ID), String.valueOf(profileId)).apply();
        LogUtils.LOGV(TAG, String.format("Twitter UserId: %d", profileId));
    }

    public static void setAuthToken(final Context context, final String authToken) {
        if (hasActiveAccount(context)) {
            setAuthToken(context, getActiveAccountName(context), authToken);
        } else {
            LogUtils.LOGE(TAG, "Can't set auth token because there is no chosen account");
        }
    }

    /**
     * Checks the user's email account has already authentication-token or not
     *
     * @param context     The Application Context
     * @param accountName The User Account Name
     * @return Boolean Type. If the token of this user email's account have already stored into <code>Preference</code> object,
     * It will returns true. Otherwise returns false.
     */
    public static boolean hasEmailToken(final Context context, final String accountName) {
        SharedPreferences sp = getSharedPreferences(context);
        return !TextUtils.isEmpty(sp.getString(makeAccountSpecificPrefKey(accountName, PREFIX_PREF_AUTH_TOKEN), null));
    }

    /**
     * Checks the user's facebook account has already authentication-token or not
     *
     * @param context The Application Context
     * @param account The User Account Name
     * @return Boolean Type. If the token of this user facebook account have already stored into <code>Preference</code> object,
     * It will returns true. Otherwise returns false.
     */
    public static boolean hasFacebookToken(final Context context, final String account) {
        SharedPreferences sp = getSharedPreferences(context);
        return !TextUtils.isEmpty(sp.getString(makeAccountSpecificPrefKey(account, PREFIX_PREF_FACEBOOK_AUTH_TOKEN), null));
    }

    /**
     * Checks the user's twitter account has already authentication-token or not
     *
     * @param context The Application Context
     * @param account The User Account Name
     * @return Boolean Type. If the token of this user twitter account have already stored into <code>Preference</code> object,
     * It will returns true. Otherwise returns false.
     */
    public static boolean hasTwitterAccountToken(final Context context, final String account) {
        SharedPreferences sp = getSharedPreferences(context);
        return !TextUtils.isEmpty(sp.getString(makeAccountSpecificPrefKey(account, PREFIX_PREF_TWITTER_AUTH_TOKEN), null));
    }

    public static void setEmailAccountAddress(final Context context, final String accountName, final String address) {
        SharedPreferences sp = getSharedPreferences(context);
        sp.edit().putString(makeAccountSpecificPrefKey(accountName, PREFIX_PREF_EMAIL_NAME), address).apply();
    }

    public static String getEmailAccountAddress(final Context context) {
        SharedPreferences sp = getSharedPreferences(context);
        return hasActiveAccount(context) ? sp.getString(makeAccountSpecificPrefKey(context, PREFIX_PREF_EMAIL_NAME), null) : null;
    }

    public static void setEmailAccountImageURL(final Context context, final String accountName, final String imageURL) {
        SharedPreferences sp = getSharedPreferences(context);
        sp.edit().putString(makeAccountSpecificPrefKey(accountName, PREFIX_PREF_EMAIL_IMAGE_URL), imageURL).apply();
    }

    public static String getEmailAccountImageURL(final Context context, final String accountName) {
        SharedPreferences sp = getSharedPreferences(context);
        return hasActiveAccount(context) ? sp.getString(makeAccountSpecificPrefKey(accountName, PREFIX_PREF_EMAIL_IMAGE_URL), null) : null;
    }

    public static void setEmailCoverURL(final Context context, final String accountName, String coverPhotoURL) {
        SharedPreferences sp = getSharedPreferences(context);
        sp.edit().putString(makeAccountSpecificPrefKey(accountName, PREFIX_PREF_EMAIL_COVER_URL), coverPhotoURL).apply();
    }

    public static String getEmailCoverUrl(final Context context, String accountName) {
        SharedPreferences sp = getSharedPreferences(context);
        return hasActiveAccount(context) ? sp.getString(makeAccountSpecificPrefKey(accountName, PREFIX_PREF_EMAIL_COVER_URL), null) : null;
    }

	public static void removeActiveAccount(final Context context) {
		SharedPreferences sp = getSharedPreferences(context);
		sp.edit().remove(PREF_ACTIVE_ACCOUNT_NAME);
		sp.edit().remove(PREF_ACTIVE_ACCOUNT_TYPE);
		sp.edit().apply();
	}

	public static void removeUserIdAccount(final Context context) {
		if (getUserId(context) != 0) {
			SharedPreferences sp = getSharedPreferences(context);
			sp.edit().remove(makeAccountSpecificPrefKey(context, PREFIX_PREF_USER_ID));
			sp.edit().apply();
		}
	}

	public static void removeUserToken(final Context context) {
		final Account account = getActiveAccount(context);
        final String accountType;
        if (account != null) {
            accountType = account.mType;
            if (accountType.equalsIgnoreCase(AppConstants.EMAIL_ACCOUNT_TYPE)) {
                SharedPreferences sp = getSharedPreferences(context);
                sp.edit().remove(makeAccountSpecificPrefKey(context, PREFIX_PREF_AUTH_TOKEN));
                sp.edit().apply();
            } else if (accountType.equalsIgnoreCase(AppConstants.FACEBOOK_ACCOUNT_TYPE)) {
                SharedPreferences sp = getSharedPreferences(context);

                sp.edit().remove(makeAccountSpecificPrefKey(context, PREFIX_PREF_FACEBOOK_AUTH_TOKEN));
                sp.edit().remove(makeAccountSpecificPrefKey(context, PREFIX_PREF_FACEBOOK_AUTH_TOKEN_SECRET));
                sp.edit().apply();
            } else if (accountType.equalsIgnoreCase(AppConstants.TWITTER_ACCOUNT_TYPE)) {
                SharedPreferences sp = getSharedPreferences(context);
                sp.edit().remove(makeAccountSpecificPrefKey(context, PREFIX_PREF_TWITTER_AUTH_TOKEN));
                sp.edit().remove(makeAccountSpecificPrefKey(context, PREFIX_PREF_TWITTER_AUTH_TOKEN_SECRET));
                sp.edit().apply();
            }
        }
	}

	public static void removeUserDescription(final Context context) {
		SharedPreferences sp = getSharedPreferences(context);
		if (!TextUtils.isEmpty(getUserDescription(context))) {
			sp.edit().remove(makeAccountSpecificPrefKey(context, PREFIX_PREF_USER_DESCRIPTION));
			sp.edit().apply();
		}
	}

	public static void removeEmailAccountImageURL(final Context context) {
		SharedPreferences sp = getSharedPreferences(context);
		final Account account = getActiveAccount(context);
        final String accountName;
        if (account != null) {
            accountName = account.mName;
            if (!TextUtils.isEmpty(getEmailAccountImageURL(context, accountName))) {
                sp.edit().remove(makeAccountSpecificPrefKey(context, PREFIX_PREF_EMAIL_COVER_URL));
                sp.edit().apply();
            }
        }
	}

    public static void removeAllPrefs(final Context context) {
        SharedPreferences sp = getSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.clear();
        editor.commit();
    }
    public static boolean isSignedIn(final Context context) {
        return StringUtils.isNotEmpty(getAuthToken(context))
                || StringUtils.isNotEmpty(getActiveAccountName(context))
                || StringUtils.isNotEmpty(getEmailAccountAddress(context));
    }

    public static boolean isSignedOut(final Context context) {
        return !isSignedIn(context);
    }
}
