package co.soml.android.app.models;

import java.util.ArrayList;

import co.soml.android.app.utils.LogUtils;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since May.29.2015
 * <p>
 * The StoryFeedList class represent to handle the ArrayList of Story which will be reused into adapter or models, controllers and other
 * part.
 */
public class StoryFeedList extends ArrayList<Story> {

    public static final String TAG = LogUtils.makeLogTag(StoryFeedList.class.getSimpleName());

    @Override
    public Object clone() {
        return super.clone();
    }

    /**
     * Returns the index of specified story into this list
     *
     * @param story The specified story
     * @return Integer type.
     */
    public int indexOfStory(Story story) {
        if (story == null) {
            return -1;
        }

        for (int i = 0; i < size(); i++) {
            if (this.get(i).getId() == story.getId()) {
                return i;
            }
        }
        return -1;
    }

    public Story findStoryById(int storyId) {
        if (storyId == -1) {
            return null;
        }

        for (int index = 0; index < size(); index++) {
            if (this.get(index).getId() == storyId) {
                return this.get(index);
            }
        }
        return null;
    }
    /**
     * Checks the specified list of stories has been same with this list?
     *
     * @param feedList The specified list of story.
     * @return Boolean Type.
     */
    public boolean isSameList(StoryFeedList feedList) {
        if (feedList == null || feedList.size() != this.size()) {
            return false;
        }

        for (Story story : feedList) {
            int index = indexOfStory(story);
            if (index == -1 || !story.isSameAs(this.get(index))) {
                return false;
            }
        }
        return true;
    }

    /**
     * Returns the List of stories had same {@code StoryId} into this list
     *
     * @param storyId The specified story_id which used to check into the current list.
     * @return The List of Story.
     */
    public StoryFeedList getStoriesInList(long storyId) {
        StoryFeedList stories = new StoryFeedList();
        for (Story story : this) {
            if (story.getId() == storyId) {
                stories.add(story);
            }
        }
        return stories;
    }
}


