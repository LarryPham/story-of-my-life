/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/27/15 3:00 PM.
 **/

package co.soml.android.app.activities;

import com.squareup.picasso.Picasso;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import butterknife.Bind;
import butterknife.ButterKnife;
import co.soml.android.app.R;
import co.soml.android.app.StoryApp;
import co.soml.android.app.controllers.UserProfileController;
import co.soml.android.app.models.Account;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.models.User;
import co.soml.android.app.utils.AccountUtils;
import co.soml.android.app.utils.EditTextUtils;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.widgets.CircleImageView;
import co.soml.android.app.widgets.StoryTextView;

public class EditUserProfileActivity extends BaseActivity {
    public static final String TAG = LogUtils.makeLogTag(EditUserProfileActivity.class.getSimpleName());

    public StoryApp mApp;
    public StoryDataModel mDataModel;
    public UserProfileController mController;

    private Intent mEditIntent= null;
    private User mCurrentUser = null;
    private Bundle mProfileParams;
    protected Context mContext;

    @Bind(R.id.submit_profile_button)
    protected StoryTextView mSubmitProfileButton;
    @Bind(R.id.account_profile_image)
    protected CircleImageView mAvatarImageButton;

    @Bind(R.id.edit_user_full_name)
    protected EditText mEditProfileFullName;
    @Bind(R.id.edit_user_description)
    protected EditText mEditUserDescription;
    @Bind(R.id.user_email_address)
    protected StoryTextView mUserEmailAddress;

    @Bind(R.id.edit_profile_toolbar)
    protected Toolbar mEditProfileToolbar;

    protected int mUSerId;
    protected String mUserFullNameStr;
    protected String mUserDescriptionStr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);

        mEditIntent = getIntent();
        final Bundle params = mEditIntent.getExtras();
        mCurrentUser = (User) params.get("CurrentUser");
        mUSerId = mCurrentUser != null  ? mCurrentUser.getId() : 0;

        mContext = this.getApplicationContext();
        mApp = (StoryApp) getApplicationContext();
        mDataModel = mApp.getAppDataModel();
        mController = new UserProfileController(this, mDataModel);
        onBindingUI();
    }

    protected void onBindingUI() {
        final String emailAddress = AccountUtils.getEmailAccountAddress(EditUserProfileActivity.this);
        this.mUserEmailAddress.setText(emailAddress);
        this.mEditProfileFullName.setText(mCurrentUser != null ? mCurrentUser.getFullName() : null);
        this.mEditUserDescription.setText(mCurrentUser != null ? mCurrentUser.getDescription() : null);
        Picasso.with(this).load(TextUtils.isEmpty(mCurrentUser.getAvatarOriginalUrl()) ? null
                : mCurrentUser.getAvatarOriginalUrl()).placeholder(R.drawable.person_image_empty).into(mAvatarImageButton);
    }

    protected void getEditTextContent() {
        mUserFullNameStr = EditTextUtils.getText(this.mEditProfileFullName);
        mUserDescriptionStr = EditTextUtils.getText(this.mEditUserDescription);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSubmitProfileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mController != null) {

                }
            }
        });
    }

    public void onPostCreate() {
        if (mEditProfileToolbar != null) {
            setSupportActionBar(mEditProfileToolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_close);
            }
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        onPostCreate();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    @Override
    protected void invalidate() {

    }

    @Override
    protected void invalidate(Object... params) {

    }
}
