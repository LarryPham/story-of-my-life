/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/6/15 5:24 AM.
 **/

package co.soml.android.app.camera.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.effect.Effect;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import co.soml.android.app.AppConstants;
import co.soml.android.app.R;
import co.soml.android.app.StoryApp;
import co.soml.android.app.camera.CameraManager;
import co.soml.android.app.camera.EffectManager;
import co.soml.android.app.camera.ImageFilterAdapter;
import co.soml.android.app.camera.effect.FilterEffect;
import co.soml.android.app.camera.utils.CameraHelper;
import co.soml.android.app.camera.utils.EffectUtil;
import co.soml.android.app.camera.utils.GPUImageFilterTools;
import co.soml.android.app.models.OverlayTagItem;
import co.soml.android.app.models.Sticker;
import co.soml.android.app.utils.CommonUtil;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.utils.StringUtils;
import co.soml.android.app.widgets.ImageViewDrawableOverlay;
import co.soml.android.app.widgets.LabelView;
import co.soml.android.app.widgets.StoryTextView;
import it.sephiroth.android.library.widget.AdapterView;
import it.sephiroth.android.library.widget.HListView;
import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageView;

public class PhotoProcessActivity extends FragmentActivity {
    public static final String TAG = LogUtils.makeLogTag(PhotoProcessActivity.class.getSimpleName());
    private GPUImageView mGPUImageView;
    private ViewGroup mDrawArea;

    private StoryTextView mStickerButton;
    private StoryTextView mFilterButton;
    private StoryTextView mLabelButton;
    private StoryTextView mCurrentButton;

    private HListView mBottomToolBar;
    private ViewGroup mToolArea;

    private ImageViewDrawableOverlay mImageViewOverlay;
    private Bitmap mSmallImageBackground;
    private LabelView mEmptyLabelView;
    private StoryTextView mEmptyTextView;

    private List<LabelView> mLabels = new ArrayList<>();

    private View mCommonLabelArea;
    private CameraHelper mCameraHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_process);
        EffectUtil.clear();

        mGPUImageView = (GPUImageView) findViewById(R.id.gpuimage);
        mDrawArea = (ViewGroup) findViewById(R.id.drawing_view_container);
        mStickerButton = (StoryTextView) findViewById(R.id.sticker_button);
        mFilterButton = (StoryTextView) findViewById(R.id.filter_button);
        mLabelButton = (StoryTextView) findViewById(R.id.text_button);
        mBottomToolBar = (HListView) findViewById(R.id.list_tools);
        mToolArea = (ViewGroup) findViewById(R.id.toolbar_area);

        initView();
        initEvent();
        initStickerBar();

        mCameraHelper = new CameraHelper(this);
        CameraManager.getInstance().addActivity(this);
    }

    protected void initView() {
        View overlay = LayoutInflater.from(PhotoProcessActivity.this).inflate(R.layout.view_drawable_overlay, null);
        mImageViewOverlay = (ImageViewDrawableOverlay) overlay.findViewById(R.id.drawable_overlay);
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(CommonUtil.getScreenWidth(StoryApp.getContext()), CommonUtil
                .getScreenHeight(StoryApp.getContext()));
        mImageViewOverlay.setLayoutParams(params);
        overlay.setLayoutParams(params);
        mDrawArea.addView(overlay);

        RelativeLayout.LayoutParams relativeLayoutParams = new RelativeLayout.LayoutParams(CommonUtil.getScreenHeight(StoryApp.getContext
                ()), CommonUtil.getScreenHeight(StoryApp.getContext()));

        mGPUImageView.setLayoutParams(relativeLayoutParams);
        mEmptyLabelView = new LabelView(this);
        EffectUtil.addLabelEditable(mImageViewOverlay, mDrawArea, mEmptyTextView, mImageViewOverlay.getWidth() / 2, mImageViewOverlay
                .getWidth()/2);
        mEmptyTextView.setVisibility(View.INVISIBLE);
    }

    protected void initEvent() {
        mStickerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!setCurrentButton(mStickerButton)) {
                    return;
                }
            }
        });
    }

    private boolean setCurrentButton(StoryTextView button) {
        if (mCurrentButton == null) {
            mCurrentButton = button;
        } else if (mCurrentButton.equals(button)) {
            return false;
        } else {
            mCurrentButton.setTextColor(Color.rgb(208, 190, 185));
            mCurrentButton.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        }
        //Drawable mImageDrawable = getResources().getDrawable(R.drawable.select_icon);
        button.setTextColor(Color.rgb(255,255,255));
        // Setting the compound drawable for current button
        mCurrentButton = button;
        return true;
    }

    private void initStickerBar() {

    }

    protected void initStickerToolBar() {
        mBottomToolBar.setAdapter(new StickerToolAdapter(PhotoProcessActivity.this, EffectUtil.mStickerList));
        mBottomToolBar.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                Sticker sticker = EffectUtil.mStickerList.get(arg2);
                EffectUtil.addStickerImage(mImageViewOverlay, PhotoProcessActivity.this, sticker, new EffectUtil.StickerCallback() {
                    @Override
                    public void onRemoveSticker(Sticker sticker) {

                    }
                });
            }
        });
        setCurrentButton(mStickerButton);
    }

    protected void initFilterToolBar() {
        final List<FilterEffect> filters = EffectManager.getInstance().getLocalFilters();
        final ImageFilterAdapter adapter = new ImageFilterAdapter(PhotoProcessActivity.this, filters, mSmallImageBackground);
        mBottomToolBar.setAdapter(adapter);
        mBottomToolBar.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                if (adapter.getSelectFilter() != arg2) {
                    adapter.setSelectFilter(arg2);
                    GPUImageFilter filter = GPUImageFilterTools.createFilterForType(PhotoProcessActivity.this, filters.get(arg2).getType());
                    mGPUImageView.setFilter(filter);
                    GPUImageFilterTools.FilterAdjuster mFilterAdjuster = new GPUImageFilterTools.FilterAdjuster(filter);

                    if (mFilterAdjuster.canAdjust()) {
                        mFilterAdjuster.adjust(100);
                    }
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (AppConstants.ACTION_EDIT_LABEL == requestCode && data != null) {
            String text = data.getStringExtra(AppConstants.PARAM_EDIT_TEXT);
            if (StringUtils.isNotEmpty(text)) {
                OverlayTagItem tagItem = new OverlayTagItem(AppConstants.POST_TYPE_TAG, text);
                // Adding label
            }
        } else if (AppConstants.ACTION_EDIT_LABEL_POI == requestCode && data != null) {
            String text = data.getStringExtra(AppConstants.PARAM_EDIT_TEXT);
            if (StringUtils.isNotEmpty(text)) {
                OverlayTagItem tagItem = new OverlayTagItem(AppConstants.POST_TYPE_POI, text);
                // Adding Label
            }
        }
    }
}
