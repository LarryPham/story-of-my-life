/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 * @author: Larry Pham (email: larrypham.vn@gmail.com)
 * Date: 6/2/15 4:16 PM
 **/

package co.soml.android.app.controllers;

import android.os.Message;

import co.soml.android.app.StoryApp;
import co.soml.android.app.activities.PublishStoryActivity;
import co.soml.android.app.activities.BaseActivity;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.utils.LogUtils;

public class PublishStoryController extends BaseController {
    public static final String TAG = LogUtils.makeLogTag(PublishStoryController.class.getSimpleName());
    private StoryApp mStoryApp;
    private StoryDataModel mStoryDataModel;
    private PublishStoryActivity mActivity;

    public PublishStoryController(BaseActivity activity, StoryDataModel dataModel) {
        super(activity, dataModel);
        mActivity = (PublishStoryActivity) activity;
        mStoryApp = (StoryApp) activity.getApplicationContext();
        mStoryDataModel = mStoryApp.getAppDataModel();
    }

    @Override
    protected void handleMessage(Message msg) {
        switch (msg.what) {
	        case ControllerMessage.ACTION_WAITING_START: {
		        break;
	        }
	        case ControllerMessage.ACTION_WAITING_END: {
		        break;
	        }
        }
    }
}
