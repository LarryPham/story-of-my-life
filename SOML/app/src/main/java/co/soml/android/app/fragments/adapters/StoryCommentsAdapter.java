/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 * @author: Larry Pham (email: larrypham.vn@gmail.com)
 * Date: 6/10/15 2:47 AM
 **/

package co.soml.android.app.fragments.adapters;

import android.content.Context;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import co.soml.android.app.R;
import co.soml.android.app.StoryApp;
import co.soml.android.app.activities.ReaderStoryInterfaces;
import co.soml.android.app.controllers.StoryReaderActions;
import co.soml.android.app.models.Comment;
import co.soml.android.app.models.CommentList;
import co.soml.android.app.models.Story;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.models.User;
import co.soml.android.app.utils.CommonUtil;
import co.soml.android.app.utils.DateTimeUtils;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.widgets.CircleImageView;
import co.soml.android.app.widgets.StoryIconCountView;
import co.soml.android.app.widgets.StoryRecyclerView;
import co.soml.android.app.widgets.StoryTextView;

/**
 * The comments adapter which represent to adapt the data from the list of comments result to the UI component. RecyclerView of
 * JsonComment object.
 */
public class StoryCommentsAdapter extends StoryRecyclerView.Adapter<StoryCommentsAdapter.CommentHolder> {
    public static final String TAG = LogUtils.makeLogTag(StoryCommentsAdapter.class.getSimpleName());

    public static final int COMMENTS_VIEW_TYPE_DEFAULT = 0;
    public static final int COMMENTS_VIEW_TYPE_LOADING = 1;

    private Context mContext;

    private Story mStory;
    private StoryApp mStoryApp;
    private StoryDataModel mDataModel;

    private boolean mMoreCommentsExist;

    private static final int MAX_INDENT_LEVEL = 2;

    private int mIndentPerLevel;
    private int mUserProfileSize;

    private long mHighlightCommentId = 0;
    private boolean mShowProgressForHighlightedComment = false;


    private boolean mIsPrivateStory;

    private int mLinkColor;
    private int mNoLinkColor;

    private String mLike;
    private String mLikeBy;
    private String mLikesSingle;
    private String mLikedByYou;
    private String mLikesMulti;

    private boolean mAnimationLocked = false;
    private boolean mDelayEnterAnimation = true;

    public interface OnLoadMoreListener {
        void onLoadMore();
    }

    public interface RequestReplyListener {
        void onRequestReply(long commentId);
    }

    private CommentList mComments = new CommentList();
    private final HashSet<Integer> mSelectedPositions = new HashSet<Integer>();
    private final List<Long> mModeratingCommentsIds = new ArrayList<Long>();
    private final int mSelectionColor;
    private boolean mEnableSelection;

    private RequestReplyListener mReplyListener;
    private ReaderStoryInterfaces.DataLoadedListener mDataLoadedListener;
    private StoryReaderActions.DataRequestedListener mDataRequestedListener;


    class CommentHolder extends RecyclerView.ViewHolder {
        private ViewGroup mContainer;
        private StoryTextView mAuthorTextView;
        private StoryTextView mContentTextView;
        private StoryTextView mDateTextView;
        private View mSpacerIndent;

        private CircleImageView mProfileImage;
        private StoryTextView mReplyTextView;

        private ViewGroup mActionsLayout;
        private StoryIconCountView mLikeView;
        private StoryIconCountView mReplyView;
        private ProgressBar mCommentProgressBar;

        public CommentHolder(View view) {
            super(view);
            mContainer = (ViewGroup) view.findViewById(R.id.layout_comment_container);

            mAuthorTextView = (StoryTextView) view.findViewById(R.id.comment_author);
            mContentTextView = (StoryTextView) view.findViewById(R.id.comment_content);
            mDateTextView = (StoryTextView) view.findViewById(R.id.comment_status_date);
            mProfileImage = (CircleImageView) view.findViewById(R.id.user_comment_profile);
            mCommentProgressBar = (ProgressBar) view.findViewById(R.id.progress_comment);
            mSpacerIndent = (View) view.findViewById(R.id.spacer_comment_indent);

            mActionsLayout = (ViewGroup) view.findViewById(R.id.layout_comment_actions);
            mLikeView = (StoryIconCountView) view.findViewById(R.id.comment_action_like);
            mReplyView = (StoryIconCountView) view.findViewById(R.id.comment_action_reply);

            mContentTextView.setLinksClickable(true);
        }
    }

    public StoryCommentsAdapter(Context context, Story story) {
        this.mContext = context;
        this.mStory = story;

        this.mStoryApp = (StoryApp) context.getApplicationContext();
        this.mDataModel = mStoryApp.getAppDataModel();

        mLike = mContext.getResources().getString(R.string.comment_like_content);
        mLikeBy = mContext.getResources().getString(R.string.comment_like_by_content);
        mLikesSingle = mContext.getResources().getString(R.string.comment_like_by_one_shot);
        mLikedByYou = mContext.getResources().getString(R.string.comment_like_by_you);
        mLikesMulti = mContext.getResources().getString(R.string.comment_like_multi_shot);
        mSelectionColor =  mContext.getResources().getColor(R.color.secondary_text_default_material_dark);
        setHasStableIds(true);
    }

    @Override
    public CommentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_comments_item, null);
        CommentHolder holder = new CommentHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(CommentHolder holder, int position) {
        final Comment comment = mComments.get(position);
        final User author = new User(mStory.getUser());
        holder.mAuthorTextView.setText(author.getFullName());

        Picasso.with(mContext).load(author.getAvatarMediumUrl())
                              .placeholder(R.drawable.person_image_empty).into(holder.mProfileImage);
        holder.mDateTextView.setText(DateTimeUtils.dateToTimeSpan(mContext, comment.getCreatedAt()));
        holder.mContentTextView.setText(comment.getBody());

        if (comment.getStoryId() != 0) {
            View.OnClickListener authorListener = new View.OnClickListener() {
                @Override
                public void onClick(View authorView) {
                    // Showing Profile
                }
            };
            holder.mProfileImage.setOnClickListener(authorListener);
            holder.mAuthorTextView.setOnClickListener(authorListener);
            holder.mAuthorTextView.setTextColor(mLinkColor);
        }

        // show indentation space for comments with parents and indent it based on comment level
        if (!TextUtils.isEmpty(comment.getAncestry()) && comment.level > 0) {
            int indent = Math.min(MAX_INDENT_LEVEL, comment.level) * mIndentPerLevel;
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.mSpacerIndent.getLayoutParams();
            params.width = indent;
            holder.mSpacerIndent.setVisibility(View.VISIBLE);
        } else {
            holder.mSpacerIndent.setVisibility(View.GONE);
        }

        if (mHighlightCommentId != 0 && mHighlightCommentId == comment.getId()) {
            holder.mContainer.setSelected(true);
            holder.mCommentProgressBar.setVisibility(mShowProgressForHighlightedComment ? View.VISIBLE : View.GONE);
        } else {
            holder.mContainer.setSelected(comment.getStoryId() == mStory.getId());
            holder.mContainer.setVisibility(View.GONE);
        }

        if (mReplyListener != null) {
            View.OnClickListener replyClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View replyView) {
                    mReplyListener.onRequestReply(comment.getId());
                }
            };
            holder.mReplyView.setOnClickListener(replyClickListener);
        }

        showLikeStatus(holder, position);

        if (mMoreCommentsExist && mDataRequestedListener != null && (position >= getItemCount() - 1)) {
            mDataRequestedListener.onRequestedData();
        }
    }

    @Override
    public int getItemCount() {
        return (mComments != null ? mComments.size() : 0);
    }

    public boolean isEmpty() {
        return (getItemCount() == 0);
    }

    public void clear() {
        if (mComments.size() > 0) {
            mComments.clear();
            notifyDataSetChanged();
        }
    }


    public int getSelectedCommentCount() {
        return mSelectedPositions.size();
    }

    public CommentList getSelectedComments() {
        CommentList comments = new CommentList();
        if (!mEnableSelection) {
            return mComments;
        }
        for (Integer position: mSelectedPositions) {
            if (isValidPosition(position)) {
                comments.add(mComments.get(position));
            }
        }
        return mComments;
    }

    @Override
    public long getItemId(int position) {
        if (isValidPosition(position)) {
            return mComments.get(position).getId();
        } else {
            return 0;
        }
    }

    private boolean isValidPosition(int position) {
        return (position >= 0 && position < mComments.size());
    }

    public void addComment(Comment comment) {
        if (comment == null) {
            return;
        }

        if (TextUtils.isEmpty(comment.getAncestry())) {
            mComments.add(comment);
            notifyDataSetChanged();
        } else {
            refreshComments();
        }
    }

    public void refreshComments() {
        if (mIsTaskRunning) {
            LogUtils.LOGD(TAG, "CommentAdapter > Load comments task already running");
        }
        new LoadingCommentsTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    /**
     * Loading comments using AsyncTask
     */
    public void loadComments() {
        if (mIsTaskRunning) {
            LogUtils.LOGD(TAG, "Loading comments task already running");
        }
        new LoadingCommentsTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void showLikeStatus(final CommentHolder holder, final int position) {
        if (!isValidPosition(position)) {
            return;
        }

        final Comment comment = mComments.get(position);
        holder.mLikeView.setVisibility(View.VISIBLE);
        holder.mLikeView.getImageView().setSelected(mStory.isLikeByCurrentUser());

    }

    /**
     * Called from story detail when submitted a comment fails - this removes the "fake" comment that was inserted while the API call was still
     * being processed.
     *
     * @param commentId The comment's id
     */
    public void removeComment(long commentId) {
        if (commentId == mHighlightCommentId) {
            setHighLightCommentId(0, false);
        }

        int position = indexOfCommentId(commentId);
        if (position == -1) {
            return;
        }

        mComments.remove(position);
        notifyDataSetChanged();
    }

    /**
     * Replace the comment that has the passed commentId with another comment - used after a comment is submitted to replace
     * the "fake" comment with real one
     */
    public void replaceComment(long commentId, Comment comment) {
        int position = mComments.replaceComment(commentId, comment);
        if (position > -1) {
            notifyItemChanged(position);
        }
    }

    /**
     * Sets the passed comment as highlighted with a different background color and an optional progress bar (used with posting new comments)
     * Note that: we don't call notifyDataSetChanged() here since in most cases it's unecessary, so we leave it up to the caller to do that
     *
     * @param commentId The comment's id
     * @param showProgress Showing progressions
     */
    public void setHighLightCommentId(long commentId, boolean showProgress) {
        mHighlightCommentId = commentId;
        mShowProgressForHighlightedComment = showProgress;
    }

    public int indexOfCommentId(long commentId) {
        return mComments.indexOfCommentId(commentId);
    }

    /**
     * AsyncTaks to load comments from SQLite or Server
     */
    private boolean mIsTaskRunning = false;


    private class LoadingCommentsTask extends AsyncTask<Void, Void, Boolean> {

        private CommentList mTempComments;
        private boolean mTempMoreCommentsExist;

        @Override
        protected void onPreExecute() {
            mIsTaskRunning = true;
        }

        @Override
        protected void onCancelled() {
            mIsTaskRunning = false;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            if (mStory == null) {
                return false;
            }

            // Getting the list of comments from global access object (StoryApp)
            mTempComments = mDataModel.getComments();
            // Next case will be loaded from sqlite database

            if (mTempComments != null && mComments != null) {
                mTempMoreCommentsExist = mTempComments.size() > mComments.size();
                return !mComments.isSameList(mTempComments);
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            mMoreCommentsExist = mTempMoreCommentsExist;

            if (result) {
                mComments = (CommentList) (mTempComments.clone());
                notifyDataSetChanged();
            }

            if (mDataLoadedListener != null) {
                mDataLoadedListener.onDataLoaded(isEmpty());
            }

            mIsTaskRunning = false;
        }
    }

    private void toggleLike(Context context, CommentHolder holder, int position) {
        if (!CommonUtil.isNetworkOnline(context)) {
            return;
        }

        if (!isValidPosition(position)) {
            final View snakeView = LayoutInflater.from(context).inflate(R.layout.layout_snackbar, null);
            Snackbar.make(snakeView, context.getResources().getText(R.string.unable_perform_this_action), Snackbar.LENGTH_LONG).show();
            return;
        }

        Comment comment = mComments.get(position);
    }


    public RequestReplyListener getReplyListener() {
        return mReplyListener;
    }

    public void setReplyListener(RequestReplyListener replyListener) {
        mReplyListener = replyListener;
    }

    public ReaderStoryInterfaces.DataLoadedListener getDataLoadedListener() {
        return mDataLoadedListener;
    }

    public void setDataLoadedListener(ReaderStoryInterfaces.DataLoadedListener dataLoadedListener) {
        mDataLoadedListener = dataLoadedListener;
    }

    public StoryReaderActions.DataRequestedListener getDataRequestedListener() {
        return mDataRequestedListener;
    }

    public void setDataRequestedListener(StoryReaderActions.DataRequestedListener dataRequestedListener) {
        mDataRequestedListener = dataRequestedListener;
    }
}
