package co.soml.android.app.controllers;

import android.content.Intent;
import android.os.Message;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;

import co.soml.android.app.AppConstants;
import co.soml.android.app.R;
import co.soml.android.app.StoryApp;
import co.soml.android.app.activities.BaseActivity;
import co.soml.android.app.activities.InvalidateParam;
import co.soml.android.app.activities.StoryMainActivity;
import co.soml.android.app.fragments.BaseFragment;
import co.soml.android.app.fragments.CollectionsFragment;
import co.soml.android.app.fragments.MyStoryFragment;
import co.soml.android.app.fragments.PeopleFragment;
import co.soml.android.app.fragments.SearchFragment;
import co.soml.android.app.models.Story;
import co.soml.android.app.models.StoryBean;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.models.User;
import co.soml.android.app.services.StoryService;
import co.soml.android.app.services.UserResult;
import co.soml.android.app.services.tasks.Action;
import co.soml.android.app.utils.AccountUtils;
import co.soml.android.app.utils.CommonUtil;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.widgets.ProgressWheel;

public class MainStoryController extends BaseController {
	public static final String TAG = LogUtils.makeLogTag(MainStoryController.class.getSimpleName());

	private StoryApp mApp;
	private StoryDataModel mDataModel;
	private StoryMainActivity mActivity;

	private BaseFragment mFragment;

	private CollectionsFragment mHomeFragment;
	private MyStoryFragment mMyStoryFragment;
	private SearchFragment mSearchFragment;
	private PeopleFragment mSocialFragment;

	private boolean mScrollState = false;
	private ProgressWheel mProgressWheel = null;

	private static MainStoryController sInstance = null;

	private Snackbar mCheckNetworkSnackBar;
	private StoryBean mBean = StoryMainActivity.getBean();

	public static MainStoryController getInstance(BaseActivity activity, BaseFragment fragment, StoryDataModel dataModel) {
		if (sInstance != null) {
			sInstance.setActivity((StoryMainActivity) activity);
			sInstance.setFragment(fragment);
			sInstance.setDataModel(dataModel);
			return sInstance;
		} else {
			sInstance = new MainStoryController(activity, fragment, dataModel);
			return sInstance;
		}
	}

	public MainStoryController(BaseActivity activity, StoryDataModel dataModel) {
		super(activity, dataModel);
		this.mActivity = (StoryMainActivity) activity;
		this.mApp = (StoryApp) activity.getApplicationContext();
		this.mDataModel = mApp.getAppDataModel();
	}

	public MainStoryController(BaseActivity activity, StoryDataModel dataModel, boolean isFirstRunningMode) {
		super(activity, dataModel, isFirstRunningMode);
		this.mActivity = (StoryMainActivity) activity;
		this.mApp = (StoryApp) activity.getApplicationContext();
		this.mDataModel = mApp.getAppDataModel();
	}

	public MainStoryController(BaseActivity activity, BaseFragment fragment, StoryDataModel dataModel) {
		super(activity, fragment, dataModel);

		this.mActivity = (StoryMainActivity) activity;
		this.mFragment = fragment;
		this.mDataModel = dataModel;
	}

	public MainStoryController(BaseActivity activity, CollectionsFragment homeFragment, StoryDataModel dataModel) {
		super(activity, homeFragment, dataModel);
		this.mActivity = (StoryMainActivity) activity;
		this.mDataModel = dataModel;
		this.mHomeFragment = homeFragment;
		this.mApp = (StoryApp) activity.getApplicationContext();
	}

	public MainStoryController(BaseActivity activity, MyStoryFragment myStoryFragment, StoryDataModel dataModel) {
		super(activity, myStoryFragment, dataModel);

		this.mActivity = (StoryMainActivity) activity;
		this.mDataModel = dataModel;
		this.mMyStoryFragment = myStoryFragment;
		this.mApp = (StoryApp) activity.getApplicationContext();
	}

	public MainStoryController(BaseActivity activity, SearchFragment searchFragment, StoryDataModel dataModel) {
		super (activity, searchFragment, dataModel);
		this.mActivity = (StoryMainActivity) activity;
		this.mDataModel = dataModel;
		this.mSearchFragment = searchFragment;
		this.mApp = (StoryApp) activity.getApplicationContext();
	}

	public MainStoryController(BaseActivity activity, PeopleFragment socialFragment, StoryDataModel dataModel) {
		super (activity, socialFragment, dataModel);
		this.mActivity = (StoryMainActivity) activity;
		this.mDataModel = dataModel;
		this.mSocialFragment = socialFragment;
		this.mApp = (StoryApp) activity.getApplicationContext();
	}

	public StoryMainActivity getActivity() {
		return mActivity;
	}

	public void setActivity(StoryMainActivity activity) {
		mActivity = activity;
	}

	public BaseFragment getFragment() {
		return mFragment;
	}

	public void setFragment(BaseFragment fragment) {
		mFragment = fragment;
	}

	public StoryDataModel getDataModel() {
		return mDataModel;
	}

	public void setDataModel(StoryDataModel dataModel) {
		mDataModel = dataModel;
	}

	@Override
	protected void handleMessage(Message msg) {
		switch (msg.what) {

			case ControllerMessage.CANCEL_WAITING_DIALOG: {
				LogUtils.LOGI(TAG, "[SIGNAL] MainStoryController handleMessage[CANCEL_WAITING_DIALOG]");
				Intent intent = this.obtainIntent(ControllerMessage.CANCEL_WAITING_DIALOG);
				intent.setAction(Action.REQUEST_TO_SERVER_CANCEL);
				mActivity.startService(intent);
				mProgressWheel = null;
				break;
			}

			case ControllerMessage.ACTION_WAITING_END: {
				LogUtils.LOGI(TAG, "[SIGNAL]MainStoryController handle message[ACTION_WAITING_END]");
				Intent intent = this.obtainIntent(ControllerMessage.ACTION_WAITING_END);
				intent.setAction(Action.REQUEST_TO_SERVER_CANCEL);
				mActivity.startService(intent);
				mProgressWheel = null;
				break;
			}

			case ControllerMessage.RESET_REQUEST_COUNT: {
				this.getActionCounter().reset();
				this.mDataModel.setWaitForRequesting(false);
				break;
			}

			case ControllerMessage.PAUSE_BACKGROUND_TASK: {
				if (!mScrollState) {
					mScrollState = true;
					Intent intent = new Intent(mActivity, StoryService.class);
					intent.setAction(Action.REQUEST_IMAGE_FROM_SERVER_CANCEL);
					mActivity.startService(intent);
				}
				break;
			}

			case ControllerMessage.RESUME_BACKGROUND_TASK: {
				mScrollState = false;
				if (msg.obj == null) {
					mFragment.invalidate();
				}

				if ((Boolean) msg.obj) {
					mFragment.invalidate();
				}

				break;
			}

			case ControllerMessage.REQUEST_TO_SERVER_CANCEL: {
				Intent intent = this.obtainIntent(msg.what);
				intent.setAction(Action.REQUEST_TO_SERVER_CANCEL);
				mActivity.startService(intent);
				getActionCounter().reset();
				this.mDataModel.setWaitForRequesting(false);
				break;
			}
			// Request to get the latest list of stories from server
			case ControllerMessage.REQUEST_TO_SERVER_STORIES_FEATURED: {
				int connectMode = CommonUtil.checkConnectivityMode(mActivity);
				if (connectMode == AppConstants.CONN_3G && !CommonUtil.checkEnableMobileNetwork(mActivity)) {
					showCheckNetworkSnackBar();
				} else if (connectMode == AppConstants.CONN_DISCONNECTED) {
					if (mActivity != null) {
						((StoryMainActivity) mFragment.getActivity()).popupNoNetworkSnackbar();
					}
				} else {
					incrementActionCount();
					//onRequestListOfStoriesFromServer();
				}
				break;
			}
			// Request to get the list of story's feed into home screen
			case ControllerMessage.REQUEST_TO_SERVER_LIST_STORIES: {
				int connectMode = CommonUtil.checkConnectivityMode(mActivity);
				if (connectMode == AppConstants.CONN_3G && !CommonUtil.checkEnableMobileNetwork(mActivity)) {
					showCheckNetworkSnackBar();
				} else if (connectMode == AppConstants.CONN_DISCONNECTED) {
					if (mActivity != null) {
						((StoryMainActivity) mFragment.getActivity()).popupNoNetworkSnackbar();
					}
				} else {
					incrementActionCount();
					//onRequestListOfStoriesFromServer();
				}
				break;
			}
			// Request to get the list of following users which has been interacted with you
			case ControllerMessage.REQUEST_TO_SERVER_USER_FOLLOWING: {
				int connectMode = CommonUtil.checkConnectivityMode(mActivity);
				if (connectMode == AppConstants.CONN_DISCONNECTED) {
					((StoryMainActivity) mFragment.getActivity()).popupNoNetworkSnackbar();
				} else  {
					incrementActionCount();
					//onRequestUserFollowing();
				}
				break;
			}
			case ControllerMessage.REQUEST_TO_SERVER_USER_FOLLOWERS: {
				int connectMode = CommonUtil.checkConnectivityMode(mActivity);
				if (connectMode == AppConstants.CONN_DISCONNECTED) {
					((StoryMainActivity) mFragment.getActivity()).popupNoNetworkSnackbar();
				} else {
					incrementActionCount();
					// onRequestToGetFollowers();
				}
				break;
			}
			case ControllerMessage.REQUEST_TO_SERVER_USER_FOLLOW: {
                break;
			}
            case ControllerMessage.REQUEST_TO_SERVER_USER_DETAIL: {
                int connectMode = CommonUtil.checkConnectivityMode(mActivity);
                if (connectMode == AppConstants.CONN_DISCONNECTED) {
                    ((StoryMainActivity)mActivity).popupNoNetworkSnackbar();
                } else {
                    incrementActionCount();
                    final Intent intent = obtainIntent(ControllerMessage.REQUEST_TO_SERVER_USER_DETAIL);
                    intent.putExtra(Action.USER_ID, (Integer) msg.obj);
                    intent.setAction(Action.REQUEST_TO_SERVER_USER_DETAIL);
                    mActivity.startService(intent);
                }
                break;
            }
            case ControllerMessage.REQUEST_TO_SERVER_USER_COMPLETED: {
                if (checkOwner(msg)) {
                    final UserResult userResult = (UserResult) msg.obj;
                    if (userResult != null && userResult.getUser() != null) {
                        StoryApp.getInstance().setCurrentUser(userResult.getUser());
                        final InvalidateParam param = new InvalidateParam(msg.what, userResult.getUser());
                        mActivity.invalidate(param);
                    }
                }
                break;
            }
		}
	}

	public void decrementActionCount() {
		this.getActionCounter().decrementActionCount();
		if (this.getActionCounter().getCount() <= 0) {
			mDataModel.setWaitForRequesting(false);
		}
	}

	public void incrementActionCount() {
		this.getActionCounter().incrementActionCount();
		if (this.getActionCounter().getCount() > 0) {
			mDataModel.setWaitForRequesting(true);
		}
	}

	public void showCheckNetworkSnackBar() {
		String strCheckNetworkToast = mActivity.getResources().getString(R.string.msg_error_http_connect_failed);
		if (mCheckNetworkSnackBar == null) {
			View snackBarView = LayoutInflater.from(mActivity).inflate(R.layout.layout_snackbar, null);
			mCheckNetworkSnackBar = Snackbar.make(snackBarView, strCheckNetworkToast, Snackbar.LENGTH_SHORT);
		} else {
			mCheckNetworkSnackBar.setText(strCheckNetworkToast);
		}
		mCheckNetworkSnackBar.show();
	}
}
