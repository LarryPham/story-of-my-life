package co.soml.android.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its
 * documentation are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable
 * form without the prior written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with
 * respect to the contents, and assumes no responsibility for any errors that might appear in the software and
 * documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.03.2015
 */
public class JsonStory implements Serializable {
    public static final long serialVersionUID = 2472424234L;
    @Expose
    @SerializedName("id")
    public int mId;

    @Expose
    @SerializedName("user_id")
    public int mUserId;

    @Expose
    @SerializedName("name")
    public String mName;

    @Expose
    @SerializedName("interval")
    public int mInterval;

    @Expose
    @SerializedName("description")
    public String mDescription;

    @Expose
    @SerializedName("latitude")
    public String mLatitude;

    @Expose
    @SerializedName("longitude")
    public String mLongitude;

    @Expose
    @SerializedName("state")
    public String mState;

    @Expose
    @SerializedName("created_at")
    public Date mCreatedAt;

    @Expose
    @SerializedName("updated_at")
    public Date mUpdatedAt;

    @Expose
    @SerializedName("deleted_at")
    public Date mDeletedAt;

    @Expose
    @SerializedName("location")
    public String mLocation;

    @Expose
    @SerializedName("photo_order")
    public int[] mPhotoOrder;

    @Expose
    @SerializedName("cover_id")
    public int mCoverId;

    @Expose
    @SerializedName("view_count")
    public int mViewCount;

    @Expose
    @SerializedName("slug")
    public String mSlug;

    @Expose
    @SerializedName("playback_interval")
    public int mPlaybackInterval;

    @Expose
    @SerializedName("song_id")
    public int mSongId;

    @Expose
    @SerializedName("likes_count")
    public int mLikesCount;

    @Expose
    @SerializedName("comments_count")
    public int mCommentsCount;

    @Expose
    @SerializedName("photos")
    public List<JsonPhoto> mPhoto;

    @Expose
    @SerializedName("user")
    public User mUser;

    @Expose
    @SerializedName("share_link")
    public String mShareLink;

    @Expose
    @SerializedName("is_liked_by_current_user")
    public boolean mIsLikeByCurrentUser;

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public int getInterval() {
        return mInterval;
    }

    public void setInterval(int interval) {
        mInterval = interval;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getLatitude() {
        return mLatitude;
    }

    public void setLatitude(String latitude) {
        mLatitude = latitude;
    }

    public String getLongitude() {
        return mLongitude;
    }

    public void setLongitude(String longitude) {
        mLongitude = longitude;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

    public Date getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(Date createdAt) {
        mCreatedAt = createdAt;
    }

    public void setCreatedAt(String createdAt) {

    }

    public Date getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        mUpdatedAt = updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {

    }

    public int getUserId() {
        return mUserId;
    }

    public void setUserId(int userId) {
        mUserId = userId;
    }

    public Date getDeletedAt() {
        return mDeletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        mDeletedAt = deletedAt;
    }

    public void setDeletedAt(String deletedAt) {

    }

    public String getLocation() {
        return mLocation;
    }

    public void setLocation(String location) {
        mLocation = location;
    }

    public int[] getPhotoOrder() {
        return mPhotoOrder;
    }

    public void setPhotoOrder(int[] photoOrder) {
        mPhotoOrder = photoOrder;
    }

    public int getCoverId() {
        return mCoverId;
    }

    public void setCoverId(int coverId) {
        mCoverId = coverId;
    }

    public int getViewCount() {
        return mViewCount;
    }

    public void setViewCount(int viewCount) {
        mViewCount = viewCount;
    }

    public String getSlug() {
        return mSlug;
    }

    public void setSlug(String slug) {
        mSlug = slug;
    }

    public int getPlaybackInterval() {
        return mPlaybackInterval;
    }

    public void setPlaybackInterval(int playbackInterval) {
        mPlaybackInterval = playbackInterval;
    }

    public int getSongId() {
        return mSongId;
    }

    public void setSongId(int songId) {
        mSongId = songId;
    }

    public int getLikesCount() {
        return mLikesCount;
    }

    public void setLikesCount(int likesCount) {
        mLikesCount = likesCount;
    }

    public int getCommentsCount() {
        return mCommentsCount;
    }

    public void setCommentsCount(int commentsCount) {
        mCommentsCount = commentsCount;
    }

    public List<JsonPhoto> getPhoto() {
        return mPhoto;
    }

    public void setPhoto(List<JsonPhoto> photo) {
        mPhoto = photo;
    }

    public User getUser() {
        return mUser;
    }

    public void setUser(User user) {
        mUser = user;
    }

    public String getShareLink() {
        return mShareLink;
    }

    public void setShareLink(String shareLink) {
        mShareLink = shareLink;
    }

    public boolean isLikeByCurrentUser() {
        return mIsLikeByCurrentUser;
    }

    public void setIsLikeByCurrentUser(boolean isLikeByCurrentUser) {
        mIsLikeByCurrentUser = isLikeByCurrentUser;
    }
}
