/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/4/15 10:46 AM.
 **/

package co.soml.android.app.activities.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import co.soml.android.app.R;
import co.soml.android.app.camera.utils.DistanceUtil;
import co.soml.android.app.models.Photo;
import co.soml.android.app.utils.LogUtils;

public class GalleryAdapter extends BaseAdapter {
    public static final String TAG = LogUtils.makeLogTag(GalleryAdapter.class.getSimpleName());
    private Context mContext;
    private List<Photo> mValues;
    private static GalleryHolder mHolder;

    public GalleryAdapter(Context context, ArrayList<Photo> photos) {
        this.mContext = context;
        this.mValues = photos;
    }

    @Override
    public int getCount() {
        return mValues.size();
    }

    @Override
    public Object getItem(int position) {
        return mValues.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final GalleryHolder holder;
        int width = DistanceUtil.getCameraAlbumWidth();
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.layout_gallery_item, null);
            holder = new GalleryHolder();
            holder.mGalleryImage = (ImageView) convertView.findViewById(R.id.gallery_item_image);
            holder.mGalleryImage.setLayoutParams(new AbsListView.LayoutParams(width, width));
            convertView.setTag(holder);
        } else {
            holder = (GalleryHolder) convertView.getTag();
        }
        final Photo gallery = (Photo) getItem(position);
        // Loading the image from photo's uri.
        return convertView;
    }

    class GalleryHolder {
        ImageView mGalleryImage;
    }
}
