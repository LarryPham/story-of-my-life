package co.soml.android.app.utils;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.widget.MultiAutoCompleteTextView;

public class SuggestionTokenizer implements MultiAutoCompleteTextView.Tokenizer {

    public static final String TAG = LogUtils.makeLogTag(SuggestionTokenizer.class.getSimpleName());

    @Override
    public int findTokenStart(CharSequence text, int cursor) {
        int index = cursor;
        while (index > 0 && text.charAt(index - 1) != '@') {
            index--;
        }
        if (index < 1 || text.charAt(index - 1) != '@') {
            return cursor;
        }

        return index;
    }

    @Override
    public int findTokenEnd(CharSequence text, int cursor) {
        int index = cursor;
        int len = text.length();
        while (index < len) {
            if (text.charAt(index) == ' ') {
                return index;
            } else {
                index++;
            }
        }
        return len;
    }

    @Override
    public CharSequence terminateToken(CharSequence text) {
        int index = text.length();
        while (index > 0 && text.charAt(index - 1) == ' ') {
            index--;
        }

        if (text instanceof Spanned) {
            SpannableString sp = new SpannableString(text + " ");
            TextUtils.copySpansFrom((Spanned) text, 0, text.length(), Object.class, sp, 0);
            LogUtils.LOGD(TAG, String.format("Spanned Text: %s", sp.toString()));
            return sp;
        } else {
            return text + " ";
        }
    }
}
