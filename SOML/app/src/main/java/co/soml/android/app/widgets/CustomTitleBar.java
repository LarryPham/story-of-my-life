/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/6/15 9:37 AM.
 **/

package co.soml.android.app.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import co.soml.android.app.R;
import co.soml.android.app.utils.LogUtils;

public class CustomTitleBar extends RelativeLayout {
    public static final String TAG = LogUtils.makeLogTag(CustomTitleBar.class.getSimpleName());
    private static final int BUTTON_LIMIT_TIME = 500;
    private String mTitleTextStr;
    private StoryTextView mLeftButton;
    private ImageView mLeftButtonImage;

    private StoryTextView mMiddleButton;

    private StoryTextView mRightButton;
    private ImageView  mRightButtonImage;

    private int mLeftButtonIconId;
    private int mRightButtonIconId;

    private String mLeftButtonStr;
    private String mRightButtonStr;

    private String mBackgroundStyle;

    public CustomTitleBar(Context context) {
        super(context);
    }

    public CustomTitleBar(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray arr = context.obtainStyledAttributes(attrs, R.styleable.CommonTitleBar);
        mLeftButtonStr = arr.getString(R.styleable.CommonTitleBar_leftButtonText);
        mLeftButtonIconId = arr.getResourceId(R.styleable.CommonTitleBar_leftButtonIcon, 0);

        mTitleTextStr = arr.getString(R.styleable.CommonTitleBar_titleText);
        mLeftButtonStr = arr.getString(R.styleable.CommonTitleBar_leftButtonText);
        mRightButtonStr = arr.getString(R.styleable.CommonTitleBar_rightButtonText);

        mRightButtonIconId = arr.getResourceId(R.styleable.CommonTitleBar_rightButtonIcon, 0);
        mBackgroundStyle = arr.getString(R.styleable.CommonTitleBar_baseStyle);

        if (isInEditMode()) {
            LayoutInflater.from(context).inflate(R.layout.view_title_bar, this);
            return;
        }

        LayoutInflater.from(context).inflate(R.layout.view_title_bar, this);
        findViewById(R.id.title_out_frame).setBackgroundResource(R.color.color_primary);
        arr.recycle();
    }

    protected void onFinishInflate() {
        super.onFinishInflate();

        if (isInEditMode()) {
            return;
        }

        mLeftButtonImage = (ImageView) findViewById(R.id.title_left_button);
        mLeftButton = (StoryTextView) findViewById(R.id.title_left);
        mMiddleButton = (StoryTextView) findViewById(R.id.title_middle);

        mRightButtonImage = (ImageView) findViewById(R.id.title_right_button);
        mRightButton = (StoryTextView) findViewById(R.id.title_right);

        if (mLeftButtonIconId != 0) {
            mLeftButtonImage.setImageResource(mLeftButtonIconId);
            mLeftButtonImage.setVisibility(View.VISIBLE);
        } else {
            mLeftButtonImage.setVisibility(View.GONE);
        }

        if (mRightButtonIconId != 0) {
            mRightButtonImage.setImageResource(mRightButtonIconId);
            mRightButtonImage.setVisibility(View.VISIBLE);
        } else {
            mRightButtonImage.setVisibility(View.GONE);
        }
        setLeftTextButton(mLeftButtonStr);
        setTitleText(mTitleTextStr);
        setRightTextButton(mRightButtonStr);
    }

    public void setLeftTextButton(String leftButtonStr) {
        if (!TextUtils.isEmpty(leftButtonStr)) {
            mLeftButton.setText(leftButtonStr);
            mLeftButton.setVisibility(View.VISIBLE);
        } else {
            mLeftButton.setVisibility(View.GONE);
        }
    }

    public void setRightTextButton(String rightButtonStr) {
        if (!TextUtils.isEmpty(rightButtonStr)) {
            mRightButton.setText(mRightButtonStr);
            mRightButton.setVisibility(View.VISIBLE);
        } else {
            mRightButton.setVisibility(View.GONE);
        }
    }

    public void setTitleText(String title) {
        if (!TextUtils.isEmpty(title)) {
            mMiddleButton.setText(title);
            mMiddleButton.setVisibility(View.VISIBLE);
        } else {
            mMiddleButton.setVisibility(View.GONE);
        }
    }

    public void hideRightButton() {
        mRightButton.setVisibility(View.GONE);
        mRightButtonImage.setVisibility(View.GONE);
        findViewById(R.id.title_right_area).setOnClickListener(null);
    }

    public void hideLeftButton() {
        mLeftButton.setVisibility(View.GONE);
        mLeftButtonImage.setVisibility(View.GONE);
        findViewById(R.id.title_left_area).setOnClickListener(null);
    }

    public void setLeftButtonOnClickListener(OnClickListener listener) {
        OnClickListener limitListener = new GlobalLimitClickOnClickListener(listener, BUTTON_LIMIT_TIME);
        findViewById(R.id.title_left_area).setOnClickListener(limitListener);
    }

    public void setRightButtonOnClickListener(OnClickListener listener) {
        OnClickListener limitListener = new GlobalLimitClickOnClickListener(listener, BUTTON_LIMIT_TIME);
        findViewById(R.id.title_right_area).setOnClickListener(limitListener);
    }
}
