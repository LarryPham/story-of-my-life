package co.soml.android.app.controllers;

import android.os.Message;

import co.soml.android.app.StoryApp;
import co.soml.android.app.activities.BaseActivity;
import co.soml.android.app.activities.StoryMainActivity;
import co.soml.android.app.fragments.BaseFragment;
import co.soml.android.app.fragments.ProgressFragment;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.widgets.ProgressCircleDialog;
import co.soml.android.app.widgets.ProgressWheel;

public class SearchController extends BaseController {
    public static final String TAG = LogUtils.makeLogTag(SearchController.class.getSimpleName());

    public StoryMainActivity mActivity;
    public StoryApp mApp;
    public StoryDataModel mDataModel;

    private ProgressWheel mWaitDialog = null;
    private boolean mScrollState = false;
    private ProgressFragment mProgressFragment = null;

    public SearchController(BaseActivity activity, StoryDataModel dataModel) {
        super(activity, dataModel);
        this.mActivity = (StoryMainActivity) activity;
        this.mApp = (StoryApp) mActivity.getApplicationContext();
        this.mDataModel = dataModel;
    }

    public SearchController(BaseActivity activity, StoryDataModel dataModel, boolean isFirstRunningMode) {
        super(activity, dataModel, isFirstRunningMode);
        this.mActivity = (StoryMainActivity) activity;
        this.mApp = (StoryApp) mActivity.getApplicationContext();
        this.mDataModel = dataModel;
    }

    public SearchController(BaseActivity activity, BaseFragment fragment,
                            StoryDataModel dataModel) {
        super(activity, fragment, dataModel);
        this.mActivity = (StoryMainActivity) activity;
        this.mApp = (StoryApp) mActivity.getApplicationContext();
        this.mDataModel = dataModel;
    }

    private void waitDialogFragment() {

    }
    @Override
    protected void handleMessage(Message msg) {

    }
}
