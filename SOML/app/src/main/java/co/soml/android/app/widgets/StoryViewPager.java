package co.soml.android.app.widgets;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

import co.soml.android.app.utils.LogUtils;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since May.20.2015
 */
public class StoryViewPager extends ViewPager {
    public static final String TAG = LogUtils.makeLogTag(StoryViewPager.class.getSimpleName());

    public StoryViewPager(Context context) {
        super(context);
    }

    public StoryViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * This method has been added blocks of code for resolving the issues "Pointer Index Out Of Range" bug in the compatibility lib
     **/
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        try {
            return super.onInterceptTouchEvent(ev);
        } catch (IllegalArgumentException ex) {
            LogUtils.LOGE(TAG, ex.toString());
            return false;
        }
    }

    /**
     * This method has been added blocks of code for resolving the issues "Pointer Index Out Of Range" bug in the compatibility lib
     **/
    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        try {
            return super.onTouchEvent(ev);
        } catch (IllegalArgumentException ex) {
            LogUtils.LOGE(TAG, ex.toString());
            return false;
        }
    }
}
