package co.soml.android.app.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.SearchView;

import co.soml.android.app.AppConstants;
import co.soml.android.app.StoryApp;
import co.soml.android.app.controllers.SearchController;
import co.soml.android.app.models.StoryDataModel;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.14.2015
 * <p>
 * The <code>SearchActivity</code> used to be integrated into ActionBar. It provided the functionality for searching the people or
 * stories that was existed into the server side.
 */
public class SearchActivity extends BaseActivity {
    public static final String TAG = AppConstants.PREFIX + SearchActivity.class.getSimpleName();

    private StoryApp mApp;
    private SearchController mController;
    private StoryDataModel mDataModel;
    private Context mContext;
    private SearchView mSearchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this.getApplicationContext();
        mApp = (StoryApp) this.getApplicationContext();
        mDataModel = mApp.getAppDataModel();
        mController = new SearchController(SearchActivity.this, mDataModel);
    }

    @Override
    public void invalidate() {

    }

    @Override
    public void invalidate(Object... params) {

    }
}
