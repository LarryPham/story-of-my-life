/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/20/15 8:48 AM.
 **/

package co.soml.android.app.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class File implements Parcelable, Serializable {

    private String mUrl;

    public String getUri() {
        return mUrl;
    }

    public void setUri(String fileUri) {
        mUrl = fileUri;
    }

    public File(String url) {
        this.mUrl = url;
    }

    public File(File other) {
        this.mUrl = other.mUrl;
    }

    public File(Parcel in) {
        mUrl = in.readString();
    }

    public static final Creator<File> CREATOR = new Creator<File>() {
        @Override
        public File createFromParcel(Parcel in) {
            return new File(in);
        }

        @Override
        public File[] newArray(int size) {
            return new File[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mUrl);
    }
}
