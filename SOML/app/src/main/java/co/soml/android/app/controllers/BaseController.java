package co.soml.android.app.controllers;

import android.content.Intent;
import android.os.Message;

import co.soml.android.app.AppConstants;
import co.soml.android.app.StoryApp;
import co.soml.android.app.activities.BaseActivity;
import co.soml.android.app.fragments.BaseFragment;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.services.StoryService;
import co.soml.android.app.services.tasks.Action;
import co.soml.android.app.utils.LogUtils;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its
 * documentation are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable
 * form without the prior written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with
 * respect to the contents, and assumes no responsibility for any errors that might appear in the software and
 * documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.01.2015
 */
public abstract class BaseController {
    protected static final String TAG = AppConstants.PREFIX + BaseController.class.getSimpleName();
    protected BaseActivity mActivity;
    protected BaseFragment mFragment;
    protected StoryApp mStoryApp;
    protected StoryDataModel mDataModel;

    protected BaseActivityHandler mHandler = new BaseActivityHandler();
    protected ActionCounter mCounter = new ActionCounter();

    public BaseController(BaseActivity activity, StoryDataModel dataModel) {
        this.mActivity = activity;
        this.mDataModel = dataModel;
        this.mStoryApp = (StoryApp) mActivity.getApplicationContext();
        onCreate();
    }

    public BaseController(BaseActivity activity, StoryDataModel dataModel, boolean isFirstRunningMode) {
        this.mActivity = activity;
        this.mDataModel = dataModel;
        this.mStoryApp = (StoryApp) mActivity.getApplicationContext();
        if (isFirstRunningMode) {
            mStoryApp.removeAllHandler();
        }
        onCreate();
    }

    public BaseController(BaseActivity activity, BaseFragment fragment, StoryDataModel dataModel) {
        this.mActivity = activity;
        this.mFragment = fragment;
        this.mDataModel = dataModel;
        this.mStoryApp = (StoryApp) mActivity.getApplicationContext();
        onCreate();
    }

    public BaseController(BaseActivity activity, BaseFragment fragment, StoryDataModel dataModel, boolean isFirstRunningMode) {
        this.mActivity = activity;
        this.mFragment = fragment;
        this.mDataModel = dataModel;
        if (isFirstRunningMode) {
            mStoryApp.removeAllHandler();
        }
        onCreate();
    }

    public void onCreate() {
        mStoryApp.addHandler(mHandler);
    }

    public void onDestroy() {
        String fn = "onDestroy()";
        LogUtils.LOGD(TAG, fn + String.format("removeHandler"));
        mStoryApp.removeHandler(mHandler);
    }

    public void sendMessage(int what, Object obj) {
        final Message msg = new Message();
        msg.what = what;
        msg.obj = obj;
        handleMessage(msg);
    }

    protected Intent obtainIntent(int msg) {
        final Intent intent = new Intent(mActivity, StoryService.class);
        intent.putExtra(Action.REQUEST_OWNER, this.hashCode());
        intent.putExtra(Action.REQUEST_MSG, msg);
        return intent;
    }

    protected boolean checkOwner(Message msg) {
        if (msg.arg1 == this.hashCode()) {
            return true;
        } else {
            LogUtils.LOGD(TAG, String.format("[Controller] checkOwner false [%d:%d]", msg.arg1, this.hashCode()));
            return false;
        }
    }

    public ActionCounter getActionCounter() {
        return this.mCounter;
    }

    public void setActionCounter(ActionCounter counter) {
        this.mCounter = counter;
    }

    protected abstract void handleMessage(Message msg);

    public class BaseActivityHandler extends ActivityHandler {

        @Override
        public void callBack(Message msg) {
            handleMessage(msg);
        }
    }
}
