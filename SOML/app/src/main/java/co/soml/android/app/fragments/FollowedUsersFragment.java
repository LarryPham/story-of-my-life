/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 * @author: Larry Pham (email: larrypham.vn@gmail.com)
 * Date: 6/2/15 4:18 PM
 **/

package co.soml.android.app.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import co.soml.android.app.StoryApp;
import co.soml.android.app.activities.StoryMainActivity;
import co.soml.android.app.controllers.PeopleController;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.utils.LogUtils;

public class FollowedUsersFragment extends BaseFragment {
    public static final String TAG = LogUtils.makeLogTag(FollowedUsersFragment.class.getSimpleName());

	private StoryMainActivity mActivity;
    private StoryApp mStoryApp;
    private StoryDataModel mDataModel;
	private PeopleController mController;
	private View mRootView;
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		this.mActivity = (StoryMainActivity) this.getActivity();
		this.mStoryApp = (StoryApp) mActivity.getApplication();
		this.mDataModel = mStoryApp.getAppDataModel();
		this.mController = new PeopleController(mActivity, this, mDataModel);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
    public void invalidate() {

    }

    @Override
    public void invalidate(Object... params) {

    }
}
