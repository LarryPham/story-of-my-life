package co.soml.android.app.widgets;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

import co.soml.android.app.utils.LogUtils;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since May.20.2015
 */
public class StoryRecyclerView extends RecyclerView {
    public static final String TAG = LogUtils.makeLogTag(StoryRecyclerView.class.getSimpleName());

    public StoryRecyclerView(Context context) {
        super(context);
        initialize(context);
    }

    public StoryRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize(context);
    }

    public StoryRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initialize(context);
    }

    private void initialize(Context context) {
        if (!isInEditMode()) {
            setLayoutManager(new LinearLayoutManager(context));
        }
    }

    /**
     * Dividers for Story Cards
     */
    public static class StoryItemDecoration extends RecyclerView.ItemDecoration {
        private final int mHorizontalSpacing;
        private final int mVerticalSpacing;

        public StoryItemDecoration(int horizontalSpacing, int verticalSpacing) {
            super();
            mHorizontalSpacing = horizontalSpacing;
            mVerticalSpacing = verticalSpacing;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, State state) {
            super.getItemOffsets(outRect, view, parent, state);
            outRect.set(mHorizontalSpacing, mVerticalSpacing, mHorizontalSpacing, 0);
        }
    }
}
