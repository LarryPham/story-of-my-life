/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/17/15 11:04 AM.
 **/

package co.soml.android.app.services;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import co.soml.android.app.models.JsonPhoto;
import co.soml.android.app.utils.Lists;
import co.soml.android.app.utils.LogUtils;

public class PhotosResult extends BaseResult {

    public static final String TAG = LogUtils.makeLogTag(PhotosResult.class.getSimpleName());

    @Expose
    @SerializedName("photos")
    private List<JsonPhoto> mPhotoList = Lists.newArrayList();

    public PhotosResult() {

    }

    public List<JsonPhoto> getPhotoList() {
        return mPhotoList;
    }

    public void setPhotoList(List<JsonPhoto> photoList) {
        mPhotoList = photoList;
    }
}
