/**
 * Copyright (C)  2015 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 * Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 * @author: Larry Pham (email: larrypham.vn@gmail.com)
 * @date: 7/15/15 8:45 AM
 **/

package co.soml.android.app.fragments.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import java.lang.ref.WeakReference;
import java.util.List;

import co.soml.android.app.fragments.BaseFragment;
import co.soml.android.app.fragments.ProfileFollowersFragment;
import co.soml.android.app.fragments.UsersListFragment;
import co.soml.android.app.fragments.ProfileStoriesFragment;
import co.soml.android.app.models.StoryBean;
import co.soml.android.app.utils.Lists;
import co.soml.android.app.utils.LogUtils;

public class ProfileTabsAdapter extends FragmentPagerAdapter {
	public static final String TAG = LogUtils.makeLogTag(ProfileTabsAdapter.class.getSimpleName());

	private final SparseArray<WeakReference<BaseFragment>> mFragmentArray = new SparseArray<WeakReference<BaseFragment>>();
	private final List<ProfileHolder> mHolderList = Lists.newArrayList();
	private final Context mContext;
	private int mCurrentPage;
	private StoryBean mStoryBean;

	public ProfileTabsAdapter(Context context, FragmentManager fragmentManager) {
		super(fragmentManager);
		this.mContext = context;
	}

	public void add(final Class<? extends BaseFragment> className, final Bundle params) {
		final ProfileHolder  holder = new ProfileHolder();
		holder.mClassName = className.getName();
		holder.mParams = params;

		final int position = mHolderList.size();
		mHolderList.add(position, holder);
		notifyDataSetChanged();
	}

	@Override
	public BaseFragment getItem(int position) {
		final ProfileHolder holder = mHolderList.get(position);
		return (BaseFragment) BaseFragment.instantiate(mContext, holder.mClassName, holder.mParams);
	}

	public BaseFragment getFragment(final int position) {
		final WeakReference<BaseFragment> weakRefFragment = mFragmentArray.get(position);
		if (weakRefFragment != null && weakRefFragment.get() != null) {
			return weakRefFragment.get();
		}
		return getItem(position);
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		final BaseFragment fragment = (BaseFragment) super.instantiateItem(container, position);
		final WeakReference<BaseFragment> weakRefFragment = mFragmentArray.get(position);

		if (weakRefFragment != null) {
			weakRefFragment.clear();
		}
		mFragmentArray.put(position, new WeakReference<BaseFragment>(fragment));
		return fragment;
	}

	@Override
	public int getCount() {
		return mHolderList.size();
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		super.destroyItem(container, position, object);
		final WeakReference<BaseFragment> weakRefFragment = mFragmentArray.get(position);
		if (weakRefFragment != null) {
			weakRefFragment.clear();
		}
	}

	public int getCurrentPage() {
		return mCurrentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.mCurrentPage = currentPage;
	}

	public enum ProfileFragment {
		STORIES(ProfileStoriesFragment.class),
		FOLLOWING(UsersListFragment.class),
		FOLLOWERS(UsersListFragment.class);

		private Class<? extends BaseFragment> mFragmentClass;

		ProfileFragment(final Class<? extends BaseFragment> fragmentClass) {
			mFragmentClass = fragmentClass;
		}

		public Class<? extends BaseFragment> getFragmentClass() {
			return mFragmentClass;
		}
	}

	public static final class ProfileHolder {
		String mClassName;
		Bundle mParams;
	}
}
