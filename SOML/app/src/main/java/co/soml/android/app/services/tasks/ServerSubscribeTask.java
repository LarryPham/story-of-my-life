package co.soml.android.app.services.tasks;

import android.os.Handler;
import android.os.Message;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import co.soml.android.app.AppConstants;
import co.soml.android.app.controllers.ControllerMessage;
import co.soml.android.app.models.Error;
import co.soml.android.app.models.JsonComment;
import co.soml.android.app.models.StorySettings;
import co.soml.android.app.models.User;
import co.soml.android.app.services.CommentListResult;
import co.soml.android.app.services.StatusResult;
import co.soml.android.app.services.StoryResult;
import co.soml.android.app.services.UserResult;
import co.soml.android.app.services.VoteResult;
import co.soml.android.app.services.apis.AppRestApi;
import co.soml.android.app.services.rest.ItemTypeAdapterFactory;
import co.soml.android.app.utils.LogUtils;
import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its
 * documentation are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable
 * form without the prior written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with
 * respect to the contents, and assumes no responsibility for any errors that might appear in the software and
 * documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.03.2015
 * <p>
 * This Thread used to handle the subscribing the story content into the List of StoryBoard(Market Page) and then when
 * user wanna subscribe the content as the followers, we should used this situation for subscriber-task,
 * and when checking the following users we should also renew this task for supporting this feature.
 */
public class ServerSubscribeTask implements Runnable {
    public static final String TAG = AppConstants.PREFIX + ServerSubscribeTask.class.getSimpleName();

    private Handler mServiceHandler = null;
    private String mAction = null;
    private SubscribeType mSubscribeType = SubscribeType.NONE;
    private int mRequestOwner = 0;
    private int mRequestMsg = 0;
    private boolean mCancelled = false;
    private String mReason;
    private String mMessage;
    private Gson mGson;
    private RequestInterceptor mInterceptor;
    private String mAccessToken;

    private String mAuthToken;
    private String mAuthTokenSecret;
    private long mAuthFabricId;
    private int mApiVersion;

    private StorySettings mStorySettings;
    private String mUserEmail;
    private String mUserPassword;
    private String mUserPasswordConfirmation;
    private String mDescription;
    private String mUserFullName;
    private int mUserId;
    private int mStoryId;
    private int mCollaborationId;
    private int mPhotoId;
    private int mCommentId;
    private String mCommentBody;
    private int mSongId;
    private int mStickerPackId;
    private boolean isCancelled = false;
    private Callback mCallback;
    private String mStoryName;
    private int mStoryInterval;
    private String mStoryDescription;
    private float mStoryLatitude;
    private float mStoryLongtitude;
    private String mStoryLocation;
    private GsonConverter mGsonConverter = new GsonConverter(mGson);
    private String mStoryScope;
    private com.twitter.sdk.android.core.Callback mTwitterCallback;
    private com.twitter.sdk.android.core.AuthToken mTwitterAuthToken;

    public ServerSubscribeTask(String action, Handler handler) {
        this.mAction = action;
        this.mServiceHandler = handler;
        this.mGson = new Gson();
        this.mGsonConverter = new GsonConverter(mGson);
        mGson = new GsonBuilder()
                .registerTypeAdapterFactory(new ItemTypeAdapterFactory()) // This is the important line ;)
                .create();
    }

    public String getUserEmail() {
        return mUserEmail;
    }

    public void setUserEmail(String userEmail) {
        this.mUserEmail = userEmail;
    }

    public String getUserPassword() {
        return mUserPassword;
    }

    public void setUserPassword(String userPassword) {
        this.mUserPassword = userPassword;
    }

    public String getUserPasswordConfirmation() {
        return mUserPasswordConfirmation;
    }

    public void setUserPasswordConfirmation(String userPasswordConfirmation) {
        mUserPasswordConfirmation = userPasswordConfirmation;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getUserFullName() {
        return mUserFullName;
    }

    public void setUserFullName(String userFullName) {
        mUserFullName = userFullName;
    }

    public int getUserId() {
        return mUserId;
    }

    public void setUserId(int userId) {
        mUserId = userId;
    }

    public int getStoryId() {
        return mStoryId;
    }

    public void setStoryId(int storyId) {
        mStoryId = storyId;
    }

    public int getCollaborationId() {
        return mCollaborationId;
    }

    public void setCollaborationId(int collaborationId) {
        mCollaborationId = collaborationId;
    }

    public int getPhotoId() {
        return mPhotoId;
    }

    public void setPhotoId(int photoId) {
        mPhotoId = photoId;
    }

    public int getCommentId() {
        return mCommentId;
    }

    public void setCommentId(int commentId) {
        mCommentId = commentId;
    }

    public String getCommentBody() {
        return mCommentBody;
    }

    public void setCommentBody(String commentBody) {
        mCommentBody = commentBody;
    }

    public int getSongId() {
        return mSongId;
    }

    public void setSongId(int songId) {
        mSongId = songId;
    }

    public int getStickerPackId() {
        return mStickerPackId;
    }

    public void setStickerPackId(int stickerPackId) {
        mStickerPackId = stickerPackId;
    }

    public String getAuthToken() {
        return mAuthToken;
    }

    public void setAuthToken(String authToken) {
        mAuthToken = authToken;
    }

    public int getApiVersion() {
        return mApiVersion;
    }

    public void setApiVersion(int apiVersion) {
        mApiVersion = apiVersion;
    }

    public String getAuthTokenSecret() {
        return mAuthTokenSecret;
    }

    public void setAuthTokenSecret(String authTokenSecret) {
        mAuthTokenSecret = authTokenSecret;
    }

    public long getAuthFabricId() {
        return mAuthFabricId;
    }

    public void setAuthFabricId(long authFabricId) {
        mAuthFabricId = authFabricId;
    }

    public void buildAPIs() {
        mGson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();
        final String authHeader = mAccessToken;
    }

    public boolean isCancelled() {
        return this.isCancelled;
    }

    public void setCancelled(boolean isCancelled) {
        this.isCancelled = isCancelled;
    }

    public Callback getSubscribeCallback() {
        return this.mCallback;
    }

    public void setSubscribeCallback(Callback callback) {
        this.mCallback = callback;
    }

    public String getAccessToken() {
        return mAccessToken;
    }

    public void setAccessToken(String accessToken) {
        this.mAccessToken = accessToken;
    }

    public void setSubscribeType(SubscribeType subscribeType) {
        this.mSubscribeType = subscribeType;
    }

    public int getRequestOwner() {
        return mRequestOwner;
    }

    public void setRequestOwner(int requestOwner) {
        mRequestOwner = requestOwner;
    }

    public int getRequestMsg() {
        return mRequestMsg;
    }

    public void setRequestMsg(int requestMsg) {
        mRequestMsg = requestMsg;
    }

    public String getStoryName() {
        return mStoryName;
    }

    public void setStoryName(String storyName) {
        mStoryName = storyName;
    }

    public int getStoryInterval() {
        return mStoryInterval;
    }

    public void setStoryInterval(int storyInterval) {
        mStoryInterval = storyInterval;
    }

    public String getStoryDescription() {
        return mStoryDescription;
    }

    public void setStoryDescription(String storyDescription) {
        mStoryDescription = storyDescription;
    }

    public RequestInterceptor getInterceptor() {
        return mInterceptor;
    }

    public void setRequestInterceptor(RequestInterceptor interceptor) {
        this.mInterceptor = interceptor;
    }

    public float getStoryLatitude() {
        return mStoryLatitude;
    }

    public void setStoryLatitude(float storyLatitude) {
        mStoryLatitude = storyLatitude;
    }

    public float getStoryLongtitude() {
        return mStoryLongtitude;
    }

    public void setStoryLongtitude(float storyLongtitude) {
        mStoryLongtitude = storyLongtitude;
    }

    public String getStoryLocation() {
        return mStoryLocation;
    }

    public void setStoryLocation(String storyLocation) {
        mStoryLocation = storyLocation;
    }

    public String getReason() {
        return mReason;
    }

    public void setReason(String reason) {
        mReason = reason;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getStoryScope() {
        return mStoryScope;
    }

    public void setStoryScope(String storyScope) {
        mStoryScope = storyScope;
    }

    @Override
    public void run() {
        String fn = "run(): ";
        switch (mSubscribeType) {
            // Register user
            case ACCOUNT_REGISTER: {
                registerAccount();
                break;
            }
            // Login with facebook
            case SUBSCRIBE_FACEBOOK: {
                subscribeWithFacebook();
                break;
            }
            // Login with email access token
            case SUBSCRIBE_EMAIL: {
                subscribeWithEmailAccount();
                break;
            }
            // Login with twitter access token
            case SUBSCRIBE_TWITTER: {
                subscribeWithTwitter();
                break;
            }
            case GUEST_ACCOUNT: {
                subscribeGuest();
                break;
            }
            case USER_FOLLOW: {
                followUser();
                break;
            }
            case USER_UNFOLLOW: {
                unfollowUser();
                break;
            }
            // Update story
            case STORY_UPDATE: {
                updateStory();
                break;
            }
            case STORY_DELETE: {
                deleteStory();
                break;
            }
            case STORY_REPORT: {
                reportStory();
                break;
            }
            case STORY_LIKE: {
                likeStory();
                break;
            }
            case STORY_UNLIKE: {
                unlikeStory();
                break;
            }
            case STORY_POST: {
                postStory();
                break;
            }
            case POST_PHOTOS: {
                break;
            }
            case UPDATE_PHOTO_DESCRIPTION: {
                break;
            }
            case CHANGE_PHOTOS_ORDER: {
                break;
            }
            case COMMENT_REPLY: {
                replyComment();
                break;
            }
            case COMMENT_LIKE: {
                likeComment();
                break;
            }
            case COMMENT_UNLIKE: {
                unlikeComment();
                break;
            }
            case COMMENT_STORY: {
                commentStory();
                break;
            }
            case COMMENT_UPDATE: {
                updateComment();
                break;
            }
            case COMMENT_DELETE: {
                deleteComment();
                break;
            }
            case COMMENT_REPORT: {
                reportComment();
                break;
            }
        }
    }

    public void registerAccount() {
        final String fn = "[registerAccount]:";
        final UserResult result = new UserResult();
        final AppRestApi api = RestApiUtils.getAppRestApi();

        mCallback = new Callback<UserResult>() {
            @Override
            public void success(UserResult userResult, Response response) {
                if (userResult != null) {
                    result.setUser(userResult.getUser());
                    result.setResultCode(response.getStatus());
                    result.setResultMessage(response.getReason());
                    LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_REGISTER_USER_COMPLETED %s", fn, result));
                    sendMessage(ControllerMessage.REQUEST_TO_SERVER_REGISTER_USER_COMPLETED, mRequestOwner, mRequestMsg, result);
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                final Error error = new Error();
                final int errorCode = retrofitError.getResponse().getStatus();
                final String errorMessage = retrofitError.getMessage();
                final String errorException = retrofitError.getResponse().getReason();

                error.setErrorCode(errorCode);
                error.setErrorMessage(errorMessage);
                error.setErrorException(errorException);

                if (errorCode == 422) {
                    // missing params
                    sendMessage(ControllerMessage.REQUEST_ERROR_REASON_PARAMS_MISSING, mRequestOwner, mRequestMsg, error);
                } else if (errorCode == 401) {
                    // Authenticated error
                    sendMessage(ControllerMessage.REQUEST_ERROR_REASON_HTTP_UNAUTHORIZED, mRequestOwner, mRequestMsg, error);
                } else if (errorCode == 403) {
                    LogUtils.LOGD(TAG, String.format("REQUEST_ERROR_REASON_FORBIDDEN %d", errorCode));
                    sendMessage(ControllerMessage.REQUEST_ERROR_REASON_SERVER_FORBIDDEN, mRequestOwner, mRequestMsg, error);
                } else {
                    if (retrofitError.getCause() instanceof java.net.SocketTimeoutException) {
                        // Connection timeout..
                        sendMessage(ControllerMessage.REQUEST_ERROR_REASON_SERVER_CONNECT_TIMEOUT, mRequestOwner, mRequestMsg, error);
                    }
                    // Network disconnected
                    if (retrofitError.getCause() instanceof java.net.UnknownHostException) {
                        sendMessage(ControllerMessage.REQUEST_ERROR_REASON_UNKNOWN_HOST, mRequestOwner, mRequestMsg, error);
                    }
                    // General errors
                    LogUtils.LOGD(TAG, String.format("Exception: %s", retrofitError.getResponse().getReason()));
                    LogUtils.LOGD(TAG, fn + String.format("REQUEST_TO_SERVER_ERROR --- %d, %s, %s", errorCode, errorMessage, errorException));
                    sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, mRequestOwner, mRequestMsg, error);
                }

            }
        };
        api.registerAccount(mUserFullName, mUserEmail, mUserPassword, mUserPasswordConfirmation, mDescription, mCallback);
    }

    /**
     * Method susbcribeWithFacebook() used to post the Facebook's AccessToken to current app's server for getting
     * the generated JsonUser object for this kind of user who used social network authentication.
     */
    public void subscribeWithFacebook() {
        String fn = "subscribeWithFacebook: ";
        final UserResult result = new UserResult();

        //this.mAccessToken = mStorySettings.getAccessToken();
        mInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade requestFacade) {
                requestFacade.addHeader("X_AUTH_TOKEN", mAccessToken);
            }
        };
        final AppRestApi appRestApi = RestApiUtils.getAppRestApi(mInterceptor, new GsonConverter(mGson));
        mCallback = new Callback<UserResult>() {
            @Override
            public void success(UserResult user, Response response) {
                if (user != null) {
                    result.setUser(user.getUser());
                    result.setResultCode(response.getStatus());
                    result.setResultMessage(response.getReason());
                    LogUtils.LOGD(TAG, String.format("REQUEST_TO_SERVER_SIGN_IN_WITH_FACEBOOK %s", result));
                    sendMessage(ControllerMessage.REQUEST_TO_SERVER_SIGN_IN_WITH_FACEBOOK_COMPLETED, mRequestOwner, mRequestMsg, result);
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                LogUtils.LOGD(TAG, String.format("Response: %s-%s", retrofitError.getResponse(), retrofitError.getBody()));
                final int errorCode = retrofitError.getResponse() != null ? retrofitError.getResponse().getStatus() : 0;
                final String errorMessage = retrofitError.getMessage() != null ? retrofitError.getMessage() : null;
                final String errorException = retrofitError.getResponse() != null
                        ? (retrofitError.getResponse().getReason() != null ?
                        retrofitError.getResponse().getReason() : null) : null;
                final Error error = new Error();
                error.setErrorCode(errorCode);
                error.setErrorMessage(errorMessage);
                error.setErrorException(errorException);


                LogUtils.LOGD(TAG, String.format("REQUEST_TO_SERVER_ERROR --- %d,s %s, %", errorCode, errorMessage, errorException));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, mRequestOwner, mRequestMsg, error);
            }
        };
        appRestApi.signInByFacebook(mAccessToken, mCallback);
    }

    public void addXAPIAuthen() {
        if (getAuthToken() != null && getApiVersion() != 0) {
            mInterceptor = new RequestInterceptor() {
                @Override
                public void intercept(RequestFacade requestFacade) {
                    requestFacade.addHeader("X_API_VERSION", String.valueOf(getApiVersion()));
                    requestFacade.addHeader("X_AUTH_TOKEN", getAuthToken());
                }
            };
            // Changing the new interceptor with 2 params for headers
            setRequestInterceptor(mInterceptor);
        }
    }

    /**
     * Method susbcribeWithTwitter() used to post the Twitter's AccessToken to current app's server for getting
     * the generated JsonUser object for this kind of user who used social network authentication.
     */
    public void subscribeWithTwitter() {
        final String fn = "subscribeWithTwitter: ";
        final UserResult result = new UserResult();
        mInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade requestFacade) {
                requestFacade.addHeader("X_AUTH_TOKEN", mAccessToken);
            }
        };
        final AppRestApi appRestApi = RestApiUtils.getAppRestApi(mInterceptor, new GsonConverter(mGson));
        mCallback = new Callback<UserResult>() {

            @Override
            public void success(UserResult user, Response response) {
                if (user != null) {
                    LogUtils.LOGD(TAG, fn + String.format("REQUEST_TO_SERVER_SIGN_IN_WITH_TWITTER_COMPLETED %s", user));
                    result.setUser(user.getUser());
                    result.setResultCode(response.getStatus());
                    result.setResultMessage(response.getReason());
                    LogUtils.LOGD(TAG, String.format("REQUEST_TO_SERVER_SIGN_IN_WITH_TWITTER_COMPLETED %s", result.toString()));
                    sendMessage(ControllerMessage.REQUEST_TO_SERVER_SIGN_IN_WITH_TWITTER_COMPLETED, mRequestOwner, mRequestMsg, result);
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                final Error error = new Error();

                LogUtils.LOGD(TAG, fn + String.format("Response: %s", retrofitError.getResponse()));
                final int errorCode = retrofitError.getResponse().getStatus();
                final String errorMessage = retrofitError.getMessage();
                final String errorException = retrofitError.getResponse().getReason();

                error.setErrorCode(errorCode);
                error.setErrorMessage(errorMessage);
                error.setErrorException(errorException);

                if (errorCode == 422) {
                    // missing params
                    sendMessage(ControllerMessage.REQUEST_ERROR_REASON_PARAMS_MISSING, mRequestOwner, mRequestMsg, error);
                } else if (errorCode == 401) {
                    // Authenticated error
                    sendMessage(ControllerMessage.REQUEST_ERROR_REASON_HTTP_UNAUTHORIZED, mRequestOwner, mRequestMsg, error);
                } else {
                    if (retrofitError.getCause() instanceof java.net.SocketTimeoutException) {
                        // Connection timeout..
                        sendMessage(ControllerMessage.REQUEST_ERROR_REASON_SERVER_CONNECT_TIMEOUT, mRequestOwner, mRequestMsg, error);
                    }
                    // Network disconnected
                    if (retrofitError.getCause() instanceof java.net.UnknownHostException) {
                        sendMessage(ControllerMessage.REQUEST_ERROR_REASON_UNKNOWN_HOST, mRequestOwner, mRequestMsg, error);
                    }
                    // General errors
                    LogUtils.LOGD(TAG, String.format("Exception: %s", retrofitError.getResponse().getReason()));
                    LogUtils.LOGD(TAG, fn + String.format("REQUEST_TO_SERVER_ERROR --- %d, %s, %s", errorCode, errorMessage, errorException));
                    sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, mRequestOwner, mRequestMsg, error);
                }
            }
        };
        LogUtils.LOGD(TAG, String.format("Token: %s, Secret: %s, FabricId: %d", mAuthToken, mAuthTokenSecret, mAuthFabricId));
        appRestApi.signInByFabric(mAuthToken, mAuthTokenSecret, mAuthFabricId, mCallback);
    }

    /**
     * Method subscribeWithEmailAccount() used to post the email, password to server with specified api routes.
     * Renewing <code>CallBack</code> object to handling this situation for getting a JsonUser object.
     */
    public void subscribeWithEmailAccount() {
        String fn = "subscribeWithEmailAccount: ";
        final UserResult result = new UserResult();
        final AppRestApi appRestApi = RestApiUtils.getAppRestApi(new GsonConverter(mGson));
        mCallback = new Callback<UserResult>() {
            @Override
            public void success(UserResult user, Response response) {
                LogUtils.LOGD(TAG, String.format("User: %s", user.toString()));
                result.setUser(user.getUser());
                result.setResultCode(response.getStatus());
                result.setResultMessage(response.getReason());
                LogUtils.LOGD(TAG, String.format("REQUEST_TO_SERVER_SIGN_IN_WITH_EMAIL_ACCOUNT_COMPLETED %s", result));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_SIGN_IN_WITH_EMAIL_ACCOUNT_COMPLETED, mRequestOwner, mRequestMsg, result);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                final Error error = new Error();

                LogUtils.LOGD(TAG, String.format("Response: %s-%s", retrofitError.getResponse(), retrofitError.getBody()));
                final int errorCode = retrofitError.getResponse() != null ? retrofitError.getResponse().getStatus() : 0;
                final String errorMessage = retrofitError.getMessage() != null ? retrofitError.getMessage() : null;
                final String errorException = retrofitError.getResponse() != null
                        ? (retrofitError.getResponse().getReason() != null ?
                        retrofitError.getResponse().getReason() : null) : null;
                error.setErrorCode(errorCode);
                error.setErrorMessage(errorMessage);
                error.setErrorException(errorException);
                LogUtils.LOGD(TAG, String.format("REQUEST_TO_SERVER_ERROR --- %d,%s", errorCode, errorMessage));
                if (errorCode == 422) {
                    // missing params
                    sendMessage(ControllerMessage.REQUEST_ERROR_REASON_PARAMS_MISSING, mRequestOwner, mRequestMsg, error);
                } else if (errorCode == 401) {
                    // Authenticated error
                    sendMessage(ControllerMessage.REQUEST_ERROR_REASON_HTTP_UNAUTHORIZED, mRequestOwner, mRequestMsg, error);
                } else {
                    if (retrofitError.getCause() instanceof java.net.SocketTimeoutException) {
                        // Connection timeout..
                        sendMessage(ControllerMessage.REQUEST_ERROR_REASON_SERVER_CONNECT_TIMEOUT, mRequestOwner, mRequestMsg, error);
                    }
                    // Network disconnected
                    if (retrofitError.getCause() instanceof java.net.UnknownHostException) {
                        sendMessage(ControllerMessage.REQUEST_ERROR_REASON_UNKNOWN_HOST, mRequestOwner, mRequestMsg, error);
                    }
                    // General errors
                    sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, mRequestOwner, mRequestMsg, error);
                }
            }
        };
        appRestApi.signInByEmailAccount(mUserEmail, mUserPassword, mCallback);
    }

    /**
     * Method subscribeGuest() used to get anonymous token returned from server.
     * Renewing <code>CallBack</code> object to handling this situation for getting a JsonUser object.
     */
    public void subscribeGuest() {
        final String fn = "[subscribeGuest]: ";
        final UserResult result = new UserResult();
        final AppRestApi api = RestApiUtils.getAppRestApi();
        mCallback = new Callback<UserResult>() {
            @Override
            public void success(UserResult user, Response response) {
                if (user != null) {
                    result.setUser(user.getUser());
                    result.setResultCode(response.getStatus());
                    result.setResultMessage(response.getReason());
                    LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_SIGN_IN_GUEST_COMPLETED %s", fn, result));
                    sendMessage(ControllerMessage.REQUEST_TO_SERVER_SIGN_IN_GUEST_COMPLETED, mRequestOwner, mRequestMsg, result);
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                if (retrofitError != null) {
                    final Error error = new Error();
                    if (retrofitError.getCause() instanceof java.net.UnknownHostException) {
                        error.setErrorCode(404);
                        error.setErrorMessage(retrofitError.getMessage());
                        error.setErrorException(retrofitError.getCause().getMessage());
                        LogUtils.LOGD(TAG, String.format("%s REQUEST_ERROR_REASON_UNKNOWN_HOST --- %d,%s", fn, error.getErrorCode(), error
                                .getErrorMessage()));
                        sendMessage(ControllerMessage.REQUEST_ERROR_REASON_UNKNOWN_HOST, mRequestOwner, mRequestMsg, error);
                    } else {
                        LogUtils.LOGD(TAG, String.format("Response: %s", retrofitError.getResponse()));
                        if (retrofitError.getResponse() != null) {
                            final int errorCode = retrofitError.getResponse().getStatus();
                            final String errorMessage = retrofitError.getMessage();
                            final String errorException = retrofitError.getResponse().getReason();

                            error.setErrorCode(errorCode);
                            error.setErrorMessage(errorMessage);
                            error.setErrorException(errorException);
                            LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_ERROR --- %d,%s", fn, errorCode, errorMessage));
                            sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, mRequestOwner, mRequestMsg, error);
                        }
                    }
                }
            }
        };
        api.signInGuest(mCallback);
    }

    public void reportStory() {
        final String fn = "[reportStory]: ";
        final StoryResult result = new StoryResult();
        final AppRestApi api = RestApiUtils.getAppRestApi();
        mCallback = new Callback<StoryResult>() {
            @Override
            public void success(StoryResult storyResult, Response response) {
                result.setStory(storyResult.getStory());
                result.setResultCode(response.getStatus());
                result.setResultMessage(response.getReason());
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_STORY_REPORT_COMPLETED %s", fn, result));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_STORY_REPORT_COMPLETED, mRequestOwner, mRequestMsg, result);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                final Error error = new Error();

                LogUtils.LOGD(TAG, String.format("Response: %s", retrofitError.getResponse()));
                final int errorCode = retrofitError.getResponse().getStatus();
                final String errorMessage = retrofitError.getMessage();
                final String errorException = retrofitError.getResponse().getReason();

                error.setErrorCode(errorCode);
                error.setErrorMessage(errorMessage);
                error.setErrorException(errorException);
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_ERROR --- %d,%s", fn, errorCode, errorMessage));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, mRequestOwner, mRequestMsg, error);
            }
        };
        api.reportStory(mReason, mMessage, mCallback);
    }

    public void likeStory() {
        final String fn = "[likeStory]: ";
        final VoteResult result = new VoteResult();
        addXAPIAuthen();
        final AppRestApi api = RestApiUtils.getAppRestApi(getInterceptor(), new GsonConverter(mGson));
        mCallback = new Callback<VoteResult>() {
            @Override
            public void success(VoteResult voteResult, Response response) {
                result.setId(mStoryId);
                result.setVote(voteResult.getVote());
                result.setResultCode(response.getStatus());
                result.setResultMessage(response.getReason());
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_STORY_LIKE_COMPLETED %s", fn, result));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_STORY_LIKE_COMPLETED, mRequestOwner, mRequestMsg, result);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                final Error error = new Error();

                LogUtils.LOGD(TAG, String.format("Response: %s", retrofitError.getResponse()));
                final int errorCode = retrofitError.getResponse().getStatus();
                final String errorMessage = retrofitError.getMessage();
                final String errorException = retrofitError.getResponse().getReason();

                error.setErrorCode(errorCode);
                error.setErrorMessage(errorMessage);
                error.setErrorException(errorException);
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_ERROR --- %d,%s", fn, errorCode, errorMessage));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, mRequestOwner, mRequestMsg, error);
            }
        };
        api.likeStory(mStoryId, mCallback);
    }

    public void unlikeStory() {
        final String fn = "[unlikeStory]: ";
        final VoteResult result = new VoteResult();
        addXAPIAuthen();
        final AppRestApi api = RestApiUtils.getAppRestApi(getInterceptor(), new GsonConverter(mGson));
        mCallback = new Callback<VoteResult>() {
            @Override
            public void success(VoteResult voteResult, Response response) {
                result.setId(mStoryId);
                result.setVote(voteResult.getVote());
                result.setResultCode(response.getStatus());
                result.setResultMessage(response.getReason());
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_STORY_UNLIKE_COMPLETED %s", fn, result));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_STORY_UNLIKE_COMPLETED, mRequestOwner, mRequestMsg, result);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                final Error error = new Error();

                LogUtils.LOGD(TAG, String.format("Response: %s", retrofitError.getResponse()));
                final int errorCode = retrofitError.getResponse().getStatus();
                final String errorMessage = retrofitError.getMessage();
                final String errorException = retrofitError.getResponse().getReason();

                error.setErrorCode(errorCode);
                error.setErrorMessage(errorMessage);
                error.setErrorException(errorException);
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_ERROR --- %d,%s", fn, errorCode, errorMessage));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, mRequestOwner, mRequestMsg, error);
            }
        };
        api.unlikeStory(mStoryId, mCallback);
    }

    public void postStory() {
        final String fn = "[postStory]: ";
        final StoryResult result = new StoryResult();
        final AppRestApi api = RestApiUtils.getAppRestApi();
        mCallback = new Callback<StoryResult>() {
            @Override
            public void success(StoryResult storyResult, Response response) {
                result.setStory(storyResult.getStory());
                result.setResultCode(response.getStatus());
                result.setResultMessage(response.getReason());
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_STORY_POST_COMPLETED %s", fn, result));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_STORY_POST_COMPLETED, mRequestOwner, mRequestMsg, result);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                final Error error = new Error();

                LogUtils.LOGD(TAG, String.format("Response: %s", retrofitError.getResponse()));
                final int errorCode = retrofitError.getResponse().getStatus();
                final String errorMessage = retrofitError.getMessage();
                final String errorException = retrofitError.getResponse().getReason();

                error.setErrorCode(errorCode);
                error.setErrorMessage(errorMessage);
                error.setErrorException(errorException);
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_ERROR --- %d,%s", fn, errorCode, errorMessage));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, mRequestOwner, mRequestMsg, error);
            }
        };
        api.postStory(mStoryName, mStoryInterval, mStoryDescription, mStoryLatitude, mStoryLongtitude, mStoryLocation, mCallback);
    }

    public void updateStory() {
        final String fn = "[updateStory]: ";
        final StoryResult result = new StoryResult();
        final AppRestApi api = RestApiUtils.getAppRestApi();
        mCallback = new Callback<StoryResult>() {
            @Override
            public void success(StoryResult storyResult, Response response) {
                result.setStory(storyResult.getStory());
                result.setResultCode(response.getStatus());
                result.setResultMessage(response.getReason());
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_STORY_UPDATE_COMPLETED %s", fn, result));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_STORY_UPDATE_COMPLETED, mRequestOwner, mRequestMsg, result);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                final Error error = new Error();

                LogUtils.LOGD(TAG, String.format("Response: %s", retrofitError.getResponse()));
                final int errorCode = retrofitError.getResponse().getStatus();
                final String errorMessage = retrofitError.getMessage();
                final String errorException = retrofitError.getResponse().getReason();

                error.setErrorCode(errorCode);
                error.setErrorMessage(errorMessage);
                error.setErrorException(errorException);
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_ERROR --- %d,%s", fn, errorCode, errorMessage));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, mRequestOwner, mRequestMsg, error);
            }
        };
        api.updateStory(mStoryId, mStoryName, mStoryInterval, mStoryDescription, mStoryLatitude,
                mStoryLongtitude, mStoryLocation, mCallback);
    }

    public void deleteStory() {
        final String fn = "[deleteStory]: ";
        final StoryResult result = new StoryResult();
        final AppRestApi api = RestApiUtils.getAppRestApi();
        mCallback = new Callback<StoryResult>() {
            @Override
            public void success(StoryResult storyResult, Response response) {
                result.setStory(storyResult.getStory());
                result.setResultCode(response.getStatus());
                result.setResultMessage(response.getReason());
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_STORY_REMOVE_COMPLETED %s", fn, result));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_STORY_REMOVE_COMPLETED, mRequestOwner, mRequestMsg, result);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                final Error error = new Error();

                LogUtils.LOGD(TAG, String.format("Response: %s", retrofitError.getResponse()));
                final int errorCode = retrofitError.getResponse().getStatus();
                final String errorMessage = retrofitError.getMessage();
                final String errorException = retrofitError.getResponse().getReason();

                error.setErrorCode(errorCode);
                error.setErrorMessage(errorMessage);
                error.setErrorException(errorException);
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_ERROR --- %d,%s", fn, errorCode, errorMessage));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, mRequestOwner, mRequestMsg, error);
            }
        };
        api.deleteStory(mStoryId, mCallback);
    }

    public void followUser() {
        final String fn = "[followUser]: ";
        final StatusResult result = new StatusResult();
        addXAPIAuthen();
        final AppRestApi api = RestApiUtils.getAppRestApi(getInterceptor(), new GsonConverter(mGson));
        mCallback = new Callback<StatusResult>() {
            @Override
            public void success(StatusResult statusResult, Response response) {
                result.setUserId(mUserId);
                result.setStatus(statusResult.getStatus());
                result.setResultCode(response.getStatus());
                result.setResultMessage(response.getReason());
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_USER_FOLLOW_COMPLETED %s", fn, result));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_USER_FOLLOW_COMPLETED, mRequestOwner, mRequestMsg, result);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                final Error error = new Error();

                LogUtils.LOGD(TAG, String.format("Response: %s", retrofitError.getResponse()));
                final int errorCode = retrofitError.getResponse().getStatus();
                final String errorMessage = retrofitError.getMessage();
                final String errorException = retrofitError.getResponse().getReason();

                error.setErrorCode(errorCode);
                error.setErrorMessage(errorMessage);
                error.setErrorException(errorException);
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_ERROR --- %d,%s", fn, errorCode, errorMessage));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, mRequestOwner, mRequestMsg, error);
            }
        };
        api.followUser(mUserId, mCallback);
    }

    public void unfollowUser() {
        final String fn = "[unfollowUser]: ";
        final StatusResult result = new StatusResult();
        addXAPIAuthen();
        final AppRestApi api = RestApiUtils.getAppRestApi(getInterceptor(), new GsonConverter(mGson));
        mCallback = new Callback<StatusResult>() {
            @Override
            public void success(StatusResult userResult, Response response) {
                result.setUserId(mUserId);
                result.setStatus(userResult.getStatus());
                result.setResultCode(response.getStatus());
                result.setResultMessage(response.getReason());
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_USER_UNFOLLOW_COMPLETED %s", fn, result));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_USER_UNFOLLOW_COMPLETED, mRequestOwner, mRequestMsg, result);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                final Error error = new Error();

                LogUtils.LOGD(TAG, String.format("Response: %s", retrofitError.getResponse()));
                final int errorCode = retrofitError.getResponse().getStatus();
                final String errorMessage = retrofitError.getMessage();
                final String errorException = retrofitError.getResponse().getReason();

                error.setErrorCode(errorCode);
                error.setErrorMessage(errorMessage);
                error.setErrorException(errorException);
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_ERROR --- %d,%s", fn, errorCode, errorMessage));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, mRequestOwner, mRequestMsg, error);
            }
        };
        api.unfollowUser(mUserId, mCallback);
    }

    public void reportUser() {
        final String fn = "[reportUser]: ";
        final UserResult result = new UserResult();
        final AppRestApi api = RestApiUtils.getAppRestApi();
        mCallback = new Callback<User>() {
            @Override
            public void success(User user, Response response) {
                result.setUser(user);
                result.setResultCode(response.getStatus());
                result.setResultMessage(response.getReason());
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_USER_REPORT_COMPLETED %s", fn, result));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_USER_REPORT_COMPLETED, mRequestOwner, mRequestMsg, result);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                final Error error = new Error();

                LogUtils.LOGD(TAG, String.format("Response: %s", retrofitError.getResponse()));
                final int errorCode = retrofitError.getResponse().getStatus();
                final String errorMessage = retrofitError.getMessage();
                final String errorException = retrofitError.getResponse().getReason();

                error.setErrorCode(errorCode);
                error.setErrorMessage(errorMessage);
                error.setErrorException(errorException);
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_ERROR --- %d,%s", fn, errorCode, errorMessage));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, mRequestOwner, mRequestMsg, error);
            }
        };
        api.reportUser(mReason, mMessage, mCallback);
    }

    public void replyComment() {
        final String fn = "[replyComment]: ";
        final CommentListResult result = new CommentListResult();
        final AppRestApi api = RestApiUtils.getAppRestApi();
        mCallback = new Callback<List<JsonComment>>() {
            @Override
            public void success(List<JsonComment> commentList, Response response) {
                result.setCommentList(commentList);
                result.setResultCode(response.getStatus());
                result.setResultMessage(response.getReason());
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_COMMENT_REPLY_COMPLETED %s", fn, result));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_COMMENT_REPLY_COMPLETED, mRequestOwner, mRequestMsg, result);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                final Error error = new Error();

                LogUtils.LOGD(TAG, String.format("Response: %s", retrofitError.getResponse()));
                final int errorCode = retrofitError.getResponse().getStatus();
                final String errorMessage = retrofitError.getMessage();
                final String errorException = retrofitError.getResponse().getReason();

                error.setErrorCode(errorCode);
                error.setErrorMessage(errorMessage);
                error.setErrorException(errorException);
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_ERROR --- %d,%s", fn, errorCode, errorMessage));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, mRequestOwner, mRequestMsg, error);
            }
        };
        api.replyComment(mStoryId, mCommentId, mCommentBody, mCallback);
    }

    public void likeComment() {
        final String fn = "[replyComment]: ";
        final CommentListResult result = new CommentListResult();
        final AppRestApi api = RestApiUtils.getAppRestApi();
        mCallback = new Callback<List<JsonComment>>() {
            @Override
            public void success(List<JsonComment> commentList, Response response) {
                result.setCommentList(commentList);
                result.setResultCode(response.getStatus());
                result.setResultMessage(response.getReason());
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_COMMENT_LIKE_COMPLETED %s", fn, result));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_COMMENT_LIKE_COMPLETED, mRequestOwner, mRequestMsg, result);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                final Error error = new Error();

                LogUtils.LOGD(TAG, String.format("Response: %s", retrofitError.getResponse()));
                final int errorCode = retrofitError.getResponse().getStatus();
                final String errorMessage = retrofitError.getMessage();
                final String errorException = retrofitError.getResponse().getReason();

                error.setErrorCode(errorCode);
                error.setErrorMessage(errorMessage);
                error.setErrorException(errorException);
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_ERROR --- %d,%s", fn, errorCode, errorMessage));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, mRequestOwner, mRequestMsg, error);
            }
        };
        api.likeComment(mStoryId, mCommentId, mCallback);
    }

    public void unlikeComment() {
        final String fn = "[unlikeComment]: ";
        final CommentListResult result = new CommentListResult();
        final AppRestApi api = RestApiUtils.getAppRestApi();
        mCallback = new Callback<List<JsonComment>>() {
            @Override
            public void success(List<JsonComment> commentList, Response response) {
                result.setCommentList(commentList);
                result.setResultCode(response.getStatus());
                result.setResultMessage(response.getReason());
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_COMMENT_UNLIKE_COMPLETED %s", fn, result));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_COMMENT_UNLIKE_COMPLETED, mRequestOwner, mRequestMsg, result);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                final Error error = new Error();

                LogUtils.LOGD(TAG, String.format("Response: %s", retrofitError.getResponse()));
                final int errorCode = retrofitError.getResponse().getStatus();
                final String errorMessage = retrofitError.getMessage();
                final String errorException = retrofitError.getResponse().getReason();

                error.setErrorCode(errorCode);
                error.setErrorMessage(errorMessage);
                error.setErrorException(errorException);
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_ERROR --- %d,%s", fn, errorCode, errorMessage));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, mRequestOwner, mRequestMsg, error);
            }
        };
        api.unlikeComment(mStoryId, mCommentId, mCallback);
    }

    public void commentStory() {
        final String fn = "[commentStory]: ";
        final CommentListResult result = new CommentListResult();
        final AppRestApi api = RestApiUtils.getAppRestApi();
        mCallback = new Callback<List<JsonComment>>() {
            @Override
            public void success(List<JsonComment> commentList, Response response) {
                result.setCommentList(commentList);
                result.setResultCode(response.getStatus());
                result.setResultMessage(response.getReason());
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_COMMENT_STORY_COMPLETED %s", fn, result));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_COMMENT_STORY_COMPLETED, mRequestOwner, mRequestMsg, result);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                final Error error = new Error();

                LogUtils.LOGD(TAG, String.format("Response: %s", retrofitError.getResponse()));
                final int errorCode = retrofitError.getResponse().getStatus();
                final String errorMessage = retrofitError.getMessage();
                final String errorException = retrofitError.getResponse().getReason();

                error.setErrorCode(errorCode);
                error.setErrorMessage(errorMessage);
                error.setErrorException(errorException);
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_ERROR --- %d,%s", fn, errorCode, errorMessage));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, mRequestOwner, mRequestMsg, error);
            }
        };
        api.commentStory(mStoryId, mCommentBody, mCallback);
    }

    public void updateComment() {
        final String fn = "[updateComment]: ";
        final CommentListResult result = new CommentListResult();
        final AppRestApi api = RestApiUtils.getAppRestApi();
        mCallback = new Callback<List<JsonComment>>() {
            @Override
            public void success(List<JsonComment> commentList, Response response) {
                result.setCommentList(commentList);
                result.setResultCode(response.getStatus());
                result.setResultMessage(response.getReason());
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_COMMENT_UPDATE_COMPLETED %s", fn, result));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_COMMENT_UPDATE_COMPLETED, mRequestOwner, mRequestMsg, result);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                final Error error = new Error();

                LogUtils.LOGD(TAG, String.format("Response: %s", retrofitError.getResponse()));
                final int errorCode = retrofitError.getResponse().getStatus();
                final String errorMessage = retrofitError.getMessage();
                final String errorException = retrofitError.getResponse().getReason();

                error.setErrorCode(errorCode);
                error.setErrorMessage(errorMessage);
                error.setErrorException(errorException);
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_ERROR --- %d,%s", fn, errorCode, errorMessage));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, mRequestOwner, mRequestMsg, error);
            }
        };
        api.updateComment(mStoryId, mCommentId, mCommentBody, mCallback);
    }

    public void deleteComment() {
        final String fn = "[deleteComment]: ";
        final CommentListResult result = new CommentListResult();
        final AppRestApi api = RestApiUtils.getAppRestApi();
        mCallback = new Callback<List<JsonComment>>() {
            @Override
            public void success(List<JsonComment> commentList, Response response) {
                result.setCommentList(commentList);
                result.setResultCode(response.getStatus());
                result.setResultMessage(response.getReason());
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_COMMENT_DELETE_COMPLETED %s", fn, result));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_COMMENT_DELETE_COMPLETED, mRequestOwner, mRequestMsg, result);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                final Error error = new Error();

                LogUtils.LOGD(TAG, String.format("Response: %s", retrofitError.getResponse()));
                final int errorCode = retrofitError.getResponse().getStatus();
                final String errorMessage = retrofitError.getMessage();
                final String errorException = retrofitError.getResponse().getReason();

                error.setErrorCode(errorCode);
                error.setErrorMessage(errorMessage);
                error.setErrorException(errorException);
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_ERROR --- %d,%s", fn, errorCode, errorMessage));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, mRequestOwner, mRequestMsg, error);
            }
        };
        api.deleteComment(mStoryId, mCommentId, mCallback);
    }

    public void reportComment() {
        final String fn = "[reportComment]: ";
        final CommentListResult result = new CommentListResult();
        final AppRestApi api = RestApiUtils.getAppRestApi();
        mCallback = new Callback<List<JsonComment>>() {
            @Override
            public void success(List<JsonComment> commentList, Response response) {
                result.setCommentList(commentList);
                result.setResultCode(response.getStatus());
                result.setResultMessage(response.getReason());
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_COMMENT_REPORT_COMPLETED %s", fn, result));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_COMMENT_REPORT_COMPLETED, mRequestOwner, mRequestMsg, result);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                final Error error = new Error();

                LogUtils.LOGD(TAG, String.format("Response: %s", retrofitError.getResponse()));
                final int errorCode = retrofitError.getResponse().getStatus();
                final String errorMessage = retrofitError.getMessage();
                final String errorException = retrofitError.getResponse().getReason();

                error.setErrorCode(errorCode);
                error.setErrorMessage(errorMessage);
                error.setErrorException(errorException);
                LogUtils.LOGD(TAG, String.format("%s REQUEST_TO_SERVER_ERROR --- %d,%s", fn, errorCode, errorMessage));
                sendMessage(ControllerMessage.REQUEST_TO_SERVER_ERROR, mRequestOwner, mRequestMsg, error);
            }
        };
        api.reportComment(mStoryId, mCommentId, mReason, mMessage, mCallback);
    }

    public void sendMessage(int code, int requestOwner, int requestMsg, Object obj) {
        String fn = "sendMessage(): ";
        final Message msg = new Message();
        LogUtils.LOGD(TAG, fn + "code   = " + ControllerMessage.toString(code) + "(" + code + ")");
        LogUtils.LOGD(TAG, fn + "requestOwner = " + requestOwner);
        LogUtils.LOGD(TAG, fn + "requestMessage = " + requestMsg);
        if (obj instanceof Error) {
            final Error error = (Error) obj;
            int errorCode = error.getErrorCode();
            LogUtils.LOGD(TAG, fn + "errorCode = " + ControllerMessage.toString(error.getErrorCode()) + "(" + errorCode + ")");
        }

        msg.what = code;
        msg.arg1 = requestOwner;
        msg.arg2 = requestMsg;
        msg.obj = obj;

        if (isCancelled()) {
            LogUtils.LOGD(TAG, String.format("Task cancelled. Do not send message"));
        } else {
            mServiceHandler.sendMessage(msg);
        }
    }

    public enum SubscribeType {
        NONE, SUBSCRIBE_EMAIL, SUBSCRIBE_FACEBOOK, SUBSCRIBE_TWITTER, GUEST_ACCOUNT, ACCOUNT_REGISTER,
        STORY_UPDATE, STORY_DELETE, STORY_REPORT, STORY_LIKE, STORY_UNLIKE, STORY_POST,
        POST_PHOTOS, UPDATE_PHOTO_DESCRIPTION, CHANGE_PHOTOS_ORDER,
        USER_FOLLOW, USER_UNFOLLOW, USER_REPORT,
        COMMENT_REPLY, COMMENT_LIKE, COMMENT_UNLIKE, COMMENT_STORY, COMMENT_UPDATE, COMMENT_DELETE, COMMENT_REPORT,
	    FOLLOWING_USERS, FOLLOWERS
    }

    public class EmailSignInParam {
        public String mEmail;
        public String mPassword;
    }
}
