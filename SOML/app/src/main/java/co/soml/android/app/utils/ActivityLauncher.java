/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 7/28/15 4:06 AM.
 **/

package co.soml.android.app.utils;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import co.soml.android.app.AppConstants;
import co.soml.android.app.R;
import co.soml.android.app.activities.BaseActivity;
import co.soml.android.app.activities.CommentsActivity;
import co.soml.android.app.activities.StoryDetailActivity;
import co.soml.android.app.models.Story;

public class ActivityLauncher {
    /**
     * Shows StoryDetailActivity by the specified story-id
     * @param context The Application Context.
     * @param storyId The Story's Identification.
     */
    public static void showStoryDetail(final Context context, long storyId) {
        Intent intent = new Intent(context, StoryDetailActivity.class);
        intent.putExtra(AppConstants.STORY_ID, storyId);

        if (context instanceof FragmentActivity) {
            CharSequence title = null;
            ActionBar actionBar = ((AppCompatActivity) context).getSupportActionBar();
            if (actionBar != null) {
                title = actionBar.getTitle();
            }

            if (title == null) {
                title = ((AppCompatActivity) context).getTitle();
            }
            intent.putExtra(AppConstants.TITLE, title);
        }
    }

    /**
     * Shows CommentsActivity screen
     * @param context The Application Context.
     * @param story The specified story.
     */
    public static void showStoryComments(final Context context, Story story) {
        if (story == null) {
            return;
        }

        Intent intent = new Intent(context, CommentsActivity.class);
        intent.putExtra(AppConstants.STORY_ID, story.getId());
        if (context instanceof BaseActivity) {
            BaseActivity activity = (BaseActivity) context;
            ActivityOptionsCompat options = ActivityOptionsCompat.makeCustomAnimation(activity, R.anim.story_flyin,R.anim.fab_scale_up);
            ActivityCompat.startActivity(activity, intent, options.toBundle());
        } else {
            context.startActivity(intent);
        }
    }

}
