package co.soml.android.app.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import co.soml.android.app.R;
import co.soml.android.app.utils.CommonUtil;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.utils.StoryAnimator;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since May.19.2015
 * <p>
 * The classs <code>StoryIconCountView</code> represent the abstract buttons which have been displayed into the Story CardView, to
 * responsible to handle the comment, like, like count, comment count...
 */
public class StoryIconCountView extends LinearLayout {
    public static final String TAG = LogUtils.makeLogTag(StoryIconCountView.class.getSimpleName());
    private ImageView mImageView;
    private TextView mTextCount;
    private int mCurrentCount;

    private static final int ICON_LIKE = 0;
    private static final int ICON_COMMENT = 1;
    private static final int ICON_SHARE = 2;

    private static final int ICON_COMMENT_LIKE = 3;
    private static final int ICON_COMMENT_REPLY = 4;

    private int mIconMode;

    public TextView getTextCount() {
        return mTextCount;
    }

    public StoryIconCountView(Context context) {
        super(context);
        initView(context, null);
    }

    public StoryIconCountView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context, attrs);
    }

    public StoryIconCountView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    private void initView(Context context, AttributeSet attrs) {
        inflate(context, R.layout.story_icon_count_view, this);
        mImageView = (ImageView) findViewById(R.id.image_count);
        mTextCount = (TextView) findViewById(R.id.text_count);

        if (attrs != null) {
            TypedArray array = context.getTheme().obtainStyledAttributes(attrs, R.styleable.StoryIconCountView, 0, 0);
            try {
                int icon = array.getInteger(R.styleable.StoryIconCountView_storyIcon, ICON_LIKE);
                switch (icon) {
                    case ICON_LIKE:
                        mIconMode = ICON_LIKE;
                        mImageView.setImageResource(R.drawable.button_like_background);
                        break;
                    case ICON_COMMENT:
                        mIconMode = ICON_COMMENT;
                        mImageView.setImageResource(R.drawable.button_comment_background);
                        break;
                    case ICON_SHARE:
                        mIconMode = ICON_SHARE;
                        mImageView.setImageResource(R.drawable.share_button_background);
                        break;
                    case ICON_COMMENT_LIKE: {
                        mIconMode = ICON_COMMENT_LIKE;
                        mImageView.setImageResource(R.drawable.button_like_background);
                        break;
                    }
                    case ICON_COMMENT_REPLY: {
                        mIconMode = ICON_COMMENT_REPLY;
                        mImageView.setImageResource(R.drawable.ic_comment_reply);
                        break;
                    }
                }
            } finally {
                array.recycle();
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // set ripple drawable background for button if the current os is Lollipop
            mImageView.setBackgroundResource(R.drawable.ripple_oval);
        }
    }

    public ImageView getImageView() {
        return mImageView;
    }

    public void setSelected(boolean selected) {
        this.mImageView.setSelected(selected);
    }

    public void setCount(int count, boolean animateChanges) {
        if (count != 0) {
            if (mIconMode == ICON_LIKE) {
                // Set the TextCount with new value of counter
                if (count > 1) {
                    mTextCount.setText(String.valueOf(CommonUtil.formatInt(count) + " likes"));
                } else {
                    mTextCount.setText(String.valueOf(CommonUtil.formatInt(count) + " like"));
                }
            } else if (mIconMode == ICON_COMMENT) {
                // Set the TextCount with new value of counter
                if (count > 1) {
                    mTextCount.setText(String.valueOf(CommonUtil.formatInt(count) + " comments"));
                } else {
                    mTextCount.setText(String.valueOf(CommonUtil.formatInt(count) + " comment"));
                }
            } else if (mIconMode == ICON_SHARE) {
                mTextCount.setVisibility(View.INVISIBLE);
            }
        }

        if (animateChanges && count != mCurrentCount) {
            if (count == 0 && mTextCount.getVisibility() == View.VISIBLE) {
                StoryAnimator.scaleOut(mTextCount, View.GONE, StoryAnimator.Duration.LONG, null);
            } else if (mCurrentCount == 0 && mTextCount.getVisibility() != View.VISIBLE) {
                StoryAnimator.scaleIn(mTextCount, StoryAnimator.Duration.LONG);
            } else {
                mTextCount.setVisibility(count > 0 ? View.VISIBLE : View.GONE);
            }
        } else {
            mTextCount.setVisibility(count > 0 ? View.VISIBLE : View.GONE);
        }
        mCurrentCount = count;
    }

}
