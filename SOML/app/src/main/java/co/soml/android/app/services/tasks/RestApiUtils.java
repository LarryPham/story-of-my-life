package co.soml.android.app.services.tasks;

import co.soml.android.app.Properties;
import co.soml.android.app.services.apis.AppRestApi;
import co.soml.android.app.utils.LogUtils;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.converter.Converter;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.21.2015
 */
public class RestApiUtils {
    public static final String TAG = LogUtils.makeLogTag(RestApiUtils.class);
    public static RestAdapter.Builder sBuilder;
    public static RestAdapter sRestAdapter;

    private RestApiUtils() {

    }

    public static AppRestApi getAppRestApi() {
        sBuilder = new RestAdapter.Builder();
        sBuilder.setEndpoint(Properties.BASE_SERVER_URL);
        sBuilder.setLogLevel(RestAdapter.LogLevel.FULL);
        sRestAdapter = sBuilder.build();
        return sRestAdapter.create(AppRestApi.class);
    }

    public static AppRestApi getAppRestApi(RequestInterceptor interceptor) {
        sBuilder = new RestAdapter.Builder();
        sBuilder.setEndpoint(Properties.BASE_SERVER_URL);
        sBuilder.setRequestInterceptor(interceptor);
        sBuilder.setLogLevel(RestAdapter.LogLevel.FULL);
        sRestAdapter = sBuilder.build();
        return sRestAdapter.create(AppRestApi.class);
    }

    public static AppRestApi getAppRestApi(Converter converter) {
        sBuilder = new RestAdapter.Builder();
        sBuilder.setEndpoint(Properties.BASE_SERVER_URL);
        sBuilder.setConverter(converter);
        sBuilder.setLogLevel(RestAdapter.LogLevel.FULL);
        sRestAdapter = sBuilder.build();
        return sRestAdapter.create(AppRestApi.class);
    }

    public static AppRestApi getAppRestApi(RequestInterceptor interceptor, Converter converter) {
        sBuilder = new RestAdapter.Builder();
        sBuilder.setEndpoint(Properties.BASE_SERVER_URL);
        sBuilder.setRequestInterceptor(interceptor);
        sBuilder.setConverter(converter);
        sBuilder.setLogLevel(RestAdapter.LogLevel.FULL);
        sRestAdapter = sBuilder.build();
        return sRestAdapter.create(AppRestApi.class);
    }
}
