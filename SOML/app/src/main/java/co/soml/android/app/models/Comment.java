package co.soml.android.app.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

import co.soml.android.app.AppConstants;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its
 * documentation are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable
 * form without the prior written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with
 * respect to the contents, and assumes no responsibility for any errors that might appear in the software and
 * documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.03.2015
 */
public class Comment implements Parcelable {
    public static final String TAG = AppConstants.PREFIX + Comment.class.getSimpleName();

    private int mId;
    private int mUserId;
    private int mStoryId;
    private String mBody;
    private Date mCreatedAt;
    private Date mUpdatedAt;
    private Date mDeletedAt;
    private String mAncestry;

    // not stored in db - denotes the indentation level when displaying this comment
    public transient int level = 0;

    public Comment(Parcel source) {
        readFromParcel(source);
    }

    public Comment(JsonComment jsonComment) {
        this.mId = jsonComment.getId();
        this.mUserId = jsonComment.getUserId();
        this.mStoryId = jsonComment.getStoryId();
        this.mBody = jsonComment.getBody();
        this.mCreatedAt = jsonComment.getCreatedAt();
        this.mUpdatedAt = jsonComment.getUpdatedAt();
        this.mDeletedAt = jsonComment.getDeletedAt();
        this.mAncestry = jsonComment.getAncestry();
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getUserId() {
        return mUserId;
    }

    public void setUserId(int userId) {
        mUserId = userId;
    }

    public int getStoryId() {
        return mStoryId;
    }

    public void setStoryId(int storyId) {
        mStoryId = storyId;
    }

    public String getBody() {
        return mBody;
    }

    public void setBody(String body) {
        mBody = body;
    }

    public Date getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(Date createdAt) {
        mCreatedAt = createdAt;
    }

    public Date getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        mUpdatedAt = updatedAt;
    }

    public Date getDeletedAt() {
        return mDeletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        mDeletedAt = deletedAt;
    }

    public String getAncestry() {
        return mAncestry;
    }

    public void setAncestry(String ancestry) {
        mAncestry = ancestry;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mId);
        dest.writeInt(this.mUserId);
        dest.writeInt(this.mStoryId);
        dest.writeString(this.mBody);
        dest.writeSerializable(this.mCreatedAt);
        dest.writeSerializable(this.mUpdatedAt);
        dest.writeSerializable(this.mDeletedAt);
        dest.writeString(this.mAncestry);
    }

    public void readFromParcel(Parcel source) {
        this.mId = source.readInt();
        this.mUserId = source.readInt();
        this.mStoryId = source.readInt();
        this.mBody = source.readString();

        this.mCreatedAt = (Date) source.readSerializable();
        this.mUpdatedAt = (Date) source.readSerializable();
        this.mDeletedAt = (Date) source.readSerializable();
        this.mAncestry = source.readString();
    }

    public static final Creator<Comment> CREATOR = new Creator<Comment>() {
        @Override
        public Comment createFromParcel(Parcel source) {
            return new Comment(source);
        }

        @Override
        public Comment[] newArray(int size) {
            return new Comment[size];
        }
    };

    public boolean isSameAs(Comment other) {
        return other != null && this.getId() == other.getId() && this.getBody().equals(other.getBody())
                && this.getCreatedAt() == other.getCreatedAt();
    }
}
