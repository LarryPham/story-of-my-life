/*
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 * @author: Larry Pham (email: larrypham.vn@gmail.com)
 */

package co.soml.android.app.controllers;

import co.soml.android.app.models.Comment;
import co.soml.android.app.models.Story;

public class StoryReaderActions {

    private StoryReaderActions() {
        throw new AssertionError();
    }

    /**
     * result when a specific action is performed (linking a story, etc..)
     */
    public interface ActionListener {
        public void onActionResult(boolean succeeded);
    }

    /**
     * result when submitting a comment
     */
    public interface CommentActionListener {
        public void onActionResult(boolean succeeded, Comment newComment);
    }

    /**
     * result when updating data (getting latest or comments for a story, etc)
     */
    public enum UpdateResult {
        HAS_NEW, CHANGED, UNCHANGED, FAILED;

        public boolean isNewOrChanged() {
            return (this == HAS_NEW || this == CHANGED);
        }
    }

    public interface UpdateResultListener {
        public void onUpdateResult(UpdateResult result);
    }

    public interface DataRequestedListener {
        public void onRequestedData();
    }

    public interface UpdateStoryInfoListener {
        public void onResult(Story story);
    }
}
