package co.soml.android.app.activities;

import android.os.Bundle;

import co.soml.android.app.StoryApp;
import co.soml.android.app.controllers.EditStoryController;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.utils.LogUtils;

public class EditStoryActivity extends BaseActivity {
	public static final String TAG = LogUtils.makeLogTag(EditStoryActivity.class.getSimpleName());
	public StoryApp mApp;
	public StoryDataModel mDataModel;
	public EditStoryController mController;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mApp = (StoryApp) this.getApplicationContext();
		mDataModel = mApp.getAppDataModel();
		mController = new EditStoryController(this, mDataModel);
	}

	@Override
	protected void invalidate() {

	}

	@Override
	protected void invalidate(Object... params) {

	}
}
