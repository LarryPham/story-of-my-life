/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 * @author: Larry Pham (email: larrypham.vn@gmail.com)
 * Date: 6/2/15 4:14 PM
 **/

package co.soml.android.app.activities;

import android.content.ComponentName;
import android.os.Bundle;
import android.support.v4.content.IntentCompat;
import android.support.v7.internal.widget.TintImageView;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import butterknife.Bind;
import butterknife.ButterKnife;
import co.soml.android.app.R;
import co.soml.android.app.StoryApp;
import co.soml.android.app.controllers.PublishStoryController;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.utils.LogUtils;
import co.soml.android.app.widgets.StoryKenBurnsView;
import co.soml.android.app.widgets.StoryTextView;

public class PublishStoryActivity extends BaseActivity {
    public static final String TAG = LogUtils.makeLogTag(PublishStoryActivity.class.getSimpleName());
    private StoryApp mStoryApp;
    private StoryDataModel mStoryDataModel;
    private PublishStoryController mController;

    @Bind(R.id.new_story_toolbar)
    protected Toolbar mNewStoryToolbar;
    @Bind(R.id.new_story_photos_preview)
    protected ImageView mPhotosPreview;

    @Bind(R.id.new_story_description_scrollview)
    protected ScrollView mNewStoryDescription;
    @Bind(R.id.new_story_input_title)
    protected EditText mInputTitleEditText;

    @Bind(R.id.new_story_photos)
    protected RelativeLayout mNewStoryPhotos;
    @Bind(R.id.new_story_songs)
    protected RelativeLayout mNewStorySongs;
    @Bind(R.id.new_story_intervals)
    protected RelativeLayout mNewStoryIntervals;

    @Bind(R.id.new_story_photos_caption)
    protected StoryTextView mPhotosCaption;
    @Bind(R.id.new_story_add_photos)
    protected ImageButton mAddPhotos;
    @Bind(R.id.new_story_intervals_edittext)
    protected EditText mStoryIntervals;

    @Bind(R.id.new_story_songs_caption)
    protected StoryTextView mSongsCaption;
    @Bind(R.id.new_story_add_songs)
    protected ImageButton mStoryAddSongs;

    @Bind(R.id.new_story_reminders_caption)
    protected StoryTextView mStoryRemindersCaption;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_story);
        ButterKnife.bind(this);

        mNewStoryToolbar.setTitle(getString(R.string.app_name));
        mNewStoryToolbar.setNavigationIcon(R.drawable.ic_back);
        mNewStoryToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateUpToFromChild(PublishStoryActivity.this, IntentCompat.makeMainActivity(new ComponentName(PublishStoryActivity
                        .this, StoryMainActivity.class)));

            }
        });
        setSupportActionBar(mNewStoryToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setElevation(0.15f);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        mStoryApp = (StoryApp) this.getApplicationContext();
        mStoryDataModel = mStoryApp.getAppDataModel();
        mController = new PublishStoryController(this, mStoryDataModel);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    @Override
    protected void invalidate() {

    }

    @Override
    protected void invalidate(Object... params) {

    }
}
