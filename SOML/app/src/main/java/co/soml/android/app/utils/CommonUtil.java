package co.soml.android.app.utils;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.imageaware.ImageViewAware;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.telephony.ServiceState;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Hashtable;

import co.soml.android.app.AppConstants;
import co.soml.android.app.R;
import co.soml.android.app.StoryApp;
import co.soml.android.app.models.Comment;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its
 * documentation are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable
 * form without the prior written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with
 * respect to the contents, and assumes no responsibility for any errors that might appear in the software and
 * documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.06.2015
 */
public class CommonUtil {
    private static final String TAG = AppConstants.PREFIX + CommonUtil.class.getSimpleName();
    private static final Hashtable<String, Typeface> mTypefaceCache = new Hashtable<String, Typeface>();
    public static final DisplayMetrics displayMetrics = new DisplayMetrics();

    private static final ThreadLocal<NumberFormat> mIntegerInstance = new ThreadLocal<NumberFormat>() {
        @Override
        protected NumberFormat initialValue() {
            return NumberFormat.getInstance();
        }
    };

    private static final ThreadLocal<DecimalFormat> mDecimalInstance = new ThreadLocal<DecimalFormat>() {
        @Override
        protected DecimalFormat initialValue() {
            return (DecimalFormat) DecimalFormat.getInstance();
        }
    };

    private DisplayMetrics mDisplayMetrics = null;

    private CommonUtil() {

    }

    public static int convertDpToPx(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return Math.round(dp * scale);
    }

    public static boolean hasJellyBean() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
    }

    public static boolean hasLollipop() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }

    public static int dp(int density, float value) {
        return (int)Math.ceil(density * value);
    }

    public static String getStatusMessageFromCode(Context context, int code) {
        String msg = null;
        switch (code) {
            case AppConstants.SUCCESSFULLY: {
                msg = context.getResources().getString(R.string.http_message_status_successfully);
                break;
            }
            case AppConstants.NOT_AUTHORIZED: {
                msg = context.getResources().getString(R.string.http_message_status_not_authorized);
                break;
            }
            case AppConstants.CREATED: {
                msg = context.getResources().getString(R.string.http_message_status_created);
                break;
            }
            case AppConstants.NO_CONTENT: {
                msg = context.getResources().getString(R.string.http_message_status_no_content);
                break;
            }
            case AppConstants.NOT_FOUND: {
                msg = context.getResources().getString(R.string.http_message_status_not_found);
                break;
            }
            case AppConstants.MISSING_PARAMS: {
                msg = context.getResources().getString(R.string.http_message_status_missing_parameters);
                break;
            }
            case AppConstants.FORBIDDEN: {
                msg = context.getResources().getString(R.string.http_message_status_forbidden);
                break;
            }
            default:
                break;
        }
        return msg;
    }

    public static Typeface getTypeface(final Context context, String assetPath) {
        synchronized (mTypefaceCache) {
            if (!mTypefaceCache.containsKey(assetPath)) {
                try {
                    Typeface typeface = Typeface.createFromAsset(context.getAssets(), assetPath);
                    mTypefaceCache.put(assetPath, typeface);
                } catch (Exception ex) {
                    Log.e(TAG, "Could not get typeface '" + assetPath + "' because '" + ex.getMessage());
                    return null;
                }
            }
            return mTypefaceCache.get(assetPath);
        }
    }

    public static boolean isNetworkOnline(Context context) {
        boolean status = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getNetworkInfo(0);
            if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED) {
                status = true;
            } else {
                netInfo = cm.getNetworkInfo(1);
                if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED) {
                    status = true;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        return status;
    }

    public static boolean isKeyboardShowed(View view) {
        if (view == null) {
            return false;
        }

        InputMethodManager inputManager = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        return inputManager.isActive(view);
    }

    /**
     * Method hideKeyboard() used to hide the displayed soft intput panel
     *
     * @param view The Current View.
     */
    public static void hideKeyboard(View view) {
        if (view == null) {
            return;
        }

        InputMethodManager inputMethodManager = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (!inputMethodManager.isActive()) {
            return;
        }

        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int dp2px(float value) {
        return (int) (0.5F + value * getDensity(StoryApp.getContext()));
    }

    public static int px2dp(float pxValue) {
        return (int) (pxValue / getDensity(StoryApp.getContext()) + 0.5f);
    }

    public static File getCacheDir(Context context) {
        String state = null;
        try {
            state = Environment.getExternalStorageState();
        } catch (Exception ex) {
            Log.e(TAG, String.format("Getting Status of ExternalStorage throws Exception: [%s]", ex));
        }
        if (state == null || state.startsWith(Environment.MEDIA_MOUNTED)) {
            try {
                File file = context.getExternalCacheDir();
                if (file != null) {
                    return file;
                }
            } catch (Exception ex) {
                Log.e(TAG, String.format("Getting the Cache's Directory throws exception: %s", ex.getMessage()));
            }
        }

        try {
            File file = context.getExternalCacheDir();
            if (file != null) {
                return file;
            }
        } catch (Exception ex) {
            Log.e(TAG, String.format("Getting Cache's Directory throws exception: %s", ex));
        }
        return new File("");
    }

    public static int getScreenWidth(Context context) {
        final WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        final Display display = wm.getDefaultDisplay();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            Point size = new Point();
            display.getSize(size);
            return size.x;
        }
        return display.getWidth();
    }

    public static void hideKeyboard(Activity activity) {
        if (activity != null && activity.getCurrentFocus() != null) {
            InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public static int getScreenHeight(Context context) {
        final WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        final Display display = wm.getDefaultDisplay();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            Point point = new Point();
            display.getSize(point);
            return point.y;
        }
        return display.getHeight();
    }

    public static String formatInt(int value) {
        return mIntegerInstance.get().format(value);
    }

    public static String formatDecimal(int value) {
        return mDecimalInstance.get().format(value);
    }

    public static long daysBetween(Calendar startDate, Calendar endDate) {
        Calendar date = (Calendar) startDate.clone();
        long daysBetween = 0;
        while (date.before(endDate)) {
            date.add(Calendar.DAY_OF_MONTH, 1);
            daysBetween++;
        }
        return daysBetween;
    }

    public static long daysBetweenEffective(final Calendar startDate, final Calendar endDate) {
        int MILLIS_IN_DAY = 1000 * 60 * 60 * 24;
        long endInstant = endDate.getTimeInMillis();
        int presumedDays = (int) ((endInstant - startDate.getTimeInMillis()) / MILLIS_IN_DAY);
        Calendar cursor = (Calendar) startDate.clone();
        cursor.add(Calendar.DAY_OF_YEAR, presumedDays);
        long instant = cursor.getTimeInMillis();
        if (instant == endInstant) {
            return presumedDays;
        }
        final int step = instant < endInstant ? 1 : -1;
        do {
            cursor.add(Calendar.DAY_OF_MONTH, step);
            presumedDays += step;
        } while (cursor.getTimeInMillis() != endInstant);
        return presumedDays;
    }

    public static <T> void execute(final boolean foreSeial, final AsyncTask<T, ?, ?> task, final T... args) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.DONUT) {
            throw new UnsupportedOperationException("This class can only be used on API 4 and above");
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB || foreSeial) {
            task.execute(args);
        } else {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, args);
        }
    }

    public static long getParentId(Comment comment) {
        if (comment == null) {
            return -1;
        }

        if (TextUtils.isEmpty(comment.getAncestry())) {
            return -1;
        }

        long[] outTemp;
        BufferedReader reader;
        try {
            reader = new BufferedReader(new StringReader(comment.getAncestry()));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] inputLine = line.split("\\|");
                outTemp = convertToLong(inputLine);
                if (outTemp != null && outTemp.length > 0) {
                    return outTemp[0];
                }
            }
        } catch (IOException ex) {
            LogUtils.LOGE(TAG, "Cannot Parse Comment's ParentId From Ancestry");
            ex.printStackTrace();
        }
        return -1;
    }

    public static boolean hasParentId(String ancestry) {

        if (TextUtils.isEmpty(ancestry)) {
            return false;
        }

        long[] outTemp;
        BufferedReader reader;

        try {
            reader = new BufferedReader(new StringReader(ancestry));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] inputLine = line.split("\\|");
                outTemp = convertToLong(inputLine);
                return outTemp != null && outTemp.length > 0;
            }
        }catch (IOException ex) {
            LogUtils.LOGE(TAG, "Cannot Parse Comment's ParentId From Ancestry");
            ex.printStackTrace();
        }
        return false;
    }

    public static long[] convertToLong(String[] str) {
        long[] output = new long[str.length];
        for (int index = 0; index < output.length; index++) {
            output[index] = Long.parseLong(str[index]);
        }
        return output;
    }

    public static int checkConnectivityMode(final Context context) {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        boolean isConnWifi = false;
        boolean isConn3G = false;
        boolean isConnWimax = false;

        NetworkInfo networkInfo = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (networkInfo != null && networkInfo.isConnected()) {
            isConnWifi = true;
        } else if (networkInfo == null) {
            LogUtils.LOGE(TAG, "checkConnectivityMode() - TYPE_WIFI NetworkInfo is null!!!");
        }

        networkInfo = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (networkInfo != null && networkInfo.isConnected()) {
            isConn3G = true;
        } else if (networkInfo == null){
            LogUtils.LOGE(TAG, "checkConnectivityMode() - TYPE_MOBILE NetworkInfo is null!!!");
        }

        networkInfo = manager.getNetworkInfo(ConnectivityManager.TYPE_WIMAX);
        if (networkInfo != null && networkInfo.isConnected()) {
            isConnWimax = true;
        } else if (networkInfo == null) {
            LogUtils.LOGE(TAG, "checkConnectivityMode() - TYPE_WIMAX NetworkInfo is null!!!");
        }

        networkInfo = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE_HIPRI);
        LogUtils.LOGD(TAG, String.format("checkConnectivityMode() - isWifi = %s, is3G = %s, isWimax = %s",
                String.valueOf(isConnWifi), String.valueOf(isConn3G), String.valueOf(isConnWimax)));

        if (isConnWifi) {
            return AppConstants.CONN_WIFI;
        } else if (isConn3G) {
            return AppConstants.CONN_3G;
        } else if (isConnWimax) {
            return AppConstants.CONN_3G;
        } else {
            return AppConstants.CONN_DISCONNECTED;
        }
    }

    public static int checkConnectionFailedCase(final Context context) {
        TelephonyManager telManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        int flightMode = Settings.System.getInt(context.getContentResolver(), Settings.Global.AIRPLANE_MODE_ON, 0);
        boolean isRoaming = telManager.isNetworkRoaming();
        boolean isMobileData = isMobileDataEnabled(context);

        int limitServiceStateValue = getLimitedServiceState(context);
        boolean isReachToDataLimit = isMobilePolicyDataEnable(context);
        boolean isDataRoamingValue = isDataRoaming(context);

        int networkErrorType = -1;
        if (flightMode == 1) {
            networkErrorType = 0;
        } else if (!isMobileData) {
            networkErrorType = 1;
        } else if (isRoaming && !isDataRoamingValue) {
            networkErrorType = 2;
        } else if (isRoaming) {
            networkErrorType = 7;
        } else if (!isReachToDataLimit) {
            networkErrorType = 3;
        } else if (limitServiceStateValue != ServiceState.STATE_IN_SERVICE) {
            networkErrorType = 4;
        } else {
            networkErrorType = 6;
        }
        LogUtils.LOGE(TAG, "NetworkErrorType's Value: " + networkErrorType);
        return networkErrorType;
    }

    public static boolean isRoaming(Context context) {
        boolean isRoaming = false;
        TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (manager != null  && manager.isNetworkRoaming()) {
            isRoaming = true;
        } else if (manager == null) {
            LogUtils.LOGW(TAG, "checkConnectivityMode() - TelephonyManager is null!!!");
        }
        return isRoaming;
    }

    public static boolean isMobileDataEnabled(Context context) {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        try {
            boolean result = false;
            Class<?> clazz = Class.forName(manager.getClass().getName());
            Method method = clazz.getDeclaredMethod("getMobileDataEnabled");
            method.setAccessible(true);
            LogUtils.LOGE(TAG, "Exist Method! - getMobileDataEnabled()");
            result = (Boolean)method.invoke(manager);
            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
            LogUtils.LOGE(TAG, "No such method! - getMobileEnabledData()");
            return false;
        }
    }

    public static int getLimitedServiceState(final Context context){
        TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        try {
            int result = -1;
            Class<?> clazz = Class.forName(manager.getClass().getName());
            Method method = clazz.getDeclaredMethod("getDataServiceState");
            method.setAccessible(true);
            LogUtils.LOGE(TAG, "Exist Method! - getDataSerivceState()");
            result = (Integer) method.invoke(manager);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.LOGE(TAG, "No such method! = getDataServiceState()");
            return -1;
        }
    }

    public static boolean isMobilePolicyDataEnable(Context context) {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        try {
            boolean result = false;
            Class<?> clazz = Class.forName(manager.getClass().getName());
            Method method = clazz.getDeclaredMethod(manager.getClass().getName());
            method.setAccessible(true);
            LogUtils.LOGE(TAG, "Exist method - isMobilePolicyEnable()");
            result = (Boolean) method.invoke(manager);
            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
            LogUtils.LOGE(TAG, "No Such Method! - isMobilePolicyEnable()");
            return false;
        }
    }

    public static boolean isDataRoaming(final Context context) {
        TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        try {
            boolean result = false;
            Class<?> clazz = Class.forName(manager.getClass().getName());
            Method method = clazz.getDeclaredMethod("getDataRoamingEnabled");
            method.setAccessible(true);
            LogUtils.LOGE(TAG, "Exist method! - getDataRoamingEnabled()");
            result = (Boolean) method.invoke(manager);
            return result;
        } catch (Exception ex) {
            ex.printStackTrace();
            LogUtils.LOGE(TAG, "No Such Method! - getDataRoamingEnabled()");
            return false;
        }
    }

    public static boolean checkDoNotShowConnectMobileNetworkDialogRoaming(Context context) {
        SharedPreferences pref = context.getSharedPreferences("GeneralSettings", Context.MODE_PRIVATE);
        return pref.getBoolean("DO_NOT_SHOW_MOBILE_NETWORK_DIALOG_ROAMING", false);
    }

    public static boolean checkDoNotShowConnectMobileNetworkDialog(Context context) {
        SharedPreferences pref = context.getSharedPreferences("GeneralSettings", Context.MODE_PRIVATE);
        return pref.getBoolean("DO_NOT_SHOW_MOBILE_NETWORK_DIALOG", false);
    }

    public static boolean checkEnableMobileNetwork(Context context) {
        boolean connectViaMobileNetwork = false;
        SharedPreferences preferences = context.getSharedPreferences("GeneralSettings", Context.MODE_PRIVATE);
        connectViaMobileNetwork = preferences.getBoolean("CONNECT_VIA_MOBILE_NETWORK", false);
        return connectViaMobileNetwork;
    }

    public static void setEnableMobileNetwork(final Context context, final boolean enabled) {
        SharedPreferences prefs = context.getSharedPreferences("GeneralSettings", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("CONNECT_VIA_MOBILE_NETWORK", enabled);
        editor.apply();
    }

	public static int getDensity(final Context context) {
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		DisplayMetrics metrics = new DisplayMetrics();
		wm.getDefaultDisplay().getMetrics(metrics);
		return metrics.densityDpi;
	}

    public void setDisplayMetrics(DisplayMetrics displayMetrics) {
        this.mDisplayMetrics = displayMetrics;
    }

    public float getScreenDensity() {
        if (this.mDisplayMetrics == null) {
            setDisplayMetrics(StoryApp.getContext().getResources().getDisplayMetrics());
        }
        return this.mDisplayMetrics.density;
    }

   /* public  int getScreenHeight() {
        if (this.mDisplayMetrics == null) {
            setDisplayMetrics(StoryApp.getContext().getResources().getDisplayMetrics());
        }
        return this.mDisplayMetrics.heightPixels;
    }*/

    public static int getScreenWidth() {
        final DisplayMetrics displayMetrics = StoryApp.getContext().getResources().getDisplayMetrics();
        return displayMetrics.widthPixels;
    }

    public static int getScreenHeight() {
        final DisplayMetrics displayMetrics = StoryApp.getContext().getResources().getDisplayMetrics();
        return displayMetrics.heightPixels;
    }

    public static void displayLocalImage(String uri, ImageView imageView, DisplayImageOptions options) {
        ImageLoader.getInstance().displayImage("file://" + uri, new ImageViewAware(imageView), options, null, null);
    }

    public static void displayDrawableImage(String uri, ImageView imageView, DisplayImageOptions options) {
        ImageLoader.getInstance().displayImage("drawable://" + uri, new ImageViewAware(imageView), options, null, null);
    }

    public static void runOnUIThread(Runnable runnable, long delay) {
        if (delay == 0) {
            StoryApp.getInstance().getAppMainHandler().post(runnable);
        } else {
            StoryApp.getInstance().getAppMainHandler().postDelayed(runnable, delay);
        }
    }

    public static void cancelRunOnUIThread(Runnable runnable) {
        StoryApp.getInstance().getAppMainHandler().removeCallbacks(runnable);
    }

    public static float getPixelsInCM(float cm, boolean isX) {
        return (cm / 2.54f) * (isX ? displayMetrics.xdpi : displayMetrics.ydpi);
    }
}

