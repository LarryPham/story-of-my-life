package co.soml.android.app.activities.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import co.soml.android.app.activities.BaseActivity;
import co.soml.android.app.fragments.BaseFragment;
import co.soml.android.app.fragments.NewUserFragment;
import co.soml.android.app.fragments.SignInFragment;
import co.soml.android.app.fragments.SocialSigninFragment;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.27.2015
 * <p>
 * A {@link android.support.v4.app.FragmentPagerAdapter} class for swiping between Sign-in, Social Sign-in,
 * Sign-up {@link android.support.v4.app.Fragment} for the Sessions Activity screen
 */
public class SessionsPagerAdapter extends FragmentPagerAdapter {
    public final SparseArray<WeakReference<BaseFragment>> mFragmentsArray = new SparseArray<WeakReference<BaseFragment>>();
    private final BaseActivity mFragmentActivity;
    private int mCurrentPage;
    private final List<SessionsHolder> mHolderList = new ArrayList<SessionsHolder>();

    /**
     * Constructor of <code>SessionsPagerAdapter</code>
     *
     * @param fragmentActivity The {@link co.soml.android.app.activities.BaseActivity} of the {@link co.soml.android.app.fragments.BaseFragment}
     */
    public SessionsPagerAdapter(final BaseActivity fragmentActivity) {
        super(fragmentActivity.getSupportFragmentManager());
        mFragmentActivity = fragmentActivity;
    }

    /**
     * Method that adds a new BaseFragment class to the viewer ( the fragment is internally instantiate)
     *
     * @param className The full qualified name of fragment class.
     * @param params    The instantiate params.
     */
    public void add(final Class<? extends BaseFragment> className, final Bundle params) {
        final SessionsHolder holder = new SessionsHolder();
        holder.mClassName = className.getName();
        holder.mParams = params;

        final int position = mHolderList.size();
        mHolderList.add(position, holder);
        notifyDataSetChanged();
    }

    /**
     * Method that returns the {@link co.soml.android.app.fragments.BaseFragment} in the argument position.
     *
     * @param position The position of the fragment to return.
     * @return BaseFragment The {@link co.soml.android.app.fragments.BaseFragment} in the argument position.
     */
    public BaseFragment getFragment(final int position) {
        final WeakReference<BaseFragment> mWeakFragment = mFragmentsArray.get(position);
        if (mWeakFragment != null && mWeakFragment.get() != null) {
            return mWeakFragment.get();
        }
        return getItem(position);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        final BaseFragment mFragment = (BaseFragment) super.instantiateItem(container, position);
        final WeakReference<BaseFragment> mWeakFragment = mFragmentsArray.get(position);
        if (mWeakFragment != null) {
            mWeakFragment.clear();
        }

        mFragmentsArray.put(position, new WeakReference<BaseFragment>(mFragment));
        return mFragment;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BaseFragment getItem(int position) {
        final SessionsHolder mCurrentHolder = mHolderList.get(position);
        final BaseFragment mFragment = (BaseFragment) Fragment.instantiate(mFragmentActivity, mCurrentHolder.mClassName, mCurrentHolder.mParams);
        return mFragment;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
        final WeakReference<BaseFragment> mWeakFragment = mFragmentsArray.get(position);
        if (mWeakFragment != null) {
            mWeakFragment.clear();
        }
    }

    @Override
    public int getCount() {
        return mHolderList.size();
    }

    /**
     * Method that returns the current page Position
     *
     * @return in The current page.
     */
    public int getCurrentPage() {
        return mCurrentPage;
    }

    /**
     * Method that returns the current page position.
     *
     * @param currentPage The current page.
     */
    protected void setCurrentPage(final int currentPage) {
    }

    /**
     * An enumeration of all the main fragments supported
     */
    public enum SessionsFragments {

        /**
         * The Social Fragment
         */
        SOCIAL(SocialSigninFragment.class),
        /**
         * The Sign-in Fragment
         */
        SIGNIN(SignInFragment.class),
        /**
         * The Sign-up Fragment
         */
        SIGNUP(NewUserFragment.class);

        private Class<? extends BaseFragment> mFragmentClass;

        /**
         * Constructor of <code>SessionsFragments</code>
         *
         * @param fragmentClass The fragment class
         */
        private SessionsFragments(final Class<? extends BaseFragment> fragmentClass) {
            mFragmentClass = fragmentClass;
        }

        /**
         * Method that returns the fragment class.
         *
         * @return Class<? extends BaseFragment> The fragment class.
         */
        public Class<? extends BaseFragment> getFragmentClass() {
            return mFragmentClass;
        }
    }

    /**
     * A private class with information about fragment initialization
     */
    private final static class SessionsHolder {
        String mClassName;
        Bundle mParams;
    }
}
