package co.soml.android.app.models;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import co.soml.android.app.AppConstants;
import co.soml.android.app.utils.Lists;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its
 * documentation are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable
 * form without the prior written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with
 * respect to the contents, and assumes no responsibility for any errors that might appear in the software and
 * documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.01.2015
 */
class ComparatorJStoryCreatedAt implements Comparator<Object> {
    @Override
    public int compare(Object lhs, Object rhs) {
        return ((Story) lhs).getCreatedAt().compareTo(((Story) rhs).getCreatedAt());
    }
}

class ComparatorJPhotoCreateAt implements Comparator<Object> {
    @Override
    public int compare(Object lhs, Object rhs) {
        return ((JsonPhoto) lhs).getCreatedAt().compareTo(((JsonPhoto) rhs).getCreatedAt());
    }
}

public class StoryDataModel extends BaseModel {
    private static final String TAG = AppConstants.PREFIX + StoryDataModel.class.getSimpleName();
    private final int mMaxStoryImageCount = 50;
    private final int mMaxRecycleImageCount = 10;
    private StoryFeedList mStoryFeedList;

    // Due to the scopes of routes, we have 7 type of scope: following, trending, featured, private, published, collaborated
    private StoryFeedList mTrendingStoryList;
    private StoryFeedList mFollowingStoryList;
    private StoryFeedList mFeaturedStoryList;
    private StoryFeedList mLatestStoryList;
    private StoryFeedList mMyStoryPrivateList;
    private StoryFeedList mMyStoryPublishedList;
    private StoryFeedList mMyStoryCollaboratedList;
    private StickerPackList mStickerPackList;
	private List<String> mSearchAutoCompletedList;
    private StoryUserList mUsers;

	private CommentList mComments;

    // Searching story-list
    private List<Object> mSearchStoryList;

    private User mCurrentUser;
    private User mAnonymousUser;

    private StoryUserList mFollowers;
    private StoryUserList mFollowingUsers;

    private List<Integer> mStoryPhotosOrder;
    private HashMap<String, List<Object>> mCategoryStoryListMap;
    private List<Story> mLastStoriesList;

    public boolean mMarketClickable = true;

    public enum ViewState {
        OPEN_STORY, SEARCH, MY_STORY, PEOPLE
    }

    public enum EmptyViewMessageType {
        LOADING, NO_CONTENT, NETWORK_ERROR, PERMISSION_ERROR, GENERIC_ERROR, NO_CONTENT_CUSTOM_DATE;

        public static EmptyViewMessageType getEnumFromString(String value) {
            for (EmptyViewMessageType id : values()) {
                if (id.name().equals(value)) {
                    return id;
                }
            }
            return NO_CONTENT;
        }
    }

    private ViewState mCurrentViewState;
    private ViewState mPreviousViewState;

    public StoryDataModel() {
        this.mTrendingStoryList = new StoryFeedList();
        this.mFeaturedStoryList = new StoryFeedList();
        this.mFollowingStoryList = new StoryFeedList();
        this.mLatestStoryList = new StoryFeedList();
        this.mMyStoryPrivateList = new StoryFeedList();
        this.mMyStoryPublishedList = new StoryFeedList();
        this.mMyStoryCollaboratedList = new StoryFeedList();
        this.mLastStoriesList = new ArrayList<Story>();
        this.mSearchStoryList = new ArrayList<Object>();
        this.mCategoryStoryListMap = new HashMap<String, List<Object>>();

        this.mFollowers = new StoryUserList();
        this.mFollowingUsers = new StoryUserList();
        this.mStickerPackList = new StickerPackList();

        this.mComments = new CommentList();
	    this.mSearchAutoCompletedList = Lists.newArrayList();
    }

    public void removeAll() {
        this.mTrendingStoryList.clear();
        this.mFeaturedStoryList.clear();
        this.mFollowingStoryList.clear();
        this.mLatestStoryList.clear();
        this.mMyStoryPrivateList.clear();
        this.mMyStoryPublishedList.clear();
        this.mMyStoryCollaboratedList.clear();
        this.mLastStoriesList.clear();
        this.mSearchStoryList.clear();
        this.mCategoryStoryListMap.clear();
		this.mSearchAutoCompletedList.clear();

        this.mFollowingUsers.clear();
        this.mFollowers.clear();
        this.mComments.clear();
        this.mStickerPackList.getStickerPacks().clear();
    }


    public void setMarketClickable(boolean marketClickable) {
        this.mMarketClickable = marketClickable;
    }

    public boolean isMarketClickable() {
        return this.mMarketClickable;
    }

    public void setCurrentViewState(ViewState currentViewState) {
        if (this.mCurrentViewState != currentViewState) {
            this.mPreviousViewState = this.mCurrentViewState;
        } else {
            this.mCurrentViewState = currentViewState;
        }
    }

    public ViewState getCurrentViewState() {
        return this.mCurrentViewState;
    }

    public ViewState getPreviousViewState() {
        return this.mPreviousViewState;
    }

    public StoryFeedList getTrendingStoryList() {
        return mTrendingStoryList;
    }

    public void setTrendingStoryList(StoryFeedList trendingStoryList) {
        mTrendingStoryList = trendingStoryList;
    }

    public StoryFeedList getFollowingStoryList() {
        return mFollowingStoryList;
    }

    public void setFollowingStoryList(StoryFeedList followingStoryList) {
        mFollowingStoryList = followingStoryList;
    }

    public User getAnonymousUser() {
        return mAnonymousUser;
    }

    public void setAnonymousUser(User anonymousUser) {
        if (mAnonymousUser != null) {
            if (mAnonymousUser.getId() != anonymousUser.getId()) {
                mAnonymousUser = new User(anonymousUser);
            }
        }
        mAnonymousUser = new User(anonymousUser);
    }

    public void setCurrentUser(User user) {
        if (mCurrentUser != null) {
            if (mCurrentUser.getId() != user.getId()) {
                mCurrentUser = new User(user);
            }
        }
        mCurrentUser = new User(user);
    }

    public User getCurrentUser() {
        return this.mCurrentUser;
    }

    public StoryUserList getUsers() {
        return mUsers;
    }

    public void setUsers(StoryUserList users) {
        this.mUsers = users;
    }

    public Story getStory(long storyId, StoryFeedList storyFeedList) {
        for (Story story : storyFeedList) {
            if (story.getId() == storyId) {
                return story;
            }
        }
        return null;
    }

    public List<Integer> getStoryPhotosOrder() {
        return mStoryPhotosOrder;
    }

    public void setStoryPhotosOrder(List<Integer> storyPhotosOrder) {
        mStoryPhotosOrder = storyPhotosOrder;
    }

    public HashMap<String, List<Object>> getCategoryStoryListMap() {
        return mCategoryStoryListMap;
    }

    public void setCategoryStoryListMap(HashMap<String, List<Object>> categoryStoryListMap) {
        mCategoryStoryListMap = categoryStoryListMap;
    }

    public List<Story> getLastStoriesList() {
        return mLastStoriesList;
    }

    public void setLastStoriesList(List<Story> lastStoriesList) {
        mLastStoriesList = lastStoriesList;
    }

    public void setPreviousViewState(ViewState previousViewState) {
        mPreviousViewState = previousViewState;
    }

    public StoryFeedList getFeaturedStoryList() {
        return mFeaturedStoryList;
    }

    public void setFeaturedStoryList(StoryFeedList featuredStoryList) {
        mFeaturedStoryList = featuredStoryList;
    }

    public List<Object> getSearchStoryList() {
        return mSearchStoryList;
    }

    public void setSearchStoryList(List<Object> searchStoryList) {
        mSearchStoryList = searchStoryList;
    }

    public StoryFeedList getMyStoryPrivateList() {
        return mMyStoryPrivateList;
    }

    public void setMyStoryPrivateList(StoryFeedList myStoryPrivateList) {
        this.mMyStoryPrivateList = myStoryPrivateList;
    }

    public StoryFeedList getMyStoryPublishedList() {
        return mMyStoryPublishedList;
    }

    public void setMyStoryPublishedList(StoryFeedList myStoryPublishedList) {
        this.mMyStoryPublishedList = myStoryPublishedList;
    }

    public StoryFeedList getMyStoryCollaboratedList() {
        return mMyStoryCollaboratedList;
    }

    public void setMyStoryCollaboratedList(StoryFeedList myStoryCollaboratedList) {
        this.mMyStoryCollaboratedList = myStoryCollaboratedList;
    }

    public StoryFeedList getLatestStoryList() {
        return mLatestStoryList;
    }

    public void setLatestStoryList(StoryFeedList latestStoryList) {
        mLatestStoryList = latestStoryList;
    }

    public StoryUserList getFollowers() {
        return mFollowers;
    }

    public void setFollowers(StoryUserList followers) {
        this.mFollowers = followers;
    }

    public StoryUserList getFollowingUsers() {
        return mFollowingUsers;
    }

    public void setFollowingUsers(StoryUserList followingUsers) {
        mFollowingUsers = followingUsers;
    }

    public CommentList getComments() {
        return mComments;
    }

    public void setComments(CommentList comments) {
        mComments = comments;
    }

	public List<String> getSearchAutoCompletedList() {
		return mSearchAutoCompletedList;
	}

	public void setSearchAutoCompletedList(List<String> searchAutoCompletedList) {
		this.mSearchAutoCompletedList = searchAutoCompletedList;
	}

    public void setStickerPackList(StickerPackList stickerPackList) {
        this.mStickerPackList = stickerPackList;
    }

    public StickerPackList getStickerPackList() {
        return this.mStickerPackList;
    }

    public enum UsersType {
        FOLLOWING, FOLLOWERS, SUGGESTION
    }

    public enum MainPagerType {
        HOME_MAIN_PAGE, SEARCH_MAIN_PAGE, MY_STORY_PAGE, SOCIAL_PAGE, SETTING_PAGE;

        public boolean isHomeMainPage() {
            return this.equals(HOME_MAIN_PAGE);
        }

        public boolean isSearchMainPage() {
            return this.equals(SEARCH_MAIN_PAGE);
        }

        public boolean isMyStoryPage() {
            return this.equals(MY_STORY_PAGE);
        }

        public boolean isSocialPage() {
            return this.equals(SOCIAL_PAGE);
        }

        public boolean isSettingsPage() {
            return this.equals(SETTING_PAGE);
        }
    }

	public enum ProfilePageType {
		STORIES_PROFILE, FOLLOWING_PROFILE, FOLLOWERS_PROFILE;

		public boolean isStoriesPage() {
			return this.equals(STORIES_PROFILE);
		}

		public boolean isFollowingPage() {
			return this.equals(FOLLOWING_PROFILE);
		}

		public boolean isFollowersPage() {
			return this.equals(FOLLOWERS_PROFILE);
		}
	}
}
