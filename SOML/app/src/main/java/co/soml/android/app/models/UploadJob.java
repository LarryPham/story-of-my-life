/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/14/15 2:19 PM.
 **/

package co.soml.android.app.models;

import android.os.Parcel;
import android.os.Parcelable;

import co.soml.android.app.utils.LogUtils;

public class UploadJob implements Parcelable {
    public static final String TAG = LogUtils.makeLogTag(UploadJob.class.getSimpleName());
    public static final int INVALID_ID = -1;
    private static int ID_GEN = -1;
    private int mId;
    private String mFromUri;
    private String mToUri;

    private int mProgressValue = 0;
    private Photo mPhoto;
    private Story mStory;
    private UploadState mState = UploadState.NONE;

    private long mUploadingSize = 0;
    private long mTotalUploadingSize = 0;

    public enum UploadState {
        NONE, CANCELLING, PAUSE, QUEUEING_JOB, UPLOADING_JOB
    }

    protected UploadJob(String fromUri, String toUri) {
        mId = ID_GEN++;
        mFromUri = fromUri;
        mToUri = toUri;
        mState = UploadState.NONE;
    }

    protected UploadJob(Parcel in) {
        readFromParcel(in);
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getFromUri() {
        return mFromUri;
    }

    public void setFromUri(String fromUri) {
        mFromUri = fromUri;
    }

    public String getToUri() {
        return mToUri;
    }

    public void setToUri(String toUri) {
        mToUri = toUri;
    }

    public int getProgressValue() {
        return mProgressValue;
    }

    public void setProgressValue(int progressValue) {
        mProgressValue = progressValue;
    }

    public Photo getPhoto() {
        return mPhoto;
    }

    public void setPhoto(Photo photo) {
        mPhoto = photo;
    }

    public Story getStory() {
        return mStory;
    }

    public void setStory(Story story) {
        mStory = story;
    }

    public UploadState getState() {
        return mState;
    }

    public void setState(UploadState state) {
        mState = state;
    }

    public long getUploadingSize() {
        return mUploadingSize;
    }

    public void setUploadingSize(long uploadingSize) {
        mUploadingSize = uploadingSize;
    }

    public long getTotalUploadingSize() {
        return mTotalUploadingSize;
    }

    public void setTotalUploadingSize(long totalUploadingSize) {
        mTotalUploadingSize = totalUploadingSize;
    }

    public static final Creator<UploadJob> CREATOR = new Creator<UploadJob>() {
        @Override
        public UploadJob createFromParcel(Parcel in) {
            return new UploadJob(in);
        }

        @Override
        public UploadJob[] newArray(int size) {
            return new UploadJob[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mId);
        dest.writeString(mFromUri);
        dest.writeString(mToUri);
        dest.writeLong(mUploadingSize);
    }

    protected void readFromParcel(Parcel in) {
        mId = in.readInt();
        mFromUri = in.readString();
        mToUri = in.readString();
        mUploadingSize = in.readLong();
    }
}
