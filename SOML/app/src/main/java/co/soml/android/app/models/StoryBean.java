package co.soml.android.app.models;

import android.content.SharedPreferences;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.FrameLayout;

import java.io.Serializable;

import co.soml.android.app.fragments.BaseFragment;
import co.soml.android.app.fragments.SearchHistoryFragment;
import co.soml.android.app.utils.LogUtils;

/**
 *  Copyright (C) 2015 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation
 *  are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied,
 *  reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior
 *  written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with respect to the contents, and
 *  assumes no responsibility for any errors that might appear in the software and documents. This publication and the
 *  contents hereof are subject to change without notice.
 *  @author Larry Pham(Email: larrypham.vn@gmail.com)
 *  @since 6/29/15 10:35 AM
 *
 * The <code>StoryBean</code> class represent to wrapp some recently attributes
 **/

public class StoryBean implements Serializable {
    public final static String TAG = LogUtils.makeLogTag(StoryBean.class.getSimpleName());

    public int mLastTabPosition = 0;
    public int mCurrentCheckPosition = 0;

    public boolean isStoryFeedsScreen = false;
    public boolean isSearchScreen = false;
    public boolean isPeopleScreen = false;
    public boolean isMyStoryScreen = false;
    public boolean isSettingsScreen = false;
    public boolean isProfileScreen = false;

    public String mSearchKeyword = "";
    public SearchView mSearchView = null;
    public FrameLayout mWaitFragment = null;

    public View mFeedsFrame = null;
    public View mMyStoryFrame = null;
    public View mPeopleFrame = null;
    public View mSearchFrame = null;

    public BaseFragment mCurrentTabFragment = null;
    public BaseFragment mCurrentFragment = null;

    public BaseFragment mStoryFeedsFragment = null;
    public BaseFragment mStoryFeedsDetailFragment = null;

    public BaseFragment mSearchFragment = null;
    public BaseFragment mSearchListFragment = null;
	public BaseFragment mSearchHistoryFragment = null;

    public BaseFragment mMyStoryFragment = null;
    public BaseFragment mMyStoryDetailFragment = null;

    public BaseFragment mPeopleFragment = null;

    public StoryDataModel mStoryDataModel = null;
    public Story mStory = null;

    public SharedPreferences.Editor mEditor = null;
    public SharedPreferences.Editor mViewByEditor = null;

    public SharedPreferences mPrefs = null;
    public SharedPreferences mViewByPrefs = null;

    public boolean mEnableMobileNetwork = false;
    public boolean mIsDialogState = false;

    public int mUpdateState = 0;

    public static StoryBean sInstance = null;

    public static StoryBean getInstance() {
        if (sInstance == null) {
            sInstance = new StoryBean();
        }
        return sInstance;
    }

    public StoryBean() {

    }

    public View getFeedsFrame() {
        return mFeedsFrame;
    }

    public void setFeedsFrame(View feedsFrame) {
        mFeedsFrame = feedsFrame;
    }

    public View getMyStoryFrame() {
        return mMyStoryFrame;
    }

    public void setMyStoryFrame(View myStoryFrame) {
        mMyStoryFrame = myStoryFrame;
    }

    public View getPeopleFrame() {
        return mPeopleFrame;
    }

    public void setPeopleFrame(View peopleFrame) {
        mPeopleFrame = peopleFrame;
    }

    public View getSearchFrame() {
        return mSearchFrame;
    }

    public void setSearchFrame(View searchFrame) {
        mSearchFrame = searchFrame;
    }

    public int getLastTabPosition() {
        return mLastTabPosition;
    }

    public void setLastTabPosition(int lastTabPosition) {
        mLastTabPosition = lastTabPosition;
    }

    public int getCurrentCheckPosition() {
        return mCurrentCheckPosition;
    }

    public void setCurrentCheckPosition(int currentCheckPosition) {
        mCurrentCheckPosition = currentCheckPosition;
    }

    public boolean isStoryFeedsScreen() {
        return isStoryFeedsScreen;
    }

    public void setIsStoryFeedsScreen(boolean isStoryFeedsScreen) {
        this.isStoryFeedsScreen = isStoryFeedsScreen;
    }

    public boolean isSearchScreen() {
        return isSearchScreen;
    }

    public void setIsSearchScreen(boolean isSearchScreen) {
        this.isSearchScreen = isSearchScreen;
    }

    public boolean isPeopleScreen() {
        return isPeopleScreen;
    }

    public void setIsPeopleScreen(boolean isPeopleScreen) {
        this.isPeopleScreen = isPeopleScreen;
    }

    public boolean isMyStoryScreen() {
        return isMyStoryScreen;
    }

    public void setIsMyStoryScreen(boolean isMyStoryScreen) {
        this.isMyStoryScreen = isMyStoryScreen;
    }

    public boolean isSettingsScreen() {
        return isSettingsScreen;
    }

    public void setIsSettingsScreen(boolean isSettingsScreen) {
        this.isSettingsScreen = isSettingsScreen;
    }

    public boolean isProfileScreen() {
        return isProfileScreen;
    }

    public void setIsProfileScreen(boolean isProfileScreen) {
        this.isProfileScreen = isProfileScreen;
    }

    public String getSearchKeyword() {
        return mSearchKeyword;
    }

    public void setSearchKeyword(String searchKeyword) {
        mSearchKeyword = searchKeyword;
    }

    public SearchView getSearchView() {
        return mSearchView;
    }

    public void setSearchView(SearchView searchView) {
        mSearchView = searchView;
    }

    public FrameLayout getWaitFragment() {
        return mWaitFragment;
    }

    public void setWaitFragment(FrameLayout waitFragment) {
        mWaitFragment = waitFragment;
    }

    public BaseFragment getCurrentTabFragment() {
        return mCurrentTabFragment;
    }

    public void setCurrentTabFragment(BaseFragment currentTabFragment) {
        mCurrentTabFragment = currentTabFragment;
    }

    public BaseFragment getCurrentFragment() {
        return mCurrentFragment;
    }

    public void setCurrentFragment(BaseFragment currentFragment) {
        mCurrentFragment = currentFragment;
    }

    public BaseFragment getStoryFeedsFragment() {
        return mStoryFeedsFragment;
    }

    public void setStoryFeedsFragment(BaseFragment storyFeedsFragment) {
        mStoryFeedsFragment = storyFeedsFragment;
    }

    public BaseFragment getStoryFeedsDetailFragment() {
        return mStoryFeedsDetailFragment;
    }

    public void setStoryFeedsDetailFragment(BaseFragment storyFeedsDetailFragment) {
        mStoryFeedsDetailFragment = storyFeedsDetailFragment;
    }

    public BaseFragment getSearchFragment() {
        return mSearchFragment;
    }

    public void setSearchFragment(BaseFragment searchFragment) {
        mSearchFragment = searchFragment;
    }

    public BaseFragment getSearchListFragment() {
        return mSearchListFragment;
    }

    public void setSearchListFragment(BaseFragment searchListFragment) {
        mSearchListFragment = searchListFragment;
    }

	public BaseFragment getSearchHistoryFragment() {
		return mSearchHistoryFragment;
	}

	public void setSearchHistoryFragment(SearchHistoryFragment fragment) {
		this.mSearchHistoryFragment = fragment;
	}

    public BaseFragment getMyStoryFragment() {
        return mMyStoryFragment;
    }

    public void setMyStoryFragment(BaseFragment myStoryFragment) {
        mMyStoryFragment = myStoryFragment;
    }

    public BaseFragment getMyStoryDetailFragment() {
        return mMyStoryDetailFragment;
    }

    public void setMyStoryDetailFragment(BaseFragment myStoryDetailFragment) {
        mMyStoryDetailFragment = myStoryDetailFragment;
    }

    public BaseFragment getPeopleFragment() {
        return mPeopleFragment;
    }

    public void setPeopleFragment(BaseFragment peopleFragment) {
        mPeopleFragment = peopleFragment;
    }

    public StoryDataModel getStoryDataModel() {
        return mStoryDataModel;
    }

    public void setStoryDataModel(StoryDataModel storyDataModel) {
        mStoryDataModel = storyDataModel;
    }

    public Story getStory() {
        return mStory;
    }

    public void setStory(Story story) {
        mStory = story;
    }

    public SharedPreferences.Editor getEditor() {
        return mEditor;
    }

    public void setEditor(SharedPreferences.Editor editor) {
        mEditor = editor;
    }

    public SharedPreferences.Editor getViewByEditor() {
        return mViewByEditor;
    }

    public void setViewByEditor(SharedPreferences.Editor viewByEditor) {
        mViewByEditor = viewByEditor;
    }

    public SharedPreferences getPrefs() {
        return mPrefs;
    }

    public void setPrefs(SharedPreferences prefs) {
        mPrefs = prefs;
    }

    public SharedPreferences getViewByPrefs() {
        return mViewByPrefs;
    }

    public void setViewByPrefs(SharedPreferences viewByPrefs) {
        mViewByPrefs = viewByPrefs;
    }

    public boolean isEnableMobileNetwork() {
        return mEnableMobileNetwork;
    }

    public void setEnableMobileNetwork(boolean enableMobileNetwork) {
        mEnableMobileNetwork = enableMobileNetwork;
    }

    public boolean isDialogState() {
        return mIsDialogState;
    }

    public void setIsDialogState(boolean isDialogState) {
        mIsDialogState = isDialogState;
    }

    public int getUpdateState() {
        return mUpdateState;
    }

    public void setUpdateState(int updateState) {
        mUpdateState = updateState;
    }
}
