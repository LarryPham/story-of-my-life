/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/20/15 8:31 AM.
 **/

package co.soml.android.app.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.Date;

import co.soml.android.app.utils.LogUtils;

public class Song implements Parcelable, Serializable {

    public final static String TAG = LogUtils.makeLogTag(Song.class.getSimpleName());
    private long mSongId;
    private String mTitle;
    private String mArtist;
    private int mLength;
    private Date mCreatedAt;
    private Date mUpdatedAt;
    private String mCover;
    private File mFile;

    public long getSongId() {
        return mSongId;
    }

    public void setSongId(long songId) {
        this.mSongId = songId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getArtist() {
        return mArtist;
    }

    public void setArtist(String artist) {
        mArtist = artist;
    }

    public int getLength() {
        return mLength;
    }

    public void setLength(int length) {
        mLength = length;
    }

    public Date getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(Date createdAt) {
        mCreatedAt = createdAt;
    }

    public Date getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        mUpdatedAt = updatedAt;
    }

    public String getCover() {
        return mCover;
    }

    public void setCover(String cover) {
        mCover = cover;
    }

    public File getFile() {
        return mFile;
    }

    public void setFile(File file) {
        mFile = file;
    }

    public Song() {

    }

    public Song(Song other) {
        this.mSongId = other.mSongId;
        this.mTitle = other.mTitle;
        this.mArtist = other.mArtist;
        this.mCover = other.mCover;
        this.mLength = other.mLength;
        this.mCreatedAt = other.mCreatedAt;
        this.mUpdatedAt = other.mUpdatedAt;
        this.mFile = other.mFile;
    }

    public Song(Parcel in) {
        readFromParcel(in);
    }

    protected void readFromParcel(Parcel source) {
        this.mSongId = source.readInt();
        this.mTitle = source.readString();
        this.mArtist = source.readString();
        this.mLength = source.readInt();

        this.mCreatedAt = (Date) source.readSerializable();
        this.mUpdatedAt = (Date) source.readSerializable();
        this.mCover =  source.readString();
        this.mFile = (File) source.readSerializable();
    }

    public static final Creator<Song> CREATOR = new Creator<Song>() {
        @Override
        public Song createFromParcel(Parcel in) {
            return new Song(in);
        }

        @Override
        public Song[] newArray(int size) {
            return new Song[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.mSongId);
        dest.writeString(this.mTitle);
        dest.writeString(this.mArtist);
        dest.writeInt(this.mLength);
        dest.writeSerializable(mCreatedAt);
        dest.writeSerializable(mUpdatedAt);
        dest.writeString(mCover);
        dest.writeSerializable(mFile);
    }

    public String getDuration() {
        int minutes = this.mLength % 60;
        int seconds = (this.mLength - minutes) * 60;
        return String.format("%2d:%2d", minutes, seconds);
    }
}
