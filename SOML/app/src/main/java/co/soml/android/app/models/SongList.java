/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/20/15 8:38 AM.
 **/

package co.soml.android.app.models;

import android.text.TextUtils;

import java.util.ArrayList;

import co.soml.android.app.utils.LogUtils;

public class SongList extends ArrayList<Song> {

    public ArrayList<Long> getSongsIds() {
        ArrayList<Long> ids= new ArrayList<>();
        for (Song song : this) {
            ids.add(song.getSongId());
        }
        return ids;
    }

    public Song getSongAtIndex(int index) {
        if (index >= 0 && index < this.size()) {
            return this.get(index);
        }
        return null;
    }

    public String getTitleForSongId(long songId) {
        if (songId == 0) {
            return null;
        }
        for (Song song : this) {
            if (song.getSongId() >= 0 && song.getSongId() == songId) {
                return song.getTitle();
            }
        }
        return null;
    }

    public String getUrlStringForSongId(long songId) {
        if (songId == 0) {
            return null;
        }

        for (Song song: this) {
            if (song.getSongId() > 0 && song.getSongId() == songId && song.getFile() != null) {
                return song.getFile().getUri();
            }
        }
        return null;
    }

    public int getSongsCount() {
        return size();
    }

    public int indexOfSong(Song song) {
        if (song == null) {
            return -1;
        }

        for (int index = 0; index < size(); index++) {
            if (this.get(index).getSongId() == song.getSongId()) {
                return index;
            }
        }
        return -1;
    }

    public boolean isSameList(ArrayList<Integer> compareIds) {
        if (compareIds == null || compareIds.size() != this.size()) {
            return false;
        }
        return this.containsAll(compareIds);
    }
}
