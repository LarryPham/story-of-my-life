package co.soml.android.app.activities;

import android.view.View;

import co.soml.android.app.models.Story;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since May.26.2015
 * <p>
 * The <code>ReaderStoryInterfaces</code> interface which represent to define the methods for other commont components which used it for
 * handling the UI and component events
 **/
public class ReaderStoryInterfaces {

    public static interface OnStorySelectedListener {
        public void onStorySelected(int mStoryId);
    }

    /**
     * Called from Story Detail fragment so toolbar can animate  in/out when scrolling
     */
    public static interface AutoHideToolbarListener {
        public void onShowHideToolbar(boolean show);
    }

    /**
     * used by adapters to notify when data has been loaded
     */
    public interface DataLoadedListener {
        public void onDataLoaded(boolean isEmpty);
    }

    public interface OnStoryPopupListener {
        public void onShowPopup(View view, Story story);
    }

    /**
     * Used by story list & story list adapter when user asks to edit a story
     */
    public interface RequestReStoryListener {
        public void onRequestReStory(Story story, View sourceView);
    }

}
