package co.soml.android.app.services;

import com.google.gson.annotations.Expose;

import co.soml.android.app.AppConstants;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its
 * documentation are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable
 * form without the prior written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with
 * respect to the contents, and assumes no responsibility for any errors that might appear in the software and
 * documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.03.2015
 */
public class BaseResult {
    public static final String TAG = AppConstants.PREFIX + BaseResult.class.getSimpleName();
    // from the specification
    @Expose
    protected int mResultCode = 0;
    @Expose
    protected int mResultSize = 0;
    @Expose
    protected int mResultIndex = 0;
    @Expose
    protected int mResultTotal = 0;
    @Expose
    protected String mException = null;
    @Expose
    protected String mResultMessage = null;
    @Expose
    protected int mErrorCode = 0;

    public BaseResult() {
    }

    public int getResultCode() {
        return mResultCode;
    }

    public void setResultCode(int resultCode) {
        mResultCode = resultCode;
    }

    public int getResultSize() {
        return mResultSize;
    }

    public void setResultSize(int resultSize) {
        mResultSize = resultSize;
    }

    public int getResultIndex() {
        return mResultIndex;
    }

    public void setResultIndex(int resultIndex) {
        mResultIndex = resultIndex;
    }

    public int getResultTotal() {
        return mResultTotal;
    }

    public void setResultTotal(int resultTotal) {
        mResultTotal = resultTotal;
    }

    public String getResultMessage() {
        return mResultMessage;
    }

    public void setResultMessage(String resultMessage) {
        mResultMessage = resultMessage;
    }

    public int getErrorCode() {
        return mErrorCode;
    }

    public void setErrorCode(int errorCode) {
        mErrorCode = errorCode;
    }

    public String getException() {
        return mException;
    }

    public void setException(String exception) {
        mException = exception;
    }
}
