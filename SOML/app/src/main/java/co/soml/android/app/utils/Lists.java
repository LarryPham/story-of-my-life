package co.soml.android.app.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since May.28.2015
 * <p>
 * Provides static methods for creating {@code List} instance easily, and other utility methods for working with lists.
 */
public final class Lists {

    /**
     * This class is never instantiated
     */
    public Lists() {

    }

    /**
     * Creates an empty {@code ArrayList } instance.
     * Note: <p>If you need an immutable empty List, use {@link java.util.Collections#emptyList}</p>
     *
     * @return a newly-created, initialy-empty {@code ArrayList}
     */
    public static <E> ArrayList<E> newArrayList() {
        return new ArrayList<E>();
    }

    /**
     * Creates an empty {@code LinkedList} instance.
     * Note: <p>If you only need an <i>immutable</i> empty List, use {@link Collections#emptyList()}</p>
     *
     * @return a newly-created, initialy-empty {@code ArrayList}
     */
    public static <E> LinkedList<E> newLinkedList() {
        return new LinkedList<E>();
    }
}
