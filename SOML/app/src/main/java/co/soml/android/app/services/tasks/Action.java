package co.soml.android.app.services.tasks;

import android.os.Parcel;
import android.os.Parcelable;

import co.soml.android.app.AppConstants;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its
 * documentation are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable
 * form without the prior written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with
 * respect to the contents, and assumes no responsibility for any errors that might appear in the software and
 * documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.01.2015
 */
public class Action implements Parcelable {
    private static final String TAG = AppConstants.PREFIX + Action.class.getSimpleName();


    public static final String REQUEST_TO_SERVER_NOTICE_LIST = "co.soml.android.app.Action.REQUEST_TO_SERVER_NOTICE_LIST";
    public static final String ACTION_PREFIX_NAME = "co.soml.android.app.Action";

    public static final String SOCIAL_NETWORK_ACCESS_TOKEN = "co.soml.android.app.Action.SOCIAL_ACCESS_TOKEN";

    public static final String FABRIC_ID = "co.soml.android.app.Action.FABRIC_ID";
    public static final String FABRIC_AUTH_TOKEN = "co.soml.android.app.Action.FABRIC_AUTH_TOKEN";
    public static final String FABRIC_AUTH_TOKEN_SECRET = "co.soml.android.app.Action.FABRIC_AUTH_TOKEN_SECRET";

    // Params which has been added into header of request
    public static final String X_AUTH_TOKEN = "co.soml.android.app.Action.X_AUTH_TOKEN";
    public static final String X_API_VERSION = "co.soml.android.app.Action.X_API_VERSION";

    public static final String REQUEST_OWNER = "co.soml.android.app.Action.REQUEST_OWNER";
    public static final String REQUEST_MSG = "co.soml.android.app.Action.REQUEST_MESSAGE";
    public static final String LOAD_FEED_STORY_PAGE = "co.soml.android.app.Action.LOAD_FEED_STORY_PAGE";
    public static final String LOAD_STORY_PAGE = "co.soml.android.app.Action.LOAD_STORY_PAGE";

    public static final String PARSE_FEED_STORY = "co.soml.android.app.Action.PARSE_FEED_STORY";
    public static final String PARSE_FEED_STORY_CANCEL = "co.soml.android.app.Action.PARSE_FEED_STORY_CANCEL";
    public static final String LOADING_STORY_IMAGE = "co.soml.android.app.Action.LOADING_STORY_IMAGE";

    public static final String DOWNLOAD_CONTENT = "co.soml.android.app.Action.DOWNLOAD_CONTENT";
    public static final String DOWNLOAD_CONTENT_ALL = "co.soml.android.app.Action.DOWNLOAD_CONTENT_ALL";
    public static final String DOWNLOAD_CONTENT_CANCEL = "co.soml.android.app.Action.DOWNLOAD_CONTENT_CANCEL";
    public static final String DOWNLOAD_CONTENT_ALL_CANCEL = "co.soml.android.app.Action.DOWNLOAD_CONTENT_ALL_CANCEL";
    public static final String ACTION_EXIT = "co.soml.android.app.Action.ACTION_EXIT";
    public static final String RESUME_DOWNLOAD = "co.soml.android.app.Action.RESUME_DOWNLOAD";

    public static final String REQUEST_TO_SERVER_CANCEL = "co.soml.android.app.Action.REQUEST_TO_SERVER_CANCEL";
    public static final String REQUEST_IMAGE_FROM_SERVER = "co.soml.android.app.Action.REQUEST_IMAGE_FROM_SERVER";
    public static final String REQUEST_IMAGE_FROM_SERVER_CANCEL = "co.soml.android.app.Action.REQUEST_IMAGE_FROM_SERVER_CANCEL";
    public static final String AUTO_UPDATE_LIST_STORY = "co.soml.android.app.Action.AUTO_UPDATE_LIST_STORY";
    // Post method for update, creating, deleting stories
    public static final String REQUEST_TO_SERVER_REPORT_STORIES = "co.soml.android.app.Action.REQUEST_TO_SERVER_REPORT_STORIES";
    public static final String REQUEST_TO_SERVER_SUBSCRIBE_STORY = "co.soml.android.app.Action.REQUEST_TO_SERVER_SUBSCRIBE_STORY";
    public static final String REQUEST_TO_SERVER_UNSUBSCRIBE_STORY = "co.soml.android.app.Action.REQUEST_TO_SERVER_UNSUBSCRIBE_STORY";
    public static final String REQUEST_TO_SERVER_CREATE_STORIES = "co.soml.android.app.Action.REQUEST_TO_SERVER_CREATE_STORIES";
    public static final String REQUEST_TO_SERVER_UPDATE_STORY = "co.soml.android.app.Action.REQUEST_TO_SERVER_UPDATE_STORY";
    public static final String REQUEST_TO_SERVER_DELETE_STORY = "co.soml.android.app.Action.REQUEST_TO_SERVER_DELETE_STORY";
    // Post method for modify order, upload, update description, remove a photo
    public static final String REQUEST_TO_SERVER_MODIFY_PHOTO_ORDER = "co.soml.android.app.Action.REQUEST_TO_SERVER_MODIFY_PHOTO_ORDER";
    public static final String REQUEST_TO_SERVER_UPLOAD_PHOTO = "co.soml.android.app.Action.REQUEST_TO_SERVER_UPLOAD_PHOTO";
    // Path method for update description of photos
    public static final String REQUEST_TO_SERVER_UPDATE_PHOTO_DESCRIPTION =
            "co.soml.android.app.Action.REQUEST_TO_SERVER_UPDATE_PHOTO_DESCRIPTION";
    public static final String REQUEST_TO_SERVER_DELETE_PHOTO = "co.soml.android.app.Action.REQUEST_TO_SERVER_DELETE_PHOTO";
    // Post methods for replying comment, give comment, report a comment
    public static final String REQUEST_TO_SERVER_REPLY_COMMENT = "co.soml.android.app.Action.REQUEST_TO_SERVER_REPLY_COMMENT";
    public static final String REQUEST_TO_SERVER_GIVE_COMMENT = "co.soml.android.app.Action.REQUEST_TO_SERVER_GIVE_COMMENT";
    // Path methods for update comment
    public static final String REQUEST_TO_SERVER_UPDATE_COMMENT = "co.soml.android.app.Action.REQUEST_TO_SERVER_UPDATE_COMMENT";
    public static final String REQUEST_TO_SERVER_REPORT_COMMENT = "co.soml.android.app.Action.REQUEST_TO_SERVER_REPORT_COMMENT";
    public static final String REQUEST_TO_SERVER_REPORT_USER = "co.soml.android.app.Action.REQUEST_TO_SERVER_REPORT_USER";
    public static final String REQUEST_TO_SERVER_FOLLOW_USER = "co.soml.android.app.Action.REQUEST_TO_SERVER_FOLLOW_USER";

    // Post methods for account management
    public static final String REQUEST_TO_SERVER_ACCOUNT_REGISTER = "co.soml.android.app.Action.REQUEST_TO_SERVER_ACCOUNT_REGISTER";
    public static final String REQUEST_TO_SERVER_ACCOUNT_FOLLOWING = "co.soml.android.app.Action.REQUEST_TO_SERVER_ACCOUNT_FOLLOWING";
    public static final String REQUEST_TO_SERVER_ACCOUNT_LIKED = "co.soml.android.app.Action.REQUEST_TO_SERVER_ACCOUNT_LIKED";
    public static final String REQUEST_TO_SERVER_ACCOUNT_NEW = "co.soml.android.app.Action.REQUEST_TO_SERVER_ACCOUNT_NEW";
    // Post methods for social network sessions
    public static final String REQUEST_TO_SERVER_AUTHENTICATE_FACEBOOK = "co.soml.android.app.Action.REQUEST_TO_SERVER_AUTHENTICATE_FACEBOOK";
    public static final String REQUEST_TO_SERVER_AUTHENTICATE_EMAIL_ACCOUNT = "co.soml.android.app.Action"
            + ".REQUEST_TO_SERVER_AUTHENTICATE_EMAIL_ACCOUNT";
    public static final String REQUEST_TO_SERVER_AUTHENTICATE_TWITTER = "co.soml.android.app.Action.REQUEST_TO_SERVER_AUTHENTICATE_TWITTER";
    public static final String REQUEST_TO_SERVER_AUTHENTICATE_GUEST = "co.soml.android.app.Action.REQUEST_TO_SERVER_SIGN_IN_GUEST";
    public static final String FINISH_APPLICATION = "com.soml.android.app.Action.FINISH_APPLICATION";
    // Actions for story management
    public static final String REQUEST_TO_SERVER_STORY_LATEST = "com.soml.android.app.Action.REQUEST_TO_SERVER_STORY_LATEST";
    public static final String REQUEST_TO_SERVER_STORY_FEATURED = "com.soml.android.app.Action.REQUEST_TO_SERVER_STORY_FEATURED";
    public static final String REQUEST_TO_SERVER_STORY_FOLLOWING = "com.soml.android.app.Action.REQUEST_TO_SERVER_STORY_FOLLOWING";
    public static final String REQUEST_TO_SERVER_STORY_COLLABORATING = "com.soml.android.app.Action.REQUEST_TO_SERVER_STORY_COLLABORATING";

    public static final String REQUEST_TO_SERVER_MY_STORY = "com.soml.android.app.Action.REQUEST_TO_SERVER_MY_STORY";
    public static final String REQUEST_TO_SERVER_MY_STORY_PRIVATE = "com.soml.android.app.Action.REQUEST_TO_SERVER_MY_STORY_PRIVATE";
    public static final String REQUEST_TO_SERVER_MY_STORY_PUBLISHED = "com.soml.android.app.Action.REQUEST_TO_SERVER_MY_STORY_PUBLISHED";
    public static final String REQUEST_TO_SERVER_MY_STORY_COLLABORATING = "com.soml.android.app.Action.REQUEST_TO_SERVER_MY_STORY_COLLABORATING";

    public static final String REQUEST_TO_SERVER_STORY_REPORT = "com.soml.android.app.Action.REQUEST_TO_SERVER_STORY_REPORT";
    public static final String REQUEST_TO_SERVER_STORY_LIKE = "com.soml.android.app.Action.REQUEST_TO_SERVER_STORY_LIKE";
    public static final String REQUEST_TO_SERVER_STORY_UNLIKE = "com.soml.android.app.Action.REQUEST_TO_SERVER_STORY_UNLIKE";
    public static final String REQUEST_TO_SERVER_STORY_POST = "com.soml.android.app.Action.REQUEST_TO_SERVER_STORY_POST";
    public static final String REQUEST_TO_SERVER_STORY_DETAIL = "com.soml.android.app.Action.REQUEST_TO_SERVER_STORY_DETAIL";
    public static final String REQUEST_TO_SERVER_STORY_UPDATE = "com.soml.android.app.Action.REQUEST_TO_SERVER_STORY_UPDATE";
    public static final String REQUEST_TO_SERVER_STORY_DELETE = "com.soml.android.app.Action.REQUEST_TO_SERVER_STORY_DELETE";
    // Actions for user management
    public static final String REQUEST_TO_SERVER_USER_FOLLOWERS = "com.soml.android.app.Action.REQUEST_TO_SERVER_USER_FOLLOWERS";
    public static final String REQUEST_TO_SERVER_USER_FOLLOW = "com.soml.android.app.Action.REQUEST_TO_SERVER_USER_FOLLOW";
    public static final String REQUEST_TO_SERVER_USER_UNFOLLOW = "com.soml.android.app.Action.REQUEST_TO_SERVER_USER_UNFOLLOW";
    public static final String REQUEST_TO_SERVER_USER_FOLLOWING = "com.soml.android.app.Action.REQUEST_TO_SERVER_USER_FOLLOWING";
    public static final String REQUEST_TO_SERVER_USER_REPORT = "com.soml.android.app.Action.REQUEST_TO_SERVER_USER_REPORT";
    public static final String REQUEST_TO_SERVER_USER_LIST = "com.soml.android.app.Action.REQUEST_TO_SERVER_USER_LIST";
    public static final String REQUEST_TO_SERVER_USER_DETAIL = "com.soml.android.app.Action.REQUEST_TO_SERVER_USER_DETAIL";
    // Actions for comment management
    public static final String REQUEST_TO_SERVER_COMMENT_REPLY = "com.soml.android.app.Action.REQUEST_TO_SERVER_COMMENT_REPLY";
    public static final String REQUEST_TO_SERVER_COMMENT_LIKE = "com.soml.android.app.Action.REQUEST_TO_SERVER_COMMENT_LIKE";
    public static final String REQUEST_TO_SERVER_COMMENT_UNLIKE = "com.soml.android.app.Action.REQUEST_TO_SERVER_COMMENT_UNLIKE";
    public static final String REQUEST_TO_SERVER_COMMENT_STORY = "com.soml.android.app.Action.REQUEST_TO_SERVER_COMMENT_STORY";
    public static final String REQUEST_TO_SERVER_COMMENT_UPDATE = "com.soml.android.app.Action.REQUEST_TO_SERVER_COMMENT_UPDATE";
    public static final String REQUEST_TO_SERVER_COMMENT_DELETE = "com.soml.android.app.Action.REQUEST_TO_SERVER_COMMENT_DELETE";
    public static final String REQUEST_TO_SERVER_COMMENT_REPORT = "com.soml.android.app.Action.REQUEST_TO_SERVER_COMMENT_REPORT";


    public static final String USER_FULL_NAME = "com.soml.android.app.Action.USER_FULL_NAME";
    public static final String USER_FOLLOWED = "com.soml.android.app.Action.USER_FOLLOWED";
    public static final String USER_EMAIL_ADDRESS = "com.soml.android.app.Action.USER_EMAIL_ADDRESS";
    public static final String USER_PASSWORD = "com.soml.android.app.Action.USER_PASSWORD";
    public static final String USER_PASSWORD_CONFIRMATION = "com.soml.android.app.Action.USER_PASSWORD_CONFIRMATION";
    public static final String USER_DESCRIPTION = "com.soml.android.app.Action.USER_DESCRIPTION";
    public static final String USER_ID = "com.soml.android.app.Action.USER_ID";

    public static final String STORY_ID = "com.soml.android.app.Action.STORY_ID";
    public static final String STORY_NAME = "com.soml.android.app.Action.STORY_NAME";
    public static final String STORY_INTERVAL = "com.soml.android.app.Action.STORY_INTERVAL";
    public static final String STORY_DESCRIPTION = "com.soml.android.app.Action.STORY_DESCRIPTION";
    public static final String STORY_LATITUDE = "com.soml.android.app.Action.STORY_LATITUDE";
    public static final String STORY_LONGITUDE = "com.soml.android.app.Action.STORY_LONGITUDE";
    public static final String STORY_LOCATION = "com.soml.android.app.Action.STORY_LOCATION";
    public static final String STORY_SCOPE = "com.soml.android.app.Action.STORY_SCOPE";
    public static final String STORY_STATE = "com.soml.android.app.Action.STATE";

    public static final String COMMENT_ID = "com.soml.android.app.Action.COMMENT_ID";
    public static final String COMMENT_BODY = "com.soml.android.app.Action.COMMENT_BODY";

    public static final String KEYWORD = "com.soml.android.app.Action.KEYWORD";
    public static final String START_INDEX = "com.soml.android.app.Action.START_INDEX";
    public static final String LIMIT = "com.soml.android.app.Action.LIMIT";
    public static final String OFFSET = "com.soml.android.app.Action.OFFSET";
    public static final String REASON = "com.soml.android.app.Action.REASON";
    public static final String MESSAGE = "com.soml.android.app.Action.MESSAGE";
	public static final String SHOW_PROGRESS = "com.soml.android.app.Action.SHOW_PROGRESS";
	public static final String DISMISS = "com.soml.android.app.Action.DISMISS";
	public static final String REQUEST_TO_SET_ALARM_FOR_AUTO_UPDATE = "com.soml.android.app.REQUEST_TO_SET_ALARM_AUTO_UPDATE";
    public static final String UPLOADING_CONTENT = "com.soml.android.app.UPLOADING_CONTENT";

    protected int mId;
    protected String mCode;
    protected Object mObject;

    public Action(String code, int id, Object object) {
        mCode = code;
        mId = id;
        mObject = object;
    }

    public Action(Parcel source) {
        readFromParcel(source);
    }

    public int ID() {
        return mId;
    }

    public String Code() {
        return mCode;
    }

    public Object obj() {
        return mObject;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mId);
        dest.writeString(mCode);
        dest.writeValue(mObject);
    }

    public void readFromParcel(Parcel source) {
        mId = source.readInt();
        mCode = source.readString();
        mObject = source.readValue(Object.class.getClassLoader());
    }

    public static Creator<Action> CREATOR = new Creator<Action>() {
        @Override
        public Action createFromParcel(Parcel source) {
            return new Action(source);
        }

        @Override
        public Action[] newArray(int size) {
            return new Action[size];
        }
    };
}
