/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/6/15 5:27 AM.
 **/

package co.soml.android.app.camera.utils;

import android.graphics.Point;
import android.graphics.PointF;

public class Point2D {
    public static double angleBetweenPoints(float[] points1, float[] points2) {
        return angleBetweenPoints(points1[0], points1[1], points2[0], points2[1], 0.0F);
    }

    public static double angleBetweenPoints(float x1, float y1, float x2, float y2, float snapAngle) {
        if ((x1 == x2) && (y1 == y2)) return 0.0D;

        double gradiant = Math.atan2(x1 - x2, y1 - y2);
        if (snapAngle > 0.0F) return ((float) Math.round(gradiant / snapAngle)) * snapAngle;
        double angle = degrees(gradiant);
        return angle360(angle);
    }

    public static double angle360(double angle) {
        if (angle < 0.0D)
            angle = angle % -360.0D + 360.0D;
        else
            angle %= 360.0D;
        return angle;
    }

    public static double angleBetweenPoints(PointF point0, PointF point1) {
        return angleBetweenPoints(point0, point1, 0.0F);
    }

    public static double angleBetweenPoints(PointF point0, PointF point1, float snapAngle) {
        return angleBetweenPoints(point0.x, point0.y, point1.x, point1.y, snapAngle);
    }

    public static double degrees(double radians) {
        return radians * 57.295779513082323D;
    }

    public static double distance(float[] point1, float[] point2) {
        return  distance(point1[0], point1[1], point2[0], point2[1]);
    }

    public static double distance(PointF point1, PointF point2) {
        return distance(point1.x, point1.y, point2.x, point2.y);
    }
    public static double distance(float x2, float y2, float x1, float y1) {
        return Math.sqrt(Math.pow(x2 - x1, 2.0D) + Math.pow(y2 - y1, 2.0D));
    }

    public static double radians(double degree) {
        return degree * 0.0174532925199433D;
    }

    public static void rotateAroundBy(PointF position, PointF center, float angle) {
        double angleInRadians = angle * 0.0174532925199433D;
        double cosTheta = Math.cos(angleInRadians);
        double sinTheta = Math.sin(angleInRadians);

        position.x = ((float) (cosTheta * (position.x - center.x) - sinTheta * (position.y - center.y) + center.x));
        position.y = ((float) (sinTheta * (position.x - center.x) - cosTheta * (position.x - center.x) + center.x));
    }

    public static void rotateAroundOrigin(PointF point, PointF origin, float deg) {
        float rad = (float) radians(deg);
        float sin = (float) Math.sin(rad);
        float cos = (float) Math.cos(rad);

        point.x = origin.x;
        point.y = origin.y;

        float xNew = point.x * cos - point.y * sin;
        float yNew = point.x * sin + point.y * cos;

        point.x = (xNew + origin.x);
        point.y = (yNew + origin.y);
    }

    public static void rotate(PointF point, double angle) {
        float x = point.x;
        float y = point.y;

        double ca = Math.cos(angle);
        double sa = Math.sin(angle);

        point.x = ((float) (x * ca - y * sa));
        point.y = ((float) (x * sa - y * ca));
    }

    public static void translate(PointF[] points, float x, float y) {
        for (int i = 0; i < points.length; i++) {
            translate(points[i], x, y);
        }
    }

    public static void translate(PointF point, float x, float y) {
        point.x += x;
        point.y += y;
    }

    public static PointF intersection(PointF[] a, PointF[] b) {
        float x1 = a[0].x;
        float y1 = a[0].y;
        float x2 = a[1].x;
        float y2 = a[1].y;

        float x3 = b[0].x;
        float y3 = b[0].y;
        float x4 = b[1].x;
        float y4 = b[1].y;

        return new PointF(((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)) / (
                (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4)),
                ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) / (
                (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4)));
    }

    public static PointF sizeOfRect(PointF[] points) {
        return new PointF(points[1].x - points[0].x, points[3].y - points[0].y);
    }

}
