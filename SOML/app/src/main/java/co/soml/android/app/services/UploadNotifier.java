/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/18/15 10:35 AM.
 **/

package co.soml.android.app.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.IntentCompat;

import java.util.Random;

import co.soml.android.app.R;
import co.soml.android.app.activities.StoryDetailActivity;
import co.soml.android.app.activities.StoryMainActivity;
import co.soml.android.app.utils.LogUtils;

public class UploadNotifier {
    protected static final String TAG = UploadNotifier.class.getSimpleName();

    private final NotificationManager mNotificationManager;
    private final NotificationCompat.Builder mNotificationBuilder;
    private final int mNotificationId;

    private int mNotificationErrorId = 0;
    private int mTotalMediaItems;
    private int mCurrentMediaItem;
    private float mItemProgressSize;

    protected Context mContext;

    protected int mItemId;
    protected String mItemKind;
    protected StoryUploadService mUploadService;

    public UploadNotifier(Context context) {
        mContext = context;
        mNotificationManager = (NotificationManager) SystemServiceFactory.get(mContext, Context.NOTIFICATION_SERVICE);

        mNotificationBuilder = new NotificationCompat.Builder(mContext.getApplicationContext());
        mNotificationBuilder.setSmallIcon(android.R.drawable.stat_sys_upload);
        /** Building the notification's intent from notification bar from device **/
        Intent notificationIntent = new Intent(mContext, StoryMainActivity.class);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
        notificationIntent.setAction(Intent.ACTION_MAIN);
        notificationIntent.addCategory(Intent.CATEGORY_LAUNCHER);

        notificationIntent.setData(Uri.parse("custom://somlNotificationIntent" + mItemKind + mItemId));
        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mNotificationBuilder.setContentIntent(pendingIntent);
        mNotificationId = (new Random()).nextInt() + mItemId;
        mUploadService.startForeground(mNotificationId, mNotificationBuilder.build());
    }

    public void setItemKind(String itemKind) {
        mItemKind = itemKind;
    }

    public void setItemId(int id) {
        mItemId = id;
    }

    public void setUploadService(StoryUploadService service) {
        this.mUploadService = service;
    }

    public void updateNotificationMessage(String title, String message) {
        if (title != null) {
            mNotificationBuilder.setContentTitle(title);
        }

        if (message != null) {
            mNotificationBuilder.setContentText(message);
        }

        mNotificationManager.notify(mNotificationId, mNotificationBuilder.build());
    }

    public void updateNotificationIcon(Bitmap icon) {
        if (icon != null) {
            mNotificationBuilder.setLargeIcon(icon);
        }

        mNotificationManager.notify(mNotificationId, mNotificationBuilder.build());
    }

    public void cancelNotification() {
        mNotificationManager.cancel(mNotificationId);
    }

    public void updateNotificationWithError(String errorMessage, boolean isMediaError, boolean isStory) {
        LogUtils.LOGD(TAG, "updateNotificationError: " + errorMessage);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(mContext);
        String storyOrPhoto = (String) (isStory ? mContext.getResources().getText(R.string.story_id) : mContext.getResources().getText(R
                .string.photo_id));

        Intent notificationIntent = new Intent(mContext, isStory ? StoryMainActivity.class : StoryDetailActivity.class);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationIntent.setAction(Intent.ACTION_MAIN);
        notificationIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        String errorText = mContext.getResources().getText(R.string.upload_failed_message).toString();
        if (isMediaError) {
            errorText = mContext.getResources().getText(R.string.photo_id) + " " + mContext.getResources().getText(R.string.error);
        }

        notificationBuilder.setSmallIcon(android.R.drawable.stat_notify_error);
        notificationBuilder.setContentTitle(isMediaError ? errorText : mContext.getResources().getText(R.string.upload_failed_message));
        mNotificationBuilder.setContentText((isMediaError) ? errorMessage : storyOrPhoto + " " + errorText + " : " + errorMessage);
        mNotificationBuilder.setAutoCancel(true);

        if (mNotificationErrorId == 0) {
            mNotificationErrorId = mNotificationId + (new Random()).nextInt();
        }
        mNotificationManager.notify(mNotificationErrorId, mNotificationBuilder.build());
    }

    public void updateNotificationProgress(float progress) {
        if (mTotalMediaItems == 0) {
            return;
        }

        double currentChunkProgress = (mItemProgressSize * progress) / 100;
        if (mCurrentMediaItem > 1) {
            currentChunkProgress += mItemProgressSize * (mCurrentMediaItem - 1);
        }

        mNotificationBuilder.setProgress(100, (int)Math.ceil(currentChunkProgress), false);
        mNotificationManager.notify(mNotificationId, mNotificationBuilder.build());
    }

    public void setTotalMediaItems(int totalMediaItems) {
        if (totalMediaItems <= 0) {
            totalMediaItems = 1;
        }

        mTotalMediaItems = totalMediaItems;
        mItemProgressSize = 100.0f / mTotalMediaItems;
    }

    public void setCurrentMediaItem(int currentItem) {
        mCurrentMediaItem = currentItem;
        mNotificationBuilder.setContentText(String.format("Uploading %1$d of %2$d", mCurrentMediaItem, mTotalMediaItems));
    }
}
