/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 * @author: Larry Pham (email: larrypham.vn@gmail.com)
 * Date: 6/8/15 4:58 PM
 **/

package co.soml.android.app.utils;

import android.content.Context;
import android.text.format.DateUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import co.soml.android.app.R;
import co.soml.android.app.StoryApp;

public class DateTimeUtils {

    public StoryApp mStoryApp;

    private DateTimeUtils() {
        throw new AssertionError();
    }

    /**
     * An instance for formatting the UTC-Date via ISO-8601 Format ("yyyy-MM-dd'T'HH:mm:ssZ")
     */
    private static final ThreadLocal<DateFormat> mISO8601Format = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US);
        }
    };

    public static String dateToTimeSpan(final Context context, final Date date) {
        if (date == null) {
            return "";
        }
        // Getting the milliseconds time of passed date
        long passedTime = date.getTime();
        // Getting the current time with milliseconds
        long currentTime = System.currentTimeMillis();

        // Convert the range of the subtraction of current with passed time
        long secondSince = (currentTime - passedTime) / 1000;
        if (secondSince < 60) {
            return context.getString(R.string.story_timespan_now);
        }
        // Calculate the minutes since before current time
        long minutesSince = secondSince / 60;
        if (minutesSince < 60) {
            return Long.toString(minutesSince) + context.getString(R.string.minutes_time_format_long);
        }
        // Calculate the since hours
        long hoursSince = minutesSince / 60;
        if (hoursSince < 60) {
            return Long.toString(hoursSince) + context.getString(R.string.hours_time_format_long);
        }
        // Calculate the days since
        long daysSince = hoursSince / 24;
        if (daysSince < 7) {
            return Long.toString(daysSince) + context.getString(R.string.days_time_format_long);
        }

        if (daysSince < 365) {
            return DateUtils.formatDateTime(context, passedTime, DateUtils.FORMAT_NO_YEAR | DateUtils.FORMAT_ABBREV_ALL);
        }
        return DateUtils.formatDateTime(context, passedTime, DateUtils.FORMAT_ABBREV_ALL);
    }

    /**
     * Converts the date string from the ISO-8601 Format to Java DateTime format.
     *
     * @param strDate The date's string
     * @return Date DateTime Type. The java date-time format
     */
    public static Date convertFromIso8601ToJavaDate(final String strDate) {
        try {
            DateFormat formatter = mISO8601Format.get();
            return formatter.parse(strDate);
        } catch (ParseException ex) {
            return null;
        }
    }

    /**
     * Converts the java-time date to the iso-8601 datetime's string.
     *
     * @param date Java Date Type.
     * @return String type.
     */
    public static String convertFromJavaDateToIso8601(Date date) {
        if (date == null) {
            return "";
        }
        DateFormat formatter = mISO8601Format.get();
        return formatter.format(date);
    }

    /**
     * Returns current UTC date
     */
    public static Date nowUTC() {
        Date dateTimeNow = new Date();
        return localDateToUTC(dateTimeNow);
    }

    public static Date localDateToUTC(Date localDate) {
        if (localDate == null) {
            return null;
        }
        // Getting default TimeZone's instance
        final TimeZone timeZone = TimeZone.getDefault();
        int currentOffsetFromUTC = timeZone.getRawOffset() + (timeZone.inDaylightTime(localDate) ? timeZone.getDSTSavings() : 0);
        return new Date(localDate.getTime() - currentOffsetFromUTC);
    }

    public static int minutesBetween(Date firstDate, Date secondDate) {
        long minsDiff = millisSecondsBetween(firstDate, secondDate);
        if (minsDiff == 0) {
            return 0;
        }
        return (int) (minsDiff / 60000);
    }

    public static int secondsBetween(Date firstDate, Date secondDate) {
        long secondsDiff = millisSecondsBetween(firstDate, secondDate);
        if (secondsDiff == 0) {
            return 0;
        }
        return (int) (secondsDiff / 1000);
    }

    public static long millisSecondsBetween(Date firstDate, Date secondDate) {
        if (firstDate == null || secondDate == null) {
            return 0;
        }

        return Math.abs(firstDate.getTime() - secondDate.getTime());
    }

    public static long convertIso8601ToTimeStamp(final String strDate) {
        Date date = convertFromIso8601ToJavaDate(strDate);
        if (date == null) {
            return 0;
        }
        return (date.getTime() / 1000);
    }

    /**
     * Routines involving UNIX TimeStamp (GMT assumed)
     *
     * @param timeStamp
     * @return
     */
    public static Date timeStampToDate(long timeStamp) {
        return new java.util.Date(timeStamp * 1000);
    }

    public static String timeStampToIso8601(long timeStamp) {
        return convertFromJavaDateToIso8601(timeStampToDate(timeStamp));
    }

    public static String timeStampToTimeSpan(long timeSpan) {
        Date gmtDate = timeStampToDate(timeSpan);
        return convertFromJavaDateToIso8601(gmtDate);
    }

    public static String javaDateToTimeSpan(final Date date) {
        if (date == null)
            return "";

        long passedTime = date.getTime();
        long currentTime = System.currentTimeMillis();

        // return "now" if less than a minute has elapsed
        long secondsSince = (currentTime - passedTime) / 1000;
        if (secondsSince < 60)
            return StoryApp.getContext().getString(R.string.story_timespan_now);

        // less than an hour (ex: 12m)
        long minutesSince = secondsSince / 60;
        if (minutesSince < 60)
            return Long.toString(minutesSince) + "m";

        // less than a day (ex: 17h)
        long hoursSince = minutesSince / 60;
        if (hoursSince < 24)
            return Long.toString(hoursSince) + "h";

        // less than a week (ex: 5d)
        long daysSince = hoursSince / 24;
        if (daysSince < 7)
            return Long.toString(daysSince) + "d";

        // less than a year old, so return day/month without year (ex: Jan 30)
        if (daysSince < 365)
            return DateUtils.formatDateTime(StoryApp.getContext(), passedTime, DateUtils.FORMAT_NO_YEAR | DateUtils.FORMAT_ABBREV_ALL);

        // date is older, so include year (ex: Jan 30, 2013)
        return DateUtils.formatDateTime(StoryApp.getContext(), passedTime, DateUtils.FORMAT_ABBREV_ALL);
    }

}
