package co.soml.android.app.widgets;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import co.soml.android.app.R;
import co.soml.android.app.utils.LogUtils;

/**
 *  Copyright (C) 2015 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation
 *  are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied,
 *  reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior
 *  written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with respect to the contents, and
 *  assumes no responsibility for any errors that might appear in the software and documents. This publication and the
 *  contents hereof are subject to change without notice.
 *  @author Larry Pham(Email: larrypham.vn@gmail.com)
 *  @since 6/26/15 2:45 PM
 *
 *  This class is the default empty state view for most ListView, RecyclerView/ Fragments.
 *  It allows the ability to set a main text, a main highlight text and a secondary text
 *  By default this container has some strings loaded, but other classes can all the apis to chang the text
 **/

public class NoResultContainer extends LinearLayout {
    public static final String TAG = LogUtils.makeLogTag(NoResultContainer.class.getSimpleName());

    public StoryTextView mEmptyContent;
    public StoryTextView mEmptyDescription;
    public ImageView mEmptyStatusImage;
    protected View mRootView;

    public NoResultContainer(Context context) {
        super(context);
        init(context, null);
    }

    public NoResultContainer(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public NoResultContainer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    protected void init(Context context, AttributeSet attrs) {
        mEmptyContent = (StoryTextView) findViewById(R.id.title_empty);
        mEmptyDescription = (StoryTextView) findViewById(R.id.description_empty);
        mEmptyStatusImage = (ImageView) findViewById(R.id.inbox_empty_imageview);
    }

    public void setEmptyContent(final int resId) {
        if (resId == 0) {
            mEmptyContent.setVisibility(View.GONE);
        } else {
            mEmptyContent.setVisibility(View.VISIBLE);
            mEmptyContent.setText(resId);
        }
    }

    public void setEmptyDescription(final String text){
        if (TextUtils.isEmpty(text)) {
            mEmptyDescription.setVisibility(View.GONE);
        } else {
            mEmptyDescription.setText(text);
            mEmptyDescription.setVisibility(View.VISIBLE);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void setEmptyStatusImage(final int resId) {
        if (resId == 0) {
            mEmptyStatusImage.setVisibility(View.GONE);
        } else {
            mEmptyStatusImage.setVisibility(View.VISIBLE);
            mEmptyStatusImage.setImageDrawable(this.getContext().getDrawable(resId));
        }
    }

    public void setEmptyStatusImage(final Drawable resDrawable) {
        if (resDrawable == null) {
            mEmptyStatusImage.setVisibility(View.GONE);
        } else {
            mEmptyStatusImage.setVisibility(View.VISIBLE);
            mEmptyStatusImage.setImageDrawable(resDrawable);
        }
    }
}
