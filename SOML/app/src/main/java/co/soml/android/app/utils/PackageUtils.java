package co.soml.android.app.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import co.soml.android.app.BuildConfig;

public class PackageUtils {
	/**
	 * Returns true if Debug build. false otherwise.
	 */
	public static boolean isDebugBuild() {
		return BuildConfig.DEBUG;
	}

	public static PackageInfo getPackageInfo(final Context context) {
		try {
			PackageManager manager = context.getPackageManager();
			return manager.getPackageInfo(context.getPackageName(), 0);
		} catch (PackageManager.NameNotFoundException ex) {
			return null;
		}
	}

	/**
	 * Returns version code, or 0 if it can't be read
	 */
	public static int getVersionCode(final Context context) {
		PackageInfo packageInfo = getPackageInfo(context);
		if (packageInfo != null) {
			return packageInfo.versionCode;
		}
		return 0;
	}

	/**
	 * Returns version name, or the string "0" if it can't be read
	 */
	public static String getVersionName(Context context) {
		PackageInfo packageInfo = getPackageInfo(context);
		if (packageInfo != null) {
			return packageInfo.versionName;
		}
		return "0";
	}
}
