/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 * @author: Larry Pham (email: larrypham.vn@gmail.com)
 * Date: 6/17/15 11:45 AM
 **/

package co.soml.android.app.controllers;

import android.os.Message;

import co.soml.android.app.StoryApp;
import co.soml.android.app.activities.BaseActivity;
import co.soml.android.app.activities.StoryDetailActivity;
import co.soml.android.app.activities.StoryDetailActivity;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.utils.LogUtils;

public class StoryDetailController extends BaseController {

    public static final String TAG = LogUtils.makeLogTag(StoryDetailController.class.getSimpleName());

    public StoryApp mApp;
    public StoryDataModel mDataModel;
    public StoryDetailActivity mActivity;

    public StoryDetailController(BaseActivity activity, StoryDataModel dataModel) {
        super(activity, dataModel);
        this.mActivity = (StoryDetailActivity) activity;
        this.mDataModel = dataModel;
        this.mApp = (StoryApp) activity.getApplicationContext();
    }

    public StoryDetailController(BaseActivity activity, StoryDataModel dataModel, boolean isFirstRunningMode) {
        super(activity, dataModel, isFirstRunningMode);
        this.mActivity = (StoryDetailActivity) activity;
        this.mDataModel = dataModel;
        this.mApp = (StoryApp) activity.getApplicationContext();
    }

    @Override
    protected void handleMessage(Message msg) {
        final String fn = "handleMessage(): ";
        switch (msg.what) {
            case ControllerMessage.REQUEST_TO_SERVER_STORY_DETAIL: {
                LogUtils.LOGD(TAG, String.format("Request to display the story's details "));

            }
        }
    }
}
