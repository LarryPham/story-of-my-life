/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/11/15 9:26 AM.
 **/

package co.soml.android.app.camera;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

import co.soml.android.app.R;
import co.soml.android.app.camera.effect.FilterEffect;
import co.soml.android.app.camera.utils.GPUImageFilterTools;
import co.soml.android.app.widgets.StoryTextView;
import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageView;

public class ImageFilterAdapter extends BaseAdapter {

    protected List<FilterEffect> mFilterUris;
    protected Context mContext;
    private Bitmap mBackground;
    private int mSelectFilter = 0;

    public int getSelectFilter() {
        return mSelectFilter;
    }

    public void setSelectFilter(int selectFilter) {
        mSelectFilter = selectFilter;
    }

    public ImageFilterAdapter(Context context, List<FilterEffect> effects, Bitmap background) {
        mFilterUris = effects;
        mContext = context;
        this.mBackground = background;
    }

    @Override
    public int getCount() {
        return mFilterUris.size();
    }

    @Override
    public Object getItem(int position) {
        return mFilterUris.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        EffectHolder holder = null;
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.item_bottom_tool, null);
            holder = new EffectHolder();
            holder.mFilteredImageView = (GPUImageView) convertView.findViewById(R.id.small_filter);
            holder.mFilterName = (StoryTextView) convertView.findViewById(R.id.filter_name);
            convertView.setTag(holder);
        } else {
            holder = (EffectHolder) convertView.getTag();
        }

        final FilterEffect effect = (FilterEffect) getItem(position);
        holder.mFilteredImageView.setImage(mBackground);
        holder.mFilterName.setText(effect.getTitle());
        GPUImageFilter filter = GPUImageFilterTools.createFilterForType(mContext, effect.getType());
        holder.mFilteredImageView.setFilter(filter);

        return convertView;
    }

    class EffectHolder {
        GPUImageView mFilteredImageView;
        StoryTextView mFilterName;
    }
}
