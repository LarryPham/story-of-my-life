/**
 * Copyright (C)  2015 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 * Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 * @author: Larry Pham (email: larrypham.vn@gmail.com)
 * @date: 7/15/15 8:46 AM
 **/

package co.soml.android.app.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import co.soml.android.app.R;
import co.soml.android.app.StoryApp;
import co.soml.android.app.activities.StoryMainActivity;
import co.soml.android.app.activities.UserProfileActivity;
import co.soml.android.app.controllers.ControllerMessage;
import co.soml.android.app.controllers.PeopleController;
import co.soml.android.app.controllers.UserProfileController;
import co.soml.android.app.models.StoryBean;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.utils.LogUtils;

public class ProfileFollowersFragment extends BaseFragment {
	public static final String TAG = LogUtils.makeLogTag(ProfileFollowersFragment.class.getSimpleName());

	private UserProfileController mController;
	private StoryDataModel mDataModel;
	private UserProfileActivity mActivity;
	private StoryApp mApp;

	private View mRootView;

	private static ProfileFollowersFragment sInstance = new ProfileFollowersFragment();
	public static StoryBean mStoryBean;

	public static ProfileFollowersFragment newInstance() {
		LogUtils.LOGD(TAG, "newInstance() ");
		sInstance = new ProfileFollowersFragment();
		return sInstance;
	}

	public static ProfileFollowersFragment getInstance() {
		if (sInstance == null) {
			sInstance = new ProfileFollowersFragment();
		}
		return sInstance;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		this.mActivity = (UserProfileActivity) this.getActivity();
		this.mApp = (StoryApp) mActivity.getApplication();
		this.mDataModel = mApp.getAppDataModel();
		this.mController = new UserProfileController(mActivity, this, mDataModel);
		mStoryBean = StoryMainActivity.getBean();
		mStoryBean.setStoryDataModel(mDataModel);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		LogUtils.LOGD(TAG, "onCreateView() ");
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		LogUtils.LOGD(TAG, "onActivityCreated() ");
		if (mController == null) {
			mController = new UserProfileController(mActivity, this, mDataModel);
		}
		mStoryBean = StoryMainActivity.getBean();
	}

	@Override
	public void onResume() {
		super.onResume();
		LogUtils.LOGD(TAG, "onResume");
		if (mController == null) {
			mController = new UserProfileController(mActivity, this, mDataModel);
		}
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		LogUtils.LOGD(TAG, "onDestroyView()");
		if (mController != null) {
			mController.onDestroy();
			mController = null;
		}

	}

	@Override
	public void invalidate() {

	}

	@Override
	public void invalidate(Object... params) {

	}
}
