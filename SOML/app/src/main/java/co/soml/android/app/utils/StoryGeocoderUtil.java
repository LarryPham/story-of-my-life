/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 * @author: Larry Pham (email: larrypham.vn@gmail.com)
 * Date: 6/9/15 3:06 PM
 **/

package co.soml.android.app.utils;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

class StoryGeoCoderUtil {
    public static final String TAG = LogUtils.makeLogTag(StoryGeoCoderUtil.class.getSimpleName());

    private StoryGeoCoderUtil() {
        throw new AssertionError();
    }

    public static Geocoder getGeoCoder(Context context) {
        if (!Geocoder.isPresent()) {
            return null;
        }

        Geocoder geocoder;
        try {
            geocoder = new Geocoder(context, Locale.getDefault());
        } catch (NullPointerException cannotInstantiateEx) {
            LogUtils.LOGE(TAG, String.format("Throwing exception: %s", cannotInstantiateEx.getCause()));
            return null;
        }
        return geocoder;
    }

    public static Address getAddressFromCoords(Context context, double latitude, double longitude) {
        Address address = null;
        List<Address> addressList = null;
        Geocoder geocoder = getGeoCoder(context);

        if (geocoder == null) {
            return null;
        }

        try {
            addressList = geocoder.getFromLocation(latitude, longitude, 1);
        } catch (IOException e) {
            e.printStackTrace();
            LogUtils.LOGD(TAG, String.format("Unable to parse response from server. Is geocoder server hitting the sever two frequently: %s", e));
        }

        if (addressList != null && addressList.size() > 0) {
            address = addressList.get(0);
        }
        return address;
    }

    public static Address getAddressFromLocationName(Context context, String locationName) {
        int maxResult = 1;
        Address address = null;
        List<Address> addressList = null;

        Geocoder geocoder = getGeoCoder(context);
        if (geocoder == null) {
            return null;
        }

        try {
            addressList = geocoder.getFromLocationName(locationName, maxResult);
        } catch (IOException e) {
            e.printStackTrace();
            LogUtils.LOGE(TAG, String.format("Failed to get coordinate from location: %s", e.getCause()));
        }

        if (addressList != null && addressList.size() > 0) {
            address = addressList.get(0);
        }
        return address;
    }

    public static String getLocationNameFromAddress(Address address) {
        String locality = "", adminArea = "", country = "";
        if (address.getLocality() != null) {
            locality = address.getLocality();
        }

        if (address.getAdminArea() != null) {
            adminArea = address.getAdminArea();
        }

        if (address.getCountryName() != null) {
            country = address.getCountryName();
        }

        return ((locality.equals("")) ? locality : locality + ",") + ((adminArea.equals("")) ? adminArea : adminArea + " ") + country;
    }

    public static double[] getCoordsFromAddress(Address address) {
        double[] coordinates = new double[2];
        if (address.hasLatitude() && address.hasLongitude()) {
            coordinates[0] = address.getLatitude();
            coordinates[1] = address.getLongitude();
        }
        return coordinates;
    }
}
