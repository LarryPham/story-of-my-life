package co.soml.android.app;

import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import co.soml.android.app.utils.LogUtils;

public class VersionManager {
	public static final String TAG = LogUtils.makeLogTag(VersionManager.class.getSimpleName());
	public static final String appId = "co.soml.android.app.storyapp";
	public static final int SOCKET_TIMEOUT = 30000;

	class Define {
		public static final String NULL_STRING = "";
		public static final String APP_ID_TAG = "appId";
		public static final String APP_RESULT_CODE = "resultCode";
		public static final String APP_INFO = "appInfo";
	}

	public static boolean canUseNetwork(final Context context) {
		ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = manager.getActiveNetworkInfo();
		if (info == null) {
			return false;
		}

		if (!info.isConnected()) {
			return false;
		}

		return true;
	}

	public static String getNodeData(String inform, String nodeName) {
		String stringReturn = null;

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbFactory.newDocumentBuilder();
			Document doc = db.parse(new ByteArrayInputStream(inform.getBytes()));
			NodeList items = doc.getElementsByTagName(nodeName);
			stringReturn = ((CharacterData) items.item(0).getFirstChild()).getData();
		} catch (Exception ex) {
			LogUtils.LOGE(TAG, "Throws exception: " + ex.toString());
		}
		return stringReturn;
	}

	public static int canUpdateStoryApp(final Context context) {
		if (!isExistingStoryApp(context)) {
			return -2;
		}

		if (!canUseNetwork(context)) {
			return -1;
		}

		String strXml = getStoryAppVersionFromServer(context, false);
		if (strXml == null) {
			return -1;
		}
		LogUtils.LOGD(TAG, strXml);
		String resultCode = getNodeData(strXml, "resultCode");
		if (resultCode == null) {
			return -1;
		}

		LogUtils.LOGD(TAG, "resultCode: " + resultCode);
		return Integer.valueOf(resultCode);
	}

	public static boolean isExistingStoryApp(Context context) {
		LogUtils.LOGD(TAG, "isExistStoryApp");
		PackageManager pm = context.getPackageManager();
		try {
			pm.getPackageInfo("co.soml.android.app", PackageManager.GET_ACTIVITIES);
		} catch (PackageManager.NameNotFoundException ex) {
			return false;
		}
		return true;
	}

	public static String getStoryAppVersionFromServer(Context context) {
		LogUtils.LOGD(TAG, "getStoryAppVersionFromServer()");
		String versionName = null;
		try {
			PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			versionName = info.versionName;

		} catch (PackageManager.NameNotFoundException ex) {

		}

		return versionName;
	}

	public static String getStoryAppVersionFromServer(final Context context, boolean testMode) {
		LogUtils.LOGD(TAG, "getStoryAppVersionFromServer...");
		String result = null;
		InputStream retInputStream = null;
		try {
			String strHost = "";
			String strUrl = strHost + "";
			URL url = new URL(strUrl + "");
			LogUtils.LOGD(TAG, url.toString());

			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoInput(true);
			conn.setRequestMethod("GET");
			conn.setConnectTimeout(SOCKET_TIMEOUT);

			try {
				conn.connect();
				retInputStream = conn.getInputStream();
				if (retInputStream != null) {
					try {
						byte[] bytes = new byte[4096];
						StringBuffer temp = new StringBuffer();
						for (int n; (n = retInputStream.read(bytes)) != -1; ) {
							temp.append(new String(bytes, 0, n));
						}

						if (temp != null) {
							result = temp.toString();
						}
					} catch (IOException ex) {
						LogUtils.LOGE(TAG, ex.toString());
						return null;
					}
				}
			} catch (SocketTimeoutException socketTimeoutException) {
				LogUtils.LOGE(TAG, socketTimeoutException.toString());
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	public static boolean invokeStoryApp(final Context context) {
		LogUtils.LOGD(TAG, "Invoking storyapp...");
		if (isExistingStoryApp(context)) {
			final Intent intent = new Intent(Intent.ACTION_MAIN);
			intent.setClassName("co.soml.android.app", "co.soml.android.app.storyapp");
			intent.putExtra("directCall", true);
			intent.putExtra("GUID", appId);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
			context.startActivity(intent);
			return true;
		}
		return false;
	}
}
