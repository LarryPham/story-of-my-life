/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 * @author: Larry Pham (email: larrypham.vn@gmail.com)
 * Date: 6/16/15 4:43 PM
 **/

package co.soml.android.app.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ViewAnimator;

import co.soml.android.app.utils.LogUtils;

public class StorySendCommentButton extends ViewAnimator implements View.OnClickListener {

    public static final String TAG = LogUtils.makeLogTag(StorySendCommentButton.class.getSimpleName());

    public static final int SEND_STATE = 0;
    public static final int DONE_STATE = 1;

    public static final long RESET_STATE_DELAY_MILLIS = 2000;

    private OnSendClickListener mSendClickListener;
    private int mCurrentState;

    private Runnable mRevertStateRunnable = new Runnable() {
        @Override
        public void run() {
            setCurrentState(SEND_STATE);
        }
    };

    public StorySendCommentButton(Context context) {
        super(context);
        init(context);
    }

    public StorySendCommentButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public void init(Context context) {
    }

    @Override
    public void onClick(View v) {

    }

    public void setCurrentState(int state) {
    }

    public OnSendClickListener getSendClickListener() {
        return mSendClickListener;
    }

    public void setSendClickListener(OnSendClickListener sendClickListener) {
        mSendClickListener = sendClickListener;
    }

    public interface OnSendClickListener {
        public void onSendClicked(View sendButton);
    }
}
