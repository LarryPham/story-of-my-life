package co.soml.android.app.services;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import co.soml.android.app.AppConstants;
import co.soml.android.app.models.Story;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its
 * documentation are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable
 * form without the prior written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with
 * respect to the contents, and assumes no responsibility for any errors that might appear in the software and
 * documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.06.2015
 * <p>
 * The StoriesResult which used to adaptable for getting the data after parsing the json data. And then,
 * feeding them to result object, It will also be used to notify the application call the controllers back for
 * updating the database.
 */
public class StoriesResult extends BaseResult {
    public static final String TAG = AppConstants.PREFIX + StoriesResult.class.getSimpleName();

    @Expose
    @SerializedName("stories")
    private List<Story> mStoryList = new ArrayList<Story>();

    public StoriesResult() {
    }

    public List<Story> getStoryList() {
        return mStoryList;
    }

    public void setStoryList(List<Story> storyList) {
        mStoryList = storyList;
    }
}
