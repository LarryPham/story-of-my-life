package co.soml.android.app.services.apis;

import java.util.List;

import co.soml.android.app.models.Photo;
import co.soml.android.app.services.CommentsResult;
import co.soml.android.app.services.PhotoResult;
import co.soml.android.app.services.StatusResult;
import co.soml.android.app.services.StoriesResult;
import co.soml.android.app.services.StoryResult;
import co.soml.android.app.services.UserListResult;
import co.soml.android.app.services.UserResult;
import co.soml.android.app.services.VoteResult;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.Field;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.PATCH;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.mime.TypedFile;

public interface AppRestApi {

    /**
     * Session management apis
     */
    // Login into app by using facebook, fabric, email account
    @POST("/sessions/facebook")
    void signInByFacebook(@Query("fb_token") String fbAuthToken,
                          Callback<UserResult> callback);

    @POST("/sessions/fabric")
    void signInByFabric(@Query("fabric_auth_token") String fabricToken,
                        @Query("fabric_auth_token_secret") String secret,
                        @Query("fabric_id") long fabricId,
                        Callback<UserResult> userResultCallback);

    @POST("/sessions")
    void signInByEmailAccount(@Query("email") String email,
                              @Query("password") String password,
                              Callback<UserResult> callback);

    /**
     * Account management apis
     */
    @POST("/account")
    void signInGuest(Callback<UserResult> userResultCallback);

    @POST("/account")
    void registerAccount(@Query("name") String name,
                         @Query("email") String email,
                         @Query("password") String password,
                         @Query("password_confirmation") String passwordConfirmation,
                         @Query("description") String description,
                         Callback<UserResult> callback);

    @GET("/account/following/{user_id}")
    void checkAccountFollowingUser(@Path("user_id") int userId,
                                   Callback<UserResult> callback);

    @GET("/account/liked/{story_id}")
    void checkAccountLikedStory(@Path("story_id") int storyId,
                                Callback<StoryResult> callback);

    @GET("/account/new")
    void fetchAccountInfo(Callback<UserResult> callback);

    /**
     * Stories apis for searching stories by query
     */
    @GET("/stories/{scope}")
    void getStoriesByScope(@Path("scope") String scope,
                           @Query("q") String q,
                           @Query("user_id") int userId,
                           @Query("start_index") int index,
                           @Query("limit") int limit,
                           @Query("state") String state,
                           Callback<StoriesResult> callback);

    /**
     * Stories Collections Apis for featured and latest
     */
    @GET("/stories/{scope}")
    void getStoriesByScope(@Path("scope") String scope,
                           @Query("start_index") int index,
                           @Query("limit") int limit,
                           Callback<StoriesResult> callback);

    @GET("/stories")
    void getMyStories(@Query("user_id") int userId,
                      @Query("start_index") int index,
                      @Query("limit") int limit,
                      @Query("state") String state,
                      Callback<StoriesResult> callback);

    @POST("/stories/report")
    /*void reportStory(@Body Map<String, String> params,
                      Callback<JsonStory> callback);*/
    void reportStory(@Query("reason") String reason,
                     @Query("message") String message,
                     Callback<StoryResult> callback);

    @POST("/stories/{id}/likes")
    void likeStory(@Path("id") int storyId,
                   Callback<VoteResult> callback);

    @POST("/stories/{id}/unlikes")
    void unlikeStory(@Path("id") int storyId,
                     Callback<VoteResult> callback);

    @POST("/stories")
    void postStory(@Query("story[name]") String storyName,
                   @Query("story[interval]") int storyInterval,
                   @Query("story[description]") String storyDesc,
                   @Query("story[latitude]") float storyLatitude,
                   @Query("story[longitude]") float storyLongitude,
                   @Query("story[location]") String storyLocation,
                   Callback<StoryResult> callback);

    @GET("/stories/{id}")
    void getStory(@Path("id") int storyId,
                  Callback<StoryResult> callback);

    @PATCH("/stories/{id}")
    void updateStory(@Path("id") int storyId,
                     @Query("story[name]") String storyName,
                     @Query("story[interval]") int storyInterval,
                     @Query("story[description]") String storyDesc,
                     @Query("story[latitude]") float storyLatitude,
                     @Query("story[longitude]") float storyLongitude,
                     @Query("story[location]") String storyLocation,
                     Callback<StoryResult> callback);

    @DELETE("/stories/{id}")
    void deleteStory(@Path("id") int storyId,
                     Callback<StoryResult> callback);

    /**
     * Follower apis
     */
    @GET("/users/{user_id}/followers")
    void getFollowers(@Path("user_id") int userId,
                      Callback<UserListResult> callback);

    /**
     * Following apis
     */
    @DELETE("/users/{user_id}/follow")
    void unfollowUser(@Path("user_id") int userId,
                      Callback<StatusResult> callback);

    @POST("/users/{user_id}/follow")
    void followUser(@Path("user_id") int userId,
                    Callback<StatusResult> callback);

    @GET("/users/{user_id}/following")
    void getFollowingUsers(@Path("user_id") int userId,
                           Callback<UserListResult> callback);

    /**
     * Users apis
     */
    @POST("/users/report")
    void reportUser(@Query("reason") String reason,
                    @Query("message") String message,
                    Callback<UserResult> callback);

    @GET("/users")
    void getUsers(@Query("name") String name,
                  @Query("followed") boolean followed,
                  @Query("limit") int limit,
                  @Query("offset") int offset,
                  Callback<UserListResult> callback);

    @GET("/users/{id}")
    void getUser(@Path("id") int id,
                 Callback<UserResult> callback);

    /**
     * Comments apis
     */
    @POST("/stories/{story_id}/comments/{id}")
    void replyComment(@Path("story_id") int storyId,
                      @Path("id") int commentId,
                      @Query("comment[body]") String commentBody,
                      Callback<CommentsResult> callback);

    @POST("/stories/{story_id}/comments/{id}/likes")
    void likeComment(@Path("story_id") int storyId,
                     @Path("id") int commentId,
                     Callback<CommentsResult> callback);

    @POST("/stories/{story_id}/comments/{id}/unlikes")
    void unlikeComment(@Path("story_id") int storyId,
                       @Path("id") int commentId,
                       Callback<CommentsResult> callback);

    @POST("/stories/{story_id}/comments")
    void commentStory(@Path("story_id") int storyId,
                      @Query("comment[body]") String commentBody,
                      Callback<CommentsResult> callback);

    @PATCH("/stories/{story_id}/comments/{id}")
    void updateComment(@Path("story_id") int storyId,
                       @Path("id") int commentId,
                       @Query("comment[body]") String commentBody,
                       Callback<CommentsResult> callback);

    @DELETE("/stories/{story_id}/comments/{id}")
    void deleteComment(@Path("story_id") int storyId,
                       @Path("id") int commentId,
                       Callback<CommentsResult> callback);

    @POST("/stories/{story_id}/comments/{id}/report")
    void reportComment(@Path("story_id") int storyId,
                       @Path("id") int commentId,
                       @Query("reason") String reason,
                       @Query("message") String message,
                       Callback<CommentsResult> callback);

    /**
     * Photos apis
     */
    // APIs for songs
    @POST("/stories/{story_id}/photos/order")
    void modifyPhotosOrder(@Path("story_id") int storyId,
                           Callback<Photo> callback);

    @GET("/stories/{story_id}/photos")
    void getPhotos(@Path("story_id") int storyId,
                   Callback<List<PhotoResult>> callback);

    @Multipart
    @POST("/stories/{story_id}/photos")
    void uploadPhotos(@Path("story_id") long storyId,
                      @Part("file") TypedFile photo,
                      @Query("caption") String photoDescription,
                      Callback<List<PhotoResult>> callback);

    @PATCH("/stories/{story_id}/photos/{id}")
    void updatePhotoDescription(@Path("story_id") int storyId,
                                @Path("id") int photoId,
                                @Query("caption") String caption,
                                @Query("story_id") int updateStoryId,
                                @Query("photo_id") int updatePhotoId,
                                Callback<PhotoResult> callback);

    @DELETE("/stories/{story_id}/photos/{id}")
    void deletePhoto(@Path("story_id") int storyId,
                     @Path("id") int photoId,
                     Callback<PhotoResult> callback);
}
