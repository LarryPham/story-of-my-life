package co.soml.android.app.utils;

import android.os.SystemClock;

import java.util.ArrayList;

/**
 * Forked from android.util.TimingLogger to use LogUtils instead of Log + new static interface.
 */
public class ProfilingUtils {
	private static final String TAG = LogUtils.makeLogTag(ProfilingUtils.class.getSimpleName());
	private static ProfilingUtils sInstance;
	private String mLabel;
	private ArrayList<Long> mSplits;
	private ArrayList<String> mSplitLabels;

	public static ProfilingUtils getInstance() {
		if (sInstance == null) {
			sInstance = new ProfilingUtils();
		}
		return sInstance;
	}

	public static void start(String label) {
		getInstance().reset(label);
	}

	public static void split(String splitLabel) {
		getInstance().addSplit(splitLabel);
	}

	public static void dump() {
		getInstance().dumpToLog();
	}

	public void reset(String label) {
		mLabel = label;
		reset();
	}

	public void reset() {
		if (mSplits == null) {
			mSplits = new ArrayList<Long>();
			mSplitLabels = new ArrayList<String>();
		} else {
			mSplits.clear();
			mSplitLabels.clear();
		}
		addSplit(null);
	}

	public void addSplit(String splitLabel) {
		long now = SystemClock.elapsedRealtime();
		mSplits.add(now);
		mSplitLabels.add(splitLabel);
	}
	public void dumpToLog() {
		LogUtils.LOGD(TAG, mLabel + ":begin");
		final long first = mSplits.get(0);
		long now = first;
		for (int index = 1; index < mSplits.size(); index++) {
			now = mSplits.get(index);
			final String splitLabel = mSplitLabels.get(index);
			final long prev = mSplits.get(index - 1);
			LogUtils.LOGD(TAG, mLabel + ":     " + (now - prev) + " ms, " + splitLabel);
		}
		LogUtils.LOGD(TAG, mLabel + ": end, " + (now - first) + " ms ");
	}
}
