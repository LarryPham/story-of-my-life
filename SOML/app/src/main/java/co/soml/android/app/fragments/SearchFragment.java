package co.soml.android.app.fragments;

import com.twitter.sdk.android.core.models.Search;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.SearchView;

import co.soml.android.app.Properties;
import co.soml.android.app.R;
import co.soml.android.app.StoryApp;
import co.soml.android.app.activities.SearchActivity;
import co.soml.android.app.activities.StoryMainActivity;
import co.soml.android.app.controllers.ControllerMessage;
import co.soml.android.app.controllers.SearchController;
import co.soml.android.app.models.StoryBean;
import co.soml.android.app.models.StoryDataModel;
import co.soml.android.app.services.tasks.Action;
import co.soml.android.app.utils.CommonUtil;
import co.soml.android.app.utils.DisplayUtils;
import co.soml.android.app.utils.LogUtils;

/**
 *  Copyright (C) 2015 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation
 *  are confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied,
 *  reproduced, transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior
 *  written consent of Sugar Ventures Inc. Sugar Ventures Inc makes no representations with respect to the contents, and
 *  assumes no responsibility for any errors that might appear in the software and documents. This publication and the
 *  contents hereof are subject to change without notice.
 *  @author Larry Pham(Email: larrypham.vn@gmail.com)
 *  @since 6/29/15 2:39 PM
 **/

public class SearchFragment extends BaseFragment implements OnQueryTextListener {
	public static final String TAG = LogUtils.makeLogTag(SearchFragment.class.getSimpleName());

	public StoryApp mApp;
	public StoryDataModel mDataModel;
	public SearchController mController;
	public StoryMainActivity mActivity;

	private float mDipScale;
	private int mDensity;

	private SharedPreferences mPrefs;
	private SharedPreferences.Editor mPrefsEditor;

	static Intent mSearchIntent;
	private SearchView mSearchView;
	private View mRootView;

	private static String mSearchKeyword;
	private static String mSearchResultKeyword;

	private boolean mIsSearchInput = true;
	private boolean mIsSearchSubmit = false;
	private boolean mIsDuplication = false;

	private Snackbar mSnackBar = null;
	private Boolean mFlag = false;
	private int mSetLastWidthSize = -1;
	private StoryBean mBean;

	public static SearchFragment newInstance(Bundle param) {
		final SearchFragment fragment = new SearchFragment();
		mSearchKeyword = param.getString("SearchKeyword");
		mSearchResultKeyword = param.getString("SearchResultKeyword");
		return fragment;
	}

	@Override
	public void onAttach(Activity activity) {
		LogUtils.LOGD(TAG, "onAttach()");
		super.onAttach(activity);

		mActivity = (StoryMainActivity) getActivity();
		mApp = (StoryApp) mActivity.getApplication();
		mDataModel = mApp.getAppDataModel();

		mPrefs = mActivity.getSharedPreferences("GeneralSettings", Context.MODE_PRIVATE);
		mPrefsEditor = mPrefs.edit();
		mDipScale =  getResources().getDisplayMetrics().density;
		mDensity = CommonUtil.getDensity(mApp);
		mBean = StoryMainActivity.getBean();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		LogUtils.LOGD(TAG, "onCreate()");
		super.onCreate(savedInstanceState);
		mActivity.registerReceiver(mReceiver, new IntentFilter(Action.FINISH_APPLICATION));
	}

	public BroadcastReceiver mReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			final String action = intent.getAction();
			LogUtils.LOGD(TAG, String.format("onReceive: Action[%s]", action));
			if (action.equalsIgnoreCase(Action.FINISH_APPLICATION)) {
				mActivity.finish();
			} else if (action.equalsIgnoreCase(Action.SHOW_PROGRESS)) {
			} else if (action.equalsIgnoreCase(Action.DISMISS)) {
			}
		}
	};

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		LogUtils.LOGD(TAG, "onCreateView() ");
		if (mController == null) {
			mController = new SearchController(mActivity, this, mDataModel);
		}

		mBean = StoryMainActivity.getBean();
		mRootView = (View) inflater.inflate(R.layout.layout_comment_box, null);

		if (mBean.getStoryDataModel().getCurrentViewState() != StoryDataModel.ViewState.SEARCH) {
			mBean.getStoryDataModel().setCurrentViewState(StoryDataModel.ViewState.SEARCH);
		}

		setHasOptionsMenu(true);
		return mRootView;
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		LogUtils.LOGD(TAG, "onActivityCreated()");

	}

	@Override
	public void onDestroyView() {
		LogUtils.LOGD(TAG, "onDestroyView");

		if (mController != null) {
			mController.onDestroy();
		}

		if (mRootView != null) {
			((ViewGroup) mRootView.getParent()).removeView(mRootView);
			mRootView = null;
		}

		mActivity.cancelRequestContent();
		super.onDestroyView();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		setDeviceWidth();
	}

	@Override
    public void invalidate() {

    }

    @Override
    public void invalidate(Object... params) {

    }

	@Override
	public boolean onQueryTextSubmit(String query) {
		mSearchKeyword = query;
		mSearchResultKeyword = mSearchKeyword;
		if (mSearchKeyword.length() < Properties.MIN_SEARCH_KEYWORD_LENGTH) {
			if (mSnackBar == null) {
				Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) LayoutInflater.from(mActivity).inflate(R.layout
						.layout_snackbar, null);
				mSnackBar = Snackbar.make(layout, String.format(getResources().getString(R.string.text_search_require_min_char),
						Properties.MIN_SEARCH_KEYWORD_LENGTH), Snackbar.LENGTH_SHORT);
			} else {
				mSnackBar.setText(String.format(getResources().getString(R.string.text_search_require_min_char), Properties
						.MIN_SEARCH_KEYWORD_LENGTH));
			}
			mSnackBar.show();
		} else {
			InputMethodManager manager = (InputMethodManager) mActivity.getSystemService(mActivity.INPUT_METHOD_SERVICE);
			manager.hideSoftInputFromWindow(mSearchView.getWindowToken(), 0);
			mSearchView.clearFocus();
			mIsSearchSubmit = true;

			mBean.getStoryDataModel().getSearchStoryList().clear();

			final Bundle params = new Bundle();
			params.putString("SearchKeyword", mSearchKeyword);
			params.putString("SearchResultKeyword", mSearchResultKeyword);

			if (!mDataModel.getSearchAutoCompletedList().isEmpty()) {
				for (int count = 0; count < mDataModel.getSearchAutoCompletedList().size(); count++) {
					if (mDataModel.getSearchAutoCompletedList().get(count).toString().equals(mSearchResultKeyword)) {
						mIsDuplication = true;
						break;
					}
				}

				if (!mIsDuplication) {
					mDataModel.getSearchAutoCompletedList().add(mSearchResultKeyword);
				}

				mIsDuplication = false;
			} else {
				mDataModel.getSearchAutoCompletedList().add(mSearchResultKeyword);
			}

			mSearchView.setQuery("", false);
			mSearchView.setIconified(true);

			if (mController == null) {
				mController = new SearchController(mActivity, this, mDataModel);
			}
		}
		return false;
	}

	@Override
	public boolean onQueryTextChange(String inputString) {
		final Bundle param = new Bundle();
		param.putString("AutoCompleteString", inputString);
		if (mController == null) {
			mController = new SearchController(mActivity, this, mDataModel);
		}

		if (inputString.length() == 1 && mIsSearchInput) {
			FragmentTransaction transaction = getFragmentManager().beginTransaction();
			SearchHistoryFragment fragment = SearchHistoryFragment.newInstance(param);
			transaction.replace(R.id.frame_content, fragment, "SearchHistory");

			transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
			mBean.setSearchHistoryFragment(fragment);
			transaction.commit();
			mIsSearchInput = false;
		} else if (inputString.length() == 0) {
			if (mIsSearchSubmit) {
				mIsSearchSubmit = false;
				mIsSearchInput = true;
			} else if (!mIsSearchInput) {
				//showDetails();
				mIsSearchInput = true;
			}
		} else {
			if (mBean.getSearchHistoryFragment() == null) {
				FragmentTransaction transaction = getFragmentManager().beginTransaction();
				SearchHistoryFragment searchHistoryFragment = SearchHistoryFragment.newInstance(param);
				transaction.replace(R.id.frame_content, searchHistoryFragment,  "SearchHistory");

				transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
				mBean.setSearchHistoryFragment(searchHistoryFragment);
				transaction.commit();
				mIsSearchInput = false;
			} else {
				mController.sendMessage(ControllerMessage.SHOW_SEARCH_AUTOCOMPLETE_LIST, (Object) param);
			}
		}
		return false;
	}

	private void setDeviceWidth() {

	}
}
