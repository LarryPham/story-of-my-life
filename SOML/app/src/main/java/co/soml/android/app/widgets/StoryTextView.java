package co.soml.android.app.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;

import co.soml.android.app.R;
import co.soml.android.app.utils.LogUtils;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since May.08.2015
 * <p>
 * Customized TextView used into layouts - enables keeping custom typeface handling into one place(so we avoid having to set the typeface
 * for every single TextView in every single activity)
 */
public class StoryTextView extends TextView {

    public static final String TAG = LogUtils.makeLogTag(StoryTextView.class);
    protected boolean mFixWidowWordEnabled;

    public StoryTextView(Context context) {
        super(context);
        TypefaceCache.setCustomTypeface(context, this, null);
    }

    public StoryTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypefaceCache.setCustomTypeface(context, this, attrs);
        readCustomAttrs(context, attrs);
    }

    public StoryTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypefaceCache.setCustomTypeface(context, this, attrs);
        readCustomAttrs(context, attrs);
    }

    public void setFixWidowWord(boolean enabled) {
        mFixWidowWordEnabled = enabled;
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        if (!mFixWidowWordEnabled) {
            super.setText(text, type);
            return;
        }
        Spannable out;
        int lastSpace = String.valueOf(text).lastIndexOf(' ');
        if (lastSpace != -1 && lastSpace < text.length() - 1) {
            // Replace last space character by a non breaking space.
            CharSequence tmpText = replaceCharacter(text, lastSpace, "\u00A0");
            out = new SpannableString(tmpText);
            // Restore spans if text is an instance of Spanned
            if (text instanceof Spanned) {
                TextUtils.copySpansFrom((Spanned) text, 0, text.length(), null, out, 0);
            }
        } else {
            out = new SpannableString(text);
        }
        super.setText(out, type);
    }

    private void readCustomAttrs(Context context, AttributeSet attrs) {
        TypedArray array = context.getTheme().obtainStyledAttributes(attrs, R.styleable.StoryTextView, 0, 0);
        if (array != null) {
            mFixWidowWordEnabled = array.getBoolean(R.styleable.StoryTextView_fixWidowWords, false);

            if (mFixWidowWordEnabled) {
                // Force text update
                setText(getText());
            }
        }

    }

    private CharSequence replaceCharacter(CharSequence source, int charIndex, CharSequence replacement) {
        if (charIndex != -1 && charIndex < source.length() - 1) {
            return TextUtils.concat(source.subSequence(0, charIndex), replacement, source.subSequence(charIndex + 1, source.length()));
        }
        return source;
    }
}
