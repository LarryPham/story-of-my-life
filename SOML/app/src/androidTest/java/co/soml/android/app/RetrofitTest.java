package co.soml.android.app;

import android.os.Handler;
import android.test.suitebuilder.annotation.MediumTest;

import junit.framework.TestCase;

import co.soml.android.app.services.tasks.Action;
import co.soml.android.app.services.tasks.ServerSubscribeTask;

/**
 * Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are
 * confidential and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced,
 * transmitted, translated, or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures
 * Inc. Sugar Ventures Inc makes no representations with respect to the contents, and assumes no responsibility for any errors that might
 * appear in the software and documents. This publication and the contents hereof are subject to change without notice. History
 *
 * @author Larry Pham
 * @since Apr.13.2015
 */
public class RetrofitTest extends TestCase {

  private ServerSubscribeTask mSubscribeTask;
  private String mAction;
  private Handler mWeakHandler = new Handler();
  private String mUserEmail = "larrypham.vn@gmail.com";
  private String mPassword = "abcd1234!";

  @Override
  protected void setUp() throws Exception {
    super.setUp();
    mAction = Action.REQUEST_TO_SERVER_AUTHENTICATE_EMAIL_ACCOUNT;
    mSubscribeTask = new ServerSubscribeTask(mAction, mWeakHandler);
    mSubscribeTask.setUserEmail(mUserEmail);
    mSubscribeTask.setUserPassword(mPassword);
  }

  @MediumTest
  public void testAuthenticateEmailAccount() {
    mSubscribeTask.subscribeWithEmailAccount();

  }
}
