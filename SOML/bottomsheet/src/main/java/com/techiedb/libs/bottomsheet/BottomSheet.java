/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/22/15 12:36 AM.
 **/

package com.techiedb.libs.bottomsheet;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.MenuRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.annotation.StyleRes;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class BottomSheet extends Dialog implements AdapterView.OnItemClickListener, CollapsingView.CollapseListener{
    private static final int NO_RESOURCE = -1;
    private static final String TAG = BottomSheet.class.getSimpleName();
    private static final int[] ATTRS = new int[] {
            R.attr.bottom_sheet_bg_color,
            R.attr.bottom_sheet_title_color,
            R.attr.bottom_sheet_list_item_color,
            R.attr.bottom_sheet_grid_item_color,
            R.attr.bottom_sheet_item_icon_color
    };

    private Builder mBuilder;
    private BaseAdapter mBaseAdapter;
    private GridView mGridView;
    private TextView mTitle;
    private BottomSheetListener mListener;

    public BottomSheet(Context context, Builder builder, @StyleRes int style, BottomSheetListener listener) {
        super(context, style);
        mBuilder = builder;
        mListener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!canCreateSheet()) {
            throw new IllegalStateException("Unable to create BottomSheet, missing params");
        }

        Window window = getWindow();
        if (window != null) {
            int width = getContext().getResources().getDimensionPixelSize(R.dimen.bottom_sheet_width);
            window.setLayout(width <= 0 || mBuilder.mIsGrid ? ViewGroup.LayoutParams.MATCH_PARENT : width
                    , ViewGroup.LayoutParams.WRAP_CONTENT);
            window.setGravity(Gravity.BOTTOM);
        } else {
            Log.e(TAG, "window came back as null, unable to set defaults");
        }

        TypedArray array = getContext().obtainStyledAttributes(ATTRS);
        initLayout(array);
        if (mBuilder.mMenuItems != null) {
            initMenu(array);
            if (mListener != null) mListener.onSheetShown();
        } else {
            mGridView.setAdapter(mBaseAdapter = new AppAdapter(getContext(), mBuilder));
        }
        array.recycle();
    }

    @Override
    public void dismiss() {
        if (mListener != null) mListener.onSheetDismissed();
        super.dismiss();
    }

    protected void initLayout(TypedArray array) {
        Resources res = getContext().getResources();
        setCancelable(mBuilder.mCancelable);
        View view = LayoutInflater.from(getContext()).inflate(R.layout.bottom_sheet_layout, null);
        ((CollapsingView) view).setCollapseListener(this);

        view.findViewById(R.id.container).setBackgroundColor(array.getColor(0, Color.WHITE));
        mGridView = (GridView) view.findViewById(R.id.grid);
        mGridView.setOnItemClickListener(this);

        mTitle = (TextView) view.findViewById(R.id.title);
        boolean hasTitle = !TextUtils.isEmpty(mBuilder.mTitle);

        if (hasTitle) {
            mTitle.setText(mBuilder.mTitle);
            mTitle.setVisibility(View.VISIBLE);

            if (mBuilder.mTitleColor != Integer.MIN_VALUE) {
                mTitle.setTextColor(mBuilder.mTitleColor);
            } else {
                mTitle.setTextColor(array.getColor(1, res.getColor(R.color.black_55)));
            }
        } else {
            mTitle.setVisibility(View.GONE);
        }

        if (mBuilder.mIsGrid) {
            int gridPadding = res.getDimensionPixelSize(R.dimen.bottom_sheet_grid_padding);
            int topPadding = res.getDimensionPixelSize(R.dimen.bottom_sheet_dialog_padding);
            mGridView.setNumColumns(res.getInteger(R.integer.bottomsheet_num_columns));
            mGridView.setVerticalSpacing(res.getDimensionPixelSize(R.dimen.bottom_sheet_grid_spacing));
            mGridView.setPadding(0, topPadding, 0, gridPadding);
        } else {
            int padding = res.getDimensionPixelSize(R.dimen.bottom_sheet_list_padding);
            mGridView.setPadding(0, hasTitle ? 0 : padding, 0, padding);
        }
        setContentView(view);
    }

    protected void initMenu(TypedArray array) {
        Resources res = getContext().getResources();
        int listColor;
        int gridColor;

        if (mBuilder.mItemColor != Integer.MIN_VALUE) {
            listColor = mBuilder.mItemColor;
            gridColor = mBuilder.mItemColor;
        } else {
            listColor = array.getColor(2, res.getColor(R.color.black_85));
            gridColor = array.getColor(3, res.getColor(R.color.black_85));
        }

        if (mBuilder.mMenuItemTintColor == Integer.MIN_VALUE) {
            mBuilder.mMenuItemTintColor = array.getColor(4, Integer.MIN_VALUE);
        }

        mGridView.setAdapter(mBaseAdapter = new GridAdapter(getContext(), mBuilder, listColor, gridColor));
    }
    @Override
    public void onCollapse() {
        dismiss();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (mBaseAdapter instanceof GridAdapter) {
            if (mListener != null) {
                MenuItem item = ((GridAdapter) mBaseAdapter).getItem(position);
                mListener.onSheetItemSelected(item);
            }
        } else if (mBaseAdapter instanceof AppAdapter) {
            AppAdapter.AppInfo info = ((AppAdapter) mBaseAdapter).getItem(position);
            final Intent intent = new Intent(mBuilder.mShareIntent);
            intent.setComponent(new ComponentName(info.mPackageName, info.mName));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getContext().startActivity(intent);
        }
        dismiss();
    }

    public boolean canCreateSheet() {
        return mBuilder != null && ((mBuilder.mMenuItems != null && !mBuilder.mMenuItems.isEmpty() || (mBuilder.mApps != null) &&
                (!mBuilder.mApps.isEmpty())));
    }
    @Nullable
    public static BottomSheet createShareBottomSheet(Context context, Intent intent, String shareTitle, boolean isGrid) {
        if (context == null || intent == null) return null;

        PackageManager manager = context.getPackageManager();
        List<ResolveInfo> apps = manager.queryIntentActivities(intent, 0);

        if (apps != null && !apps.isEmpty()) {
            List<AppAdapter.AppInfo> appResources = new ArrayList<>(apps.size());

            for (ResolveInfo resolveInfo : apps) {
                String title = resolveInfo.loadLabel(manager).toString();
                String packageName = resolveInfo.activityInfo.packageName;
                String name = resolveInfo.activityInfo.name;
                Drawable drawable = resolveInfo.loadIcon(manager);
                appResources.add(new AppAdapter.AppInfo(title, packageName, name, drawable));
            }

            Builder builder = new Builder(context).setApps(appResources, intent).setTitle(shareTitle);
            if (isGrid) builder.grid();
            return builder.create();
        }
        return null;
    }

    @Nullable
    public static BottomSheet createShareBottomSheet(Context context, Intent intent, String shareTitle) {
        return createShareBottomSheet(context, intent, shareTitle, false);
    }

    @Nullable
    public static BottomSheet createShareBottomSheet(Context context, Intent intent, @StringRes int shareTitle) {
        return createShareBottomSheet(context, intent, context.getString(shareTitle), false);
    }

    public static class Builder {
        @StyleRes
        int mStyle = NO_RESOURCE;
        String mTitle = null;
        boolean mCancelable = true;
        boolean mIsGrid = false;
        List<MenuItem> mMenuItems;

        @ColorInt
        int mMenuItemTintColor = Integer.MIN_VALUE;
        @ColorInt
        int mTitleColor = Integer.MIN_VALUE;
        @ColorInt
        int mItemColor = Integer.MIN_VALUE;

        Context mContext;
        BottomSheetListener mListener;
        List<AppAdapter.AppInfo> mApps;
        Intent mShareIntent;

        public Builder(Context context) {
            this (context, NO_RESOURCE, R.style.BottomSheet);
        }

        public Builder(Context context, @MenuRes int sheetItems) {
            this (context, sheetItems, R.style.BottomSheet);
        }

        public Builder(Context context, @MenuRes int sheetItems, @StyleRes int style) {
            this.mContext = context;
            this.mStyle = style;
            if (sheetItems != NO_RESOURCE) setSheet(sheetItems);
        }

        public Builder setTitle(String title) {
            this.mTitle = title;
            return this;
        }

        public Builder setTitle(@StringRes int title) {
            return setTitle(mContext.getString(title));
        }

        public Builder grid() {
            mIsGrid = true;
            return this;
        }

        public Builder setCancelable(boolean cancelable) {
            this.mCancelable = cancelable;
            return this;
        }

        public Builder setListener(BottomSheetListener listener) {
            this.mListener = listener;
            return this;
        }

        public Builder dark() {
            mStyle = R.style.BottomSheet_Dark;
            return this;
        }

        public Builder setStyle(@StyleRes int style) {
            this.mStyle = style;
            return this;
        }

        public Builder setSheet(@MenuRes int sheetItems) {
            BottomSheetMenu menu = new BottomSheetMenu(mContext);
            new MenuInflater(mContext).inflate(sheetItems, menu);
            return setMenu(menu);
        }

        public Builder setMenu(@Nullable Menu menu) {
            if (menu != null) {
                List<MenuItem> items = new ArrayList<>(menu.size());
                for (int index = 0; index < menu.size(); index++) {
                    items.add(menu.getItem(index));
                }
                return setMenuItem(items);
            }
            return this;
        }

        public Builder setMenuItem(@Nullable List<MenuItem> menuItems) {
            this.mMenuItems = menuItems;
            return this;
        }

        public Builder addMenuItem(MenuItem item) {
            if (mMenuItems == null) mMenuItems = new ArrayList<>();
            mMenuItems.add(item);
            return this;
        }

        public Builder setMenuItemTintColorRes(@ColorRes int colorRes) {
            final int menuItemTintColor = mContext.getResources().getColor(colorRes);
            return setMenuItemTintColor(mMenuItemTintColor);
        }

        public Builder setMenuItemTintColor(@ColorInt int menuItemTintColor) {
            this.mMenuItemTintColor = menuItemTintColor;
            return this;
        }

        public Builder setTitleColor(@ColorInt int titleColor) {
            this.mTitleColor = titleColor;
            return this;
        }

        public Builder setTitleColorRes(@ColorRes int colorRes) {
            return setTitleColor(mContext.getResources().getColor(colorRes));
        }

        public Builder setItemColor(@ColorInt int itemColor) {
            this.mItemColor = itemColor;
            return this;
        }

        public Builder setItemColorRes(@ColorRes int colorRes) {
            return setItemColor(mContext.getResources().getColor(colorRes));
        }

        private Builder setApps(List<AppAdapter.AppInfo> apps, Intent intent) {
            this.mApps = apps;
            mShareIntent = intent;
            return this;
        }

        public BottomSheet create() {
            return new BottomSheet(mContext, this, mStyle, mListener);
        }

        public void show() {
            create().show();
        }
    }

    private static class GridAdapter extends BaseAdapter {
        private final List<MenuItem> mItems;
        private final LayoutInflater mInflater;
        private boolean mIsGrid;
        private int mListTextColor;
        private int mGridTextColor;
        private int mTintColor;

        public GridAdapter(Context context, Builder builder, int listTextColor, int gridTextColor) {
            mItems = builder.mMenuItems;
            mIsGrid = builder.mIsGrid;
            mInflater = LayoutInflater.from(context);
            mListTextColor = listTextColor;
            mGridTextColor = gridTextColor;
            mTintColor = builder.mMenuItemTintColor;
        }

        @Override
        public int getCount() {
            return mItems.size();
        }

        @Override
        public MenuItem getItem(int position) {
            return mItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return getItem(position).getItemId();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            MenuItem item = getItem(position);
            ViewHolder holder;

            if (convertView == null) {
                convertView = mInflater.inflate(mIsGrid ? R.layout.bottom_sheet_grid_item : R.layout.bottom_sheet_list_item, parent, false);
                holder = new ViewHolder(convertView);
                holder.mTitle.setTextColor(mIsGrid ? mGridTextColor : mListTextColor);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            Drawable menuIcon = item.getIcon();
            if (mTintColor != Integer.MIN_VALUE && menuIcon != null) {
                menuIcon = menuIcon.mutate();
                menuIcon.setColorFilter(new LightingColorFilter(Color.BLACK, mTintColor));
            }

            holder.mIcon.setImageDrawable(menuIcon);
            holder.mTitle.setText(item.getTitle());
            return convertView;
        }
    }

    private static class AppAdapter extends BaseAdapter {
        List<AppInfo> mApps;
        private LayoutInflater mInflater;
        private int mTextColor;
        private boolean mIsGrid;

        public AppAdapter(Context context, Builder builder) {
            mApps = builder.mApps;
            mInflater = LayoutInflater.from(context);
            mTextColor = context.getResources().getColor(R.color.black_85);
            mIsGrid = builder.mIsGrid;
        }

        @Override
        public int getCount() {
            return mApps.size();
        }

        @Override
        public AppInfo getItem(int position) {
            return mApps.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            AppInfo appInfo = getItem(position);
            ViewHolder holder;

            if (convertView == null) {
                convertView = mInflater.inflate(mIsGrid ? R.layout.bottom_sheet_grid_item : R.layout.bottom_sheet_list_item, parent, false);
                holder = new ViewHolder(convertView);
                holder.mTitle.setTextColor(mTextColor);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.mIcon.setImageDrawable(appInfo.mDrawable);
            holder.mTitle.setText(appInfo.mTitle);
            return convertView;
        }

        private static class AppInfo {
            public String mTitle;
            public String mPackageName;
            public String mName;
            public Drawable mDrawable;

            public AppInfo(String title, String packageName, String name, Drawable drawable) {
                this.mTitle = title;
                this.mPackageName = packageName;
                this.mName = name;
                this.mDrawable = drawable;
            }
        }
    }
    private static class ViewHolder {
        public TextView mTitle;
        public ImageView mIcon;

        public ViewHolder(View view) {
            mTitle = (TextView) view.findViewById(R.id.title);
            mIcon = (ImageView) view.findViewById(R.id.icon);
            view.setTag(this);
        }
    }
}
