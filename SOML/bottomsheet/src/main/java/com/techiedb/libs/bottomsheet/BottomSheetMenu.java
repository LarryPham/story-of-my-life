/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/22/15 12:06 AM.
 **/

package com.techiedb.libs.bottomsheet;

import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;

import java.util.ArrayList;
import java.util.List;

public class BottomSheetMenu implements Menu {
    private Context mContext;
    private boolean mIsQwerty;
    private ArrayList<BottomSheetMenuItem> mItems;

    public BottomSheetMenu(Context context) {
        mContext = context;
        mItems = new ArrayList<>();
    }

    public Context getContext() {
        return mContext;
    }

    @Override
    public MenuItem add(CharSequence title) {
        return add(0, 0, 0, title);
    }

    @Override
    public MenuItem add(int titleRes) {
        return add(0, 0, 0, titleRes);
    }

    @Override
    public MenuItem add(int groupId, int itemId, int order, CharSequence title) {
        final BottomSheetMenuItem item = new BottomSheetMenuItem(getContext(), groupId, itemId, 0, order, title);
        mItems.add(item);
        return item;
    }

    @Override
    public MenuItem add(int groupId, int itemId, int order, int titleRes) {
        return add(groupId, itemId, order, mContext.getResources().getString(titleRes));
    }

    @Override
    public SubMenu addSubMenu(CharSequence title) {
        return null;
    }

    @Override
    public SubMenu addSubMenu(int titleRes) {
        return null;
    }

    @Override
    public SubMenu addSubMenu(int groupId, int itemId, int order, CharSequence title) {
        return null;
    }

    @Override
    public SubMenu addSubMenu(int groupId, int itemId, int order, int titleRes) {
        return null;
    }

    @Override
    public int addIntentOptions(int groupId, int itemId, int order, ComponentName caller, Intent[] specifics, Intent intent,
                                int flags, MenuItem[] outSpecificItems) {
        PackageManager pm = mContext.getPackageManager();
        final List<ResolveInfo> resolveInfos = pm.queryIntentActivityOptions(caller, specifics, intent, 0);
        final int N = resolveInfos != null ? resolveInfos.size() : 0;

        if ((flags & FLAG_APPEND_TO_GROUP) == 0) {
            removeGroup(groupId);
        }

        for (int index = 0; index < N; index++) {

            final ResolveInfo resolveInfo = resolveInfos.get(index);

            Intent resolveIntent = new Intent(resolveInfo.specificIndex < 0 ? intent : specifics[resolveInfo.specificIndex]);
            resolveIntent.setComponent(new ComponentName(resolveInfo.activityInfo.applicationInfo.packageName,
                    resolveInfo.activityInfo.name));
            final MenuItem item = add(groupId, itemId, order, resolveInfo.loadLabel(pm)).setIcon(resolveInfo
                    .loadIcon(pm)).setIntent(resolveIntent);
            if (outSpecificItems != null && resolveInfo.specificIndex >= 0) {
                outSpecificItems[resolveInfo.specificIndex] = item;
            }
        }
        return N;
    }

    @Override
    public void removeItem(int id) {
        mItems.remove(findItemIndex(id));
    }

    @Override
    public void removeGroup(int groupId) {
        final ArrayList<BottomSheetMenuItem> items = mItems;
        int itemCount = items.size();
        int index = 0;
        while (index < itemCount) {
            if (items.get(index).getGroupId() == groupId) {
                items.remove(index);
                itemCount--;
            } else {
                index++;
            }
        }
    }

    @Override
    public void clear() {
        mItems.clear();
    }

    @Override
    public void setGroupCheckable(int group, boolean checkable, boolean exclusive) {
        final ArrayList<BottomSheetMenuItem> items = mItems;
        final int itemCount = items.size();

        for (int index = 0; index < itemCount; index++) {
            final BottomSheetMenuItem item = items.get(index);
            if (item.getGroupId() == group) {
                item.setCheckable(checkable);
                item.setExclusiveCheckable(exclusive);
            }
        }
    }

    @Override
    public void setGroupVisible(int group, boolean visible) {
        final ArrayList<BottomSheetMenuItem> items = mItems;
        final int itemCount = items.size();

        for (int index = 0; index < itemCount; index++) {
            final BottomSheetMenuItem item = items.get(index);
            if (item.getGroupId() == group) {
                item.setVisible(visible);
            }
        }
    }

    @Override
    public void setGroupEnabled(int group, boolean enabled) {
        final ArrayList<BottomSheetMenuItem> items = mItems;
        final int itemCount = items.size();

        for (int index = 0; index < itemCount; index++) {
            final BottomSheetMenuItem item = items.get(index);
            if (item.getGroupId() == group) {
                item.setEnabled(enabled);
            }
        }
    }

    @Override
    public boolean hasVisibleItems() {
        final ArrayList<BottomSheetMenuItem> items = mItems;
        final int itemCount = items.size();

        for (int index = 0; index < itemCount; index++) {
            if (items.get(index).isVisible()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public MenuItem findItem(int id) {
        return mItems.get(findItemIndex(id));
    }

    @Override
    public int size() {
        return mItems.size();
    }

    @Override
    public MenuItem getItem(int index) {
        return mItems.get(index);
    }

    private int findItemIndex(int id) {
        final ArrayList<BottomSheetMenuItem> items = mItems;
        final int itemCount = items.size();
        for (int index = 0; index < itemCount; index++) {
            if (items.get(index).getItemId() == id) {
                return index;
            }
        }
        return -1;
    }

    @Override
    public void close() {

    }

    private BottomSheetMenuItem findItemWithShortcut(int keyCode, KeyEvent event) {
        final boolean qwerty = mIsQwerty;
        final ArrayList<BottomSheetMenuItem> items = mItems;
        final int itemCount = items.size();

        for (int index = 0; index < itemCount; index++) {
            final BottomSheetMenuItem item = items.get(index);
            final char shortcut = qwerty ? item.getAlphabeticShortcut() : item.getNumericShortcut();
            if (keyCode == shortcut) {
                return item;
            }
        }
        return null;
    }


    @Override
    public boolean performShortcut(int keyCode, KeyEvent event, int flags) {
        BottomSheetMenuItem item = findItemWithShortcut(keyCode, event);
        if (item == null) {
            return false;
        }
        return item.invoke();
    }

    @Override
    public boolean isShortcutKey(int keyCode, KeyEvent event) {
        return findItemWithShortcut(keyCode, event) != null;
    }

    @Override
    public boolean performIdentifierAction(int id, int flags) {
        final int index = findItemIndex(id);
        if (index < 0) {
            return false;
        }
        return mItems.get(index).invoke();
    }

    @Override
    public void setQwertyMode(boolean isQwerty) {
        mIsQwerty = isQwerty;
    }
}
