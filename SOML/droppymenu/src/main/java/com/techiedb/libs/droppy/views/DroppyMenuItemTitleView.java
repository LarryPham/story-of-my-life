/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/22/15 3:05 PM.
 **/

package com.techiedb.libs.droppy.views;

import com.techiedb.libs.droppy.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public class DroppyMenuItemTitleView extends TextView {
    public DroppyMenuItemTitleView(Context context) {
        this(context, null);
    }

    public DroppyMenuItemTitleView(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.droppyMenuItemTitleStyle);
    }

    public DroppyMenuItemTitleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        final int defaultWidth = ViewGroup.LayoutParams.MATCH_PARENT;
        final int defaultMinWidth = (int) getResources().getDimension(R.dimen.default_menu_item_title_minWidth);
        final int defaultMinHeight = (int) getResources().getDimension(R.dimen.default_menu_item_title_minHeight);
        final float defaultWeight = 1;
        final int defaultColor = getResources().getColor(R.color.default_menu_item_title_textColor);
        final int defaultGravity = Gravity.CENTER_VERTICAL;
        final int defaultLayoutGravity = Gravity.END | Gravity.CENTER_VERTICAL;

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.DroppyMenuItemTitleView, defStyleAttr, 0);
        int minWidth = (int) a.getDimension(R.styleable.DroppyMenuItemTitleView_android_minWidth, defaultMinWidth);
        int minHeight = (int) a.getDimension(R.styleable.DroppyMenuItemTitleView_android_minHeight, defaultMinHeight);
        int width = a.getLayoutDimension(R.styleable.DroppyMenuItemTitleView_android_layout_width, defaultWidth);
        int height = a.getLayoutDimension(R.styleable.DroppyMenuItemTitleView_android_layout_height, ViewGroup.LayoutParams.WRAP_CONTENT);
        int color = a.getColor(R.styleable.DroppyMenuItemTitleView_android_textColor, defaultColor);

        setGravity(a.getInt(R.styleable.DroppyMenuItemTitleView_android_gravity, defaultGravity));

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(width, height);
        lp.width = width;
        lp.height = height;
        lp.weight = a.getFloat(R.styleable.DroppyMenuItemTitleView_android_layout_weight, defaultWeight);
        lp.gravity = a.getInteger(R.styleable.DroppyMenuItemTitleView_android_layout_gravity, defaultLayoutGravity);

        final float marginTop = getResources().getDimension(R.dimen.item_margin_top);
        final float marginBottom = getResources().getDimension(R.dimen.item_margin_bottom);
        lp.topMargin = (int) marginTop;
        lp.bottomMargin = (int) marginBottom;

        setLayoutParams(lp);
        setMinHeight(minWidth);
        setMinHeight(minHeight);
        Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "SlaboSmaller-Regular.ttf");
        setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        setTypeface(typeface);
        setTextColor(color);
    }
}
