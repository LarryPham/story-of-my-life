/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/22/15 3:07 PM.
 **/

package com.techiedb.libs.droppy.views;

import com.techiedb.libs.droppy.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.FrameLayout;

public class DroppyMenuPopupView extends FrameLayout {
    public DroppyMenuPopupView(Context context) {
        this(context, null);
    }

    public DroppyMenuPopupView(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.droppyPopupStyle);
    }

    public DroppyMenuPopupView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        final Drawable defaultDrawable = getResources().getDrawable(R.drawable.default_popup_background);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.DroppyMenuPopupView, defStyleAttr, 0);
        Drawable background = a.getDrawable(R.styleable.DroppyMenuPopupView_android_background);
        int height = a.getLayoutDimension(R.styleable.DroppyMenuPopupView_android_layout_height, ViewGroup.LayoutParams.WRAP_CONTENT);
        int width = a.getLayoutDimension(R.styleable.DroppyMenuPopupView_android_layout_width, ViewGroup.LayoutParams.WRAP_CONTENT);
        ViewGroup.LayoutParams lp = getLayoutParams();
        if (lp == null) {
            lp = new ViewGroup.LayoutParams(width, height);
        } else {
            lp.width = width;
            lp.height = height;
        }


        this.setLayoutParams(lp);

        if (background != null) {
            setBackgroundDrawable(background);
        } else {
            setBackgroundDrawable(defaultDrawable);
        }
    }
}
