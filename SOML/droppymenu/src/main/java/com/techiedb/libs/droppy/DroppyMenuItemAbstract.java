/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/22/15 3:12 PM.
 **/

package com.techiedb.libs.droppy;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class DroppyMenuItemAbstract implements DroppyMenuItemInterface {
    final protected static int TYPE_MENU_ITEM = 0;
    final protected static int TYPE_CUSTOM = 1;
    final protected static int TYPE_MENU_SEPARATOR = 2;

    protected String title;
    protected int icon = -1;
    protected int type = TYPE_MENU_ITEM;
    protected View renderedView;
    protected int customViewResourceId = -1;
    protected int id = -1;
    protected boolean isClickable = true;

    @Override
    public View render(Context context) {
        if (this.renderedView == null) {
            this.renderedView = LayoutInflater.from(context).inflate(this.customViewResourceId, null);
        }
        ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        renderedView.setLayoutParams(lp);

        return renderedView;
    }

    @Override
    public int getType() {
        return type;
    }

    @Override
    public DroppyMenuItemInterface setId(int id) {
        this.id = id;

        return this;
    }

    @Override
    public DroppyMenuItemInterface setClickable(boolean isClickable) {
        this.isClickable = isClickable;

        return this;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public boolean isClickable() {
        return isClickable;
    }

    @Override
    public View getView() {
        return renderedView;
    }
}
