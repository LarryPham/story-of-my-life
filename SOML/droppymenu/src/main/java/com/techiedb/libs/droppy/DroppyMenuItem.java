/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/22/15 3:14 PM.
 **/

package com.techiedb.libs.droppy;

import com.techiedb.libs.droppy.views.DroppyMenuItemIconView;
import com.techiedb.libs.droppy.views.DroppyMenuItemTitleView;
import com.techiedb.libs.droppy.views.DroppyMenuItemView;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;

public class DroppyMenuItem extends DroppyMenuItemAbstract {
    private Drawable iconDrawable;
    protected DroppyMenuItemView renderedView;

    void initMenuItem(String title, int iconResourceId)
    {
        this.title = title;
        if (iconResourceId > 0) {
            this.icon = iconResourceId;
        }
    }

    public DroppyMenuItem(String title)
    {
        initMenuItem(title, -1);
    }

    public DroppyMenuItem(String title, int iconResourceId)
    {
        initMenuItem(title, iconResourceId);

    }

    public void setIcon(Drawable iconDrawable)
    {
        this.iconDrawable = iconDrawable;
    }

    @Override
    public View render(Context context) {

        renderedView = new DroppyMenuItemView(context);

        if (this.icon != -1) {
            DroppyMenuItemIconView droppyMenuItemIcon = new DroppyMenuItemIconView(context);
            droppyMenuItemIcon.setImageResource(this.icon);
        } else if (this.iconDrawable != null) {
            DroppyMenuItemIconView droppyMenuItemIcon = new DroppyMenuItemIconView(context);
            droppyMenuItemIcon.setImageDrawable(iconDrawable);
            renderedView.addView(droppyMenuItemIcon);
        }

        DroppyMenuItemTitleView droppyMenuItemTitle = new DroppyMenuItemTitleView(context);
        droppyMenuItemTitle.setText(this.title);
        renderedView.addView(droppyMenuItemTitle);

        return renderedView;
    }
}
