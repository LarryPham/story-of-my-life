/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/22/15 3:00 PM.
 **/

package com.techiedb.libs.droppy.views;

import com.techiedb.libs.droppy.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class DroppyMenuItemIconView extends ImageView {

    public DroppyMenuItemIconView(Context context) {
        this (context, null);
    }

    public DroppyMenuItemIconView(Context context, AttributeSet attrs) {
        this (context, attrs, R.attr.droppyMenuItemIconStyle);
    }

    public DroppyMenuItemIconView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        final int defaultMaxWidth = (int) getResources().getDimension(R.dimen.default_menu_item_icon_maxWidth);
        final int defaultMaxHeight = (int) getResources().getDimension(R.dimen.default_menu_item_icon_maxHeight);
        final float defaultWeight = 0;

        final int defaultLayoutGravity = Gravity.START | Gravity.CENTER_VERTICAL;
        final int defaultMarginLeft = (int) getResources().getDimension(R.dimen.default_menu_item_icon_marginLeft);
        final int defaultMarginRight = (int) getResources().getDimension(R.dimen.default_menu_item_icon_marginRight);

        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.DroppyMenuItemIconView, defStyleAttr, 0);
        int maxWidth = (int) array.getDimension(R.styleable.DroppyMenuItemIconView_android_maxWidth, defaultMaxWidth);
        int maxHeight = (int) array.getDimension(R.styleable.DroppyMenuItemIconView_android_maxHeight, defaultMaxHeight);
        int width = array.getLayoutDimension(R.styleable.DroppyMenuItemIconView_android_layout_width, ViewGroup.LayoutParams.WRAP_CONTENT);
        int height = array.getLayoutDimension(R.styleable.DroppyMenuItemIconView_android_layout_height, ViewGroup.LayoutParams.WRAP_CONTENT);

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(width, height);
        lp.rightMargin = array.getDimensionPixelSize(R.styleable.DroppyMenuItemIconView_android_layout_marginRight, defaultMarginRight);
        lp.leftMargin = array.getDimensionPixelSize(R.styleable.DroppyMenuItemIconView_android_layout_marginLeft, defaultMarginLeft);
        lp.width = width;
        lp.height = height;
        lp.weight = array.getFloat(R.styleable.DroppyMenuItemIconView_android_layout_weight, defaultWeight);
        lp.gravity = array.getInteger(R.styleable.DroppyMenuItemIconView_android_layout_gravity, defaultLayoutGravity);

        setMaxHeight(maxWidth);
        setMaxHeight(maxHeight);
        setLayoutParams(lp);
        array.recycle();
    }
}
