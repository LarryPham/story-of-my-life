/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/22/15 3:08 PM.
 **/

package com.techiedb.libs.droppy.views;

import com.techiedb.libs.droppy.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.LinearLayout;

public class DroppyMenuSeparatorView extends LinearLayout {
    public DroppyMenuSeparatorView(Context context) {
        this(context, null);
    }

    public DroppyMenuSeparatorView(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.droppyMenuSeparatorStyle);
    }

    public DroppyMenuSeparatorView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.DroppyMenuItemView, defStyleAttr, 0);

        final Drawable defaultSeparatorBackground = getResources().getDrawable(R.drawable.droppy_separator_background);
        final int defaultHeight = getResources().getDimensionPixelSize(R.dimen.default_menu_separator_height);
        ViewGroup.LayoutParams lp = getLayoutParams();
        int width = a.getLayoutDimension(R.styleable.DroppyMenuItemView_android_layout_width, ViewGroup.LayoutParams.MATCH_PARENT);
        int height = a.getLayoutDimension(R.styleable.DroppyMenuItemView_android_layout_height, defaultHeight);
        if (lp == null) {
            lp = new ViewGroup.LayoutParams(width, height);
        } else {
            lp.width = width;
            lp.height = height;
        }
        setOrientation(LinearLayout.HORIZONTAL);

        Drawable background = a.getDrawable(R.styleable.DroppyMenuSeparatorView_android_background);
        if (background != null) {
            setBackgroundDrawable(background);
        } else {
            setBackgroundDrawable(defaultSeparatorBackground);
        }

        this.setLayoutParams(lp);
        a.recycle();
    }

}
