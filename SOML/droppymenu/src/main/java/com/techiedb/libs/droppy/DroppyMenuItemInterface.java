/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/22/15 3:11 PM.
 **/

package com.techiedb.libs.droppy;

import android.content.Context;
import android.view.View;

public interface DroppyMenuItemInterface {

    int getType();

    View render(Context context);

    DroppyMenuItemInterface setId(int id);

    DroppyMenuItemInterface setClickable(boolean isClickable);

    int getId();

    boolean isClickable();

    View getView();
}
