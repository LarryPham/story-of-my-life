/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/4/15 11:30 AM.
 **/

package co.soml.android.mediapicker.source;

import android.content.Context;
import android.hardware.Camera;
import android.os.Parcel;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import co.soml.android.mediapicker.MediaItem;
import co.soml.android.mediapicker.MediaUtils;

public class MediaSourceCaptureImage implements MediaSource, Camera.PictureCallback {

    @Override
    public void gather(Context context) {

    }

    @Override
    public void cleanUp() {

    }

    @Override
    public void setListener(OnMediaChange listener) {

    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public MediaItem getMedia(int position) {
        return null;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, LayoutInflater inflater, MediaUtils.ImageCache cache) {
        return null;
    }

    @Override
    public boolean onMediaItemSelected(MediaItem mediaItem, boolean selected) {
        return false;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {

    }
}
