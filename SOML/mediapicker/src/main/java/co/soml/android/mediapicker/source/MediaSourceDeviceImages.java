/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/4/15 11:32 AM.
 **/

package co.soml.android.mediapicker.source;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Parcel;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import co.soml.android.mediapicker.MediaItem;
import co.soml.android.mediapicker.MediaUtils;
import co.soml.android.mediapicker.R;

public class MediaSourceDeviceImages implements MediaSource {
    public static final String TAG = MediaSourceDeviceImages.class.getSimpleName();

    private static final String[] THUMBNAIL_QUERY_COLUMNS = {
            MediaStore.Images.Thumbnails._ID,
            MediaStore.Images.Thumbnails.DATA,
            MediaStore.Images.Thumbnails.IMAGE_ID
    };
    private static final String[] IMAGE_QUERY_COLUMNS = {
            MediaStore.Images.Media._ID,
            MediaStore.Images.Media.DATA,
            MediaStore.Images.Media.DATE_TAKEN,
            MediaStore.Images.Media.ORIENTATION
    };

    protected final List<MediaItem> mMediaItemList;

    protected Context mContext;
    private OnMediaChange mListener;
    private AsyncTask<Void, String, Void> mGatheringTask;

    public MediaSourceDeviceImages() {
        mMediaItemList = new ArrayList<>();
    }

    protected List<MediaItem> createMediaItems() {
        Cursor thumbnailCursor = MediaUtils.getMediaStoreThumbnails(mContext.getContentResolver(), THUMBNAIL_QUERY_COLUMNS);
        Map<String,String> thumbnailData = MediaUtils.getMediaStoreThumbnailData(thumbnailCursor, MediaStore.Images.Thumbnails.DATA,
                MediaStore.Images.Thumbnails.IMAGE_ID);
        return MediaUtils.createMediaItems(thumbnailData, MediaStore.Images.Media.query(mContext.getContentResolver(),
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, IMAGE_QUERY_COLUMNS, null, null, MediaStore.MediaColumns.DATE_MODIFIED +
                        "DESC"), MediaUtils.BackgroundFetchThumbnail.TYPE_IMAGE);
    }

    @Override
    public void gather(Context context) {
        if (mGatheringTask != null) {
            mGatheringTask.cancel(true);
        }

        mContext = context;
        mGatheringTask = new GatherDeviceImagesTask();
        mGatheringTask.execute();
    }

    @Override
    public void cleanUp() {
        if (mGatheringTask != null) {
            // cancel gathering media data immediately, do not wait for the task to finish
            mGatheringTask.cancel(true);
        }
        mMediaItemList.clear();
    }

    @Override
    public void setListener(final OnMediaChange listener) {
        mListener = listener;
    }

    @Override
    public int getCount() {
        return mMediaItemList.size();
    }

    @Override
    public MediaItem getMedia(int position) {
        return (position < mMediaItemList.size()) ? mMediaItemList.get(position) : null;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent, LayoutInflater inflater, final MediaUtils.ImageCache cache) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.media_item_image, parent, false);
        }
        final MediaItem mediaItem = mMediaItemList.get(position);
        if (convertView != null && mediaItem != null) {
            final ImageView imageView = (ImageView) convertView.findViewById(R.id.image_view_background);
            final Uri imageSource;
            if (mediaItem.getPreviewSource() != null && !mediaItem.getPreviewSource().toString().isEmpty()) {
                imageSource = mediaItem.getPreviewSource();
            } else {
                imageSource = mediaItem.getSource();
            }

            if (imageView != null) {
                int width = imageView.getWidth();
                int height = imageView.getHeight();

                if (width <= 0 || height <= 0) {
                    imageView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                        @Override
                        public boolean onPreDraw() {
                            int width = imageView.getWidth();
                            int height = imageView.getHeight();
                            MediaUtils.fadeMediaItemImageIntoView(imageSource, cache, imageView, mediaItem, width, height,
                                    MediaUtils.BackgroundFetchThumbnail.TYPE_IMAGE);
                            imageView.getViewTreeObserver().removeOnPreDrawListener(this);
                            return true;
                        }
                    });
                } else {
                    MediaUtils.fadeMediaItemImageIntoView(imageSource, cache, imageView, mediaItem, width, height, MediaUtils
                            .BackgroundFetchThumbnail.TYPE_IMAGE);
                }
            }
        }
        return convertView;
    }

    @Override
    public boolean onMediaItemSelected(MediaItem mediaItem, boolean selected) {
        return !selected;
    }

    /**
     * Clears the current media items then adds the provided items.
     */
    protected void setMediaItems(List<MediaItem> mediaItems) {
        mMediaItemList.clear();
        mMediaItemList.addAll(mediaItems);
    }

    /**
     * Invokes {@link co.soml.android.mediapicker.source.MediaSource.OnMediaChange#onMediaLoaded(boolean)} if {@link #mListener} is not
     * null.
     * @param success pass through parameter
     */
    protected void notifyMediaLoaded(boolean success) {
        if (mListener != null) {
            mListener.onMediaLoaded(success);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(mMediaItemList);
    }

    /**
     * {@link android.os.Parcelable} interface
     */
    public static final Creator<MediaSourceDeviceImages> CREATOR = new Creator<MediaSourceDeviceImages>() {
        @Override
        public MediaSourceDeviceImages createFromParcel(Parcel source) {
            List<MediaItem> parcelData = new ArrayList<>();
            source.readTypedList(parcelData, MediaItem.CREATOR);
            MediaSourceDeviceImages newItem = new MediaSourceDeviceImages();

            if (parcelData.size() > 0) {
                newItem.setMediaItems(parcelData);
            }

            return newItem;
        }

        @Override
        public MediaSourceDeviceImages[] newArray(int size) {
            return new MediaSourceDeviceImages[size];
        }
    };

    /**
     * Gathers media items on a background thread.
     */
    protected class GatherDeviceImagesTask extends AsyncTask<Void, String, Void> {

        @Override
        protected void onPreExecute() {
            mMediaItemList.clear();
        }

        @Override
        protected Void doInBackground(Void... params) {
            mMediaItemList.addAll(createMediaItems());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            notifyMediaLoaded(true);
            nullGatheringReference();
        }

        @Override
        protected void onCancelled(Void aVoid) {
            nullGatheringReference();
        }

        protected void nullGatheringReference() {
            if (mGatheringTask == this) {
                mGatheringTask = null;
            }
        }
    }
}
