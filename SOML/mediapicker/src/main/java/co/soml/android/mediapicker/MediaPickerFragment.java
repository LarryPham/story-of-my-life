/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/5/15 10:29 AM.
 **/

package co.soml.android.mediapicker;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import co.soml.android.mediapicker.source.MediaSource;

public class MediaPickerFragment extends Fragment implements AdapterView.OnItemClickListener, AbsListView.MultiChoiceModeListener,
        MediaSource.OnMediaChange{
    public static final String TAG = MediaPickerFragment.class.getSimpleName();
    public static final String KEY_SELECTED_CONTENT = "key-selected-content";
    public static final String KEY_MEDIA_SOURCES = "key-media-sources";
    public static final String KEY_CUSTOM_LAYOUT = "key-custom-view";
    public static final String KEY_ACTION_MODE_MENU = "key-action-mode-menu";

    public static final String KEY_LOADING_TEXT = "key-loading-text";
    public static final String KEY_EMPTY_TEXT ="key-empty-text";
    public static final String KEY_ERROR_TEXT = "key-error-text";

    private static final int DEFAULT_VIEW = R.layout.media_picker_fragment;

    public interface OnMediaSelected {
        void onMediaSelectionStarted();
        void onMediaSelected(MediaItem mediaItem, boolean selected);
        void onMediaSelectionConfirmed(ArrayList<MediaItem> mediaContent);
        void onMediaSelectionCancelled();
        boolean onMenuItemSelected(MenuItem menuItem, ArrayList<MediaItem> mediaContent);
        MediaUtils.ImageCache getImageCache();
    }

    private final ArrayList<MediaSource> mMediaSources;
    private final ArrayList<MediaItem> mSelectedContent;

    private OnMediaSelected mListener;

    private boolean mConfirmed;

    private TextView mEmptyView;
    private AbsListView mAdapterView;

    private MediaSourceAdapter mAdapter;
    private int mCustomLayout;
    private int mActionModeMenu;

    private String mLoadingText;
    private String mEmptyText;
    private String mErrorText;

    public MediaPickerFragment() {
        super();

        mCustomLayout = -1;
        mActionModeMenu = -1;
        mMediaSources = new ArrayList<>();
        mSelectedContent = new ArrayList<>();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (mListener == null && activity instanceof OnMediaSelected) {
            mListener = (OnMediaSelected) activity;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            savedInstanceState = getArguments();
        }

        restoreFromBundle(savedInstanceState);

        setDefaultTextValues();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        restoreFromBundle(savedInstanceState);
        int viewToInflate = mCustomLayout < 0 ? DEFAULT_VIEW : mCustomLayout;
        View mediaPickerView = inflater.inflate(viewToInflate, container, false);
        if (mediaPickerView != null) {
            if (mEmptyView == null) {
                mEmptyView = (TextView) mediaPickerView.findViewById(R.id.media_empty_view);
                if (mMediaSources.size() == 0) {
                    updateEmptyView(getString(R.string.error_fetching_media));
                } else {
                    updateEmptyView(mLoadingText);
                }
            } else {
                mEmptyView = (TextView) mediaPickerView.findViewById(R.id.media_empty_view);
                if (mAdapter.getCount() == 0) {
                    updateEmptyView(mEmptyText);
                }
            }

            mAdapterView = (AbsListView) mediaPickerView.findViewById(R.id.media_adapter_view);
            if (mAdapterView != null) {
                layoutAdapterView();
                if (mAdapter == null) {
                    generateAdapter();
                } else {
                    mAdapterView.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();
                }
                toggleEmptyVisibility();
            }
        }
        return mediaPickerView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        cleanUpMediaSources();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (mSelectedContent.size() > 0) {
            outState.putParcelableArrayList(KEY_SELECTED_CONTENT, mSelectedContent);
        }

        if (mMediaSources.size() > 0) {
            outState.putParcelableArrayList(KEY_MEDIA_SOURCES, mMediaSources);
        }

        if (mCustomLayout > -1) {
            outState.putInt(KEY_CUSTOM_LAYOUT, mCustomLayout);
        }

        if (mLoadingText != null) {
            outState.putString(KEY_LOADING_TEXT, mLoadingText);
        }

        if (mErrorText != null) {
            outState.putString(KEY_ERROR_TEXT, mErrorText);
        }

        if (mEmptyText != null) {
            outState.putString(KEY_EMPTY_TEXT, mEmptyText);
        }
    }

    /**
     * Restores state from a given {@link android.os.Bundle }. Checks for media sources, selected content, custom view, custom action
     * mode menu, and custom empty text.
     *
     * @param bundle
     * Bundle containing all the data, can be null.
     */
    private void restoreFromBundle(Bundle bundle) {
        if (bundle != null) {
            if (bundle.containsKey(KEY_MEDIA_SOURCES)) {
                ArrayList<MediaSource> mediaSources = bundle.getParcelableArrayList(KEY_MEDIA_SOURCES);
                setMediaSources(mediaSources);

                if (bundle.containsKey(KEY_SELECTED_CONTENT)) {
                    ArrayList<MediaItem> mediaItems = bundle.getParcelableArrayList(KEY_SELECTED_CONTENT);

                    if (mediaItems != null) {
                        mSelectedContent.addAll(mediaItems);
                    }
                }

                if (bundle.containsKey(KEY_CUSTOM_LAYOUT)) {
                    setCustomLayout(bundle.getInt(KEY_ACTION_MODE_MENU, -1));
                }

                if (bundle.containsKey(KEY_ACTION_MODE_MENU)) {
                    mLoadingText = bundle.getString(KEY_LOADING_TEXT, mLoadingText);
                }

                if (bundle.containsKey(KEY_EMPTY_TEXT)) {
                    mEmptyText = bundle.getString(KEY_EMPTY_TEXT, mEmptyText);
                }

                if (bundle.containsKey(KEY_ERROR_TEXT)) {
                    mErrorText = bundle.getString(KEY_ERROR_TEXT, mErrorText);
                }
            }

        }
    }
    @Override
    public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
        notifyMediaSelected(position, true);
        if (checked) {
            if (!mSelectedContent.contains(mAdapter.getItem(position))) {
                mSelectedContent.add(mAdapter.getItem(position));
            }
        } else {
            mSelectedContent.remove(mAdapter.getItem(position));
        }
        mode.setTitle(getActivity().getTitle() + " (" + mSelectedContent.size() + ")");
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        mode.setTitle(getActivity().getTitle());
        getActivity().onActionModeStarted(mode);
        inflateActionModeMenu(menu);

        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        notifyMediaSelectionStarted();
        mConfirmed = false;

        return true;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        if (item.getItemId() == R.id.menu_action_selection_confirmed) {
            notifyMediaSelectionConfirmed();
            mode.finish();
            return true;
        } else if (mListener != null) {
            return mListener.onMenuItemSelected(item, mSelectedContent);
        }

        return false;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        if (!mConfirmed) {
            notifyMediaSelectionCancelled();
        }

        mSelectedContent.clear();
        getActivity().onActionModeFinished(mode);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (!notifyMediaSelected(position, true)) {
            notifyMediaSelectionConfirmed();
        }
    }

    @Override
    public void onMediaLoaded(boolean success) {
        if (success) {
            if (mAdapter != null && mAdapter.getCount() > 0) {
                toggleEmptyVisibility();
                mAdapter.notifyDataSetChanged();;
            } else {
                updateEmptyView(mEmptyText);
            }
        } else {
            updateEmptyView(mErrorText);
        }
    }

    @Override
    public void onMediaAdded(MediaSource source, List<MediaItem> addedItems) {
        toggleEmptyVisibility();
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onMediaRemoved(MediaSource source, List<MediaItem> removedItems) {
        toggleEmptyVisibility();
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onMediaChanged(MediaSource source, List<MediaItem> changedItems) {
        mAdapter.notifyDataSetChanged();
    }

    /**
     * Sets the listener. Calling this method will overwrite the current listener.
     * @param selected
     * the new listener, can be null
     */
    public void setListener(OnMediaSelected selected) {
        mListener = selected;
    }
    public void setCustomLayout(int customLayout) {
        mCustomLayout = customLayout;
    }

    /**
     * Sets the {@link MediaSource }'s to be presented. The current sources and selected cotnent are cleaned up and cleared before the
     * new list added.
     *
     * @param sources the new sources.
     */
    public void setMediaSources(ArrayList<MediaSource> sources) {
        mSelectedContent.clear();

        cleanUpMediaSources();
        mMediaSources.clear();

        if (sources != null) {
            mMediaSources.addAll(sources);
            generateAdapter();
        }
    }

    /**
     * Sets the default empty text strings if they are not already set to something
     */
    private void setDefaultTextValues() {
        if (mLoadingText == null)  setLoadingText(getString(R.string.fetching_media));
        if (mEmptyText == null) setEmptyText(getString(R.string.no_media));
        if (mErrorText == null) setErrorText(getString(R.string.error_fetching_media));
    }

    public void setLoadingText(int resId) {
        if (resId < 0) {
            setLoadingText(null);
        } else {
            setLoadingText(getString(resId));
        }
    }

    public void setKeyEmptyText(int resId) {
        if (resId < 0) {
            setEmptyText(null);
        } else {
            setEmptyText(getString(resId));
        }
    }

    public void setErrorText(int resId) {
        if (resId < 0) {
            setErrorText(null);
        } else {
            setErrorText(getString(resId));
        }
    }

    public void setLoadingText(String loadingText) {
        mLoadingText = loadingText;
    }

    public void setEmptyText(String emptyText) {
        mEmptyText = emptyText;
    }

    public void setErrorText(String errorText) {
        mErrorText = errorText;
    }

    /**
     * Sets the adapter
     *
     * @param adapter the new instance of the <code>MediaSourceAdapter</code>
     */
    public void setAdapter(MediaSourceAdapter adapter) {
        mAdapter = adapter;

        if (mAdapterView != null) {
            mAdapterView.setAdapter(mAdapter);
        }
    }

    private void updateEmptyView(String text) {
        if (mEmptyView != null) {
            mEmptyView.setText(text);
        }
    }

    /**
     * Call {@link MediaSource.OnMediaChange#cleanUp()} on all of non-null sources.
     */
    private void cleanUpMediaSources() {
        for (MediaSource source : mMediaSources) {
            if (source != null) {
                source.cleanUp();
            }
        }
    }

    /**
     * Constructs the {@link MediaSourceAdapter} and attaches it to the adapter view if possible.
     */
    private void generateAdapter() {
        Activity activity = getActivity();
        if (activity != null) {
            MediaUtils.ImageCache imageCache = mListener.getImageCache();
            MediaSourceAdapter adapter = new MediaSourceAdapter(activity, mMediaSources,imageCache);

            adapter.gatherFromSource(this);
            setAdapter(adapter);
        }
    }
    /**
     * Creates the {@link MediaSourceAdapter} and initializes the adapter view to display it.
     */
    private void layoutAdapterView() {
        Activity activity = getActivity();
        Resources resources = activity.getResources();
        Drawable background = resources.getDrawable(R.drawable.media_picker_background);

        mAdapterView.setBackgroundDrawable(background);
        mAdapterView.setClipToPadding(false);
        mAdapterView.setMultiChoiceModeListener(this);
        mAdapterView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE_MODAL);
        mAdapterView.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
    }
    /**
     * Inflates custom mActionModeMenu if set, otherwise inflates default media_picker_action_mode
     */
    private void inflateActionModeMenu(Menu menu) {
        MenuInflater menuInflater = getActivity().getMenuInflater();

        if (mActionModeMenu != -1) {
            menuInflater.inflate(mActionModeMenu, menu);
            addSelectionConfirmationButtonMenuItem(menu);
        }
    }

    /**
     * Adds a menu item to confirm media selection during Action Mode. Only adds one if one is not defined.
     * @param menu
     * the menu to add a confirm option to
     */
    private void addSelectionConfirmationButtonMenuItem(Menu menu) {
        if (menu != null && menu.findItem(R.id.menu_action_selection_confirmed) == null) {
            menu.add(Menu.NONE, R.id.menu_action_selection_confirmed, Menu.FIRST, R.string.confirm)
            .setIcon(R.drawable.action_mode_confirm_checkmark);
        }
    }

    /**
     * If the current adapter does not have any items the empty view will be shown and the adapter view will be hidden. Otherwise the
     * empty view will be hidden and the adapter view presented.
     */
    private void toggleEmptyVisibility() {
        int empty = (mAdapter != null && mAdapter.getCount() == 0) ? View.VISIBLE : View.GONE;
        int adapter = (mAdapter != null && mAdapter.getCount() == 0) ? View.GONE : View.VISIBLE;

        if (mEmptyView != null) {
            mEmptyView.setVisibility(empty);
        }

        if (mAdapterView != null) {
            mAdapterView.setVisibility(adapter);
        }
    }

    /**
     * Notifies non-null listener that media selection has started
     */
    private void notifyMediaSelectionStarted() {
        if (mListener != null) {
            mListener.onMediaSelectionStarted();
        }
    }

    /**
     * Notifies non-null listener when selection state changes on a media-item
     */
    private boolean notifyMediaSelected(int position, boolean selected) {
        MediaItem mediaItem = mAdapter.getItem(position);

        if (mediaItem != null) {
            MediaSource mediaSource = mAdapter.sourceAtPosition(position);

            if (mediaSource == null || !mediaSource.onMediaItemSelected(mediaItem, selected)) {
                if (mListener != null) {
                    mListener.onMediaSelected(mediaItem, selected);
                }

                mSelectedContent.add(mediaItem);
                return false;
            }
        }
        return true;
    }

    /**
     * Notifies non-null listener that media selection has been confirmed
     */
    private void notifyMediaSelectionConfirmed() {
        if (mListener != null) {
            mListener.onMediaSelectionConfirmed(mSelectedContent);
        }
        mConfirmed = true;
    }

    /**
     * Notifies non-null listener that media selection has been cancelled.
     */
    private void notifyMediaSelectionCancelled() {
        if (mListener != null) {
            mListener.onMediaSelectionCancelled();
        }
    }
}
