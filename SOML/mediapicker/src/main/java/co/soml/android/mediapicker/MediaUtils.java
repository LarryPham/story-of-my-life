/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/4/15 11:39 AM.
 **/

package co.soml.android.mediapicker;

import android.content.ContentResolver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;

import java.lang.ref.WeakReference;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

public class MediaUtils {

    private static final long FADE_TIME_MS = 250;

    public static void fadeInImage(ImageView imageView, Bitmap bitmap) {
        fadeInImage(imageView, bitmap, FADE_TIME_MS);
    }

    public static void fadeInImage(ImageView imageView, Bitmap image, long duration) {
        if (imageView != null) {
            imageView.setImageBitmap(image);
            Animation alpha = new AlphaAnimation(0.25f, 1.0f);
            alpha.setDuration(duration);
            imageView.startAnimation(alpha);
        }
    }

    public static Cursor getMediaStoreThumbnails(ContentResolver contentResolver, String[] columns) {
        if (contentResolver == null) return null;
        Uri thumbnailUri = MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI;
        return MediaStore.Images.Thumbnails.query(contentResolver, thumbnailUri, columns);
    }

    public static Cursor getDeviceMediaStoreVideos(ContentResolver contentResolver, String[] columns) {
        if (contentResolver == null) return null;
        Uri videoUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
        return MediaStore.Video.query(contentResolver, videoUri, columns);
    }

    public static Map<String,String> getMediaStoreThumbnailData(Cursor thumbnailCursor, String dataColumnName, String idColumnName) {
        final Map<String, String> data = new HashMap<>();

        if (thumbnailCursor != null) {
            if (thumbnailCursor.moveToFirst()) {
                do {
                    int dataColumnIndex = thumbnailCursor.getColumnIndex(dataColumnName);
                    int imageIdColumnIndex = thumbnailCursor.getColumnIndex(idColumnName);

                    if (dataColumnIndex != -1 && imageIdColumnIndex != -1) {
                        data.put(thumbnailCursor.getString(imageIdColumnIndex), thumbnailCursor.getString(dataColumnIndex));
                    }
                } while (thumbnailCursor.moveToNext());
            }

            thumbnailCursor.close();
        }
        return data;
    }

    public static List<MediaItem> createMediaItems(Map<String, String> thumbnailData, Cursor mediaCursor, int type) {
        final List<MediaItem> mediaItems = new ArrayList<>();
        final List<String> ids = new ArrayList<>();

        if (mediaCursor != null) {
            if (mediaCursor.moveToFirst()) {
                do {
                    MediaItem newContent = type == BackgroundFetchThumbnail.TYPE_IMAGE ? getMediaItemFromImageCursor(mediaCursor,
                            thumbnailData) : new MediaItem();
                    if (newContent != null && !ids.contains(newContent.getTag())) {
                        mediaItems.add(newContent);
                        ids.add(newContent.getTag());
                    }
                } while (mediaCursor.moveToNext());
            }
            mediaCursor.close();
        }
        return mediaItems;
    }

    public static void fadeMediaItemImageIntoView(Uri imageSource, ImageCache cache, ImageView imageView, MediaItem mediaItem, int width,
                                                  int height, int type) {
        if (imageSource != null && !imageSource.toString().isEmpty()) {
            Bitmap imageBitmap = null;
            if (cache != null) {
                imageBitmap = cache.getBitmap(imageSource.toString());
            }

            if (imageBitmap == null) {
                imageView.setImageResource(R.drawable.default_img);
                BackgroundFetchThumbnail backgroundDownload = new MediaUtils.BackgroundFetchThumbnail(imageView, cache, type, width,
                        height, mediaItem.getRotation());
                imageView.setTag(backgroundDownload);
                backgroundDownload.executeWithLimit(imageSource);
            } else {
                fadeInImage(imageView, imageBitmap);
            }
        } else {
            imageView.setTag(null);
            imageView.setImageResource(R.drawable.default_img);
        }
    }

    /**
     * Implementation of AsyncTask that limits the number of executions. Child classes must call super.* methods fo all of
     * onPreExecute/doInBackground/onPostExecute/onCancelled or none.
     *
     * Subclasses are passes a generic Object in startExecution and it is expected to be cast to the appropriate type when calling
     * execute/executeOnExecutor.
     */
    public static abstract class LimitedBackgroundOperation<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {
        private static final int MAX_FETCHES = 5;
        private static final Queue<LimitedBackgroundOperation> sFetchQueue = new LinkedList<>();
        private static int sNumFetching = 0;
        private Params mParams;

        @Override
        protected void onPreExecute() {
            performPreExecute();
            ++sNumFetching;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected final Result doInBackground(Params... params) {
            return performBackgroundOperation(params);
        }

        @Override
        protected void onPostExecute(Result result) {
            performPostExecute(result);
            continueExclusiveExecution();
        }

        @Override
        protected void onCancelled(Result result) {
            performCancelled(result);
            continueExclusiveExecution();
        }

        protected void performPreExecute() {

        }

        protected void performPostExecute(Result result) {

        }

        protected void performCancelled(Result result) {

        }

        public abstract void startExecution(Object params);

        public void executeWithLimit(Params params) {
            mParams = params;
            startExclusiveExecution();
        }

        private void startExclusiveExecution() {
            if (sNumFetching < MAX_FETCHES) {
                startExecution(mParams);
            } else {
                sFetchQueue.add(this);
            }
        }

        private void continueExclusiveExecution() {
            if (--sNumFetching < MAX_FETCHES && sFetchQueue.size() > 0) {
                LimitedBackgroundOperation next = sFetchQueue.remove();
                if (next != null) {
                    next.startExecution(next.mParams);
                }
            }
        }

        @SuppressWarnings("unchecked")
        public abstract Result performBackgroundOperation(Params... params);
    }

    public static class BackgroundFetchThumbnail extends LimitedBackgroundOperation<Uri, String, Bitmap> {
        public static final int TYPE_IMAGE = 0;
        public static final int TYPE_AUDIO = 1;

        private WeakReference<ImageView> mReference;
        private ImageCache mCache;
        private int mType;
        private int mWidth;
        private int mHeight;
        private int mRotation;

        public BackgroundFetchThumbnail(ImageView resultStore, ImageCache cache, int type, int width, int height, int rotation) {
            this.mReference = new WeakReference<ImageView>(resultStore);
            mCache = cache;
            mType = type;
            mWidth = width;
            mHeight = height;
            mRotation = rotation;
        }

        @Override
        protected void performPostExecute(Bitmap bitmap) {
            ImageView imageView = mReference.get();
            if (imageView != null) {
                if (imageView.getTag() == this) {
                    imageView.setTag(null);

                    if (bitmap == null) {
                        imageView.setImageResource(R.drawable.default_img);
                    } else {
                        fadeInImage(imageView, bitmap);
                    }
                }
            }
        }

        @Override
        public void startExecution(Object params) {
            if (!(params instanceof Uri)) {
                throw new IllegalArgumentException("Params must of type Uri");
            }

            executeOnExecutor(THREAD_POOL_EXECUTOR, (Uri) params);
        }

        @Override
        public Bitmap performBackgroundOperation(Uri... params) {
            String uri = params[0].toString();
            Bitmap bitmap = null;

            if (mType == TYPE_IMAGE) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(uri, options);
                options.inJustDecodeBounds = false;
                options.inSampleSize = calculateSampleSize(options);
                bitmap = BitmapFactory.decodeFile(uri, options);

                if (bitmap != null) {
                    Matrix rotationMatrix = new Matrix();
                    rotationMatrix.setRotate(mRotation, bitmap.getWidth() / 2.0f, bitmap.getHeight() / 2.0f);
                    bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), rotationMatrix, false);
                }
            } else if (mType == TYPE_AUDIO) {
                //bitmap = ThumbnailUtils.createVideoThumbnail(uri, MediaStore.)
            }

            if (mCache != null && bitmap != null) {
                mCache.putBitmap(uri, bitmap);
            }
            return bitmap;
        }

        private int calculateSampleSize(BitmapFactory.Options options) {
            final int height = options.outHeight;
            final int width = options.outWidth;
            int inSampleSize = 1;

            if (height > mHeight || width > mWidth) {
                final int halfHeight = height / 2;
                final int halfWidth = width / 2;

                while ((halfHeight / inSampleSize) > mHeight && (halfWidth / inSampleSize) > mWidth) {
                    inSampleSize *=2;
                }
            }
            return inSampleSize;
        }
    }

    private static MediaItem getMediaItemFromImageCursor(Cursor imageCursor, Map<String,String> thumbnailData) {
        MediaItem newContent = null;

        int imageIdColumnIndex = imageCursor.getColumnIndex(MediaStore.Images.Media._ID);
        int imageDataColumnIndex = imageCursor.getColumnIndex(MediaStore.Images.Media.DATA);
        int imageOrientationColumnIndex = imageCursor.getColumnIndex(MediaStore.Images.Media.ORIENTATION);

        if (imageIdColumnIndex != -1) {
            newContent = new MediaItem();
            newContent.setTag(imageCursor.getString(imageIdColumnIndex));
            newContent.setTitle("");

            if (imageDataColumnIndex != -1) {
                String imageSource = imageCursor.getString(imageDataColumnIndex);
                if (imageSource == null) {
                    return null;
                }
                newContent.setSource(Uri.parse(imageSource));
            }
            if (thumbnailData.containsKey(newContent.getTag())) {
                String thumbnailSource = thumbnailData.get(newContent.getTag());
                if (thumbnailSource == null) {
                    return null;
                }
                newContent.setPreviewSource(Uri.parse(thumbnailSource));
            }

            if (imageOrientationColumnIndex != -1) {
                newContent.setRotation(imageCursor.getInt(imageOrientationColumnIndex));
            }
        }
        return newContent;
    }

    public interface ImageCache {
        Bitmap getBitmap(String url);
        void putBitmap(String url, Bitmap bitmap);
    }
}
