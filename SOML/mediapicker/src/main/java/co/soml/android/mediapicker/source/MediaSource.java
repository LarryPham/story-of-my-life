/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/4/15 11:22 AM.
 **/

package co.soml.android.mediapicker.source;

import android.content.Context;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import co.soml.android.mediapicker.MediaItem;
import co.soml.android.mediapicker.MediaUtils;

/**
 * MediaSource's are used to gather {@link co.soml.android.mediapicker.MediaItem}'s and create {@link android.view.View} for displaying
 * them.
 */
public interface MediaSource extends Parcelable {
    interface OnMediaChange {
        void onMediaLoaded(boolean success);

        void onMediaAdded(MediaSource source, List<MediaItem> addedItems);

        void onMediaRemoved(MediaSource source, List<MediaItem> removedItems);

        void onMediaChanged(MediaSource source, List<MediaItem> changedItems);
    }
    // Load MediaItem data
    void gather(Context context);
    // Destroy MediaItem data
    void cleanUp();
    // Can be ignored if no listener is needed
    void setListener(final OnMediaChange listener);
    // Get the number of MediaItems accessible through the source
    int getCount();
    // Get the MediaItem at the specified position
    MediaItem getMedia(int position);
    View getView(int position, View convertView, ViewGroup parent, LayoutInflater inflater, MediaUtils.ImageCache cache);
    // Callback when MediaItem is selected; return true if handled internally, false to propagate
    boolean onMediaItemSelected(final MediaItem mediaItem, boolean selected);
}
