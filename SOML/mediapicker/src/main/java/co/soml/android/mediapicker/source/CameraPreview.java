/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/5/15 9:35 AM.
 **/

package co.soml.android.mediapicker.source;

import android.content.Context;
import android.hardware.Camera;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;
import java.util.List;

public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback, Camera.PictureCallback {
    public static final String TAG = CameraPreview.class.getSimpleName();

    private static final int INVALID_CAMERA_ID = -1;
    private int mCameraId;
    private Camera mCamera;
    private boolean mResolution;
    private boolean mCanTakePicture;
    private boolean mTakingPicture;

    private Camera.ShutterCallback mShutterCallback;
    private Camera.PictureCallback mRawCallback;
    private Camera.PictureCallback mJpegCallback;

    public CameraPreview(Context context) {
        super(context);
        mCameraId = INVALID_CAMERA_ID;
        getHolder().addCallback(this);
    }

    public CameraPreview(Context context, AttributeSet attrs) {
        super(context, attrs);
        mCameraId = INVALID_CAMERA_ID;
        getHolder().addCallback(this);
    }

    public CameraPreview(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mCameraId = INVALID_CAMERA_ID;
        getHolder().addCallback(this);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (mCanTakePicture && mCamera != null && !mTakingPicture) {
            mTakingPicture = true;
            mCamera.takePicture(mShutterCallback, mRawCallback, this);

            return true;
        }
        return false;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        // surfaceChanged is called immediately after, no need to duplicate code
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if (holder.getSurface() != null) {
            stopCameraPreview();
            prepareCamera();
            startCameraPreview(holder);
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        stopCameraPreview();
        releaseCamera();
    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        if (mJpegCallback != null) {
            mJpegCallback.onPictureTaken(data, camera);

            mTakingPicture = false;
        }
    }

    public void setCanTakePicture(boolean canTake) {
        mCanTakePicture = canTake;
    }

    public void setCameraCallbacks(Camera.ShutterCallback shutter, Camera.PictureCallback raw, Camera.PictureCallback jpeg) {
        mShutterCallback = shutter;
        mRawCallback = raw;
        mJpegCallback = jpeg;
    }

    public void setHighResolution(boolean highResolution) {
        mResolution = highResolution;
    }

    public Camera getCamera() {
        return mCamera;
    }

    public int getCameraId() {
        return mCameraId;
    }

    public void setCamera(int id) {
        if (mCamera == null && mCameraId == INVALID_CAMERA_ID) {
            try {
                mCameraId = id;
                mCamera = Camera.open(id);
            } catch (RuntimeException openException) {
                mCameraId = INVALID_CAMERA_ID;
                Log.w(TAG, "Camera failed to open: " + mCameraId);
            }
        }
    }

    public void prepareCamera() {
        if (mCamera == null) {
            return;
        }

        Camera.Parameters parameters = mCamera.getParameters();
        List<Camera.Size> localSizes = parameters.getSupportedPreviewSizes();
        Camera.Size size = localSizes.get(mResolution ? 0 : localSizes.size() - 1);
        parameters.setPreviewSize(size.width, size.height);
        mCamera.setParameters(parameters);
        mCamera.setDisplayOrientation(90);
    }

    public void releaseCamera() {
        if (mCamera != null) {
            stopCameraPreview();
            mCamera.release();
            mCamera = null;
            mCameraId = INVALID_CAMERA_ID;
        }
    }

    public void startCameraPreview(SurfaceHolder holder) {
        if (mCamera != null) {
            try {
                mCamera.setPreviewDisplay(holder);
                mCamera.startPreview();
            } catch (IOException ex) {
                Log.w(TAG, "Failed to start camera preview");
            }
        }
    }

    public void stopCameraPreview() {
        if (mCamera != null) {
            try {
                mCamera.stopPreview();
            } catch (Exception ex) {
                Log.w(TAG, "Failure stopping preview", ex);
            }
        }
    }
}
