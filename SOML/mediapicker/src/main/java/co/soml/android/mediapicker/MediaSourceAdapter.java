/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/5/15 10:04 AM.
 **/

package co.soml.android.mediapicker;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

import co.soml.android.mediapicker.source.MediaSource;

public class MediaSourceAdapter extends BaseAdapter {

    public static final String TAG = MediaSourceAdapter.class.getSimpleName();
    private final Context mContext;
    private final MediaUtils.ImageCache mImageCache;
    private final List<MediaSource> mMediaSourceList;

    public MediaSourceAdapter(Context context, List<MediaSource> sources, MediaUtils.ImageCache imageCache) {
        mMediaSourceList = sources;
        mContext = context;
        mImageCache = imageCache;
    }

    public void gatherFromSource(final MediaSource.OnMediaChange listener) {
        for (MediaSource source : mMediaSourceList) {
            if (source != null) {
                source.setListener(listener);
                source.gather(mContext);
            }
        }
    }

    @Override
    public int getViewTypeCount() {
        return mMediaSourceList.size() <= 0 ? 1 : mMediaSourceList.size();
    }

    @Override
    public int getCount() {
        return totalItems();
    }

    @Override
    public int getItemViewType(int position) {
        return mMediaSourceList.indexOf(sourceAtPosition(position));
    }

    @Override
    public MediaItem getItem(int position) {
        MediaSource sourceAtPosition = sourceAtPosition(position);

        return sourceAtPosition != null ? sourceAtPosition.getMedia(position - offsetAtPosition(position)) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MediaSource itemSource = sourceAtPosition(position);

        if (itemSource != null) {
            return itemSource.getView(position - offsetAtPosition(position), convertView, parent, LayoutInflater.from(mContext),
                    mImageCache);
        }
        return null;
    }

    /**
     * Determines the total number of items in all MediaSources
     *
     * @return the total number of MediaItems contained in the MediaSource
     */
    private int totalItems() {
        int count = 0;
        for (int index = 0; index < mMediaSourceList.size(); index++) {
            MediaSource source = mMediaSourceList.get(index);
            count += source != null ? source.getCount() : 0;
        }

        return count;
    }

    /**
     * Determines the MediaSource that contains the MediaItem at the specified position.
     *
     * @param position the absolute position in the list of all MediaItems.
     *
     * @return the parent MediaSource for the MediaItem at the specified position.
     */
    public MediaSource sourceAtPosition(int position) {
        int count = 0;
        for (int i = 0; i < mMediaSourceList.size(); i++) {
            MediaSource source = mMediaSourceList.get(i);
            count += source != null ? source.getCount() : 0;

            if (position < count) {
                return mMediaSourceList.get(i);
            }
        }
        return null;
    }

    /**
     * Determines offset into individual MediaSource
     *
     * @param position
     *  position of item within all MediaSources
     *
     * @return of item within its parent MediaSource.
     */
    private int offsetAtPosition(int position) {
        int offset = 0;
        for (int i = 0; i < mMediaSourceList.size(); i++) {
            if (position < (offset + mMediaSourceList.get(i).getCount())) {
                return offset;
            }

            MediaSource source = mMediaSourceList.get(i);
            offset += source != null ? source.getCount() : 0;
        }
        return offset;
    }
}
