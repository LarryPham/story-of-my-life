/**
 *    Copyright (C) 2014 Sugar Ventures Inc. All rights reserved. Mobile UX Promotion Division. This software and its documentation are confidential
 *    and proprietary information of Sugar Ventures Inc.  No part of the software and documents may be copied, reproduced, transmitted, translated,
 *    or reduced to any electronic medium or machine-readable form without the prior written consent of Sugar Ventures Inc.
 *    Sugar Ventures Inc makes no representations with respect to the contents and assumes no responsibility for any errors
 *    that might appear in the software and documents. This publication and the contents hereof are subject to change without notice.
 *
 *    @author: Larry Pham (email: larrypham.vn@gmail.com)
 *    Date: 8/4/15 11:06 AM.
 **/

package co.soml.android.mediapicker;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Simple data model to describe media information. Contains the following data:
 * - Tag: Used to store an arbitrary String for implementation specific use and is therefore not used by MediaPickerFragment to determine
 * behavior.
 * - Content Title: Title of the media content.
 * - Preview Source: URI to media content preview.
 * - Content Source: Direct URI to the media content.
 * - Rotation: Specifies the orientation that the media was captured in.
 *
 * Implements {@link android.os.Parcelable} to allow passing via {@link android.content.Intent's.
 */
public class MediaItem implements Parcelable {

    public static final String PARCEL_KEY_TAG = "tag";
    public static final String PARCEL_KEY_TITLE = "title";
    public static final String PARCEL_KEY_PREVIEW = "preview";
    public static final String PARCEL_KEY_SOURCE = "source";
    public static final String PARCEL_KEY_ROTATION = "rotation";

    private String mTag;
    private String mContentTitle;
    private Uri mContentPreviewSource;
    private Uri mContentSource;
    private int mRotation;

    public String getTag() {
        return mTag;
    }

    public void setTag(String tag) {
        mTag = tag;
    }

    public String getTitle() {
        return mContentTitle;
    }

    public void setTitle(String contentTitle) {
        mContentTitle = contentTitle;
    }

    public Uri getPreviewSource() {
        return mContentPreviewSource;
    }

    public void setPreviewSource(Uri contentPreviewSource) {
        mContentPreviewSource = contentPreviewSource;
    }

    public Uri getSource() {
        return mContentSource;
    }

    public void setSource(Uri contentSource) {
        mContentSource = contentSource;
    }

    public int getRotation() {
        return mRotation;
    }

    public void setRotation(int rotation) {
        mRotation = rotation;
    }

    public static final Parcelable.Creator<MediaItem> CREATOR =  new Parcelable.Creator<MediaItem>() {
        @Override
        public MediaItem createFromParcel(Parcel source) {
            List<String> parcelData = new ArrayList<String>();
            source.readStringList(parcelData);

            if (parcelData.size() > 0) {
                MediaItem newItem = new MediaItem();

                while (parcelData.size() > 0) {
                    String data = parcelData.remove(0);
                    if (data == null) continue;

                    String key = data.substring(0, data.indexOf('='));
                    String value = data.substring(data.indexOf('=') + 1, data.length());

                    if (!key.isEmpty()) {
                        switch (key) {
                            case PARCEL_KEY_TAG:
                                newItem.setTag(value);
                                break;
                            case PARCEL_KEY_TITLE:
                                newItem.setTitle(value);
                                break;
                            case PARCEL_KEY_SOURCE:
                                newItem.setSource(Uri.parse(value));
                                break;
                            case PARCEL_KEY_PREVIEW:
                                newItem.setPreviewSource(Uri.parse(value));
                                break;
                            case PARCEL_KEY_ROTATION:
                                newItem.setRotation(Integer.parseInt(value));
                                break;
                        }
                    }
                    return newItem;
                }
            }
            return null;
        }

        @Override
        public MediaItem[] newArray(int size) {
            return new MediaItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        List<String> dataList = new ArrayList<>();

        dataList.add(PARCEL_KEY_ROTATION + "=" + mRotation);
        if (mTag != null && !mTag.isEmpty()) {
            dataList.add(PARCEL_KEY_TAG + "=" + mTag);
        }

        if (mContentTitle != null && !mContentTitle.isEmpty()) {
            dataList.add(PARCEL_KEY_TITLE + "=" + mContentTitle);
        }

        if (mContentPreviewSource != null && !mContentPreviewSource.toString().isEmpty()) {
            dataList.add(PARCEL_KEY_PREVIEW + "=" + mContentPreviewSource.toString());
        }

        if (mContentSource != null && !mContentSource.toString().isEmpty()) {
            dataList.add(PARCEL_KEY_SOURCE + "=" + mContentSource.toString());
        }

        dest.writeStringList(dataList);
    }
}
