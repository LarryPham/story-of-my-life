/***
  Copyright (c) 2013-2014 CommonsWare, LLC
  Portions Copyright (C) 2007 The Android Open Source Project
  
  Licensed under the Apache License, Version 2.0 (the "License"); you may
  not use this file except in compliance with the License. You may obtain
  a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package com.jf.soml.camera;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.CameraInfo;
import android.media.MediaRecorder;
import android.os.Build;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.OrientationEventListener;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;

import java.io.IOException;

import com.jf.soml.camera.CameraHost.FailureReason;
import com.jf.soml.camera.ImageCleanupTask.ImageCleanUpCallback;
import com.jf.soml.util.Logger;

public class CameraView extends ViewGroup implements AutoFocusCallback {

	static final String TAG = "CWAC-Camera";

	/*
	 * Preivew stratety
	 */
	private PreviewStrategy previewStrategy;

	/*
	 * Preivew size
	 */
	private Camera.Size previewSize;

	/*
	 * Camera
	 */
	private Camera mCamera = null;

	/*
	 * Camera ID
	 */
	private int mCameraId = -1;

	/*
	 * Camera host
	 */
	private CameraHost mCameraHost = null;

	/*
	 * Orientation change listener
	 */
	private OnOrientationChange onOrientationChange = null;

	/*
	 * Media recorder
	 */
	private MediaRecorder mMediaRecorder = null;

	/*
	 * Preview parameter
	 */
	private Camera.Parameters mPreviewParams = null;

	private boolean inPreview = false, isDetectingFaces = false,
			isAutoFocusing = false;

	private int displayOrientation = -1;
	private int outputOrientation = -1;
	private int lastPictureOrientation = -1;

	public CameraView(Context context) {
		super(context);

		onOrientationChange = new OnOrientationChange(
				context.getApplicationContext());
	}

	public CameraView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public CameraView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		onOrientationChange = new OnOrientationChange(
				context.getApplicationContext());
		if (context instanceof CameraHostProvider) {
			setHost(((CameraHostProvider) context).getCameraHost());
		} else {
			throw new IllegalArgumentException("To use the two- or "
					+ "three-parameter constructors on CameraView, "
					+ "your activity needs to implement the "
					+ "CameraHostProvider interface");
		}
	}

	public CameraHost getHost() {
		return (mCameraHost);
	}

	/**
	 * Set camera host
	 * 
	 * @param host
	 */
	public void setHost(CameraHost host) {
		this.mCameraHost = host;

		if (host.getDeviceProfile().useTextureView()) {
			previewStrategy = new TexturePreviewStrategy(this);
		} else {
			previewStrategy = new SurfacePreviewStrategy(this);
		}
	}

	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	public void onResume() {
		addView(previewStrategy.getWidget());

		if (mCamera == null) {
			mCameraId = getHost().getCameraId();

			if (mCameraId >= 0) {
				try {
					mCamera = Camera.open(mCameraId);

					if (getActivity().getRequestedOrientation() != ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED) {
						onOrientationChange.enable();
					}

					setCameraDisplayOrientation();

					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH
							&& getHost() instanceof Camera.FaceDetectionListener) {
						mCamera.setFaceDetectionListener((Camera.FaceDetectionListener) getHost());
					}
				} catch (Exception e) {
					getHost().onCameraFail(FailureReason.UNKNOWN);
				}
			} else {
				getHost().onCameraFail(FailureReason.NO_CAMERAS_REPORTED);
			}
		}
	}

	public void onPause() {
		if (mCamera != null) {
			previewDestroyed();
		}

		removeView(previewStrategy.getWidget());
		onOrientationChange.disable();
		lastPictureOrientation = -1;
	}

	// based on CameraPreview.java from ApiDemos

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		final int width = resolveSize(getSuggestedMinimumWidth(),
				widthMeasureSpec);
		final int height = resolveSize(getSuggestedMinimumHeight(),
				heightMeasureSpec);
		setMeasuredDimension(width, height);

		if (width > 0 && height > 0) {
			if (mCamera != null) {
				Camera.Size newSize = null;

				try {
					if (getHost().getRecordingHint() != CameraHost.RecordingHint.STILL_ONLY) {
						// Camera.Size deviceHint=
						// host.getDeviceProfile()
						// .getPreferredPreviewSizeForVideo(getDisplayOrientation(),
						// width,
						// height,
						// camera.getParameters());

						newSize = getHost().getPreferredPreviewSizeForVideo(
								getDisplayOrientation(), width, height,
								mCamera.getParameters(), null);

						// if (newSize != null) {
						// android.util.Log.wtf("CameraView",
						// String.format("getPreferredPreviewSizeForVideo: %d x %d",
						// newSize.width,
						// newSize.height));
						// }
					}

					if (newSize == null
							|| newSize.width * newSize.height < 65536) {
						newSize = getHost().getPreviewSize(
								getDisplayOrientation(), width, height,
								mCamera.getParameters());
					}
				} catch (Exception e) {
					android.util.Log.e(getClass().getSimpleName(),
							"Could not work with camera parameters?", e);
					// TODO get this out to library clients
				}

				if (newSize != null) {
					if (previewSize == null) {
						previewSize = newSize;
					} else if (previewSize.width != newSize.width
							|| previewSize.height != newSize.height) {
						if (inPreview) {
							stopPreview();
						}

						previewSize = newSize;
						initPreview(width, height, false);
					}
				}
			}
		}
	}

	// based on CameraPreview.java from ApiDemos

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		if (changed && getChildCount() > 0) {
			final View child = getChildAt(0);
			final int width = r - l;
			final int height = b - t;
			int previewWidth = width;
			int previewHeight = height;

			// handle orientation

			if (previewSize != null) {
				if (getDisplayOrientation() == 90
						|| getDisplayOrientation() == 270) {
					previewWidth = previewSize.height;
					previewHeight = previewSize.width;
				} else {
					previewWidth = previewSize.width;
					previewHeight = previewSize.height;
				}
			}

			boolean useFirstStrategy = (width * previewHeight > height
					* previewWidth);
			boolean useFullBleed = getHost().useFullBleedPreview();

			if ((useFirstStrategy && !useFullBleed)
					|| (!useFirstStrategy && useFullBleed)) {
				final int scaledChildWidth = previewWidth * height
						/ previewHeight;
				child.layout((width - scaledChildWidth) / 2, 0,
						(width + scaledChildWidth) / 2, height);
			} else {
				final int scaledChildHeight = previewHeight * width
						/ previewWidth;
				child.layout(0, (height - scaledChildHeight) / 2, width,
						(height + scaledChildHeight) / 2);
			}
		}
	}

	public int getDisplayOrientation() {
		return (displayOrientation);
	}

	public void lockToLandscape(boolean enable) {
		if (enable) {
			getActivity().setRequestedOrientation(
					ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
			onOrientationChange.enable();
		} else {
			getActivity().setRequestedOrientation(
					ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
			onOrientationChange.disable();
		}
	}

	public void restartPreview() {
		if (!inPreview) {
			startPreview();
		}
	}

	/**
	 * Take picture
	 * 
	 * @param needBitmap
	 * @param needByteArray
	 */
	public void takePicture(boolean needBitmap, boolean needByteArray,
			ImageCleanUpCallback callback) {
		PictureTransaction xact = new PictureTransaction(getHost());

		takePicture(xact.needBitmap(needBitmap).needByteArray(needByteArray),
				callback);
	}

	/**
	 * Take picture
	 * 
	 * @param xact
	 */
	public void takePicture(final PictureTransaction xact,
			final ImageCleanUpCallback callback) {
		if (inPreview) {
			if (isAutoFocusing) {
				throw new IllegalStateException(
						"Camera cannot take a picture while auto-focusing");
			} else {
				mPreviewParams = mCamera.getParameters();

				Camera.Parameters pictureParams = mCamera.getParameters();
				Camera.Size pictureSize = xact.host.getPictureSize(xact,
						pictureParams);

				pictureParams.setPictureSize(pictureSize.width,
						pictureSize.height);

				pictureParams.setPictureFormat(ImageFormat.JPEG);

				if (xact.flashMode != null) {
					pictureParams.setFlashMode(xact.flashMode);
				}

				if (!onOrientationChange.isEnabled()) {
					setCameraPictureOrientation(pictureParams);
				}

				mCamera.setParameters(xact.host.adjustPictureParameters(xact,
						pictureParams));
				xact.cameraView = this;

				postDelayed(new Runnable() {
					@Override
					public void run() {
						try {
							mCamera.takePicture(xact, null,
									new PictureTransactionCallback(xact,
											callback));
						} catch (Exception e) {
							Log.e(getClass().getSimpleName(),
									"Exception taking a picture", e);
							// TODO get this out to library clients
						}
					}
				}, xact.host.getDeviceProfile().getPictureDelay());

				inPreview = false;
			}
		} else {
			throw new IllegalStateException(
					"Preview mode must have started before you can take a picture");
		}
	}

	public boolean isRecording() {
		return (mMediaRecorder != null);
	}

	public void record() throws Exception {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			throw new UnsupportedOperationException(
					"Video recording supported only on API Level 11+");
		}

		if (displayOrientation != 0 && displayOrientation != 180) {
			throw new UnsupportedOperationException(
					"Video recording supported only in landscape");
		}

		Camera.Parameters pictureParams = mCamera.getParameters();

		setCameraPictureOrientation(pictureParams);
		mCamera.setParameters(pictureParams);

		stopPreview();
		mCamera.unlock();

		try {
			mMediaRecorder = new MediaRecorder();
			mMediaRecorder.setCamera(mCamera);
			getHost().configureRecorderAudio(mCameraId, mMediaRecorder);
			mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
			getHost().configureRecorderProfile(mCameraId, mMediaRecorder);
			getHost().configureRecorderOutput(mCameraId, mMediaRecorder);
			mMediaRecorder.setOrientationHint(outputOrientation);
			previewStrategy.attach(mMediaRecorder);
			mMediaRecorder.prepare();
			mMediaRecorder.start();
		} catch (IOException e) {
			mMediaRecorder.release();
			mMediaRecorder = null;
			throw e;
		}
	}

	public void stopRecording() throws IOException {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			throw new UnsupportedOperationException(
					"Video recording supported only on API Level 11+");
		}

		MediaRecorder tempRecorder = mMediaRecorder;

		mMediaRecorder = null;
		tempRecorder.stop();
		tempRecorder.release();
		mCamera.reconnect();
		startPreview();
	}

	public void autoFocus() {
		if (inPreview) {
			mCamera.autoFocus(this);
			isAutoFocusing = true;
		}
	}

	public void cancelAutoFocus() {
		mCamera.cancelAutoFocus();
	}

	public boolean isAutoFocusAvailable() {
		return (inPreview);
	}

	@Override
	public void onAutoFocus(boolean success, Camera camera) {
		isAutoFocusing = false;

		if (getHost() instanceof AutoFocusCallback) {
			getHost().onAutoFocus(success, camera);
		}
	}

	public String getFlashMode() {
		return (mCamera.getParameters().getFlashMode());
	}

	public void setFlashMode(String mode) {
		if (mCamera != null) {
			Camera.Parameters params = mCamera.getParameters();

			params.setFlashMode(mode);
			mCamera.setParameters(params);
		}
	}

	public ZoomTransaction zoomTo(int level) {
		if (mCamera == null) {
			throw new IllegalStateException(
					"Yes, we have no camera, we have no camera today");
		} else {
			Camera.Parameters params = mCamera.getParameters();

			if (level >= 0 && level <= params.getMaxZoom()) {
				return (new ZoomTransaction(mCamera, level));
			} else {
				throw new IllegalArgumentException(String.format(
						"Invalid zoom level: %d", level));
			}
		}
	}

	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	public void startFaceDetection() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH
				&& mCamera != null && !isDetectingFaces
				&& mCamera.getParameters().getMaxNumDetectedFaces() > 0) {
			mCamera.startFaceDetection();
			isDetectingFaces = true;
		}
	}

	public void stopFaceDetection() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH
				&& mCamera != null && isDetectingFaces) {
			try {
				mCamera.stopFaceDetection();
			} catch (Exception e) {
				// TODO get this out to hosting app
			}

			isDetectingFaces = false;
		}
	}

	public boolean doesZoomReallyWork() {
		Camera.CameraInfo info = new Camera.CameraInfo();
		Camera.getCameraInfo(getHost().getCameraId(), info);

		return (getHost().getDeviceProfile()
				.doesZoomActuallyWork(info.facing == CameraInfo.CAMERA_FACING_FRONT));
	}

	void previewCreated() {
		if (mCamera != null) {
			try {
				previewStrategy.attach(mCamera);
			} catch (IOException e) {
				getHost().handleException(e);
			}
		}
	}

	void previewDestroyed() {
		if (mCamera != null) {
			previewStopped();
			mCamera.release();
			mCamera = null;
		}
	}

	void previewReset(int width, int height) {
		previewStopped();
		initPreview(width, height);
	}

	private void previewStopped() {
		if (inPreview) {
			stopPreview();
		}
	}

	public void initPreview(int w, int h) {
		initPreview(w, h, true);
	}

	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	public void initPreview(int w, int h, boolean firstRun) {
		if (mCamera != null) {
			Camera.Parameters parameters = mCamera.getParameters();

			parameters.setPreviewSize(previewSize.width, previewSize.height);

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
				parameters
						.setRecordingHint(getHost().getRecordingHint() != CameraHost.RecordingHint.STILL_ONLY);
			}

			requestLayout();

			mCamera.setParameters(getHost().adjustPreviewParameters(parameters));
			startPreview();
		}
	}

	private void startPreview() {
		mCamera.startPreview();
		inPreview = true;
		getHost().autoFocusAvailable();
	}

	private void stopPreview() {
		inPreview = false;
		getHost().autoFocusUnavailable();
		mCamera.stopPreview();
	}

	// based on
	// http://developer.android.com/reference/android/hardware/Camera.html#setDisplayOrientation(int)
	// and http://stackoverflow.com/a/10383164/115145

	private void setCameraDisplayOrientation() {
		Camera.CameraInfo info = new Camera.CameraInfo();
		int rotation = getActivity().getWindowManager().getDefaultDisplay()
				.getRotation();
		int degrees = 0;
		DisplayMetrics dm = new DisplayMetrics();

		Camera.getCameraInfo(mCameraId, info);
		getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);

		switch (rotation) {
		case Surface.ROTATION_0:
			degrees = 0;
			break;
		case Surface.ROTATION_90:
			degrees = 90;
			break;
		case Surface.ROTATION_180:
			degrees = 180;
			break;
		case Surface.ROTATION_270:
			degrees = 270;
			break;
		}

		if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
			displayOrientation = (info.orientation + degrees) % 360;
			displayOrientation = (360 - displayOrientation) % 360;
		} else {
			displayOrientation = (info.orientation - degrees + 360) % 360;
		}

		boolean wasInPreview = inPreview;

		if (inPreview) {
			stopPreview();
		}

		mCamera.setDisplayOrientation(displayOrientation);

		if (wasInPreview) {
			startPreview();
		}
	}

	private void setCameraPictureOrientation(Camera.Parameters params) {
		Camera.CameraInfo info = new Camera.CameraInfo();

		Camera.getCameraInfo(mCameraId, info);

		if (getActivity().getRequestedOrientation() != ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED) {
			outputOrientation = getCameraPictureRotation(getActivity()
					.getWindowManager().getDefaultDisplay().getOrientation());
		} else if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
			outputOrientation = (360 - displayOrientation) % 360;
		} else {
			outputOrientation = displayOrientation;
		}

		if (lastPictureOrientation != outputOrientation) {
			params.setRotation(outputOrientation);
			lastPictureOrientation = outputOrientation;
		}
	}

	// based on:
	// http://developer.android.com/reference/android/hardware/Camera.Parameters.html#setRotation(int)

	private int getCameraPictureRotation(int orientation) {
		Camera.CameraInfo info = new Camera.CameraInfo();
		Camera.getCameraInfo(mCameraId, info);
		int rotation = 0;

		orientation = (orientation + 45) / 90 * 90;

		if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
			rotation = (info.orientation - orientation + 360) % 360;
		} else { // back-facing camera
			rotation = (info.orientation + orientation) % 360;
		}

		return (rotation);
	}

	Activity getActivity() {
		return ((Activity) getContext());
	}

	private class OnOrientationChange extends OrientationEventListener {
		private boolean isEnabled = false;

		public OnOrientationChange(Context context) {
			super(context);
			disable();
		}

		@Override
		public void onOrientationChanged(int orientation) {
			if (mCamera != null && orientation != ORIENTATION_UNKNOWN) {
				int newOutputOrientation = getCameraPictureRotation(orientation);

				if (newOutputOrientation != outputOrientation) {
					outputOrientation = newOutputOrientation;

					Camera.Parameters params = mCamera.getParameters();

					params.setRotation(outputOrientation);

					try {
						mCamera.setParameters(params);
						lastPictureOrientation = outputOrientation;
					} catch (Exception e) {
						Log.e(getClass().getSimpleName(),
								"Exception updating camera parameters in orientation change",
								e);
						// TODO: get this info out to hosting app
					}
				}
			}
		}

		@Override
		public void enable() {
			isEnabled = true;
			super.enable();
		}

		@Override
		public void disable() {
			isEnabled = false;
			super.disable();
		}

		boolean isEnabled() {
			return (isEnabled);
		}
	}

	private class PictureTransactionCallback implements Camera.PictureCallback {
		PictureTransaction xact = null;
		ImageCleanUpCallback callback = null;

		PictureTransactionCallback(PictureTransaction xact,
				ImageCleanUpCallback callback) {
			this.xact = xact;
			this.callback = callback;
		}

		@Override
		public void onPictureTaken(byte[] data, Camera camera) {
			camera.setParameters(mPreviewParams);
			if (data != null) {
				Logger.e("Clean up image");
				new ImageCleanupTask(getContext(), data, mCameraId, xact,
						callback).start();
			}

			if (!xact.useSingleShotMode()) {
				startPreview();
			}
		}
	}
}