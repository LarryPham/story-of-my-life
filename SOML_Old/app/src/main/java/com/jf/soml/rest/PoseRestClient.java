package com.jf.soml.rest;

import com.jf.soml.util.Logger;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class PoseRestClient {
    private static final String BASE_URL = "https://staging.soml.co/";

    private static AsyncHttpClient httpClient;
    private static PoseRestClient instance;

    public static PoseRestClient getInstance() {
        if (instance == null) {
            instance = new PoseRestClient();
        }

        return instance;
    }

    private PoseRestClient() {
        httpClient = new AsyncHttpClient();
        httpClient.setEnableRedirects(true);
        httpClient.setMaxConnections(10);
        httpClient.setMaxRetriesAndTimeout(3, 150000);
    }

    public void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Logger.d("get " + getAbsoluteUrl(url));
        httpClient.get(getAbsoluteUrl(url), params, responseHandler);
    }

    public void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Logger.d("post " + getAbsoluteUrl(url));
        httpClient.post(getAbsoluteUrl(url), params, responseHandler);
    }

    public void put(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Logger.d("put " + getAbsoluteUrl(url));
        httpClient.put(getAbsoluteUrl(url), params, responseHandler);
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }
}
