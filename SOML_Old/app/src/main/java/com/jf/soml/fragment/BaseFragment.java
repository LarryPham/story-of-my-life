package com.jf.soml.fragment;

import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.jf.soml.rest.RestClientTask;

import org.json.JSONObject;

/**
 * Created by hai on 12/23/14.
 */
public abstract class BaseFragment extends Fragment implements RestClientTask.OnPreExecuteDelegate, RestClientTask.OnPostExecuteDelegate{

    protected abstract void initView();


    /**
     * Display message
     *
     * @param message
     */
    protected void makeToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void actionPre(RestClientTask task) {

    }

    @Override
    public void actionPost(RestClientTask task, JSONObject result) {

    }
}
