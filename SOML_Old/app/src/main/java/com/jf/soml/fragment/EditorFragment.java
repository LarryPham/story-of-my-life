package com.jf.soml.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.jf.soml.R;
import com.jf.soml.adapter.StoryAdapter;
import com.jf.soml.model.Story;
import com.jf.soml.util.Logger;
import com.jf.soml.widget.CircleProgressView;
import com.parse.FindCallback;
import com.parse.ParseException;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by hai on 12/23/14.
 */
public class EditorFragment extends BaseFragment {

    private static final Handler HANDLE = new Handler();

    @InjectView(R.id.list_view)
    ListView mListView;

    @InjectView(R.id.progress_view)
    CircleProgressView mProgressView;

    StoryAdapter mStoryAdapter;

    public static Fragment newInstance() {return new EditorFragment();}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_story, container, false);
        ButterKnife.inject(this, rootView);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    @Override
    protected void initView() {
        mProgressView.setVisibility(View.VISIBLE);

        mStoryAdapter = new StoryAdapter(getActivity());

        mListView.setFooterDividersEnabled(false);
        mListView.setAdapter(mStoryAdapter);

        // Feed the story
        Story.findInBackground(new FindCallback<Story>() {

            @Override
            public void done(final List<Story> stories, ParseException e) {
                // TODO Auto-generated method stub
                mProgressView.setVisibility(View.GONE);
                if (stories == null) {
                    Logger.e("ParseException: " + e.getLocalizedMessage());
                    return;
                }

                HANDLE.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        mStoryAdapter.addAll(stories);
                    }
                }, 500);
            }
        });
    }
}
