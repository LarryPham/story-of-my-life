package com.jf.soml.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;

import com.jf.soml.R;
import com.jf.soml.activity.base.BaseActivity;
import com.jf.soml.common.DataManager;
import com.jf.soml.model.UserModel;
import com.jf.soml.parse.DataParse;
import com.jf.soml.util.Logger;
import com.jf.soml.widget.CircleProgressView;
import com.jf.soml.widget.FancyButton;
import com.jf.soml.widget.MaterialEditText;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.picasso.transformation.StackBlurTransformation;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnEditorAction;

public class SignInActivity extends BaseActivity {
    final String RANDOM_STORY = "https://dl.dropboxusercontent.com/u/3701068/background.jpg";

    @InjectView(R.id.background_view)
    ImageView backgroundView;

    @InjectView(R.id.progress_view)
    CircleProgressView progressView;


    @InjectView(R.id.email_view)
    MaterialEditText emailView;

    @InjectView(R.id.password_view)
    MaterialEditText passwordView;


    @InjectView(R.id.content_view)
    View contentView;

    @InjectView(R.id.btn_sign_in)
    FancyButton btnSignIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        ButterKnife.inject(this);

        initView();

        Picasso.with(this).load(RANDOM_STORY).placeholder(R.drawable.background)
                .error(R.drawable.background)
                .transform(new StackBlurTransformation(5))
                .into(backgroundView, new Callback() {
                    @Override
                    public void onSuccess() {
                        progressView.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        Logger.e("Error to load image");
                    }
                });
    }


    @Override
    protected void initView() {
        // TODO Auto-generated method stub
        progressView.setVisibility(View.GONE);

        emailView.setText("khaintt@gmail.com");
        passwordView.setText("123456");
    }

    public void getSignInUser(String email, String password) throws JSONException {

        DataManager.getInstance(this).getSignInUserTask(email, password, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                progressView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                if (response == null) {
                    return;
                }

                DataParse parse = new DataParse(response);
                if (response.has("code") && parse.getInt("code") == 0) {
                    makeToast("Sign in failed: " + parse.getString("error_message"));
                    Logger.e(response.toString());
                } else {
                    UserModel model =  parse.parseUser();
                    DataManager.getInstance(SignInActivity.this).setUserModel(model);
                    DataManager.getInstance(SignInActivity.this).setAuthToken(model.getAuthToken());

                    startActivity(new Intent(SignInActivity.this, HomeActivity.class));
                    finish();
                    // getAccountDetailTask();
                }
            }

            @Override
            public void onFinish() {
                super.onFinish();
                progressView.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                makeToast("HTTP " + statusCode + " " + responseString);
            }
        });
    }

    private void getAccountDetailTask() {
        DataManager.getInstance(this).getAccountDetail(new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                progressView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                if (response == null) {
                    return;
                }

                DataParse parse = new DataParse(response);
                DataManager.getInstance(SignInActivity.this).setUserModel(parse.parseAccountInfo());

                startActivity(new Intent(SignInActivity.this, HomeActivity.class));
                finish();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                progressView.setVisibility(View.GONE);
            }

        });
    }

    @Override
    protected void tintStatusBar() {
        // TODO Auto-generated method stub
        super.tintStatusBar();
    }

    @OnClick(R.id.btn_sign_in)
    public void onSignInClick(View v) {
        String email = emailView.getText().toString();
        String password = passwordView.getText().toString();

        try {
            getSignInUser(email, password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @OnEditorAction(R.id.password_view)
    public boolean onConfirmPasswordDone(KeyEvent key) {
        String email = emailView.getText().toString();
        String password = passwordView.getText().toString();

        try {
            getSignInUser(email, password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return true;
    }

    @OnClick(R.id.btn_sign_up)
    public void onSignUpClick(View v) {
        startActivityForResult(new Intent(this, SignUpActivity.class), 100);
    }
}
