package com.jf.soml.rest;

import android.content.Context;

import com.jf.soml.util.Logger;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

public class RestClient {
    private final static int CONNECT_TIMEOUT = 15 * 1000;

    private static RestClient instance = null;
    private static HttpClient httpClient;

    private ArrayList<NameValuePair> params;
    private ArrayList<NameValuePair> headers;

    private int responseCode;
    private String message;
    private String response;
    private String jsonBody;
    private String baseUrl;

    public static RestClient getInstance(Context ctx) {
        // TODO Auto-generated constructor stub
        if (instance == null) {
            instance = new RestClient(ctx);
        }

        return instance;
    }

    public RestClient(Context ctx) {
        // TODO Auto-generated constructor stub
        this.baseUrl = "";
        this.params = new ArrayList<NameValuePair>();
        this.headers = new ArrayList<NameValuePair>();

        RestClientManager singletonHttpClient = new RestClientManager();
        httpClient = new DefaultHttpClient(
                singletonHttpClient.getClientConnectionManager(),
                singletonHttpClient.getHttpParams());
    }

    public RestClient(Context context, String baseUrl) {
        // TODO Auto-generated constructor stub
        this.baseUrl = baseUrl;
        this.params = new ArrayList<NameValuePair>();
        this.headers = new ArrayList<NameValuePair>();

        RestClientManager singletonHttpClient = new RestClientManager();
        httpClient = new DefaultHttpClient(
                singletonHttpClient.getClientConnectionManager(),
                singletonHttpClient.getHttpParams());

    }

    public void addParam(String name, String value) {
        params.add(new BasicNameValuePair(name, value));
    }

    public void addParam(String name, double value) {
        params.add(new BasicNameValuePair(name, value + ""));
    }

    public void addParam(String name, int value) {
        params.add(new BasicNameValuePair(name, value + ""));
    }

    public void addParam(String name, float value) {
        params.add(new BasicNameValuePair(name, value + ""));
    }

    public void get(String url) {
        url = buildApiUrl(url);
        Logger.d(RestClient.class.getName(), "GET > " + url);
        HttpGet httpGet = new HttpGet(url + getParams());
        try {
            httpGet = (HttpGet) addHeaderParams(httpGet);
            executeRequest(httpGet, url);
            params.removeAll(params);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void post(String path) {
        path = buildApiUrl(path);
        Logger.d(RestClient.class.getName(), "POST > " + path);
        HttpPost request = new HttpPost(path);
        try {
            request = (HttpPost) addHeaderParams(request);
            request = (HttpPost) addBodyParams(request);
            executeRequest(request, path);
            params.removeAll(params);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void put(String url) {
        url = buildApiUrl(url);
        Logger.d(RestClient.class.getName(), "PUT > " + url);
        HttpPut request = new HttpPut(url);
        try {
            request = (HttpPut) addHeaderParams(request);
            request = (HttpPut) addBodyParams(request);
            executeRequest(request, url);
            params.removeAll(params);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void delete(String url) {
        url = buildApiUrl(url);
        HttpDelete request = new HttpDelete(url);
        try {
            request = (HttpDelete) addHeaderParams(request);
            params.removeAll(params);
        } catch (Exception e) {
            e.printStackTrace();
        }
        executeRequest(request, url);
    }

    public String getErrorMessage() {
        return message;
    }

    public String getResponse() {
        return response;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setJSONString(String data) {
        jsonBody = data;

        Logger.d(data);
    }

    private void executeRequest(HttpUriRequest request, String url) {
        HttpParams params = httpClient.getParams();
        HttpConnectionParams.setConnectionTimeout(params, CONNECT_TIMEOUT);
        HttpConnectionParams.setSoTimeout(params, CONNECT_TIMEOUT);

        HttpResponse httpResponse;
        InputStream inputStream = null;

        try {
            httpResponse = httpClient.execute(request);

            responseCode = httpResponse.getStatusLine().getStatusCode();
            message = httpResponse.getStatusLine().getReasonPhrase();

            HttpEntity entity = httpResponse.getEntity();
            if (entity != null) {
                inputStream = entity.getContent();
                response = convertStreamToString(inputStream);
                inputStream.close();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (HttpHostConnectException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null)
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }

    private HttpUriRequest addHeaderParams(HttpUriRequest request)
            throws Exception {
        for (NameValuePair h : headers) {
            request.addHeader(h.getName(), h.getValue());
        }
        return request;
    }

    private HttpUriRequest addBodyParams(HttpUriRequest request)
            throws Exception {
        if (jsonBody != null) {
            request.addHeader("Content-Type", "application/json");
            if (request instanceof HttpPost)
                ((HttpPost) request).setEntity(new StringEntity(jsonBody,
                        "UTF-8"));
            else if (request instanceof HttpPut)
                ((HttpPut) request).setEntity(new StringEntity(jsonBody,
                        "UTF-8"));

        } else if (!params.isEmpty()) {
            if (request instanceof HttpPost)
                ((HttpPost) request).setEntity(new UrlEncodedFormEntity(params,
                        HTTP.UTF_8));
            else if (request instanceof HttpPut)
                ((HttpPut) request).setEntity(new UrlEncodedFormEntity(params,
                        HTTP.UTF_8));
        }
        return request;
    }

    private String getParams() {
        StringBuffer combinedParams = new StringBuffer();
        try {
            if (!params.isEmpty()) {
                combinedParams.append("?");
                for (NameValuePair p : params) {

                    combinedParams.append((combinedParams.length() > 1 ? "&"
                            : "")
                            + p.getName()
                            + "="
                            + URLEncoder.encode(p.getValue(), "UTF-8"));
                }
            }
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return combinedParams.toString();
    }

    private String buildApiUrl(String path) {
        // TODO Auto-generated method stub
        return baseUrl + path;
    }

    private static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return sb.toString();
    }

    public ArrayList<NameValuePair> getParam() {
        // TODO Auto-generated method stub
        return params;
    }
}
