package com.jf.soml.fragment;

import android.support.v4.app.Fragment;
import android.widget.AbsListView;

import com.jf.soml.fragment.BaseFragment;
import com.jf.soml.widget.ScrollTabHolder;

public abstract class ScrollTabHolderFragment extends BaseFragment implements ScrollTabHolder {

	protected ScrollTabHolder mScrollTabHolder;

	public void setScrollTabHolder(ScrollTabHolder scrollTabHolder) {
		mScrollTabHolder = scrollTabHolder;
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount, int pagePosition) {
		// nothing
	}

}