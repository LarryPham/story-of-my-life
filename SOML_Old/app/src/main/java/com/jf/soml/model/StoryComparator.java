package com.jf.soml.model;

import java.util.Comparator;

/**
 * A Comparator that sorts talks by created time
 */
public class StoryComparator implements Comparator<Story> {
	final private static StoryComparator instance = new StoryComparator();

	public static StoryComparator get() {
		return instance;
	}

	@Override
	public int compare(Story lhs, Story rhs) {
		int compare = rhs.getCreatedAt().compareTo(lhs.getCreatedAt());
		return compare;
	}
}
