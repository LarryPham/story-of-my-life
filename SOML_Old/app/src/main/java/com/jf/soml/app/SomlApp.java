package com.jf.soml.app;

import android.app.Application;

import com.jf.soml.R;
import com.jf.soml.model.Comment;
import com.jf.soml.model.Photo;
import com.jf.soml.model.Story;
import com.jf.soml.model.UserModel;
import com.parse.Parse;
import com.parse.ParseObject;

public class SomlApp extends Application {

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		ParseObject.registerSubclass(Story.class);
		ParseObject.registerSubclass(UserModel.class);
		ParseObject.registerSubclass(Comment.class);
		ParseObject.registerSubclass(Photo.class);

		Parse.initialize(this, getString(R.string.parse_app_id),
				getString(R.string.parse_app_key));

		Parse.setLogLevel(Parse.LOG_LEVEL_DEBUG);
	}

}
