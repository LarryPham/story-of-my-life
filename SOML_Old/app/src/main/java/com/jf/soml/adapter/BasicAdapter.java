package com.jf.soml.adapter;

import android.content.Context;
import android.widget.BaseAdapter;

public abstract class BasicAdapter extends BaseAdapter {
	private Context context;

	public BasicAdapter(Context ctx) {
		// TODO Auto-generated constructor stub
		context = ctx;
	}

	public Context getContext() {
		return context;
	}

}
