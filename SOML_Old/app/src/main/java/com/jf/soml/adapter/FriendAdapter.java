package com.jf.soml.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jf.soml.R;
import com.jf.soml.model.UserModel;
import com.jf.soml.widget.CircularImageView;
import com.jf.soml.widget.FancyButton;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by hai on 12/24/14.
 */
public class FriendAdapter extends BasicAdapter {


    public interface OnFollowCallback {
        public void onFollow(UserModel userModel, boolean following);
    }

    private boolean following = false;
    private List<UserModel> mUserList = new ArrayList<>();
    private OnFollowCallback callback;

    public FriendAdapter(Context context, OnFollowCallback callback) {
        super(context);
        this.callback = callback;
    }


    public FriendAdapter(Context context, boolean following, OnFollowCallback callback) {
        super(context);
        this.following = following;
        this.callback = callback;
    }


    @Override
    public int getCount() {
        return mUserList.size();
    }

    @Override
    public UserModel getItem(int position) {
        return mUserList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.view_friend, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final UserModel model = getItem(position);

        holder.titleView.setText(model.getName());
        holder.storyView.setText(model.getStoryCount() + " stories");
        Picasso.with(getContext()).load(model.getThumbnail()).resize(300, 300).into(holder.avatarView);

        if (following) {
            holder.followBtn.setText("Following");
        } else {
            holder.followBtn.setText("Follow");
        }
        holder.followBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onFollow(model, following);
            }
        });

        return convertView;
    }

    public void addAll(List<UserModel> userList) {
        if (userList != null) {
            mUserList.addAll(userList);
            notifyDataSetChanged();
        }
    }

    public void remove(UserModel model) {
        mUserList.remove(model);
        notifyDataSetChanged();
    }

    class ViewHolder {
        TextView titleView, storyView;
        CircularImageView avatarView;
        FancyButton followBtn;

        ViewHolder(View v) {
            titleView = ButterKnife.findById(v, R.id.title_view);
            storyView = ButterKnife.findById(v, R.id.story_view);
            avatarView = ButterKnife.findById(v, R.id.avatar_view);
            followBtn = ButterKnife.findById(v, R.id.btn_follow);
        }
    }
}
