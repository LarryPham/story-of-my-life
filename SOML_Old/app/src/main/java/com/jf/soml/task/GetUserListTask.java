package com.jf.soml.task;

import android.content.Context;

import com.jf.soml.common.DataManager;
import com.jf.soml.rest.RestClientFactory;
import com.jf.soml.rest.RestClientTask;

import org.json.JSONObject;

/**
 * Created by Khai Ng on 12/8/14.
 */
public class GetUserListTask extends RestClientTask {

    public GetUserListTask(Context context) {
        super(context, RestClientFactory.getInstance().getPhotoClient(context));
    }

    @Override
    protected void doExecute() {
        restClient.addParam("auth_token", DataManager.getInstance(getContext()).getAuthToken().getToken());
        restClient.get("/api/users");
    }
}
