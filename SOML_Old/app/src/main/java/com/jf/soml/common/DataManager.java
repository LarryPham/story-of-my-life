package com.jf.soml.common;

import android.content.Context;

import com.jf.soml.model.AuthToken;
import com.jf.soml.model.UserModel;
import com.jf.soml.rest.PoseRestClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

/**
 * Created by hai on 1/7/15.
 */
public class DataManager {

    private static DataManager instance;

    private static Object lock = new Object();
    private Context context;
    private UserModel userModel;
    private AuthToken authToken;


    public static DataManager getInstance(Context context) {
        synchronized (lock) {
            if (instance == null)
                instance = new DataManager(context);

            return instance;
        }
    }

    private DataManager(Context context) {
        this.context = context;
    }

    /**
     * Get current user
     * @return
     */
    public UserModel getCurrentUser() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    public AuthToken getAuthToken() {
        return authToken;
    }

    public void setAuthToken(AuthToken authToken) {
        this.authToken = authToken;
    }

    private RequestParams getRequestParams() {
        RequestParams params = new RequestParams();
        params.add("auth_token", DataManager.getInstance(context).getAuthToken().getToken());

        return params;
    }

    public void getSignInUserTask(String email, String password, JsonHttpResponseHandler responseHandler) {
        RequestParams params = new RequestParams();
        params.put("email", email);
        params.put("password", password);

        PoseRestClient.getInstance().post("api/sessions", params, responseHandler);
    }

    public void getAccountDetail(JsonHttpResponseHandler responseHandler) {
        RequestParams params = getRequestParams();
        PoseRestClient.getInstance().get("api/account", params, responseHandler);
    }

    public void getUserList(JsonHttpResponseHandler responseHandler) {
        RequestParams params = getRequestParams();
        PoseRestClient.getInstance().get("api/users", params, responseHandler);
    }

    public void followUser(UserModel model, boolean follow, JsonHttpResponseHandler responseHandler) {
        RequestParams params = getRequestParams();
        PoseRestClient.getInstance().get("api/users/" + model.getId() + "/follows", params, responseHandler);
    }

    public void getUserFollower(JsonHttpResponseHandler responseHandler) {
        RequestParams params = getRequestParams();
        PoseRestClient.getInstance().get("/api/users/" + userModel.getId() + "/followings", params, responseHandler);
    }
}
