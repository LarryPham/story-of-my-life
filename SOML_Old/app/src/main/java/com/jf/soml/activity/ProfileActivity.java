package com.jf.soml.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.util.SparseArrayCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.balysv.materialmenu.MaterialMenuDrawable;
import com.balysv.materialmenu.extras.toolbar.MaterialMenuIconToolbar;
import com.jf.soml.R;
import com.jf.soml.activity.base.BaseActivity;
import com.jf.soml.common.DataManager;
import com.jf.soml.fragment.FavoriteFragment;
import com.jf.soml.fragment.MyStoryFragment;
import com.jf.soml.fragment.ScrollTabHolderFragment;
import com.jf.soml.fragment.SuggestionFragment;
import com.jf.soml.model.UserModel;
import com.jf.soml.widget.CircularImageView;
import com.jf.soml.widget.ScrollTabHolder;
import com.jf.soml.widget.SlidingTabLayout;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by hai on 12/23/14.
 */
public class ProfileActivity extends BaseActivity {
    final String COVER_IMAGE_URL = "https://lh5.googleusercontent.com/-DnUk7SnlO0w/VH1hxjXBz8I/AAAAAAAAMtc/ad9I44PQ2to/s630-fcrop64=1,000012cdfffff97c/big_hero_6_disney_baymax-wallpaper-1920x1200.jpg";
    final String AVATAR_IMAGE_URL = "http://images.boomsbeat.com/data/images/full/32034/422817_original-jpg.jpg";

    final Handler handler = new Handler();

    @InjectView(R.id.toolbar_view)
    Toolbar mToolBar;

    private MaterialMenuIconToolbar materialMenu;

    @InjectView(R.id.header_picture)
    ImageView headerImageView;

    @InjectView(R.id.header_logo)
    CircularImageView profileImageView;

    @InjectView(R.id.pager_view)
    ViewPager mViewPager;

    @InjectView(R.id.tab_view)
    SlidingTabLayout mPagerIndicator;

    @InjectView(R.id.title_view)
    TextView titleView;

    @InjectView(R.id.subtitle_view)
    TextView subTitleView;

    PagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // tintStatusBar();
        setContentView(R.layout.activity_profile);

        ButterKnife.inject(this);

        setSupportActionBar(mToolBar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");

        materialMenu = new MaterialMenuIconToolbar(this, Color.WHITE, MaterialMenuDrawable.Stroke.THIN) {
            @Override
            public int getToolbarViewId() {
                return R.id.toolbar_view;
            }
        };

        mToolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                materialMenu.animatePressedState(intToState(0));

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                }, 500);
            }
        });

        materialMenu.animatePressedState(intToState(1));

        mPagerAdapter = new PagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mPagerAdapter);

        mPagerIndicator.setViewPager(mViewPager);
        mPagerIndicator.setCustomTabView(R.layout.tab_indicator, R.id.tab_title);
        mPagerIndicator.setSelectedIndicatorColors(getResources().getColor(R.color.accent));
        mPagerIndicator.setDistributeEvenly(true);

        Picasso.with(this).load(COVER_IMAGE_URL).into(headerImageView);
        Picasso.with(this).load(AVATAR_IMAGE_URL).resize(200, 200).into(profileImageView);

        UserModel model;
        if (getIntent().getSerializableExtra("data") == null)
            model = DataManager.getInstance(this).getCurrentUser();
        else
            model = (UserModel) getIntent().getSerializableExtra("data");

        titleView.setText(model.getName());
        subTitleView.setText(model.getStoryCount() + " stories");
    }


    @Override
    protected void initView() {

    }

    public static MaterialMenuDrawable.IconState intToState(int state) {
        switch (state) {
            case 0:
                return MaterialMenuDrawable.IconState.BURGER;
            case 1:
                return MaterialMenuDrawable.IconState.ARROW;
            case 2:
                return MaterialMenuDrawable.IconState.X;
            case 3:
                return MaterialMenuDrawable.IconState.CHECK;
        }

        throw new IllegalArgumentException("Must be a number [0,3)");
    }


    public class PagerAdapter extends FragmentPagerAdapter {

        private SparseArrayCompat<ScrollTabHolder> mScrollTabHolders;
        private final String[] TITLES = {"My Story", "Favorite", "Following", "Follower"};
        private ScrollTabHolder mListener;

        public PagerAdapter(FragmentManager fm) {
            super(fm);
            mScrollTabHolders = new SparseArrayCompat<ScrollTabHolder>();
        }

        public void setTabHolderScrollingContent(ScrollTabHolder listener) {
            mListener = listener;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }

        @Override
        public Fragment getItem(int position) {
            ScrollTabHolderFragment fragment;

            switch (position) {
                case 0:
                    fragment = (ScrollTabHolderFragment) MyStoryFragment.newInstance(position);
                    break;
                case 1:
                    fragment = (ScrollTabHolderFragment) FavoriteFragment.newInstance(position);
                    break;
                case 2:
                    fragment = (ScrollTabHolderFragment) SuggestionFragment.newInstance(position);
                    break;
                case 3:
                    fragment = (ScrollTabHolderFragment) SuggestionFragment.newInstance(position);
                    break;
                default:
                    fragment = (ScrollTabHolderFragment) MyStoryFragment.newInstance(position);
                    break;
            }

            mScrollTabHolders.put(position, fragment);
            if (mListener != null) {
                fragment.setScrollTabHolder(mListener);
            }

            return fragment;
        }

        public SparseArrayCompat<ScrollTabHolder> getScrollTabHolders() {
            return mScrollTabHolders;
        }

    }
}
