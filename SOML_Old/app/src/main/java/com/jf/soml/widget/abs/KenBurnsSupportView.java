package com.jf.soml.widget.abs;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.jf.soml.R;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.view.ViewHelper;
import com.nineoldandroids.view.ViewPropertyAnimator;
import com.squareup.picasso.Picasso;

/**
 * Created by f.laurent on 21/11/13.
 */
public class KenBurnsSupportView extends FrameLayout {

	// private static final String TAG = "KenBurnsView";
	private final Handler HANDLER = new Handler();
	private final Random RANDOM = new Random();

	private ImageView[] mImageViews;
	private int mActiveImageIndex = -1;
	private int mActiveUrlIndex = -1;

	private int mSwapMs = 5000;
	private int mFadeInOutMs = 400;
	private float maxScaleFactor = 1.5F;
	private float minScaleFactor = 1.2F;

	private List<String> mImageUrlList = new ArrayList<String>();

	private Runnable mSwapImageRunnable = new Runnable() {
		@Override
		public void run() {
			swapImage();
			HANDLER.postDelayed(mSwapImageRunnable, mSwapMs - mFadeInOutMs * 2);
		}
	};

	public KenBurnsSupportView(Context context) {
		this(context, null);
	}

	public KenBurnsSupportView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public KenBurnsSupportView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	private void swapImage() {
		if (mActiveImageIndex == -1) {
			mActiveImageIndex = 1;
			mActiveUrlIndex = 1;

			if (mImageUrlList.size() > 0) {
				String uri = mImageUrlList.get(0);

				if (URLUtil.isValidUrl(uri))
					Picasso.with(getContext()).load(uri)
							.into(mImageViews[mActiveImageIndex]);
				else
					Picasso.with(getContext()).load(new File(uri))
							.into(mImageViews[mActiveImageIndex]);
			}

			animate(mImageViews[mActiveImageIndex]);

			return;
		}

		int inactiveIndex = mActiveImageIndex;
		mActiveImageIndex = (1 + mActiveImageIndex) % mImageViews.length;

		ImageView activeImageView = mImageViews[mActiveImageIndex];
		ViewHelper.setAlpha(activeImageView, 0.0f);

		if (mActiveUrlIndex < mImageUrlList.size() - 1) {
			mActiveUrlIndex++;
		} else {
			mActiveUrlIndex = 0;
		}

		String uri = mImageUrlList.get(mActiveUrlIndex);
		if (URLUtil.isValidUrl(uri))
			Picasso.with(getContext()).load(uri).into(activeImageView);
		else
			Picasso.with(getContext()).load(new File(uri))
					.into(activeImageView);

		ImageView inactiveImageView = mImageViews[inactiveIndex];

		animate(activeImageView);

		mAnimatorSet = new AnimatorSet();
		mAnimatorSet.setDuration(mFadeInOutMs);
		mAnimatorSet.playTogether(
				ObjectAnimator.ofFloat(inactiveImageView, "alpha", 1.0f, 0.0f),
				ObjectAnimator.ofFloat(activeImageView, "alpha", 0.0f, 1.0f));
		mAnimatorSet.start();
	}

	private AnimatorSet mAnimatorSet;
	private ViewPropertyAnimator mPropertyAnimator;

	private void start(View view, long duration, float fromScale,
			float toScale, float fromTranslationX, float fromTranslationY,
			float toTranslationX, float toTranslationY) {
		ViewHelper.setScaleX(view, fromScale);
		ViewHelper.setScaleY(view, fromScale);
		ViewHelper.setTranslationX(view, fromTranslationX);
		ViewHelper.setTranslationY(view, fromTranslationY);

		mPropertyAnimator = ViewPropertyAnimator.animate(view)
				.translationX(toTranslationX).translationY(toTranslationY)
				.scaleX(toScale).scaleY(toScale).setDuration(duration);

		mPropertyAnimator.start();
	}

	private float pickScale() {
		return this.minScaleFactor + this.RANDOM.nextFloat()
				* (this.maxScaleFactor - this.minScaleFactor);
	}

	private float pickTranslation(int value, float ratio) {
		return value * (ratio - 1.0f) * (this.RANDOM.nextFloat() - 0.5f);
	}

	public void animate(View view) {
		float fromScale = pickScale();
		float toScale = pickScale();
		float fromTranslationX = pickTranslation(view.getWidth(), fromScale);
		float fromTranslationY = pickTranslation(view.getHeight(), fromScale);
		float toTranslationX = pickTranslation(view.getWidth(), toScale);
		float toTranslationY = pickTranslation(view.getHeight(), toScale);

		start(view, this.mSwapMs, fromScale, toScale, fromTranslationX,
				fromTranslationY, toTranslationX, toTranslationY);
	}

	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
		// startKenBurnsAnimation();
	}

	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		HANDLER.removeCallbacks(mSwapImageRunnable);
	}

	private void startKenBurnsAnimation() {
		HANDLER.removeCallbacks(mSwapImageRunnable);

		HANDLER.post(mSwapImageRunnable);
	}

	private void stopKenBurnsAnimation() {
		if (mPropertyAnimator != null)
			mPropertyAnimator.cancel();

		if (mAnimatorSet != null)
			mAnimatorSet.cancel();

		HANDLER.removeCallbacks(mSwapImageRunnable);
	}

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		View view = inflate(getContext(), R.layout.view_kenburns, this);

		mImageViews = new ImageView[2];
		mImageViews[0] = (ImageView) view.findViewById(R.id.image0);
		mImageViews[1] = (ImageView) view.findViewById(R.id.image1);
	}

	public void setImageResIds(int... resourceIds) {
		// TODO Auto-generated method stub
		for (int i = 0; i < mImageViews.length; i++) {
			mImageViews[i].setImageResource(resourceIds[i]);
		}
	}

	public void setImageUrls(List<String> imageList) {
		// TODO Auto-generated method stub
		mImageUrlList.clear();

		// Set new URLs list
		if (imageList.size() > 0)
			mImageUrlList.addAll(imageList);

	}

	public void startStory() {
		for (int i = 0; i < mImageViews.length; i++) {
			if (i < mImageUrlList.size()) {
				String uri = mImageUrlList.get(i);
				if (URLUtil.isValidUrl(uri))
					Picasso.with(getContext()).load(uri).into(mImageViews[i]);
				else
					Picasso.with(getContext()).load(new File(uri))
							.into(mImageViews[i]);
			} else {
				String uri = mImageUrlList.get(0);
				if (URLUtil.isValidUrl(uri))
					Picasso.with(getContext()).load(uri).into(mImageViews[i]);
				else
					Picasso.with(getContext()).load(new File(uri))
							.into(mImageViews[i]);
			}
		}

		startKenBurnsAnimation();
	}

	public void stopStory() {
		stopKenBurnsAnimation();
	}
}
