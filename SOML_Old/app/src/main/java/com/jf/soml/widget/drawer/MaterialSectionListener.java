package com.jf.soml.widget.drawer;

/**
 * Created by neokree on 08/11/14.
 */
public interface MaterialSectionListener {

    public void onClick(MaterialSection section);
}
