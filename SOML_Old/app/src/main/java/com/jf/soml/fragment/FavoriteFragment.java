package com.jf.soml.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;

import com.jf.soml.adapter.StoryAdapter;
import com.jf.soml.model.Story;
import com.jf.soml.util.Logger;
import com.jf.soml.widget.CircleProgressView;
import com.parse.FindCallback;
import com.parse.ParseException;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import co.soml.android.R;

/**
 * Created by hai on 12/23/14.
 */
public class FavoriteFragment extends ScrollTabHolderFragment implements AbsListView.OnScrollListener {

    private static final Handler HANDLE = new Handler();

    private static final String ARG_POSITION = "position";

    @InjectView(R
        .id.list_view)
    ListView mListView;

    @InjectView(R.id.progress_view)
    CircleProgressView mProgressView;

    StoryAdapter mStoryAdapter;

    int mPosition;

    public static Fragment newInstance(int position) {
        FavoriteFragment fragment = new FavoriteFragment();

        Bundle bundle = new Bundle();
        bundle.putInt(ARG_POSITION, position);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPosition = getArguments().getInt(ARG_POSITION);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_story, container, false);
        ButterKnife.inject(this, rootView);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    @Override
    protected void initView() {
        mProgressView.setVisibility(View.VISIBLE);

        mStoryAdapter = new StoryAdapter(getActivity());

        mListView.setFooterDividersEnabled(false);
        mListView.setAdapter(mStoryAdapter);
        mListView.setOnScrollListener(this);

        // Feed the story
        Story.findInBackground(new FindCallback<Story>() {

            @Override
            public void done(final List<Story> stories, ParseException e) {
                // TODO Auto-generated method stub
                mProgressView.setVisibility(View.GONE);
                if (stories == null) {
                    Logger.e("ParseException: " + e.getLocalizedMessage());
                    return;
                }

                HANDLE.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        mStoryAdapter.addAll(stories);
                    }
                }, 500);
            }
        });
    }

    @Override
    public void adjustScroll(int scrollHeight) {
        if (scrollHeight == 0 && mListView.getFirstVisiblePosition() >= 1) {
            return;
        }

        mListView.setSelectionFromTop(1, scrollHeight);

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (mScrollTabHolder != null)
            mScrollTabHolder.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount, mPosition);
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        // nothing
    }
}
