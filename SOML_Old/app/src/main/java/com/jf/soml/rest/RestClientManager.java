package com.jf.soml.rest;

import org.apache.http.HttpVersion;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

public class RestClientManager {
	BasicHttpParams httpParams;
	SchemeRegistry schemeRegistry;

	public RestClientManager() {
		// TODO Auto-generated constructor stub
		schemeRegistry = new SchemeRegistry();
		schemeRegistry.register(new Scheme("http", PlainSocketFactory
				.getSocketFactory(), 80));
		schemeRegistry.register(new Scheme("https", PlainSocketFactory
				.getSocketFactory(), 443));

		httpParams = new BasicHttpParams();
	}

	public ClientConnectionManager getClientConnectionManager() {
		ConnManagerParams.setMaxTotalConnections(httpParams, 100);
		HttpProtocolParams.setVersion(httpParams, HttpVersion.HTTP_1_1);
		HttpProtocolParams.setUseExpectContinue(httpParams, true);

		return new ThreadSafeClientConnManager(httpParams, schemeRegistry);
	}

	public HttpParams getHttpParams() {
		// TODO Auto-generated method stub
		return httpParams;
	}
}
