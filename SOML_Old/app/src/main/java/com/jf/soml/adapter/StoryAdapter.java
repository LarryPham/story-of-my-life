package com.jf.soml.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.jf.soml.widget.abs.KenBurnsSupportView;
import com.jf.soml.R;
import com.jf.soml.model.Story;
import com.jf.soml.util.TimeAgo;
import com.jf.soml.widget.CircleProgressView;
import com.jf.soml.widget.CircularImageView;
import com.jf.soml.widget.SquareImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class StoryAdapter extends BasicAdapter {
	private List<Story> mStoryList;
	private Animation mFadeInAnim;

	private HashMap<String, Boolean> mLoadMap = new HashMap<String, Boolean>();

	private HashMap<Integer, Boolean> mPlayMap = new HashMap<Integer, Boolean>();

	static TimeAgo mTimeAgo;

	public StoryAdapter(Context ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
		mStoryList = new ArrayList<Story>();
		mFadeInAnim = AnimationUtils.loadAnimation(ctx, R.anim.fade_in);

		mTimeAgo = new TimeAgo(getContext());
	}

	public StoryAdapter(Context ctx, List<Story> storyList) {
		super(ctx);
		// TODO Auto-generated constructor stub
		mStoryList = new ArrayList<Story>();
		mFadeInAnim = AnimationUtils.loadAnimation(ctx, R.anim.fade_in);
		if (storyList != null) {
			mStoryList.addAll(storyList);
		}
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mStoryList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mStoryList.get(position);
	}

	@Override
	public long getItemId(int id) {
		// TODO Auto-generated method stub
		return id;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final StoryHolder holder;
		if (convertView == null) {
			convertView = LayoutInflater.from(getContext()).inflate(
					R.layout.view_story, null);
			holder = new StoryHolder(convertView);
			convertView.setTag(holder);
		} else {
			holder = (StoryHolder) convertView.getTag();
		}

		mPlayMap.put(position, false);

		holder.updateView(mStoryList.get(position));

		holder.btnPlay.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (!mPlayMap.get(position)) {
					mPlayMap.put(position, true);

					holder.playerView.startStory();

					holder.btnPlay.setText(R.string.ic_pause);

					holder.playerView.setVisibility(View.VISIBLE);

					holder.featurePhotoView.setVisibility(View.INVISIBLE);
				} else {
					mPlayMap.put(position, false);

					holder.playerView.stopStory();

					holder.featurePhotoView.setVisibility(View.VISIBLE);

					holder.playerView.setVisibility(View.INVISIBLE);

					holder.btnPlay.setText(R.string.ic_play);
				}
			}
		});

		if (!mPlayMap.get(position)) {
			holder.featurePhotoView.setVisibility(View.VISIBLE);
			holder.playerView.setVisibility(View.INVISIBLE);
			holder.btnPlay.setText(R.string.ic_play);
			holder.playerView.stopStory();
		}

		return convertView;
	}

	class StoryHolder {
		TextView title, time, like, comment;
		TextView btnPlay;
		TextView location;
		SquareImageView featurePhotoView;
		CircleProgressView progressView;
		TextView userName;
		CircularImageView userAvatar;
		KenBurnsSupportView playerView;

		public StoryHolder(View view) {
			// TODO Auto-generated constructor stub
			title = (TextView) view.findViewById(R.id.title_view);
			time = (TextView) view.findViewById(R.id.time_view);
			like = (TextView) view.findViewById(R.id.like_view);
			comment = (TextView) view.findViewById(R.id.comment_view);
			btnPlay = (TextView) view.findViewById(R.id.btn_play);

			playerView = (KenBurnsSupportView) view
					.findViewById(R.id.player_view);

			location = (TextView) view.findViewById(R.id.location_view);
			userName = (TextView) view.findViewById(R.id.username_view);

			featurePhotoView = (SquareImageView) view
					.findViewById(R.id.feature_photo);
			userAvatar = (CircularImageView) view
					.findViewById(R.id.avatar_view);

			progressView = (CircleProgressView) view
					.findViewById(R.id.loading_view);
			progressView.setVisibility(View.GONE);

		}

		public void updateView(final Story story) {
			// TODO Auto-generated method stub
			title.setText(story.getName());
			time.setText(mTimeAgo.timeAgo(story.getCreatedAt()));
			like.setText(story.getLikeCount() + " likes");
			comment.setText(story.getCommentCount() + " comments");
			location.setText(story.getLocation());
			userName.setText(story.getUserName());

			Picasso.with(getContext()).load(story.getUserAvatar())
					.resize(150, 150).error(R.drawable.user_avatar)
					.placeholder(R.drawable.user_avatar).into(userAvatar);

			Picasso.with(getContext()).load(story.getFeaturePhoto())
					.error(R.drawable.story_preview)
					// .placeholder(R.drawable.story_preview)
					.into(featurePhotoView, new Callback() {

						@Override
						public void onSuccess() {
							// TODO Auto-generated method stub
							if (mLoadMap.get(story.getObjectId()) == null
									|| !mLoadMap.get(story.getObjectId())) {
								mLoadMap.put(story.getObjectId(), true);

								mFadeInAnim
										.setAnimationListener(new AnimationListener() {

											@Override
											public void onAnimationStart(
													Animation animation) {
												// TODO Auto-generated method
												// stub

											}

											@Override
											public void onAnimationRepeat(
													Animation animation) {
												// TODO Auto-generated method
												// stub

											}

											@Override
											public void onAnimationEnd(
													Animation animation) {
												// TODO Auto-generated method
												// stub
												// featurePhotoView
												// .setVisibility(View.VISIBLE);
											}
										});
								featurePhotoView.startAnimation(mFadeInAnim);
							} else {
								// Do nothing
							}
						}

						@Override
						public void onError() {
							// TODO Auto-generated method stub

						}
					});

			if (story.getImageList().size() > 0)
				playerView.setImageUrls(story.getImageList());
		}
	}

	public void addAll(List<Story> stories) {
		// TODO Auto-generated method stub
		mStoryList.clear();
		if (stories != null && stories.size() > 0) {
			mStoryList.addAll(stories);
			notifyDataSetChanged();
		}
	}
}
