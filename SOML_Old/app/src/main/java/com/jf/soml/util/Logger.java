package com.jf.soml.util;

import android.util.Log;

public class Logger {
	private static final String TAG = "Photor";
	private static final boolean DEBUG = true;

	public static void e(String tag, String message) {
		if (DEBUG) {
			Log.e(tag, message);
		}
	}

	public static <T> void e(Class<T> cls, String message) {
		if (DEBUG) {
			Log.e(cls.getSimpleName(), message);
		}
	}

	public static void e(String message) {
		if (DEBUG) {
			Log.e(TAG, message);
		}
	}

	public static void d(String tag, String message) {
		if (DEBUG) {
			Log.d(tag, message);
		}
	}

	public static void v(String tag, String message) {
		if (DEBUG) {
			Log.v(tag, message);
		}
	}

	public static <T> void d(Class<T> cls, String message) {
		if (DEBUG) {
			Log.d(cls.getSimpleName(), message);
		}
	}

	public static void d(String message) {
		if (DEBUG) {
			Log.d(TAG, message);
		}
	}

	public static void i(String tag, String message) {
		if (DEBUG) {
			Log.i(tag, message);
		}
	}

	public static <T> void i(Class<T> cls, String message) {
		if (DEBUG) {
			Log.i(cls.getSimpleName(), message);
		}
	}

	public static void i(String message) {
		if (DEBUG) {
			Log.i(TAG, message);
		}
	}

}
