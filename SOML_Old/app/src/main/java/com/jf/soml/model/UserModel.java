package com.jf.soml.model;

import java.io.Serializable;

import com.parse.ParseClassName;
import com.parse.ParseObject;

@ParseClassName("User")
public class UserModel extends ParseObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9007643425043236806L;

    private int id;

	private String email;

	private String fullName;

	private String thumbnail;

    private String language;

    private int storyCount;

    private AuthToken authToken;
    private boolean following;

    public UserModel() {
		// TODO Auto-generated constructor stub
	}

    public UserModel(int id) {
        // TODO Auto-generated constructor stub
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public AuthToken getAuthToken() {
        return authToken;
    }

    public void setAuthToken(AuthToken authToken) {
        this.authToken = authToken;
    }

    public int getStoryCount() {
        return storyCount;
    }

    public void setStoryCount(int storyCount) {
        this.storyCount = storyCount;
    }

    public boolean isFollowing() {
        return following;
    }
}
