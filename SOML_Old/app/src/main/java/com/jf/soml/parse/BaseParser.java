package com.jf.soml.parse;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import org.json.JSONArray;
import org.json.JSONObject;

public abstract class BaseParser {
	protected static final int ERROR_CODE = -99;

	protected JSONObject mData;

	public BaseParser(JSONObject result) {
		// TODO Auto-generated constructor stub
		mData = result;
	}

	protected boolean isNull(String key) {
		// TODO Auto-generated constructor stub
		return !mData.has(key);
	}

	public JSONObject getJsonObject(String key) {
		// TODO Auto-generated method stub
		try {
			return mData.getJSONObject(key);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	public JSONObject getJsonObject(JSONObject jsonObject, String key) {
		// TODO Auto-generated method stub
		try {
			return jsonObject.getJSONObject(key);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	public JSONObject getJsonObject(JSONArray jsonArray, int i) {
		// TODO Auto-generated method stub
		try {
			return jsonArray.getJSONObject(i);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	public JSONArray getJSONArray(String key) {
		try {
			return mData.getJSONArray(key);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	public JSONArray getJSONArray(JSONObject jsonObject, String key) {
		if (jsonObject == null)
			return null;

		if (jsonObject.has(key)) {
			try {
				return jsonObject.getJSONArray(key);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		return null;
	}

	/**
	 * Parse lyric from Zing MP3
	 * 
	 * @param key
	 * @return
	 */
	public String getString(String key) {
		try {
			return URLDecoder.decode(mData.getString(key), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "";
	}

	public String getString(JSONObject jsObj, String key) {
		try {
			if (jsObj != null && key != null && jsObj.has(key)) {
				String data = jsObj.getString(key);
				if (data != null)
					return URLDecoder.decode(jsObj.getString(key), "UTF-8");
				else
					return "";
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new String("");
	}

	public int getInt(String key) {
		try {
			if (mData.has(key))
				return mData.getInt(key);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return ERROR_CODE;
	}

	public double getDouble(JSONObject object, String key) {
		try {
			return object.getDouble(key);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return ERROR_CODE;
	}

	public double getDouble(String key) {
		try {
			return mData.getDouble(key);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return ERROR_CODE;
	}

	public int getInt(JSONObject jsObj, String key) {
		try {
			return jsObj.getInt(key);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return ERROR_CODE;
	}

	public boolean getBoolean(String key) {
		// TODO Auto-generated method stub
		try {
			return mData.getBoolean(key);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return false;
	}

	public boolean getBoolean(JSONObject jsObj, String key) {
		try {
			return jsObj.getBoolean(key);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return false;
	}

	public long getLong(String key) {
		try {
			return mData.getLong(key);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return ERROR_CODE;
	}

	public long getLong(JSONObject jsObj, String key) {
		try {
			return jsObj.getLong(key);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return ERROR_CODE;
	}
}
