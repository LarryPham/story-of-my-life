package com.jf.soml.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.jf.soml.R;
import com.jf.soml.activity.base.BaseActivity;
import com.jf.soml.camera.ImageCleanupTask.ImageCleanUpCallback;
import com.jf.soml.fragment.CaptureFragment;

import java.io.File;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class CaptureActivity extends BaseActivity implements
        CaptureFragment.Contract {

    private static final String STATE_SINGLE_SHOT = "single_shot";
    private static final String STATE_LOCK_TO_LANDSCAPE = "lock_to_landscape";
    private static final int CONTENT_REQUEST = 1337;

    private CaptureFragment mStandardCameraFragment = null;
    private CaptureFragment mFrontFacingCameraFragment = null;
    private CaptureFragment mCameraFragment = null;

    private boolean singleShot = false;
    private boolean isLockedToLandscape = false;

    @InjectView(R.id.toolbar_view)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capture);
        ButterKnife.inject(this);

        tintStatusBar();
        setSupportActionBar(toolbar);
		getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mCameraFragment = CaptureFragment.newInstance(false);

        getFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, mCameraFragment).commit();

        Toast toast = Toast.makeText(this, "To start, capture first photo",
                Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();

        initView();
    }

    @Override
    protected void initView() {
        // TODO Auto-generated method stub

    }

    /**
     * Change camera front/face
     *
     * @param position
     * @return
     */
    private boolean onCameraChange(int position) {
        if (position == 0) {
            if (mStandardCameraFragment == null) {
                mStandardCameraFragment = CaptureFragment
                        .newInstance(false);
            }

            mCameraFragment = mStandardCameraFragment;
        } else {
            if (mFrontFacingCameraFragment == null) {
                mFrontFacingCameraFragment = CaptureFragment
                        .newInstance(true);
            }

            mCameraFragment = mFrontFacingCameraFragment;
        }

        getFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, mCameraFragment).commit();

        findViewById(android.R.id.content).post(new Runnable() {
            @Override
            public void run() {
                mCameraFragment.lockToLandscape(isLockedToLandscape);
            }
        });

        return (true);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        setSingleShotMode(savedInstanceState.getBoolean(STATE_SINGLE_SHOT));
        isLockedToLandscape = savedInstanceState
                .getBoolean(STATE_LOCK_TO_LANDSCAPE);

        if (mCameraFragment != null) {
            mCameraFragment.lockToLandscape(isLockedToLandscape);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(STATE_SINGLE_SHOT, isSingleShotMode());
        outState.putBoolean(STATE_LOCK_TO_LANDSCAPE, isLockedToLandscape);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // new MenuInflater(this).inflate(R.menu.menu_creat, menu);
        // menu.findItem(R.id.landscape).setChecked(isLockedToLandscape);
        return (super.onCreateOptionsMenu(menu));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.content) {
            Intent capture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File dir = Environment
                    .getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
            File output = new File(dir, "CameraContentDemo.jpeg");
            capture.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(output));
            startActivityForResult(capture, CONTENT_REQUEST);

        } else if (item.getItemId() == R.id.landscape) {
            item.setChecked(!item.isChecked());
            mCameraFragment.lockToLandscape(item.isChecked());
            isLockedToLandscape = item.isChecked();
        } else if (item.getItemId() == R.id.fullscreen) {
            // startActivity(new Intent(this, FullScreenActivity.class));
        }

        return (super.onOptionsItemSelected(item));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CONTENT_REQUEST) {
            if (resultCode == RESULT_OK) {
                // do nothing
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_CAMERA && mCameraFragment != null
                && !mCameraFragment.isSingleShotProcessing()) {
            mCameraFragment.takePicture(new ImageCleanUpCallback() {

                @Override
                public void onComplete() {
                    // TODO Auto-generated method stub

                }

            });

            return (true);
        }

        return (super.onKeyDown(keyCode, event));
    }

    @Override
    public boolean isSingleShotMode() {
        return (singleShot);
    }

    @Override
    public void setSingleShotMode(boolean mode) {
        singleShot = mode;
    }

}
