package com.jf.soml.activity.base;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.balysv.materialmenu.MaterialMenuDrawable;
import com.balysv.materialmenu.extras.toolbar.MaterialMenuIconToolbar;
import com.jf.soml.R;
import com.jf.soml.widget.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.LinkedList;
import java.util.List;

import com.jf.soml.widget.drawer.MaterialAccount;
import com.jf.soml.widget.drawer.MaterialAccountListener;
import com.jf.soml.widget.drawer.MaterialSection;
import com.jf.soml.widget.drawer.MaterialSectionListener;

/**
 * Activity that implements ActionBarActivity with a Navigation Drawer with Material Design style
 *
 * @author created by neokree
 */
@SuppressLint("InflateParams")
public abstract class NavigationDrawerActivity<Fragment> extends BaseActivity implements MaterialSectionListener {

    public static final int BOTTOM_SECTION_START = 100;
    public static final int USER_CHANGE_TRANSITION = 500;

    private DrawerLayout layout;
    private ActionBar actionBar;
    private ActionBarDrawerToggle pulsante;
    private Toolbar toolbar;
    private RelativeLayout content;
    private RelativeLayout drawer;

    private CircularImageView userphoto;
    private CircularImageView userSecondPhoto;
    private CircularImageView userThirdPhoto;

    private ImageView usercover;
    private ImageView usercoverSwitcher;
    private TextView username;
    private TextView usermail;
    private LinearLayout sections;
    private LinearLayout bottomSections;


    private List<MaterialSection> sectionList;
    private List<MaterialSection> bottomSectionList;
    private List<MaterialAccount> accountManager;
    private MaterialSection currentSection;
    private MaterialAccount currentAccount;

    private MaterialMenuIconToolbar materialMenu;

    private CharSequence title;
    private float density;
    private int primaryColor;
    private int primaryDarkColor;

    private boolean slidingDrawerEffect = false;
    private boolean multiPaneSupport = false;
    private boolean isDrawerOpened = false;

    private Resources resources;


    private View.OnClickListener currentAccountListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (accountListener != null) {
                accountListener.onAccountOpening(currentAccount);
            }

            if (!deviceSupportMultiPane())
                layout.closeDrawer(drawer);
        }
    };

    private View.OnClickListener secondAccountListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // account change
            MaterialAccount account = findAccountNumber(MaterialAccount.SECOND_ACCOUNT);
            if (account != null) {
                if (accountListener != null)
                    accountListener.onChangeAccount(account);

                switchAccounts(account);
            } else {
                accountListener.onAccountOpening(currentAccount);
                if (!deviceSupportMultiPane())
                    layout.closeDrawer(drawer);
            }

        }
    };
    private View.OnClickListener thirdAccountListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            // account change
            MaterialAccount account = findAccountNumber(MaterialAccount.THIRD_ACCOUNT);
            if (account != null) {
                if (accountListener != null)
                    accountListener.onChangeAccount(account);

                switchAccounts(account);
            } else {// if there is no second account user clicked for open it
                accountListener.onAccountOpening(currentAccount);
                if (!deviceSupportMultiPane())
                    layout.closeDrawer(drawer);
            }

        }
    };
    private MaterialAccountListener accountListener;

    @Override
    /**
     * Do not Override this method!!! <br>
     * Use init() instead
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer);
        Window window = this.getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        // Replace action bar by toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        materialMenu = new MaterialMenuIconToolbar(this, Color.WHITE, MaterialMenuDrawable.Stroke.THIN) {
            @Override
            public int getToolbarViewId() {
                return R.id.toolbar;
            }
        };
        materialMenu.setNeverDrawTouch(true);

        layout = (DrawerLayout) this.findViewById(R.id.drawer_layout);
        content = (RelativeLayout) this.findViewById(R.id.content);
        drawer = (RelativeLayout) this.findViewById(R.id.drawer);
        username = (TextView) this.findViewById(R.id.user_nome);
        usermail = (TextView) this.findViewById(R.id.user_email);

        userphoto = (CircularImageView) this.findViewById(R.id.user_photo);
        userSecondPhoto = (CircularImageView) this.findViewById(R.id.user_photo_2);
        userThirdPhoto = (CircularImageView) this.findViewById(R.id.user_photo_3);

        usercover = (ImageView) this.findViewById(R.id.user_cover);
        usercoverSwitcher = (ImageView) this.findViewById(R.id.user_cover_switcher);
        sections = (LinearLayout) this.findViewById(R.id.sections);
        bottomSections = (LinearLayout) this.findViewById(R.id.bottom_sections);

        sectionList = new LinkedList<>();
        bottomSectionList = new LinkedList<>();
        accountManager = new LinkedList<>();

        userphoto.setOnClickListener(currentAccountListener);
        usercover.setOnClickListener(currentAccountListener);
        userSecondPhoto.setOnClickListener(secondAccountListener);
        userThirdPhoto.setOnClickListener(thirdAccountListener);

        resources = this.getResources();
        density = resources.getDisplayMetrics().density;

        Resources.Theme theme = this.getTheme();
        TypedValue typedValue = new TypedValue();
        theme.resolveAttribute(R.attr.colorPrimary, typedValue, true);
        primaryColor = typedValue.data;
        theme.resolveAttribute(R.attr.colorPrimaryDark, typedValue, true);
        primaryDarkColor = typedValue.data;


        if (layout.isDrawerOpen(drawer))
            layout.closeDrawer(drawer);

        init(savedInstanceState);

        if (sectionList.size() == 0) {
            throw new RuntimeException("You must add at least one Section to top list.");
        }

        title = sectionList.get(0).getTitle();

        Configuration configuration = this.getResources().getConfiguration();
        if (deviceSupportMultiPane()) {
            layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN, drawer);
            DrawerLayout.LayoutParams params = new DrawerLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            params.setMargins((int) (320 * density), 0, 0, 0);
            content.setLayoutParams(params);
            layout.setScrimColor(Color.TRANSPARENT);
            layout.openDrawer(drawer);
        } else {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);

            pulsante = new ActionBarDrawerToggle(this, layout, toolbar, R.string.app_name, R.string.app_name) {

                public void onDrawerClosed(View view) {
                    isDrawerOpened = false;
                    invalidateOptionsMenu();
                }

                public void onDrawerOpened(View drawerView) {
                    isDrawerOpened = true;
                    invalidateOptionsMenu();
                }

                @Override
                public void onDrawerSlide(View drawerView, float slideOffset) {
                    if (slidingDrawerEffect)
                        super.onDrawerSlide(drawerView, slideOffset);

                    materialMenu.setTransformationOffset(
                            MaterialMenuDrawable.AnimationState.BURGER_ARROW,
                            isDrawerOpened ? 2 - slideOffset : slideOffset
                    );
                }
            };

            layout.setDrawerListener(pulsante);
        }

        ViewTreeObserver vto = usercover.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                int width = drawer.getWidth();
                int heightCover = (9 * width) / 16;

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                    heightCover -= (density * 25);
                }

                usercover.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, heightCover));
                usercoverSwitcher.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, heightCover));

                Point size = new Point();
                getWindowManager().getDefaultDisplay().getSize(size);
                int heightDrawer = (int) (((25 + 8 + 1) * density) + heightCover + sections.getHeight() + ((density * 48) * bottomSectionList.size()));

                if (heightDrawer >= size.y) {
                    addDivisor();
                    for (MaterialSection section : bottomSectionList) {
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) (48 * density));
                        sections.addView(section.getView(), params);
                    }
                } else
                    for (MaterialSection section : bottomSectionList) {
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) (48 * density));
                        bottomSections.addView(section.getView(), params);
                    }

                ViewTreeObserver obs = usercover.getViewTreeObserver();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    obs.removeOnGlobalLayoutListener(this);
                } else {
                    obs.removeGlobalOnLayoutListener(this);
                }
            }

        });

        if (accountManager.size() > 0) {
            currentAccount = accountManager.get(0);
            notifyAccountDataChanged();
        }


        MaterialSection section = sectionList.get(0);
        currentSection = section;
        section.select();
        setFragment((Fragment) section.getTargetFragment(), section.getTitle(), null);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        materialMenu.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (!deviceSupportMultiPane())
            if (pulsante.onOptionsItemSelected(item)) {
                return true;
            }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (!deviceSupportMultiPane())
            pulsante.syncState();

        isDrawerOpened = layout.isDrawerOpen(Gravity.START);
        materialMenu.syncState(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (!deviceSupportMultiPane()) {
            pulsante.onConfigurationChanged(newConfig);
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        this.title = title;
        this.getSupportActionBar().setTitle(title);
    }

    private void setFragment(Fragment fragment, String title, Fragment oldFragment) {
        // si setta il titolo
        setTitle(title);

        // change for default Fragment / support Fragment
        if (fragment instanceof android.app.Fragment) {
            if (oldFragment instanceof android.support.v4.app.Fragment)
                throw new RuntimeException("You should use only one type of Fragment");


            FragmentTransaction ft = getFragmentManager().beginTransaction();
            if (oldFragment != null && fragment != oldFragment)
                ft.remove((android.app.Fragment) oldFragment);

            ft.replace(R.id.frame_container, (android.app.Fragment) fragment).commit();
        } else if (fragment instanceof android.support.v4.app.Fragment) {
            if (oldFragment instanceof android.app.Fragment)
                throw new RuntimeException("You should use only one type of Fragment");

            android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            if (oldFragment != null && oldFragment != fragment)
                ft.remove((android.support.v4.app.Fragment) oldFragment);

            ft.replace(R.id.frame_container, (android.support.v4.app.Fragment) fragment).commit();
        } else
            throw new RuntimeException("Fragment must be android.app.Fragment or android.support.v4.app.Fragment");

        if (!deviceSupportMultiPane())
            layout.closeDrawer(drawer);
    }

    private MaterialAccount findAccountNumber(int number) {
        for (MaterialAccount account : accountManager)
            if (account.getAccountNumber() == number)
                return account;


        return null;
    }

    private void switchAccounts(final MaterialAccount newAccount) {
        final ImageView floatingImage = new ImageView(this);
        Rect startingRect = new Rect();
        Rect finalRect = new Rect();
        Point offsetHover = new Point();

        // 64dp primary user image / 40dp other user image = 1.6 scale
        float finalScale = 1.6f;

        final int statusBarHeight;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            statusBarHeight = (int) (25 * density);
        } else {
            statusBarHeight = 0;
        }

        ImageView photoClicked;
        if (newAccount.getAccountNumber() == MaterialAccount.SECOND_ACCOUNT) {
            photoClicked = userSecondPhoto;
        } else {
            photoClicked = userThirdPhoto;
        }
        photoClicked.getGlobalVisibleRect(startingRect, offsetHover);
        floatingImage.setImageDrawable(photoClicked.getDrawable());

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(photoClicked.getWidth(), photoClicked.getHeight());
        params.setMargins(offsetHover.x, offsetHover.y - statusBarHeight, 0, 0);
        drawer.addView(floatingImage, params);

        photoClicked.setImageBitmap(currentAccount.getCircularPhoto());

        usercoverSwitcher.setImageBitmap(newAccount.getBackground());

        userphoto.getGlobalVisibleRect(finalRect);

        int offset = (((finalRect.bottom - finalRect.top) - (startingRect.bottom - finalRect.top)) / 2);
        finalRect.offset(offset, offset - statusBarHeight);
        startingRect.offset(0, -statusBarHeight);

        AnimatorSet set = new AnimatorSet();

        set.play(ObjectAnimator.ofFloat(floatingImage, View.X, startingRect.left, finalRect.left))
                .with(ObjectAnimator.ofFloat(floatingImage, View.Y, startingRect.top, finalRect.top))
                .with(ObjectAnimator.ofFloat(floatingImage, View.SCALE_X, 1f, finalScale))
                .with(ObjectAnimator.ofFloat(floatingImage, View.SCALE_Y, 1f, finalScale))
                .with(ObjectAnimator.ofFloat(userphoto, View.ALPHA, 1f, 0f))
                .with(ObjectAnimator.ofFloat(usercover, View.ALPHA, 1f, 0f))
                .with(ObjectAnimator.ofFloat(photoClicked, View.SCALE_X, 0f, 1f))
                .with(ObjectAnimator.ofFloat(photoClicked, View.SCALE_Y, 0f, 1f));
        set.setDuration(USER_CHANGE_TRANSITION);
        set.setInterpolator(new DecelerateInterpolator());
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {

                ((View) userphoto).setAlpha(1);
                setFirstAccountPhoto(newAccount.getCircularPhoto());

                drawer.removeView(floatingImage);

                setUserEmail(newAccount.getSubTitle());
                setUsername(newAccount.getTitle());

                setDrawerBackground(currentAccount.getCoverUrl());
                ((View) usercover).setAlpha(1);

                currentAccount.setAccountNumber(newAccount.getAccountNumber());
                newAccount.setAccountNumber(MaterialAccount.FIRST_ACCOUNT);

                currentAccount = newAccount;

                if (!deviceSupportMultiPane())
                    layout.closeDrawer(drawer);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                onAnimationEnd(animation);
            }
        });

        set.start();

    }

    private void setUserEmail(String email) {
        this.usermail.setText(email);
    }

    private void setUsername(String username) {
        this.username.setText(username);
    }

    private void setFirstAccountPhoto(Bitmap photo) {
        userphoto.setImageBitmap(photo);
    }

    private void setFirstAccountPhoto(String photoUrl) {
        if (photoUrl != null && URLUtil.isValidUrl(photoUrl))
            Picasso.with(this).load(photoUrl).resize(300, 300).into(userphoto);
    }

    private void setSecondAccountPhoto(Bitmap photo) {
        userSecondPhoto.setImageBitmap(photo);
    }

    private void setThirdAccountPhoto(Bitmap photo) {
        userThirdPhoto.setImageBitmap(photo);
    }

    private void setDrawerBackground(Bitmap background) {
        usercover.setImageBitmap(background);
    }

    private void setDrawerBackground(String background) {
        Picasso.with(this).load(background).fit().into(usercover);
    }

    private boolean deviceSupportMultiPane() {
        if (multiPaneSupport && resources.getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE && resources.getConfiguration().smallestScreenWidthDp >= 600)
            return true;
        else
            return false;
    }

    protected int darkenColor(int color) {
        if (color == primaryColor)
            return primaryDarkColor;

        float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);
        hsv[2] *= 0.8f; // value component
        return Color.HSVToColor(hsv);
    }

    @Override
    public void onClick(MaterialSection section) {

        if (section.getTarget() == MaterialSection.TARGET_FRAGMENT) {
            setFragment((Fragment) section.getTargetFragment(), section.getTitle(), (Fragment) currentSection.getTargetFragment());

            // setting toolbar color if is setted
            if (section.hasSectionColor()) {
                this.getToolbar().setBackgroundColor(section.getSectionColor());
            } else {
                this.getToolbar().setBackgroundColor(primaryColor);
            }
        } else {
            this.startActivity(section.getTargetIntent());
        }

        int position = section.getPosition();

        for (MaterialSection mySection : sectionList) {
            if (position != mySection.getPosition())
                mySection.unSelect();
        }
        for (MaterialSection mySection : bottomSectionList) {
            if (position != mySection.getPosition())
                mySection.unSelect();
        }

        currentSection = section;
    }

    public void setAccountListener(MaterialAccountListener listener) {
        this.accountListener = listener;
    }

    public void addMultiPaneSupport() {
        this.multiPaneSupport = true;
    }

    public void allowArrowAnimation() {
        slidingDrawerEffect = true;
    }

    // Method used for customize layout

    public void addSection(MaterialSection section) {
        section.setPosition(sectionList.size());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) (48 * density));
        sectionList.add(section);
        sections.addView(section.getView(), params);
    }

    public void addBottomSection(MaterialSection section) {
        section.setPosition(BOTTOM_SECTION_START + bottomSectionList.size());
        //LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,(int)(48 * density));
        bottomSectionList.add(section);
        //bottomSections.addView(section.getView(),params);
    }

    public void addDivisor() {
        View view = new View(this);
        view.setBackgroundColor(Color.parseColor("#e0e0e0"));
        // height 1 px
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 1);
        params.setMargins(0, (int) (8 * density), 0, (int) (8 * density));

        sections.addView(view, params);
    }

    public void addAccount(MaterialAccount account) {
        if (accountManager.size() == 3)
            throw new RuntimeException("Currently are supported max 3 accounts");

        account.setAccountNumber(accountManager.size());
        accountManager.add(account);
    }


    /**
     * Reload Application data from Account Information
     */
    public void notifyAccountDataChanged() {
        switch (accountManager.size()) {
            case 3:
                this.setThirdAccountPhoto(findAccountNumber(MaterialAccount.THIRD_ACCOUNT).getCircularPhoto());
            case 2:
                this.setSecondAccountPhoto(findAccountNumber(MaterialAccount.SECOND_ACCOUNT).getCircularPhoto());
            case 1:
                this.setFirstAccountPhoto(currentAccount.getPhotoUrl());
                this.setDrawerBackground(currentAccount.getCoverUrl());
                this.setUsername(currentAccount.getTitle());
                this.setUserEmail(currentAccount.getSubTitle());
            default:
        }
    }

    // create sections

    public MaterialSection newSection(String title, Drawable icon, Fragment target) {
        MaterialSection section = new MaterialSection<Fragment>(this, true, MaterialSection.TARGET_FRAGMENT);
        section.setOnClickListener(this);
        section.setIcon(icon);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    public MaterialSection newSection(String title, Drawable icon, Intent target) {
        MaterialSection section = new MaterialSection<Fragment>(this, true, MaterialSection.TARGET_ACTIVITY);
        section.setOnClickListener(this);
        section.setIcon(icon);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    public MaterialSection newSection(String title, Bitmap icon, Fragment target) {
        MaterialSection section = new MaterialSection<Fragment>(this, true, MaterialSection.TARGET_FRAGMENT);
        section.setOnClickListener(this);
        section.setIcon(icon);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    public MaterialSection newSection(String title, Bitmap icon, Intent target) {
        MaterialSection section = new MaterialSection<Fragment>(this, true, MaterialSection.TARGET_ACTIVITY);
        section.setOnClickListener(this);
        section.setIcon(icon);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    public MaterialSection newSection(String title, Fragment target) {
        MaterialSection section = new MaterialSection<Fragment>(this, false, MaterialSection.TARGET_FRAGMENT);
        section.setOnClickListener(this);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    public MaterialSection newSection(String title, Intent target) {
        MaterialSection section = new MaterialSection<Fragment>(this, false, MaterialSection.TARGET_ACTIVITY);
        section.setOnClickListener(this);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    public abstract void init(Bundle savedInstanceState);

    public Toolbar getToolbar() {
        return toolbar;
    }

    public MaterialSection getCurrentSection() {
        return currentSection;
    }

    public MaterialAccount getCurrentAccount() {
        return currentAccount;
    }

    public MaterialAccount getAccountAtCurrentPosition(int position) {

        if (position < 0 || position >= accountManager.size())
            throw new RuntimeException("Account Index Overflow");

        return findAccountNumber(position);
    }


}