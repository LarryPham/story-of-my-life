package com.jf.soml.activity.base;

import android.annotation.TargetApi;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.jf.soml.R;
import com.jf.soml.rest.RestClientTask;
import com.jf.soml.util.Logger;
import com.jf.soml.util.SystemBarTintManager;
import com.jf.soml.widget.CircleProgressView;

import org.json.JSONObject;

public abstract class BaseActivity extends ActionBarActivity implements RestClientTask.OnPreExecuteDelegate, RestClientTask.OnPostExecuteDelegate {

    protected abstract void initView();

    protected CharSequence mTitle;
    protected CircleProgressView mProgressView;
    protected Location mLastKnownLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        // tintStatusBar();
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if ((ni == null) || (!ni.isConnected())) {
            Toast.makeText(getApplicationContext(),
                    getResources().getString(R.string.device_offline_message),
                    Toast.LENGTH_LONG).show();
        }
    }

    protected void tintStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            setTranslucentStatus(true);
        }

        SystemBarTintManager tintManager = new SystemBarTintManager(this);
        tintManager.setStatusBarTintEnabled(true);
        tintManager.setStatusBarTintResource(R.color.premium);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @TargetApi(19)
    protected void setTranslucentStatus(boolean on) {
        Window window = getWindow();
        WindowManager.LayoutParams windowParams = window.getAttributes();
        final int bits = WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;
        if (on) {
            windowParams.flags |= bits;
        } else {
            windowParams.flags &= ~bits;
        }
        window.setAttributes(windowParams);
    }

    @Override
    public void actionPost(RestClientTask task, JSONObject result) {
        if (result == null) {
            Logger.e("Failed to execute task " + task.getClass().getName());
            return;
        }
    }

    @Override
    public void actionPre(RestClientTask task) {

    }


    /**
     * Display message
     *
     * @param message
     */
    protected void makeToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    /**
     * Hide soft keyboard
     */
    protected void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }


    @Override
    public void finish() {
        // TODO Auto-generated method stub
        super.finish();
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        // overridePendingTransition(R.anim.hold_in, R.anim.push_right_out);
    }

    /**
     * Get lastknown location
     *
     * @return
     */
    protected Location getLastKnownLocation() {
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_LOW);
        String provider = lm.getBestProvider(criteria, true);
        if (provider == null) {
            return null;
        }

        return lm.getLastKnownLocation(provider);
    }
}
