/**
 *
 */
/**
 * @author Khai Nguyen
 *
 */
package com.jf.soml.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.jf.soml.R;
import com.jf.soml.adapter.NotificationAdapter;
import com.jf.soml.widget.HeaderListView;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class NotificationFragment extends BaseFragment implements
        OnItemClickListener {
    @InjectView(R.id.list_view)
    HeaderListView mListView;

//    @InjectView(R.id.progress_view)
    // CircleProgressView progressView;

    public static Fragment newInstance() {
        return new NotificationFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View rootView = inflater.inflate(R.layout.fragment_notification, null);
        ButterKnife.inject(this, rootView);

//        ((BaseDrawerActivity) getActivity()).getToolbar().setBackgroundColor(getResources().getColor(R.color.premium));

        initView();
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View rootView,
                            int position, long id) {
        // TODO Auto-generated method stub

    }

    @Override
    protected void initView() {
        // TODO Auto-generated method stub

        mListView.setAdapter(new NotificationAdapter(getActivity()));
    }


}