package com.jf.soml.adapter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class NotificationSection implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3479474594990319226L;

	private long date;

	private List<Notification> mNotificationList = new ArrayList<Notification>();

	public NotificationSection(long when) {
		// TODO Auto-generated constructor stub
		this.date = when;
	}

	public long getWhen() {
		return date;
	}

	public void setDate(long when) {
		this.date = when;
	}

	/**
	 * 
	 * @return
	 */
	public List<Notification> getNotificationList() {
		return mNotificationList;
	}

	/**
	 * 
	 * @param notifications
	 */
	public void setNotificationList(List<Notification> notifications) {
		// TODO Auto-generated method stub
		mNotificationList = notifications;
	}

	public void add(List<Notification> list) {
		// TODO Auto-generated method stub
		mNotificationList.addAll(list);
	}

	public void add(Notification categoryModel) {
		// TODO Auto-generated method stub
		mNotificationList.add(categoryModel);
	}

	public int getRowNumb() {
		// TODO Auto-generated method stub
		return mNotificationList.size();
	}

	public Notification getRow(int row) {
		// TODO Auto-generated method stub
		return mNotificationList.get(row);
	}
}
