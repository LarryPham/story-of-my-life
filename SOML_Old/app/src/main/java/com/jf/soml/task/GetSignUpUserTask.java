package com.jf.soml.task;

import android.content.Context;

import com.jf.soml.rest.RestClientFactory;
import com.jf.soml.rest.RestClientTask;

import org.json.JSONObject;

/**
 * Created by Khai Ng on 12/8/14.
 */
public class GetSignUpUserTask extends RestClientTask {
    private String password;
    private String name, email;

    public GetSignUpUserTask(Context context, String name, String email, String password) {
        super(context, RestClientFactory.getInstance().getPhotoClient(context));
        this.password = password;
        this.name = name;
        this.email = email;
    }


    @Override
    protected void doExecute() {
//        restClient.addParam("email", email);
//        restClient.addParam("name", name);
//        restClient.addParam("password", password);
//        restClient.addParam("password_confirmation", password);

        try {
            JSONObject data = new JSONObject();
            data.put("email", email);
            data.put("name", name);
            data.put("password", password);
            data.put("password_confirmation", password);

            restClient.setJSONString(data.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }


        restClient.post("/api/account");
    }
}
