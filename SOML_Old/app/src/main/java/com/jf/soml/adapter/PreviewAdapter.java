package com.jf.soml.adapter;

import java.io.File;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jf.soml.R;
import com.jf.soml.model.Photo;
import com.jf.soml.model.Story;
import com.jf.soml.widget.SquareImageView;
import com.squareup.picasso.Picasso;

public class PreviewAdapter extends BasicAdapter {
	private Story mStory;

	public PreviewAdapter(Context ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}

	public PreviewAdapter(Context ctx, Story story) {
		super(ctx);
		// TODO Auto-generated constructor stub
		this.mStory = story;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mStory.getOfflinePhotoList().size();
	}

	@Override
	public Photo getItem(int position) {
		// TODO Auto-generated method stub
		return mStory.getOfflinePhotoList().get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;

		if (convertView == null) {
			convertView = LayoutInflater.from(getContext()).inflate(
					R.layout.view_preview, parent, false);

			holder = new ViewHolder();
			holder.imageView = (SquareImageView) convertView
					.findViewById(R.id.image_view);
			holder.titleView = (TextView) convertView
					.findViewById(R.id.title_view);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		Photo photo = mStory.getOfflinePhotoList().get(position);

		holder.titleView.setText(photo.getCaption());
		Picasso.with(getContext()).load(new File(photo.getOfflineUri()))
				.resize(300, 300).into(holder.imageView);

		return convertView;
	}

	class ViewHolder {
		public SquareImageView imageView;
		public TextView titleView;
	}

}
