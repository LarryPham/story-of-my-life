package com.jf.soml.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

import com.jf.soml.R;
import com.jf.soml.activity.ProfileActivity;
import com.jf.soml.adapter.FriendAdapter;
import com.jf.soml.common.DataManager;
import com.jf.soml.model.UserModel;
import com.jf.soml.parse.DataParse;
import com.jf.soml.rest.RestClientTask;
import com.jf.soml.task.GetUserListTask;
import com.jf.soml.widget.CircleProgressView;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by hai on 12/23/14.
 */
public class FollowingFragment extends ScrollTabHolderFragment implements FriendAdapter.OnFollowCallback, AbsListView.OnScrollListener {

    private static final Handler HANDLE = new Handler();

    private static final String ARG_POSITION = "position";

    @InjectView(R.id.list_view)
    ListView mListView;

    @InjectView(R.id.progress_view)
    CircleProgressView mProgressView;

    FriendAdapter mFriendAdapter;

    int mPosition ;

    public static Fragment newInstance(int position) {
        FollowingFragment fragment = new FollowingFragment();

        Bundle bundle = new Bundle();
        bundle.putInt(ARG_POSITION, position);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPosition = getArguments().getInt(ARG_POSITION);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_friend, container, false);
        ButterKnife.inject(this, rootView);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    @Override
    protected void initView() {
        mProgressView.setVisibility(View.VISIBLE);

        mFriendAdapter = new FriendAdapter(getActivity(), true, this);

        mListView.setFooterDividersEnabled(false);
        mListView.setAdapter(mFriendAdapter);
        mProgressView.setVisibility(View.GONE);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent =  new Intent(getActivity(), ProfileActivity.class);
                intent.putExtra("data", mFriendAdapter.getItem(position));
                startActivity(intent);
            }
        });

        DataManager.getInstance(getActivity()).getUserFollower(new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                mProgressView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                mProgressView.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

//                DataParse parse = new DataParse(response);
//                mFriendAdapter.addAll(parse.parseUserList());
            }
        });
    }

    @Override
    public void actionPre(RestClientTask task) {
        super.actionPre(task);
        mProgressView.setVisibility(View.VISIBLE);
    }

    @Override
    public void actionPost(RestClientTask task, JSONObject result) {
        super.actionPost(task, result);
        mProgressView.setVisibility(View.GONE);


        if (result == null) {
            return;
        }

        DataParse parse = new DataParse(result);
        if (result.has("code") && parse.getInt("code") == 0) {
            makeToast(parse.getString("error_message"));
            return;
        }

        if (task instanceof GetUserListTask) {


        }
    }

    @Override
    public void onFollow(final UserModel model, boolean follow) {
        DataManager.getInstance(getActivity()).followUser(model, follow, new JsonHttpResponseHandler(){
            @Override
            public void onStart() {
                super.onStart();
                mProgressView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                mProgressView.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                makeToast("Un-follow " + model.getName());

                HANDLE.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                         mFriendAdapter.remove(model);
                    }
                }, 1000);
            }
        });
    }

    @Override
    public void adjustScroll(int scrollHeight) {
        if (scrollHeight == 0 && mListView.getFirstVisiblePosition() >= 1) {
            return;
        }

        mListView.setSelectionFromTop(1, scrollHeight);

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (mScrollTabHolder != null)
            mScrollTabHolder.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount, mPosition);
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        // nothing
    }
}
