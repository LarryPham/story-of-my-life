package com.jf.soml.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;

import com.jf.soml.R;
import com.jf.soml.activity.base.BaseActivity;
import com.jf.soml.common.DataManager;
import com.jf.soml.model.UserModel;
import com.jf.soml.parse.DataParse;
import com.jf.soml.rest.PoseRestClient;
import com.jf.soml.rest.RestClientTask;
import com.jf.soml.task.GetSignUpUserTask;
import com.jf.soml.util.Logger;
import com.jf.soml.widget.MaterialEditText;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.commons.validator.routines.EmailValidator;
import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnEditorAction;

/**
 * Created by hai on 12/30/14.
 */
public class SignUpActivity extends BaseActivity {
    @InjectView(R.id.toolbar_view)
    Toolbar toolbarView;

    @InjectView(R.id.email_view)
    MaterialEditText emailView;

    @InjectView(R.id.password_view)
    MaterialEditText passwordView;

    @InjectView(R.id.confirm_view)
    MaterialEditText confirmView;

    @InjectView(R.id.name_view)
    MaterialEditText nameView;

    ProgressDialog progressDialog;

    UserModel userModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        tintStatusBar();
        ButterKnife.inject(this);
        setSupportActionBar(toolbarView);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initView();
    }

    @Override
    protected void initView() {
        progressDialog = new ProgressDialog(this);
    }


    @OnClick(R.id.btn_signup)
    public void onSignUpClick(View view) {
        signUpUser();
    }

    @OnEditorAction(R.id.confirm_view)
    public boolean onConfirmPasswordDone(KeyEvent key) {
        if (key.getKeyCode() == KeyEvent.KEYCODE_ENTER)
            signUpUser();
        return true;
    }

    /**
     * Sign up user
     */
    private void signUpUser() {
        String name = nameView.getText().toString();
        if (name.length() < 5) {
            makeToast("So nice to have your name");
            return;
        }

        String email = emailView.getText().toString();
        if (!EmailValidator.getInstance().isValid(email)) {
            makeToast("Invalid email address");
            return;
        }

        String password = passwordView.getText().toString();
        String confirm = confirmView.getText().toString();
        if (password.length() < 5) {
            makeToast("Password length at least 5 characters long");
            return;
        }

        if (!password.equals(confirm)) {
            makeToast("Password not match");
            return;
        }

        try {
            getSignUpUser(name, email, password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getSignUpUser(String name, String email, String password) throws JSONException {
        final RequestParams requestParams = new RequestParams();
        requestParams.add("name", name);
        requestParams.add("email", email);
        requestParams.add("password", password);


        PoseRestClient.getInstance().post("api/account", requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();

                progressDialog.setMessage("Registering user...");
                progressDialog.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                if (response == null) {
                    return;
                }
                Logger.e(response.toString());

                DataParse parse = new DataParse(response);
                if (response.has("code") && parse.getInt("code") == 0) {
                    makeToast("Sign in failed: " + parse.getString("error_message"));
                    Logger.e("Sign up failed: " + parse.getString("exception"));
                } else {
                    UserModel userModel = parse.parseUser();
                    if (userModel != null) {
                        DataManager.getInstance(SignUpActivity.this).setUserModel(userModel);

                        makeToast("Sign up successful");
                        finish();
                    } else {
                        makeToast("Sign up failed: " + parse.getString("error_message"));
                        Logger.e("Sign up failed: " + parse.getString("exception"));
                    }
                }
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (progressDialog != null)
                    progressDialog.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                Logger.e("HTTP " + statusCode + " " + responseString);
            }
        });
    }
}
