package com.jf.soml.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jf.soml.R;
import com.jf.soml.activity.CaptureActivity;
import com.jf.soml.activity.CreateActivity;
import com.jf.soml.widget.SlidingTabLayout;
import com.melnykov.fab.FloatingActionButton;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by hai on 12/23/14.
 */
public class HomeFragment extends BaseFragment {

    static final Handler HANDLER = new Handler();

    public static Fragment newInstance() {
        return new HomeFragment();
    }

    @InjectView(R.id.pager_view)
    ViewPager mViewPager;

    @InjectView(R.id.tab_view)
    SlidingTabLayout mPagerIndicator;

    @InjectView(R.id.btn_create)
    FloatingActionButton mCreateButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View rootView = inflater.inflate(R.layout.fragment_pager_payment, null);

        ButterKnife.inject(this, rootView);

        initView();

        return rootView;
    }

    @Override
    protected void initView() {
        // TODO Auto-generated method stub
        PagerAdapter pagerAdapter = new PagerAdapter(getChildFragmentManager());


        mViewPager.setAdapter(pagerAdapter);
        mViewPager.setOffscreenPageLimit(pagerAdapter.getCount());

        mPagerIndicator.setCustomTabView(R.layout.tab_indicator, R.id.tab_title);
        mPagerIndicator.setSelectedIndicatorColors(getResources().getColor(R.color.accent));
        mPagerIndicator.setDistributeEvenly(true);
        mPagerIndicator.setViewPager(mViewPager);

        mPagerIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {
            }

            @Override
            public void onPageSelected(int i) {
            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });

        mCreateButton.setVisibility(View.VISIBLE);
        mCreateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult((new Intent(getActivity(),
                        CreateActivity.class)), 100);
            }
        });
    }


    public class PagerAdapter extends FragmentPagerAdapter {
        private final String[] TITLES = {
                getActivity().getString(R.string.label_tab_latest),
                getActivity().getString(R.string.label_tab_trending),
                getActivity().getString(R.string.label_tab_choice)};

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return EditorFragment.newInstance();
                case 1:
                    return UpdateFragment.newInstance();
                default:
                    break;
            }

            return EditorFragment.newInstance();
        }
    }
}
