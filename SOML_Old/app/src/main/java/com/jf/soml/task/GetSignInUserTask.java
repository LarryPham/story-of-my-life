package com.jf.soml.task;

import android.content.Context;

import com.jf.soml.rest.RestClientFactory;
import com.jf.soml.rest.RestClientTask;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Khai Ng on 12/8/14.
 */
public class GetSignInUserTask extends RestClientTask {
    private String password;
    private String  email;

    public GetSignInUserTask(Context context, String email, String password) {
        super(context, RestClientFactory.getInstance().getPhotoClient(context));
        this.password = password;
        this.email = email;
    }


    @Override
    protected void doExecute() {
        try {
            JSONObject data = new JSONObject();
            data.put("email", email);
            data.put("password", password);

            restClient.setJSONString(data.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        restClient.post("/api/sessions");
    }
}
