package com.jf.soml.model;

import java.io.Serializable;

/**
 * Created by hai on 1/7/15.
 */
public class AuthToken implements Serializable {
    private String token;

    public AuthToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setAuthToken(String token) {
        this.token = token;
    }
}
