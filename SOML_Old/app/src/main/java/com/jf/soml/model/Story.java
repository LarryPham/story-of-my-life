package com.jf.soml.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.net.Uri;

import com.jf.soml.util.Logger;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQuery.CachePolicy;

@ParseClassName("Story")
public class Story extends ParseObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5349436834483557067L;

	/**
	 * Wraps a FindCallback so that we can use the CACHE_THEN_NETWORK caching
	 * policy, but only call the callback once, with the first data available.
	 */
	private abstract static class StoryFindCallback extends FindCallback<Story> {
		private boolean isCachedResult = true;
		private boolean calledCallback = false;

		@Override
		public void done(List<Story> objects, ParseException e) {
			if (!calledCallback) {
				if (objects != null) {
					calledCallback = true;
					doneOnce(objects, null);
				} else if (!isCachedResult) {
					doneOnce(null, e);
				}
			}
			isCachedResult = false;
		}

		/**
		 * Override this method with the callback that should only be called
		 * once.
		 */
		protected abstract void doneOnce(List<Story> objects, ParseException e);
	}

	/**
	 * Creates a query for talks with all the includes
	 */
	private static ParseQuery<Story> createQuery() {
		ParseQuery<Story> query = new ParseQuery<Story>(Story.class);
		// query.include("name");
		// query.include("name");
		// query.include("description");
		// query.include("user_name");
		// query.include("user_avatar");
		// query.include("thumb_url");
		// query.include("comment_count");
		// query.include("like_count");
		// query.include("location");
		// query.include("latitude");
		// query.include("longtitude");
		// query.include("createAt");
		// query.include("objectId");
		query.setCachePolicy(CachePolicy.CACHE_THEN_NETWORK);
		return query;
	}

	/**
	 * Gets the objectId of the Story associated with the given URI.
	 */
	public static String getStoryId(Uri uri) {
		List<String> path = uri.getPathSegments();
		if (path.size() != 2 || !"story".equals(path.get(0))) {
			throw new RuntimeException("Invalid URI for story: " + uri);
		}
		return path.get(1);
	}

	/**
	 * Retrieves the set of all stories, ordered by time. Uses the cache if
	 * possible.
	 */
	public static void findInBackground(final FindCallback<Story> callback) {
		ParseQuery<Story> query = Story.createQuery();
		query.findInBackground(new StoryFindCallback() {

			@Override
			protected void doneOnce(List<Story> objects, ParseException e) {
				if (objects != null) {
					Collections.sort(objects, StoryComparator.get());
				}
				callback.done(objects, e);
			}
		});
	}

	/**
	 * Gets the data for a single story. We use this instead of calling fetch on
	 * a ParseObject so that we can use query cache if possible.
	 */
	public static void getInBackground(final String objectId,
			final GetCallback<Story> callback) {
		ParseQuery<Story> query = Story.createQuery();
		query.whereEqualTo("objectId", objectId);
		query.findInBackground(new StoryFindCallback() {
			@Override
			protected void doneOnce(List<Story> objects, ParseException e) {
				if (objects != null) {
					if (objects.size() < 1) {
						callback.done(null, new ParseException(
								ParseException.OBJECT_NOT_FOUND,
								"No story with id " + objectId + " was found."));
					} else {
						callback.done(objects.get(0), e);
					}
				} else {
					callback.done(null, e);
				}
			}
		});
	}

	public static void findInBackground(String title,
			final GetCallback<Story> callback) {
		ParseQuery<Story> talkQuery = ParseQuery.getQuery(Story.class);
		talkQuery.whereEqualTo("name", title);
		talkQuery.getFirstInBackground(new GetCallback<Story>() {

			@Override
			public void done(Story talk, ParseException e) {
				if (e == null) {
					callback.done(talk, null);
				} else {
					callback.done(null, e);
				}
			}
		});
	}

	/**
	 * Returns a URI to use in Intents to represent this story. The format is
	 * soml://story/theObjectId
	 */
	public Uri getUri() {
		Uri.Builder builder = new Uri.Builder();
		builder.scheme("soml");
		builder.path("story/" + getObjectId());
		return builder.build();
	}

	public String getName() {
		return getString("name");
	}

	public String getDesc() {
		return getString("description");
	}

	public String getUserName() {
		return getString("user_name");
	}

	public String getUserAvatar() {
		return getString("user_thumb");
	}

	public String getFeaturePhoto() {
		return getString("thumb_url");
	}

	public int getCommentCount() {
		return getInt("comment_count");
	}

	public int getLikeCount() {
		return getInt("like_count");
	}

	public String getLocation() {
		return getString("location");
	}

	public double getLatitude() {
		return getDouble("latitude");
	}

	public double getLongtitude() {
		return getDouble("longtitude");
	}

	public List<String> getImageList() {
		// TODO Auto-generated method stub
		List<String> result = new ArrayList<String>();

		try {
			JSONObject jsonObject = new JSONObject(getString("photo"));
			JSONArray imageArray = jsonObject.getJSONArray("data");
			for (int j = 0; j < imageArray.length(); j++) {
				result.add(imageArray.getString(j));
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}

	// Offline photo list
	private List<Photo> mPhotoList = new ArrayList<Photo>();

	public void setOfflinePhotoList(List<Photo> list) {
		// TODO Auto-generated method stub
		mPhotoList.clear();
		if (list != null && list.size() > 0)
			mPhotoList.addAll(list);
	}

	public List<Photo> getOfflinePhotoList() {
		// TODO Auto-generated method stub
		return mPhotoList;
	}

	public void setTitle(String title) {
		// TODO Auto-generated method stub
		this.put("name", title);

	}

}
