package com.jf.soml.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.EditText;

import com.jf.soml.R;

public class BetterEditText extends EditText {

	private int[] lockedCompoundPadding;
	private boolean frozen = false;

	public BetterEditText(Context context) {
		super(context);
		init(null);
	}

	public BetterEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(attrs);
	}

	public BetterEditText(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(attrs);
	}

	public void init(AttributeSet attrs) {
		if (attrs != null) {
			TypedArray a = getContext().obtainStyledAttributes(attrs,
					R.styleable.BetterTextView);

			String typefaceName = a
					.getString(R.styleable.BetterTextView_typeface);

			if (typefaceName != null) {
				Typeface typeface = Typeface.createFromAsset(getContext()
						.getAssets(), String.format("fonts/%s.ttf",
						typefaceName));
				setTypeface(typeface);
			}

			if (a.hasValue(R.styleable.BetterTextView_backgroundx)) {
				Drawable background = a
						.getDrawable(R.styleable.BetterTextView_backgroundx);
				if (background != null) {
				} else {
					this.setBackgroundColor(a.getColor(
							R.styleable.BetterTextView_backgroundx, 0xff000000));
				}
			}

			a.recycle();
		}
	}

	@Override
	public void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		freeze();
		int restoreColor = getCurrentTextColor();
		this.setCompoundDrawables(null, null, null, null);
		this.setShadowLayer(0, 0, 0, restoreColor);
		this.setTextColor(restoreColor);
		setTextColor(restoreColor);
		unfreeze();
	}

	// Keep these things locked while onDraw in processing
	public void freeze() {
		lockedCompoundPadding = new int[] { getCompoundPaddingLeft(),
				getCompoundPaddingRight(), getCompoundPaddingTop(),
				getCompoundPaddingBottom() };
		frozen = true;
	}

	public void unfreeze() {
		frozen = false;
	}

	@Override
	public void requestLayout() {
		if (!frozen)
			super.requestLayout();
	}

	@Override
	public void postInvalidate() {
		if (!frozen)
			super.postInvalidate();
	}

	@Override
	public void postInvalidate(int left, int top, int right, int bottom) {
		if (!frozen)
			super.postInvalidate(left, top, right, bottom);
	}

	@Override
	public void invalidate() {
		if (!frozen)
			super.invalidate();
	}

	@Override
	public void invalidate(Rect rect) {
		if (!frozen)
			super.invalidate(rect);
	}

	@Override
	public void invalidate(int l, int t, int r, int b) {
		if (!frozen)
			super.invalidate(l, t, r, b);
	}

	@Override
	public int getCompoundPaddingLeft() {
		return !frozen ? super.getCompoundPaddingLeft()
				: lockedCompoundPadding[0];
	}

	@Override
	public int getCompoundPaddingRight() {
		return !frozen ? super.getCompoundPaddingRight()
				: lockedCompoundPadding[1];
	}

	@Override
	public int getCompoundPaddingTop() {
		return !frozen ? super.getCompoundPaddingTop()
				: lockedCompoundPadding[2];
	}

	@Override
	public int getCompoundPaddingBottom() {
		return !frozen ? super.getCompoundPaddingBottom()
				: lockedCompoundPadding[3];
	}

}
