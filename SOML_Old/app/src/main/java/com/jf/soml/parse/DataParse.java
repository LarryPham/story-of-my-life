package com.jf.soml.parse;

import com.jf.soml.model.AuthToken;
import com.jf.soml.model.UserModel;
import com.jf.soml.util.Logger;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DataParse extends BaseParser {

    public DataParse(JSONObject result) {
        super(result);
        // TODO Auto-generated constructor stub
        Logger.d("DataParse", "---> " + result.toString());
    }

    public boolean isOK() {
        // TODO Auto-generated constructor stub
        return getInt("code") == 0;
    }

    public boolean isLogin() {
        // TODO Auto-generated method stub
        return getInt("code") == 204;
    }

    public String getMessage() {
        // TODO Auto-generated method stub
        return getString("message");
    }

    public UserModel parseUser() {
        UserModel userModel = new UserModel();

        JSONObject userObject = getJsonObject("user");
        if (userObject == null) {
            return null;
        }

        userModel.setFullName(getString(userObject, "name"));
        userModel.setEmail(getString(userObject, "email"));
        userModel.setStoryCount(new Random().nextInt(30));
        userModel.setAuthToken(new AuthToken(getString(userObject, "auth_token")));
        userModel.setThumbnail("http://images.boomsbeat.com/data/images/full/32034/422817_original-jpg.jpg");

        return userModel;
    }

    public UserModel parseAccountInfo() {
        UserModel userModel = new UserModel();

        JSONObject userObject = getJsonObject("account");
        if (userObject == null) {
            return null;
        }

        userModel.setId(getInt(userObject, "id"));
        userModel.setFullName(getString(userObject, "name"));
        userModel.setEmail(getString(userObject, "email"));
        userModel.setStoryCount(new Random().nextInt(30));
        userModel.setThumbnail("http://images.boomsbeat.com/data/images/full/32034/422817_original-jpg.jpg");

        return userModel;
    }

    public List<UserModel> parseUserList() {
        JSONArray jsonArray = getJSONArray("users");
        if (jsonArray == null || jsonArray.length() == 0)
            return null;

        List<UserModel> result = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject userObject = getJsonObject(jsonArray, i);

            UserModel model = new UserModel(getInt(userObject, "id"));
            model.setEmail(getString(userObject, "email"));
            model.setFullName(getString(userObject, "name"));
            model.setThumbnail("http://images.boomsbeat.com/data/images/full/32034/422817_original-jpg.jpg");
            model.setStoryCount(new Random().nextInt(30));

            result.add(model);
        }

        return result;

    }
}
