package com.jf.soml.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.jf.soml.R;

import java.util.ArrayList;
import java.util.List;

public class NotificationAdapter extends SectionAdapter {
    private List<NotificationSection> mNotificationList = new ArrayList<NotificationSection>();

    public NotificationAdapter(Context context) {
        // TODO Auto-generated constructor stub
        super(context);
    }

    @Override
    public int numberOfSections() {
        // TODO Auto-generated method stub
//		return mNotificationList.size();
        return 4;
    }


    @Override
    public int numberOfRows(int section) {
        // TODO Auto-generated method stub
//		return mNotificationList.get(section).getRowNumb();
        return 5;
    }


    public NotificationSection getSectionItem(int section) {
        // TODO Auto-generated method stub
//        return mNotificationList.get(section);
        return null;
    }


    @Override
    public Notification getRowItem(int section, int row) {
        // TODO Auto-generated method stub
//        return mNotificationList.get(section).getRow(row);
        return null;
    }

    @Override
    public View getRowView(int section, int row, View convertView,
                           ViewGroup parent) {
        // TODO Auto-generated method stub
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.view_menu_sector, null);
        }

        Notification notification = getRowItem(section, row);

        ((TextView) convertView.findViewById(R.id.title_view))
                .setText("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua");


        ((TextView) convertView.findViewById(R.id.count_view))
                .setText("5 hours ago");

        return convertView;
    }

    @Override
    public View getSectionHeaderView(int section, View convertView,
                                     ViewGroup parent) {
        // TODO Auto-generated method stub
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.view_menu_headr, null);
        }

        ((TextView) convertView.findViewById(R.id.sector_view))
                .setText(" 01 December 2014");

        return convertView;
    }

    @Override
    public boolean hasSectionHeaderView(int section) {
        return true;
    }

    @Override
    public int getSectionHeaderViewTypeCount() {
        return 2;
    }

    @Override
    public int getSectionHeaderItemViewType(int section) {
        return section % 2;
    }

}
