package com.jf.soml.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import com.jf.soml.R;
import com.jf.soml.activity.base.NavigationDrawerActivity;
import com.jf.soml.common.DataManager;
import com.jf.soml.fragment.DiscoverFragment;
import com.jf.soml.fragment.FriendFragment;
import com.jf.soml.fragment.HomeFragment;
import com.jf.soml.fragment.NotificationFragment;
import com.jf.soml.gmc.GcmUtil;
import com.jf.soml.model.UserModel;
import com.jf.soml.util.Logger;

import com.jf.soml.widget.drawer.MaterialAccount;
import com.jf.soml.widget.drawer.MaterialAccountListener;

public class HomeActivity extends NavigationDrawerActivity implements MaterialAccountListener {
    MaterialAccount materialAccount;

    final String COVER_IMAGE_URL = "https://lh5.googleusercontent.com/-DnUk7SnlO0w/VH1hxjXBz8I/AAAAAAAAMtc/ad9I44PQ2to/s630-fcrop64=1,000012cdfffff97c/big_hero_6_disney_baymax-wallpaper-1920x1200.jpg";

    @Override
    public void init(Bundle savedInstanceState) {
        tintStatusBar();

        UserModel model = DataManager.getInstance(this).getCurrentUser();

        materialAccount = new MaterialAccount(model.getName(), model.getEmail(), new ColorDrawable(Color.parseColor("#9e9e9e")), COVER_IMAGE_URL);
        materialAccount.setPhoto(model.getThumbnail());

        addAccount(materialAccount);
        setAccountListener(this);
        allowArrowAnimation();
        addSection(newSection("Home", HomeFragment.newInstance()));
        addSection(newSection("Discovery", DiscoverFragment.newInstance(0)));
        addSection(newSection("Friends", FriendFragment.newInstance()));
        // addSection(newSection("Notification", NotificationFragment.newInstance()));

        addBottomSection(newSection("Settings", this.getResources().getDrawable(R.drawable.ic_settings_black_24dp), new Intent(this, ProfileActivity.class)));

        addMultiPaneSupport();

        mGcmUtil = GcmUtil.getInstance(this);

        if (mGcmUtil.checkPlayService(this)) {
            if (mGcmUtil.getRegistrationId().isEmpty()) {
                GcmUtil.getInstance(this).register(new GcmUtil.GcmRegisterCallback() {
                    @Override
                    public void onRegistered(String registrationId) {
                        Logger.i("gcm id: " + registrationId);
                        // updateGcmRegistrationId(registrationId);
                    }
                });
            } else {
                Logger.i("gcm id: " + mGcmUtil.getRegistrationId());

                // updateGcmRegistrationId(mGcmUtil.getRegistrationId());
            }
        }
    }

    private GcmUtil mGcmUtil;

    @Override
    protected void initView() {

    }

    @Override
    public void onAccountOpening(MaterialAccount account) {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }

    @Override
    public void onChangeAccount(MaterialAccount newAccount) {
    }

}
