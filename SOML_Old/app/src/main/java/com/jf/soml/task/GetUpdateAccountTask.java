package com.jf.soml.task;

import android.content.Context;

import com.jf.soml.rest.RestClientFactory;
import com.jf.soml.rest.RestClientTask;

/**
 * Created by Khai Ng on 12/8/14.
 */
public class GetUpdateAccountTask extends RestClientTask {

    public GetUpdateAccountTask(Context context) {
        super(context, RestClientFactory.getInstance().getPhotoClient(context));
    }

    @Override
    protected void doExecute() {
        restClient.put("/api/account");
    }
}
