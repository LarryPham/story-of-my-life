package com.jf.soml.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.jf.soml.R;
import com.jf.soml.activity.base.BaseActivity;
import com.jf.soml.adapter.PreviewAdapter;
import com.jf.soml.model.Photo;
import com.jf.soml.model.Story;
import com.jf.soml.util.Logger;
import com.jf.soml.widget.MaterialEditText;
import com.parse.ParseException;
import com.parse.SaveCallback;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class PreviewActivity extends BaseActivity implements
        OnItemClickListener {
    private static final String THUMB_URL = "https://dl.dropboxusercontent.com/u/3701068/soml/10808910_719522848143596_1135534957_n.jpg";
    private static final String AVATAR_URL = "https://dl.dropboxusercontent.com/u/3701068/soml/10731600_418309671649595_1315982655_n.jpg";

    private GridView mGridView;
    private PreviewAdapter mPhotoAdapter;

    private Story mPhotoStory;

    @InjectView(R.id.toolbar_view)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);
        ButterKnife.inject(this);
        setSupportActionBar(toolbar);
        initView();
    }


    @Override
    protected void initView() {
        // TODO Auto-generated method stub
        mGridView = (GridView) findViewById(R.id.grid_view);

        findViewById(R.id.btn_publish).setOnClickListener(
                new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        prepareStory();
                    }
                });

        mPhotoStory = (Story) getIntent().getSerializableExtra("data");

        mPhotoAdapter = new PreviewAdapter(this, mPhotoStory);
        mGridView.setAdapter(mPhotoAdapter);
        mGridView.setOnItemClickListener(this);
    }

    private void editStory(String title, String desc, String location) {
        // TODO Auto-generated method stub
        mPhotoStory.put("name", title);
        mPhotoStory.put("description", desc);
        mPhotoStory.put("location", location);
        mPhotoStory.put("thumb_url", THUMB_URL);
        mPhotoStory.put("user_name", "Hai Nam");
        mPhotoStory.put("user_thumb", AVATAR_URL);

        try {
            List<Photo> photoList = mPhotoStory.getOfflinePhotoList();
            JSONArray jsonArray = new JSONArray();

            for (int i = 0; i < photoList.size(); i++) {
                jsonArray.put(photoList.get(i).getOfflineUri());
            }

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("data", jsonArray);

            Logger.e(jsonObject.toString());

            mPhotoStory.put("photo", jsonObject.toString());
        } catch (Exception e) {
            // TODO: handle exception
        }

        mPhotoStory.saveInBackground(new SaveCallback() {

            @Override
            public void done(ParseException e) {
                // TODO Auto-generated method stub
                if (e == null) {
                    makeToast("Done");
                    getIntent().putExtra("data", mPhotoStory);

                    setResult(RESULT_OK);
                    finish();
                } else {
                    makeToast("Save story failed. Try again later"
                    );
                }
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view,
                            final int position, long id) {
        // TODO Auto-generated method stub
        editCaption(mPhotoAdapter.getItem(position), position);
    }

    @SuppressLint("InflateParams")
    private void editCaption(Photo photo, final int position) {
        // TODO Auto-generated method stub
        final View submitView = LayoutInflater.from(this).inflate(R.layout.view_edit_caption, null);
        final MaterialEditText captionView = (MaterialEditText) submitView
                .findViewById(R.id.title_view);

        new MaterialDialog.Builder(this)
                .title("Edit Caption")
                .customView(submitView)
                .positiveText("Submit")
                .negativeText("Cancel")
                .callback(new MaterialDialog.Callback() {
                    @Override
                    public void onNegative(MaterialDialog materialDialog) {
                        mPhotoAdapter.getItem(position).setCaption(
                                captionView.getText().toString());
                        mPhotoAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onPositive(MaterialDialog materialDialog) {

                    }
                })
                .build()
                .show();


    }

    @SuppressLint("InflateParams")
    private void prepareStory() {
        // TODO Auto-generated method stub
        if (mPhotoStory.getString("name") != null) {
            mPhotoStory.saveInBackground(new SaveCallback() {

                @Override
                public void done(ParseException e) {
                    // TODO Auto-generated method stub
                    if (e == null) {
                        makeToast("Done");
                        getIntent().putExtra("data", mPhotoStory);

                        setResult(RESULT_OK);
                        finish();
                    } else {
                        makeToast("Save story failed. Try again later");
                    }
                }
            });

            return;
        }


        View submitView = LayoutInflater.from(this).inflate(
                R.layout.view_edit_story, null);

        final MaterialEditText titleView = (MaterialEditText) submitView
                .findViewById(R.id.title_view);

        final MaterialEditText descView = (MaterialEditText) submitView
                .findViewById(R.id.desc_view);

        final MaterialEditText locationView = (MaterialEditText) submitView
                .findViewById(R.id.location_view);

        new MaterialDialog.Builder(this)
                .title("Edit Caption")
                .customView(submitView)
                .positiveText("Submit")
                .negativeText("Cancel")
                .callback(new MaterialDialog.Callback() {
                    @Override
                    public void onNegative(MaterialDialog materialDialog) {
                        editStory(titleView.getText().toString(), descView.getText()
                                .toString(), locationView.getText().toString());
                    }

                    @Override
                    public void onPositive(MaterialDialog materialDialog) {

                    }
                })
                .build().show();

    }
}
