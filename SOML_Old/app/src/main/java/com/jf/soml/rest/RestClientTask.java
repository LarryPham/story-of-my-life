package com.jf.soml.rest;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.util.Random;

public abstract class RestClientTask extends
        AsyncTask<Void, Integer, JSONObject> {
    private static final int MAX_ATTEMPTS = 3;
    private static final int BACKOFF = 2000;
    private static final Random RANDOM = new Random();
    private static final String TAG = RestClientTask.class.getSimpleName();

    protected RestClient restClient;

    private Context context;
    private OnPostExecuteDelegate onPostExecuteDelegate;
    private OnPreExecuteDelegate onPreExecuteDelegate;
    private OnProgressExecuteDelegate onProgressExecuteDelegate;

    private boolean needReplace = false;

    public RestClientTask(Context ctx) {
        context = ctx;
        restClient = RestClient.getInstance(ctx);
    }

    public RestClientTask(Context ctx, RestClient client) {
        // TODO Auto-generated constructor stub
        context = ctx;
        restClient = client;
    }

    /**
     * Get context
     *
     * @return
     */
    public Context getContext() {
        return context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (null != onPreExecuteDelegate) {
            onPreExecuteDelegate.actionPre(this);
        }
    }

    @Override
    protected void onPostExecute(JSONObject result) {
        super.onPostExecute(result);
        if (null != onPostExecuteDelegate) {
            onPostExecuteDelegate.actionPost(this, result);
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        if (null != onProgressExecuteDelegate) {
            onProgressExecuteDelegate.actionPublish(this, values);
        }

    }

    public JSONObject getResult() {
        if (null == restClient)
            return null;
        try {
            String result = restClient.getResponse();
            if (result == null || result.length() == 0) {
                return null;
            } else {
                if (!result.startsWith("{")) {
                    result = "{\"data\":" + result + "}";
                }

                return new JSONObject(result);
            }
        } catch (Exception e) {
            e.fillInStackTrace();
        }

        return null;
    }

    protected abstract void doExecute();

    @Override
    protected JSONObject doInBackground(Void... params) {
        long backOff = BACKOFF + RANDOM.nextInt(1000);
        for (int i = 0; i < MAX_ATTEMPTS; i++) {
            Log.d(TAG, "Try #" + i);
            doExecute();
            if (getResult() != null) {
                return getResult();
            } else {
                if (i == MAX_ATTEMPTS) {
                    break;
                }
                try {
                    Log.d(TAG, "HTTP " + restClient.getResponseCode());
                    Log.d(TAG, "Sleeping for " + backOff + "ms before retry");
                    Thread.sleep(backOff);
                } catch (InterruptedException e) {
                    Log.d(TAG, "Thread interrupted: Remaining retries aborted!");
                    Thread.currentThread().interrupt();
                    return null;
                }
                backOff *= 2;
            }
        }
        return null;
    }

    public void setNeedReplace(boolean replace) {
        this.needReplace = replace;
    }

    public boolean isNeedReplace() {
        return this.needReplace;
    }

    public void setOnPostExecuteDelegate(OnPostExecuteDelegate delegate) {
        onPostExecuteDelegate = delegate;
    }

    public void setOnProgressExecuteDelegate(OnProgressExecuteDelegate delegate) {
        onProgressExecuteDelegate = delegate;
    }

    public void setOnPreExecuteDelegate(OnPreExecuteDelegate delegate) {
        onPreExecuteDelegate = delegate;
    }

    public interface OnPostExecuteDelegate {
        public void actionPost(RestClientTask task, JSONObject result);
    }

    public interface OnPreExecuteDelegate {
        public void actionPre(RestClientTask task);
    }

    public interface OnProgressExecuteDelegate {
        public void actionPublish(RestClientTask task, Integer[] progress);
    }

}
