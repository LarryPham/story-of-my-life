package com.jf.soml.fragment;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Camera;
import android.hardware.Camera.Face;
import android.hardware.Camera.Parameters;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Toast;

import com.jf.soml.activity.PreviewActivity;
import com.jf.soml.R;
import com.jf.soml.camera.CameraFragment;
import com.jf.soml.camera.CameraHost;
import com.jf.soml.camera.CameraUtil;
import com.jf.soml.camera.ImageCleanupTask.ImageCleanUpCallback;
import com.jf.soml.camera.PictureTransaction;
import com.jf.soml.camera.SimpleCameraHost;
import com.jf.soml.model.Photo;
import com.jf.soml.model.Story;
import com.jf.soml.util.Logger;
import com.jf.soml.widget.SquareFrameLayout;

public class CaptureFragment extends CameraFragment implements
		OnSeekBarChangeListener, OnClickListener {
	private static final String KEY_USE_FFC = "com.commonsware.cwac.camera.demo.USE_FFC";

	public interface Contract {
		boolean isSingleShotMode();

		void setSingleShotMode(boolean mode);
	}

	private long lastFaceToast = 0L;
	private String flashMode = null;
	private boolean singleShotProcessing = false;

	private SimpleCameraHost mCameraHost;
	private SimpleCameraHost.Builder mCameraBuilder;

	private List<Photo> mPhotoList = new ArrayList<Photo>();

	public static CaptureFragment newInstance(boolean useFFC) {
		CaptureFragment fragment = new CaptureFragment();
		Bundle args = new Bundle();
		args.putBoolean(KEY_USE_FFC, useFFC);
		fragment.setArguments(args);
		return (fragment);
	}

	@Override
	public void onCreate(Bundle state) {
		super.onCreate(state);
		setHasOptionsMenu(true);

		mCameraBuilder = new SimpleCameraHost.Builder(new SquareCameraHost(
				getActivity())).photoDirectory(new File(Environment
				.getExternalStorageDirectory(), "SOML"));

		mCameraHost = mCameraBuilder.useFullBleedPreview(true).build();

		setHost(mCameraHost);
	}

	private SquareFrameLayout mCameraFrame;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View cameraView = super.onCreateView(inflater, container,
				savedInstanceState);

		View rootView = inflater.inflate(R.layout.fragment_camera, container,
				false);

		mCameraFrame = (SquareFrameLayout) rootView
				.findViewById(R.id.camera_view);
		((ViewGroup) mCameraFrame).addView(cameraView);

		rootView.findViewById(R.id.btn_capture).setOnClickListener(this);
		rootView.findViewById(R.id.btn_done).setOnClickListener(this);
		rootView.findViewById(R.id.btn_review).setOnClickListener(this);

		return (rootView);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onPause() {
		super.onPause();
		getActivity().invalidateOptionsMenu();
	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		switch (view.getId()) {
		case R.id.camera_view:
			autoFocus();

			break;

		case R.id.btn_capture:
			capture(new ImageCleanUpCallback() {

				@Override
				public void onComplete() {
					// TODO Auto-generated method stub
					Logger.e("Photo path: "
							+ mCameraHost.getPhotoFile().getAbsolutePath());

					mPhotoList.add(new Photo(mCameraHost.getPhotoFile()
							.getAbsolutePath(), mCameraHost.getPhotoFile()
							.getName()));
				}
			});

			break;

		case R.id.btn_done:
			Story story = new Story();
			story.setOfflinePhotoList(mPhotoList);

			Intent intent = new Intent(getActivity(), PreviewActivity.class);
			intent.putExtra("data", story);
			startActivityForResult(intent, 0);
			break;

		case R.id.btn_review:

			break;

		default:
			break;
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 0 && resultCode == Activity.RESULT_OK) {
			getActivity().setResult(Activity.RESULT_OK);
			getActivity().finish();
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// inflater.inflate(R.menu.menu_camera, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.camera:
			// capture();

			return (true);

		case R.id.autofocus:
			autoFocus();
			return (true);

		case R.id.single_shot:
			item.setChecked(!item.isChecked());
			getContract().setSingleShotMode(item.isChecked());

			return (true);

		}

		return super.onOptionsItemSelected(item);
	}

	public boolean isSingleShotProcessing() {
		return (singleShotProcessing);
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		// ignore
	}

	private Contract getContract() {
		return ((Contract) getActivity());
	}

	/**
	 * Capture picture
	 */
	public void capture(ImageCleanUpCallback callback) {
		PictureTransaction xact = new PictureTransaction(getHost());
		xact.flashMode(flashMode);

		takePicture(xact, callback);
	}

	class SquareCameraHost extends SimpleCameraHost implements
			Camera.FaceDetectionListener {
		boolean supportsFaces = false;

		public SquareCameraHost(Context _ctxt) {
			super(_ctxt);
		}

		@Override
		public boolean useFrontFacingCamera() {
			if (getArguments() == null) {
				return (false);
			}

			return (getArguments().getBoolean(KEY_USE_FFC));
		}

		@Override
		public boolean useSingleShotMode() {
			return false;
		}

		@Override
		public void saveImage(PictureTransaction xact, byte[] image) {
			if (useSingleShotMode()) {
				singleShotProcessing = false;
			} else {
				super.saveImage(xact, image);
			}
		}

		@Override
		public void autoFocusAvailable() {
			if (supportsFaces)
				startFaceDetection();
		}

		@Override
		public void autoFocusUnavailable() {
			stopFaceDetection();
		}

		@Override
		public void onCameraFail(CameraHost.FailureReason reason) {
			super.onCameraFail(reason);
			makeToast("Can not connect to camera!");
		}

		@Override
		public Parameters adjustPreviewParameters(Parameters parameters) {
			flashMode = CameraUtil.findBestFlashModeMatch(parameters,
					Camera.Parameters.FLASH_MODE_RED_EYE,
					Camera.Parameters.FLASH_MODE_AUTO,
					Camera.Parameters.FLASH_MODE_ON);

			if (doesZoomReallyWork() && parameters.getMaxZoom() > 0) {
				// zoom.setMax(parameters.getMaxZoom());
				// zoom.setOnSeekBarChangeListener(SquareCameraFragment.this);
			} else {
				// zoom.setEnabled(false);
			}

			if (parameters.getMaxNumDetectedFaces() > 0) {
				supportsFaces = true;
			} else {
				Toast.makeText(getActivity(),
						"Face detection not available for this camera",
						Toast.LENGTH_LONG).show();
			}

			return (super.adjustPreviewParameters(parameters));
		}

		@Override
		public void onFaceDetection(Face[] faces, Camera camera) {
			if (faces.length > 0) {
				long now = SystemClock.elapsedRealtime();

				if (now > lastFaceToast + 10000) {
					Toast.makeText(getActivity(), "I see your face!",
							Toast.LENGTH_LONG).show();
					lastFaceToast = now;
				}
			}
		}

		@Override
		@TargetApi(16)
		public void onAutoFocus(boolean success, Camera camera) {
			super.onAutoFocus(success, camera);
		}

		@Override
		public boolean mirrorFFC() {
			return false;
		}
	}

	private void makeToast(String message) {
		Toast toast = Toast.makeText(getActivity(), message, Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
	}
}