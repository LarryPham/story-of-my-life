package com.jf.soml.model;

import java.io.Serializable;

import com.parse.ParseClassName;
import com.parse.ParseObject;

@ParseClassName("Photo")
public class Photo extends ParseObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 647508149433096901L;

	private String userId;

	private String caption;

	private String offlineUri;

	public Photo() {
		// TODO Auto-generated constructor stub
	}

	public Photo(String offlineUri, String caption) {
		// TODO Auto-generated constructor stub
		this.caption = caption;
		this.offlineUri = offlineUri;
	}

	public String getOfflineUri() {
		// TODO Auto-generated method stub
		return offlineUri;
	}

	public String getCaption() {
		// TODO Auto-generated method stub
		return caption;
	}

	public void setCaption(String caption) {
		// TODO Auto-generated method stub
		this.caption = caption;

	}

}
