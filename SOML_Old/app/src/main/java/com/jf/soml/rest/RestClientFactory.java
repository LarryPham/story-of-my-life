package com.jf.soml.rest;

import android.content.Context;

public class RestClientFactory {
	private static RestClient baseClient;

	public static final String DATA_BASE_URL = "https://staging.soml.co";

	private RestClientFactory() {

	}

	public static RestClientFactory getInstance() {
		return RestClientSFHolder.INSTANCE;
	}

	private static class RestClientSFHolder {
		private static RestClientFactory INSTANCE = new RestClientFactory();
	}

	public RestClient getPhotoClient(Context context) {
		if (baseClient == null)
			baseClient = new RestClient(context, DATA_BASE_URL);

		return baseClient;
	}

}
