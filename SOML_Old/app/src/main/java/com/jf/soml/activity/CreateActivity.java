package com.jf.soml.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import com.jf.soml.R;
import com.jf.soml.activity.base.BaseActivity;
import com.jf.soml.widget.MaterialEditText;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class CreateActivity extends BaseActivity {
    final String COVER_IMAGE_URL = "https://lh5.googleusercontent.com/-DnUk7SnlO0w/VH1hxjXBz8I/AAAAAAAAMtc/ad9I44PQ2to/s630-fcrop64=1,000012cdfffff97c/big_hero_6_disney_baymax-wallpaper-1920x1200.jpg";

    @InjectView(R.id.toolbar_view)
    Toolbar toolbar;

    @InjectView(R.id.feature_photo)
    ImageView headerImageView;

    @InjectView(R.id.name_view)
    MaterialEditText nameView;

    @InjectView(R.id.tag_view)
    MaterialEditText tagView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);
        ButterKnife.inject(this);
        setSupportActionBar(toolbar);
        tintStatusBar();
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initView();
    }

    @Override
    protected void initView() {

        Picasso.with(this).load(COVER_IMAGE_URL).into(headerImageView);
    }
}
